using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Drawing;
using System.Net;
using System.IO;
using System.Drawing.Imaging;
using System.Diagnostics;
using EarthCape.Module.Core;
using EarthCape.Module.BHL.MOBOT.BHL;
using System.Text.RegularExpressions;
using DevExpress.ExpressApp.Xpo;
using EarthCape.Module.Library;

namespace EarthCape.Module.BHL
{
    public partial class BHL_ViewController : ViewController
    {
        public BHL_ViewController()
        {
   //         InitializeComponent();
            RegisterActions(components);
        }

 string apiKey="d68ffb6f-5f95-4ad0-ac3d-f0156924b8cf";


 
 private void GetTaxonomicName(string taxonName, bool DownloadThumbnails, bool DownloadOCR, bool DownloadFullSizeImages, bool DownloadPageNames, string folder)
 {
     taxonName =taxonName.Trim();
     using (soap Soap = new soap())
     {
         Name[] nameList = null;
         try
         {
             nameList = Soap.NameSearch(taxonName, apiKey);
         }
         catch (Exception ex)
         {
             if (ex is WebException)
                 // throw (new Exception("BHL web service did not respond. Please try again later. Tweet #fail @BioDivLibrary (OR check your internet connection)."));
                 throw (new Exception("BHL web service did not respond. Please try again later or check your internet connection."));
         }
         EarthCape.Module.Core.Project Project = null;
         /* if (AddReferencesToGroup != null)
          {
              using (UnitOfWork uow1 = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
              {
                  Project = uow1.FindObject<Group>(new BinaryOperator("Oid", AddReferencesToProject.Oid));
                  if (Project == null)
                      Project = uow1.FindObject<Group>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Oid", AddReferencesToProject.Oid));
                  Project.DefaultNames.Add(uow1.FindObject<TaxonomicName>(new BinaryOperator("Oid", TaxonomicName.Oid)));
                  Project.Save();
                  uow1.CommitChanges();
              }
          }*/
       if (nameList!=null)
         foreach (Name name in nameList)
         {
     //        log.WriteLine("Doing :" + "\t" + name + "\t" + DateTime.Now.ToString());

             Name name1 = null;
             name1 = Soap.NameGetDetailForName(name.NameConfirmed, apiKey);
             if (name1 != null)
             {
                 //XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                 IObjectSpace uow = Application.CreateObjectSpace();// new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);
               foreach (EarthCape.Module.BHL.MOBOT.BHL.Title title in name1.Titles)
                 {
                   Library.Title libraryTitle = GetTitle(uow, title);
                     if (title.Items.Length > 0)
                     {
                         // Title title1 = soap.GetTitleMetadata(title.TitleID.ToString(), "true", apiKey);
                         foreach (EarthCape.Module.BHL.MOBOT.BHL.Item item in title.Items)
                         {
                             Library.Item libraryItem = GetItem(uow, item);
                             libraryItem.Title = libraryTitle;
                             libraryItem.Save();
                             foreach (EarthCape.Module.BHL.MOBOT.BHL.Page page in item.Pages)
                             {
                                 Library.Page libraryPage = GetPage(uow, page);
                                 libraryPage.Item = libraryItem;
                                 libraryPage.Save();
                                 //Page page1 = soap.GetPageMetadata(page.PageID.ToString(), "true", "true", apiKey);
                                 if (DownloadFullSizeImages)
                                 {
                                     try
                                     {
                                         GetPageImage(libraryPage,uow,page, folder, log, Project);
                                         pageimages += 1;
                                         // FileLink linkedFile = GetLinkedFile(uow, page, DownloadThumbnails, DownloadOCR, DownloadFullSizeImages);

                                     }
                                     catch (Exception )
                                     {
                                         if (log!=null)
                                         log.WriteLine("Image download failed" + "\t" + page.FullSizeImageUrl);

                                     }
                                     finally
                                     {

                                     }
                                 }
                                 if (DownloadPageNames)
                                 {
                                     Name[] names=Soap.GetPageNames(page.PageID.ToString(), apiKey);
                                     foreach (Name bhlname in names)
                                     {
                                         CriteriaOperator cr = CriteriaOperator.And(new BinaryOperator("Page.Oid", libraryPage.Oid), new BinaryOperator("Name", bhlname.NameConfirmed));
                                         PageName pageName = uow.FindObject<PageName>(cr);
                                         if (pageName == null)
                                         {
                                             pageName = uow.FindObject<PageName>( cr,true);
                                         }
                                         if (pageName == null)
                                         {
                                             pageName = uow.CreateObject<PageName>();
                                             pageName.Page = libraryPage;
                                             pageName.Name = bhlname.NameConfirmed;
                                             pageName.Save();
                                         }
                                     } 
                                     
                                 }

                                 if (DownloadOCR)
                                 {
                                     try
                                     {
                                         MOBOT.BHL.Page page1 = Soap.GetPageMetadata(page.PageID.ToString(), "true", "false", apiKey);
                                         //FileItem linkedFile = GetPageOCR(uow, page1, fileStore, folder, log, Project,Reference);
                                         // FileLink linkedFile = GetLinkedFile(uow, page, DownloadThumbnails, DownloadOCR, DownloadFullSizeImages);
                                         //if (linkedFile != null)
                                         //   my_page.DerivedItems.Add(linkedFile);
                                         libraryPage.OCR = page1.OcrText;
                                         /* StringEditValueToDocumentModelConverter conv = new StringEditValueToDocumentModelConverter(DocumentFormat.PlainText, Encoding.Default);
                                          using (DocumentModel dom = new DocumentModel())
                                          {
                                                            
                                              conv.ConvertToDocumentModel(dom, page1.OcrText);
                                              DocumentModelToStringConverter converter2 = new DocumentModelToStringConverter(DocumentFormat.Rtf, Encoding.Default);
                                              string text = converter2.ConvertToEditValue(dom) as string;
                                              my_page.OcrFormatted = text;
                                          }*/

                                     }
                                     catch (Exception )
                                     {
                                         if (log!=null)
                                         log.WriteLine("OCR download failed" + "\t" + page.OcrUrl);

                                     }
                                     finally
                                     {

                                     }


                                 }

                                 libraryPage.Save();
                                /* TaxonomicName _taxonomicName = uow.FindObject<TaxonomicName>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", name));
                                 if (_taxonomicName == null)
                                 {
                                     _taxonomicName = uow.FindObject<TaxonomicName>(new BinaryOperator("FullName", name));
                                 }
                                 if (_taxonomicName != null)
                                 {
                                     libraryPage.Objects.Add(_taxonomicName);
                                     libraryPage.Save();
                                 }*/

                                 //AddFileAttachment(uow, Reference, "BHL Page", page.PageUrl, "http://www.biodiversitylibrary.org");

                                 //AddFileAttachment(uow, Reference, "Internet Archive", GetIAFolder(item.ItemUrl), "http://www.biodiversitylibrary.org");
                                 uow.CommitChanges();
                                 //log.Flush();
                             }
                         }
                     }
                 }
                 uow.Dispose();
             }
         }
     }
 }

    /*    private static TaxonomicName GetNameRef(TaxonomicName taxonomicName, Name name1, IObjectSpace uow, Title Reference, Page page)
        {
          // TaxonomicName taxonomicName = null;
           // string sourceId1 = "BHL-" + name1.NameBankID + "-" + page.PageID;
            //CriteriaOperator cr = CriteriaOperator.And(new ContainsOperator("Pages", new BinaryOperator("Path", page.PageUrl)), new BinaryOperator("NameDigitized", name1.NameConfirmed));
            CriteriaOperator cr1 = new BinaryOperator("Reference.Oid", Reference.Oid);
            CriteriaOperator cr2 = new BinaryOperator("Name", name1.NameFound);
            taxonomicName = uow.FindObject<TaxonomicName>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(cr1,cr2));
            if (taxonomicName == null)
                taxonomicName = uow.FindObject<TaxonomicName>(cr2);
            if (taxonomicName == null)
            {
              //TaxonomicName = new TaxonomicName(uow);// os.CreateObject<BhlPage>();
              //  TaxonomicName.SourceId = sourceId1;
               // if (name1.NameConfirmed == taxonomicName.Name)
               // {
                    //TaxonomicName.Cites = uow.FindObject<TaxonomicName>(new BinaryOperator("Oid", TaxonomicName.Oid));
                  /*  if (TaxonomicName.Cites.UbioNameBankId == null)
                    {
                        TaxonomyHelper.GetUBioData(TaxonomicName.TaxonomicName);
                    }*/
               // }
               /* else
                    if (AutoCreateNames)
                    {
                        if (name1.NameBankID > 0)
                        {
                            TaxonomicName temp_name = uow.FindObject<TaxonomicName>(new BinaryOperator("UbioNameBankId", name1.NameBankID));
                            if (temp_name == null)
                                temp_name = uow.FindObject<TaxonomicName>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("UbioNameBankId", name1.NameBankID));
                            if (temp_name != null)
                            {
                                if ((temp_name.UbioNameBankId == null))
                                    TaxonomyHelper.GetUBioData(temp_name);
                            }
                            else
                            {
                                temp_name = TaxonomyHelper.NameBankObjectToName(uow, name1.NameBankID);
                            }
                            TaxonomicName.TaxonomicName = temp_name;
                        }
                    }*/
             //   taxonomicName.Name = name1.NameConfirmed;//NameConfirmed;//


              //  taxonomicName.Save();
                //TaxonomicName.Work = Reference;
               /* if (string.IsNullOrEmpty(TaxonomicName.Pages))
                    TaxonomicName.Pages = page.;
                else
                    TaxonomicName.Pages = String.Format("{0}, {1}", TaxonomicName.Pages, page.Number);
            }
                     return taxonomicName;
        }
*/
        private static void AddFileAttachment(IObjectSpace uow, Reference Reference, string tagName, string pageUrl, string sourceUrl)
        {
            /*InternetLink uri = null;
            CriteriaOperator cr = new BinaryOperator("Uri", pageUrl);
            uri = uow.FindObject<InternetLink>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
            if (uri == null)
                uri = uow.FindObject<InternetLink>(cr);
            if (uri == null)
            {
                uri = new InternetLink(uow);
            }
            uri.Uri = pageUrl;
            uri.SourceUrl = sourceUrl;
            uri.Save();
*/
        
        }

   /*     private static CustomField GetTag(IObjectSpace uow, string tagName, CustomValueType tgt)
        {
            CustomField cat = uow.FindObject<CustomField>(new BinaryOperator("Name", tagName));
            if (cat == null)
                cat = uow.FindObject<CustomField>( new BinaryOperator("Name", tagName),true);
            if (cat == null)
            {
                cat =uow.CreateObject<CustomField>();
                cat.Name = tagName;
                cat.ValueType = tgt;
                cat.Save();
            }
            return cat;
        }
        */
       /* private static FileItem GetPageImage(IObjectSpace uow, Page page, FileStore fileStore, string folder, StreamWriter log, Group Project, Reference Reference)
        {
            FileItem FileItem = null;
            if ((!String.IsNullOrEmpty(folder)) && (fileStore != null))
                if (folder.IndexOf("\\") != 0) folder = "\\" + folder;
            string newpath = "";
            if (fileStore != null)
                newpath = String.Format("{0}{1}\\{2}", fileStore.FolderPath, folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID));
            else
                newpath = String.Format("{0}\\{1}", folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID));
            string temppath = "";
            FileDataStored file = null;
            if (!File.Exists(newpath))
            {

                Image img = Image.FromStream(new WebClient().OpenRead(page.FullSizeImageUrl));
                using (MemoryStream memstream = new MemoryStream())
                {
                    img.Save(memstream, ImageFormat.Jpeg);
                    temppath = String.Format("{0}{1}.jpg", Path.GetTempPath(), new Guid());
                    img.Save(temppath);

                }

            }
            else
            { temppath = newpath;
            file = new FileDataStored(uow);
            FileStore _fileStore = null;
            if (fileStore != null)
                _fileStore = uow.FindObject<FileStore>(new BinaryOperator("Oid", fileStore.Oid));
            file.LoadFromFile(temppath);
            file.Save();
            
            }
            string sourceId = page.PageID.ToString();
            //CriteriaOperator cr = CriteriaOperator.And(new ContainsOperator("Pages", new BinaryOperator("Path", page.PageUrl)), new BinaryOperator("NameDigitized", name1.NameConfirmed));
            CriteriaOperator cr = new BinaryOperator("SourceUrl", page.FullSizeImageUrl);
            FileItem = uow.FindObject<FileItem>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
            if (FileItem == null)
                FileItem = uow.FindObject<FileItem>(cr);
            if (FileItem == null)
            {

                FileItem = new FileItem(uow);
                if (fileStore==null)
                using (FileStream filestream = new FileStream(temppath, FileMode.Open))
                {
                    file = new FileDataStored(uow);
                    FileStore _fileStore = null;
                    if (fileStore != null)
                        _fileStore = uow.FindObject<FileStore>(new BinaryOperator("Oid", fileStore.Oid));
                    file.LoadFromStream(_fileStore, folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID), filestream);
                    file.Save();
                     filestream.Close();
                }


                FileItem.FileData = file;
              /*  FileItem.Objects.Add( = page.PageID.ToString();
                FileItem.SourceUrl = page.FullSizeImageUrl;
                FileItem.Work = Reference;*/
            /*    FileItem.Name = String.Format("BHL-PAGE-{0}.JPG", page.PageID);
               FileItem.Save();
            }
            uow.CommitChanges();

            return FileItem;
        }*/

        private static void GetPageImage(Library.Page Page,IObjectSpace uow, MOBOT.BHL.Page page, string folder, StreamWriter log, EarthCape.Module.Core.Project Project)
        {
          /*  if ((!String.IsNullOrEmpty(folder)) && (fileStore != null))
                if (folder.IndexOf("\\") != 0) folder = "\\" + folder;
            string newpath = "";
            if (fileStore != null)
                newpath = String.Format("{0}{1}\\{2}", fileStore.FolderPath, folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID));
            else
                newpath = String.Format("{0}\\{1}", folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID));
            string temppath = "";
            FileDataStored file = null;
            if (!File.Exists(newpath))
            {

                Image img = Image.FromStream(new WebClient().OpenRead(page.FullSizeImageUrl));
                using (MemoryStream memstream = new MemoryStream())
                {
                    img.Save(memstream, ImageFormat.Jpeg);
                    temppath =  newpath;
               //     temppath = String.Format("{0}\\{1}.jpg", newpath//Path.GetTempPath(), new Guid());
                    img.Save(temppath);

                }

            }
            else
            {
                temppath = newpath;
                file = uow.CreateObject<FileDataStored>();
                FileStore _fileStore = null;
                if (fileStore != null)
                    _fileStore = uow.FindObject<FileStore>(new BinaryOperator("Oid", fileStore.Oid));
                file.(temppath);
                file.Save();

            }
            //string sourceId = page.PageID.ToString();
            //CriteriaOperator cr = CriteriaOperator.And(new ContainsOperator("Pages", new BinaryOperator("Path", page.PageUrl)), new BinaryOperator("NameDigitized", name1.NameConfirmed));
            //CriteriaOperator cr = new BinaryOperator("SourceUrl", page.FullSizeImageUrl);
            if (file == null)
            {

                    // using (FileStream filestream = new FileStream(temppath, FileMode.Open))
                   // {
                file = uow.CreateObject<FileDataStored>();
                        FileStore _fileStore = null;
                        if (fileStore != null)
                            _fileStore = uow.FindObject<FileStore>(new BinaryOperator("Oid", fileStore.Oid));
                        file.LoadFromFile(temppath);
                        //file.LoadFromStream(_fileStore, folder, String.Format("BHL-PAGE-{0}.JPG", page.PageID), filestream);
                        file.Save();
                       // filestream.Close();
                  //  }
            }

                Page.FileData = file;
             
                Page.Save();
           
            uow.CommitChanges();*/

        }

        /*  private static FileItem GetPageOCR(IObjectSpace uow, MOBOT.BHL.Page page, FileStore fileStore, string folder, StreamWriter log, EarthCape.Module.Core.Project Project)
          {
              FileItem FileItem = null;
              if ((!String.IsNullOrEmpty(folder)) && (fileStore != null))
                  if (folder.IndexOf("\\") != 0) folder = "\\" + folder;
              string newpath = "";
              if (fileStore != null)
                  newpath = String.Format("{0}{1}\\{2}", fileStore.FolderPath, folder, String.Format("BHL-PAGE-{0}.TXT", page.PageID));
              else
                  newpath = String.Format("{0}\\{1}", folder, String.Format("BHL-PAGE-{0}.TXT", page.PageID));
         
              if (!File.Exists(newpath))
              {
                  string sourceId = page.PageID.ToString();
                  //CriteriaOperator cr = CriteriaOperator.And(new ContainsOperator("Pages", new BinaryOperator("Path", page.PageUrl)), new BinaryOperator("NameDigitized", name1.NameConfirmed));
                  CriteriaOperator cr = new BinaryOperator("SourceUrl", page.OcrUrl);
                  FileItem = uow.FindObject<FileItem>( cr,true);
                  if (FileItem == null)
                      FileItem = uow.FindObject<FileItem>(cr);
                  if (FileItem == null)
                  {

                      FileItem = uow.CreateObject<FileItem>();// new FileItem(uow);
                  }

                  string ocr = page.OcrText;
                  StreamWriter fileWriter = null;
                  fileWriter = File.CreateText(newpath);
                  try
                  {
                      fileWriter.Write(ocr);
                  }
                  finally
                  {
                      fileWriter.Close();
                  }
                  // using (FileStream filestream = new FileStream(filePath, FileMode.Open))
                  // {
                  FileDataStored file = uow.CreateObject<FileDataStored>();// new FileDataStored(uow);
                  FileStore _fileStore = null;
                  if (fileStore != null)
                      _fileStore = uow.FindObject<FileStore>(new BinaryOperator("Oid", fileStore.Oid));
                  file.LoadFromStream(_fileStore, folder, String.Format("BHL-PAGE-{0}.TXT", page.PageID), null);
                  FileItem.FileData = file;
                  //      filestream.Close();
                  // }

                  FileItem.SourceUrl = page.OcrUrl;
                  FileItem.SourceId = page.PageID.ToString();
                 // FileItem.Work = Reference;
                  FileItem.Name = String.Format("BHL-PAGE-{0}.TXT", page.PageID);
                 // if (Project != null)
                    //  FileItem.Groups.Add(Project);
                  FileItem.Save();
                  uow.CommitChanges();
              }
              return FileItem;
            
          }*/

        /*   private static Reference GetReference(IObjectSpace uow, Title title, Item item)
           {
               Reference Reference = null;
               CriteriaOperator cr1 = new BinaryOperator("SourceId", item.ItemID);
               Reference = uow.FindObject<title>(PersistentCriteriaEvaluationBehavior.InTransaction, cr1);
               if (Reference == null)
               {
                   Reference = uow.FindObject<title>(cr1);
                   if (Reference == null)
                   {
                       Reference = new title(uow);
                       //Reference.Year = item.Year;
                       Reference.SourceId = item.ItemID.ToString();
                       if (title.FullTitle != null)
                       {
                           Reference.Title = title.FullTitle;
                       }
                       if (title.ShortTitle != null)
                           Reference.ShortTitle = title.ShortTitle;
                       if (Reference.Title != null)
                           if (Reference.ShortTitle == null)
                               if (Reference.Title.Length > 255)
                                   Reference.ShortTitle = Reference.Title.Substring(0, 255);
                               else
                                   Reference.ShortTitle = Reference.Title;
                       if (Reference.ShortTitle != null)
                           if (Reference.Title == null)
                               Reference.Title = Reference.ShortTitle;
                       if (item.Volume != null)
                       {
                           if (item.Volume.Length > 255)
                               Reference.Volume = item.Volume.Substring(0, 255);
                           else
                               Reference.Volume = item.Volume;
                       }
                       Reference.SourceUrl = item.ItemUrl;
                       Reference.SourceContributor = item.Contributor;
                       Reference.CopyrightRegion = item.CopyrightRegion;
                       Reference.CopyrightStatus = item.CopyrightStatus;
                       Reference.Language = item.Language;
                       if (!string.IsNullOrEmpty(item.ItemThumbUrl))
                           try
                           {
                               Reference.Thumbnail = Image.FromStream(new WebClient().OpenRead(item.ItemThumbUrl));
                           }
                           catch (Exception ex)
                           {

                           }
                       Reference.Save();
                   }
               }
               return Reference;
           }*/
        private static Library.Title GetTitle(IObjectSpace uow, EarthCape.Module.BHL.MOBOT.BHL.Title title)
        {
            Library.Title libraryTitle = null;
            CriteriaOperator cr1 = new BinaryOperator("TitleId", title.TitleID);
            libraryTitle = uow.FindObject<Library.Title>( cr1,true);
            if (libraryTitle == null)
            {
                libraryTitle = uow.FindObject<Library.Title>(cr1);
                if (libraryTitle == null)
                {
                    libraryTitle = uow.CreateObject<Library.Title>();// new Library.Title(uow);
                    libraryTitle.TitleId = title.TitleID;
                    //Library.Title.Year = item.Year;
                   // libraryTitle.SourceId = item.ItemID.ToString();
                    if (title.FullTitle != null)
                    {
                        libraryTitle.FullName = title.FullTitle;
                    }
                    if (title.ShortTitle != null)
                        libraryTitle.ShortTitle = title.ShortTitle;
                    if (title.FullTitle != null)
                        if (title.ShortTitle == null)
                            if (title.FullTitle.Length > 255)
                                libraryTitle.ShortTitle = title.FullTitle.Substring(0, 255);
                            else
                                libraryTitle.ShortTitle = title.ShortTitle;
                    if (libraryTitle.ShortTitle != null)
                        if (libraryTitle.FullTitle == null)
                            libraryTitle.FullTitle = title.ShortTitle;
                  //libraryTitle.Authors=title.Authors[].;
                 // libraryTitle.=title.BibliographicLevel;
                  libraryTitle.CallNumber=title.CallNumber;
                  libraryTitle.DOI=title.Doi;
                  libraryTitle.Edition=title.Edition;
                  //libraryTitle.OtherID=string.Join(";",title.Identifiers[].);
                  libraryTitle.PublicationDate = title.PublicationDate;
                  //libraryTitle.=title.PublicationFrequency;
                  libraryTitle.Publisher=title.PublisherName;
                  libraryTitle.PublisherPlace = title.PublisherPlace;
                  libraryTitle.ShortTitle=title.ShortTitle;
                  libraryTitle.TitleUrl=title.TitleUrl;
                     if (!string.IsNullOrEmpty(title.Items[0].ItemThumbUrl))
                        try
                        {
                            libraryTitle.Thumbnail = Image.FromStream(new WebClient().OpenRead(title.Items[0].ItemThumbUrl));
                        }
                        catch (Exception )
                        {

                        }
                    libraryTitle.Save();
                }
            }
            return libraryTitle;
        }

        private static Library.Item GetItem(IObjectSpace uow, EarthCape.Module.BHL.MOBOT.BHL.Item item)
        {
            Library.Item libraryItem = null;
            CriteriaOperator cr1 = new BinaryOperator("ItemId", item.ItemID);
            libraryItem = uow.FindObject<Library.Item>(cr1,true);
            if (libraryItem == null)
            {
                libraryItem = uow.FindObject<Library.Item>(cr1);
                if (libraryItem == null)
                {
                    libraryItem = uow.CreateObject<Library.Item>();// new Library.Item(uow);
                    libraryItem.ItemId = item.ItemID;
                      if (item.Volume != null)
                    {
                        if (item.Volume.Length > 255)
                            libraryItem.Volume = item.Volume.Substring(0, 255);
                        else
                            libraryItem.Volume = item.Volume;
                    }
                      libraryItem.Volume = item.Volume;
                      libraryItem.Source = item.Source;
                      libraryItem.SourceIdentifier = item.SourceIdentifier;
                      libraryItem.Rights = item.Rights;
                      libraryItem.LicenseUrl = item.LicenseUrl;
                      libraryItem.ItemUrl = item.ItemUrl;
                     // libraryItem.Contributor = item.;
                    libraryItem.CopyrightRegion = item.CopyrightRegion;
                    libraryItem.CopyrightStatus = item.CopyrightStatus;
                    libraryItem.Language = item.Language;
                    if (!string.IsNullOrEmpty(item.ItemThumbUrl))
                        try
                        {
                            libraryItem.Thumbnail = Image.FromStream(new WebClient().OpenRead(item.ItemThumbUrl));
                        }
                        catch (Exception )
                        {

                        }
                    libraryItem.Save();
                }
            }
            return libraryItem;
        }

        private static Library.Page GetPage(IObjectSpace uow, EarthCape.Module.BHL.MOBOT.BHL.Page page)
        {
            Library.Page libraryPage = null;
            CriteriaOperator cr1 = new BinaryOperator("PageId", page.PageID);
            libraryPage = uow.FindObject<Library.Page>( cr1,true);
            if (libraryPage == null)
            {
                libraryPage = uow.FindObject<Library.Page>(cr1);
                if (libraryPage == null)
                {
                    libraryPage = uow.CreateObject<Library.Page>();
                    libraryPage.PageId = page.PageID;
                    if (page.Volume != null)
                    {
                        if (page.Volume.Length > 255)
                            libraryPage.Volume = page.Volume.Substring(0, 255);
                        else
                            libraryPage.Volume = page.Volume;
                    }
                    libraryPage.FileUrl = page.PageUrl;
                    libraryPage.Name = String.Format("BHL-PAGE-{0}.JPG", page.PageID);
                    libraryPage.PageNumber = page.PageNumbers[0].Number;
                    libraryPage.JpegUrl = page.FullSizeImageUrl;
                    libraryPage.Volume = page.Volume;
                    libraryPage.Year = page.Year;
                    libraryPage.Issue = page.Issue;
                    if (!string.IsNullOrEmpty(page.ThumbnailUrl))
                        try
                        {
                            libraryPage.Thumbnail = Image.FromStream(new WebClient().OpenRead(page.ThumbnailUrl));
                        }
                        catch (Exception )
                        {

                        }
                    libraryPage.Save();
                }
            }
            return libraryPage;
        }
        
        /*    private void BHL(TaxonomicName TaxonomicName)
             {
                 string taxonName = (TaxonomicName.Name).Trim();
                 using (NameService names = new NameService())
                 {

                     Name[] nameList = names.NameSearch(taxonName);
                     foreach (Name name in nameList)
                     {
                         if ((name.NameConfirmed == taxonName) || (name.NameConfirmed.IndexOf(String.Format("{0} ", taxonName)) > -1))
                         {
                             Name name1 = null;
                             try
                             {
                                 name1 = names.NameGetDetail(name.NameBankID.ToString());
                             }

                             catch (Exception ex)
                             {
                                 // throw new Exception(String.Format("BHL NameSearch service error: {0}", ex));
                             }
                             finally
                             {

                             }
                             if (name1 != null)
                             {
                                 //XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                                 IObjectSpace uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

                                 foreach (Title title in name1.Titles)
                                     if (title.Items.Length > 0)
                                     {
                                         foreach (Item item in title.Items)
                                         {
                                             foreach (Page page in title.Items[0].Pages)
                                             {
                                                 BhlPage refer = null;
                                                 CriteriaOperator cr = CriteriaOperator.And(new BinaryOperator("PageID", page.PageID), new BinaryOperator("NameBankID", name1.NameBankID));
                                                 refer = uow.FindObject<BhlPage>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                                                 if (refer == null)
                                                     refer = uow.FindObject<BhlPage>(cr);
                                                 if (refer == null)
                                                 {
                                                     refer = new BhlPage(uow);// os.CreateObject<BhlPage>();
                                                     if (title.PublicationTitle.Length > 255)
                                                         refer.Title = title.PublicationTitle.Substring(0, 254);
                                                     else
                                                         refer.Title = title.PublicationTitle;
                                                     refer.PublicationDetails = title.PublicationDetails;
                                                     refer.TaxonomicName = uow.FindObject<TaxonomicName>(new BinaryOperator("Oid", TaxonomicName.Oid));
                                                     refer.BhlName = name1.NameConfirmed;
                                                     refer.NameBankID = name1.NameBankID;
                                                     refer.TitleID = title.TitleID;
                                                     refer.ItemID = item.ItemID;
                                                     refer.ItemUrl = item.ItemUrl;
                                                     refer.ItemVolumeInfo = item.VolumeInfo;
                                                     refer.ImageUrl = page.ImageUrl;
                                                     refer.ThumbnailUrl = page.ThumbnailUrl;
                                                     //refer.PdfUrl = GetPath(page.ImageUrl);
                                                     //refer.ImageUrl = GetJPG(page.ImageUrl);
                                                     refer.Issue = page.Issue;
                                                     refer.PageNumber = page.Number;
                                                     refer.PageUrl = page.PageUrl;
                                                     refer.PageID = page.PageID;
                                                     refer.Volume = page.Volume;
                                                     refer.Year = page.Year;
                                                     if (!string.IsNullOrEmpty(page.ThumbnailUrl))
                                                         try
                                                         {
                                                             refer.Thumbnail = Image.FromStream(new WebClient().OpenRead(page.ThumbnailUrl));
                                                         }
                                                         catch (Exception ex)
                                                         {
                                                             refer.ErrorMessage = ex.Message;

                                                         }
                                                     refer.Author = title.Author;
                                                     refer.Url = title.TitleUrl;
                                                     refer.Save();
                                                 }
                                             }
                                             uow.CommitChanges();
                                         }
                                     }
                                 uow.Dispose();
                             }
                         }
                     }
                 }
             }*/
        private static string GetIAFolder(string p)
        {
            string imtype = "imageURL=";
            string[] words = p.Split('&');
            string result = "";
            string ending = "";
            foreach (string word in words)
            {
                if (word.IndexOf(imtype) > -1)
                {
                    result = word.Substring(imtype.Length, word.Length - imtype.Length);
                    if (!string.IsNullOrEmpty(result))
                    {
                        ending = result.Substring("http://www.archive.org/download/".Length, result.Length - "http://www.archive.org/download/".Length);
                        ending = ending.Split('/')[0];
                        string[] words1 = result.Split('_');
                        result = words1[0];
                    }
                }
            }
            return result;
        }

        private void singleChoiceAction_DownloadItem_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            
         /*   if (e.SelectedChoiceActionItem.Caption == "PDF")
            {
                System.Diagnostics.Process.Start(String.Format("{0}.pdf", ((BhlPage)e.CurrentObject).IAFolder));
            }
            if (e.SelectedChoiceActionItem.Caption == "BW PDF")
            {
                System.Diagnostics.Process.Start(String.Format("{0}_bw.pdf", ((BhlPage)e.CurrentObject).IAFolder));
            }
            if (e.SelectedChoiceActionItem.Caption == "TXT")
            {
                System.Diagnostics.Process.Start(String.Format("{0}_djvu.txt", ((BhlPage)e.CurrentObject).IAFolder));
            }
            if (e.SelectedChoiceActionItem.Caption == "JPG")
            {
                System.Diagnostics.Process.Start(String.Format("{0}_flippy.zip", ((BhlPage)e.CurrentObject).IAFolder));
            }
            if (e.SelectedChoiceActionItem.Caption == "GIF")
            {
                System.Diagnostics.Process.Start(String.Format("{0}.gif", ((BhlPage)e.CurrentObject).IAFolder));
            }*/
        }

        private void popupWindowShowAction_BhlSearch_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            BhlSearchParameters param = obs.CreateObject<BhlSearchParameters>();
            if (typeof(TaxonomicName).IsAssignableFrom(View.ObjectTypeInfo.Type))
            if (View.SelectedObjects.Count>0)
                foreach (TaxonomicName item in View.SelectedObjects)
                {
                    string separator = (!string.IsNullOrEmpty(param.NamesList)) ? "\r\n" : "";
                    param.NamesList =  param.NamesList + separator + ((TaxonomicName)item).FullName;
      	
                }
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;

        }
            StreamWriter log = null;
            int pageimages = 0;
        
        private void popupWindowShowAction_BhlSearch_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            
            XPObjectSpace os1 = (XPObjectSpace)Application.CreateObjectSpace();
            CollectionSource ds = new CollectionSource(os1, typeof(TaxonomicName));
            //ListView view = Application.CreateListView("NameRef_ListView", ds, false);
            BhlSearchParameters par = ((BhlSearchParameters)e.PopupWindow.View.CurrentObject);
          
                if (!string.IsNullOrEmpty(par.FolderName))
                {
                    if (!Directory.Exists(par.FolderName))
                    {
                        Directory.CreateDirectory(par.FolderName);
                    }
                }
            
          
            string path = "";
            if (!String.IsNullOrEmpty(par.FolderName))
                path =par.FolderName+"\\log.txt";
            if (!string.IsNullOrEmpty(path))
            {
                log = new StreamWriter(path, true);
                log.WriteLine("Download started " + DateTime.Now.ToString());
            }
            string[] lines = Regex.Split(par.NamesList, "\r\n");
          
            foreach (string TaxonomicName in lines)
            {

                int pageimages = 0;
            
                GetTaxonomicName(TaxonomicName, par.DownloadThumbnails, par.DownloadOcr, par.DownloadFullSizeImages,par.DownloadAllPageNames, par.FolderName);
                if (log != null)
                {
                    log.WriteLine(String.Format(TaxonomicName + ": {0} page images", pageimages.ToString()));
                }
                //BHL(TaxonomicName);
                //TaxonomyHelper.BhlXml(TaxonomicName);
                //TaxonomyHelper.BhlJson(TaxonomicName);
            }
            if (log != null)
            {
                log.WriteLine("Download finished " + DateTime.Now.ToString());
                log.Close();
                Process.Start(path);
            }
            //Frame.SetView(view);
  
        }

        private void simpleAction_DownloadPageImage_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
           /* foreach (Page item in View.SelectedObjects)
            {
            	GetPageImage(item,
            }
            GetPageImage*/
        }
    }
}
