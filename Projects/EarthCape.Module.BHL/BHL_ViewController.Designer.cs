namespace EarthCape.Module.BHL
{
    partial class BHL_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_BhlSearch =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_DownloadPageImage = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_BhlSearch.AcceptButtonCaption = null;
            this.popupWindowShowAction_BhlSearch.CancelButtonCaption = null;
            this.popupWindowShowAction_BhlSearch.Caption = "BHL";
            this.popupWindowShowAction_BhlSearch.Category = "Tools";
            this.popupWindowShowAction_BhlSearch.ConfirmationMessage = null;
            this.popupWindowShowAction_BhlSearch.Id = "popupWindowShowAction_BhlSearch";
            this.popupWindowShowAction_BhlSearch.ImageName = "bhl_icon";
            this.popupWindowShowAction_BhlSearch.ToolTip = "Looks up pages that contain selected names in BHL archive";
            this.popupWindowShowAction_BhlSearch.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_BhlSearch_CustomizePopupWindowParams);
            this.popupWindowShowAction_BhlSearch.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_BhlSearch_Execute);
            this.simpleAction_DownloadPageImage.Caption = "Get page image";
            this.simpleAction_DownloadPageImage.Category = "Tools";
            this.simpleAction_DownloadPageImage.ConfirmationMessage = null;
            this.simpleAction_DownloadPageImage.Id = "simpleAction_DownloadPageImage";
            this.simpleAction_DownloadPageImage.SelectionDependencyType =
                DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_DownloadPageImage.TargetObjectType = typeof(EarthCape.Module.Library.Page);
            this.simpleAction_DownloadPageImage.ToolTip = null;
            this.simpleAction_DownloadPageImage.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_DownloadPageImage_Execute);
            this.Actions.Add(this.popupWindowShowAction_BhlSearch);
            this.Actions.Add(this.simpleAction_DownloadPageImage);
        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_BhlSearch;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DownloadPageImage;
    }
}
