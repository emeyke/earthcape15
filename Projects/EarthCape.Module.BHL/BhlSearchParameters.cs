using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;
using EarthCape.Module.Core;

namespace EarthCape.Module.BHL
{
    [NonPersistent]
    public class BhlSearchParameters : XPCustomObject
    {
        private string _NamesList;
        [Size(SizeAttribute.Unlimited)]
        public string NamesList
        {
            get
            {
                return _NamesList;
            }
            set
            {
                SetPropertyValue("NamesList", ref _NamesList, value);
            }
        }

       
        private string _FolderName;
        public string FolderName
        {
            get
            {
                return _FolderName;
            }
            set
            {
                SetPropertyValue("FolderName", ref _FolderName, value);
            }
        }
        public BhlSearchParameters(Session session) : base(session) { }
    /*    private bool _AutoCreateTaxons=false;
        public bool AutoCreateTaxons
        {
            get
            {
                return _AutoCreateTaxons;
            }
            set
            {
                SetPropertyValue("AutoCreateTaxons", ref _AutoCreateTaxons, value);
            }
        }*/
        private bool _DownloadFullSizeImages;
        public bool DownloadFullSizeImages
        {
            get
            {
                return _DownloadFullSizeImages;
            }
            set
            {
                SetPropertyValue("DownloadFullSizeImages", ref _DownloadFullSizeImages, value);
            }
        }
        private bool _DownloadAllTitlePages;
        public bool DownloadAllTitlePages
        {
            get
            {
                return _DownloadAllTitlePages;
            }
            set
            {
                SetPropertyValue("DownloadAllTitlePages", ref _DownloadAllTitlePages, value);
            }
        }
        private bool _DownloadAllPageNames;
        public bool DownloadAllPageNames
        {
            get
            {
                return _DownloadAllPageNames;
            }
            set
            {
                SetPropertyValue("DownloadAllPageNames", ref _DownloadAllPageNames, value);
            }
        }
        private bool _DownloadOcr;
        public bool DownloadOcr
        {
            get
            {
                return _DownloadOcr;
            }
            set
            {
                SetPropertyValue("DownloadOcr", ref _DownloadOcr, value);
            }
        }

        private bool _DownloadThumbnails=false;
        public bool DownloadThumbnails
        {
            get
            {
                return _DownloadThumbnails;
            }
            set
            {
                SetPropertyValue("DownloadThumbnails", ref _DownloadThumbnails, value);
            }
        }
        private Project _AddReferencesToGroup;
        [ImmediatePostData]
        public Project AddReferencesToGroup
        {
            get
            {
                return _AddReferencesToGroup;
            }
            set
            {
                SetPropertyValue("AddReferencesToGroup", ref _AddReferencesToGroup, value);
            }
        }
    }

}
