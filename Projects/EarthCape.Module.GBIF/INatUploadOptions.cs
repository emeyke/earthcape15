using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
  
    [NonPersistent]
    public class INatUploadOptions : XPCustomObject
    {
        public INatUploadOptions(Session session) : base(session) { }

    }

}
