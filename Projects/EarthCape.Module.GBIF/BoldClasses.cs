﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.BOLD
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class bold_records
    {

        private bold_recordsRecord recordField;

        /// <remarks/>
        public bold_recordsRecord record
        {
            get
            {
                return this.recordField;
            }
            set
            {
                this.recordField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecord
    {

        private uint record_idField;

        private string processidField;

        private string bin_uriField;

        private bold_recordsRecordSpecimen_identifiers specimen_identifiersField;

        private bold_recordsRecordTaxonomy taxonomyField;

        private bold_recordsRecordSpecimen_desc specimen_descField;

        private bold_recordsRecordCollection_event collection_eventField;

        private bold_recordsRecordRead[] tracefilesField;

        private bold_recordsRecordSequence[] sequencesField;

        private object notesField;

        /// <remarks/>
        public uint record_id
        {
            get
            {
                return this.record_idField;
            }
            set
            {
                this.record_idField = value;
            }
        }

        /// <remarks/>
        public string processid
        {
            get
            {
                return this.processidField;
            }
            set
            {
                this.processidField = value;
            }
        }

        /// <remarks/>
        public string bin_uri
        {
            get
            {
                return this.bin_uriField;
            }
            set
            {
                this.bin_uriField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordSpecimen_identifiers specimen_identifiers
        {
            get
            {
                return this.specimen_identifiersField;
            }
            set
            {
                this.specimen_identifiersField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordTaxonomy taxonomy
        {
            get
            {
                return this.taxonomyField;
            }
            set
            {
                this.taxonomyField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordSpecimen_desc specimen_desc
        {
            get
            {
                return this.specimen_descField;
            }
            set
            {
                this.specimen_descField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordCollection_event collection_event
        {
            get
            {
                return this.collection_eventField;
            }
            set
            {
                this.collection_eventField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("read", IsNullable = false)]
        public bold_recordsRecordRead[] tracefiles
        {
            get
            {
                return this.tracefilesField;
            }
            set
            {
                this.tracefilesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("sequence", IsNullable = false)]
        public bold_recordsRecordSequence[] sequences
        {
            get
            {
                return this.sequencesField;
            }
            set
            {
                this.sequencesField = value;
            }
        }

        /// <remarks/>
        public object notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordSpecimen_identifiers
    {

        private string sampleidField;

        private string fieldnumField;

        private string institution_storingField;

        /// <remarks/>
        public string sampleid
        {
            get
            {
                return this.sampleidField;
            }
            set
            {
                this.sampleidField = value;
            }
        }

        /// <remarks/>
        public string fieldnum
        {
            get
            {
                return this.fieldnumField;
            }
            set
            {
                this.fieldnumField = value;
            }
        }

        /// <remarks/>
        public string institution_storing
        {
            get
            {
                return this.institution_storingField;
            }
            set
            {
                this.institution_storingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomy
    {

        private string identification_provided_byField;

        private bold_recordsRecordTaxonomyPhylum phylumField;

        private bold_recordsRecordTaxonomyClass classField;

        private bold_recordsRecordTaxonomyOrder orderField;

        /// <remarks/>
        public string identification_provided_by
        {
            get
            {
                return this.identification_provided_byField;
            }
            set
            {
                this.identification_provided_byField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordTaxonomyPhylum phylum
        {
            get
            {
                return this.phylumField;
            }
            set
            {
                this.phylumField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordTaxonomyClass @class
        {
            get
            {
                return this.classField;
            }
            set
            {
                this.classField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordTaxonomyOrder order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyPhylum
    {

        private bold_recordsRecordTaxonomyPhylumTaxon taxonField;

        /// <remarks/>
        public bold_recordsRecordTaxonomyPhylumTaxon taxon
        {
            get
            {
                return this.taxonField;
            }
            set
            {
                this.taxonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyPhylumTaxon
    {

        private byte taxIDField;

        private string nameField;

        /// <remarks/>
        public byte taxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyClass
    {

        private bold_recordsRecordTaxonomyClassTaxon taxonField;

        /// <remarks/>
        public bold_recordsRecordTaxonomyClassTaxon taxon
        {
            get
            {
                return this.taxonField;
            }
            set
            {
                this.taxonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyClassTaxon
    {

        private byte taxIDField;

        private string nameField;

        /// <remarks/>
        public byte taxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyOrder
    {

        private bold_recordsRecordTaxonomyOrderTaxon taxonField;

        /// <remarks/>
        public bold_recordsRecordTaxonomyOrderTaxon taxon
        {
            get
            {
                return this.taxonField;
            }
            set
            {
                this.taxonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordTaxonomyOrderTaxon
    {

        private byte taxIDField;

        private string nameField;

        /// <remarks/>
        public byte taxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordSpecimen_desc
    {

        private object voucher_statusField;

        private string reproductionField;

        private string sexField;

        private string lifestageField;

        /// <remarks/>
        public object voucher_status
        {
            get
            {
                return this.voucher_statusField;
            }
            set
            {
                this.voucher_statusField = value;
            }
        }

        /// <remarks/>
        public string reproduction
        {
            get
            {
                return this.reproductionField;
            }
            set
            {
                this.reproductionField = value;
            }
        }

        /// <remarks/>
        public string sex
        {
            get
            {
                return this.sexField;
            }
            set
            {
                this.sexField = value;
            }
        }

        /// <remarks/>
        public string lifestage
        {
            get
            {
                return this.lifestageField;
            }
            set
            {
                this.lifestageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordCollection_event
    {

        private string collectorsField;

        private string countryField;

        private string province_stateField;

        private bold_recordsRecordCollection_eventCoordinates coordinatesField;

        /// <remarks/>
        public string collectors
        {
            get
            {
                return this.collectorsField;
            }
            set
            {
                this.collectorsField = value;
            }
        }

        /// <remarks/>
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string province_state
        {
            get
            {
                return this.province_stateField;
            }
            set
            {
                this.province_stateField = value;
            }
        }

        /// <remarks/>
        public bold_recordsRecordCollection_eventCoordinates coordinates
        {
            get
            {
                return this.coordinatesField;
            }
            set
            {
                this.coordinatesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordCollection_eventCoordinates
    {

        private decimal latField;

        private decimal lonField;

        private object coord_sourceField;

        private object coord_accuracyField;

        /// <remarks/>
        public decimal lat
        {
            get
            {
                return this.latField;
            }
            set
            {
                this.latField = value;
            }
        }

        /// <remarks/>
        public decimal lon
        {
            get
            {
                return this.lonField;
            }
            set
            {
                this.lonField = value;
            }
        }

        /// <remarks/>
        public object coord_source
        {
            get
            {
                return this.coord_sourceField;
            }
            set
            {
                this.coord_sourceField = value;
            }
        }

        /// <remarks/>
        public object coord_accuracy
        {
            get
            {
                return this.coord_accuracyField;
            }
            set
            {
                this.coord_accuracyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordRead
    {

        private uint trace_idField;

        private System.DateTime run_dateField;

        private string sequencing_centerField;

        private string directionField;

        private string seq_primerField;

        private string trace_nameField;

        private string trace_linkField;

        private string markercodeField;

        private string[] textField;

        /// <remarks/>
        public uint trace_id
        {
            get
            {
                return this.trace_idField;
            }
            set
            {
                this.trace_idField = value;
            }
        }

        /// <remarks/>
        public System.DateTime run_date
        {
            get
            {
                return this.run_dateField;
            }
            set
            {
                this.run_dateField = value;
            }
        }

        /// <remarks/>
        public string sequencing_center
        {
            get
            {
                return this.sequencing_centerField;
            }
            set
            {
                this.sequencing_centerField = value;
            }
        }

        /// <remarks/>
        public string direction
        {
            get
            {
                return this.directionField;
            }
            set
            {
                this.directionField = value;
            }
        }

        /// <remarks/>
        public string seq_primer
        {
            get
            {
                return this.seq_primerField;
            }
            set
            {
                this.seq_primerField = value;
            }
        }

        /// <remarks/>
        public string trace_name
        {
            get
            {
                return this.trace_nameField;
            }
            set
            {
                this.trace_nameField = value;
            }
        }

        /// <remarks/>
        public string trace_link
        {
            get
            {
                return this.trace_linkField;
            }
            set
            {
                this.trace_linkField = value;
            }
        }

        /// <remarks/>
        public string markercode
        {
            get
            {
                return this.markercodeField;
            }
            set
            {
                this.markercodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string[] Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class bold_recordsRecordSequence
    {

        private uint sequenceIDField;

        private string markercodeField;

        private string nucleotidesField;

        /// <remarks/>
        public uint sequenceID
        {
            get
            {
                return this.sequenceIDField;
            }
            set
            {
                this.sequenceIDField = value;
            }
        }

        /// <remarks/>
        public string markercode
        {
            get
            {
                return this.markercodeField;
            }
            set
            {
                this.markercodeField = value;
            }
        }

        /// <remarks/>
        public string nucleotides
        {
            get
            {
                return this.nucleotidesField;
            }
            set
            {
                this.nucleotidesField = value;
            }
        }
    }


}
