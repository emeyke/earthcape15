﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace EarthCape.Module.Integrations.GBIF.EML
{
    [XmlRoot(ElementName = "title")]
    public class Title
    {
        [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string Lang { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "individualName")]
    public class IndividualName
    {
        [XmlElement(ElementName = "givenName")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "surName")]
        public string SurName { get; set; }
    }

    [XmlRoot(ElementName = "address")]
    public class Address
    {
        [XmlElement(ElementName = "deliveryPoint")]
        public string DeliveryPoint { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
        [XmlElement(ElementName = "administrativeArea")]
        public string AdministrativeArea { get; set; }
        [XmlElement(ElementName = "country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "postalCode")]
        public string PostalCode { get; set; }
    }

    [XmlRoot(ElementName = "userId")]
    public class UserId
    {
        [XmlAttribute(AttributeName = "directory")]
        public string Directory { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "creator")]
    public class Creator
    {
        [XmlElement(ElementName = "positionName")]
        public List<string> PositionName { get; set; }
        [XmlElement(ElementName = "individualName")]
        public List<IndividualName> IndividualName { get; set; }
        [XmlElement(ElementName = "organizationName")]
        public List<string> OrganizationName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "onlineUrl")]
        public string OnlineUrl { get; set; }
        [XmlElement(ElementName = "userId")]
        public List<UserId> UserId { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "metadataProvider")]
    public class MetadataProvider
    {
        [XmlElement(ElementName = "organizationName")]
        public string OrganizationName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "onlineUrl")]
        public string OnlineUrl { get; set; }
        [XmlElement(ElementName = "userId")]
        public List<UserId> UserId { get; set; }
        [XmlElement(ElementName = "individualName")]
        public IndividualName IndividualName { get; set; }
        [XmlElement(ElementName = "positionName")]
        public List<string> PositionName { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "associatedParty")]
    public class AssociatedParty
    {
        [XmlElement(ElementName = "organizationName")]
        public List<string> OrganizationName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "userId")]
        public List<UserId> UserId { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
        [XmlElement(ElementName = "individualName")]
        public List<IndividualName> IndividualName { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
    }

    [XmlRoot(ElementName = "ulink")]
    public class Ulink
    {
        [XmlElement(ElementName = "citetitle")]
        public string Citetitle { get; set; }
        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }
    }

    [XmlRoot(ElementName = "para")]
    public class Para
    {
        [XmlElement(ElementName = "ulink")]
        public Ulink Ulink { get; set; }
    }

    [XmlRoot(ElementName = "abstract")]
    public class Abstract
    {
        [XmlElement(ElementName = "para")]
        public List<Para> Para { get; set; }
    }

    [XmlRoot(ElementName = "keywordSet")]
    public class KeywordSet
    {
        [XmlElement(ElementName = "keyword")]
        public List<string> Keyword { get; set; }
        [XmlElement(ElementName = "keywordThesaurus")]
        public string KeywordThesaurus { get; set; }
    }

    [XmlRoot(ElementName = "additionalInfo")]
    public class AdditionalInfo
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "intellectualRights")]
    public class IntellectualRights
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "url")]
    public class Url
    {
        [XmlAttribute(AttributeName = "function")]
        public string Function { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "online")]
    public class Online
    {
        [XmlElement(ElementName = "url")]
        public Url Url { get; set; }
    }

    [XmlRoot(ElementName = "distribution")]
    public class Distribution
    {
        [XmlElement(ElementName = "online")]
        public Online Online { get; set; }
        [XmlAttribute(AttributeName = "scope")]
        public string Scope { get; set; }
    }

    [XmlRoot(ElementName = "boundingCoordinates")]
    public class BoundingCoordinates
    {
        [XmlElement(ElementName = "westBoundingCoordinate")]
        public string WestBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "eastBoundingCoordinate")]
        public string EastBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "northBoundingCoordinate")]
        public string NorthBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "southBoundingCoordinate")]
        public string SouthBoundingCoordinate { get; set; }
    }

    [XmlRoot(ElementName = "geographicCoverage")]
    public class GeographicCoverage
    {
        [XmlElement(ElementName = "geographicDescription")]
        public string GeographicDescription { get; set; }
        [XmlElement(ElementName = "boundingCoordinates")]
        public BoundingCoordinates BoundingCoordinates { get; set; }
    }

    [XmlRoot(ElementName = "beginDate")]
    public class BeginDate
    {
        [XmlElement(ElementName = "calendarDate")]
        public string CalendarDate { get; set; }
    }

    [XmlRoot(ElementName = "endDate")]
    public class EndDate
    {
        [XmlElement(ElementName = "calendarDate")]
        public string CalendarDate { get; set; }
    }

    [XmlRoot(ElementName = "rangeOfDates")]
    public class RangeOfDates
    {
        [XmlElement(ElementName = "beginDate")]
        public BeginDate BeginDate { get; set; }
        [XmlElement(ElementName = "endDate")]
        public EndDate EndDate { get; set; }
    }

    [XmlRoot(ElementName = "temporalCoverage")]
    public class TemporalCoverage
    {
        [XmlElement(ElementName = "rangeOfDates")]
        public RangeOfDates RangeOfDates { get; set; }
    }

    [XmlRoot(ElementName = "coverage")]
    public class Coverage
    {
        [XmlElement(ElementName = "geographicCoverage")]
        public GeographicCoverage GeographicCoverage { get; set; }
        [XmlElement(ElementName = "temporalCoverage")]
        public TemporalCoverage TemporalCoverage { get; set; }
    }

    [XmlRoot(ElementName = "purpose")]
    public class Purpose
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "maintenance")]
    public class Maintenance
    {
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "maintenanceUpdateFrequency")]
        public string MaintenanceUpdateFrequency { get; set; }
    }

    [XmlRoot(ElementName = "contact")]
    public class Contact
    {
        [XmlElement(ElementName = "organizationName")]
        public List<string> OrganizationName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "onlineUrl")]
        public string OnlineUrl { get; set; }
        [XmlElement(ElementName = "userId")]
        public List<UserId> UserId { get; set; }
        [XmlElement(ElementName = "positionName")]
        public List<string> PositionName { get; set; }
        [XmlElement(ElementName = "individualName")]
        public IndividualName IndividualName { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "methodStep")]
    public class MethodStep
    {
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "studyExtent")]
    public class StudyExtent
    {
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "samplingDescription")]
    public class SamplingDescription
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "sampling")]
    public class Sampling
    {
        [XmlElement(ElementName = "studyExtent")]
        public StudyExtent StudyExtent { get; set; }
        [XmlElement(ElementName = "samplingDescription")]
        public SamplingDescription SamplingDescription { get; set; }
    }

    [XmlRoot(ElementName = "qualityControl")]
    public class QualityControl
    {
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "methods")]
    public class Methods
    {
        [XmlElement(ElementName = "methodStep")]
        public List<MethodStep> MethodStep { get; set; }
        [XmlElement(ElementName = "sampling")]
        public List<Sampling> Sampling { get; set; }
        [XmlElement(ElementName = "qualityControl")]
        public List<QualityControl> QualityControl { get; set; }
    }

    [XmlRoot(ElementName = "personnel")]
    public class Personnel
    {
        [XmlElement(ElementName = "organizationName")]
        public List<string> OrganizationName { get; set; }
        [XmlElement(ElementName = "positionName")]
        public string PositionName { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
        [XmlElement(ElementName = "individualName")]
        public List<IndividualName> IndividualName { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "userId")]
        public List<UserId> UserId { get; set; }
        [XmlElement(ElementName = "address")]
        public string Address { get; set; }
    }

    [XmlRoot(ElementName = "funding")]
    public class Funding
    {
        [XmlElement(ElementName = "para")]
        public Para Para { get; set; }
    }

    [XmlRoot(ElementName = "descriptor")]
    public class Descriptor
    {
        [XmlElement(ElementName = "descriptorValue")]
        public string DescriptorValue { get; set; }
    }

    [XmlRoot(ElementName = "studyAreaDescription")]
    public class StudyAreaDescription
    {
        [XmlElement(ElementName = "descriptor")]
        public Descriptor Descriptor { get; set; }
    }

    [XmlRoot(ElementName = "designDescription")]
    public class DesignDescription
    {
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "project")]
    public class Project
    {
        [XmlElement(ElementName = "title")]
        public Title Title { get; set; }
        [XmlElement(ElementName = "personnel")]
        public List<Personnel> Personnel { get; set; }
        [XmlElement(ElementName = "abstract")]
        public Abstract Abstract { get; set; }
        [XmlElement(ElementName = "funding")]
        public Funding Funding { get; set; }
        [XmlElement(ElementName = "studyAreaDescription")]
        public StudyAreaDescription StudyAreaDescription { get; set; }
        [XmlElement(ElementName = "designDescription")]
        public DesignDescription DesignDescription { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "dataset")]
    public class Dataset
    {
        [XmlElement(ElementName = "alternateIdentifier")]
        public List<string> AlternateIdentifier { get; set; }
        [XmlElement(ElementName = "title")]
        public Title Title { get; set; }
        [XmlElement(ElementName = "creator")]
        public List<Creator> Creator { get; set; }
        [XmlElement(ElementName = "metadataProvider")]
        public List<MetadataProvider> MetadataProvider { get; set; }
        [XmlElement(ElementName = "associatedParty")]
        public List<AssociatedParty> AssociatedParty { get; set; }
        [XmlElement(ElementName = "pubDate")]
        public string PubDate { get; set; }
        [XmlElement(ElementName = "language")]
        public string Language { get; set; }
        [XmlElement(ElementName = "abstract")]
        public Abstract Abstract { get; set; }
        [XmlElement(ElementName = "keywordSet")]
        public List<KeywordSet> KeywordSet { get; set; }
        [XmlElement(ElementName = "additionalInfo")]
        public AdditionalInfo AdditionalInfo { get; set; }
        [XmlElement(ElementName = "intellectualRights")]
        public IntellectualRights IntellectualRights { get; set; }
        [XmlElement(ElementName = "distribution")]
        public Distribution Distribution { get; set; }
        [XmlElement(ElementName = "coverage")]
        public Coverage Coverage { get; set; }
        [XmlElement(ElementName = "purpose")]
        public Purpose Purpose { get; set; }
        [XmlElement(ElementName = "maintenance")]
        public Maintenance Maintenance { get; set; }
        [XmlElement(ElementName = "contact")]
        public List<Contact> Contact { get; set; }
        [XmlElement(ElementName = "methods")]
        public Methods Methods { get; set; }
        [XmlElement(ElementName = "project")]
        public Project Project { get; set; }
        [XmlAttribute(AttributeName = "xml", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xml { get; set; }
        [XmlAttribute(AttributeName = "nsA", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string NsA { get; set; }
        [XmlAttribute(AttributeName = "eml", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Eml { get; set; }
        [XmlAttribute(AttributeName = "dc", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Dc { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }

}
