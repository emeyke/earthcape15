﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.GBIF
{

    public class GbifOccurrenceSearch
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public bool endOfRecords { get; set; }
        public int count { get; set; }
        public GbifOccurrence[] results { get; set; }
        public object[] facets { get; set; }
    }

    public class GbifOccurrence
    {
        public long key { get; set; }
        public string datasetKey { get; set; }
        public string publishingOrgKey { get; set; }
        public object[] networkKeys { get; set; }
        public string installationKey { get; set; }
        public string publishingCountry { get; set; }
        public string protocol { get; set; }
        public DateTime lastCrawled { get; set; }
        public DateTime lastParsed { get; set; }
        public int crawlId { get; set; }
        public Extensions extensions { get; set; }
        public string basisOfRecord { get; set; }
        public int taxonKey { get; set; }
        public int kingdomKey { get; set; }
        public int phylumKey { get; set; }
        public int classKey { get; set; }
        public int orderKey { get; set; }
        public int familyKey { get; set; }
        public int genusKey { get; set; }
        public int speciesKey { get; set; }
        public int acceptedTaxonKey { get; set; }
        public string scientificName { get; set; }
        public string acceptedScientificName { get; set; }
        public string kingdom { get; set; }
        public string phylum { get; set; }
        public string order { get; set; }
        public string family { get; set; }
        public string genus { get; set; }
        public string species { get; set; }
        public string genericName { get; set; }
        public string specificEpithet { get; set; }
        public string taxonRank { get; set; }
        public string taxonomicStatus { get; set; }
        public DateTime dateIdentified { get; set; }
        public float decimalLongitude { get; set; }
        public float decimalLatitude { get; set; }
        public float coordinateUncertaintyInMeters { get; set; }
        public string stateProvince { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
        public DateTime eventDate { get; set; }
        public string[] issues { get; set; }
        public DateTime modified { get; set; }
        public DateTime lastInterpreted { get; set; }
        public string references { get; set; }
        public string license { get; set; }
        public object[] identifiers { get; set; }
        public Medium[] media { get; set; }
        public object[] facts { get; set; }
        public object[] relations { get; set; }
        public string geodeticDatum { get; set; }
        public string _class { get; set; }
        public string countryCode { get; set; }
        public string country { get; set; }
        public string rightsHolder { get; set; }
        public string identifier { get; set; }
        public string httpunknownorgnick { get; set; }
        public string verbatimEventDate { get; set; }
        public string datasetName { get; set; }
        public string verbatimLocality { get; set; }
        public string gbifID { get; set; }
        public string collectionCode { get; set; }
        public string occurrenceID { get; set; }
        public string taxonID { get; set; }
        public string catalogNumber { get; set; }
        public string recordedBy { get; set; }
        public string httpunknownorgoccurrenceDetails { get; set; }
        public string institutionCode { get; set; }
        public string rights { get; set; }
        public string eventTime { get; set; }
        public string identificationID { get; set; }
        public string occurrenceRemarks { get; set; }
    }

    public class Extensions
    {
    }

    public class Medium
    {
        public string type { get; set; }
        public string format { get; set; }
        public string identifier { get; set; }
        public string references { get; set; }
        public DateTime created { get; set; }
        public string creator { get; set; }
        public string publisher { get; set; }
        public string license { get; set; }
        public string rightsHolder { get; set; }
    }

}
