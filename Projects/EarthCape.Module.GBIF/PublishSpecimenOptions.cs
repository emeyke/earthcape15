using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.GBIF
{
   
    [NonPersistent]
    public class PublishSpecimenOptions : XPCustomObject
    {
        public PublishSpecimenOptions(Session session) : base(session) { }


        private bool _Test=true;
        public bool Test
        {
            get { return _Test; }
            set { SetPropertyValue<bool>(nameof(Test), ref _Test, value); }
        }









    }

}
