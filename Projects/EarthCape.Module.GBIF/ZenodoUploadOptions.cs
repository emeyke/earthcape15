using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
  
    [NonPersistent]
    public class ZenodoUploadOptions : XPCustomObject
    {
        public ZenodoUploadOptions(Session session) : base(session) { }

        private bool _Test=true;
        public bool Test
        {
            get { return _Test; }
            set { SetPropertyValue<bool>(nameof(Test), ref _Test, value); }
        }

   /*     private bool _Republish=false;
        public bool Republish
        {
            get { return _Republish; }
            set { SetPropertyValue<bool>(nameof(Republish), ref _Republish, value); }
        }

        private bool _UpdateOnlyMetadata=false;
        public bool UpdateOnlyMetadata
        {
            get { return _UpdateOnlyMetadata; }
            set { SetPropertyValue<bool>(nameof(UpdateOnlyMetadata), ref _UpdateOnlyMetadata, value); }
        }*/




    }

}
