﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class GbifSettings_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PopupWindowShowAction_CreateGbifInstallation = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // PopupWindowShowAction_CreateGbifInstallation
            // 
            this.PopupWindowShowAction_CreateGbifInstallation.AcceptButtonCaption = null;
            this.PopupWindowShowAction_CreateGbifInstallation.CancelButtonCaption = null;
            this.PopupWindowShowAction_CreateGbifInstallation.Caption = "Create New GBIF Installation";
            this.PopupWindowShowAction_CreateGbifInstallation.ConfirmationMessage = null;
            this.PopupWindowShowAction_CreateGbifInstallation.Id = "PopupWindowShowAction_CreateGbifInstallation";
            this.PopupWindowShowAction_CreateGbifInstallation.ToolTip = null;
            this.PopupWindowShowAction_CreateGbifInstallation.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_CreateGbifInstallation_CustomizePopupWindowParams);
            this.PopupWindowShowAction_CreateGbifInstallation.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_CreateGbifInstallation_Execute);
            // 
            // GbifSettings_ViewController
            // 
            this.Actions.Add(this.PopupWindowShowAction_CreateGbifInstallation);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction PopupWindowShowAction_CreateGbifInstallation;
    }
}
