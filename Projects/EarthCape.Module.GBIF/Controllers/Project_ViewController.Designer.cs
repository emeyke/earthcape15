﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class Project_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_GbifRegisterOrg = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_GBIFDownload_GrSciCollInstituion = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_GbifRegisterOrg
            // 
            this.popupWindowShowAction_GbifRegisterOrg.AcceptButtonCaption = null;
            this.popupWindowShowAction_GbifRegisterOrg.CancelButtonCaption = null;
            this.popupWindowShowAction_GbifRegisterOrg.Caption = "Register in GBIF";
            this.popupWindowShowAction_GbifRegisterOrg.Category = "Tools";
            this.popupWindowShowAction_GbifRegisterOrg.ConfirmationMessage = "This will register a new organization in GBIF or update an existing registration " +
    "if your permissions allow. Proceed?";
            this.popupWindowShowAction_GbifRegisterOrg.Id = "popupWindowShowAction_GbifRegisterOrg";
            this.popupWindowShowAction_GbifRegisterOrg.ToolTip = null;
            this.popupWindowShowAction_GbifRegisterOrg.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_GbifRegisterOrg_CustomizePopupWindowParams);
            this.popupWindowShowAction_GbifRegisterOrg.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_GbifRegisterOrg_Execute);
            // 
            // simpleAction_GBIFDownload_GrSciCollInstituion
            // 
            this.simpleAction_GBIFDownload_GrSciCollInstituion.Caption = "GBIFDownload_GrSciCollInstituion";
            this.simpleAction_GBIFDownload_GrSciCollInstituion.Category = "Tools";
            this.simpleAction_GBIFDownload_GrSciCollInstituion.ConfirmationMessage = null;
            this.simpleAction_GBIFDownload_GrSciCollInstituion.Id = "simpleAction_GBIFDownload_GrSciCollInstituion";
            this.simpleAction_GBIFDownload_GrSciCollInstituion.ToolTip = null;
            this.simpleAction_GBIFDownload_GrSciCollInstituion.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_GBIFDownload_GrSciCollInstituion_Execute);
            // 
            // Project_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_GbifRegisterOrg);
            this.Actions.Add(this.simpleAction_GBIFDownload_GrSciCollInstituion);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Project);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GbifRegisterOrg;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GBIFDownload_GrSciCollInstituion;
    }
}
