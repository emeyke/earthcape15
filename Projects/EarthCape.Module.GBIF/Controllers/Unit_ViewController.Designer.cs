﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class Unit_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_DownloadInat = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_UploadINaturalist = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_DownloadInat
            // 
            this.simpleAction_DownloadInat.Caption = "Download INaturalist";
            this.simpleAction_DownloadInat.Category = "Tools";
            this.simpleAction_DownloadInat.ConfirmationMessage = "Update records with INaturalist IDs?";
            this.simpleAction_DownloadInat.Id = "simpleAction_DownloadInat";
            this.simpleAction_DownloadInat.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_DownloadInat.ToolTip = null;
            this.simpleAction_DownloadInat.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_DownloadInat_Execute);
            // 
            // popupWindowShowAction_UploadINaturalist
            // 
            this.popupWindowShowAction_UploadINaturalist.AcceptButtonCaption = null;
            this.popupWindowShowAction_UploadINaturalist.CancelButtonCaption = null;
            this.popupWindowShowAction_UploadINaturalist.Caption = "Upload INaturalist";
            this.popupWindowShowAction_UploadINaturalist.Category = "Tools";
            this.popupWindowShowAction_UploadINaturalist.ConfirmationMessage = null;
            this.popupWindowShowAction_UploadINaturalist.Id = "popupWindowShowAction_UploadINaturalist";
            this.popupWindowShowAction_UploadINaturalist.ToolTip = null;
            this.popupWindowShowAction_UploadINaturalist.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_UploadINaturalist_CustomizePopupWindowParams);
            this.popupWindowShowAction_UploadINaturalist.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_UploadINaturalist_Execute);
            // 
            // Unit_ViewController
            // 
            this.Actions.Add(this.simpleAction_DownloadInat);
            this.Actions.Add(this.popupWindowShowAction_UploadINaturalist);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Unit);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DownloadInat;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_UploadINaturalist;
    }
}
