﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.GBIF.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Project_ViewController : ViewController
    {
        public Project_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_GBIFRegisterOrg_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
           }

        private void PopupWindowShowAction_GbifRegisterOrg_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            GbifRegisterOptions options = ((GbifRegisterOptions)e.PopupWindowViewCurrentObject);
            foreach (Project item in View.SelectedObjects)
            {
                PublishHelper.GbifRegisterOrg(options.Test, View.ObjectSpace, item, settings, options.Endorsed);
            }

        }

        private void PopupWindowShowAction_GbifRegisterOrg_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GbifRegisterOptions options = os.CreateObject<GbifRegisterOptions>();
                options.Test = true;
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }

        private void SimpleAction_GBIFDownload_GrSciCollInstituion_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            foreach (Project item in View.SelectedObjects)
            {
                PublishHelper.GbifDownloadGrSciCollInst(View.ObjectSpace, item, settings);
            }
            View.ObjectSpace.CommitChanges();
        }
    }
}
