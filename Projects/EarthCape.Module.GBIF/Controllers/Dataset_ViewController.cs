﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using EarthCape.Module.Core;
using RestSharp;
using RestSharp.Authenticators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.GBIF.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Dataset_ViewController : ViewController
    {
        public Dataset_ViewController()
        {
            InitializeComponent();
            TargetObjectType = typeof(Dataset);
            TargetViewNesting = Nesting.Root;
           // TargetViewType = ViewType.DetailView;
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            View.ObjectSpace.ObjectSaved += ObjectSpace_ObjectSaved;
        }
        bool IsGoodToPublish = false;

        private void ObjectSpace_ObjectSaved(object sender, ObjectManipulatingEventArgs e)
        {
            CheckPublishOk();

            popupWindowShowAction_DatasetPublish.Active.SetItemValue("GoodToPublish", IsGoodToPublish);
        }

        private void CheckPublishOk()
        {
            if (View == null) { IsGoodToPublish = false;return; }
            if (View.CurrentObject == null) { IsGoodToPublish = false; return; }
            Dataset ds = (Dataset)(View.CurrentObject);
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            IsGoodToPublish = PublishHelper.IsDatasetGoodToPublish(View.ObjectSpace,ds,settings);
        }

       

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
            if (View.ObjectTypeInfo != null)
            {
                CheckPublishOk();
                if (popupWindowShowAction_DatasetPublish.Active)
                {
                   // popupWindowShowAction_DatasetPublish.Active.SetItemValue("GoodToPublish", IsGoodToPublish);
                    popupWindowShowAction_DatasetPublish.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                }
                if (PopupWindowShowAction_GbifOccurrenceSearchByDataset.Active)
                    PopupWindowShowAction_GbifOccurrenceSearchByDataset.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                if (simpleAction_GBIF_Crawl.Active)
                    simpleAction_GBIF_Crawl.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                if (singleChoiceAction_PublishActions.Active)
                    singleChoiceAction_PublishActions.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_PublishToGBIF_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
          
         }


        private void SimpleAction_GBIF_Crawl_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            Dataset dataset = (Dataset)View.CurrentObject;
            if (dataset.GbifPublished && (!string.IsNullOrEmpty(dataset.GbifUrl)) && (!string.IsNullOrEmpty(dataset.GbifKey)))
            {
                PublishHelper.GbifCrawl(settings, dataset);
            }

        }

      

        private void PopupWindowShowAction_DatasetPublish_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
         
            PublishOptions options =(PublishOptions)e.PopupWindowViewCurrentObject;
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            IObjectSpace os = View.ObjectSpace;
            bool test =options.Test;
            IModelListView modelview = (IModelListView)Application.Model.Views["Unit_ListView_DwC"];
            IModelListView mediaview = (IModelListView)Application.Model.Views["UnitAttachment_ListView_Dwc"];
            IModelListView eventview = (IModelListView)Application.Model.Views["LocalityVisit_ListView_DwC"];

            var gbifClient = new RestClient(test ? PublishHelper.gbifSandboxApi : PublishHelper.gbifApi);
            gbifClient.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);

            foreach (Dataset dataset in View.SelectedObjects)
            {
                if (PublishHelper.IsDatasetGoodToPublish(View.ObjectSpace, dataset, settings))
                {
                    if (options.Method == PublishMethod.ZenodoGBIF)
                    {
                        bool gbif = false;
                        bool zenodo = false;
                        bool gbifendpoint = false;
                        string publishstatus = "";
                        gbif = PublishHelper.GBIFPublishDataset(gbifClient, test, dataset, os);
                        if (gbif)
                        {
                            zenodo = PublishHelper.ZenodoPublishDataset(test, dataset, os, modelview, mediaview,eventview, dataset.GbifPreferredIdentifier, out publishstatus);
                            if (zenodo)
                            {
                                gbifendpoint = PublishHelper.GbifAddDatasetEndPoint(gbifClient, os, dataset);
                                PublishHelper.GbifCrawl(settings, dataset);
                            }
                            dataset.Save();
                            os.CommitChanges();
                        }
                        /*  MessageOptions options1 = new MessageOptions();
                         options1.Duration = 3000;
                         options1.Message = string.Format((gbif ? "GBIF dataset created: ": "GBIF dataset failed to create: ")+
                             string.Format("{0} in GBIF via {1} for user {2} with response: {3}",
                             dataset.Name,
                             test ? PublishHelper.gbifSandboxApi : PublishHelper.gbifApi,
                             test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser,
                             ""));
                         options1.Type =gbif ? InformationType.Success : InformationType.Error;
                         options1.Web.Position = InformationPosition.Top;
                         options1.Win.Caption = "Publishing...";
                         options1.Win.Type = WinMessageType.Toast;
                         Application.ShowViewStrategy.ShowMessage(options1);

                     options1 = new MessageOptions();
                     options1.Duration = 3000;
                     options1.Message = string.Format((zenodo ? "Zenodo record created/updated" : "Zenodo dataset failed to create/update")+". Message: "+ publishstatus);
                     options1.Type = zenodo ? InformationType.Success : InformationType.Error;
                     options1.Web.Position = InformationPosition.Top;
                     options1.Win.Caption = "Publishing...";
                     options1.Win.Type = WinMessageType.Toast;
                     Application.ShowViewStrategy.ShowMessage(options1);*/
                    }
                    //    if (options.Zenodo && !options.GBIF)
                    //       PublishHelper.PublishToZenodo(((PublishOptions)e.PopupWindowViewCurrentObject).Test, (Dataset)View.CurrentObject, View.ObjectSpace, (IModelListView)Application.Model.Views["Unit_ListView_DwC"], (IModelListView)Application.Model.Views["UnitAttachment_ListView_Dwc"], ((Dataset)View.CurrentObject).GbifPreferredIdentifier);
                    if (options.Method == PublishMethod.GBIF)
                    {
                        PublishHelper.GBIFPublishDataset(gbifClient, test, dataset, os);
                        PublishHelper.GbifAddDatasetEndPoint(gbifClient, os, dataset);
                    }
                    if (options.Method == PublishMethod.ZenodoDataPackage)
                    {
                     }
                }
            }
        }

        private void PopupWindowShowAction_DatasetPublish_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            PublishOptions options = os.CreateObject<PublishOptions>();
              options.Test = true;
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;

        }
        private void SingleChoiceAction_GBIF_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            IObjectSpace os = View.ObjectSpace;
            foreach (Dataset ds in View.SelectedObjects)
            {
                bool test = ds.GbifUrl.Contains("gbif-uat");
                var client = new RestClient(test ? PublishHelper.gbifSandboxApi : PublishHelper.gbifApi);
                client.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);
                if (e.SelectedChoiceActionItem.Id == "UpdateEndpoint")
                {
                    PublishHelper.GbifAddDatasetEndPoint(client, os, ds);
                }
                if (e.SelectedChoiceActionItem.Id == "UpdateAuthors")
                {
                    PublishHelper.GbifUpdateAuthors(client, test, os, ds);
                    PublishHelper.ZenodoUpdateMetadata(test, ds,os,ds.GbifPreferredIdentifier);
                }
            }

           
          
        }

        private void PopupWindowShowAction_GbifOccurrenceSearchByDataset_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GbifOccurrenceSearchOptions options = os.CreateObject<GbifOccurrenceSearchOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }

        private void PopupWindowShowAction_GbifOccurrenceSearchByDataset_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            GbifOccurrenceSearchOptions options = (GbifOccurrenceSearchOptions)e.PopupWindowViewCurrentObject;
            foreach (Dataset name in View.SelectedObjects)
            {
                PublishHelper.GbifDownloadOcurrencesByDataset(View.ObjectSpace, name, options.Limit);
                //name.Save();
            }
            View.ObjectSpace.CommitChanges();

        }

        private void simpleAction_GBIFUpdateCitationText_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            foreach (Dataset dataset in View.SelectedObjects)
            {
                if (dataset.GbifPublished && (!string.IsNullOrEmpty(dataset.GbifUrl)) && (!string.IsNullOrEmpty(dataset.GbifKey)))
                {
                    PublishHelper.GbifUpdateCitationText(settings, dataset);
                }
            }
            View.ObjectSpace.CommitChanges();
        }
    }
}
