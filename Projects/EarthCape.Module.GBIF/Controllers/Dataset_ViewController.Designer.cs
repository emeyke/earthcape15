﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class Dataset_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem7 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem8 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.simpleAction_GBIF_Crawl = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_DatasetPublish = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.singleChoiceAction_PublishActions = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_GBIFUpdateCitationText = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_GBIF_Crawl
            // 
            this.simpleAction_GBIF_Crawl.Caption = "GBIF re-crawl";
            this.simpleAction_GBIF_Crawl.Category = "Tools";
            this.simpleAction_GBIF_Crawl.ConfirmationMessage = null;
            this.simpleAction_GBIF_Crawl.Id = "simpleAction_GBIF_Crawl";
            this.simpleAction_GBIF_Crawl.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_GBIF_Crawl.ToolTip = "Pings GBIF crawler to reindex the dataset";
            this.simpleAction_GBIF_Crawl.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_GBIF_Crawl_Execute);
            // 
            // popupWindowShowAction_DatasetPublish
            // 
            this.popupWindowShowAction_DatasetPublish.AcceptButtonCaption = null;
            this.popupWindowShowAction_DatasetPublish.CancelButtonCaption = null;
            this.popupWindowShowAction_DatasetPublish.Caption = "Publish";
            this.popupWindowShowAction_DatasetPublish.Category = "Tools";
            this.popupWindowShowAction_DatasetPublish.ConfirmationMessage = "This action deposits DwC-A to Zenodo and publishes GBIF dataset with Zenodo DOI. " +
    "Proceed?";
            this.popupWindowShowAction_DatasetPublish.Id = "popupWindowShowAction_DatasetPublish";
            this.popupWindowShowAction_DatasetPublish.ImageName = "RichEditBookmark";
            this.popupWindowShowAction_DatasetPublish.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_DatasetPublish.ToolTip = "Deposits DwC-A to Zenodo and publishes GBIF dataset with Zenodo DOI";
            this.popupWindowShowAction_DatasetPublish.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_DatasetPublish_CustomizePopupWindowParams);
            this.popupWindowShowAction_DatasetPublish.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_DatasetPublish_Execute);
            // 
            // singleChoiceAction_PublishActions
            // 
            this.singleChoiceAction_PublishActions.Caption = "Publish actions";
            this.singleChoiceAction_PublishActions.Category = "Tools";
            this.singleChoiceAction_PublishActions.ConfirmationMessage = null;
            this.singleChoiceAction_PublishActions.Id = "singleChoiceAction_GBIF";
            choiceActionItem7.Caption = "Update GBIF endpoint";
            choiceActionItem7.Id = "EndPoint";
            choiceActionItem7.ImageName = null;
            choiceActionItem7.Shortcut = null;
            choiceActionItem7.ToolTip = null;
            choiceActionItem8.Caption = "Update Zenodo and GBIF authors";
            choiceActionItem8.Id = "UpdateAuthors";
            choiceActionItem8.ImageName = null;
            choiceActionItem8.Shortcut = null;
            choiceActionItem8.ToolTip = null;
            this.singleChoiceAction_PublishActions.Items.Add(choiceActionItem7);
            this.singleChoiceAction_PublishActions.Items.Add(choiceActionItem8);
            this.singleChoiceAction_PublishActions.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.singleChoiceAction_PublishActions.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.singleChoiceAction_PublishActions.TargetViewId = "";
            this.singleChoiceAction_PublishActions.ToolTip = null;
            this.singleChoiceAction_PublishActions.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SingleChoiceAction_GBIF_Execute);
            // 
            // PopupWindowShowAction_GbifOccurrenceSearchByDataset
            // 
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.AcceptButtonCaption = null;
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.CancelButtonCaption = null;
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.Caption = "Download GBIF occurrences";
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.Category = "Tools";
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.ConfirmationMessage = null;
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.Id = "PopupWindowShowAction_GbifOccurrenceSearchByDataset";
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.ToolTip = null;
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_GbifOccurrenceSearchByDataset_CustomizePopupWindowParams);
            this.PopupWindowShowAction_GbifOccurrenceSearchByDataset.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_GbifOccurrenceSearchByDataset_Execute);
            // 
            // simpleAction_GBIFUpdateCitationText
            // 
            this.simpleAction_GBIFUpdateCitationText.Caption = "GBIF download citation text";
            this.simpleAction_GBIFUpdateCitationText.Category = "Tools";
            this.simpleAction_GBIFUpdateCitationText.ConfirmationMessage = "Overwrite current citation text values for selected datasets_";
            this.simpleAction_GBIFUpdateCitationText.Id = "simpleAction_GBIFUpdateCitationText";
            this.simpleAction_GBIFUpdateCitationText.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_GBIFUpdateCitationText.ToolTip = "Downloads up to date citation text from GBIF and stores in GBIF Citation Text fie" +
    "ld for selected datasets";
            this.simpleAction_GBIFUpdateCitationText.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_GBIFUpdateCitationText_Execute);
            // 
            // Dataset_ViewController
            // 
            this.Actions.Add(this.simpleAction_GBIF_Crawl);
            this.Actions.Add(this.popupWindowShowAction_DatasetPublish);
            this.Actions.Add(this.singleChoiceAction_PublishActions);
            this.Actions.Add(this.PopupWindowShowAction_GbifOccurrenceSearchByDataset);
            this.Actions.Add(this.simpleAction_GBIFUpdateCitationText);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GBIF_Crawl;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DatasetPublish;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_PublishActions;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction PopupWindowShowAction_GbifOccurrenceSearchByDataset;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GBIFUpdateCitationText;
    }
}
