﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.GBIF.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Unit_ViewController : ViewController
    {
        public Unit_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
            bool canWrite = SecuritySystem.IsGranted(
                new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write));
            if (simpleAction_DownloadInat.Active)
                simpleAction_DownloadInat.Active.SetItemValue("SecurityAllowance", canWrite);
            if (popupWindowShowAction_UploadINaturalist.Active)
                popupWindowShowAction_UploadINaturalist.Active.SetItemValue("SecurityAllowance", canWrite);
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_DownloadInat_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Unit unit in View.SelectedObjects)
            {
                PublishHelper.INatDownloadOcurrence(View.ObjectSpace, unit);
                unit.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void SimpleAction_DownloadBoldSpecimen_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
          }

        private void PopupWindowShowAction_UploadINaturalist_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            foreach (Unit unit in View.SelectedObjects)
            {
                PublishHelper.INatUploadUnit(unit,settings);
                unit.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void PopupWindowShowAction_UploadINaturalist_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            INatUploadOptions options = os.CreateObject<INatUploadOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }
    }
}
