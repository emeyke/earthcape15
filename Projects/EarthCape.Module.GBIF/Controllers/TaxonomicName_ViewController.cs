using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Security;

namespace EarthCape.Module.GBIF
{
    public partial class TaxonomicName_ViewController : ViewController
    {
        public TaxonomicName_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            ViewControlsCreated += TaxonomicName_ViewController_ViewControlsCreated;
        }

        private void TaxonomicName_ViewController_ViewControlsCreated(object sender, EventArgs e)
        {
              if (View.ObjectTypeInfo != null)
            {
                if (simpleAction_UpdateNCBI_id.Active)
                    simpleAction_UpdateNCBI_id.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                if (simpleAction_UpdateTaxonomyGBIF.Active)
                    simpleAction_UpdateTaxonomyGBIF.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                            new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }


        private void popupWindowShowAction_GBIF_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            DowloadAndMapGbifForExtentParameters param = obs.CreateObject<DowloadAndMapGbifForExtentParameters>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;
        }

       

        private void simpleAction_UpdateTaxonomyGBIF_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (TaxonomicName item in View.SelectedObjects)
            {
                try
                {
                    PublishHelper.GBIFTaxonomy(item, View.ObjectSpace);
                }
                catch
                {
                }
                finally
                { }
            }
            View.ObjectSpace.CommitChanges();
        }

        private void simpleAction_UpdateNCBI_id_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (TaxonomicName item in View.SelectedObjects)
            {
                PublishHelper.UpdateNcbi(item, View.ObjectSpace);
            }
            View.ObjectSpace.CommitChanges();
        }

        private void simpleAction_BrowseNcbi_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Process.Start("http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=" + ((TaxonomicName)View.CurrentObject).NcbiTaxonId);
        }

        private void SimpleAction_UpdateGbifStatus_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (TaxonomicName item in View.SelectedObjects)
            {
                try
                {
                    PublishHelper.GBIFStatus(item, View.ObjectSpace);
                }
                catch
                {
                }
                finally
                { }
            }
            View.ObjectSpace.CommitChanges();
        }

        private void PopupWindowShowAction_GbifOccurrenceSearchByTaxon_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            GbifOccurrenceSearchOptions options = (GbifOccurrenceSearchOptions)e.PopupWindowViewCurrentObject;
            foreach (TaxonomicName name in View.SelectedObjects)
            {
                PublishHelper.GbifDownloadOcurrencesByTaxon(View.ObjectSpace,name,options.Limit);
                //name.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void PopupWindowShowAction_GbifOccurrenceSearchByTaxon_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GbifOccurrenceSearchOptions options = os.CreateObject<GbifOccurrenceSearchOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }
    }
}
