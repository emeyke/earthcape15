﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class RecordSet_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_DownloadINat = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_DownloadINat
            // 
            this.simpleAction_DownloadINat.Caption = "Download INat data";
            this.simpleAction_DownloadINat.Category = "Tools";
            this.simpleAction_DownloadINat.ConfirmationMessage = null;
            this.simpleAction_DownloadINat.Id = "simpleAction_DownloadINat";
            this.simpleAction_DownloadINat.TargetObjectType = typeof(EarthCape.Module.Core.RecordSet);
            this.simpleAction_DownloadINat.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.simpleAction_DownloadINat.ToolTip = null;
            this.simpleAction_DownloadINat.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.simpleAction_DownloadINat.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_DownloadINat_Execute);
            // 
            // RecordSet_ViewController
            // 
            this.Actions.Add(this.simpleAction_DownloadINat);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DownloadINat;
    }
}
