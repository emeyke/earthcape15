﻿namespace EarthCape.Module.GBIF.Controllers
{
    partial class DnaExtract_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_DownloadBoldSpecimen = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_DownloadBoldSpecimen
            // 
            this.simpleAction_DownloadBoldSpecimen.Caption = "Download BOLD";
            this.simpleAction_DownloadBoldSpecimen.Category = "Tools";
            this.simpleAction_DownloadBoldSpecimen.ConfirmationMessage = "Update records with BOLD Specimen and sequence data. Proceed?";
            this.simpleAction_DownloadBoldSpecimen.Id = "simpleAction_DownloadBoldSpecimen";
            this.simpleAction_DownloadBoldSpecimen.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_DownloadBoldSpecimen.ToolTip = null;
            this.simpleAction_DownloadBoldSpecimen.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_DownloadBoldSpecimen_Execute);
            // 
            // DnaExtract_ViewController
            // 
            this.Actions.Add(this.simpleAction_DownloadBoldSpecimen);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DownloadBoldSpecimen;
    }
}
