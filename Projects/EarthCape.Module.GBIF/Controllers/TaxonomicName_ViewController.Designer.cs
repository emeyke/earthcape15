using EarthCape.Module.Core;
namespace EarthCape.Module.GBIF
{
    partial class TaxonomicName_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_UpdateTaxonomyGBIF = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_UpdateNCBI_id = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_BrowseNcbi = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_UpdateGbifStatus = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_UpdateTaxonomyGBIF
            // 
            this.simpleAction_UpdateTaxonomyGBIF.Caption = "Update GBIF taxonomy";
            this.simpleAction_UpdateTaxonomyGBIF.Category = "Tools";
            this.simpleAction_UpdateTaxonomyGBIF.ConfirmationMessage = "Run GBIF update for selected records? This make take considerable time.";
            this.simpleAction_UpdateTaxonomyGBIF.Id = "simpleAction_UpdateTaxonomyGBIF";
            this.simpleAction_UpdateTaxonomyGBIF.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateTaxonomyGBIF.ToolTip = null;
            this.simpleAction_UpdateTaxonomyGBIF.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateTaxonomyGBIF_Execute);
            // 
            // simpleAction_UpdateNCBI_id
            // 
            this.simpleAction_UpdateNCBI_id.Caption = "Update NCBI IDs";
            this.simpleAction_UpdateNCBI_id.Category = "Tools";
            this.simpleAction_UpdateNCBI_id.ConfirmationMessage = null;
            this.simpleAction_UpdateNCBI_id.Id = "simpleAction_UpdateNCBI_id";
            this.simpleAction_UpdateNCBI_id.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateNCBI_id.ToolTip = null;
            this.simpleAction_UpdateNCBI_id.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateNCBI_id_Execute);
            // 
            // simpleAction_BrowseNcbi
            // 
            this.simpleAction_BrowseNcbi.Caption = "Go to NCBI";
            this.simpleAction_BrowseNcbi.Category = "Tools";
            this.simpleAction_BrowseNcbi.ConfirmationMessage = null;
            this.simpleAction_BrowseNcbi.Id = "simpleAction_BrowseNcbi";
            this.simpleAction_BrowseNcbi.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_BrowseNcbi.ToolTip = null;
            this.simpleAction_BrowseNcbi.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_BrowseNcbi_Execute);
            // 
            // simpleAction_UpdateGbifStatus
            // 
            this.simpleAction_UpdateGbifStatus.Caption = "Update GBIF status";
            this.simpleAction_UpdateGbifStatus.Category = "Tools";
            this.simpleAction_UpdateGbifStatus.ConfirmationMessage = null;
            this.simpleAction_UpdateGbifStatus.Id = "simpleAction_UpdateGbifStatus";
            this.simpleAction_UpdateGbifStatus.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateGbifStatus.ToolTip = null;
            this.simpleAction_UpdateGbifStatus.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_UpdateGbifStatus_Execute);
            // 
            // popupWindowShowAction_GbifOccurrenceSearchByTaxon
            // 
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.AcceptButtonCaption = null;
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.CancelButtonCaption = null;
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.Caption = "Download GBIF occurrences";
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.Category = "Tools";
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.ConfirmationMessage = null;
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.Id = "popupWindowShowAction_GbifOccurrenceSearchByTaxon";
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.ToolTip = null;
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_GbifOccurrenceSearchByTaxon_CustomizePopupWindowParams);
            this.popupWindowShowAction_GbifOccurrenceSearchByTaxon.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_GbifOccurrenceSearchByTaxon_Execute);
            // 
            // TaxonomicName_ViewController
            // 
            this.Actions.Add(this.simpleAction_UpdateTaxonomyGBIF);
            this.Actions.Add(this.simpleAction_UpdateNCBI_id);
            this.Actions.Add(this.simpleAction_BrowseNcbi);
            this.Actions.Add(this.simpleAction_UpdateGbifStatus);
            this.Actions.Add(this.popupWindowShowAction_GbifOccurrenceSearchByTaxon);
            this.TargetObjectType = typeof(EarthCape.Module.Core.TaxonomicName);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateTaxonomyGBIF;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateNCBI_id;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_BrowseNcbi;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateGbifStatus;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GbifOccurrenceSearchByTaxon;
    }
}
