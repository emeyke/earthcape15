﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

namespace EarthCape.Module.GBIF.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GbifSettings_ViewController : ViewController
    {
        public GbifSettings_ViewController()
        {
            InitializeComponent();
            this.TargetObjectType = typeof(Settings);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (View.ObjectTypeInfo != null)
            {
                if (PopupWindowShowAction_CreateGbifInstallation.Active)
                    PopupWindowShowAction_CreateGbifInstallation.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_CreateGbifInstallation_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            
        }

        private void PopupWindowShowAction_CreateGbifInstallation_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            GbifInstallationOptions options = (GbifInstallationOptions)e.PopupWindow.View.CurrentObject;
            Settings settings = (Settings)View.CurrentObject;
            IRestResponse response = PublishHelper.GbifCreateInstallation(options, settings);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                string dsUUID = response.Content.Replace("\"", "");
                if (options.Test)
                settings.GbifSandboxInstallationKey = dsUUID;
                else
                    settings.GbifInstallationKey = dsUUID;
                settings.Save();
                View.ObjectSpace.CommitChanges();


            }
        }

      

        private void PopupWindowShowAction_CreateGbifInstallation_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GbifInstallationOptions options = os.CreateObject<GbifInstallationOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }
    }
}
