using System;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class GbifInstallationOptions : XPCustomObject
    {
        public GbifInstallationOptions(Session session) : base(session) { }


        private string _GbifInstallationTitle;
        public string GbifInstallationTitle
        {
            get { return _GbifInstallationTitle; }
            set { SetPropertyValue<string>(nameof(GbifInstallationTitle), ref _GbifInstallationTitle, value); }
        }

        private bool _Test=true;
        public bool Test
        {
            get { return _Test; }
            set { SetPropertyValue<bool>(nameof(Test), ref _Test, value); }
        }




    }

}
