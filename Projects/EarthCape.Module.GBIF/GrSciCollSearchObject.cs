﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.INat
{

    public class GrSciCollSearch
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public bool endOfRecords { get; set; }
        public int count { get; set; }
        public GrSciColl[] results { get; set; }
    }

    public class GrSciColl
    {
        public string key { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
        public string[] email { get; set; }
        public string[] phone { get; set; }
        public string homepage { get; set; }
        public object[] disciplines { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public Mailingaddress mailingAddress { get; set; }
        public Address address { get; set; }
        public object[] additionalNames { get; set; }
        public DateTime foundingDate { get; set; }
        public int numberSpecimens { get; set; }
        public bool indexHerbariorumRecord { get; set; }
        public string createdBy { get; set; }
        public string modifiedBy { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public object[] tags { get; set; }
        public Identifier[] identifiers { get; set; }
        public Contact[] contacts { get; set; }
        public object[] machineTags { get; set; }
    }

    public class Mailingaddress
    {
        public int key { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalCode { get; set; }
    }

    public class Address
    {
        public int key { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalCode { get; set; }
    }

    public class Identifier
    {
        public int key { get; set; }
        public string type { get; set; }
        public string identifier { get; set; }
        public string createdBy { get; set; }
        public DateTime created { get; set; }
    }

    public class Contact
    {
        public string key { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string position { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public Mailingaddress1 mailingAddress { get; set; }
        public string createdBy { get; set; }
        public string modifiedBy { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public object[] tags { get; set; }
        public object[] machineTags { get; set; }
        public Identifier1[] identifiers { get; set; }
    }

    public class Mailingaddress1
    {
        public int key { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalCode { get; set; }
    }

    public class Identifier1
    {
        public int key { get; set; }
        public string type { get; set; }
        public string identifier { get; set; }
        public string createdBy { get; set; }
        public DateTime created { get; set; }
    }

}
