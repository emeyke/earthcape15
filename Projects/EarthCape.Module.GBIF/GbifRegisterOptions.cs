using System;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class GbifRegisterOptions : XPCustomObject
    {
        public GbifRegisterOptions(Session session) : base(session) { }


        private bool _Test=true;
        public bool Test
        {
            get { return _Test; }
            set { SetPropertyValue<bool>(nameof(Test), ref _Test, value); }
        }


        private bool _Endorsed=false;
        public bool Endorsed
        {
            get { return _Endorsed; }
            set { SetPropertyValue<bool>(nameof(Endorsed), ref _Endorsed, value); }
        }





    }

}
