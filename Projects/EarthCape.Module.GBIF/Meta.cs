﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace EarthCape.Module.Integrations.GBIF.Meta
{
    [XmlRoot(ElementName = "files", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Files
    {
        [XmlElement(ElementName = "location", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public string Location { get; set; }
    }

    [XmlRoot(ElementName = "id", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Id
    {
        [XmlAttribute(AttributeName = "index")]
        public string Index { get; set; }
    }

    [XmlRoot(ElementName = "field", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Field
    {
        [XmlAttribute(AttributeName = "index")]
        public string Index { get; set; }
        [XmlAttribute(AttributeName = "term")]
        public string Term { get; set; }
        [XmlAttribute(AttributeName = "delimitedBy")]
        public string DelimitedBy { get; set; }
    }

    [XmlRoot(ElementName = "core", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Core
    {
        [XmlElement(ElementName = "files", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public Files Files { get; set; }
        [XmlElement(ElementName = "id", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public Id Id { get; set; }
        [XmlElement(ElementName = "field", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public List<Field> Field { get; set; }
        [XmlAttribute(AttributeName = "encoding")]
        public string Encoding { get; set; }
        [XmlAttribute(AttributeName = "fieldsTerminatedBy")]
        public string FieldsTerminatedBy { get; set; }
        [XmlAttribute(AttributeName = "linesTerminatedBy")]
        public string LinesTerminatedBy { get; set; }
        [XmlAttribute(AttributeName = "fieldsEnclosedBy")]
        public string FieldsEnclosedBy { get; set; }
        [XmlAttribute(AttributeName = "ignoreHeaderLines")]
        public string IgnoreHeaderLines { get; set; }
        [XmlAttribute(AttributeName = "rowType")]
        public string RowType { get; set; }
    }

    [XmlRoot(ElementName = "coreid", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Coreid
    {
        [XmlAttribute(AttributeName = "index")]
        public string Index { get; set; }
    }

    [XmlRoot(ElementName = "extension", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Extension
    {
        [XmlElement(ElementName = "files", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public Files Files { get; set; }
        [XmlElement(ElementName = "coreid", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public Coreid Coreid { get; set; }
        [XmlElement(ElementName = "field", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public List<Field> Field { get; set; }
        [XmlAttribute(AttributeName = "encoding")]
        public string Encoding { get; set; }
        [XmlAttribute(AttributeName = "fieldsTerminatedBy")]
        public string FieldsTerminatedBy { get; set; }
        [XmlAttribute(AttributeName = "linesTerminatedBy")]
        public string LinesTerminatedBy { get; set; }
        [XmlAttribute(AttributeName = "fieldsEnclosedBy")]
        public string FieldsEnclosedBy { get; set; }
        [XmlAttribute(AttributeName = "ignoreHeaderLines")]
        public string IgnoreHeaderLines { get; set; }
        [XmlAttribute(AttributeName = "rowType")]
        public string RowType { get; set; }
    }

    [XmlRoot(ElementName = "archive", Namespace = "http://rs.tdwg.org/dwc/text/")]
    public class Archive
    {
        [XmlElement(ElementName = "core", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public Core Core { get; set; }
        [XmlElement(ElementName = "extension", Namespace = "http://rs.tdwg.org/dwc/text/")]
        public List<Extension> Extension { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "metadata")]
        public string Metadata { get; set; }
    }

}
