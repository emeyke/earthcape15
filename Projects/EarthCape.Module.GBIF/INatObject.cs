﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.INat
{
    public class CommonName
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool is_valid { get; set; }
        public string lexicon { get; set; }
    }

    public class Taxon
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string rank { get; set; }
        public string ancestry { get; set; }
        public CommonName common_name { get; set; }
    }

    public class IconicTaxon
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string rank { get; set; }
        public double rank_level { get; set; }
        public string ancestry { get; set; }
    }

    public class User
    {
        public string login { get; set; }
        public string user_icon_url { get; set; }
    }

    public class Photo
    {
        public int? id { get; set; }
        public int? user_id { get; set; }
        public string native_photo_id { get; set; }
        public string square_url { get; set; }
        public string thumb_url { get; set; }
        public string small_url { get; set; }
        public string medium_url { get; set; }
        public string large_url { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string native_page_url { get; set; }
        public string native_username { get; set; }
        public string native_realname { get; set; }
        public int? license { get; set; }
        public object subtype { get; set; }
        public object native_original_image_url { get; set; }
        public string license_code { get; set; }
        public string attribution { get; set; }
        public string license_name { get; set; }
        public string license_url { get; set; }
        public string type { get; set; }
    }
    public class Comment
    {
        public int? id { get; set; }
        public int? user_id { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string body { get; set; }
        public string parent_type { get; set; }
        public int? parent_id { get; set; }
        public User user { get; set; }
      
    }

    public class Observation
    {
        public int? id { get; set; }
        public DateTime? observed_on { get; set; }
        public string description { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public object map_scale { get; set; }
        public object timeframe { get; set; }
        public string species_guess { get; set; }
        public int? user_id { get; set; }
        public int? taxon_id { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string place_guess { get; set; }
        public bool id_please { get; set; }
        public string observed_on_string { get; set; }
        public int? iconic_taxon_id { get; set; }
        public int? num_identification_agreements { get; set; }
        public int? num_identification_disagreements { get; set; }
        public DateTime? time_observed_at { get; set; }
        public string time_zone { get; set; }
        public bool location_is_exact { get; set; }
        public bool delta { get; set; }
        public int? positional_accuracy { get; set; }
        public object private_latitude { get; set; }
        public object private_longitude { get; set; }
        public object private_positional_accuracy { get; set; }
        public object geoprivacy { get; set; }
        public string quality_grade { get; set; }
        public object positioning_method { get; set; }
        public object positioning_device { get; set; }
        public bool? out_of_range { get; set; }
        public string license { get; set; }
        public string uri { get; set; }
        public int? observation_photos_count { get; set; }
        public int? comments_count { get; set; }
        public string zic_time_zone { get; set; }
        public int? oauth_application_id { get; set; }
        public int? observation_sounds_count { get; set; }
        public int? identifications_count { get; set; }
        public bool captive { get; set; }
        public int? community_taxon_id { get; set; }
        public int? site_id { get; set; }
        public object old_uuid { get; set; }
        public int? public_positional_accuracy { get; set; }
        public bool mappable { get; set; }
        public int? cached_votes_total { get; set; }
        public DateTime? last_indexed_at { get; set; }
        public object private_place_guess { get; set; }
        public string uuid { get; set; }
        public object taxon_geoprivacy { get; set; }
        public string short_description { get; set; }
        public string user_login { get; set; }
        public string iconic_taxon_name { get; set; }
        public List<object> tag_list { get; set; }
        public int? faves_count { get; set; }
        public DateTime? created_at_utc { get; set; }
        public DateTime? updated_at_utc { get; set; }
        public DateTime? time_observed_at_utc { get; set; }
        public bool? owners_identification_from_vision { get; set; }
        public Taxon taxon { get; set; }
        public IconicTaxon iconic_taxon { get; set; }
        public User user { get; set; }
        public List<Photo> photos { get; set; }
        public List<Comment> comments { get; set; }
    }
}
