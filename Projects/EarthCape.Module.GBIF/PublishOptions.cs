using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
    public enum PublishMethod
    {
        ZenodoDataPackage,
        ZenodoGBIF,
        GBIF
    }
    [NonPersistent]
    public class PublishOptions : XPCustomObject
    {
        public PublishOptions(Session session) : base(session) { }


        private PublishMethod _Method;
        public PublishMethod Method
        {
            get { return _Method; }
            set { SetPropertyValue<PublishMethod>(nameof(Method), ref _Method, value); }
        }



        private bool _Test=true;
        [ImmediatePostData(true)]
        public bool Test
        {
            get { return _Test; }
            set { SetPropertyValue<bool>(nameof(Test), ref _Test, value); }
        }

      /*  private bool _GbifUseZenodoDoi=true;
        public bool GbifUseZenodoDoi
        {
            get { return _GbifUseZenodoDoi; }
            set { SetPropertyValue<bool>(nameof(GbifUseZenodoDoi), ref _GbifUseZenodoDoi, value); }
        }

    */









    }

}
