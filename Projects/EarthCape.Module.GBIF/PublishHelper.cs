using System;
using System.Net;
using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Spreadsheet;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.DB;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using RestSharp;
using RestSharp.Authenticators;

using EarthCape.Module.Core;
using Project = EarthCape.Module.Core.Project;
using EarthCape.Module.INat;
using EarthCape.Module.BOLD;
using EarthCape.Module.Lab;
using Comment = EarthCape.Module.INat.Comment;
using EarthCape.Module.Integrations.GBIF.EML;
using Dataset = EarthCape.Module.Core.Dataset;
using EarthCape.Zenodo;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using DevExpress.Compression;


namespace EarthCape.Module.GBIF
{

    public class PublishHelper
    {

        //sandbox
        public static string gbifSandboxUrl = "http://www.gbif-uat.org/";
        public static string gbifSandboxApi = "http://api.gbif-uat.org/v1/";
        public static string zenodoSandboxApi = "https://sandbox.zenodo.org/api/";
        public static string zenodoSandboxUrl = "https://sandbox.zenodo.org/";
        public static string zenodoSandboxToken = "f4aG5Gvbd47hPbKOXH8rEAMt7CAPeiOqNR06gejqWVBL9Cckt8OOADAbSpSH";

        //prod
        public static string boldApiCombined = "http://www.boldsystems.org/index.php/API_Public/combined";
        public static string gbifApi = "http://api.gbif.org/v1/";
        public static string gbifUrl = "http://www.gbif.org/";
        public static string zenodoApi = "https://zenodo.org/api/";
        public static string zenodoUrl = "https://zenodo.org/";

        public static string inaturalistApi = "https://api.inaturalist.org/v1";

        public static void BoldSpecimenCombined(DnaExtract dna, string unitID, IObjectSpace os)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(bold_records));
            bold_records boldRecords = (bold_records)serializer.Deserialize(new XmlTextReader(String.Format("{0}?ids={1}", boldApiCombined, dna.BoldRecordId)));
            //bold_records boldRecords = (bold_records)serializer.Deserialize(new XmlTextReader(@"C:\Users\meykepro5\Dropbox\Downloads\bold_data (2).xml"));
            record rec = boldRecords.record[0];
            dna.BoldBin = rec.bin_uri;
            if (rec.taxonomy.species != null)
                if (rec.taxonomy.species != null)
                {
                    dna.Tissue.TaxonomicName = GeneralHelper.GetSpecies(os, rec.taxonomy.species.taxon.name, false);
                    dna.Tissue.Save();
                }
            if (rec.tracefiles != null)
                foreach (read item in rec.tracefiles)
                {
                    UnitAttachment att = os.FindObject<UnitAttachment>(new BinaryOperator("BoldTraceFileId", item.trace_id.ToString()));
                    if (att == null)
                        att = os.CreateObject<UnitAttachment>();
                    att.FileUrl = item.trace_link;
                    if (string.IsNullOrEmpty(att.Name))
                        att.Name = item.trace_name;
                    if (string.IsNullOrEmpty(att.ID))
                        att.ID = item.trace_id.ToString();
                    att.BoldTraceFileId = item.trace_id.ToString();
                    att.DnaExtract = dna;
                    att.Unit = dna.Tissue;
                    att.Date = item.run_date;
                    att.DcmiType = null;
                    att.Save();
                }
            if (rec.sequences != null)
                foreach (sequence item in rec.sequences)
                {
                    Sequence seq = os.FindObject<Sequence>(new BinaryOperator("BoldId", item.sequenceID.ToString()));
                    if (seq == null)
                        seq = os.CreateObject<Sequence>();
                    seq.BoldId = item.sequenceID.ToString();
                    if (string.IsNullOrEmpty(seq.Name))
                        seq.Name = item.sequenceID.ToString();
                    seq.Marker = GeneralHelper.GetMarker(item.markercode, os);
                    seq.Nucleotides = item.nucleotides;
                    seq.DnaExtract = dna;
                    seq.Unit = dna.Tissue;
                    seq.Genbank = item.genbank_accession;
                    seq.Save();
                }
            if (rec.specimen_imagery != null)
                foreach (recordMedia item in rec.specimen_imagery)
                {
                    UnitAttachment att = os.FindObject<UnitAttachment>(new BinaryOperator("BoldMediaId", item.mediaID.ToString()));
                    if (att == null)
                        att = os.CreateObject<UnitAttachment>();
                    att.JpegUrl = item.Item;
                    att.FileUrl = item.Item;
                    if (string.IsNullOrEmpty(att.Name))
                        att.Name = item.mediaID.ToString();
                    if (string.IsNullOrEmpty(att.ID))
                        att.ID = item.mediaID.ToString();
                    att.BoldMediaId = item.mediaID;
                    att.Unit = dna.Tissue;
                    att.DcmiType = DcmiType.StillImage;
                    att.Save();
                }
            dna.Save();

        }

        internal static void INatUploadUnit(Unit unit, Settings settings)
        {
            //  Observation observation = new Observation();
            string param = string.Format(@"observation[species_guess]={0}&observation[observed_on_string]={1}&observation[description]={2}&observation[place_guess]={3}&observation[latitude]={4}&observation[longitude]={5}",
            unit.TaxonomicName.FullName,
            String.Format("{0:s}", unit.Date),
            unit.Comment,
            unit.PlaceDetails,
            unit.Latitude.ToString(),
            unit.Longitude.ToString()).Replace(" ", "+");


            //    string json = JsonConvert.SerializeObject(observation);
            RestClient inatclient = new RestClient(inaturalistApi);
            //  inatclient.Authenticator = new HttpBasicAuthenticator("emeyke", "Delphius78");
            RestRequest request = new RestRequest("observations?" + param, RestSharp.Method.POST);
            request.AddHeader("Authorization", "Bearer " + settings.INaturalistToken);
            IRestResponse response = inatclient.Post(request);
            if (response.IsSuccessful)
            {
                Observation inatObservation = JsonConvert.DeserializeObject<Observation>(response.Content);
                unit.INatId = inatObservation.id;
                unit.INatUrl = "https://www.inaturalist.org/observations/" + inatObservation.id;
                //   INatUpdateUnit(objectSpace, null, inatObservation, unit, true);
            }



        }

        public static void GBIFXMLDownloadOccurrencesForTaxonomicName(TaxonomicName item)
        {
            string tname = item.Name.Replace(" ", "+");
            if (tname.IndexOf("+") < 0)
                tname = String.Format("{0}*", tname);
            int start_index = 0;
            while (start_index > -1)
            {
                using (DataSet dataSet = new DataSet())
                {

                    try
                    {
                        dataSet.ReadXml(String.Format("http://data.gbif.org/ws/rest/occurrence/list?startindex={0}&maxresults=2000&scientificname={1}&format=darwin&stylesheet=", start_index.ToString(), tname), XmlReadMode.InferSchema);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    { }
                    DataTable table = dataSet.Tables["TaxonOccurrence"];
                    if (table == null) return;
                    if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;
                    using (UnitOfWork objectSpace = new UnitOfWork(item.Session.DataLayer))
                    {

                        foreach (DataRow row in table.Rows)
                        {
                            Unit unit = new Unit(objectSpace);
                            unit.TaxonomicName = objectSpace.FindObject<TaxonomicName>(new BinaryOperator("Oid", item.Oid));
                            if (table.Columns.IndexOf("decimalLatitude") > -1)
                                if (row["decimalLatitude"] != null)
                                    if (!string.IsNullOrEmpty(row["decimalLatitude"].ToString()))
                                        if (row["decimalLatitude"].ToString() != "0")
                                            unit.Latitude = GeneralHelper.GetDouble(row["decimalLatitude"].ToString());
                            if (table.Columns.IndexOf("decimalLongitude") > -1)
                                if (row["decimalLongitude"] != null)
                                    if (!string.IsNullOrEmpty(row["decimalLongitude"].ToString()))
                                        if (row["decimalLongitude"].ToString() != "0")
                                            unit.Longitude = GeneralHelper.GetDouble(row["decimalLongitude"].ToString());
                            if (table.Columns.IndexOf("locality") > -1)
                                if (row["locality"] != null)
                                    if (!String.IsNullOrEmpty(row["locality"].ToString()))
                                    {
                                        Locality loc = objectSpace.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", row["locality"].ToString()));
                                        if (loc == null)
                                            loc = objectSpace.FindObject<Locality>(new BinaryOperator("Name", row["locality"].ToString()));
                                        if (loc == null)
                                        {
                                            loc = new Locality(objectSpace);
                                            if (row["locality"].ToString().Length > 100)
                                                loc.Name = row["locality"].ToString().Substring(0, 100);
                                            else
                                                loc.Name = row["locality"].ToString();
                                            loc.Save();
                                        }
                                        unit.Locality = loc;
                                    }
                            /* if (table.Columns.IndexOf("gbifKey") > -1)
                                 unit.gbifKey = row["gbifKey"].ToString();
                             if (table.Columns.IndexOf("gbifNotes") > -1)
                                 unit.gbifNotes = row["gbifNotes"].ToString();*/
                            if (table.Columns.IndexOf("About") > -1)
                                unit.Comment = row["About"].ToString();
                            if (table.Columns.IndexOf("basisOfRecordString") > -1)
                                unit.UnitType = row["basisOfRecordString"].ToString();
                            if (table.Columns.IndexOf("catalogNumber") > -1)
                                if (row["catalogNumber"].ToString().Length > 255)
                                    unit.UnitID = row["catalogNumber"].ToString().Substring(0, 255);
                                else
                                    unit.UnitID = row["catalogNumber"].ToString();
                            if (table.Columns.IndexOf("collector") > -1)
                                if (row["collector"].ToString().Length > 255)
                                    unit.CollectedOrObservedByText = row["collector"].ToString().Substring(0, 255);
                                else
                                    unit.CollectedOrObservedByText = row["collector"].ToString();
                            if (table.Columns.IndexOf("institutionCode") > -1)
                                if (row["institutionCode"].ToString().Length > 255)
                                    unit.InstitutionCode = row["institutionCode"].ToString().Substring(0, 255);
                                else
                                    unit.InstitutionCode = row["institutionCode"].ToString();
                            if (table.Columns.IndexOf("collectionCode") > -1)
                                if (row["collectionCode"].ToString().Length > 255)
                                    unit.CollectionCode = row["collectionCode"].ToString().Substring(0, 255);
                                else
                                    unit.CollectionCode = row["collectionCode"].ToString();
                            objectSpace.CommitChanges();
                        }

                        // objectSpace.CommitChanges();
                    }
                }
            }
        }

        internal static bool GbifUpdateCitationText(Settings settings, Dataset dataset)
        {
            if (dataset == null) return false;
            if (dataset.GbifKey == null) return false;
            RestClient gbifClient = new RestClient(gbifApi);
            RestRequest request = new RestRequest(String.Format("dataset/{0}", dataset.GbifKey), RestSharp.Method.GET);
            IRestResponse response = gbifClient.Execute(request);
            if (response.IsSuccessful)
            {
                dataset.GbifCitationText = (string)JObject.Parse(response.Content)["citation"]["text"];
                dataset.Save();
            }
            return false;
        }

        internal static void GbifDownloadGrSciCollInst(IObjectSpace objectSpace, Project item, Settings settings)
        {
            RestClient inatclient = new RestClient(gbifApi);
            RestRequest request = new RestRequest("grscicoll/institution/" + item.GRSciCollInstitution, RestSharp.Method.GET);
            IRestResponse response = inatclient.Execute(request);
            if (response.IsSuccessful)
            {
                item.Name = (string)JObject.Parse(response.Content)["name"];
                item.Code = (string)JObject.Parse(response.Content)["code"];
                item.Type = (string)JObject.Parse(response.Content)["type"];
                item.ExternalUrl = (string)JObject.Parse(response.Content)["homepage"];
                item.Address = (string)JObject.Parse(response.Content)["mailingAddress"]["address"];
                item.City = (string)JObject.Parse(response.Content)["mailingAddress"]["city"];
                item.Province = (string)JObject.Parse(response.Content)["mailingAddress"]["province"];
                item.Description = (string)JObject.Parse(response.Content)["description"];
                item.UnitsCount = ((int)JObject.Parse(response.Content)["numberSpecimens"]);
                if (JObject.Parse(response.Content)["latitude"] != null)
                    item.Latitude = (double)JObject.Parse(response.Content)["latitude"];
                if (JObject.Parse(response.Content)["longitude"] != null)
                    item.Longitude = (double)JObject.Parse(response.Content)["longitude"];
                item.Save();
            }
        }

        internal static void INatDownloadProjectToRecordSet(IObjectSpace objectSpace, RecordSet recordSet)
        {
            if (recordSet == null) return;
            if (String.IsNullOrEmpty(recordSet.INatProject)) return;
            using (WebClient wc = new WebClient())
            {
                int page = 1;
                bool cont = true;
                while (cont)
                {
                    var json = wc.DownloadString(string.Format("https://www.inaturalist.org/observations/project/{0}.json?page={1}", recordSet.INatProject, page.ToString()));
                    cont = ((!json.ToString().Contains("Server Error")) && (!json.Equals("[]")));
                    if (cont)
                    {
                        page++;
                        var list = JsonConvert.DeserializeObject<List<Observation>>(json);
                        foreach (Observation inatObservation in list)
                        {
                            if (inatObservation.id != null)
                            {
                                Unit unit = objectSpace.FindObject<Unit>(CriteriaOperator.Or(new BinaryOperator("INatId", inatObservation.id), new BinaryOperator("UnitID", inatObservation.id.ToString())));
                                if (unit == null)
                                {
                                    unit = objectSpace.CreateObject<Unit>();
                                }
                                //    if (INatUpdateUnit(objectSpace, recordSet, inatObservation, unit, true))
                                unit.Save();
                            }
                        }
                    }
                    objectSpace.CommitChanges();
                }
            }
        }

        private static bool INatUpdateUnit(IObjectSpace objectSpace, RecordSet recordSet, Observation item, Unit unit, bool photos)
        {
            if (item.id == null) return false;
            if (unit == null)
            {
                unit = objectSpace.CreateObject<Unit>();
            }
            if (String.IsNullOrEmpty(unit.UnitID))
                unit.UnitID = item.id.ToString();
            unit.INatId = item.id;
            unit.INatUrl = "https://www.inaturalist.org/observations/" + item.id;
            unit.EPSG = 4329;
            unit.Latitude = Convert.ToDouble(item.latitude);
            unit.Longitude = Convert.ToDouble(item.longitude);
            if (item.observed_on != null)
                unit.Date = item.observed_on;
            if (String.IsNullOrEmpty(unit.TemporaryName))
                unit.TemporaryName = item.species_guess;
            if (unit.TaxonomicName == null)
            {
                unit.TaxonomicName = objectSpace.FindObject<TaxonomicName>(new BinaryOperator("FullName", item.iconic_taxon_name));
            }
            if (recordSet != null)
                unit.RecordSets.Add(recordSet);
            if (String.IsNullOrEmpty(unit.PlaceDetails))
                unit.PlaceDetails = item.place_guess;
            if (String.IsNullOrEmpty(unit.Comment))
                unit.Comment = item.description;
            if (item.comments_count > 0)
            {
                INatDownloadUnitComments(objectSpace, item, unit);
            }
            if (photos)
                INatDownloadUnitPhotos(objectSpace, item, unit);
            return true;
        }

        private static void INatDownloadUnitComments(IObjectSpace objectSpace, Observation item, Unit unit)
        {
            if (item.comments == null) return;
            foreach (Comment comment in item.comments)
            {
                UnitNote c = objectSpace.FindObject<UnitNote>(new BinaryOperator("INatId", comment.id));
                if (c == null)
                {
                    c = objectSpace.CreateObject<UnitNote>();
                }
                c.INatId = comment.id;
                c.Text = comment.body;
                if (comment.created_at != null)
                    c.RecordedOn = comment.created_at.Value;
                if (comment.user != null)
                    c.INatUserLogin = comment.user.login;
                c.INatUserId = comment.user_id;
                c.Unit = unit;
                c.Save();
            }
        }

        public static void INatDownloadUnitPhotos(IObjectSpace objectSpace, Observation item, Unit unit)
        {
            if (item.photos == null) return;
            foreach (Photo photo in item.photos)
            {
                UnitAttachment ph = objectSpace.FindObject<UnitAttachment>(CriteriaOperator.Or(new BinaryOperator("INatId", photo.id), new BinaryOperator("Name", photo.id.ToString())));
                if (ph == null)
                {
                    ph = objectSpace.CreateObject<UnitAttachment>();
                    ph.INatId = photo.id;
                    ph.Name = photo.id.ToString();
                }
                ph.JpegUrl = String.Format("https://static.inaturalist.org/photos/{0}/original.jpg", photo.id);
                ph.Unit = unit;
                ph.Save();
            }
        }

        internal static bool INatDownloadOcurrence(IObjectSpace objectSpace, Unit unit)
        {
            if (unit == null) return false;
            if (unit.INatId == null) return false;
            RestClient inatclient = new RestClient(inaturalistApi);
            RestRequest request = new RestRequest("observations/" + unit.INatId, RestSharp.Method.GET);
            IRestResponse response = inatclient.Execute(request);
            if (response.IsSuccessful)
            {
                var resultObjects = AllChildren(JObject.Parse(response.Content))
                     .First(c => c.Type == JTokenType.Array && c.Path.Contains("results"))
                     .Children<JObject>();
                JObject result = resultObjects.First();
                Observation inatObservation = JsonConvert.DeserializeObject<Observation>(result.ToString());
                INatUpdateUnit(objectSpace, null, inatObservation, unit, true);
                return true;
            }
            return false;
        }
        internal static bool GbifDownloadOcurrencesByTaxon(IObjectSpace objectSpace, TaxonomicName taxonomicName, int limit)
        {
            if (taxonomicName == null) return false;
            if (taxonomicName.GbifKey == null) return false;
            RestClient gbifClient = new RestClient(gbifApi);
            RestRequest request = new RestRequest(String.Format("occurrence/search?taxonKey={0}&limit={1}", taxonomicName.GbifKey, limit), RestSharp.Method.GET);
            IRestResponse response = gbifClient.Execute(request);
            if (response.IsSuccessful)
            {
                JObject result = JObject.Parse(response.Content);
                GbifOccurrenceSearch inatObservation = JsonConvert.DeserializeObject<GbifOccurrenceSearch>(result.ToString());
                foreach (GbifOccurrence item in inatObservation.results)
                {
                    GbifStoreOccurrence(objectSpace, item, null, taxonomicName);
                }
                return true;
            }
            return false;
        }

        internal static long GbifGetRecByOccurrenceID(string id)
        {
            RestClient gbifClient = new RestClient(gbifApi);
            RestRequest request = new RestRequest(String.Format("occurrence/search?occurrenceID={0}", id), RestSharp.Method.GET);
            IRestResponse response = gbifClient.Execute(request);
            if (response.IsSuccessful)
            {
                JObject result = JObject.Parse(response.Content);
                GbifOccurrenceSearch inatObservation = JsonConvert.DeserializeObject<GbifOccurrenceSearch>(result.ToString());
                foreach (GbifOccurrence item in inatObservation.results)
                {
                    return item.key;
                }
            }
            return 0;
        }

        internal static bool GbifDownloadOcurrencesByDataset(IObjectSpace objectSpace, Dataset dataset, int limit)
        {
            if (dataset == null) return false;
            if (dataset.GbifKey == null) return false;
            RestClient gbifClient = new RestClient(gbifApi);
            RestRequest request = new RestRequest(String.Format("occurrence/search?dataset_key={0}&limit={1}", dataset.GbifKey, limit), RestSharp.Method.GET);
            IRestResponse response = gbifClient.Execute(request);
            if (response.IsSuccessful)
            {
                JObject result = JObject.Parse(response.Content);
                GbifOccurrenceSearch gbifOccurrence = JsonConvert.DeserializeObject<GbifOccurrenceSearch>(result.ToString());
                foreach (GbifOccurrence item in gbifOccurrence.results)
                {
                    GbifStoreOccurrence(objectSpace, item, dataset, null);
                }
                return true;
            }
            return false;
        }


        private static void GbifStoreOccurrence(IObjectSpace objectSpace, GbifOccurrence item, Dataset dataset, TaxonomicName taxonomicName)
        {
            Unit unit = objectSpace.FindObject<Unit>(
                CriteriaOperator.Or(
                    new BinaryOperator("GbifKey", item.key),
                    new BinaryOperator("GbifOccurrenceID", item.occurrenceID)
                    ));
            if (unit == null)
            {
                unit = objectSpace.CreateObject<Unit>();
                unit.UnitID = item.key.ToString();
                unit.GbifKey = item.key;
                unit.GbifOccurrenceID = item.occurrenceID;
            }
            if (taxonomicName != null)
                unit.TaxonomicName = taxonomicName;
            else
            if (item.taxonKey > 0)
            {
                unit.TaxonomicName = GBIFTaxonomyDownloadByKey(item.taxonKey.ToString(), null, objectSpace);
            }
            if (dataset != null)
                unit.Dataset = dataset;
            unit.PlaceDetails = item.verbatimLocality;
            unit.Date = item.eventDate;
            unit.Country = item.country;
            //todo  unit.BasisOfRecord =(BasisOfRecord)System.Enum.Parse(typeof(BasisOfRecord), item.basisOfRecord);
            unit.CollectionCode = item.collectionCode;
            unit.Latitude = item.decimalLatitude;
            unit.Longitude = item.decimalLongitude;
            //     unit.EPSG = item.geodeticDatum;
            unit.Save();
        }

        private static bool ContributionsAffiliationsExist(XPCollection<DatasetContribution> contributions)
        {
            foreach (DatasetContribution dsc in contributions)
            {
                if ((dsc.Person == null) || (dsc.Person.Affiliation == null)) return false;
            }
            return true;
        }
        internal static bool IsDatasetGoodToPublish(IObjectSpace objectSpace, Dataset ds, Settings settings)
        {
            if (ds == null) return false;
            return (ds.PublishedDescription != null) &&
                         (ds.PublishedDescription.Length > 3) &&
                          (ds.License != null) &&
                          (ds.Project != null) &&
                          (ds.Units.Count > 0) &&
                          (ds.Contributions.Count > 0) &&
                          (ContributionsAffiliationsExist(ds.Contributions));
        }

        public static bool GBIFPublishDataset(RestClient client, bool test, Dataset dataset, IObjectSpace objectspace)
        {
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            string pubId;
            if (!string.IsNullOrEmpty(dataset.Project.GbifPublisherKey))
                pubId = dataset.Project.GbifPublisherKey;
            else
            if (!string.IsNullOrEmpty(test ? settings.GbifSandboxPublisherKey : settings.GbifPublisherKey))
                pubId = test ? settings.GbifSandboxPublisherKey : settings.GbifPublisherKey;
            else
            {
                return false;
            }


            if (String.IsNullOrEmpty(dataset.GbifKey))
            {
                string hp = settings.Host;// string.Format("{0}/#ViewID=Dataset_DetailView&ObjectKey={1}&ObjectClassName=EarthCape.Module.Core.Dataset&mode=View", settings.Host, dataset.Oid);
                JObject gbifMetadata = GBIFGetMetadataJSON(test, dataset, settings, pubId, hp);
                dataset.GbifRequestMetadata = JToken.Parse(gbifMetadata.ToString()).ToString(Newtonsoft.Json.Formatting.Indented);
                var request = new RestRequest("/dataset", RestSharp.Method.POST, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(gbifMetadata.ToString());
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.Created)
                {
                    dataset.GbifResponseCreate = JToken.Parse(response.Content).ToString(Newtonsoft.Json.Formatting.Indented);

                    string dsUUID = response.Content.Replace("\"", "");
                    dataset.GbifKey = dsUUID;
                    dataset.GbifUrl = (test ? "https://www.gbif-uat.org/dataset/" : "https://www.gbif.org/dataset/") + dsUUID;

                    IRestResponse newDatsetResponse = client.Execute(new RestRequest("/dataset/" + dsUUID, RestSharp.Method.GET));
                    dataset.GbifPreferredIdentifier = (string)JObject.Parse(newDatsetResponse.Content)["doi"];
                    dataset.GbifPublished = true;
                    dataset.GbifCitation = "https://doi.org/" + dataset.GbifPreferredIdentifier;
                    dataset.Save();
                    objectspace.CommitChanges();
                    GbifUpdateAuthors(client, test, objectspace, dataset);
                    return true;
                }
                else
                {
                    throw new UserFriendlyException("GBIF responded: " + response.StatusCode.ToString());
                }
            }
            else
            {
                return true;
            }

        }
        public static void GbifRegisterOrg(bool test, IObjectSpace objectSpace, Core.Project project, Settings settings, bool endorsed)
        {


            var client = new RestClient(test ? gbifSandboxApi : gbifApi);
            client.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);


            string endorsingNode = (test) ? settings.GbifSandboxEndorsingNode : settings.GbifEndorsingNode;

            JObject orgJson = new JObject();

            orgJson.Add("endorsingNodeKey", endorsingNode);
            orgJson.Add("title", project.Caption);
            orgJson.Add("description", project.Description);
            orgJson.Add("address", new JArray(project.Address));
            orgJson.Add("city", project.City);
            orgJson.Add("country", project.ProjectCountry.CountryCode);
            orgJson.Add("latitude", project.Latitude);
            orgJson.Add("longitude", project.Longitude);
            orgJson.Add("homepage", new JArray(project.ExternalUrl));
            orgJson.Add("province", project.Province);
            orgJson.Add("postalCode", project.PostalCode);
            orgJson.Add("logoUrl", project.LogoUrl);
            orgJson.Add("email", new JArray(project.ContactEmail));
            orgJson.Add("language", "eng");
            orgJson.Add("endorsementApproved", endorsed);



            if (String.IsNullOrEmpty(project.GbifPublisherKey))
            {
                var request = new RestRequest("/organization", RestSharp.Method.POST, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(orgJson.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.Created)
                {

                    string dsUUID = response.Content.Replace("\"", "");
                    project.GbifPublisherKey = dsUUID;
                    project.GbifUrl = (test ? gbifSandboxUrl + "publisher/" : gbifUrl + "publisher/") + dsUUID;

                    project.Save();
                    objectSpace.CommitChanges();
                }
                foreach (ProjectContact contact in project.Contacts)
                {
                    //      if ((contact.Type==ProjectContactType.ADMINISTRATIVE_POINT_OF_CONTACT) || (contact.Type == ProjectContactType.TECHNICAL_POINT_OF_CONTACT))
                    GbifCreateOrgContact(test, objectSpace, contact, contact.Person == contact.Project.PrimaryContact);
                }
            }
            else
            {
                orgJson.Add("key", project.GbifPublisherKey);
                orgJson.Add("created", project.CreatedOn.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                orgJson.Add("modified", project.LastModifiedOn.ToString("yyyy-MM-dd'T'HH:mm:ss"));

                var request = new RestRequest("/organization/" + project.GbifPublisherKey, RestSharp.Method.PUT, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(orgJson.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    foreach (ProjectContact contact in project.Contacts)
                    {
                        GbifCreateOrgContact(test, objectSpace, contact, contact.Person == contact.Project.PrimaryContact);
                    }
                }

            }
        }

        internal static void GbifUpdateAuthors(RestClient client, bool test, IObjectSpace objectspace, Dataset dataset)
        {
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            foreach (DatasetContribution item in dataset.Contributions)
            {
                GbifCreateDatasetContact(test, client, objectspace, item);
            }
        }

        private static IEnumerable<JToken> AllChildren(JToken json)
        {
            foreach (var c in json.Children())
            {
                yield return c;
                foreach (var cc in AllChildren(c))
                {
                    yield return cc;
                }
            }
        }

        public static TaxonomicName GBIFTaxonomyDownloadByKey(string GbifKey, TaxonomicName syn, IObjectSpace os)
        {

            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://api.gbif.org/v1/species/" + GbifKey);
                JObject o = JObject.Parse(json);

                string status = (string)o["taxonomicStatus"];
                string species = (string)o["species"];
                string rank = (string)o["rank"];
                string genus = (string)o["genus"];
                string family = (string)o["family"];
                string order = (string)o["order"];
                string _class = (string)o["class"];
                string phylum = (string)o["phylum"];
                string kingdom = (string)o["kingdom"];

                string speciesKey = (string)o["speciesKey"];
                string genusKey = (string)o["genusKey"];
                string familyKey = (string)o["familyKey"];
                string orderKey = (string)o["orderKey"];
                string classKey = (string)o["classKey"];
                string phylumKey = (string)o["phylumKey"];
                string kingdomKey = (string)o["kingdomKey"];

                Species sp = null;
                // Genus g = null;
                // Family fam = null;
                if (syn != null)
                    if (syn.FullName.Equals(species))
                    {
                        syn.GbifStatus = status;
                        return null;
                    }
                try
                {
                    if ((rank == "SPECIES"))
                    {
                        sp = os.CreateObject<Species>();
                        sp.Name = species;
                        sp.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Species");
                        sp.GbifKey = speciesKey;
                        sp.GbifStatus = status;

                        sp.Save();
                        os.CommitChanges();
                        GBIFTaxonomy(sp, os);
                        //     os.CommitChanges();
                        return sp;
                    }

                }
                catch { }
                finally
                {
                }

            }
            return null;
        }
        /*   public static void CreateEML(Core.Dataset ds, string gbifdoi, string zendoi, IObjectSpace os, string host)
           {
               Eml eml = new Eml();
               eml.Dataset = new Integrations.GBIF.EML.Dataset();
               eml.Dataset.AlternateIdentifier = new List<String>();
               if (!string.IsNullOrEmpty(gbifdoi))
                   eml.Dataset.AlternateIdentifier.Add(gbifdoi);
               if (!string.IsNullOrEmpty(zendoi))
                   eml.Dataset.AlternateIdentifier.Add(zendoi);
               if (!string.IsNullOrEmpty(host))
                   eml.Dataset.AlternateIdentifier.Add(String.Format(@"{0}#ViewID=Dataset_DetailView&ObjectKey={1}&ObjectClassName=EarthCape.Module.Core.Dataset&mode=View", host, ds.Oid.ToString()));
               eml.Dataset.AlternateIdentifier.Add(ds.Oid.ToString());
               eml.Dataset.Title = ds.Name;
               eml.Dataset.Language = "eng";
               eml.Dataset.Abstract=new Abstract();
               eml.Dataset.Abstract.Para = ds.PublishedDescription;
               if (!String.IsNullOrEmpty(ds.GbifProjectCode))
               {
                   eml.Dataset.Project = new Integrations.GBIF.EML.Project();
                   eml.Dataset.Project.Id = ds.GbifProjectCode;
               }
               if (ds.License != null)
               {
                   eml.Dataset.IntellectualRights = new IntellectualRights();
                   eml.Dataset.IntellectualRights.Para = new Para();
                   eml.Dataset.IntellectualRights.Para.Text =String.Format("This work is licensed under a {0}.", ds.License.Name);
                   eml.Dataset.IntellectualRights.Para.Ulink = new Ulink();
                   eml.Dataset.IntellectualRights.Para.Ulink.Url = ds.License.Url;
                   eml.Dataset.IntellectualRights.Para.Ulink.Citetitle = ds.License.Name;
               }
               GBIFGetDatasetContactsList1(ds, eml.Dataset);
               XmlSerializer xmlSerialize = new XmlSerializer(typeof(Eml));
               using (StreamWriter writer = new StreamWriter(Path.GetTempPath() + @"\eml.xml"))
               {
                   xmlSerialize.Serialize(writer, eml);
               }

               /* 

                  List<string> list = new List<string>();

                  list.Add(@"<?xml version=""1.0"" encoding=""UTF-8""?>");
                  list.Add(String.Format(@"<eml:eml xmlns:eml=""eml://ecoinformatics.org/eml-2.1.1"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""eml://ecoinformatics.org/eml-2.1.1 http://rs.gbif.org/schema/eml-gbif-profile/1.1/eml.xsd"" packageId=""{0}"" system=""http://gbif.org"" scope=""system"" xml:lang=""en"">", ds.GbifKey));

                  list.Add(@"  <dataset>");
                  // list.Add(String.Format(@"    <alternateIdentifier xml:lang=""eng"">{0}</alternateIdentifier>", ds.GbifAlternativeIdentifier));
                  if (!string.IsNullOrEmpty(gbifdoi))
                      list.Add(String.Format(@"    <alternateIdentifier>https://doi.org/{0}</alternateIdentifier>", gbifdoi));
                  if (!string.IsNullOrEmpty(zendoi))
                      list.Add(String.Format(@"    <alternateIdentifier>https://doi.org/{0}</alternateIdentifier>", zendoi));
                  if (!string.IsNullOrEmpty(host))
                      list.Add(String.Format(@"    <alternateIdentifier>{0}#ViewID=Dataset_DetailView&ObjectKey={1}&ObjectClassName=EarthCape.Module.Core.Dataset&mode=View</alternateIdentifier>", host, ds.Oid.ToString()));
                  list.Add(String.Format(@"    <alternateIdentifier>{0}</alternateIdentifier>", ds.Oid));
                  list.Add(String.Format(@"    <title xml:lang=""eng"">{0}</title>", ds.Name));
                  if (!String.IsNullOrEmpty(ds.GbifProjectCode))
                      list.Add(String.Format(@"    <project id=""{0}""></project>", ds.GbifProjectCode));
               //todo   list.AddRange(GBIFGetDatasetContactsList(ds, os));
                  list.Add(@"    <abstract>");
                  list.Add(String.Format(@"      <para>{0}</para>", ds.PublishedDescription));
                  list.Add(@"    </abstract>");
                  if (ds.License != null)
                  {
                      list.Add(@"    <intellectualRights>");
                      list.Add(String.Format(@"      <para>This work is licensed under a <ulink url = ""{0}"" ><citetitle>{1}</citetitle></ulink>.</para>", ds.License.Url, ds.License.Name));
                      list.Add(@"    </intellectualRights>");
                  }
                  list.Add(@"  </dataset>");


                  list.Add(@"</eml:eml>");
                  TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\eml.xml");

                  foreach (String s in list)
                      tw.WriteLine(s);

                  tw.Close();*/


        // }*/

        public static void CreateEML(Core.Dataset ds, string gbifdoi, string zendoi, IObjectSpace os, string host)
        {



            List<string> list = new List<string>();

            list.Add(@"<?xml version=""1.0"" encoding=""UTF-8""?>");
            list.Add(String.Format(@"<eml:eml xmlns:eml=""eml://ecoinformatics.org/eml-2.1.1"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""eml://ecoinformatics.org/eml-2.1.1 http://rs.gbif.org/schema/eml-gbif-profile/1.1/eml.xsd"" packageId=""{0}"" system=""http://gbif.org"" scope=""system"" xml:lang=""en"">", ds.GbifKey));

            list.Add(@"  <dataset>");
            // list.Add(String.Format(@"    <alternateIdentifier xml:lang=""eng"">{0}</alternateIdentifier>", ds.GbifAlternativeIdentifier));
            if (!string.IsNullOrEmpty(gbifdoi))
                list.Add(String.Format(@"    <alternateIdentifier>https://doi.org/{0}</alternateIdentifier>", gbifdoi));
            if (!string.IsNullOrEmpty(zendoi))
                list.Add(String.Format(@"    <alternateIdentifier>https://doi.org/{0}</alternateIdentifier>", zendoi));
            if (!string.IsNullOrEmpty(host))
                list.Add(String.Format(@"    <alternateIdentifier>{0}#ViewID=Dataset_DetailView&ObjectKey={1}&ObjectClassName=EarthCape.Module.Core.Dataset&mode=View</alternateIdentifier>", host, ds.Oid.ToString()));
            list.Add(String.Format(@"    <alternateIdentifier>{0}</alternateIdentifier>", ds.Oid));
            list.Add(String.Format(@"    <title xml:lang=""eng"">{0}</title>", ds.Name));
            if (!String.IsNullOrEmpty(ds.GbifProjectCode))
                list.Add(String.Format(@"    <project id=""{0}""></project>", ds.GbifProjectCode));
            //todo   list.AddRange(GBIFGetDatasetContactsList(ds, os));
            list.Add(@"    <abstract>");
            list.Add(String.Format(@"      <para>{0}</para>", ds.PublishedDescription));
            list.Add(@"    </abstract>");
            if (ds.License != null)
            {
                list.Add(@"    <intellectualRights>");
                list.Add(String.Format(@"      <para>This work is licensed under a <ulink url = ""{0}"" ><citetitle>{1}</citetitle></ulink>.</para>", ds.License.Url, ds.License.Name));
                list.Add(@"    </intellectualRights>");
            }
            list.Add(@"  </dataset>");


            list.Add(@"</eml:eml>");
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\eml.xml");

            foreach (String s in list)
                tw.WriteLine(s);

            tw.Close();


        }
        public static string CreateDeletions(Dataset ds, IObjectSpace os)
        {

            List<string> list = new List<string>();

            Session session = ((XPObjectSpace)os).Session;
            SelectedData data = session.ExecuteQuery(
                    "SELECT Replace(Replace(TargetKey, '[Guid]', ''), '''', ''), Replace(Replace(TargetKey, '[Guid]', ''), '''', '') from AuditDataItemPersistent " +
                    "join AuditedObjectWeakReference a " +
                    "on a.Oid = AuditedObject " +
                    "join XPWeakReference w on w.Oid = OldObject " +
                    "where a.guidID = '" + ds.Oid.ToString() + "' " +
                    "and OperationType = 'RemovedFromCollection' and PropertyName = 'Units' " +
                    "order by ModifiedOn desc");

            foreach (SelectStatementResultRow item in data.ResultSet[0].Rows)
            {
                list.Add(item.Values[0].ToString());
            }
            string path = String.Format("{0}\\dataset-{1}-unit-deletions.txt", Path.GetTempPath(), ds.Oid.ToString());
            TextWriter tw = new StreamWriter(path);

            foreach (String s in list)
                tw.WriteLine(s);

            tw.Close();
            return path;

        }


        private static List<string> GBIFGetDatasetContactsList(Dataset ds)
        {


            List<string> list = new List<string>();


            ds.Contributions.Sorting = new SortingCollection(ds.Contributions, new SortProperty("Order", SortingDirection.Ascending));
            foreach (DatasetContribution c in ds.Contributions)
            {
                string aff = c.Person.Affiliation;
                if (String.IsNullOrEmpty(aff))
                    aff = ds.Project.Caption;
                if (c.Type == DatasetContactType.ADMINISTRATIVE_POINT_OF_CONTACT)
                    list.Add(@"<contact>");
                if (c.Type == DatasetContactType.METADATA_AUTHOR)
                    list.Add(@"<metadataProvider>");
                if ((c.Type == DatasetContactType.ORIGINATOR) || (c.Type == DatasetContactType.POINT_OF_CONTACT))
                    list.Add(@"<creator>");
                list.Add(@"<individualName>");
                list.Add(string.Format(@"<givenName>{0}</givenName>", c.Person.FirstName));
                list.Add(string.Format(@"<surName>{0}</surName>", c.Person.LastName));
                list.Add(@"</individualName>");
                list.Add(string.Format(@"<email>{0}</email>", c.Person.Email));
                list.Add(string.Format(@"<organizationName>{0}</organizationName>", aff));
                list.Add(string.Format(@"<positionName>{0}</positionName>", c.Contribution));
                list.Add(@"<address>");
                list.Add(string.Format(@"<deliveryPoint>{0}</deliveryPoint>", c.Person.Address));
                list.Add(string.Format(@"<city>{0}</city>", c.Person.City));
                list.Add(string.Format(@"<country>{0}</country>", c.Person.Country));
                list.Add(string.Format(@"<postalCode>{0}</postalCode>", c.Person.ZipCode));
                list.Add(@"</address>");

                list.Add(@"<phone>");
                list.Add(c.Person.Phone);
                list.Add(@"</phone>");
                list.Add(@"<electronicMailAddress>");
                list.Add(c.Person.Email);
                list.Add(@"</electronicMailAddress>");
                if (c.Type == DatasetContactType.ADMINISTRATIVE_POINT_OF_CONTACT)
                    list.Add(@"</contact>");
                if (c.Type == DatasetContactType.METADATA_AUTHOR)
                    list.Add(@"</metadataProvider>");
                if ((c.Type == DatasetContactType.ORIGINATOR) || (c.Type == DatasetContactType.POINT_OF_CONTACT))
                    list.Add(@"</creator>");
            }
            return list;
        }
        private static void GBIFGetDatasetContactsList1(Dataset ds, Integrations.GBIF.EML.Dataset emlds)
        {



            ds.Contributions.Sorting = new SortingCollection(ds.Contributions, new SortProperty("Order", SortingDirection.Ascending));
            foreach (DatasetContribution c in ds.Contributions)
            {
                string aff = c.Person.Affiliation;
                if (String.IsNullOrEmpty(aff))
                    aff = ds.Project.Caption;
                // Integrations.GBIF.EML.Contact person = new Contact();
                /*   person.individualName = new datasetContactIndividualName();
                   person.individualName.givenName = c.Person.FirstName;
                   person.individualName.surName = c.Person.LastName;
                   person.organizationName = c.Person.Affiliation;
                   person.phone = c.Person.Phone;
                   person.address = new datasetContactAddress();
                   person.address.country = c.Person.Country;
                   person.address.city = c.Person.City;
                   person.address.postalCode = c.Person.ZipCode;
                    person.address.deliveryPoint = c.Person.Address;*/
                /*     person.IndividualName = new IndividualName();
                  person.IndividualName.GivenName = c.Person.FirstName;
                  person.IndividualName.SurName = c.Person.LastName;*/
                //  person.OrganizationName = c.Person.Affiliation;
                /*   person.Phone = c.Person.Phone;
                   person.Address = new Address();
                   person.Address.Country = c.Person.Country;
                   person.Address.City = c.Person.City;
                   person.Address.PostalCode = c.Person.ZipCode;
                   person.Address.PostalCode = c.Person.ZipCode;
                   person.Address.DeliveryPoint = c.Person.Address;*/
                /*    if (c.Type == DatasetContactType.ADMINISTRATIVE_POINT_OF_CONTACT)
                        emlds.Contact = person;
                    if (c.Type == DatasetContactType.METADATA_AUTHOR)
                        emlds.MetadataProvider = person;
                    if ((c.Type == DatasetContactType.ORIGINATOR) || (c.Type == DatasetContactType.POINT_OF_CONTACT))
                        emlds.Creator = person;*/
            }
        }

        public static string GetDwcA(string filename, Dataset ds, IObjectSpace os, IModelListView view, IModelListView mediaview, IModelListView eventview, ref int exported, ref int empty, ref int failed, string gbifdoi, string zendoi)
        {
            Settings settings = os.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            using (Workbook wb = new Workbook())
            {

                if (String.IsNullOrEmpty(filename))
                    filename = String.Format(@"{0}dwc\dataset-{1}-dwca.zip", PathHelper.GetApplicationFolder(), ds.Oid);
                DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                Session session = new Session(((XPObjectSpace)os).Session.DataLayer);
                XPView xpv = new XPView(session, typeof(Unit));
                AddProperties(view, xpv);
                CriteriaOperator cr = CriteriaOperator.And(CriteriaOperator.Parse(view.Filter), new BinaryOperator("Dataset", ds.Oid));
                xpv.Criteria = cr;
                string expression = GetExpressionFromModel(view);
                //    expression = "Concat(Iif(IsNullOrEmpty([UnitID]),'',[UnitID]),Iif(IsNullOrEmpty([EventType.Name]),'',[EventType.Name]))";
                bool hasDynamicProperties = !String.IsNullOrEmpty(expression);
                if (hasDynamicProperties)
                    xpv.AddProperty("dynamicProperties", expression);
                if (xpv.Count > 0)
                {
                    var items = xpv.Cast<ViewRecord>();
                    ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                    dsOptions.ImportHeaders = true;
                    dsOptions.CellValueConverter = new UnitConverter();
                    WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                    try
                    {
                        using (ZipArchive archive = new ZipArchive())
                        {
                            wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\occurrences.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                            bool addmedia = CreateMultimedia(archive, ds, session, mediaview);
                            bool addEvent = CreateEvents(archive, ds, session, eventview);
                            archive.AddFile(Path.GetTempPath() + @"\occurrences.csv", @"\");
                            CreateEML(ds, gbifdoi, zendoi, os, settings.Host);
                            archive.AddFile(Path.GetTempPath() + @"\eml.xml", @"\");
                            //   string path_del=CreateDeletions(ds, os);
                            //  archive.AddFile(path_del, @"\");
                            /*List<IModelListView> views = new List<IModelListView>();
                            views.Add(view);
                            views.Add(mediaview);
                            views.Add(eventview);*/
                            CreateMeta(view, mediaview, eventview, addmedia, addEvent, hasDynamicProperties, true);
                            archive.AddFile(Path.GetTempPath() + @"\meta.xml", @"\");
                            //CreateDataPackageJson(wb,null, settings,views);
                           // archive.AddFile(Path.GetTempPath() + @"\datapackage.json", @"\");
                            archive.Save(filename);
                            exported++;
                            return String.Format(filename);
                        }
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                }
                else empty++; return "";
            }
        }


        private static bool CreateEvents(ZipArchive archive, Dataset ds, Session session, IModelListView view)
        {
            using (Workbook wb = new Workbook())
            {
                DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                XPView xpv = new XPView(session, typeof(LocalityVisit));
                IModelColumns cols = view.Columns;
                foreach (IModelColumn col in cols)
                {
                    if (col.Index > -1)
                        xpv.AddProperty(col.Caption, col.FieldName);
                }
                CriteriaOperator cr = CriteriaOperator.And(CriteriaOperator.Parse(view.Filter), new ContainsOperator("Units", new BinaryOperator("Dataset", ds.Oid)));
                xpv.Criteria = cr;
                if (xpv.Count > 0)
                {
                    var items = xpv.Cast<ViewRecord>();
                    ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                    dsOptions.ImportHeaders = true;
                    dsOptions.CellValueConverter = new UnitConverter();
                    WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                    wb.SaveDocument(Path.GetTempPath() + @"\events.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    archive.AddFile(Path.GetTempPath() + @"\events.csv", @"\");
                    return true;
                }
                return false;
            }
        }

        private static void AddProperties(IModelListView view, XPView xpv)
        {
            IModelColumns cols = view.Columns;
            foreach (IModelColumn col in cols)
            {
                if (!(col.ModelMember.GetNode("DwC").GetValue<bool>("IsDwcDynamicProperty")) && (((col.Index > -1) || (col.Index == null))))
                    xpv.AddProperty(col.Caption, col.FieldName);
            }
            if (xpv.Properties["Oid"] == null)
                xpv.AddProperty("oid", "oid");
        }

        private static string GetExpressionFromModel(IModelListView view)
        {
            IModelColumns cols = view.Columns;
            string expression = "Concat('{'";
            int count = 0;
            foreach (IModelColumn col in cols)
            {
                if (col.ModelMember.GetNode("DwC").GetValue<bool>("IsDwcDynamicProperty"))
                {
                    if (count == 0)
                    {
                        expression = expression + ",'\"" + col.Caption + "\":\"',Iif(IsNullOrEmpty([" + col.FieldName + "]),'',[" + col.FieldName + "]),'\"'";
                        count++;
                    }
                    else
                    {
                        expression = expression + ",',\"" + col.Caption + "\":\"',Iif(IsNullOrEmpty([" + col.FieldName + "]),'',[" + col.FieldName + "]),'\"'";
                        count++;
                    }
                }
            }
            expression = expression + ",'}')";
            return expression;
        }

        private static bool CreateMultimedia(ZipArchive archive, Dataset ds, Session session, IModelListView view)
        {
            using (Workbook wb = new Workbook())
            {
                DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                XPView xpv = new XPView(session, typeof(UnitAttachment));
                IModelColumns cols = view.Columns;
                foreach (IModelColumn col in cols)
                {
                    if (col.Index > -1)
                        xpv.AddProperty(col.Caption, col.FieldName);
                }
                CriteriaOperator cr = CriteriaOperator.And(CriteriaOperator.Parse(view.Filter), new BinaryOperator("Unit.Dataset", ds.Oid));
                xpv.Criteria = cr;
                if (xpv.Count > 0)
                {
                    var items = xpv.Cast<ViewRecord>();
                    ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                    dsOptions.ImportHeaders = true;
                    dsOptions.CellValueConverter = new UnitConverter();
                    WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                    wb.SaveDocument(Path.GetTempPath() + @"\multimedia.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    archive.AddFile(Path.GetTempPath() + @"\multimedia.csv", @"\");
                    return true;
                }
                return false;
            }
        }

        public static bool ZenodoPublishDataset(bool test, Dataset dataset, IObjectSpace objectspace, IModelListView modelview, IModelListView mediaview, IModelListView eventview, string gbifdoi, out string publisherror)
        {
            string deposition_id = "";
            string fileid = "";
            string zendoi = "";
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            string ZenodoApi = "";
            string ZenodoToken = "";
            if (test)
            {
                ZenodoApi = zenodoSandboxApi;
                ZenodoToken = settings.ZenodoSandboxToken;
            }
            else
            {
                ZenodoApi = zenodoApi;
                ZenodoToken = settings.ZenodoToken;
            }
            dataset.ZenodoResponsePublish = "";
            dataset.ZenodoResponseNewVersion = "";
            dataset.ZenodoResponseAddFile = "";
            /*     RestRequest getRequest = new RestRequest(Method.GET);
                    RestRequest.AddParameter("access_token", ZenodoToken);
           RestRequest.AddHeader("Accept", "application/json");
                   IRestResponse getResponse = client.Execute(RestRequest);*/

            JObject json = null;
            json = ZenodoGetDatasetMetadataJSON(dataset, gbifdoi, test, settings, false);
            dataset.ZenodoRequestMetadata = JToken.Parse(json.ToString()).ToString(Newtonsoft.Json.Formatting.Indented);
            bool isNewVersion = false;
            if (!String.IsNullOrEmpty(dataset.ZenodoConceptDoi) && !String.IsNullOrEmpty(dataset.ZenodoDoi) && !String.IsNullOrEmpty(dataset.ZenodoId))
            {
                deposition_id = dataset.ZenodoId;
                RestClient Checkclient = new RestClient(String.Format("{0}deposit/depositions/{1}?access_token={2}", ZenodoApi, dataset.ZenodoId, ZenodoToken));
                RestRequest CheckRequest = new RestRequest(RestSharp.Method.GET);
                CheckRequest.AddHeader("Content-Type", "application/json");
                IRestResponse CheckResponse = Checkclient.Execute(CheckRequest);
                if (CheckResponse.StatusCode == HttpStatusCode.OK)
                {
                    isNewVersion = true;
                    //create new version
                    RestClient client3 = new RestClient(String.Format("{0}deposit/depositions/{1}/actions/newversion?access_token={2}", ZenodoApi, dataset.ZenodoId, ZenodoToken));
                    RestRequest newVersionRequest = new RestRequest(RestSharp.Method.POST);
                    newVersionRequest.AddParameter("access_token", ZenodoToken);
                    newVersionRequest.AddHeader("Content-Type", "application/json");
                    newVersionRequest.AddJsonBody(json.ToString());
                    IRestResponse newVersionResponse = client3.Execute(newVersionRequest);
                    dataset.ZenodoResponseNewVersion = JToken.Parse(newVersionResponse.Content).ToString(Newtonsoft.Json.Formatting.Indented);
                    if (newVersionResponse.IsSuccessful)
                    {

                        deposition_id = (string)JObject.Parse(newVersionResponse.Content)["links"]["latest_draft"];// "350108"; // 
                        int pos = deposition_id.LastIndexOf("/") + 1;
                        deposition_id = deposition_id.Substring(pos, deposition_id.Length - pos);

                        //get new doi
                        Checkclient = new RestClient(String.Format("{0}/deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                        CheckRequest = new RestRequest(RestSharp.Method.GET);
                        CheckRequest.AddHeader("Content-Type", "application/json");
                        CheckResponse = Checkclient.Execute(CheckRequest);
                        zendoi = (string)JObject.Parse(CheckResponse.Content)["metadata"]["prereserve_doi"]["doi"];

                        if (isNewVersion)
                        {
                            //update

                            RestClient updateclient = new RestClient(String.Format("{0}deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                            RestRequest updateRequest = new RestRequest(RestSharp.Method.PUT);
                            updateRequest.AddParameter("access_token", ZenodoToken);
                            updateRequest.AddHeader("Content-Type", "application/json");
                            updateRequest.AddJsonBody(json.ToString());
                            IRestResponse updateResponse = updateclient.Execute(updateRequest);

                        }

                        //delete file

                        RestClient client4 = new RestClient(String.Format("{0}deposit/depositions/{1}/files/{2}", ZenodoApi, deposition_id, dataset.ZenodoFileId));
                        RestRequest deleteFileRequest = new RestRequest(RestSharp.Method.POST);
                        deleteFileRequest.AddParameter("access_token", ZenodoToken);
                        IRestResponse ndeleteFileResponse = client4.Delete(deleteFileRequest);
                    }
                }
            }
            else
            {
                RestClient client1 = new RestClient(String.Format("{0}deposit/depositions?access_token={1}", ZenodoApi, ZenodoToken));
                RestRequest postRequest = new RestRequest(RestSharp.Method.POST);
                postRequest.AddHeader("Content-Type", "application/json");
                postRequest.AddJsonBody(json.ToString());
                IRestResponse postResponse = client1.Execute(postRequest);
                dataset.ZenodoResponseDeposit = JToken.Parse(postResponse.Content).ToString(Newtonsoft.Json.Formatting.Indented);

                deposition_id = (string)JObject.Parse(postResponse.Content)["id"];// "350108"; // 
                zendoi = (string)JObject.Parse(postResponse.Content)["metadata"]["prereserve_doi"]["doi"];
            }

            int exported = 0;
            int empty = 0;
            int failed = 0;
            string path = PublishHelper.GetDwcA("", dataset, objectspace, modelview, mediaview, eventview, ref exported, ref empty, ref failed, gbifdoi, zendoi);
            string path_del = PublishHelper.CreateDeletions(dataset, objectspace);
            //add (new) file
            RestClient client2 = new RestClient(String.Format("{0}/deposit/depositions/{1}/files?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
            RestRequest addfileRequest = new RestRequest(RestSharp.Method.POST);
            //   addfileRequest.AddParameter("access_token", ZenodoToken);
            //  addfileRequest.AddParameter("data", "filename: " + Path.GetFileName(path));
            addfileRequest.AddFile("file", path, Path.GetFileName(path));
            IRestResponse addfileResponse = client2.Execute(addfileRequest);
            dataset.ZenodoResponseAddFile = JToken.Parse(addfileResponse.Content).ToString(Newtonsoft.Json.Formatting.Indented);

            if (addfileResponse.IsSuccessful)
            {
                fileid = (string)JObject.Parse(addfileResponse.Content)["id"];

                //publish
                RestClient client = new RestClient(String.Format("{0}/deposit/depositions/{1}/actions/publish?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                RestRequest publishDepositionRequest = new RestRequest(RestSharp.Method.POST);
                //  publishDepositionRequest.AddParameter("access_token", ZenodoToken);
                IRestResponse publishResponse = client.Execute(publishDepositionRequest);
                dataset.ZenodoResponsePublish = JToken.Parse(publishResponse.Content).ToString(Newtonsoft.Json.Formatting.Indented);

                if ((string)JObject.Parse(publishResponse.Content)["status"] == "400")
                {
                    //todo
                    //"{\"status\": 400, \"message\": \"Validation error.\", \"errors\": [{\"field\": null, \"message\": \"New version's files must differ from all previous versions.\", \"code\": 10}]}"
                }
                publisherror = "actions/publish: " + (string)JObject.Parse(publishResponse.Content)["message"];
                if (publishResponse.IsSuccessful)
                {
                    dataset.ZenodoFileId = fileid;
                    dataset.ZenodoId = deposition_id;
                    dataset.GbifAlternativeIdentifier = zendoi;
                    dataset.ZenodoRecId = (string)JObject.Parse(publishResponse.Content)["conceptrecid"]; ;
                    dataset.ZenodoUrl = test ? "https://sandbox.zenodo.org/record/" + deposition_id : "https://zenodo.org/record/" + deposition_id; ;
                    dataset.ZenodoConceptDoi = (string)JObject.Parse(publishResponse.Content)["conceptdoi"]; ;
                    dataset.ZenodoDoi = (string)JObject.Parse(publishResponse.Content)["doi"];
                    dataset.DwcaUrl = string.Format("{0}/files/dataset-{1}-dwca.zip", dataset.ZenodoUrl, dataset.Oid);
                    dataset.Save();
                    objectspace.CommitChanges();
                    return true;
                }
                else
                {
                    //delete
                    client = new RestClient(String.Format("{0}/deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                    RestRequest deleteDepositionRequest = new RestRequest(RestSharp.Method.DELETE);
                    publishDepositionRequest.AddParameter("access_token", ZenodoToken);
                    IRestResponse DeleteResponse = client.Execute(deleteDepositionRequest);

                    return false;
                }
            }
            publisherror = "files: " + (string)JObject.Parse(addfileResponse.Content)["message"];
            return false;
        }

        /*  public static bool ZenodoPublishSpecimen(bool test,Settings settings, Unit unit, string dp)
          {
              string deposition_id = "";
              string fileid = "";
              string zendoi = "";
              string ZenodoApi = "";
              string ZenodoToken = "";
              if (test)
              {
                  ZenodoApi = zenodoSandboxApi;
                  ZenodoToken = settings.ZenodoSandboxToken;
              }
              else
              {
                  ZenodoApi = zenodoApi;
                  ZenodoToken = settings.ZenodoToken;
              }

              JObject json = null;
              json = ZenodoGetSpecimenMetadataJSON(unit, test, settings, false);
              bool isNewVersion = false;
              if (!String.IsNullOrEmpty(unit.DOI))
              {
                  deposition_id = unit.DOI;
                  RestClient Checkclient = new RestClient(String.Format("{0}deposit/depositions/{1}?access_token={2}", ZenodoApi, unit.ZenodoId, ZenodoToken));
                  RestRequest CheckRequest = new RestRequest(RestSharp.Method.GET);
                  CheckRequest.AddHeader("Content-Type", "application/json");
                  IRestResponse CheckResponse = Checkclient.Execute(CheckRequest);
                  if (CheckResponse.StatusCode == HttpStatusCode.OK)
                  {
                      isNewVersion = true;
                      //create new version
                      RestClient client3 = new RestClient(String.Format("{0}deposit/depositions/{1}/actions/newversion?access_token={2}", ZenodoApi, unit.ZenodoId, ZenodoToken));
                      RestRequest newVersionRequest = new RestRequest(RestSharp.Method.POST);
                      newVersionRequest.AddParameter("access_token", ZenodoToken);
                      newVersionRequest.AddHeader("Content-Type", "application/json");
                      newVersionRequest.AddJsonBody(json.ToString());
                      IRestResponse newVersionResponse = client3.Execute(newVersionRequest);
                      if (newVersionResponse.IsSuccessful)
                      {

                          deposition_id = (string)JObject.Parse(newVersionResponse.Content)["links"]["latest_draft"];// "350108"; // 
                          int pos = deposition_id.LastIndexOf("/") + 1;
                          deposition_id = deposition_id.Substring(pos, deposition_id.Length - pos);

                          //get new doi
                          Checkclient = new RestClient(String.Format("{0}/deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                          CheckRequest = new RestRequest(RestSharp.Method.GET);
                          CheckRequest.AddHeader("Content-Type", "application/json");
                          CheckResponse = Checkclient.Execute(CheckRequest);
                          zendoi = (string)JObject.Parse(CheckResponse.Content)["metadata"]["prereserve_doi"]["doi"];

                          if (isNewVersion)
                          {
                              //update

                              RestClient updateclient = new RestClient(String.Format("{0}deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                              RestRequest updateRequest = new RestRequest(RestSharp.Method.PUT);
                              updateRequest.AddParameter("access_token", ZenodoToken);
                              updateRequest.AddHeader("Content-Type", "application/json");
                              updateRequest.AddJsonBody(json.ToString());
                              IRestResponse updateResponse = updateclient.Execute(updateRequest);

                          }

                          //delete file

                          RestClient client4 = new RestClient(String.Format("{0}deposit/depositions/{1}/files/{2}", ZenodoApi, deposition_id, unit.ZenodoFileId));
                          RestRequest deleteFileRequest = new RestRequest(RestSharp.Method.POST);
                          deleteFileRequest.AddParameter("access_token", ZenodoToken);
                          IRestResponse ndeleteFileResponse = client4.Delete(deleteFileRequest);
                      }
                  }
              }
              else
              {
                  RestClient client1 = new RestClient(String.Format("{0}deposit/depositions?access_token={1}", ZenodoApi, ZenodoToken));
                  RestRequest postRequest = new RestRequest(RestSharp.Method.POST);
                  postRequest.AddHeader("Content-Type", "application/json");
                  postRequest.AddJsonBody(json.ToString());
                  IRestResponse postResponse = client1.Execute(postRequest);

                  deposition_id = (string)JObject.Parse(postResponse.Content)["id"];// "350108"; // 
                  zendoi = (string)JObject.Parse(postResponse.Content)["metadata"]["prereserve_doi"]["doi"];
              }

              string path = dp;
              //add (new) file
              RestClient client2 = new RestClient(String.Format("{0}/deposit/depositions/{1}/files?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
              RestRequest addfileRequest = new RestRequest(RestSharp.Method.POST);
              //   addfileRequest.AddParameter("access_token", ZenodoToken);
              //  addfileRequest.AddParameter("data", "filename: " + Path.GetFileName(path));
              addfileRequest.AddFile("file", path, Path.GetFileName(path));
              IRestResponse addfileResponse = client2.Execute(addfileRequest);

              if (addfileResponse.IsSuccessful)
              {
                  fileid = (string)JObject.Parse(addfileResponse.Content)["id"];

                  //publish
                  RestClient client = new RestClient(String.Format("{0}/deposit/depositions/{1}/actions/publish?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                  RestRequest publishDepositionRequest = new RestRequest(RestSharp.Method.POST);
                  //  publishDepositionRequest.AddParameter("access_token", ZenodoToken);
                  IRestResponse publishResponse = client.Execute(publishDepositionRequest);

                  if ((string)JObject.Parse(publishResponse.Content)["status"] == "400")
                  {
                      //todo
                      //"{\"status\": 400, \"message\": \"Validation error.\", \"errors\": [{\"field\": null, \"message\": \"New version's files must differ from all previous versions.\", \"code\": 10}]}"
                  }
               //   publisherror = "actions/publish: " + (string)JObject.Parse(publishResponse.Content)["message"];
                  if (publishResponse.IsSuccessful)
                  {
                      unit.ZenodoFileId = fileid;
                      unit.ZenodoId = deposition_id;
                      unit.ZenodoRecId = (string)JObject.Parse(publishResponse.Content)["conceptrecid"]; ;
                      unit.ZenodoUrl = test ? "https://sandbox.zenodo.org/record/" + deposition_id : "https://zenodo.org/record/" + deposition_id; ;
                      unit.ZenodoConceptDoi = (string)JObject.Parse(publishResponse.Content)["conceptdoi"]; ;
                      unit.DOI = (string)JObject.Parse(publishResponse.Content)["doi"];
                     // unit.DwcaUrl = string.Format("{0}/files/dataset-{1}-dwca.zip", unit.ZenodoUrl, unit.Oid);
                      unit.Save();
                      return true;
                  }
                  else
                  {
                      //delete
                      client = new RestClient(String.Format("{0}/deposit/depositions/{1}?access_token={2}", ZenodoApi, deposition_id, ZenodoToken));
                      RestRequest deleteDepositionRequest = new RestRequest(RestSharp.Method.DELETE);
                      publishDepositionRequest.AddParameter("access_token", ZenodoToken);
                      IRestResponse DeleteResponse = client.Execute(deleteDepositionRequest);

                      return false;
                  }
              }
              return false;
          }
          */
        public static bool ZenodoUpdateMetadata(bool test, Dataset dataset, IObjectSpace objectspace, string gbifdoi)
        {
            // string deposition_id = "";
            // string fileid = "";
            // string zendoi = "";
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            string ZenodoApi = "";
            string ZenodoToken = "";
            if (test)
            {
                ZenodoApi = zenodoSandboxApi;
                ZenodoToken = settings.ZenodoSandboxToken;
            }
            else
            {
                ZenodoApi = zenodoApi;
                ZenodoToken = settings.ZenodoToken;
            }
            dataset.ZenodoResponsePublish = "";
            dataset.ZenodoResponseNewVersion = "";
            dataset.ZenodoResponseAddFile = "";
            /*     RestRequest getRequest = new RestRequest(Method.GET);
                    RestRequest.AddParameter("access_token", ZenodoToken);
           RestRequest.AddHeader("Accept", "application/json");
                   IRestResponse getResponse = client.Execute(RestRequest);*/

            JObject json = null;
            json = ZenodoGetDatasetMetadataJSON(dataset, gbifdoi, test, settings, true);
            dataset.ZenodoRequestMetadata = JToken.Parse(json.ToString()).ToString(Newtonsoft.Json.Formatting.Indented);
            if (!String.IsNullOrEmpty(dataset.ZenodoConceptDoi) && !String.IsNullOrEmpty(dataset.ZenodoDoi) && !String.IsNullOrEmpty(dataset.ZenodoId))
            {
                var client = new RestClient(ZenodoApi);
                var request = new RestRequest(String.Format("/deposit/depositions/{0}/actions/edit?access_token={1}", dataset.ZenodoId, ZenodoToken), RestSharp.Method.POST, DataFormat.None);
                IRestResponse updateResponse = client.Execute(request);

                request = new RestRequest(String.Format("/deposit/depositions/{0}?access_token={1}", dataset.ZenodoId, ZenodoToken), RestSharp.Method.PUT, DataFormat.Json);
                request.AddJsonBody(json.ToString());
                updateResponse = client.Execute(request);
                //     dataset.ZenodoRequestMetadata = JToken.Parse(updateResponse.Content).ToString(Newtonsoft.Json.Formatting.Indented);

                client = new RestClient(ZenodoApi);
                request = new RestRequest(String.Format("/deposit/depositions/{0}/actions/publish?access_token={1}", dataset.ZenodoId, ZenodoToken), RestSharp.Method.POST, DataFormat.None);
                updateResponse = client.Execute(request);

                dataset.Save();
                objectspace.CommitChanges();
                return true;
            }

            return false;
        }

        private static JObject ZenodoGetDatasetMetadataJSON(Dataset dataset, string doi, bool test, Settings settings, bool submitted)
        {
            JObject json;
            if (!string.IsNullOrEmpty(doi))
                json = new JObject
            {
                { "metadata", new JObject
                            {
                                { "upload_type", "dataset" },
                                { "description", dataset.PublishedDescription },
                                { "license", dataset.License.Name},
                               { "notes", "This dataset export was created by EarthCape software platform version "+System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()},
                                { "title", dataset.Name },
                                 { new JProperty("communities",ZenodoGetCommunitiesArray(test? settings.ZenodoSandboxCommunities : settings.ZenodoCommunities, dataset.Project!=null ? dataset.Project.ZenodoCommunity : ""))},
                                { new JProperty("creators", GBIFGetOrgContactsList(dataset))},
                                { new JProperty("related_identifiers", new JArray(
                                    new JObject
                                        {
                                            { "relation","isIdenticalTo" },
                                            { "identifier", doi},
                                        }
                                    ))}
                               // { "doi", doi },//excudes option of versioning
                               // { "prereserve_doi", "true"}
                            }
                },
                { "state",submitted ? "submitted" : "unsubmitted" },
                { "submitted", submitted ? "true" : "false"},
               { "title","" }
            };
            else
                json = new JObject
            {
                { "metadata", new JObject
                            {
                                { "upload_type", "dataset" },
                                { "description",dataset.PublishedDescription },
                                { "license", dataset.License.Name},
                                { "title", dataset.Name },
                                { new JProperty("communities",ZenodoGetCommunitiesArray(test? settings.ZenodoSandboxCommunities : settings.ZenodoCommunities, dataset.Project!=null ? dataset.Project.ZenodoCommunity : ""))},
                                { new JProperty("creators", GBIFGetOrgContactsList(dataset))}
                            }
                },
                { "state",submitted ? "submitted" : "unsubmitted" },
                { "submitted", submitted ? "true" : "false"},
              { "title","" }
            };
            return json;
        }

        private static JObject ZenodoGetSpecimenMetadataJSON(Unit unit, bool test, Settings settings, bool submitted)
        {
            JObject json;
            json = new JObject
            {
                { "metadata", new JObject
                            {
                                { "upload_type", "publication" },
                                { "publication_type", "other" },
                                { "description", unit.FullName },
                                { "license", unit.Dataset.License.Name},
                                { "notes", "This specimen record was created by EarthCape software platform version "+System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()},
                                { "title", "Specimen: "+unit.UnitID },
                                { new JProperty("communities","nat-hist-museum")},
                                { new JProperty("creators", GBIFGetOrgContactsList(unit.Dataset))}
                              }
                },
                { "state",submitted ? "submitted" : "unsubmitted" },
                { "submitted", submitted ? "true" : "false"},
                { "title","" }
            };

            return json;
        }

        private static JArray ZenodoGetCommunitiesArray(string communities, string prj_community)
        {
            JArray array = new JArray();
            string[] lines = communities.Split(new[] { "\r\n", "\r", "\n", ",", ", ", ";", "; " }, StringSplitOptions.None);
            foreach (string item in lines)
                array.Add(new JObject { { "identifier", item } });
            if (!string.IsNullOrEmpty(prj_community))
                array.Add(new JObject { { "identifier", prj_community } });
            return array;
        }

        private static JArray GBIFGetOrgContactsList(Dataset ds)
        {
            JArray arr = new JArray();
            ds.Contributions.Sorting = new SortingCollection(ds.Contributions, new SortProperty("Person.LastName", SortingDirection.Ascending));
            foreach (DatasetContribution c in ds.Contributions)
            {
                string aff = c.Person.Affiliation;
                if (String.IsNullOrEmpty(aff))
                    aff = ds.Project.Caption;
                JObject json = new JObject
                {
                    { "name",c.Person.FullName },
                    { "affiliation", aff},
                };
                arr.Add(json);
            }
            return arr;
        }
        private static JArray GetDoi(Dataset ds)
        {
            JArray arr = new JArray();
            ds.Contributions.Sorting = new SortingCollection(ds.Contributions, new SortProperty("Person.LastName", SortingDirection.Ascending));
            foreach (DatasetContribution c in ds.Contributions)
            {
                string aff = c.Person.Affiliation;
                if (String.IsNullOrEmpty(aff))
                    aff = ds.Project.Caption;
                JObject json = new JObject
                {
                    { "name",c.Person.FullName },
                    { "affiliation", aff},
                };
                arr.Add(json);
            }
            return arr;
        }
        public static void GbifCreateOrgContact(bool test, IObjectSpace objectspace, ProjectContact contact, bool primary)
        {
            //publish to gbif
            Project project = contact.Project;
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            JObject contactObj = new JObject
            {
                { "address", new JArray(contact.Person.Address) },
                { "createdBy", "EarthCape installation via API" },
                { "email", new JArray(contact.Person.Email) },
                { "firstName", contact.Person.FirstName },
                { "lastName", contact.Person.LastName },
                { "position", new JArray(contact.Position) },
                { "phone", new JArray(contact.Person.Phone) },
                { "primary", primary },
                { "type", contact.Type.ToString() }
            };

            if (String.IsNullOrEmpty(contact.GbifIdentifier))
            {
                var client = new RestClient(test ? gbifSandboxApi : gbifApi);
                client.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);
                //check

                var checkrequest = new RestRequest(string.Format("/organization/{0}/contact/{1}", project.GbifPublisherKey, contact.GbifIdentifier), RestSharp.Method.GET, DataFormat.Json);
                checkrequest.AddHeader("cache-control", "no-cache");
                checkrequest.AddHeader("accept", "application/json");
                IRestResponse checkresponse = client.Execute(checkrequest);

                //add/update

                var request = new RestRequest(string.Format("/organization/{0}/contact", project.GbifPublisherKey), RestSharp.Method.POST, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(contactObj.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.Created)
                {

                    contact.GbifIdentifier = response.Content;
                    contact.Save();
                    objectspace.CommitChanges();
                }
            }
            else

            {

                contactObj = new JObject
            {
                { "key", contact.GbifIdentifier },
                { "address", new JArray(contact.Person.Address) },
                { "createdBy", "EarthCape installation via API" },
                { "email", new JArray(contact.Person.Email) },
                { "firstName", contact.Person.FirstName },
                { "lastName", contact.Person.LastName },
                { "position", new JArray(contact.Position) },
                { "phone", new JArray(contact.Person.Phone) },
                { "primary", primary },
                { "created", contact.Person.CreatedOn.ToString("yyyy-MM-dd'T'HH:mm:ss")},
                { "modified", contact.LastModifiedOn.ToString("yyyy-MM-dd'T'HH:mm:ss")},
                { "type", contact.Type.ToString()}
            };
                var client = new RestClient(test ? gbifSandboxApi : gbifApi);
                client.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);
                var request = new RestRequest(string.Format("/organization/{0}/contact/{1}", project.GbifPublisherKey, contact.GbifIdentifier), RestSharp.Method.PUT, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(contactObj.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                }
            }
        }
        public static void GbifCreateDatasetContact(bool test, RestClient client, IObjectSpace objectspace, DatasetContribution contact)
        {
            //publish to gbif
            Dataset dataset = contact.Dataset;
            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            JObject contactObj = new JObject
            {
                { "address", new JArray(contact.Person.Address) },
                { "createdBy", "EarthCape installation via API" },
                { "email", new JArray(contact.Person.Email) },
                { "firstName", contact.Person.FirstName },
                { "lastName", contact.Person.LastName },
                { "position", new JArray(contact.Contribution) },
                { "phone", new JArray(contact.Person.Phone) },
                { "type", contact.Type.ToString() }
            };

            if (String.IsNullOrEmpty(contact.GbifIdentifier))
            {
                //add/update

                var request = new RestRequest(string.Format("/dataset/{0}/contact", dataset.GbifKey), RestSharp.Method.POST, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(contactObj.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.Created)
                {

                    contact.GbifIdentifier = response.Content;
                    contact.Save();
                    objectspace.CommitChanges();
                }
            }
            else

            {
                contactObj.Add("key", contact.GbifIdentifier);
                contactObj.Add("created", contact.Person.CreatedOn.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                contactObj.Add("modified", contact.LastModifiedOn.ToString("yyyy-MM-dd'T'HH:mm:ss"));


                var request = new RestRequest(string.Format("/dataset/{0}/contact/{1}", dataset.GbifKey, contact.GbifIdentifier), RestSharp.Method.PUT, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(contactObj.ToString());
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                }
            }
        }


        public static IRestResponse GbifCreateInstallation(GbifInstallationOptions options, Settings settings)
        {
            JObject result = new JObject
            {
                { "organizationKey", options.Test ? settings.GbifSandboxPublisherKey : settings.GbifPublisherKey },
                { "title", options.GbifInstallationTitle },
         { "type", "EARTHCAPE_INSTALLATION" }

            };
            string dataset_example = result.ToString();
            var client = new RestClient(options.Test ? PublishHelper.gbifSandboxApi : PublishHelper.gbifApi);
            client.Authenticator = new HttpBasicAuthenticator(options.Test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, options.Test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);
            var request = new RestRequest("/installation", RestSharp.Method.POST, DataFormat.Json);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("accept", "application/json");
            request.AddJsonBody(dataset_example);
            IRestResponse response = client.Execute(request);
            return response;
        }


        private static JObject GBIFGetMetadataJSON(bool test, Dataset dataset, Settings settings, string pubId, string hp)
        {
            return new JObject
            {
                { "installationKey", test ? settings.GbifSandboxInstallationKey : settings.GbifInstallationKey  },
              { "publishingOrganizationKey",pubId },
                { "title", dataset.Name },
             //  { "doi", dataset.ZenodoConceptDoi },
               { "license", dataset.License.Url },
               { "description", dataset.PublishedDescription },
               { "logoUrl", dataset.Project.LogoUrl },
             { "homepage", hp },
                { "type", "OCCURRENCE" }
            };
        }

        public static bool GbifAddDatasetEndPoint(RestClient client, IObjectSpace objectspace, Dataset dataset)
        {

            Settings settings = objectspace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            if (!string.IsNullOrEmpty(dataset.DwcaUrl))
            {
                JObject result = new JObject { { "url", dataset.DwcaUrl }, { "type", "DWC_ARCHIVE" }, { "description", "A Zenodo Record" }, };
                string endpoint = result.ToString();

                //delete endpoint
                if (!String.IsNullOrEmpty(dataset.GbifEndpointId))
                {
                    RestRequest request1 = new RestRequest(String.Format("/dataset/{0}/endpoint/{1}", dataset.GbifKey, dataset.GbifEndpointId), RestSharp.Method.POST, DataFormat.Json);
                    request1.AddHeader("cache-control", "no-cache");
                    request1.AddHeader("accept", "application/json");
                    IRestResponse response2 = client.Delete(request1);
                }

                //add endpoint
                RestRequest request = new RestRequest("/dataset/" + dataset.GbifKey + "/endpoint/", RestSharp.Method.POST, DataFormat.Json);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("accept", "application/json");
                request.AddJsonBody(endpoint);
                IRestResponse response1 = client.Execute(request);
                string endpointid = (string)response1.Content;


                if (response1.StatusCode == HttpStatusCode.Created)
                {
                    // dataset.DwcaUrl = ep;
                    dataset.GbifEndpointId = endpointid;
                    dataset.Save();
                    objectspace.CommitChanges();
                    return true;
                }
            }
            return false;
        }
        public static string CreateDataPackageBOLD(string filename, Settings settings, DataPackage pc, IModelListView unitsview_v, IModelListView unitsview_t, IModelListView unitsview_s, IModelListView unitsview_c, IModelListView taxaview, IModelListView locsview, IModelListView dsview, IModelListView prview, IModelListView cview, CriteriaOperator cr, CriteriaOperator crtaxa, CriteriaOperator crlocs, CriteriaOperator crdatasets, CriteriaOperator crprojects, CriteriaOperator crperson, IObjectSpace os, ref int exported, ref int empty, ref int failed, bool xlsx, bool dp)
        {
            using (Workbook wb = new Workbook())
            {

                if (String.IsNullOrEmpty(filename))
                    filename = String.Format(@"{0}output\datapackage-{1}.zip", PathHelper.GetApplicationFolder(), pc.Oid);
                DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                string sheetname = "Voucher info";
                Session session = new Session(((XPObjectSpace)os).Session.DataLayer);
                cr = session.ParseCriteria(cr.ToString());
                ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                dsOptions.ImportHeaders = true;
                dsOptions.CellValueConverter = new UnitConverter();

                sheet.Name = sheetname;
                XPView xpvunits_v = new XPView(session, typeof(DnaExtract), "", cr);
                AddProperties(unitsview_v, xpvunits_v);
                //  CriteriaOperator cr = CriteriaOperator.And(CriteriaOperator.Parse(view.Filter), CriteriaOperator.Parse(String.Format("[DataPackages][[Oid] = '{0}']", pc.Oid)));
                //   xpv.Criteria = cr;

                var items = xpvunits_v.Cast<ViewRecord>();
                WorksheetDataBinding sheetDataBinding = null;
                if (xpvunits_v.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\units.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }


                sheet = wb.Worksheets.Add();
                sheet.Name = "Taxonomy";
                XPView xpvunits_t = new XPView(session, typeof(DnaExtract), "", cr);
                AddProperties(unitsview_t, xpvunits_t);
                items = xpvunits_t.Cast<ViewRecord>();
                if (xpvunits_t.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                        wb.SaveDocument(Path.GetTempPath() + @"\taxonomy.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                sheet = wb.Worksheets.Add();
                sheet.Name = "Specimen Details";
                XPView xpvunits_s = new XPView(session, typeof(DnaExtract), "", cr);
                AddProperties(unitsview_s, xpvunits_s);
                items = xpvunits_s.Cast<ViewRecord>();
                if (xpvunits_s.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                        wb.SaveDocument(Path.GetTempPath() + @"\specimens.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                sheet = wb.Worksheets.Add();
                sheet.Name = "Collection Data";
                XPView xpvunits_c = new XPView(session, typeof(DnaExtract), "", cr);
                AddProperties(unitsview_c, xpvunits_c);
                items = xpvunits_c.Cast<ViewRecord>();
                if (xpvunits_c.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                        wb.SaveDocument(Path.GetTempPath() + @"\collection.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }


                sheet = wb.Worksheets.Add();
                sheet.Name = "Taxa";
                XPView xpvtaxa = new XPView(session, typeof(TaxonomicName), "", crtaxa);
                AddProperties(taxaview, xpvtaxa);
                items = xpvtaxa.Cast<ViewRecord>();
                if (xpvtaxa.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\taxa.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                sheet = wb.Worksheets.Add();
                sheet.Name = "Localities";
                XPView xpvlocs = new XPView(session, typeof(Locality), "", crlocs);
                AddProperties(locsview, xpvlocs);
                items = xpvlocs.Cast<ViewRecord>();
                if (xpvlocs.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\localities.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                sheet = wb.Worksheets.Add();
                sheet.Name = "Datasets";
                XPView xpvdatasets = new XPView(session, typeof(Dataset), "", crdatasets);
                AddProperties(dsview, xpvdatasets);
                items = xpvdatasets.Cast<ViewRecord>();
                if (xpvdatasets.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\datasets.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }
                sheet = wb.Worksheets.Add();
                sheet.Name = "Projects";
                XPView xpvprojects = new XPView(session, typeof(Project), "", crprojects);
                AddProperties(prview, xpvprojects);
                items = xpvprojects.Cast<ViewRecord>();
                if (xpvprojects.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\projects.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }

                sheet = wb.Worksheets.Add();
                sheet.Name = "People";
                XPView xpvpeople = new XPView(session, typeof(PersonCore), "", crperson);
                AddProperties(cview, xpvpeople);
                items = xpvpeople.Cast<ViewRecord>();
                if (xpvpeople.Count > 0)
                    sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                if (dp)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                        wb.SaveDocument(Path.GetTempPath() + @"\people.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }


                if (xlsx)
                    wb.SaveDocument(filename.Replace(".zip", ".xlsx"));

                if (dp)
                    try
                    {
                        using (ZipArchive archive = new ZipArchive())
                        {
                            archive.AddFile(Path.GetTempPath() + @"\voucher_info.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\taxonomy.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\specimens.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\taxa.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\localities.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\datasets.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\projects.csv", @"\");
                            archive.AddFile(Path.GetTempPath() + @"\people.csv", @"\");
                            CreateDataPackageJsonBOLD(pc, settings, unitsview_v, unitsview_t, unitsview_s, taxaview, locsview, dsview, prview, cview);
                            archive.AddFile(Path.GetTempPath() + @"\datapackage.json", @"\");
                            archive.Save(filename);
                            exported++;
                            return String.Format(filename);
                        }
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }
                return "";
            }
        }

        public static string CreateSpecimenPackage(string filename, Unit unit, IModelListView attachmentsview, IObjectSpace os, ref int exported, ref int empty, ref int failed, bool xlsx, bool dp)
        {
            // IModelListView unitsview = listviewunits.Model;
            Settings settings = os.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
            using (Workbook wb = new Workbook())
            {

                if (String.IsNullOrEmpty(filename))
                    filename = String.Format(@"{0}output\specimen-{1}.zip", PathHelper.GetApplicationFolder(), unit.Oid);
                DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                sheet.Name = "attachments";
                Session session = new Session(((XPObjectSpace)os).Session.DataLayer);
                XPView xpvattachments = new XPView(session, typeof(UnitAttachment), "", new BinaryOperator("Unit.Oid", unit.Oid));
                AddProperties(attachmentsview, xpvattachments);
                //  CriteriaOperator cr = CriteriaOperator.And(CriteriaOperator.Parse(view.Filter), CriteriaOperator.Parse(String.Format("[DataPackages][[Oid] = '{0}']", pc.Oid)));
                //   xpv.Criteria = cr;
                if (xpvattachments.Count > 0)
                {
                    var items = xpvattachments.Cast<ViewRecord>();
                    ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                    dsOptions.ImportHeaders = true;
                    dsOptions.CellValueConverter = new UnitConverter();
                    WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                    if (dp)
                        try
                        {
                            wb.Options.Export.Csv.WritePreamble = true;
                            wb.SaveDocument(Path.GetTempPath() + @"\people.csv", DevExpress.Spreadsheet.DocumentFormat.Csv);
                        }
                        catch (Exception)
                        {
                            failed++;
                            return "";
                        }

                }




                if (xlsx)
                    wb.SaveDocument(filename.Replace(".zip", ".xlsx"));

                if (dp)
                    try
                    {
                        using (ZipArchive archive = new ZipArchive())
                        {
                            archive.AddFile(Path.GetTempPath() + @"\attachments.csv", @"\");
                            // fails settings =null CreateDataPackageJson(pc, settings.Host, unitsview, taxaview, locsview, dsview, prview, cview);
                            archive.AddFile(Path.GetTempPath() + @"\datapackage.json", @"\");
                            /*        foreach (UnitAttachment item in unit.Attachments)
                                    {
                                        if (!String.IsNullOrEmpty(item.LocalPath))
                                        archive.AddFile(item.LocalPath);
                                    }*/
                            archive.Save(filename);
                            exported++;
                            return String.Format(filename);
                        }
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }
                return "";
            }
        }
        private static int fieldcount = 0;
        public static string CreateDataPackage(XafApplication application, DataPackageOptions options, Settings settings, CriteriaOperator crcore, IObjectSpace os, ref int exported, ref int empty, ref int failed)
        {
            fieldcount = 0;
            ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
            dsOptions.ImportHeaders = true;
            dsOptions.CellValueConverter = new UnitConverter();
            using (Workbook wb = new Workbook())
            {
                string filename = String.Format(@"{0}output\{1}.zip", PathHelper.GetApplicationFolder(), options.Name);
                Session session = new Session(((XPObjectSpace)os).Session.DataLayer);
                if (!ReferenceEquals(crcore, null))
                    crcore = session.ParseCriteria(crcore.ToString());

                List<IModelListView> coreviewslist = new List<IModelListView>();
                List<IModelListView> refviewslist = new List<IModelListView>();
                List<List<Guid>> list = new List<List<Guid>>();
                string[] coreviews = Regex.Split(options.CoreViews, "\r\n");
                foreach (string item in coreviews)
                {
                    coreviewslist.Add((IModelListView)application.Model.Views[item]);
                    list.Add(new List<Guid>());
                }
                string[] refviews = Regex.Split(options.ReferenceViews, "\r\n");
                foreach (string item in refviews)
                {
                    refviewslist.Add((IModelListView)application.Model.Views[item]);
                    list.Add(new List<Guid>());
                }

                IModelListView imodelView = (IModelListView)application.Model.Views[options.CoreViews];
                Type type = imodelView.ModelClass.TypeInfo.Type;
                XPView xpvcore = new XPView(session, type, "", crcore);
                xpvcore.AddProperty("oid", "oid");
                AddSheet(session, imodelView.Caption, wb, type, null, imodelView, options, dsOptions, crcore);

                if (xpvcore.Count > 0)
                {
                    var items = xpvcore.Cast<ViewRecord>();
                    foreach (ViewRecord r in items)
                    {
                        var obj = (r.GetObject());
                        int i = 0;
                        foreach (var v in refviewslist)
                        {
                            if (typeof(Locality).IsAssignableFrom(refviewslist[i].ModelClass.TypeInfo.Type))
                                if (obj is ILocalityObject)
                                    if (((ILocalityObject)obj).Locality != null)
                                        if (list[i].IndexOf(((ILocalityObject)obj).Locality.Oid) == -1)
                                        {
                                            list[i].Add(((ILocalityObject)obj).Locality.Oid);
                                        }
                            if (typeof(Dataset).IsAssignableFrom(refviewslist[i].ModelClass.TypeInfo.Type))
                                if (obj is IDatasetObject)
                                    if (((IDatasetObject)obj).Dataset != null)
                                    {
                                        if (list[i].IndexOf(((IDatasetObject)obj).Dataset.Oid) == -1)
                                        {
                                            list[i].Add(((IDatasetObject)obj).Dataset.Oid);
                                            /*  if (((IDatasetObject)obj).Dataset.Project != null)
                                                  if (list[i].IndexOf(((IDatasetObject)obj).Dataset.Project.Oid) == -1)
                                                  {
                                                      list[i].Add(((IDatasetObject)obj).Dataset.Project.Oid);
                                                  }
                                              foreach (DatasetContribution dc in ((IDatasetObject)obj).Dataset.Contributions)
                                              {
                                                  if (dc.Person != null)
                                                      if (list[i].IndexOf(dc.Person.Oid) == -1)
                                                          list[i].Add(dc.Person.Oid);
                                              }*/
                                        }
                                    }
                            if (typeof(TaxonomicName).IsAssignableFrom(refviewslist[i].ModelClass.TypeInfo.Type))
                                if (obj is ITaxonomicNameObject)
                                    if (((ITaxonomicNameObject)obj).TaxonomicName != null)
                                        if (list[i].IndexOf(((ITaxonomicNameObject)obj).TaxonomicName.Oid) == -1)
                                            list[i].Add(((ITaxonomicNameObject)obj).TaxonomicName.Oid);
                            i++;
                        }
                    }
                    foreach (IModelListView v in refviewslist)
                    {
                        AddSheet(session, v.Caption, wb, v.ModelClass.TypeInfo.Type, list[refviewslist.IndexOf(v)], v, options, dsOptions, null);
                    }
                    wb.Worksheets.RemoveAt(0);
                    Worksheet sheet = wb.Worksheets.Add();
                    sheet.Name = "fields";
                    sheet.Cells["A1"].SetValue("Resourse");
                    sheet.Cells["B1"].SetValue("Field");
                    sheet.Cells["C1"].SetValue("Type");
                    sheet.Cells["D1"].SetValue("ModelClass");
                    sheet.Cells["E1"].SetValue("ModelMember");
                    sheet.Cells["F1"].SetValue("PropertyName");
                    sheet.Cells["G1"].SetValue("Description");
                    wb.Worksheets.ActiveWorksheet = wb.Worksheets[0];
                }



                if (options.ExportDataPackage)
                    try
                    {
                        using (ZipArchive archive = new ZipArchive())
                        {
                            archive.AddFile(String.Format(@"{0}\{1}.csv", Path.GetTempPath(), imodelView.Caption), @"\");
                            foreach (IModelListView v in refviewslist)
                            {
                                archive.AddFile(String.Format(@"{0}\{1}.csv", Path.GetTempPath(), v.Caption), @"\");
                            }
                            /*   if (options.MakeDwCA)
                               {
                                   CreateEML(ds, gbifdoi, zendoi, os, settings.Host);
                                   archive.AddFile(Path.GetTempPath() + @"\eml.xml", @"\");
                                   CreateMeta(view, mediaview, eventview, addmedia, addEvent, hasDynamicProperties, true);
                                   archive.AddFile(Path.GetTempPath() + @"\meta.xml", @"\");
                        }*/
                            CreateDataPackageJson(wb, options, settings, coreviewslist, refviewslist);
                            archive.AddFile(Path.GetTempPath() + @"\datapackage.json", @"\");
                            archive.Save(filename);
                            exported++;
                            if (options.ExportExcel)
                                wb.SaveDocument(filename.Replace(".zip", ".xlsx"));
                            return String.Format(filename);
                        }
                    }
                    catch (Exception)
                    {
                        failed++;
                        return "";
                    }
                return "";
            }
        }
        static void AddSheet(Session session, string sh, Workbook wb, Type type, List<Guid> list, IModelListView view, DataPackageOptions options, ExternalDataSourceOptions dsOptions,CriteriaOperator crcore)
        {
            Worksheet sheet = wb.Worksheets.Add();
            sheet.Name = sh;
            CriteriaOperator cr = crcore;
            if (list != null)
                cr = new InOperator("Oid", list);
            XPView xpv = new XPView(session, type, "", cr);
            AddProperties(view, xpv);
            if (xpv.Count > 0)
            {
                sheet.DataBindings.BindToDataSource(xpv.Cast<ViewRecord>(), 0, 0, dsOptions);
                if (options.ExportDataPackage)
                    try
                    {
                        wb.Options.Export.Csv.WritePreamble = true;
                        wb.SaveDocument(String.Format(@"{0}\{1}.csv", Path.GetTempPath(), sh), DevExpress.Spreadsheet.DocumentFormat.Csv);

                    }
                    catch (Exception)
                    {
                    }
            }
        }

        public static void CreateDataPackageJsonDataset(Core.Dataset ds, string host, IModelListView view)
        {
            List<string> list = new List<string>();
            DataPackageRoot dp = new DataPackageRoot();
            dp.description = ds.PublishedDescription;
            dp.name = ds.Code;
            dp.id = ds.Oid.ToString();
            dp.title = ds.Name;
            dp.homepage = host;
            dp.profile = "tabular-data-package";
            DataPackageResource res = new DataPackageResource();
            res.name = "occurrences";
            res.path = "occurrences.csv";
            res.profile = "tabular-data-resource";
            res.description = ((IModelListView)(view)).ModelClass.GetNode("Documentation").GetValue<String>("Description");
            res.schema = new DataPackageSchema();
            res.schema.fields = new List<DataPackageField>();
            foreach (IModelColumn col in view.Columns)
            {
                DataPackageField field = new DataPackageField();
                field.name = col.Caption;
                field.description = col.ModelMember.GetNode("Documentation").GetValue<String>("Description");
                field.type = "string";
                // https://github.com/frictionlessdata/tableschema-go/blob/master/schema/field.go
                if (typeof(DateTime).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "date";
                if (typeof(float).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "number";
                if (typeof(double).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "number";
                if (typeof(int).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "integer";
                if (typeof(Int32).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "integer";
                if (typeof(bool).IsAssignableFrom(col.PropertyEditorType))
                    field.type = "boolean";
                res.schema.fields.Add(field);
            }
            //res.schema.fields.Add();
            DataPackageLicense lic = new DataPackageLicense();
            lic.name = ds.License.Name;
            lic.title = ds.License.Title;
            lic.path = ds.License.Url;
            dp.contributors = new List<DataPackageContributor>();
            ds.Contributions.Sorting = new SortingCollection(ds.Contributions, new SortProperty("Order", SortingDirection.Ascending));
            foreach (DatasetContribution contr in ds.Contributions)
            {
                if (contr.Person != null)
                {
                    PersonCore p = contr.Person;
                    DataPackageContributor c = new DataPackageContributor();
                    c.title = p.FullName;
                    if (!String.IsNullOrEmpty(contr.Contribution))
                        c.role = contr.Contribution;
                    if (!String.IsNullOrEmpty(p.Email))
                        c.email = p.Email;
                    if (!String.IsNullOrEmpty(p.Affiliation))
                        c.organisation = p.Affiliation;
                    dp.contributors.Add(c);
                }
            }
            dp.resources = new List<DataPackageResource>();
            dp.resources.Add(res);
            dp.licenses = new List<DataPackageLicense>();
            dp.licenses.Add(lic);
            //dp.version=
            JsonSerializerSettings s = new JsonSerializerSettings();
            s.MissingMemberHandling = MissingMemberHandling.Ignore;
            s.NullValueHandling = NullValueHandling.Ignore;
            string json = JsonConvert.SerializeObject(dp, Newtonsoft.Json.Formatting.Indented, s);
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\datapackage.json");
            tw.Write(json);
            tw.Close();

        }

        public static void CreateDataPackageJson(Workbook wb,DataPackageOptions options, Settings settings, List<IModelListView> coreviewslist, List<IModelListView> refviewslist)
        {
            List<string> list = new List<string>();
            DataPackageRoot dp = new DataPackageRoot();
            //dp.description = ds.Description;
            dp.name = options.Name;
            dp.id = options.Name;
            dp.title = options.Name;
            dp.homepage = settings.Host;
            dp.profile = "tabular-data-package";
            dp.resources = new List<DataPackageResource>();
            foreach (IModelListView v in coreviewslist)
            {
                DataPackageResource r = new DataPackageResource();
                AddResource(wb, v, r, v.Caption);
                dp.resources.Add(r);
            }
            foreach (IModelListView v in refviewslist)
            {
                DataPackageResource r = new DataPackageResource();
                AddResource(wb, v, r, v.Caption);
                dp.resources.Add(r);
            }
            //res.schema.fields.Add();
            DataPackageLicense lic = new DataPackageLicense();
            // lic.name = ds.License.Name;
            //lic.title = ds.License.Title;
            //lic.path = ds.License.Url;
            dp.contributors = new List<DataPackageContributor>();
          /*  ds.Contacts.Sorting = new SortingCollection(ds.Contacts, new SortProperty("Order", SortingDirection.Ascending));
            foreach (ProjectContact contr in ds.Contacts)
            {
                if (contr.Person != null)
                {
                    PersonCore p = contr.Person;
                    DataPackageContributor c = new DataPackageContributor();
                    c.title = p.FullName;
                    if (!String.IsNullOrEmpty(contr.Position))
                        c.role = contr.Position;
                    if (!String.IsNullOrEmpty(p.Email))
                        c.email = p.Email;
                    if (!String.IsNullOrEmpty(p.Affiliation))
                        c.organisation = p.Affiliation;
                    dp.contributors.Add(c);
                }
            }*/
            dp.licenses = new List<DataPackageLicense>();
            dp.licenses.Add(lic);
            //dp.version=
            JsonSerializerSettings s = new JsonSerializerSettings();
            s.MissingMemberHandling = MissingMemberHandling.Ignore;
            s.NullValueHandling = NullValueHandling.Ignore;
            string json = JsonConvert.SerializeObject(dp, Newtonsoft.Json.Formatting.Indented, s);
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\datapackage.json");
            tw.Write(json);
            tw.Close();

        }
        public static void CreateZenodoSpecimenJson(Unit unit, IModelListView unitv, IObjectSpace os, Settings settings)
        {
            List<string> list = new List<string>();
            ZenodoSpecimen dp = new ZenodoSpecimen();
            dp.Context = new Context();
            dp.Context.Dwc = new Uri("http://rs.tdwg.org/dwc/terms/");
            dp.Context.Dcterms = new Uri("http://purl.org/dc/terms/");
            dp.Context.Dc = new Uri("http://purl.org/dc/elements/1.1/");
            dp.Context.Gbif = new Uri("http://rs.gbif.org/terms/1.0");
            Specimen gr = new Specimen();
            gr.DcType = DcmiType.PhysicalObject.ToString();
            //gr.DwcCatalogNumber = unit.UnitID;
            // gr.Terms = new List<SpecimenTerm>();
            Session session = new Session(((XPObjectSpace)os).Session.DataLayer);

            XPView xpvunits_v = new XPView(session, typeof(Unit), "", new BinaryOperator("Oid", unit.Oid));
            //   if (xpvunits_v[0] != null)
            foreach (IModelColumn col in unitv.Columns)
            {
                xpvunits_v.AddProperty(col.Caption, col.FieldName);
                if (gr.GetType().GetProperty(col.Caption) != null)
                {
                    if (xpvunits_v[0][col.Caption] != null)
                    {
                        if (col.Caption == "DwcOccurrenceId")
                        {
                            gr.DwcOccurrenceId = new Uri(String.Format("{0}{2}specimen/{1}", settings.Host, unit.Oid, settings.Host.Substring(settings.Host.Length - 1) != "/" ? "/" : ""));
                        }
                        else
                        {
                            if ((col.ModelMember.Type.IsEnum) || (GeneralHelper.IsNullableEnum(col.ModelMember.Type)))
                                gr.GetType().GetProperty(col.Caption).SetValue(gr, Enum.Parse(col.ModelMember.Type, xpvunits_v[0][col.Caption].ToString()).ToString());
                            else
                                gr.GetType().GetProperty(col.Caption).SetValue(gr, xpvunits_v[0][col.Caption]);
                        }
                    }
                }
            }
            dp.Graph = new List<Specimen>();
            dp.Graph.Add(gr);


            JsonSerializerSettings s = new JsonSerializerSettings();
            s.MissingMemberHandling = MissingMemberHandling.Ignore;
            s.NullValueHandling = NullValueHandling.Ignore;
            string json = JsonConvert.SerializeObject(dp, Newtonsoft.Json.Formatting.Indented, s);
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\specimen.json");
            tw.Write(json);
            tw.Close();

        }

        public static void CreateDataPackageJsonBOLD(DataPackage ds, Settings settings, IModelListView unitsview_v, IModelListView unitsview_t, IModelListView unitsview_s, IModelListView taxaview, IModelListView locsview, IModelListView dsview, IModelListView prview, IModelListView cview)
        {
            List<string> list = new List<string>();
            DataPackageRoot dp = new DataPackageRoot();
            dp.description = ds.Description;
            dp.name = ds.Code;
            dp.id = ds.Oid.ToString();
            dp.title = ds.Name;
            dp.homepage = settings.Host;
            dp.profile = "tabular-data-package";
            DataPackageResource resunits_v = new DataPackageResource();
            DataPackageResource resunits_t = new DataPackageResource();
            DataPackageResource resunits_s = new DataPackageResource();
            DataPackageResource restaxa = new DataPackageResource();
            DataPackageResource reslocs = new DataPackageResource();
            DataPackageResource resds = new DataPackageResource();
            DataPackageResource respr = new DataPackageResource();
            DataPackageResource resc = new DataPackageResource();
            AddResource(null,unitsview_v, resunits_v, "voucher info");
            AddResource(null, unitsview_t, resunits_t, "taxonomy");
            AddResource(null, unitsview_s, resunits_s, "specimens");
            AddResource(null, taxaview, restaxa, "taxa");
            AddResource(null, locsview, reslocs, "localities");
            AddResource(null, dsview, resds, "datasets");
            AddResource(null, prview, respr, "projects");
            AddResource(null, cview, resc, "people");
            //res.schema.fields.Add();
            DataPackageLicense lic = new DataPackageLicense();
            // lic.name = ds.License.Name;
            //lic.title = ds.License.Title;
            //lic.path = ds.License.Url;
            dp.contributors = new List<DataPackageContributor>();
            ds.Contacts.Sorting = new SortingCollection(ds.Contacts, new SortProperty("Order", SortingDirection.Ascending));
            foreach (ProjectContact contr in ds.Contacts)
            {
                if (contr.Person != null)
                {
                    PersonCore p = contr.Person;
                    DataPackageContributor c = new DataPackageContributor();
                    c.title = p.FullName;
                    if (!String.IsNullOrEmpty(contr.Position))
                        c.role = contr.Position;
                    if (!String.IsNullOrEmpty(p.Email))
                        c.email = p.Email;
                    if (!String.IsNullOrEmpty(p.Affiliation))
                        c.organisation = p.Affiliation;
                    dp.contributors.Add(c);
                }
            }
            dp.resources = new List<DataPackageResource>();
            dp.resources.Add(resunits_v);
            dp.resources.Add(resunits_t);
            dp.resources.Add(resunits_s);
            dp.resources.Add(restaxa);
            dp.resources.Add(reslocs);
            dp.resources.Add(resds);
            dp.resources.Add(respr);
            dp.resources.Add(resc);
            dp.licenses = new List<DataPackageLicense>();
            dp.licenses.Add(lic);
            //dp.version=
            JsonSerializerSettings s = new JsonSerializerSettings();
            s.MissingMemberHandling = MissingMemberHandling.Ignore;
            s.NullValueHandling = NullValueHandling.Ignore;
            string json = JsonConvert.SerializeObject(dp, Newtonsoft.Json.Formatting.Indented, s);
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\datapackage.json");
            tw.Write(json);
            tw.Close();

        }
        private static void AddResource(Workbook wb,IModelListView unitsview, DataPackageResource res, string name)
        {
            res.name = name;
            res.path = name + ".csv";
            res.profile = "tabular-data-resource";
            res.description = ((IModelListView)(unitsview)).ModelClass.GetNode("Documentation").GetValue<String>("Description");
            res.schema = new DataPackageSchema();
            res.schema.fields = new List<DataPackageField>();
            foreach (IModelColumn col in unitsview.Columns)
            {
                if ((col.Index == null) || (col.Index > -1))
                {
                    fieldcount++;
                    DataPackageField field = new DataPackageField();
                    field.name = col.Caption;
                    //   field.description = col.ModelMember.GetNode("Documentation").GetValue<String>("Description");
                    // field.description = col.ToolTip;
                    field.description = col.GetNode("Documentation").GetValue<String>("Description");
                    if (String.IsNullOrEmpty(field.description))
                    field.description = col.ModelMember.GetNode("Documentation").GetValue<String>("Description");
                    field.type = col.ModelMember.Type.Name;
                    Type type = col.ModelMember.Type;
                    if (Nullable.GetUnderlyingType(type) != null) type = Nullable.GetUnderlyingType(type);
                    // https://github.com/frictionlessdata/tableschema-go/blob/master/schema/field.go
                    if (typeof(DateTime).IsAssignableFrom(type))
                        field.type = "date";
                   /* if (type.ToString().Contains("System.Double"))
                        field.type = "number";
                    if (type.ToString().Contains("System.Float"))
                        field.type = "number";
                    if (type.ToString().Contains("System.Decimal"))
                        field.type = "number";
                    if (type.ToString().Contains("System.DateTime"))
                        field.type = "date";*/
                    if (typeof(float).IsAssignableFrom(type))
                        field.type = "number";
                    if (typeof(double).IsAssignableFrom(type))
                        field.type = "number";
                    if (typeof(decimal).IsAssignableFrom(type))
                        field.type = "number";
                    if (typeof(int).IsAssignableFrom(type))
                        field.type = "integer";
                    if (typeof(Int32).IsAssignableFrom(type))
                        field.type = "integer";
                    if (typeof(bool).IsAssignableFrom(type))
                        field.type = "boolean";
                    if (typeof(String).IsAssignableFrom(type))
                        field.type = "string";
                    if (type.IsEnum)
                        field.type = "string";
                    if (GeneralHelper.IsNullableEnum(type))
                        field.type = "string";
                    res.schema.fields.Add(field);
                    if (wb != null)
                    {
                        wb.Worksheets["fields"].Cells[fieldcount, 0].SetValue(res.name);
                        wb.Worksheets["fields"].Cells[fieldcount, 1].SetValue(field.name);
                        wb.Worksheets["fields"].Cells[fieldcount, 2].SetValue(field.type);
                        wb.Worksheets["fields"].Cells[fieldcount, 3].SetValue(unitsview.ModelClass.Name);
                        wb.Worksheets["fields"].Cells[fieldcount, 4].SetValue(col.ModelMember.Name);
                        wb.Worksheets["fields"].Cells[fieldcount, 5].SetValue(col.PropertyName);
                        wb.Worksheets["fields"].Cells[fieldcount, 6].SetValue(field.description);
                    }
                }
            }
        }

        public static void CreateMeta(IModelListView unitView, IModelListView mediaView, IModelListView eventView, bool addmedia, bool addevent, bool dynamicProperties, bool add_deletions)
        {
            List<string> list = new List<string>();
            list.Add(@"<?xml version=""1.0"" encoding=""UTF-8""?>");
            list.Add(@"<archive metadata=""eml.xml"" xmlns=""http://rs.tdwg.org/dwc/text/"">");
            list.Add(@"  <core dateFormat=""YYYY-MM-DD"" encoding=""UTF-8"" fieldsTerminatedBy="","" linesTerminatedBy=""\n"" fieldsEnclosedBy=""&quot;"" ignoreHeaderLines=""1"" rowType=""http://rs.tdwg.org/dwc/terms/Occurrence"">");
            list.Add(@"    <files>");
            list.Add(@"      <location>occurrences.csv</location>");
            list.Add(@"    </files>");
            CreateMetaFields(list, unitView, "http://rs.tdwg.org/dwc/terms", dynamicProperties);
            list.Add(@"  </core>");
            if (addmedia)
            {
                list.Add(@"  <extension rowType = ""http://rs.gbif.org/terms/1.0/Multimedia"" ignoreHeaderLines = ""1"" fieldsEnclosedBy = """" linesTerminatedBy = ""\n"" fieldsTerminatedBy = "","" encoding = ""UTF-8"">");
                list.Add(@"     <files>");
                list.Add(@" <location>multimedia.csv</location>");
                list.Add(@"     </files>");
                CreateMetaFields(list, mediaView, "http://purl.org/dc/terms", false);
                list.Add(@"  </extension>");
            }
            if (addevent)
            {
                list.Add(@"  <extension rowType = ""http://rs.tdwg.org/dwc/terms/Event"" ignoreHeaderLines = ""1"" fieldsEnclosedBy = """" linesTerminatedBy = ""\n"" fieldsTerminatedBy = "","" encoding = ""UTF-8"">");
                list.Add(@"     <files>");
                list.Add(@" <location>events.csv</location>");
                list.Add(@"     </files>");
                CreateMetaFields(list, eventView, "http://rs.tdwg.org/dwc/terms", false);
                list.Add(@"  </extension>");
            }
            /*  if (add_deletions)
              {
                  list.Add(@"  <extension ignoreHeaderLines = ""0"" fieldsEnclosedBy = """" linesTerminatedBy = ""\n"" fieldsTerminatedBy = "","" encoding = ""UTF-8"">");
                  list.Add(@"     <files>");
                  list.Add(@" <location>occurrences.deletions.txt</location>");
                  list.Add(@"     </files>");
                  list.Add("<coreid index = \"0\" />");
                  list.Add(String.Format(@"    <field index=""{0}"" term=""{1}""/>", "1", "http://purl.org/dc/terms/identifier"));
                  list.Add(@"  </extension>");
              }*/
            list.Add(@" </archive>");
            TextWriter tw = new StreamWriter(Path.GetTempPath() + @"\meta.xml");

            foreach (String s in list)
                tw.WriteLine(s);

            tw.Close();
            /*string text = string.Join(Environment.NewLine, list.ToArray());
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            meta.Write(buffer, 0, buffer.Length);*/
        }
        public static void GbifCrawl(Settings settings, Dataset dataset)
        {
            bool test = dataset.GbifUrl.Contains("gbif-uat");
            var client = new RestClient(test ? PublishHelper.gbifSandboxApi : PublishHelper.gbifApi);
            client.Authenticator = new HttpBasicAuthenticator(test ? settings.GbifSandboxAdminUser : settings.GbifAdminUser, test ? settings.GbifSandboxAdminPassword : settings.GbifAdminPassword);
            var request = new RestRequest("/dataset/" + dataset.GbifKey + "/crawl", RestSharp.Method.POST, DataFormat.None);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("accept", "application/json");
            IRestResponse response = client.Execute(request);
        }
        public static void CreateMetaFields(List<string> list, IModelListView view,string defaultschema, bool dynamicProperties)
        {
            IModelColumns cols = view.Columns;
            cols.OrderBy(f => f.Caption);
            int i = 0;
            string termschema = "";
            foreach (IModelColumn col in cols)
            {
                termschema = col.ModelMember.GetNode("DwC").GetValue<string>("Term");
                if (String.IsNullOrEmpty(termschema))
                    termschema = defaultschema;
                if (!(col.ModelMember.GetNode("DwC").GetValue<bool>("IsDwcDynamicProperty")) && (((col.Index > -1) || (col.Index == null))))
                {
                    if (i == 0)
                        list.Add(String.Format(@"    <{0} index=""0""/>", col.Caption));
                    else
                        list.Add(String.Format(@"    <field index=""{0}"" term=""{1}/{2}""/>", i, termschema, col.Caption));
                    i++;
                }
            }
            if (dynamicProperties)
            {
                list.Add(String.Format(@"    <field index=""{0}"" term=""{1}/{2}""/>", i, termschema, "dynamicProperties"));
            }
        }

        public static void GetDatasets(List<Core.Dataset> datasets, ListView view)
        {
            foreach (Unit unit in view.SelectedObjects)
            {
                if (unit.Dataset != null)
                    if (datasets.IndexOf(unit.Dataset) < 0) datasets.Add(unit.Dataset);
            }
        }

        public static void GBIFTaxonomy(TaxonomicName item, IObjectSpace os)
        {
            string tname = item.FullName.Replace(" ", "+");

            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://api.gbif.org/v1/species/match?verbose=false&name=" + tname);
                JObject o = JObject.Parse(json);

                string canonicalName = (string)o["canonicalName"];
                string usageKey = (string)o["usageKey"];
                string acceptedUsageKey = (string)o["acceptedUsageKey"];
                string matchType = (string)o["matchType"];
                string status = (string)o["status"];
                string rank = (string)o["rank"];
                string genus = (string)o["genus"];
                string family = (string)o["family"];
                string order = (string)o["order"];
                string _class = (string)o["class"];
                string phylum = (string)o["phylum"];
                string kingdom = (string)o["kingdom"];

                string speciesKey = (string)o["speciesKey"];
                string genusKey = (string)o["genusKey"];
                string familyKey = (string)o["familyKey"];
                string orderKey = (string)o["orderKey"];
                string classKey = (string)o["classKey"];
                string phylumKey = (string)o["phylumKey"];
                string kingdomKey = (string)o["kingdomKey"];

                if (matchType != "EXACT") return;
                Species sp = null;
                SubspecificTaxon sspt = null;
                Genus g = null;
                Family fam = null;
                try
                {
                    if ((item is Species) && (rank == "SPECIES"))
                    {
                        sp = (Species)item;
                        sp.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Species");
                        sp.GbifKey = speciesKey;
                        sp.GbifStatus = status;
                        try
                        {
                            sp.NcbiTaxonId = GetNCBI(tname);
                        }
                        catch { }
                        if (sp.Name.Contains(" "))
                        {
                            var gen = sp.FullName.Split(new string[] { " " }, 2, StringSplitOptions.None)[0].Trim();
                            var spn = sp.FullName.Split(new string[] { " " }, 2, StringSplitOptions.None)[1].Trim();

                            g = os.FindObject<Genus>(new BinaryOperator("Name", gen), true);
                            if (g == null)
                            {
                                g = os.CreateObject<Genus>();
                                g.Name = gen;
                                g.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Genus");
                                //g.GbifKey = genusKey;
                                g.Save();
                            }
                            if (sp.Genus == null)
                            {
                                sp.Name = spn;
                                sp.Parent = g;
                                sp.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Species");
                                sp.Category = g;
                                sp.Genus = g;
                            }
                        }
                        sp.Save();
                    }
                    if ((item is SubspecificTaxon))
                    {
                        sspt = (SubspecificTaxon)item;
                        if (rank == "SUBSPECIES")
                            sspt.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Subspecies");
                        if (rank == "VARIETY")
                            sspt.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Variety");
                        if (rank == "FORMA")
                            sspt.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Forma");
                        sspt.GbifKey = usageKey;
                        sspt.GbifStatus = status;
                        var WordsArray = canonicalName.Split();
                        string sp_ = WordsArray[0] + ' ' + WordsArray[1];
                        sspt.Name = WordsArray[WordsArray.Length - 1];

                        if (sspt.Species != null)
                        {
                            GBIFTaxonomy(sspt.Species, os);
                        }
                        else
                        {

                            Species species_ = GeneralHelper.GetSpecies(os, sp_, false);
                            if (species_ == null)
                            {
                                species_ = os.CreateObject<Species>();
                                species_.Name = sp_;
                                species_.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Species");
                                species_.Save();
                                GBIFTaxonomy(species_, os);
                            }
                            sspt.Species = species_;
                        }

                        sspt.Save();
                    }
                    if ((item is Genus) && (rank == "GENUS"))
                    {
                        g = os.FindObject<Genus>(new BinaryOperator("Name", genus), true);
                        g.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Genus");
                        g.GbifKey = genusKey;
                        if (g == null)
                        {
                            g = os.CreateObject<Genus>();
                            g.Name = genus;
                        }
                        g.Save();
                    }


                    if (family != null)
                    {
                        fam = os.FindObject<Family>(new BinaryOperator("Name", family), true);
                        if (fam == null)
                        {
                            fam = os.CreateObject<Family>();
                            fam.Name = family;
                        }
                        if (String.IsNullOrEmpty(fam.GbifKey))
                            fam.GbifKey = familyKey;
                        if (fam.TaxonomicCategory == null)
                            fam.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Family");
                        fam.Save();
                        if (sp != null)
                        {
                            if (sp.Genus != null)
                            {
                                sp.Genus.Parent = fam;
                            }
                            else
                            {
                                sp.Parent = fam;
                            }
                            sp.Save();
                        }
                        if (g != null)
                        {
                            g.Parent = fam;
                            g.Save();
                        }
                        if (order != null)
                        {
                            if ((fam != null) && (fam.Parent == null))
                            {
                                HigherTaxon ord = os.FindObject<HigherTaxon>(new BinaryOperator("Name", order), true);
                                if (ord == null)
                                {
                                    ord = os.CreateObject<HigherTaxon>();
                                    ord.Name = order;
                                }
                                if (String.IsNullOrEmpty(ord.GbifKey))
                                    ord.GbifKey = orderKey;
                                if (ord.TaxonomicCategory == null)
                                    ord.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Order");
                                ord.Save();
                                if (fam != null)
                                {
                                    fam.Parent = ord;
                                    fam.Save();
                                }
                                if (_class != null)
                                {
                                    if ((ord != null) && (ord.Parent == null))
                                    {
                                        HigherTaxon class1 = os.FindObject<HigherTaxon>(new BinaryOperator("Name", _class), true);
                                        if (class1 == null)
                                        {
                                            class1 = os.CreateObject<HigherTaxon>();
                                            class1.Name = _class;
                                        }
                                        if (String.IsNullOrEmpty(class1.GbifKey))
                                            class1.GbifKey = classKey;
                                        if (class1.TaxonomicCategory == null)
                                            class1.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Class");
                                        class1.Save();

                                        ord.Parent = class1;
                                        ord.Save();
                                        if (phylum != null)
                                        {
                                            if ((class1 != null) && (class1.Parent == null))
                                            {
                                                HigherTaxon phyl = os.FindObject<HigherTaxon>(new BinaryOperator("Name", phylum), true);
                                                if (phyl == null)
                                                {
                                                    phyl = os.CreateObject<HigherTaxon>();
                                                    phyl.Name = phylum;
                                                }
                                                if (String.IsNullOrEmpty(phyl.GbifKey))
                                                    phyl.GbifKey = phylumKey;
                                                if (phyl.TaxonomicCategory == null)
                                                    phyl.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Phylum");
                                                phyl.Save();

                                                class1.Parent = phyl;
                                                class1.Save();
                                                if (kingdom != null)
                                                {
                                                    if ((phyl != null) && (phyl.Parent == null))
                                                    {
                                                        HigherTaxon king = os.FindObject<HigherTaxon>(new BinaryOperator("Name", kingdom), true);
                                                        if (king == null)
                                                        {
                                                            king = os.CreateObject<HigherTaxon>();
                                                            king.Name = kingdom;
                                                        }
                                                        if (String.IsNullOrEmpty(king.GbifKey))
                                                            king.GbifKey = kingdomKey;
                                                        if (king.TaxonomicCategory == null)
                                                            king.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Kingdom");
                                                        king.Save();

                                                        phyl.Parent = king;
                                                        phyl.Save();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
                finally
                {
                    if (item.GbifStatus == "ACCEPTED")
                    {
                        item.GbifValidTaxonomicName = null;
                    }
                    if (item.GbifStatus == "SYNONYM")
                    {
                        item.GbifValidTaxonomicName = os.FindObject<TaxonomicName>(
                            CriteriaOperator.And(
                                new BinaryOperator("GbifKey", item.GbifKey),
                                CriteriaOperator.Or(
                                   new BinaryOperator("GbifStatus", "ACCEPTED"),
                                   new BinaryOperator("GbifStatus", "DOUBTFUL")
                                   )
                                )
                                );
                        if (item.GbifValidTaxonomicName == null)
                        {
                            item.GbifValidTaxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("GbifKey", acceptedUsageKey));
                            if (item.GbifValidTaxonomicName == null)
                                GBIFTaxonomyDownloadByKey(acceptedUsageKey, item, os);
                        }
                        item.Save();
                    }
                }
            }
        }
        public static void GBIFStatus(TaxonomicName item, IObjectSpace os)
        {
            string tname = item.FullName.Replace(" ", "+");

            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://api.gbif.org/v1/species/match?verbose=false&name=" + tname);
                JObject o = JObject.Parse(json);

                string matchType = (string)o["matchType"];
                string status = (string)o["status"];
                string rank = (string)o["rank"];
                string genus = (string)o["genus"];
                string family = (string)o["family"];
                string order = (string)o["order"];
                string _class = (string)o["class"];
                string phylum = (string)o["phylum"];
                string kingdom = (string)o["kingdom"];

                string speciesKey = (string)o["speciesKey"];
                string genusKey = (string)o["genusKey"];
                string familyKey = (string)o["familyKey"];
                string orderKey = (string)o["orderKey"];
                string classKey = (string)o["classKey"];
                string phylumKey = (string)o["phylumKey"];
                string kingdomKey = (string)o["kingdomKey"];

                if (matchType != "EXACT") return;
                Species sp = null;
                Genus g = null;
                Family fam = null;
                try
                {
                    if ((item is Species) && (rank == "SPECIES"))
                    {
                        sp = (Species)item;
                        sp.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Species");
                        sp.GbifKey = speciesKey;
                        sp.GbifStatus = status;
                        try
                        {
                            sp.NcbiTaxonId = GetNCBI(tname);
                        }
                        catch { }

                        sp.Save();
                    }
                    else
                    if ((item is Genus) && (rank == "GENUS"))
                    {
                        g = os.FindObject<Genus>(new BinaryOperator("Name", genus), true);
                        if (g == null)
                        {
                            g = os.CreateObject<Genus>();
                            g.Name = genus;
                        }
                        g.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Genus");
                        g.GbifKey = genusKey;
                        g.Save();
                    }
                    else
                    if ((item is Family) && (rank == "FAMILY"))
                    {
                        fam = os.FindObject<Family>(new BinaryOperator("Name", family), true);
                        if (fam == null)
                        {
                            fam = os.CreateObject<Family>();
                            fam.Name = family;
                        }
                        g.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null, os, null, "Family");
                        g.GbifKey = familyKey;
                        g.Save();
                    }

                }
                catch { }
                finally
                {
                    if (item.GbifStatus == "ACCEPTED")
                    {
                        item.GbifValidTaxonomicName = null;
                    }
                    if (item.GbifStatus == "SYNONYM")
                    {
                        item.GbifValidTaxonomicName = os.FindObject<TaxonomicName>(
                            CriteriaOperator.And(
                                new BinaryOperator("GbifKey", item.GbifKey),
                                CriteriaOperator.Or(
                                   new BinaryOperator("GbifStatus", "ACCEPTED"),
                                   new BinaryOperator("GbifStatus", "DOUBTFUL")
                                   )
                                )
                                );
                        if (item.GbifValidTaxonomicName == null)
                        {
                            item.GbifValidTaxonomicName = GBIFTaxonomyDownloadByKey(item.GbifKey, item, os);
                        }
                        item.Save();
                    }
                }
            }
        }
        public static void UpdateNcbi(TaxonomicName item, IObjectSpace os)
        {
            string tname = item.FullName.Replace(" ", "+");
            item.NcbiTaxonId = GetNCBI(tname);
            item.Save();
        }
        private static string GetNCBI(string tname)
        {
            using (WebClient wc1 = new WebClient())
            {
                var json1 = wc1.DownloadString(String.Format("http://api.gbif.org/v1/species/search?datasetKey=fab88965-e69d-4491-a04d-e3198b626e52&q={0}&rank=SPECIES", tname));
                var resultObjects = AllChildren(JObject.Parse(json1))
           .First(c => c.Type == JTokenType.Array && c.Path.Contains("results"))
           .Children<JObject>();
                JObject result = resultObjects.First();

                return (string)result["taxonID"];
            }

        }
        public static void ZenodoUploadUnitAttachment(ZenodoClient cl, UnitAttachment at, ZenodoUploadOptions options, Settings settings)
        {
            string file = at.LocalPath;
            string name = Path.GetFileName(file);
            int? deposition_id = null;
            Deposit dep = null;
            /*  if ((at.ZenodoId != null) && (options.Republish == false) && (options.UpdateOnlyMetadata == false))
              {
                  dep = cl.NewDepositVersion(at.ZenodoId.Value);
                  deposition_id = Convert.ToInt32(JObject.Parse(Regex.Unescape(dep.Links.ToString()))["latest_draft"].ToString().Split('/').Last());
              }
              else
              {*/
            //  dep.Title = "EarthCape Image " + at.Name;
            DepositMetadata metadata = new DepositMetadata();
            metadata.Communities = new Collection<Community>();
            metadata.Related_identifiers = new Collection<RelatedIdentifier>();
            RelatedIdentifier id;
            metadata.Title = at.Name;
            if (at.Unit != null)
            {
                id = new RelatedIdentifier();
                string unitlink = String.Format("{0}{2}Unit_DetailView/{1}", settings.Host, at.Unit.Oid, settings.Host.Substring(settings.Host.Length - 1) != "/" ? "/" : "");
                id.Identifier = unitlink;
                id.Relation = "isSupplementTo";
                metadata.Related_identifiers.Add(id);

                id = new RelatedIdentifier();
                string attlink = String.Format("{0}{2}UnitAttachment_DetailView/{1}", settings.Host, at.Oid, settings.Host.Substring(settings.Host.Length - 1) != "/" ? "/" : "");
                id.Identifier = attlink;
                id.Relation = "isIdenticalTo";
                metadata.Related_identifiers.Add(id);

                if (at.Unit.Dataset != null)
                {
                    foreach (DatasetContribution item in at.Unit.Dataset.Contributions)
                    {

                        Author author = new Author();
                        author.Name = item.Person.FullName;
                        author.Affiliation = item.Person.Affiliation;
                        if (String.IsNullOrEmpty(author.Affiliation)) author.Affiliation = "Not specified";
                        metadata.Creators.Add(author);
                    }
                    string gbif = "";
                    if (!String.IsNullOrEmpty(at.Unit.Dataset.GbifCitation))
                    {
                        id = new RelatedIdentifier();
                        id.Identifier = at.Unit.Dataset.GbifCitation;
                        id.Relation = "cites";
                        metadata.Related_identifiers.Add(id);
                        long gbifkey = GbifGetRecByOccurrenceID(at.Unit.Oid.ToString());
                        if (gbifkey > 0)
                        {
                            gbif = String.Format(", <a href=https://www.gbif.org/occurrence/{0} target=_blank>GBIF</a>", gbifkey);
                        }
                    }
                    if (at.Unit.Dataset.Project != null)
                    {
                        Project prj = at.Unit.Dataset.Project;
                        if (!String.IsNullOrEmpty(prj.ZenodoCommunity))
                        {
                            Community com = new Community();
                            com.Id = prj.ZenodoCommunity;
                            metadata.Communities.Add(com);
                        }
                        metadata.Description = String.Format("Project: {0}<br>Dataset: {1}<br>Specimen: {2} (<a href=\"{5}\" target=_blank>original record</a>{6})<br>Taxonomic name: {3}<br>Locality: {4}", at.Unit.Dataset.Project.Name, at.Unit.Dataset.Name, at.Unit.UnitID, at.Unit.TaxonomicName, at.Unit.Locality, unitlink, gbif);
                    }
                    else return;
                    if (!String.IsNullOrEmpty(at.Unit.Dataset.ZenodoConceptDoi))
                    {
                        id = new RelatedIdentifier();
                        id.Identifier = at.Unit.Dataset.ZenodoDoi;
                        id.Relation = "cites";
                        metadata.Related_identifiers.Add(id);
                    }

                }
                else return;
            }
            else return;
            string comm = options.Test ? settings.ZenodoSandboxCommunities : settings.ZenodoCommunities;
            string[] lines = comm.Split(new[] { "\r\n", "\r", "\n", ";", "," }, StringSplitOptions.None);
            if (!String.IsNullOrEmpty(comm))
            {
                foreach (string line in lines)
                {
                    Community com = new Community();
                    com.Id = line;
                    metadata.Communities.Add(com);
                }
            }
            if (metadata.Creators.Count == 0)
            {
                Author author = new Author();
                author.Name = "EarthCape admin";
                author.Affiliation = "EarthCape";
                metadata.Creators.Add(author);
            }
            metadata.Access_right = "open";// DepositMetadataAccess_right.Open;
            metadata.Publication_date = DateTime.Now.ToString("yyyy'-'MM'-'dd");
            metadata.Upload_type = "image";///DepositMetadataUpload_type.Image;
            metadata.Image_type = "photo";
            id = new RelatedIdentifier();
            id.Identifier = "https://earthcape.com";
            id.Relation = "isCompiledBy";
            metadata.Related_identifiers.Add(id);
            if ((at.ZenodoId == null) || ((at.ZenodoUrl.Contains("sandbox.zenodo")) && (options.Test == false)))
            {
                dep = new Deposit();
                dep.Metadata = metadata;
                dep = cl.CreateDeposit(dep);
                deposition_id = dep.Id.Value;
                dep.Submitted = false;
                using (FileStream SourceStream = File.Open(file, FileMode.Open))
                {
                    Zenodo.FileParameter param = new Zenodo.FileParameter(SourceStream, name);
                    DepositionFile df = cl.CreateFile(deposition_id.Value, param, name);
                    if (df != null)
                    {
                        if ((df.Filename.ToLower().Contains("jpg")) || (df.Filename.ToLower().Contains("jpeg")))
                            at.JpegUrl = String.Format("{0}record/{1}/files/{2}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id, df.Filename);
                        at.ZenodoId = deposition_id;
                        at.ZenodoUrl = String.Format("{0}record/{1}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id);
                        at.Save();
                    }
                }
            }
            else
            {
                dep = cl.EditDeposit(at.ZenodoId.Value);
                deposition_id = dep.Id.Value;
                NestedDepositMetadata nd = new NestedDepositMetadata();
                nd.Metadata = metadata;
                cl.PutDeposit(dep.Id.Value, nd);
            }
            if (dep == null) return;


            if (deposition_id != null)
            {
                dep.Submitted = true;
                cl.PublishDeposit(deposition_id.Value);
            }
        }
        public static void ZenodoUploadUnitSpecimen(ZenodoClient cl, string file, Unit unit, Settings settings, PublishSpecimenOptions options)
        {
            int? deposition_id = null;
            Deposit dep = null;
            /*  if ((at.ZenodoId != null) && (options.Republish == false) && (options.UpdateOnlyMetadata == false))
              {
                  dep = cl.NewDepositVersion(at.ZenodoId.Value);
                  deposition_id = Convert.ToInt32(JObject.Parse(Regex.Unescape(dep.Links.ToString()))["latest_draft"].ToString().Split('/').Last());
              }
              else
              {*/
            //  dep.Title = "EarthCape Image " + at.Name;
            DepositMetadata metadata = new DepositMetadata();
            metadata.Communities = new Collection<Community>();
            metadata.Locations = new Collection<Location>();
            metadata.Related_identifiers = new Collection<RelatedIdentifier>();
            RelatedIdentifier id;
            if (unit != null)
            {
                string unitlink = String.Format("{0}{2}Unit_DetailView/{1}", settings.Host, unit.Oid, settings.Host.Substring(settings.Host.Length - 1) != "/" ? "/" : "");
                metadata.Title = String.Format("Specimen: {0} - {1} - {2} - {3} - {4}", unit.UnitID, unit.TaxonomicName, unit.Locality, unit.Dataset.Name, unit.Dataset.Project.Name);
                metadata.Access_right = "open";// DepositMetadataAccess_right.Open;
                metadata.Publication_date = DateTime.Now.ToString("yyyy'-'MM'-'dd");
                metadata.Upload_type = "publication";///DepositMetadataUpload_type.Image;
                metadata.Publication_type = "other";
                //     metadata.Description = unit.FullName;

                id = new RelatedIdentifier();
                id.Identifier = unitlink;
                id.Relation = "isSupplementTo";
                metadata.Related_identifiers.Add(id);


                if (unit.Dataset != null)
                {
                    foreach (DatasetContribution item in unit.Dataset.Contributions)
                    {

                        Author author = new Author();
                        author.Name = item.Person.FullName;
                        author.Affiliation = item.Person.Affiliation;
                        if (String.IsNullOrEmpty(author.Affiliation)) author.Affiliation = "Not specified";
                        metadata.Creators.Add(author);
                    }
                    string gbif = "";
                    if (!String.IsNullOrEmpty(unit.Dataset.GbifCitation))
                    {
                        id = new RelatedIdentifier();
                        id.Identifier = unit.Dataset.GbifCitation;
                        id.Relation = "cites";
                        metadata.Related_identifiers.Add(id);
                        long gbifkey = GbifGetRecByOccurrenceID(unit.Oid.ToString());
                        if (gbifkey > 0)
                        {
                            gbif = String.Format(", <a href=https://www.gbif.org/occurrence/{0} target=_blank>GBIF occurrence</a>", gbifkey);
                        }
                    }
                    string taxname = "";
                    if (unit.TaxonomicName != null)
                    {
                        taxname = unit.TaxonomicName.FullName;
                        if (!String.IsNullOrEmpty(unit.TaxonomicName.GbifKey))
                        {
                            taxname = String.Format("<a href=https://www.gbif.org/species/{0} target=_blank>{1}</a>", unit.TaxonomicName.GbifKey, taxname);
                        }
                    }


                    metadata.Description = String.Format("Project: {0}<br>Dataset: {1}<br>Specimen: {2} (<a href=\"{5}\" target=_blank>EarthCape unit</a>{6})<br>Taxonomic name: {3}<br>Locality: {4}", unit.Dataset.Project.Name, unit.Dataset.Name, unit.UnitID, taxname, unit.Locality, unitlink, gbif);
                    Community com = new Community();
                    com.Id = "nat-hist-museum";
                    metadata.Communities.Add(com);
                    string comm = options.Test ? settings.ZenodoSandboxCommunities : settings.ZenodoCommunities;
                    string[] lines = comm.Split(new[] { "\r\n", "\r", "\n", ";", "," }, StringSplitOptions.None);
                    if (!String.IsNullOrEmpty(comm))
                    {
                        foreach (string line in lines)
                        {
                            com = new Community();
                            com.Id = line;
                            metadata.Communities.Add(com);
                        }
                    }
                    if (!String.IsNullOrEmpty(unit.Dataset.ZenodoConceptDoi))
                    {
                        id = new RelatedIdentifier();
                        id.Identifier = unit.Dataset.ZenodoDoi;
                        id.Relation = "cites";
                        metadata.Related_identifiers.Add(id);
                    }

                }
                else return;
            }
            else return;

            if (metadata.Creators.Count == 0)
            {
                Author author = new Author();
                author.Name = "EarthCape admin";
                author.Affiliation = "EarthCape";
                metadata.Creators.Add(author);
            }
            if (unit.Locality != null)
            {
                Location loc = new Location();
                loc.Latitude = unit.Locality.Latitude;
                loc.Longitude = unit.Locality.Longitude;
                loc.Place = unit.Locality.Name;
                loc.Description = unit.PlaceDetails;
                metadata.Locations.Add(loc);
            }
            id = new RelatedIdentifier();
            id.Identifier = "https://earthcape.com";
            id.Relation = "isCompiledBy";
            metadata.Related_identifiers.Add(id);
            if ((unit.ZenodoId == null) || ((unit.ZenodoUrl.Contains("sandbox.zenodo")) && (options.Test == false)))
            {
                dep = new Deposit();
                dep.Metadata = metadata;
                dep = cl.CreateDeposit(dep);
                deposition_id = dep.Id.Value;
                dep.Submitted = false;
                // add specimen.json
                string spjson = Path.GetTempPath() + @"\specimen.json";
                if (File.Exists(spjson))
                {
                    using (FileStream SourceStream = File.Open(spjson, FileMode.Open))
                    {
                        string name = Path.GetFileName(spjson);
                        Zenodo.FileParameter param = new Zenodo.FileParameter(SourceStream, name);
                        DepositionFile df = cl.CreateFile(deposition_id.Value, param, name);
                    }
                }
                //    unit.zenodo = String.Format("{0}record/{1}/files/{2}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id, df.Filename);
                unit.ZenodoId = deposition_id;
                unit.ZenodoUrl = String.Format("{0}record/{1}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id);


                foreach (UnitAttachment item in unit.Attachments)
                {
                    if (!String.IsNullOrEmpty(item.LocalPath))
                    {
                        using (FileStream SourceStream = File.Open(item.LocalPath, FileMode.Open))
                        {
                            string name = Path.GetFileName(item.LocalPath);
                            Zenodo.FileParameter param = new Zenodo.FileParameter(SourceStream, name);
                            DepositionFile df = cl.CreateFile(deposition_id.Value, param, name);
                            if (df != null)
                            {
                                if ((df.Filename.ToLower().Contains("jpg")) || (df.Filename.ToLower().Contains("jpeg")))
                                    item.JpegUrl = String.Format("{0}record/{1}/files/{2}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id, df.Filename);
                                item.ZenodoId = deposition_id;
                                item.ZenodoUrl = String.Format("{0}record/{1}", options.Test ? zenodoSandboxUrl : zenodoUrl, deposition_id);
                                item.Save();
                            }
                        }
                    }
                }
            }
            else
            {
                dep = cl.EditDeposit(unit.ZenodoId.Value);
                deposition_id = dep.Id.Value;
                NestedDepositMetadata nd = new NestedDepositMetadata();
                nd.Metadata = metadata;
                cl.PutDeposit(dep.Id.Value, nd);
            }
            if (dep == null) return;


            if (deposition_id != null)
            {
                dep.Submitted = true;
                cl.PublishDeposit(deposition_id.Value);
                unit.DOI = (string)JObject.Parse(dep.Metadata.Prereserve_doi.ToString())["doi"];
                unit.Save();
            }
        }
    }
    public class UnitConverter : IBindingRangeValueConverter
    {
        public object ConvertToObject(CellValue value, Type requiredType, int columnIndex)
        {
            if (requiredType == typeof(BasisOfRecord))
            {
                BasisOfRecord w;
                if (Enum.TryParse(value.TextValue, out w)) return w;
                return null;
            }
            else
                return value.TextValue;
        }
        public CellValue TryConvertFromObject(object value)
        {
            if (value != null)
            {
                if (value is DateTime)
                {
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                }
                if (value.GetType().IsEnum)
                {
                    return value.ToString();
                }
                if (value.GetType() == typeof(Guid))
                {
                    return value.ToString();
                }
                else
                    return CellValue.TryCreateFromObject(value);
            }
            else
                return CellValue.TryCreateFromObject(value);
        }
    }

}
