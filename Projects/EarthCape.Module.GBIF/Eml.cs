﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace EarthCape.Module.Integrations.GBIF.EML
{
    [XmlRoot(ElementName = "individualName")]
    public class IndividualName
    {
        [XmlElement(ElementName = "givenName")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "surName")]
        public string SurName { get; set; }
    }

    [XmlRoot(ElementName = "address")]
    public class Address
    {
        [XmlElement(ElementName = "country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "postalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "deliveryPoint")]
        public string DeliveryPoint { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
    }



    public class Person
    {
        [XmlElement(ElementName = "individualName")]
        public IndividualName IndividualName { get; set; }
        [XmlElement(ElementName = "organizationName")]
        public string OrganizationName { get; set; }
        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "email")]
        public string Email { get; set; }
        [XmlElement(ElementName = "positionName")]
        public string PositionName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "onlineUrl")]
        public string OnlineUrl { get; set; }
    }

    [XmlRoot(ElementName = "associatedParty")]
    public class AssociatedParty
    {
        [XmlElement(ElementName = "individualName")]
        public IndividualName IndividualName { get; set; }
        [XmlElement(ElementName = "organizationName")]
        public string OrganizationName { get; set; }
        [XmlElement(ElementName = "positionName")]
        public string PositionName { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "electronicMailAddress")]
        public string ElectronicMailAddress { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
    }

    [XmlRoot(ElementName = "abstract")]
    public class Abstract
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "keywordSet")]
    public class KeywordSet
    {
        [XmlElement(ElementName = "keyword")]
        public string Keyword { get; set; }
        [XmlElement(ElementName = "keywordThesaurus")]
        public string KeywordThesaurus { get; set; }
    }

    [XmlRoot(ElementName = "description")]
    public class Description
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "methodStep")]
    public class MethodStep
    {
        [XmlElement(ElementName = "description")]
        public Description Description { get; set; }
    }

    [XmlRoot(ElementName = "studyExtent")]
    public class StudyExtent
    {
        [XmlElement(ElementName = "description")]
        public Description Description { get; set; }
    }

    [XmlRoot(ElementName = "samplingDescription")]
    public class SamplingDescription
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "sampling")]
    public class Sampling
    {
        [XmlElement(ElementName = "studyExtent")]
        public StudyExtent StudyExtent { get; set; }
        [XmlElement(ElementName = "samplingDescription")]
        public SamplingDescription SamplingDescription { get; set; }
    }

    [XmlRoot(ElementName = "methods")]
    public class Methods
    {
        [XmlElement(ElementName = "methodStep")]
        public MethodStep MethodStep { get; set; }
        [XmlElement(ElementName = "sampling")]
        public Sampling Sampling { get; set; }
    }

    [XmlRoot(ElementName = "personnel")]
    public class Personnel
    {
        [XmlElement(ElementName = "individualName")]
        public IndividualName IndividualName { get; set; }
        [XmlElement(ElementName = "role")]
        public string Role { get; set; }
    }

    [XmlRoot(ElementName = "funding")]
    public class Funding
    {
        [XmlElement(ElementName = "para")]
        public string Para { get; set; }
    }

    [XmlRoot(ElementName = "project")]
    public class Project
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
        [XmlElement(ElementName = "personnel")]
        public Personnel Personnel { get; set; }
        [XmlElement(ElementName = "abstract")]
        public Abstract Abstract { get; set; }
        [XmlElement(ElementName = "funding")]
        public Funding Funding { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "ulink")]
    public class Ulink
    {
        [XmlElement(ElementName = "citetitle")]
        public string Citetitle { get; set; }
        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }
    }

    [XmlRoot(ElementName = "para")]
    public class Para
    {
        [XmlElement(ElementName = "ulink")]
        public Ulink Ulink { get; set; }
        [XmlText]
        public String Text { get; set; }
    }

    [XmlRoot(ElementName = "intellectualRights")]
    public class IntellectualRights
    {
        [XmlElement(ElementName = "para")]
        public Para Para { get; set; }
    }

    [XmlRoot(ElementName = "url")]
    public class Url
    {
        [XmlAttribute(AttributeName = "function")]
        public string Function { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "online")]
    public class Online
    {
        [XmlElement(ElementName = "url")]
        public Url Url { get; set; }
    }

    [XmlRoot(ElementName = "distribution")]
    public class Distribution
    {
        [XmlElement(ElementName = "online")]
        public Online Online { get; set; }
        [XmlAttribute(AttributeName = "scope")]
        public string Scope { get; set; }
    }

    [XmlRoot(ElementName = "boundingCoordinates")]
    public class BoundingCoordinates
    {
        [XmlElement(ElementName = "westBoundingCoordinate")]
        public string WestBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "eastBoundingCoordinate")]
        public string EastBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "northBoundingCoordinate")]
        public string NorthBoundingCoordinate { get; set; }
        [XmlElement(ElementName = "southBoundingCoordinate")]
        public string SouthBoundingCoordinate { get; set; }
    }

    [XmlRoot(ElementName = "geographicCoverage")]
    public class GeographicCoverage
    {
        [XmlElement(ElementName = "geographicDescription")]
        public string GeographicDescription { get; set; }
        [XmlElement(ElementName = "boundingCoordinates")]
        public BoundingCoordinates BoundingCoordinates { get; set; }
    }

    [XmlRoot(ElementName = "coverage")]
    public class Coverage
    {
        [XmlElement(ElementName = "geographicCoverage")]
        public GeographicCoverage GeographicCoverage { get; set; }
    }

    [XmlRoot(ElementName = "maintenance")]
    public class Maintenance
    {
        [XmlElement(ElementName = "description")]
        public Description Description { get; set; }
        [XmlElement(ElementName = "maintenanceUpdateFrequency")]
        public string MaintenanceUpdateFrequency { get; set; }
    }

  

    [XmlRoot(ElementName = "dataset")]
    public class Dataset
    {
        [XmlElement(ElementName = "alternateIdentifier")]
        public List<string> AlternateIdentifier { get; set; }
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
   [XmlElement(ElementName = "creator")]
     public Person Creator { get; set; }
        [XmlElement(ElementName = "metadataProvider")]
        public Person MetadataProvider { get; set; }
        [XmlElement(ElementName = "associatedParty")]
        public AssociatedParty AssociatedParty { get; set; }
        [XmlElement(ElementName = "pubDate")]
        public string PubDate { get; set; }
        [XmlElement(ElementName = "language")]
        public string Language { get; set; }
        [XmlElement(ElementName = "abstract")]
        public Abstract Abstract { get; set; }
        [XmlElement(ElementName = "keywordSet")]
        public KeywordSet KeywordSet { get; set; }
        [XmlElement(ElementName = "methods")]
        public Methods Methods { get; set; }
        [XmlElement(ElementName = "project")]
        public Project Project { get; set; }
        [XmlElement(ElementName = "intellectualRights")]
        public IntellectualRights IntellectualRights { get; set; }
        [XmlElement(ElementName = "distribution")]
        public Distribution Distribution { get; set; }
        [XmlElement(ElementName = "coverage")]
        public Coverage Coverage { get; set; }
        [XmlElement(ElementName = "maintenance")]
        public Maintenance Maintenance { get; set; }
   /*     [XmlElement(ElementName = "contact")]
        public Person Contact { get; set; }*/
    }

    [XmlRoot(ElementName = "gbif")]
    public class Gbif
    {
        [XmlElement(ElementName = "dateStamp")]
        public string DateStamp { get; set; }
        [XmlElement(ElementName = "citation")]
        public string Citation { get; set; }
        [XmlElement(ElementName = "formationPeriod")]
        public string FormationPeriod { get; set; }
    }

    [XmlRoot(ElementName = "metadata")]
    public class Metadata
    {
        [XmlElement(ElementName = "gbif")]
        public Gbif Gbif { get; set; }
    }

    [XmlRoot(ElementName = "additionalMetadata")]
    public class AdditionalMetadata
    {
        [XmlElement(ElementName = "metadata")]
        public Metadata Metadata { get; set; }
    }

    [XmlRoot(ElementName = "eml", Namespace = "eml://ecoinformatics.org/eml-2.1.1")]
    public class Eml
    {
        [XmlElement(ElementName = "dataset")]
        public Dataset Dataset { get; set; }
        [XmlElement(ElementName = "additionalMetadata")]
        public AdditionalMetadata AdditionalMetadata { get; set; }
        [XmlAttribute(AttributeName = "eml", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string _eml { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "packageId")]
        public string PackageId { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "scope")]
        public string Scope { get; set; }
        [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string Lang { get; set; }
    }

}
