using System;
using DevExpress.Xpo;


namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class GbifOccurrenceSearchOptions : XPCustomObject
    {
        public GbifOccurrenceSearchOptions(Session session) : base(session) { }


        private int _Limit=100;
        public int Limit
        {
            get { return _Limit; }
            set { SetPropertyValue<int>(nameof(Limit), ref _Limit, value); }
        }






    }

}
