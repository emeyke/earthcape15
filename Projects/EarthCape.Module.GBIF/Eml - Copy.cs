﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace EarthCape.Module.Integrations.GBIF.EML
{


    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "eml://ecoinformatics.org/eml-2.1.1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "eml://ecoinformatics.org/eml-2.1.1", IsNullable = false)]
    public partial class Eml
    {

        private dataset datasetField;

        private additionalMetadata additionalMetadataField;

        private string packageIdField;

        private string systemField;

        private string scopeField;

        private string langField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public dataset dataset
        {
            get
            {
                return this.datasetField;
            }
            set
            {
                this.datasetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public additionalMetadata additionalMetadata
        {
            get
            {
                return this.additionalMetadataField;
            }
            set
            {
                this.additionalMetadataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string packageId
        {
            get
            {
                return this.packageIdField;
            }
            set
            {
                this.packageIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string system
        {
            get
            {
                return this.systemField;
            }
            set
            {
                this.systemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string scope
        {
            get
            {
                return this.scopeField;
            }
            set
            {
                this.scopeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class dataset
    {

        private List<String> alternateIdentifierField;

        private string titleField;

        private datasetCreator creatorField;

        private datasetMetadataProvider metadataProviderField;

        private datasetAssociatedParty associatedPartyField;

        private System.DateTime pubDateField;

        private string languageField;

        private datasetAbstract abstractField;

        private datasetKeywordSet keywordSetField;

        private datasetMethods methodsField;

        private datasetProject projectField;

        private datasetIntellectualRights intellectualRightsField;

        private datasetDistribution distributionField;

        private datasetCoverage coverageField;

        private datasetMaintenance maintenanceField;

        private datasetContact contactField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("alternateIdentifier")]
        public List<String> alternateIdentifier
        {
            get
            {
                return this.alternateIdentifierField;
            }
            set
            {
                this.alternateIdentifierField = value;
            }
        }

        /// <remarks/>
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public datasetCreator creator
        {
            get
            {
                return this.creatorField;
            }
            set
            {
                this.creatorField = value;
            }
        }

        /// <remarks/>
        public datasetMetadataProvider metadataProvider
        {
            get
            {
                return this.metadataProviderField;
            }
            set
            {
                this.metadataProviderField = value;
            }
        }

        /// <remarks/>
        public datasetAssociatedParty associatedParty
        {
            get
            {
                return this.associatedPartyField;
            }
            set
            {
                this.associatedPartyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime pubDate
        {
            get
            {
                return this.pubDateField;
            }
            set
            {
                this.pubDateField = value;
            }
        }

        /// <remarks/>
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        public datasetAbstract @abstract
        {
            get
            {
                return this.abstractField;
            }
            set
            {
                this.abstractField = value;
            }
        }

        /// <remarks/>
        public datasetKeywordSet keywordSet
        {
            get
            {
                return this.keywordSetField;
            }
            set
            {
                this.keywordSetField = value;
            }
        }

        /// <remarks/>
        public datasetMethods methods
        {
            get
            {
                return this.methodsField;
            }
            set
            {
                this.methodsField = value;
            }
        }

        /// <remarks/>
        public datasetProject project
        {
            get
            {
                return this.projectField;
            }
            set
            {
                this.projectField = value;
            }
        }

        /// <remarks/>
        public datasetIntellectualRights intellectualRights
        {
            get
            {
                return this.intellectualRightsField;
            }
            set
            {
                this.intellectualRightsField = value;
            }
        }

        /// <remarks/>
        public datasetDistribution distribution
        {
            get
            {
                return this.distributionField;
            }
            set
            {
                this.distributionField = value;
            }
        }

        /// <remarks/>
        public datasetCoverage coverage
        {
            get
            {
                return this.coverageField;
            }
            set
            {
                this.coverageField = value;
            }
        }

        /// <remarks/>
        public datasetMaintenance maintenance
        {
            get
            {
                return this.maintenanceField;
            }
            set
            {
                this.maintenanceField = value;
            }
        }

        /// <remarks/>
        public datasetContact contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCreator
    {

        private datasetCreatorIndividualName individualNameField;

        private string organizationNameField;

        private string positionNameField;

        private datasetCreatorAddress addressField;

        private string electronicMailAddressField;

        /// <remarks/>
        public datasetCreatorIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public string positionName
        {
            get
            {
                return this.positionNameField;
            }
            set
            {
                this.positionNameField = value;
            }
        }

        /// <remarks/>
        public datasetCreatorAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string electronicMailAddress
        {
            get
            {
                return this.electronicMailAddressField;
            }
            set
            {
                this.electronicMailAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCreatorIndividualName
    {

        private string givenNameField;

        private string surNameField;

        /// <remarks/>
        public string givenName
        {
            get
            {
                return this.givenNameField;
            }
            set
            {
                this.givenNameField = value;
            }
        }

        /// <remarks/>
        public string surName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCreatorAddress
    {

        private string countryField;

        /// <remarks/>
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMetadataProvider
    {

        private datasetMetadataProviderIndividualName individualNameField;

        private string organizationNameField;

        private string positionNameField;

        private datasetMetadataProviderAddress addressField;

        private string electronicMailAddressField;

        private string onlineUrlField;

        /// <remarks/>
        public datasetMetadataProviderIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public string positionName
        {
            get
            {
                return this.positionNameField;
            }
            set
            {
                this.positionNameField = value;
            }
        }

        /// <remarks/>
        public datasetMetadataProviderAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string electronicMailAddress
        {
            get
            {
                return this.electronicMailAddressField;
            }
            set
            {
                this.electronicMailAddressField = value;
            }
        }

        /// <remarks/>
        public string onlineUrl
        {
            get
            {
                return this.onlineUrlField;
            }
            set
            {
                this.onlineUrlField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMetadataProviderIndividualName
    {

        private string givenNameField;

        private string surNameField;

        /// <remarks/>
        public string givenName
        {
            get
            {
                return this.givenNameField;
            }
            set
            {
                this.givenNameField = value;
            }
        }

        /// <remarks/>
        public string surName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMetadataProviderAddress
    {

        private string countryField;

        /// <remarks/>
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetAssociatedParty
    {

        private datasetAssociatedPartyIndividualName individualNameField;

        private string organizationNameField;

        private string positionNameField;

        private datasetAssociatedPartyAddress addressField;

        private string electronicMailAddressField;

        private string roleField;

        /// <remarks/>
        public datasetAssociatedPartyIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public string positionName
        {
            get
            {
                return this.positionNameField;
            }
            set
            {
                this.positionNameField = value;
            }
        }

        /// <remarks/>
        public datasetAssociatedPartyAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string electronicMailAddress
        {
            get
            {
                return this.electronicMailAddressField;
            }
            set
            {
                this.electronicMailAddressField = value;
            }
        }

        /// <remarks/>
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetAssociatedPartyIndividualName
    {

        private string givenNameField;

        private string surNameField;

        /// <remarks/>
        public string givenName
        {
            get
            {
                return this.givenNameField;
            }
            set
            {
                this.givenNameField = value;
            }
        }

        /// <remarks/>
        public string surName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetAssociatedPartyAddress
    {

        private string countryField;

        /// <remarks/>
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetAbstract
    {

        private string paraField;

        /// <remarks/>
        public string para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetKeywordSet
    {

        private string keywordField;

        private string keywordThesaurusField;

        /// <remarks/>
        public string keyword
        {
            get
            {
                return this.keywordField;
            }
            set
            {
                this.keywordField = value;
            }
        }

        /// <remarks/>
        public string keywordThesaurus
        {
            get
            {
                return this.keywordThesaurusField;
            }
            set
            {
                this.keywordThesaurusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethods
    {

        private datasetMethodsMethodStep methodStepField;

        private datasetMethodsSampling samplingField;

        /// <remarks/>
        public datasetMethodsMethodStep methodStep
        {
            get
            {
                return this.methodStepField;
            }
            set
            {
                this.methodStepField = value;
            }
        }

        /// <remarks/>
        public datasetMethodsSampling sampling
        {
            get
            {
                return this.samplingField;
            }
            set
            {
                this.samplingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsMethodStep
    {

        private datasetMethodsMethodStepDescription descriptionField;

        /// <remarks/>
        public datasetMethodsMethodStepDescription description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsMethodStepDescription
    {

        private string paraField;

        /// <remarks/>
        public string para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsSampling
    {

        private datasetMethodsSamplingStudyExtent studyExtentField;

        private datasetMethodsSamplingSamplingDescription samplingDescriptionField;

        /// <remarks/>
        public datasetMethodsSamplingStudyExtent studyExtent
        {
            get
            {
                return this.studyExtentField;
            }
            set
            {
                this.studyExtentField = value;
            }
        }

        /// <remarks/>
        public datasetMethodsSamplingSamplingDescription samplingDescription
        {
            get
            {
                return this.samplingDescriptionField;
            }
            set
            {
                this.samplingDescriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsSamplingStudyExtent
    {

        private datasetMethodsSamplingStudyExtentDescription descriptionField;

        /// <remarks/>
        public datasetMethodsSamplingStudyExtentDescription description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsSamplingStudyExtentDescription
    {

        private string paraField;

        /// <remarks/>
        public string para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMethodsSamplingSamplingDescription
    {

        private string paraField;

        /// <remarks/>
        public string para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetProject
    {

        private string titleField;

        private datasetProjectPersonnel personnelField;

        private datasetProjectAbstract abstractField;

        private datasetProjectFunding fundingField;

        private string idField;

        /// <remarks/>
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public datasetProjectPersonnel personnel
        {
            get
            {
                return this.personnelField;
            }
            set
            {
                this.personnelField = value;
            }
        }

        /// <remarks/>
        public datasetProjectAbstract @abstract
        {
            get
            {
                return this.abstractField;
            }
            set
            {
                this.abstractField = value;
            }
        }

        /// <remarks/>
        public datasetProjectFunding funding
        {
            get
            {
                return this.fundingField;
            }
            set
            {
                this.fundingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetProjectPersonnel
    {

        private datasetProjectPersonnelIndividualName individualNameField;

        private string roleField;

        /// <remarks/>
        public datasetProjectPersonnelIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetProjectPersonnelIndividualName
    {

        private string givenNameField;

        private string surNameField;

        /// <remarks/>
        public string givenName
        {
            get
            {
                return this.givenNameField;
            }
            set
            {
                this.givenNameField = value;
            }
        }

        /// <remarks/>
        public string surName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetProjectAbstract
    {

        private object paraField;

        /// <remarks/>
        public object para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetProjectFunding
    {

        private object paraField;

        /// <remarks/>
        public object para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetIntellectualRights
    {

        private datasetIntellectualRightsPara paraField;

        /// <remarks/>
        public datasetIntellectualRightsPara para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetIntellectualRightsPara
    {

        private datasetIntellectualRightsParaUlink ulinkField;

        private List<String> textField;

        /// <remarks/>
        public datasetIntellectualRightsParaUlink ulink
        {
            get
            {
                return this.ulinkField;
            }
            set
            {
                this.ulinkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public List<String> Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetIntellectualRightsParaUlink
    {

        private string citetitleField;

        private string urlField;

        /// <remarks/>
        public string citetitle
        {
            get
            {
                return this.citetitleField;
            }
            set
            {
                this.citetitleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetDistribution
    {

        private datasetDistributionOnline onlineField;

        private string scopeField;

        /// <remarks/>
        public datasetDistributionOnline online
        {
            get
            {
                return this.onlineField;
            }
            set
            {
                this.onlineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string scope
        {
            get
            {
                return this.scopeField;
            }
            set
            {
                this.scopeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetDistributionOnline
    {

        private datasetDistributionOnlineUrl urlField;

        /// <remarks/>
        public datasetDistributionOnlineUrl url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetDistributionOnlineUrl
    {

        private string functionField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string function
        {
            get
            {
                return this.functionField;
            }
            set
            {
                this.functionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCoverage
    {

        private datasetCoverageGeographicCoverage geographicCoverageField;

        /// <remarks/>
        public datasetCoverageGeographicCoverage geographicCoverage
        {
            get
            {
                return this.geographicCoverageField;
            }
            set
            {
                this.geographicCoverageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCoverageGeographicCoverage
    {

        private string geographicDescriptionField;

        private datasetCoverageGeographicCoverageBoundingCoordinates boundingCoordinatesField;

        /// <remarks/>
        public string geographicDescription
        {
            get
            {
                return this.geographicDescriptionField;
            }
            set
            {
                this.geographicDescriptionField = value;
            }
        }

        /// <remarks/>
        public datasetCoverageGeographicCoverageBoundingCoordinates boundingCoordinates
        {
            get
            {
                return this.boundingCoordinatesField;
            }
            set
            {
                this.boundingCoordinatesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetCoverageGeographicCoverageBoundingCoordinates
    {

        private short westBoundingCoordinateField;

        private short eastBoundingCoordinateField;

        private byte northBoundingCoordinateField;

        private sbyte southBoundingCoordinateField;

        /// <remarks/>
        public short westBoundingCoordinate
        {
            get
            {
                return this.westBoundingCoordinateField;
            }
            set
            {
                this.westBoundingCoordinateField = value;
            }
        }

        /// <remarks/>
        public short eastBoundingCoordinate
        {
            get
            {
                return this.eastBoundingCoordinateField;
            }
            set
            {
                this.eastBoundingCoordinateField = value;
            }
        }

        /// <remarks/>
        public byte northBoundingCoordinate
        {
            get
            {
                return this.northBoundingCoordinateField;
            }
            set
            {
                this.northBoundingCoordinateField = value;
            }
        }

        /// <remarks/>
        public sbyte southBoundingCoordinate
        {
            get
            {
                return this.southBoundingCoordinateField;
            }
            set
            {
                this.southBoundingCoordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMaintenance
    {

        private datasetMaintenanceDescription descriptionField;

        private string maintenanceUpdateFrequencyField;

        /// <remarks/>
        public datasetMaintenanceDescription description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string maintenanceUpdateFrequency
        {
            get
            {
                return this.maintenanceUpdateFrequencyField;
            }
            set
            {
                this.maintenanceUpdateFrequencyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetMaintenanceDescription
    {

        private object paraField;

        /// <remarks/>
        public object para
        {
            get
            {
                return this.paraField;
            }
            set
            {
                this.paraField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetContact
    {

        private datasetContactIndividualName individualNameField;

        private string organizationNameField;

        private string positionNameField;

        private datasetContactAddress addressField;

        private string electronicMailAddressField;

        private string onlineUrlField;
        private string phoneField;

        /// <remarks/>
        public datasetContactIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public string positionName
        {
            get
            {
                return this.positionNameField;
            }
            set
            {
                this.positionNameField = value;
            }
        }

        /// <remarks/>
        public datasetContactAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string electronicMailAddress
        {
            get
            {
                return this.electronicMailAddressField;
            }
            set
            {
                this.electronicMailAddressField = value;
            }
        }

        /// <remarks/>
        public string onlineUrl
        {
            get
            {
                return this.onlineUrlField;
            }
            set
            {
                this.onlineUrlField = value;
            }
        }
        public string phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetContactIndividualName
    {

        private string givenNameField;

        private string surNameField;

        /// <remarks/>
        public string givenName
        {
            get
            {
                return this.givenNameField;
            }
            set
            {
                this.givenNameField = value;
            }
        }

        /// <remarks/>
        public string surName
        {
            get
            {
                return this.surNameField;
            }
            set
            {
                this.surNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class datasetContactAddress
    {

        private string countryField;

        /// <remarks/>
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
        private string cityField;

        /// <remarks/>
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }
        private string postalCodeField;

        /// <remarks/>
        public string postalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }
        private string deliveryPointField;

        /// <remarks/>
        public string deliveryPoint
        {
            get
            {
                return this.deliveryPointField;
            }
            set
            {
                this.deliveryPointField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class additionalMetadata
    {

        private additionalMetadataMetadata metadataField;

        /// <remarks/>
        public additionalMetadataMetadata metadata
        {
            get
            {
                return this.metadataField;
            }
            set
            {
                this.metadataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class additionalMetadataMetadata
    {

        private additionalMetadataMetadataGbif gbifField;

        /// <remarks/>
        public additionalMetadataMetadataGbif gbif
        {
            get
            {
                return this.gbifField;
            }
            set
            {
                this.gbifField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class additionalMetadataMetadataGbif
    {

        private System.DateTime dateStampField;

        private string citationField;

        private string formationPeriodField;

        /// <remarks/>
        public System.DateTime dateStamp
        {
            get
            {
                return this.dateStampField;
            }
            set
            {
                this.dateStampField = value;
            }
        }

        /// <remarks/>
        public string citation
        {
            get
            {
                return this.citationField;
            }
            set
            {
                this.citationField = value;
            }
        }

        /// <remarks/>
        public string formationPeriod
        {
            get
            {
                return this.formationPeriodField;
            }
            set
            {
                this.formationPeriodField = value;
            }
        }
    }



}
