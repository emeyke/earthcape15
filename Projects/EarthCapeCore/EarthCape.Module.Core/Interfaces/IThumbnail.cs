using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace EarthCape.Interfaces
{
    public interface IThumbnail
    {
        Guid Oid { get; }
        Image Thumbnail { get; }
        string Caption { get; }
    }
}
