using System;
using System.Collections.Generic;
using System.Text;
using EarthCape.Module;
using DevExpress.Xpo;

namespace EarthCape.Interfaces
{
    interface IIdentifiable
    {
        TaxonomicName TaxonomicName { get; set; }
        }
}
