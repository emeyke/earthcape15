using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public interface IName : IBaseObject
    {
        string Name { get; }

    }
    public interface IBaseObject
    {
        Guid Oid { get; }
        String Comment { get; }
    }
}
