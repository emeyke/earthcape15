using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Interfaces
{
    interface IDateTime
    {
        DateTime DateTimeBegins { get; set;}
        DateTime EndOns { get; set;}
        string DateTimeText { get; set;}
        string TimeZone { get; set;}
        int DayBegins { get; set;}
        int DayEnds { get; set;}
        int MonthBegins { get; set;}
        int MonthEnds { get; set;}
        int YearBegins { get; set;}
        int YearEnds { get; set;}
        bool PeriodExplicit { get; set; }
    }
}
