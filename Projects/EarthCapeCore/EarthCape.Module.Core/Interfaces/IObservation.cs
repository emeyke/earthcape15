using System;
using System.Collections.Generic;
using System.Text;
using EarthCape.Module;
using EarthCape.Module.Core;

namespace EarthCape.Interfaces
{
    public interface IUnit:IWKT
    {
        TaxonomicName TaxonomicName { get;set;}
        Locality Place { get;set;}
        DateTime? Date { get;set;}
        DateTime? EndOn { get;set;}
        Double Quantity { get;set;}
        String QuantityUnit { get; set; }
    }
}
