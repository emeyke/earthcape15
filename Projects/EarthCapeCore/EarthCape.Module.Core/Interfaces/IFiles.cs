﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Xpo;
using EarthCape.Module;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Core
{
    public interface IFiles:IBaseObject
    {
        XPCollection<FileItem> Files { get; }
    }

}
