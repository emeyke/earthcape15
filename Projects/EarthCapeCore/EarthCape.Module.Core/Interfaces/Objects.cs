using DevExpress.Xpo;
using EarthCape.Module;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public interface IDatasetObject
    {
        Dataset Dataset { get; set; }
    }

}
