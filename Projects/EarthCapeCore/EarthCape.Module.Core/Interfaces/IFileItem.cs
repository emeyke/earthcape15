using System;
using System.Collections.Generic;
using System.Text;
using EarthCape.Module.Core;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;

namespace EarthCape.Module.Core
{
    public interface IFileItem
    {
        IFileData File { get; set; }
    
    }
}
