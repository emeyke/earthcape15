using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Xpo;
using EarthCape.Module;
using DevExpress.Persistent.Base.General;

namespace EarthCape.Interfaces
{
    public interface IClassificationItemObject : IDataObject
    {
        XPCollection<ClassificationItem> ClassificationItems { get; }
    }
}
