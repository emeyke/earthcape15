using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Xpo;
using EarthCape.Module;
using DevExpress.Persistent.Base.General;

namespace EarthCape.Interfaces
{
  /*  public interface IWorkItemData
    {
        WorkItem WorkItem { get; }
    }
    public interface IWorkItemsData
    {
        XPCollection<WorkItem> WorkItems { get; }
    }*/
    public interface IClassifiedObject : ICategorizedItem
    {
        IClassificationItem ClassificationItem { get; set; }
        BaseObject ObjectClassified { get; set; }
    }

}
