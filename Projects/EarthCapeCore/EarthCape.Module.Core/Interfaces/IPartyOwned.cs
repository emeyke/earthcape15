using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Xpo;
using EarthCape.Module;

namespace EarthCape.Interfaces
{
    public interface IPartyOwned
    {
        Party Owner { get; set; }
        Party Holder { get; set; }

    }   
}
