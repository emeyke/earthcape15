﻿using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public interface IUnitData
    {
        Unit Unit { get; set; }
    }
}
