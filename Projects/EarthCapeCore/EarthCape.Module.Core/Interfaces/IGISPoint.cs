using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Interfaces
{
    interface IPlacePoint
    {
        Double X { get; set;}
        Double Y { get; set;}
        Double Altitude1 { get; set;}
        Double Altitude2 { get; set; }
    }
}
