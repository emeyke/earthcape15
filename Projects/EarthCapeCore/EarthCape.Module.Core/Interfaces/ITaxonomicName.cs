using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public interface ITaxonomicNameObject : IBaseObject
    {
        TaxonomicName TaxonomicName { get; }

    }
    
}
