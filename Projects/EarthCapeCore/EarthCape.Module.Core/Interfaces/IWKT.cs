﻿using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public interface IWKT : IName
    {
        // string WKT { get; set; }
        /*CSProjection CSProjection { get; set; }
        CSProjectionType CSProjectionType { get; set; }
        Ellipsoid Ellipsoid { get; set; }
        Datum Datum { get; set; }
        double Azimuth { get; set; }
        double CentralMeridian { get; set; }
        double FalseEasting { get; set; }
        double FalseNorthing { get; set; }
        double LatitudeOfCenter { get; set; }
        double LatitudeOfOrigin { get; set; }
        double LatitudeOfPoint_1 { get; set; }
        double LatitudeOfPoint_2 { get; set; }
        double LongitudeOfCenter { get; set; }
        double LongitudeOfPoint_1 { get; set; }
        double LongitudeOfPoint_2 { get; set; }
        double ScaleFactor { get; set; }
        double StandardParallel_1 { get; set; }
        double StandardParallel_2 { get; set; }
        double XScale { get; set; }
        double XYPlaneRotation { get; set; }
        double YScale { get; set; }
        int Zone { get; set; }*/
        //  string GetWKT();
        string WKT { get; set; }

        new Guid Oid { get; }
        //MapWindow MapWindow { get; }
        int EPSG { get; set; }
        Decimal? Centroid_X { get; set; }
        Decimal? Centroid_Y { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        double Area { get; set; }
        double Length { get; set; }
        //   double BBoxWest { get; set; }
        //  double BBoxEast { get; set; }
        // double BBoxSouth { get; set; }
        // double BBoxNorth { get; set; }
        //  double Length { get; set; }
        void Save();
        string CoordSource { get; set; }
        int? CoordAccuracy { get; set; }
        GeometryType GeometryType { get; set; }
}
}
