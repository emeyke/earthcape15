﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Interfaces
{
    public interface IGuidOid
    {
        Guid Oid { get; }
    }
}
