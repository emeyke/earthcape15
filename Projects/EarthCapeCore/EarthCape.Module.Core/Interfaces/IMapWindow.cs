using System;
using System.Collections.Generic;
using System.Text;
using EarthCape.Module;

namespace EarthCape.Interfaces
{
    public interface IMapWindow
    {
        MapWindow MapWindow {get;}
        string ProjectConfig { get; set; }
    }
}
