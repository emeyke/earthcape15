using System;

namespace EarthCape.Module.Core
{
    public enum RefType
    {
        JournalArticle,
        Book
    }
}
