#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.


using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;
using System;
using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Logistics
{
    [DocumentationAttribute("Information request. Treated as a Task object assigned to a contact.")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    [DefaultClassOptions]
    public class InformationRequest : PersonalTask
    {


        private string _RequesterName;
        [Size(255)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string RequesterName
        {
            get { return _RequesterName; }
            set { SetPropertyValue<string>(nameof(RequesterName), ref _RequesterName, value); }
        }


        private string _RequesterOrganization;
        [Size(255)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string RequesterOrganization
        {
            get { return _RequesterOrganization; }
            set { SetPropertyValue<string>(nameof(RequesterOrganization), ref _RequesterOrganization, value); }
        }


        private string _RequesterEmail;
        [Size(255)]
        [RuleRegularExpression(null, "InfoRequestContext", EarthCapeModule.EmailPattern)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string RequesterEmail
        {
            get { return _RequesterEmail; }
            set { SetPropertyValue<string>(nameof(RequesterEmail), ref _RequesterEmail, value); }
        }


        [Association("InfoRequestUnits")]
        public XPCollection<Unit> Units
        {
            get { return GetCollection<Unit>(nameof(Units)); }
        }



        public InformationRequest(Session session)
            : base(session)
        {
              }
  


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
              
            }
         }
 
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
         
           
        }




    }
}
