﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using Xpand.Persistent.Base.General;

namespace EarthCape.Module.Logistics
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
     public class Accession : MaterialTransfer, IUnits, ISupportSequenceObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Accession(Session session)
            : base(session)
        {
        }

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Prefix
        {
            get
            {

                return Year.ToString();
            }
           
        }

        private long _Sequence;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ImmediatePostData(true)]
        public long Sequence
        {
            get { return _Sequence; }
            set { SetPropertyValue<long>(nameof(Sequence), ref _Sequence, value); }
        }
        [Association("AccessionProcess"),Aggregated]
        public XPCollection<AccessionStageProcess> Process
        {
            get { return GetCollection<AccessionStageProcess>(nameof(Process)); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Year = DateTime.Now.Year;
            SequenceGenerator.GenerateSequence(this);
            if (this.Sequence == 0)
                SequenceGenerator.GenerateSequence(this);
            Name =String.Format("{0}.{1}", Prefix,Sequence);
            //   this.Direction = TransferDirection.Incoming;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Association("AccessionUnits")]
        public XPCollection<Unit> Units
        {
            get { return GetCollection<Unit>(nameof(Units)); }
        }
          private AccessionType _Type;
        public AccessionType Type
        {
            get { return _Type; }
            set { SetPropertyValue<AccessionType>(nameof(Type), ref _Type, value); }
        }
   
        private AccessionStatus _Status = AccessionStatus.NotStarted;
        public AccessionStatus Status
        {
            get { return _Status; }
            set { SetPropertyValue<AccessionStatus>(nameof(Status), ref _Status, value); }
        }


        private AccessionStage _Stage;
        [ImmediatePostData]
        public AccessionStage Stage
        {
            get { return _Stage; }
            set { SetPropertyValue<AccessionStage>(nameof(Stage), ref _Stage, value);
                if (!IsLoading && value != null)
                {
                    if (Session.FindObject<AccessionStageProcess>(CriteriaOperator.And(new BinaryOperator("Stage.Oid", Stage.Oid), new BinaryOperator("Accession.Oid", this.Oid))) == null)
                    {
                        AccessionStageProcess pr = new AccessionStageProcess(Session);
                        pr.Stage = Stage;
                        pr.Accession = this;
                        pr.Save();
                    }
                }
            }
        }


        private DateTime _Date  ;
        public DateTime Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime>(nameof(Date), ref _Date, value); }
        }

        private DateTime _DateAcknowledged;
        public DateTime DateAcknowledged
        {
            get { return _DateAcknowledged; }
            set { SetPropertyValue<DateTime>(nameof(DateAcknowledged), ref _DateAcknowledged, value); }
        }

        private string _DateVerbatim;
        public string DateVerbatim
        {
            get { return _DateVerbatim; }
            set { SetPropertyValue<string>(nameof(DateVerbatim), ref _DateVerbatim, value); }
        }

        private TaxonomicName _TaxonomicName;
        public TaxonomicName TaxonomicName
        {
            get { return _TaxonomicName; }
            set { SetPropertyValue<TaxonomicName>(nameof(TaxonomicName), ref _TaxonomicName, value); }
        }

        private Locality _Locality;
        public Locality Locality
        {
            get { return _Locality; }
            set { SetPropertyValue<Locality>(nameof(Locality), ref _Locality, value); }
        }


        private Store _Store;
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }



        private string _Geography;
        [Size(SizeAttribute.Unlimited)]
        public string Geography
        {
            get { return _Geography; }
            set { SetPropertyValue<string>(nameof(Geography), ref _Geography, value); }
        }

        private string _TaxonomyComment;
        [Size(SizeAttribute.Unlimited)]
        public string TaxonomyComment
        {
            get { return _TaxonomyComment; }
            set { SetPropertyValue<string>(nameof(TaxonomyComment), ref _TaxonomyComment, value); }
        }




    }
}