﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.Logistics
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
     public class Loan : MaterialTransfer
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Loan(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
              // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Association("LoanOutUnits")]
        public XPCollection<Unit> Units
        {
            get { return GetCollection<Unit>(nameof(Units)); }
        }

        private DateTime _OriginalDueDate;
        public DateTime OriginalDueDate
        {
            get { return _OriginalDueDate; }
            set { SetPropertyValue<DateTime>(nameof(OriginalDueDate), ref _OriginalDueDate, value); }
        }


        private bool _Returned;
        public bool Returned
        {
            get { return _Returned; }
            set { SetPropertyValue<bool>(nameof(Returned), ref _Returned, value); }
        }
        private TransferStatus _Status;
        public TransferStatus Status
        {
            get { return _Status; }
            set { SetPropertyValue<TransferStatus>(nameof(Status), ref _Status, value); }
        }

            private TransferDirection _Direction;
             public TransferDirection Direction
             {
                 get { return _Direction; }
                 set { SetPropertyValue<TransferDirection>(nameof(Direction), ref _Direction, value); }
             }


        private DateTime _ReturnDate;
        public DateTime ReturnDate
        {
            get { return _ReturnDate; }
            set { SetPropertyValue<DateTime>(nameof(ReturnDate), ref _ReturnDate, value); }
        }

        private DateTime _DueDate;
        public DateTime DueDate
        {
            get { return _DueDate; }
            set { SetPropertyValue<DateTime>(nameof(DueDate), ref _DueDate, value); }
        }











    }
}