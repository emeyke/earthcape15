using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl; 
using EarthCape.Module.Core;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp;

namespace EarthCape.Module.Logistics
{
    public class AccessionStage : BaseObject
    {

        private string _Description;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }


        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        public AccessionStage(Session session) : base(session) { }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }

        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }

        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }

        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }

  

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;

            }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


    }


}
