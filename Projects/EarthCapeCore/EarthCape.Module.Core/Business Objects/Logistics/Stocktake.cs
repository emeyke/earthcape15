#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.


using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Lab;
using System;

using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
      [DocumentationAttribute("Stocktaking record")]
    public abstract class Stocktake : BaseObject,IProjectObject
    {

        private StocktakeTask _Task;
        [Association("TaskStockTakes")]
        public StocktakeTask Task
        {
            get { return _Task; }
            set { SetPropertyValue<StocktakeTask>(nameof(Task), ref _Task, value); }
        }

        private bool _Done=false;
        public bool Done
        {
            get { return _Done; }
            set { SetPropertyValue<bool>(nameof(Done), ref _Done, value); }
        }



        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }

        private LocalityVisit _LocalityVisit;
        public LocalityVisit LocalityVisit
        {
            get { return _LocalityVisit; }
            set { SetPropertyValue<LocalityVisit>(nameof(LocalityVisit), ref _LocalityVisit, value); }
        }


        private Store _Store;
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }



        private int _Count;
        public int Count
        {
            get { return _Count; }
            set { SetPropertyValue<int>(nameof(Count), ref _Count, value); }
        }

        private string _CountUnit;
        public string CountUnit
        {
            get { return _CountUnit; }
            set { SetPropertyValue<string>(nameof(CountUnit), ref _CountUnit, value); }
        }

        private string _QuantityUnit;
        public string QuantityUnit
        {
            get { return _QuantityUnit; }
            set { SetPropertyValue<string>(nameof(QuantityUnit), ref _QuantityUnit, value); }
        }



        private double _Quantity;
        public double Quantity
        {
            get { return _Quantity; }
            set { SetPropertyValue<double>(nameof(Quantity), ref _Quantity, value); }
        }







        private Image _Image;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        public Image Image
        {
            get { return _Image; }
            set { SetPropertyValue<Image>(nameof(Image), ref _Image, value); }
        }





        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }


        private DateTime? _Date;
        public DateTime? Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime?>(nameof(Date), ref _Date, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public Stocktake(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;

            }
        }

        private StocktakeCondition _Condition;
        public StocktakeCondition Condition
        {
            get { return _Condition; }
            set { SetPropertyValue<StocktakeCondition>(nameof(Condition), ref _Condition, value); }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


    }
}
