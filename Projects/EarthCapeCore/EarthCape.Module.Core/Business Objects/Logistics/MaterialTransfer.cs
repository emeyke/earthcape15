using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl; 
using EarthCape.Module.Core;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Logistics
{
    public abstract class MaterialTransfer : BaseObject, IName, IProjectObject
    {
    

        private Project _Project;
        [Association("Project-Transfers")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private int _Year;
        public int Year
        {
            get { return _Year; }
            set { SetPropertyValue<int>(nameof(Year), ref _Year, value); }
        }




        public MaterialTransfer(Session session) : base(session) { }


        private string _SendingPerson;
        public string SendingPerson
        {
            get { return _SendingPerson; }
            set { SetPropertyValue<string>(nameof(SendingPerson), ref _SendingPerson, value); }
        }

        private PersonCore _ReceivingContact;
        [Association("PersonTransfers")]
        public PersonCore ReceivingContact
        {
            get { return _ReceivingContact; }
            set { SetPropertyValue<PersonCore>(nameof(ReceivingContact), ref _ReceivingContact, value); }
        }


        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }




        private string _TransferPurpose;
        public string TransferPurpose
        {
            get { return _TransferPurpose; }
            set { SetPropertyValue<string>(nameof(TransferPurpose), ref _TransferPurpose, value); }
        }


        private string _Duration;
        public string Duration
        {
            get { return _Duration; }
            set { SetPropertyValue<string>(nameof(Duration), ref _Duration, value); }
        }

        private string _InsuranceValue;
        public string InsuranceValue
        {
            get { return _InsuranceValue; }
            set { SetPropertyValue<string>(nameof(InsuranceValue), ref _InsuranceValue, value); }
        }

        private string _ShippingMethod;
        public string ShippingMethod
        {
            get { return _ShippingMethod; }
            set { SetPropertyValue<string>(nameof(ShippingMethod), ref _ShippingMethod, value); }
        }


        private string _Condition;
        public string Condition
        {
            get { return _Condition; }
            set { SetPropertyValue<string>(nameof(Condition), ref _Condition, value); }
        }


      


        private string _Handler;
        public string Handler
        {
            get { return _Handler; }
            set { SetPropertyValue<string>(nameof(Handler), ref _Handler, value); }
        }



        private Address _AddressForward;
        public Address AddressForward
        {
            get { return _AddressForward; }
            set { SetPropertyValue<Address>(nameof(AddressForward), ref _AddressForward, value); }
        }

        private Address _AddressDestination;
        public Address AddressDestination
        {
            get { return _AddressDestination; }
            set { SetPropertyValue<Address>(nameof(AddressDestination), ref _AddressDestination, value); }
        }


      
   

        private DateTime _SendingDate;
        public DateTime SendingDate
        {
            get { return _SendingDate; }
            set { SetPropertyValue<DateTime>(nameof(SendingDate), ref _SendingDate, value); }
        }


        private DateTime _ReceivingDate;
        public DateTime ReceivingDate
        {
            get { return _ReceivingDate; }
            set { SetPropertyValue<DateTime>(nameof(ReceivingDate), ref _ReceivingDate, value); }
        }

        private string _TransferIdSent;
        public string TransferIdSent
        {
            get { return _TransferIdSent; }
            set { SetPropertyValue<string>(nameof(TransferIdSent), ref _TransferIdSent, value); }
        }



        private string _TransferIdReceived;
        public string TransferIdReceived
        {
            get { return _TransferIdReceived; }
            set { SetPropertyValue<string>(nameof(TransferIdReceived), ref _TransferIdReceived, value); }
        }



        private int _QuantitySent;
        public int QuantitySent
        {
            get { return _QuantitySent; }
            set { SetPropertyValue<int>(nameof(QuantitySent), ref _QuantitySent, value); }
        }



        private int _QuantityReceived;
        public int QuantityReceived
        {
            get { return _QuantityReceived; }
            set { SetPropertyValue<int>(nameof(QuantityReceived), ref _QuantityReceived, value); }
        }


      


        [Association("Transfer-Shipments")]
        [Aggregated]
        public XPCollection<Shipment> Shipments
        {
            get
            {
                return GetCollection<Shipment>("Shipments");
            }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }

        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }

        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }

        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }



        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;

            }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

    }


}
