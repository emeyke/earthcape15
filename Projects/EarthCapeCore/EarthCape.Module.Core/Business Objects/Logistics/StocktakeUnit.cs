#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.


using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Lab;
using System;

using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DocumentationAttribute("Unit Stocktaking record")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    [DefaultClassOptions]
    public class StocktakeUnit : Stocktake
    {

        private Unit _Unit;
        [Association("UnitStocktaking")]
        [ImmediatePostData]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }


        public StocktakeUnit(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
             }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //todo doesnt work. why??
            if (this.Unit != null)
            {
                Store = Unit.Store;
                Quantity = Unit.Quantity;
                QuantityUnit = Unit.QuantityUnit;
                Condition = Unit.Condition;
                Count = Unit.Count;
                CountUnit = Unit.CountUnit;
            }
           
        }

     

       



        




    }
}
