
using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Logistics
{
    public class Shipment : BaseObject
    {

        public Shipment(Session session) : base(session) { }

        private MaterialTransfer _Loan;
        [Association("Transfer-Shipments")]
        public MaterialTransfer Loan
        {
            get { return _Loan; }
            set { SetPropertyValue<MaterialTransfer>(nameof(Loan), ref _Loan, value); }
        }



        private bool _IsPartial;
        public bool IsPartial
        {
            get { return _IsPartial; }
            set { SetPropertyValue<bool>(nameof(IsPartial), ref _IsPartial, value); }
        }



        private string _Quantity;
        public string Quantity
        {
            get { return _Quantity; }
            set { SetPropertyValue<string>(nameof(Quantity), ref _Quantity, value); }
        }



        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }



        private bool _LiveMaterial;
        public bool LiveMaterial
        {
            get { return _LiveMaterial; }
            set { SetPropertyValue<bool>(nameof(LiveMaterial), ref _LiveMaterial, value); }
        }


        private double _Weight;
        public double Weight
        {
            get { return _Weight; }
            set { SetPropertyValue<double>(nameof(Weight), ref _Weight, value); }
        }

        private string _Method;
        public string Method
        {
            get { return _Method; }
            set { SetPropertyValue<string>(nameof(Method), ref _Method, value); }
        }


        private string _ShipmentNumber;
        public string ShipmentNumber
        {
            get { return _ShipmentNumber; }
            set { SetPropertyValue<string>(nameof(ShipmentNumber), ref _ShipmentNumber, value); }
        }

        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime>(nameof(Date), ref _Date, value); }
        }


        private string _SippingAddress;
        public string SippingAddress
        {
            get { return _SippingAddress; }
            set { SetPropertyValue<string>(nameof(SippingAddress), ref _SippingAddress, value); }
        }

        private double _EnsuredForAmount;
        public double EnsuredForAmount
        {
            get { return _EnsuredForAmount; }
            set { SetPropertyValue<double>(nameof(EnsuredForAmount), ref _EnsuredForAmount, value); }
        }


    }
}
