using System;

namespace EarthCape.Module.Logistics
{

    public enum AccessionStatus
    {
        NotStarted,
        InProcess,
        Complete
    }
    public enum AccessionType
    {
        Exchange,
        Gift,
        Purchase,
        Contract,
        Bequest,
        FieldCollection
    }
    public enum TransferDirection
    {
        Incoming,
        Outgoing
    }
}
