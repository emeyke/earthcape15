﻿// Developer Express Code Central Example:
// How to store files in the file system, instead of the database.
// 
// See Also:
// File Attachments Module Overview
// (ms-help://Xpand.XAF/CustomDocument2781.htm)
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E965

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCapePro.Module;
using System;
using System.IO;

namespace EarthCape.Module.Core
{
    [Persistent]
    public class AttachmentStorageAmazonS3 : AttachmentStorage
    {
        public override Stream Download(string key)
        {
            try
            {
                var client = new AmazonS3Client(AttachmentHelperAWS.GetAmazonCredentials(this), RegionEndpoint.EUWest1);

                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = this.Bucket,
                    Key = key
                };
                GetObjectResponse response = client.GetObject(request);
                return response.ResponseStream;
            }
            catch (Exception exc)
            {
                //throw new UserFriendlyException(exc);
                return null;
            }
        }
        public override void Upload(Attachment attachment, Stream stream, string key)
        {
            AttachmentHelperAWS.SaveToS3(attachment, this, stream, key);
        }
        public AttachmentStorageAmazonS3(Session session) : base(session) { }
        // Fields...
        private string _Bucket;
        private string _AwsSecretKey;
        private string _AwsAccessKey;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AwsAccessKey
        {
            get
            {
                return _AwsAccessKey;
            }
            set
            {
                SetPropertyValue("AwsAccessKey", ref _AwsAccessKey, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string AwsSecretKey
        {
            get
            {
                return _AwsSecretKey;
            }
            set
            {
                SetPropertyValue("AwsSecretKey", ref _AwsSecretKey, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Bucket
        {
            get
            {
                return _Bucket;
            }
            set
            {
                SetPropertyValue("Bucket", ref _Bucket, value);
            }
        }

    }
}