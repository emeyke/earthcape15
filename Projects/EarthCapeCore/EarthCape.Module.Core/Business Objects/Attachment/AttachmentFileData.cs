﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.IO;
namespace EarthCape.Module.Core
{
    [DefaultProperty("FileName")]
    public class AttachmentFileData : BaseObject, IFileData, IEmptyCheckable, ISupportFullName
    {
        private string fullName;
        [ModelDefault("AllowEdit", "False")]
        public string FullName
        {
            get { return fullName; }
            set { SetPropertyValue("FullName", ref fullName, value); }
        }
          private string _FilePath;
        private string fileName = string.Empty;
#if MediumTrust
		private int size;
		public int Size {
			get { return size; }
			set { SetPropertyValue("Size", ref size, value); }
		}
#else
        [Persistent]
        private int size;
        public int Size
        {
            get { return size; }
        }
#endif
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
            }
        }


        public AttachmentFileData(Session session) : base(session) { }
        public virtual void LoadFromStream(string fileName, Stream stream)
        {
            Guard.ArgumentNotNull(stream, "stream");
            FileName = fileName;
            if (stream is FileStream)
                FilePath = Path.GetDirectoryName(((FileStream)stream).Name);
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            Content = bytes;
        }
        public virtual void SaveToStream(Stream stream)
        {
            if (Content != null)
            {
                stream.Write(Content, 0, Size);
            }
            stream.Flush();
        }
        public void Clear()
        {
            Content = null;
            FileName = String.Empty;
            FullName = string.Empty;
        }
        public override string ToString()
        {
            return FileName;
        }
        [Size(260)]
        public string FileName
        {
            get { return fileName; }
            set { SetPropertyValue("FileName", ref fileName, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string FilePath
        {
            get
            {
                return _FilePath;
            }
            set
            {
                SetPropertyValue("FilePath", ref _FilePath, value);
            }
        }
        [Persistent, Delayed(true)]
        [ValueConverter(typeof(CompressionConverter))]
        [MemberDesignTimeVisibility(false)]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public byte[] Content
        {
            get { return GetDelayedPropertyValue<byte[]>("Content"); }
            set
            {
                int oldSize = size;
                if (value != null)
                {
                    size = value.Length;
                }
                else
                {
                    size = 0;
                }
                SetDelayedPropertyValue("Content", value);
                OnChanged("Size", oldSize, size);
            }
        }
        #region IEmptyCheckable Members
        [NonPersistent, MemberDesignTimeVisibility(false)]
        public bool IsEmpty
        {
            get { return string.IsNullOrEmpty(FileName); }
        }
        #endregion
    }
}
