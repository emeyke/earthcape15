﻿using System;
using DevExpress.Xpo;
using Xpand.Persistent.BaseImpl.ImportExport;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class SelectIOConfiguration : XPCustomObject
    {
        public SelectIOConfiguration(Session session) : base(session) { }
        private SerializationConfigurationGroup _IOConfiguration;
        public SerializationConfigurationGroup IOConfiguration
        {
            get
            {
                return _IOConfiguration;
            }
            set
            {
                SetPropertyValue("IOConfiguration", ref _IOConfiguration, value);
            }
        }
    }

}
