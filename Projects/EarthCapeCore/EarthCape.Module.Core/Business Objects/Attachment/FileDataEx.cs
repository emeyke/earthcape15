﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.IO;
namespace EarthCape.Module.Core
{
    [DefaultProperty("FileName")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class FileDataEx : FileData, ISupportFullName, IFileData
    {
        public FileDataEx(Session session) : base(session) { }
        private string fullName;
        [ModelDefault("AllowEdit", "False")]
        public string FullName
        {
            get { return fullName; }
            set { SetPropertyValue("FullName", ref fullName, value); }
        }
        void IFileData.Clear()
        {
            base.Clear();
            FullName = string.Empty;
        }
    }
}
