
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
  
    public class ImportFileObject : FileAttachmentBase
    {
        public ImportFileObject(Session session) : base(session) { }


     
        private bool _ImportGBIFTaxonomy;
        public bool ImportGBIFTaxonomy
        {
            get { return _ImportGBIFTaxonomy; }
            set { SetPropertyValue<bool>(nameof(ImportGBIFTaxonomy), ref _ImportGBIFTaxonomy, value); }
        }



    }

}
