﻿// Developer Express Code Central Example:
// How to store files in the file system, instead of the database.
// 
// See Also:
// File Attachments Module Overview
// (ms-help://Xpand.XAF/CustomDocument2781.htm)
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E965

using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Interfaces;
using EarthCape.Module.Logistics;
using System;

using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    /*   [Appearance("NotUploaded", AppearanceItemType = "ViewItem", TargetItems = "Url;OriginalFileName",
         Criteria = "Uploaded=False", Context = "Any", BackColor = "Red",
             Priority = 2)]*/
 //   [ExcelImportKeyAttribute("OriginalFileName")]
    [DefaultProperty("Code")]
    public class Attachment : BaseObject, IThumbnail, ICode
    {
        private PersonCore _Contact;
        private License _License;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public License License
        {
            get
            {
                return _License;
            }
            set
            {
                SetPropertyValue("License", ref _License, value);
            }
        }
        private string _Code;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                SetPropertyValue("Code", ref _Code, value);
            }
        }


        [Size(SizeAttribute.Unlimited)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
        private string _Comment;
        public Attachment(Session session) : base(session) { }
        /*  [ImmediatePostData(true)]
          public AttachmentStorage Storage
          {
              get
              {
                  return _Storage;
              }
              set
              {
                  SetPropertyValue("Storage", ref _Storage, value);
              }
          }*/
        public string Caption
        {
            get { return OriginalFileName; }
        }
        [NonPersistent]
        [ImmediatePostData(true)]
        [VisibleInDetailView(true)]
        [VisibleInLookupListView(false)]
        public AttachmentFileData FileUpload
        {
            get
            {
                return _FileUpload;
            }
            set
            {
                SetPropertyValue("FileUpload", ref _FileUpload, value);
                filedata = _FileUpload;
                OriginalFileName = string.Empty;
                Url = string.Empty;
            }
        }

        [ImmediatePostData(true)]
        public AttachmentFileData FileStored
        {
            get
            {
                return _FileStored;
            }
            set
            {
                SetPropertyValue("FileStored", ref _FileStored, value);
                if (_FileStored != null)
                    filedata = _FileStored;
            }
        }
        [DevExpress.Xpo.ValueConverterAttribute(typeof(ImageValueConverter))]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Image Image
        {
            get
            {
                if (!IsLoading)
                {
                    if ((IsImage) && (!String.IsNullOrEmpty(OriginalFilePath)) && (!String.IsNullOrEmpty(OriginalFileName)))
                    {
                        return Image.FromFile(Path.Combine(OriginalFilePath, OriginalFileName));
                    }
                 /*   if ((IsImage) && (Key != null))
                    {
                        Stream stream = DownloadFromStorage();
                        if (stream != null)
                            return Image.FromStream(stream);
                    }*/
                    if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
                    {
                        WebRequest req = WebRequest.Create(Url);
                        WebResponse response = req.GetResponse();
                        Stream stream = response.GetResponseStream();
                        if (stream != null)
                            return Image.FromStream(stream);

                    }
                }
                return null;
            }
        }

        private AttachmentFileData _FileStored;
      //  private AttachmentStorage _Storage;
        private AttachmentFileData _FileUpload;
        [Persistent("IsImage")]
        private bool isImage;

        [PersistentAlias("isImage")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public bool IsImage
        {
            get { return isImage; }
        }
        [Persistent("Uploaded")]
        private bool uploaded;

        [PersistentAlias("uploaded")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public bool Uploaded
        {
            get { return uploaded; }
        }
        public void SetUploaded()
        {
            uploaded = true;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        Stream stream;
        AttachmentFileData filedata;
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            if (IsLoading) return;
            /*todo
                          if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultStorage"]))
               {
                   Storage = Session.FindObject<AttachmentStorage>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultStorage"]));
               }

             */
            bool generated = false;
            if (filedata != null)
            {
                using (stream = new MemoryStream())
                {
                    if (String.IsNullOrEmpty(OriginalFileName))
                    {
                        OriginalFileName = filedata.FileName;
                        OriginalFilePath = filedata.FilePath;
                        filedata.SaveToStream(stream);
                        if (GeneralHelper.IsRecognisedImageFile(OriginalFileName))
                        {
                            isImage = true;
                            Thumbnail = AttachmentHelper.CreateThumbnail(stream, 100, 100);// ResizeImage(Image.FromFile(filename), 100);
                            generated = true;
                        }
                    }

                }
            }
            if (!generated)
            {
                try
                {
                    generated = GenerateThumbnailFromUrl();
                }
                finally { }
            }
            base.OnSaving();

            /*   if ((Storage != null) && (String.IsNullOrEmpty(Url)))
               {
                   Key = string.Format("{0:N}{1}", Oid, Path.GetExtension(OriginalFileName)).ToUpper();

                   if (filedata != null)
                       Storage.Upload(this, stream, Key);
                   /*if ((Storage != null) && (Storage is AttachmentStorageAmazonS3))
                       Helper.SaveToS3(this, (AttachmentStorageAmazonS3)Storage, stream, Key);*/
            //}
            if ((String.IsNullOrEmpty(OriginalFileName)) && (!String.IsNullOrEmpty(Url)))
            {
                OriginalFileName = Path.GetFileName(Url);
            }

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public bool GenerateThumbnailFromUrl()
        {
            if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
            {
                WebRequest req = WebRequest.Create(Url);
                WebResponse response = req.GetResponse();
                Stream stream = response.GetResponseStream();
                if (stream != null)
                {
                    Thumbnail = AttachmentHelper.CreateThumbnail(stream, 100, 100);
                    return true;
                }

            }
            return false;
        }

        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("Attachment.Url.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string Url
        {
            get
            {
                return _Url;
            }
            set
            {
                SetPropertyValue("Url", ref _Url, value);
            }
        }


        // Fields...
        private Image _Thumbnail;
        private string _Url;
        private string _OriginalFilePath;
        private string _OriginalFileName;
        private string _Key;
        

        [DevExpress.Xpo.ValueConverterAttribute(typeof(ImageValueConverter))]
        [ImageEditor(ImageSizeMode = ImageSizeMode.Zoom,
            DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 100,
            ListViewImageEditorCustomHeight = 100)]
        public Image Thumbnail
        {
            get
            {
                return _Thumbnail;
            }
            set
            {
                SetPropertyValue("Thumbnail", ref _Thumbnail, value);
            }
        }
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [ImmediatePostData(true)]

         public string OriginalFileName
        {
            get { return _OriginalFileName; }
            set { SetPropertyValue<string>(nameof(OriginalFileName), ref _OriginalFileName, value); }
        }



        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string OriginalFilePath
        {
            get
            {
                return _OriginalFilePath;
            }
            set
            {
                SetPropertyValue("OriginalFilePath", ref _OriginalFilePath, value);
            }
        }
        /*   [Size(SizeAttribute.DefaultStringMappingFieldSize)]
           [VisibleInDetailView(false)]
           [VisibleInListView(false)]
           [VisibleInLookupListView(false)]
           public string S3_Bucket
           {
               get
               {
                   return _S3_Bucket;
               }
               set
               {
                   SetPropertyValue("S3_Bucket", ref _S3_Bucket, value);
               }
           }*/

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Key
        {
            get
            {
                return _Key;
            }
            set
            {
                SetPropertyValue("Key", ref _Key, value);
            }
        }
     /*   public Stream DownloadFromStorage()
        {
            if (Storage != null)
            {
                return Storage.Download(Key);
            }
            return null;
        }*/
        /*    public Stream DownloadFromStorage()
            {
                if ((Storage != null) && (Storage is AttachmentStorageAmazonS3))
                {
                    try
                    {
                        var client = new AmazonS3Client(Helper.GetAmazonCredentials(((AttachmentStorageAmazonS3)Storage)), RegionEndpoint.EUWest1);

                        GetObjectRequest request = new GetObjectRequest()
                        {
                            BucketName = ((AttachmentStorageAmazonS3)Storage).Bucket,
                            Key = Key
                        };
                        GetObjectResponse response = client.GetObject(request);
                        return response.ResponseStream;
                    }
                    catch (Exception exc)
                    {
                        //throw new UserFriendlyException(exc);
                        return null;
                    }
                }
                return null;
            }*/


     /*   [Association("Attachment-UnitAttachment"), Aggregated]
        public XPCollection<UnitAttachment> Units
        {
            get
            {
                return GetCollection<UnitAttachment>("Units");
            }
        }*/
       /* [Association("Attachment-DatasetAttachment"), Aggregated]
        public XPCollection<DatasetAttachment> Datasets
        {
            get
            {
                return GetCollection<DatasetAttachment>("Datasets");
            }
        }*/
        [Association("Attachment-ProjectAttachment"), Aggregated]
        public XPCollection<ProjectAttachment> Projects
        {
            get
            {
                return GetCollection<ProjectAttachment>("Projects");
            }
        }

        public PersonCore Contact
        {
            get
            {
                return _Contact;
            }
            set
            {
                SetPropertyValue("Contact", ref _Contact, value);
            }
        }

        private string _OCR;
        [Size(SizeAttribute.Unlimited)]
        public string OCR
        {
            get { return _OCR; }
            set { SetPropertyValue<string>(nameof(OCR), ref _OCR, value); }
        }


        [Association("AccessionDocuments")]
        [VisibleInDetailView(false)]
        public XPCollection<AccessionStageProcess> AccessionStageDocuments
        {
            get { return GetCollection<AccessionStageProcess>(nameof(AccessionStageDocuments)); }
        }

    }
}