using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class Genus : HigherTaxon//, ICategorizedItem
    {
        public Genus(Session session) : base(session) { }
        [Association("Genus-Species")]
        public XPCollection<Species> Species
        {
            get
            {
                return GetCollection<Species>("Species");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            TaxonomicCategory = Session.FindObject<TaxonomicCategory>(new BinaryOperator("Name", "Genus"));
        }

        /*  [Association("GenusRef"),Aggregated]
               public XPCollection<GenusRef> TaxonomicName
               {
                   get
                   {
                       return GetCollection<GenusRef>("TaxonomicName");
                   }
               }*/
        /*  [Association("Genus-SubspecificNames"),Aggregated]
          public XPCollection<SubspecificName> SubspecificNames
          {
              get
              {
                  return GetCollection<SubspecificName>("SubspecificNames");
              }
          }*/
        /*  public HigherTaxon Category
          {
              get
              {

                  GenusRef spcl = u.Session.FindObject<GenusRef>(CriteriaHelper.GetCurrentClassifiedGenus(u));
                     if (spcl != null) return spcl.Category.HigherTaxon;
                  return null;
              }
          }*/
        public override string ToString()
        {
            return Name;
        }

        /*     private HigherTaxon category;
             [Association("Category-Genus")]
             public HigherTaxon Category
             {
                 get
                 {
                     return category;
                 }
                 set
                 {
                     SetPropertyValue("Category", ref category, value);
                 }
             }*/
        /*   ITreeNode ICategorizedItem.Category
            {
                get
                {
                    return Category;
                }
                set
                {
                    //Category = (HigherTaxon)value;
                }
            }*/
    }

}
