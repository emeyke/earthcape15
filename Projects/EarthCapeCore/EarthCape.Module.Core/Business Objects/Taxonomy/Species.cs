using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    //  [DefaultProperty("FullName")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class Species : TaxonomicName//,ICategorizedItem
    {

        [Association("SpeciesInfraspecies")]
        public XPCollection<SubspecificTaxon> SubspecificTaxa
        {
            get { return GetCollection<SubspecificTaxon>(nameof(SubspecificTaxa)); }
        }

        private bool _Hybrid;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public bool Hybrid
        {
            get { return _Hybrid; }
            set { SetPropertyValue<bool>(nameof(Hybrid), ref _Hybrid, value); }
        }




        private HigherTaxon _Category;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public HigherTaxon Category
        {
            get { return _Category; }
            set { SetPropertyValue<HigherTaxon>(nameof(Category), ref _Category, value); }
        }


      

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            TaxonomicCategory = Session.FindObject<TaxonomicCategory>(new BinaryOperator("Name", "Species"));
        }

        /*  protected override void OnSaving()
          {
              if ((Genus != null) && (Parent == null))
              {
                  Parent = Genus;
                  Category = Genus;
              }
              base.OnSaving();
          }*/

        public Species(Session session) : base(session) { }


        private Genus _Genus;
        [Association("Genus-Species")]
        public Genus Genus
        {
            get { return _Genus; }
            set { SetPropertyValue<Genus>(nameof(Genus), ref _Genus, value); }
        }



        [Association("Species-NewCombinations")]
        [VisibleInDetailView(false)]
        public XPCollection<Species> NewCombinations
        {
            get
            {
                return GetCollection<Species>("NewCombinations");
            }
        }

        private Species _NewCombinationOf;
        [Association("Species-NewCombinations")]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Species NewCombinationOf
        {
            get { return _NewCombinationOf; }
            set { SetPropertyValue<Species>(nameof(NewCombinationOf), ref _NewCombinationOf, value); }
        }


       

        private bool _IsOriginalDescription;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public bool IsOriginalDescription
        {
            get { return _IsOriginalDescription; }
            set { SetPropertyValue<bool>(nameof(IsOriginalDescription), ref _IsOriginalDescription, value); }
        }


    }

}
