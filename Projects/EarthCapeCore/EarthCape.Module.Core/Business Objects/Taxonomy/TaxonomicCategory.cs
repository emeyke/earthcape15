using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultProperty("Name")]
    [CreatableItem(false)]
    [ExcelImportKeyAttribute("Name")]
    public class TaxonomicCategory : BaseObject, IProjectObject, IName
    {
        //    public Dataset(Session session,Guid guid) : base(session,guid) { }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }
        public TaxonomicCategory(Session session) : base(session) { }
        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


        private string _Name;
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        private string _Abbreviation;
        public string Abbreviation
        {
            get { return _Abbreviation; }
            set { SetPropertyValue<string>(nameof(Abbreviation), ref _Abbreviation, value); }
        }


        private string _Prefix;
        public string Prefix
        {
            get { return _Prefix; }
            set { SetPropertyValue<string>(nameof(Prefix), ref _Prefix, value); }
        }


        private string _Suffix;
        public string Suffix
        {
            get { return _Suffix; }
            set { SetPropertyValue<string>(nameof(Suffix), ref _Suffix, value); }
        }
    }
}
