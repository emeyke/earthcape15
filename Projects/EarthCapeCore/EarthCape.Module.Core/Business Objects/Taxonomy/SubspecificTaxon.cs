using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;


namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class SubspecificTaxon : TaxonomicName
    {
        public SubspecificTaxon(Session session) : base(session) { }

        public override string ToString()
        {
            return Name;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            TaxonomicCategory = Session.FindObject<TaxonomicCategory>(new BinaryOperator("Name", "Subspecies"));
        }

        private Species _Species;
        [Association("SpeciesInfraspecies")]
        public Species Species
        {
            get { return _Species; }
            set { SetPropertyValue<Species>(nameof(Species), ref _Species, value); }
        }



    }
}
