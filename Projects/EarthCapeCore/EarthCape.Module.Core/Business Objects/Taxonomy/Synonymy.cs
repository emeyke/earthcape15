using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

using System;
using System.ComponentModel;
using System.Linq;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    public class Synonymy : BaseObject, IProjectObject
    {

        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


        public Synonymy(Session session) : base(session) { }

        private TaxonomicName _TaxonomicName;
        [Association("TaxonomicName-Synonyms")]
        public TaxonomicName TaxonomicName
        {
            get { return _TaxonomicName; }
            set { SetPropertyValue<TaxonomicName>(nameof(TaxonomicName), ref _TaxonomicName, value); }
        }




        private TaxonomicName _Synonym;
        [Association("TaxonomicName-SynonymOf")]
        public TaxonomicName Synonym
        {
            get { return _Synonym; }
            set { SetPropertyValue<TaxonomicName>(nameof(Synonym), ref _Synonym, value); }
        }


        private string _SynonymString;
        public string SynonymString
        {
            get { return _SynonymString; }
            set { SetPropertyValue<string>(nameof(SynonymString), ref _SynonymString, value); }
        }


        private string _SynonymReferenceString;
        public string SynonymReferenceString
        {
            get { return _SynonymReferenceString; }
            set { SetPropertyValue<string>(nameof(SynonymReferenceString), ref _SynonymReferenceString, value); }
        }




    }

}
