using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;


namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class HigherTaxon : TaxonomicName
    {
        public HigherTaxon(Session session) : base(session) { }
        public override string ToString()
        {
            return Name;
        }
        /*   [Association("HigherTaxon-Species")]
           public XPCollection<Species> LinkedSpecies
           {
               get
               {
                   return GetCollection<Species>("LinkedSpecies");
               }
           }*/
        /*     private XPCollection<Species> _AllSpecies;
             public XPCollection<Species> AllSpecies
             {
                 get
                 {
                     if (_AllSpecies == null)
                     {
                         _AllSpecies = new XPCollection<Species>(Session, false);
                         CollectSpeciesRecursive(this, _AllSpecies);
                         _AllSpecies.BindingBehavior = CollectionBindingBehavior.AllowNone;
                     }
                     return _AllSpecies;
                 }
             }
             private XPCollection<Unit> _AllUnits;
             public XPCollection<Unit> AllUnits
             {
                 get
                 {
                     if (_AllUnits == null)
                     {
                         _AllUnits = new XPCollection<Unit>(Session, false);
                         CollectUnitsRecursive(this, _AllUnits);
                         _AllUnits.BindingBehavior = CollectionBindingBehavior.AllowNone;
                     }
                     return _AllUnits;
                 }
             }
             private void CollectSpeciesRecursive(HigherTaxon higherTaxon, XPCollection<Species> target)
             {
                 foreach (Species species in higherTaxon.LinkedSpecies)
                 {
                     target.Add(species);

                 }
                 if (higherTaxon is Genus)
                 {
                     foreach (Species species1 in ((Genus)higherTaxon).Species)
                     {
                         target.Add(species1);

                     }
                 }
                 foreach (HigherTaxon childCategory in higherTaxon.Children)
                 {
                     CollectSpeciesRecursive(childCategory, target);
                 }
             }
             private void CollectUnitsRecursive(HigherTaxon higherTaxon, XPCollection<Unit> target)
             {
                 foreach (Unit unit in higherTaxon.Units)
                 {
                     target.Add(unit);

                 }
                 if (higherTaxon is Genus)
                 {
                     foreach (Unit unit1 in ((Genus)higherTaxon).Units)
                     {
                         target.Add(unit1);

                     }
                 }
                 foreach (HigherTaxon childCategory in higherTaxon.Children)
                 {
                     CollectUnitsRecursive(childCategory, target);
                 }
             }*/

        private string _Pages;
        public string Pages
        {
            get { return _Pages; }
            set { SetPropertyValue<string>(nameof(Pages), ref _Pages, value); }
        }


    }
}
