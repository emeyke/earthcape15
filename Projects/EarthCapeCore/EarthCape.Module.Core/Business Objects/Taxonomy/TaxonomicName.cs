using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;
using System.ComponentModel;
using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [ExcelImportKeyAttribute("FullName")]
    public abstract class TaxonomicName : BaseObject, IUnits, INameTranslatable, ITreeNode, IProjectObject, IName
    {


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Parent = null;
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        private string _RU;
        [Size(1000)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string RU
        {
            get { return _RU; }
            set { SetPropertyValue<string>(nameof(RU), ref _RU, value); }
        }




        private string _UA;
        [Size(1000)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string UA
        {
            get { return _UA; }
            set { SetPropertyValue<string>(nameof(UA), ref _UA, value); }
        }




        private string _LV;
        [Size(1000)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string LV
        {
            get { return _LV; }
            set { SetPropertyValue<string>(nameof(LV), ref _LV, value); }
        }



        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }



        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            UpdateInfo();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        private TaxonomicCategory _TaxonomicCategory;
        public TaxonomicCategory TaxonomicCategory
        {
            get { return _TaxonomicCategory; }
            set { SetPropertyValue<TaxonomicCategory>(nameof(TaxonomicCategory), ref _TaxonomicCategory, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(500)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }



        private Project _Project;
        [Association("Project-Taxa")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }




        public void UpdateInfo()
        {
            string name = Name;
            // string auth = Authorship;
            // if (!String.IsNullOrEmpty(auth))
            //    name = String.Format("{0} {1}", name, auth);
            if (this is Species)
                if (((Species)this).Genus != null)
                    if (!String.IsNullOrEmpty(this.Name))
                    {
                        name = String.Format("{0} {1}", ((Species)this).Genus.Name, name);
                        ((Species)this).Category = ((Species)this).Genus;
                        ((Species)this).Parent = ((Species)this).Genus;
                        if (Session!= null)
                        ((Species)this).TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,null, Session, "Species");
                    }
                    else
                        if (!String.IsNullOrEmpty(ShortName))
                        name = ShortName;
                    else
                        name = ((Species)this).Genus.Name;
            if (typeof(SubspecificTaxon).IsAssignableFrom(this.GetType()))
            {
                string prefix = " ";
                if (this.TaxonomicCategory != null)
                {
                    if (this.TaxonomicCategory.Name == "Form")
                        prefix = " f. ";
                    if (this.TaxonomicCategory.Name == "Variety")
                        prefix = " var. ";
                    if (this.TaxonomicCategory.Name == "Subspecies")
                        prefix = " ssp. ";
                }
                if (((SubspecificTaxon)this).Species != null)
                    if (!String.IsNullOrEmpty(this.Name))
                    {
                        name = String.Format("{0}{1}{2}", ((SubspecificTaxon)this).Species.FullName, prefix, name);
                        ((SubspecificTaxon)this).Parent = ((SubspecificTaxon)this).Species;
                        if (Session != null)
                            if (this.TaxonomicCategory==null)
                            ((SubspecificTaxon)this).TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,null,Session, "Subspecies");
                    }
                    else
                        if (!String.IsNullOrEmpty(ShortName))
                        name = ShortName;
                    else
                        name = ((SubspecificTaxon)this).Species.FullName;
            }

            if (string.IsNullOrEmpty(name))
                if (!string.IsNullOrEmpty(this.ShortName))
                    name = this.ShortName;
            FullName = name;

            if (this is Species) CalcSpecies = (Species)this;
            if (this is SubspecificTaxon) CalcSpecies = ((SubspecificTaxon)this).Species;
            if (this is Genus) CalcGenus = (Genus)this;
            if (this is Species) CalcGenus = ((Species)this).Genus;
            if (this is Family) CalcFamily = (Family)this;
            TaxonomicName _name = this;
            while ((_name.Parent != null))
            {
                if (_name is Family) CalcFamily = (Family)_name;
                _name = _name.Parent;
            }
            CalcOrder = GetCat("Order");
            CalcClass = GetCat("Class");
            CalcSubfamily = GetCat("Subfamily");
            CalcPhylum = GetCat("Phylum");
            CalcKingdom = GetCat("Kingdom");
            if ((this is SubspecificTaxon)
                && (((SubspecificTaxon)this).Species != null))
            {
                CalcGenus = ((SubspecificTaxon)this).Species.Genus;
                CalcFamily = ((SubspecificTaxon)this).Species.CalcFamily;
                CalcOrder = ((SubspecificTaxon)this).Species.CalcOrder;
                CalcSubfamily = ((SubspecificTaxon)this).Species.CalcSubfamily;
                CalcClass = ((SubspecificTaxon)this).Species.CalcClass;
                CalcKingdom = ((SubspecificTaxon)this).Species.CalcKingdom;
            }

        }


        private string _NcbiTaxonId;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string NcbiTaxonId
        {
            get { return _NcbiTaxonId; }
            set { SetPropertyValue<string>(nameof(NcbiTaxonId), ref _NcbiTaxonId, value); }
        }



        private long _IucnId;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public long IucnId
        {
            get { return _IucnId; }
            set { SetPropertyValue<long>(nameof(IucnId), ref _IucnId, value); }
        }



        private string _GbifStatus;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string GbifStatus
        {
            get { return _GbifStatus; }
            set { SetPropertyValue<string>(nameof(GbifStatus), ref _GbifStatus, value); }
        }




        private TaxonomicName _GbifValidTaxonomicName;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public TaxonomicName GbifValidTaxonomicName
        {
            get { return _GbifValidTaxonomicName; }
            set { SetPropertyValue<TaxonomicName>(nameof(GbifValidTaxonomicName), ref _GbifValidTaxonomicName, value); }
        }



        private string _GbifKey;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string GbifKey
        {
            get { return _GbifKey; }
            set { SetPropertyValue<string>(nameof(GbifKey), ref _GbifKey, value); }
        }



        private string _Text;
        [Size(SizeAttribute.Unlimited)]
        public string Text
        {
            get { return _Text; }
            set { SetPropertyValue<string>(nameof(Text), ref _Text, value); }
        }



        private string _ShortName;
        public string ShortName
        {
            get { return _ShortName; }
            set { SetPropertyValue<string>(nameof(ShortName), ref _ShortName, value); }
        }



        private int _tOrder;
        public int tOrder
        {
            get { return _tOrder; }
            set { SetPropertyValue<int>(nameof(tOrder), ref _tOrder, value); }
        }



        /*  private int _EolId;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          [VisibleInLookupListView(false)]
          public int EolId
          {
              get { return _EolId; }
              set { SetPropertyValue<int>(nameof(EolId), ref _EolId, value); }
          }




          private int _EolTaxonId;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          [VisibleInLookupListView(false)]
          public int EolTaxonId
          {
              get
              {
                  return _EolTaxonId;
              }
              set
              {
                  SetPropertyValue("EolTaxonId", ref _EolTaxonId, value);
              }
          }*/

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        private TaxonomicName _Parent;
        [Association("Object-Children")]
        public TaxonomicName Parent
        {
            get { return _Parent; }
            set { SetPropertyValue<TaxonomicName>(nameof(Parent), ref _Parent, value); }
        }



        [Association("Object-Children")]
        [VisibleInDetailView(false)]
        public XPCollection<TaxonomicName> Children
        {
            get { return GetCollection<TaxonomicName>("Children"); }
        }
        #region ITreeNode
        IBindingList ITreeNode.Children
        {
            get
            {
                return Children;
            }
        }
        string ITreeNode.Name
        {
            get
            {
                return Name;
            }
        }
        ITreeNode ITreeNode.Parent
        {
            get
            {
                return Parent;
            }
        }
        #endregion


        [Association("TaxonomicName-Units")]
        public XPCollection<Unit> Units
        {
            get { return GetCollection<Unit>("Units"); }
        }

        private Reference _Reference;
        [Association("Reference-Taxa")]
        public Reference Reference
        {
            get { return _Reference; }
            set { SetPropertyValue<Reference>(nameof(Reference), ref _Reference, value); }
        }


        [Association("TaxonomicName-Citations")]
        public XPCollection<CitationTaxonomicName> Citations
        {
            get
            {
                return GetCollection<CitationTaxonomicName>("Citations");
            }
        }

        private string _Authorship;
        public string Authorship
        {
            get { return _Authorship; }
            set { SetPropertyValue<string>(nameof(Authorship), ref _Authorship, value); }
        }


        private int _AuthorshipYear;
        /// <summary>
        /// Calculated from AuthorshipDate if available
        /// </summary>
        public int AuthorshipYear
        {
            get { return _AuthorshipYear; }
            set { SetPropertyValue<int>(nameof(AuthorshipYear), ref _AuthorshipYear, value); }
        }



        private DateTime _AuthorshiDate;
        public DateTime AuthorshiDate
        {
            get { return _AuthorshiDate; }
            set { SetPropertyValue<DateTime>(nameof(AuthorshiDate), ref _AuthorshiDate, value); }
        }


        public TaxonomicName(Session session) : base(session) { }


        [Association("TaxonomicName-Synonyms")]
        [VisibleInDetailView(false)]
        public XPCollection<Synonymy> Synonyms
        {
            get
            {
                return GetCollection<Synonymy>("Synonyms");
            }
        }
        [Association("TaxonomicName-SynonymOf")]
        [VisibleInDetailView(false)]
        public XPCollection<Synonymy> SynonymOf
        {
            get
            {
                return GetCollection<Synonymy>("SynonymOf");
            }
        }

        private string _EN;
        public string EN
        {
            get { return _EN; }
            set { SetPropertyValue<string>(nameof(EN), ref _EN, value); }
        }


        private Species _CalcSpecies;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Species CalcSpecies
        {
            get { return _CalcSpecies; }
            set { SetPropertyValue<Species>(nameof(CalcSpecies), ref _CalcSpecies, value); }
        }



        private Genus _CalcGenus;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Genus CalcGenus
        {
            get { return _CalcGenus; }
            set { SetPropertyValue<Genus>(nameof(CalcGenus), ref _CalcGenus, value); }
        }



        private Family _CalcFamily;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Family CalcFamily
        {
            get { return _CalcFamily; }
            set { SetPropertyValue<Family>(nameof(CalcFamily), ref _CalcFamily, value); }
        }


        private HigherTaxon _CalcOrder;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public HigherTaxon CalcOrder
        {
            get { return _CalcOrder; }
            set { SetPropertyValue<HigherTaxon>(nameof(CalcOrder), ref _CalcOrder, value); }
        }


        private HigherTaxon _CalcClass;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public HigherTaxon CalcClass
        {
            get { return _CalcClass; }
            set { SetPropertyValue<HigherTaxon>(nameof(CalcClass), ref _CalcClass, value); }
        }
        private HigherTaxon _CalcSubfamily;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public HigherTaxon CalcSubfamily
        {
            get { return _CalcSubfamily; }
            set { SetPropertyValue<HigherTaxon>(nameof(CalcSubfamily), ref _CalcSubfamily, value); }
        }

        private HigherTaxon _CalcPhylum;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public HigherTaxon CalcPhylum
        {
            get { return _CalcPhylum; }
            set { SetPropertyValue<HigherTaxon>(nameof(CalcPhylum), ref _CalcPhylum, value); }
        }

        private HigherTaxon _CalcKingdom;
        public HigherTaxon CalcKingdom
        {
            get { return _CalcKingdom; }
            set { SetPropertyValue<HigherTaxon>(nameof(CalcKingdom), ref _CalcKingdom, value); }
        }


        private HigherTaxon GetCat(string cat)
        {
            //   return null;
            TaxonomicName name = this;
            if ((name is HigherTaxon) && (name.TaxonomicCategory != null) && (name.TaxonomicCategory.Name == cat)) return (HigherTaxon)name;
            while ((name.Parent != null))
            {
                name = name.Parent;
                if ((name is HigherTaxon) && (name.TaxonomicCategory != null) && (name.TaxonomicCategory.Name == cat)) return (HigherTaxon)name;
            }
            return null;
        }

   }

}
