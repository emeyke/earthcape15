using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.Drawing;
using DevExpress.Xpo.Metadata;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Library
{
   // [DefaultClassOptions]
    [DefaultProperty("ShortTitle")]
    public class Title : BaseObject
    {
        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(4000)]
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref _FullName, value);
            }

        }
        
        public override string ToString()
        {
            return ShortTitle;
        }
        public Title(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        private string _FullTitle;
        [Size(SizeAttribute.Unlimited)]
        public string FullTitle
        {
            get
            {
                return _FullTitle;
            }
            set
            {
                SetPropertyValue("FullTitle", ref _FullTitle, value);
            }
        }
        private int _TitleId;
        public int TitleId
        {
            get
            {
                return _TitleId;
            }
            set
            {
                SetPropertyValue("TitleId", ref _TitleId, value);
            }
        }
        private string _ShortTitle;
        [Size(255)]
        public string ShortTitle
        {
            get
            {
                return _ShortTitle;
            }
            set
            {
                SetPropertyValue("ShortTitle", ref _ShortTitle, value);
            }
        }
        private Image _Thumbnail;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        public Image Thumbnail
        {
            get
            {
                return _Thumbnail;
            }
            set
            {
                SetPropertyValue("Thumbnail", ref _Thumbnail, value);
            }
        }
        [Association("Title-Items")]
        public XPCollection<Item> Items
        {
            get
            {
                return GetCollection<Item>("Items");
            }
        }
        private string _CallNumber;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string CallNumber
        {
            get
            {
                return _CallNumber;
            }
            set
            {
                SetPropertyValue("CallNumber", ref _CallNumber, value);
            }
        }
        private string _DOI;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string DOI
        {
            get
            {
                return _DOI;
            }
            set
            {
                SetPropertyValue("DOI", ref _DOI, value);
            }
        }
        private string _Edition;
        public string Edition
        {
            get
            {
                return _Edition;
            }
            set
            {
                SetPropertyValue("Edition", ref _Edition, value);
            }
        }
        private string _TitleUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [Size(255)]
        [RuleRegularExpression("Reference.TitleUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string TitleUrl
        {
            get
            {
                return _TitleUrl;
            }
            set
            {
                SetPropertyValue("TitleUrl", ref _TitleUrl, value);
            }
        }
        private string _PublicationDate;
        public string PublicationDate
        {
            get
            {
                return _PublicationDate;
            }
            set
            {
                SetPropertyValue("PublicationDate", ref _PublicationDate, value);
            }
        }
        private string _Publisher;
        public string Publisher
        {
            get
            {
                return _Publisher;
            }
            set
            {
                SetPropertyValue("Publisher", ref _Publisher, value);
            }
        }
        private string _PublisherPlace;
        public string PublisherPlace
        {
            get
            {
                return _PublisherPlace;
            }
            set
            {
                SetPropertyValue("PublisherPlace", ref _PublisherPlace, value);
            }
        }
    }

}
