using System;
using System.ComponentModel;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Library
{
  //  [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class PageName : BaseObject
    {
        public PageName(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        private string _Name;
        [Size(255)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        private Page _Page;
        [Association("Page-Names")]
        public Page Page
        {
            get
            {
                return _Page;
            }
            set
            {
                SetPropertyValue("Page", ref _Page, value);
            }
        }
    }

}
