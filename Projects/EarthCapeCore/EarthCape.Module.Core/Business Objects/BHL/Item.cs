using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.Drawing;
using DevExpress.Xpo.Metadata;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Library
{
  //  [DefaultClassOptions]
    [DefaultProperty("ItemId")]
    public class Item : BaseObject
    {
        public override string ToString()
        {
            return "ITEM: "+ItemId;
        }
        public Item(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        private string _Volume;
        [Size(255)]
        public string Volume
        {
            get
            {
                return _Volume;
            }
            set
            {
                SetPropertyValue("Volume", ref _Volume, value);
            }
        }
        private string _ItemUrl;
        [RuleRegularExpression("Item.ItemUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string ItemUrl
        {
            get
            {
                return _ItemUrl;
            }
            set
            {
                SetPropertyValue("ItemUrl", ref _ItemUrl, value);
            }
        }
        private string _Contributor;
        public string Contributor
        {
            get
            {
                return _Contributor;
            }
            set
            {
                SetPropertyValue("Contributor", ref _Contributor, value);
            }
        }
        private string _CopyrightRegion;
        public string CopyrightRegion
        {
            get
            {
                return _CopyrightRegion;
            }
            set
            {
                SetPropertyValue("CopyrightRegion", ref _CopyrightRegion, value);
            }
        }
        private string _CopyrightStatus;
        public string CopyrightStatus
        {
            get
            {
                return _CopyrightStatus;
            }
            set
            {
                SetPropertyValue("CopyrightStatus", ref _CopyrightStatus, value);
            }
        }
        private string _Language;
        public string Language
        {
            get
            {
                return _Language;
            }
            set
            {
                SetPropertyValue("Language", ref _Language, value);
            }
        }
        private int _ItemId;
        public int ItemId
        {
            get
            {
                return _ItemId;
            }
            set
            {
                SetPropertyValue("ItemId", ref _ItemId, value);
            }
        }
        private Image _Thumbnail;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        public Image Thumbnail
        {
            get
            {
                return _Thumbnail;
            }
            set
            {
                SetPropertyValue("Thumbnail", ref _Thumbnail, value);
            }
        }
        private Title _Title;
        [Association("Title-Items")]
        public Title Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetPropertyValue("Title", ref _Title, value);
            }
        }
        [Association("Item-Pages")]
        public XPCollection<Page> Pages
        {
            get
            {
                return GetCollection<Page>("Pages");
            }
        }
        private string _Source;
        [Size(255)]
        public string Source
        {
            get
            {
                return _Source;
            }
            set
            {
                SetPropertyValue("Source", ref _Source, value);
            }
        }
        private string _SourceIdentifier;
        [Size(255)]
        public string SourceIdentifier
        {
            get
            {
                return _SourceIdentifier;
            }
            set
            {
                SetPropertyValue("SourceIdentifier", ref _SourceIdentifier, value);
            }
        }
        private string _LicenseUrl;
        [Size(255)]
        public string LicenseUrl
        {
            get
            {
                return _LicenseUrl;
            }
            set
            {
                SetPropertyValue("LicenseUrl", ref _LicenseUrl, value);
            }
        }
        private string _Rights;
        [Size(255)]
        public string Rights
        {
            get
            {
                return _Rights;
            }
            set
            {
                SetPropertyValue("Rights", ref _Rights, value);
            }
        }
    }

}
