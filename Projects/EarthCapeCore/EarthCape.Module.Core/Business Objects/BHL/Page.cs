using System;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;

using DevExpress.Xpo.Metadata;
using System.Drawing;
using System.ComponentModel;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Library
{
    [MapInheritance(MapInheritanceType.ParentTable)]
   // [DefaultClassOptions]
    [DefaultProperty("PageId")]
    public class Page : UnitAttachment
    {
        public override string ToString()
        {
            return "PAGE: "+PageId;
        }
        public Page(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

  
      /*  [ProvidedAssociation("Taxa-Pages", RelationType.ManyToMany)]
        public XPCollection<TaxonomicName> Taxa
        {
            get
            {
                return GetCollection<TaxonomicName>("Taxa");
            }
        }*/

        private Reference _Reference;
   //     [ProvidedAssociation("Reference-Pages", RelationType.OneToMany)]
        public Reference Reference
        {
            get
            {
                return _Reference;
            }
            set
            {
                SetPropertyValue("Reference", ref _Reference, value);
            }
        }
        private Item _Item;
        [Association("Item-Pages")]
        public Item Item
        {
            get
            {
                return _Item;
            }
            set
            {
                SetPropertyValue("Item", ref _Item, value);
            }
        }
        private int _PageId;
        public int PageId
        {
            get
            {
                return _PageId;
            }
            set
            {
                SetPropertyValue("PageId", ref _PageId, value);
            }
        }
   
     
        private string _NamesText;
        [Size(SizeAttribute.Unlimited)]
        public string NamesText
        {
            get
            {
                return _NamesText;
            }
            set
            {
                SetPropertyValue("NamesText", ref _NamesText, value);
            }
        }
       
        private string _Volume;
        public string Volume
        {
            get
            {
                return _Volume;
            }
            set
            {
                SetPropertyValue("Volume", ref _Volume, value);
            }
        }
        private string _Year;
        public string Year
        {
            get
            {
                return _Year;
            }
            set
            {
                SetPropertyValue("Year", ref _Year, value);
            }
        }
        private string _Issue;
        public string Issue
        {
            get
            {
                return _Issue;
            }
            set
            {
                SetPropertyValue("Issue", ref _Issue, value);
            }
        }
        private string _PageNumber;
        public string PageNumber
        {
            get
            {
                return _PageNumber;
            }
            set
            {
                SetPropertyValue("PageNumber", ref _PageNumber, value);
            }
        }
        [Association("Page-Names")]
        public XPCollection<PageName> Names
        {
            get
            {
                return GetCollection<PageName>("Names");
            }
        }
    }

}
