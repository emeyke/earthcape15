using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class SamplingPart : Locality
    {
        public SamplingPart(Session session) : base(session) { }
        // Fields...
        private Sampling _Sampling;

        [Association("Sampling-Parts")]
        public Sampling Sampling
        {
            get
            {
                return _Sampling;
            }
            set
            {
                SetPropertyValue("Sampling", ref _Sampling, value);
            }
        }
      
    }
}
