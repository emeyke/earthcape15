using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TatukGIS.NDK;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class Locality : BaseObject, IUnits, IWKT, IProjectObject, IName, ICode, IVectorMapsMarker, IMapsMarker//ICategorizedItem
    {

        private int? _CoordAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? CoordAccuracy
        {
            get { return _CoordAccuracy; }
            set { SetPropertyValue<int?>(nameof(CoordAccuracy), ref _CoordAccuracy, value); }
        }

        private GeometryType _GeometryType = GeometryType.Point;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public GeometryType GeometryType
        {
            get { return _GeometryType; }
            set { SetPropertyValue<GeometryType>(nameof(GeometryType), ref _GeometryType, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public IMapsMarker Map
        {
            get { return this; }
        }

        private string _Type;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Type
        {
            get { return _Type; }
            set { SetPropertyValue<string>(nameof(Type), ref _Type, value); }
        }


        private Habitat _Habitat;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Habitat Habitat
        {
            get { return _Habitat; }
            set { SetPropertyValue<Habitat>(nameof(Habitat), ref _Habitat, value); }
        }


        private bool _IsModified;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public bool IsModified
        {
            get { return _IsModified; }
            set { SetPropertyValue<bool>(nameof(IsModified), ref _IsModified, value); }
        }



        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }





        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            UpdateInfo();
            GISHelper.UpdateAreaAndLengthFromWKT(this);
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            EPSG = 4326;
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }



        private Project _Project;
        [Association("Project-Localities")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }




        public void UpdateInfo()
        {
            Label = Name;
        }


        private string _Name;
        [Size(1000)]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        [ModelDefault("RowCount", "1")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        //todo mrg temp fields


        private bool _NotValid;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public bool NotValid
        {
            get { return _NotValid; }
            set { SetPropertyValue<bool>(nameof(NotValid), ref _NotValid, value); }
        }




        private string _ReasonNotValid;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string ReasonNotValid
        {
            get { return _ReasonNotValid; }
            set { SetPropertyValue<string>(nameof(ReasonNotValid), ref _ReasonNotValid, value); }
        }





        private double _GrazedArea;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public double GrazedArea
        {
            get { return _GrazedArea; }
            set { SetPropertyValue<double>(nameof(GrazedArea), ref _GrazedArea, value); }
        }




        private double _Unmapped;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public double Unmapped
        {
            get { return _Unmapped; }
            set { SetPropertyValue<double>(nameof(Unmapped), ref _Unmapped, value); }
        }






        //end of MRG fields



        private string _Code;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }




        public override string ToString()
        {
            return Name;
        }

        [Association("Locality-Units")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }
        [VisibleInDetailView(false)]
        [Association("Locality1-Units1")]
        public XPCollection<Unit> Units1
        {
            get
            {
                return GetCollection<Unit>("Units1");
            }
        }
        /*  [Association("Groups-Localities")]
          public XPCollection<Group> Groups
          {
              get
              {
                  return GetCollection<Group>("Groups");
              }
          }*/


        public Locality(Session session) : base(session) { }


        private Locality _ValidLocality;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Locality ValidLocality
        {
            get { return _ValidLocality; }
            set { SetPropertyValue<Locality>(nameof(ValidLocality), ref _ValidLocality, value); }
        }



        private LocalityAdm _Category;
        [Association("Locality-Administrative")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public LocalityAdm Category
        {
            get { return _Category; }
            set { SetPropertyValue<LocalityAdm>(nameof(Category), ref _Category, value); }
        }


     

        [Persistent]
        [Size(1000)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Label;


        private string _Status;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Status
        {
            get { return _Status; }
            set { SetPropertyValue<string>(nameof(Status), ref _Status, value); }
        }


        private double _Altitude1;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Altitude1
        {
            get { return _Altitude1; }
            set { SetPropertyValue<double>(nameof(Altitude1), ref _Altitude1, value); }
        }



        private double _Altitude2;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Altitude2
        {
            get { return _Altitude2; }
            set { SetPropertyValue<double>(nameof(Altitude2), ref _Altitude2, value); }
        }



        [Association("Locality-LocalityVisits")]
        public XPCollection<LocalityVisit> LocalityVisits
        {
            get
            {
                return GetCollection<LocalityVisit>("LocalityVisits");
            }
        }

        private double _Area;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Area
        {
            get { return _Area; }
            set { SetPropertyValue<double>(nameof(Area), ref _Area, value); }
        }


        private string _CoordSource;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CoordSource
        {
            get { return _CoordSource; }
            set { SetPropertyValue<string>(nameof(CoordSource), ref _CoordSource, value); }
        }


        private double _Length;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Length
        {
            get { return _Length; }
            set { SetPropertyValue<double>(nameof(Length), ref _Length, value); }
        }



        private string _WKT;
        [Size(SizeAttribute.Unlimited)]
        public string WKT
        {
            get { return _WKT; }
            set { SetPropertyValue<string>(nameof(WKT), ref _WKT, value); }
        }




        private Decimal? _Centroid_X;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_X
        {
            get { return _Centroid_X; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_X), ref _Centroid_X, value); }
        }



        private Decimal? _Centroid_Y;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_Y
        {
            get { return _Centroid_Y; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_Y), ref _Centroid_Y, value); }
        }



        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }

        private double _Latitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Latitude
        {
            get { return _Latitude; }
            set { SetPropertyValue<double>(nameof(Latitude), ref _Latitude, value); }
        }


        private double _Longitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Longitude
        {
            get { return _Longitude; }
            set { SetPropertyValue<double>(nameof(Longitude), ref _Longitude, value); }
        }


        [Association("LocalityNotes"), Aggregated]
        public XPCollection<LocalityNote> Notes
        {
            get
            {
                return GetCollection<LocalityNote>("Notes");
            }
        }

      
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Title { get { return Name; } }

        private float _Value;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public float Value
        {
            get { return _Value; }
            set { SetPropertyValue<float>(nameof(Value), ref _Value, value); }
        }


        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Tooltip { get { return Name; } }

        /*string IBaseMapsMarker.Title
        {
            get { return Name; }
        }*/


        private string _Country;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Country
        {
            get { return _Country; }
            set { SetPropertyValue<string>(nameof(Country), ref _Country, value); }
        }


        private string _Province;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Province
        {
            get { return _Province; }
            set { SetPropertyValue<string>(nameof(Province), ref _Province, value); }
        }



        private string _District;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string District
        {
            get { return _District; }
            set { SetPropertyValue<string>(nameof(District), ref _District, value); }
        }



        private string _ParsePattern;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string ParsePattern
        {
            get { return _ParsePattern; }
            set { SetPropertyValue<string>(nameof(ParsePattern), ref _ParsePattern, value); }
        }




    }


}
