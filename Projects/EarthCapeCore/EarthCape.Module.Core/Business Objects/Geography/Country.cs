using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Core
{
     public class ProjectCountry : BaseObject,IName 
    {
        public ProjectCountry(Session session) : base(session) { }
    
        private string phoneCode;
        public string PhoneCode
        {
            get { return phoneCode; }
            set
            {
                phoneCode = value;
                OnChanged("PhoneCode");
            }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private string _Comment;
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }





        [Association("CountriesProjects")]
        public XPCollection<Project> Projects
        {
            get { return GetCollection<Project>(nameof(Projects)); }
        }


        private string _CountryCode;
        public string CountryCode
        {
            get { return _CountryCode; }
            set { SetPropertyValue<string>(nameof(CountryCode), ref _CountryCode, value); }
        }



    }
}
