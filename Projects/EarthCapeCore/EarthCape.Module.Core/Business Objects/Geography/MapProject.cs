
using DevExpress.ExpressApp;

using DevExpress.ExpressApp.Editors;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

using System;
using System.ComponentModel;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    /*  public enum MapSummaryType
      { 
          SpeciesCount,
          OccurrenceCount,
          LocalityVisitCount,
          Undefined
      }*/

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class MapProject : BaseObject, IName
    {

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        public MapProject(Session session) : base(session) { }

        protected override void OnSaving()
        {
            /*  if (EPSG<1)
                  if (Value_WindowController.Instance() != null)
                  {
                      Project Project = Value_WindowController.Instance().GetGroup(Session);
               //       EPSG = Project.EPSG;
                  }*/
            base.OnSaving();
        }


        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }
        public override string ToString()
        {
            return Name;
        }

        /*  [NonPersistent]
          [VisibleInListView(false)]
          public MapWindow MapWindow
          {
              get
              {
                  return new MapWindow(Session);
              }
          }
      */

        private string _Name;
        [Size(SizeAttribute.Unlimited)]
        [Custom("RowCount", "1")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        private string _ProjectConfig;
        [Size(SizeAttribute.Unlimited)]
        public string ProjectConfig
        {
            get { return _ProjectConfig; }
            set { SetPropertyValue<string>(nameof(ProjectConfig), ref _ProjectConfig, value); }
        }


  
    }

}
