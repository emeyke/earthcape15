
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;

using System.ComponentModel;
using TatukGIS.NDK;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class LocalityVisit : BaseObject, ILocalityObject, IWKT, ITreeNode, IProjectObject, IName, IVectorMapsMarker, IMapsMarker
    {


        private int? _CoordAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? CoordAccuracy
        {
            get { return _CoordAccuracy; }
            set { SetPropertyValue<int?>(nameof(CoordAccuracy), ref _CoordAccuracy, value); }
        }

        private GeometryType _GeometryType = GeometryType.Point;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public GeometryType GeometryType
        {
            get { return _GeometryType; }
            set { SetPropertyValue<GeometryType>(nameof(GeometryType), ref _GeometryType, value); }
        }


        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public IMapsMarker Map
        {
            get { return this; }
        }

        private string _Weather;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Weather
        {
            get { return _Weather; }
            set { SetPropertyValue<string>(nameof(Weather), ref _Weather, value); }
        }



        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Title { get { return Name; } }

        private float _Value;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]

        public float Value
        {
            get
            {
                return _Value;
            }
            set
            {
                SetPropertyValue("Value", ref _Value, value);
            }
        }
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Tooltip { get { return Name; } }


        private double? _Temp;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Temp
        {
            get { return _Temp; }
            set { SetPropertyValue<double?>(nameof(Temp), ref _Temp, value); }
        }



        private double? _Density;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Density
        {
            get { return _Density; }
            set { SetPropertyValue<double?>(nameof(Density), ref _Density, value); }
        }




        private double? _Salinity;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Salinity
        {
            get { return _Salinity; }
            set { SetPropertyValue<double?>(nameof(Salinity), ref _Salinity, value); }
        }



        private double? _Pressure;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Pressure
        {
            get { return _Pressure; }
            set { SetPropertyValue<double?>(nameof(Pressure), ref _Pressure, value); }
        }




        private double? _SoundVel;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? SoundVel
        {
            get { return _SoundVel; }
            set { SetPropertyValue<double?>(nameof(SoundVel), ref _SoundVel, value); }
        }



        private Habitat _Habitat;
        public Habitat Habitat
        {
            get { return _Habitat; }
            set { SetPropertyValue<Habitat>(nameof(Habitat), ref _Habitat, value); }
        }



        private string _Team;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Team
        {
            get { return _Team; }
            set { SetPropertyValue<string>(nameof(Team), ref _Team, value); }
        }


        private string _QualityCheck;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string QualityCheck
        {
            get { return _QualityCheck; }
            set { SetPropertyValue<string>(nameof(QualityCheck), ref _QualityCheck, value); }
        }


        private string _Sightings;

        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Sightings
        {
            get { return _Sightings; }
            set { SetPropertyValue<string>(nameof(Sightings), ref _Sightings, value); }
        }


        private int? _Year;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Year
        {
            get { return _Year; }
            set { SetPropertyValue<int?>(nameof(Year), ref _Year, value); }
        }



        private int? _Month;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Month
        {
            get { return _Month; }
            set { SetPropertyValue<int?>(nameof(Month), ref _Month, value); }
        }


        private int? _Day;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Day
        {
            get { return _Day; }
            set { SetPropertyValue<int?>(nameof(Day), ref _Day, value); }
        }



        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }





        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            GISHelper.UpdateAreaAndLengthFromWKT(this);
            if (IsLoading) return;
            if (((Year == null) || (Year == 0)) && (DateTimeStart != null))
                Year = DateTimeStart.Value.Year;
            if ((String.IsNullOrEmpty(Name)) && (DateTimeStart != null) && (DateTimeEnd != null)) { Name = Locality.Name + "-" + DateTimeStart.Value.ToString("yyyyMMdd") + "-" + DateTimeEnd.Value.ToString("yyyyMMdd"); }
            else
            if ((String.IsNullOrEmpty(Name)) && (DateTimeStart != null)) { Name = Locality.Name + "-" + DateTimeStart.Value.ToString("yyyyMMdd"); }
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        private Project _Project;
        [Association("Project-LocalityVisits")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }




        private string _Name;
        [Size(SizeAttribute.Unlimited)]
        [Custom("RowCount", "1")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private double? _TimeSpent;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? TimeSpent
        {
            get { return _TimeSpent; }
            set { SetPropertyValue<double?>(nameof(TimeSpent), ref _TimeSpent, value); }
        }



        private string _TimeSpentUnit;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string TimeSpentUnit
        {
            get { return _TimeSpentUnit; }
            set { SetPropertyValue<string>(nameof(TimeSpentUnit), ref _TimeSpentUnit, value); }
        }



        private DateTime? _DateTimeStart;
        [RuleRequiredField(DefaultContexts.Save)]
        public DateTime? DateTimeStart
        {
            get { return _DateTimeStart; }
            set { SetPropertyValue<DateTime?>(nameof(DateTimeStart), ref _DateTimeStart, value); }
        }


        private DateTime? _DateTimeEnd;
        public DateTime? DateTimeEnd
        {
            get { return _DateTimeEnd; }
            set { SetPropertyValue<DateTime?>(nameof(DateTimeEnd), ref _DateTimeEnd, value); }
        }




        private double _Area;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Area
        {
            get { return _Area; }
            set { SetPropertyValue<double>(nameof(Area), ref _Area, value); }
        }


        private string _CoordSource;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CoordSource
        {
            get { return _CoordSource; }
            set { SetPropertyValue<string>(nameof(CoordSource), ref _CoordSource, value); }
        }

        private string _EventTime;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string EventTime
        {
            get { return _EventTime; }
            set { SetPropertyValue<string>(nameof(EventTime), ref _EventTime, value); }
        }



        private double _Length;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Length
        {
            get { return _Length; }
            set { SetPropertyValue<double>(nameof(Length), ref _Length, value); }
        }



        private string _WKT;
        [Size(SizeAttribute.Unlimited)]
        public string WKT
        {
            get { return _WKT; }
            set { SetPropertyValue<string>(nameof(WKT), ref _WKT, value); }
        }




        private Decimal? _Centroid_X;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_X
        {
            get { return _Centroid_X; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_X), ref _Centroid_X, value); }
        }



        private Decimal? _Centroid_Y;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_Y
        {
            get { return _Centroid_Y; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_Y), ref _Centroid_Y, value); }
        }



        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }

        private double _Latitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Latitude
        {
            get { return _Latitude; }
            set { SetPropertyValue<double>(nameof(Latitude), ref _Latitude, value); }
        }


        private double _Longitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Longitude
        {
            get { return _Longitude; }
            set { SetPropertyValue<double>(nameof(Longitude), ref _Longitude, value); }
        }


        private string _Method;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Method
        {
            get { return _Method; }
            set { SetPropertyValue<string>(nameof(Method), ref _Method, value); }
        }


        private double _Altitude1;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Altitude1
        {
            get { return _Altitude1; }
            set { SetPropertyValue<double>(nameof(Altitude1), ref _Altitude1, value); }
        }



        private double _Altitude2;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Altitude2
        {
            get { return _Altitude2; }
            set { SetPropertyValue<double>(nameof(Altitude2), ref _Altitude2, value); }
        }

        private int? _AltitudePrecision;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? AltitudePrecision
        {
            get { return _AltitudePrecision; }
            set { SetPropertyValue<int?>(nameof(AltitudePrecision), ref _AltitudePrecision, value); }
        }


        private int? _DepthPrecision;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? DepthPrecision
        {
            get { return _DepthPrecision; }
            set { SetPropertyValue<int?>(nameof(DepthPrecision), ref _DepthPrecision, value); }
        }


        private int? _DateAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? DateAccuracy
        {
            get { return _DateAccuracy; }
            set { SetPropertyValue<int?>(nameof(DateAccuracy), ref _DateAccuracy, value); }
        }



        private double _Depth1;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Depth1
        {
            get { return _Depth1; }
            set { SetPropertyValue<double>(nameof(Depth1), ref _Depth1, value); }
        }


        private double _Depth2;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Depth2
        {
            get { return _Depth2; }
            set { SetPropertyValue<double>(nameof(Depth2), ref _Depth2, value); }
        }


        private Method _SamplingMethod;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Method SamplingMethod
        {
            get { return _SamplingMethod; }
            set { SetPropertyValue<Method>(nameof(SamplingMethod), ref _SamplingMethod, value); }
        }




        private Locality _Locality1;
        public Locality Locality1
        {
            get { return _Locality1; }
            set { SetPropertyValue<Locality>(nameof(Locality1), ref _Locality1, value); }
        }




        private string _SamplingPart;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string SamplingPart
        {
            get { return _SamplingPart; }
            set { SetPropertyValue<string>(nameof(SamplingPart), ref _SamplingPart, value); }
        }




        #region ITreeNode
        IBindingList ITreeNode.Children
        {
            get
            {
                return Children;
            }
        }
        string ITreeNode.Name
        {
            get
            {
                return this.ToString();
            }
        }
        ITreeNode ITreeNode.Parent
        {
            get
            {
                return Parent;
            }
        }
        #endregion
        public LocalityVisit(Session session)    : base(session)        {        }

        private LocalityVisit _Parent;
        [Association("Object-Children")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public LocalityVisit Parent
        {
            get { return _Parent; }
            set { SetPropertyValue<LocalityVisit>(nameof(Parent), ref _Parent, value); }
        }


        [Association("Object-Children")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public XPCollection<LocalityVisit> Children
        {
            get { return GetCollection<LocalityVisit>("Children"); }
        }

        private Locality _Locality;
        // [RuleRequiredField("Locality is required", DefaultContexts.Save, "Locality must be specified for Locality visit")]
        [Association("Locality-LocalityVisits")]
        public Locality Locality
        {
            get { return _Locality; }
            set { SetPropertyValue<Locality>(nameof(Locality), ref _Locality, value); }
        }


        [Association("LocalityVisit-Units")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }

        private int? _Temperature;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Temperature
        {
            get { return _Temperature; }
            set { SetPropertyValue<int?>(nameof(Temperature), ref _Temperature, value); }
        }



        private int? _Cloud;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Cloud
        {
            get { return _Cloud; }
            set { SetPropertyValue<int?>(nameof(Cloud), ref _Cloud, value); }
        }


        private int? _Wind;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Wind
        {
            get { return _Wind; }
            set { SetPropertyValue<int?>(nameof(Wind), ref _Wind, value); }
        }




        private string _Precipitation;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Precipitation
        {
            get { return _Precipitation; }
            set { SetPropertyValue<string>(nameof(Precipitation), ref _Precipitation, value); }
        }


        private string _Humidity;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Humidity
        {
            get { return _Humidity; }
            set { SetPropertyValue<string>(nameof(Humidity), ref _Humidity, value); }
        }


        private double _SnowDepth;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double SnowDepth
        {
            get { return _SnowDepth; }
            set { SetPropertyValue<double>(nameof(SnowDepth), ref _SnowDepth, value); }
        }



        private string _Zone;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Zone
        {
            get { return _Zone; }
            set { SetPropertyValue<string>(nameof(Zone), ref _Zone, value); }
        }
    }
}
