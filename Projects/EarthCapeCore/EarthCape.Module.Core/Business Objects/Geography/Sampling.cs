using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class Sampling : Locality
    {
        public Sampling(Session session) : base(session) { }
        [Association("Sampling-Parts")]
        public XPCollection<SamplingPart> Parts
        {
            get
            {
                return GetCollection<SamplingPart>("Parts");
            }
        }
        // Fields...
        private Method _SamplingMethod;
        [Association("Sampling-LocalityVisits")]
        [VisibleInDetailView(false)]
        public XPCollection<LocalityVisit> SamplingVisits
        {
            get
            {
                return GetCollection<LocalityVisit>("SamplingVisits");
            }
        }
        public Method SamplingMethod
        {
            get
            {
                return _SamplingMethod;
            }
            set
            {
                SetPropertyValue("SamplingMethod", ref _SamplingMethod, value);
            }
        }
    }
}
