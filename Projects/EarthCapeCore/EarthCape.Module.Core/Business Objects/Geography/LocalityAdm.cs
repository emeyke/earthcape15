
using DevExpress.ExpressApp;

using DevExpress.Persistent.Base;

using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.Validation;

using DevExpress.Xpo;
using System;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class LocalityAdm : Locality, ITreeNode
    {
        public LocalityAdm(Session session) : base(session) { }

        private LocalityAdm parent;
        [Association("Object-Children")]
        public LocalityAdm Parent
        {
            get { return parent; }
            set
            {
                parent = value;
            }
        }
        [Association("Object-Children")]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public XPCollection<LocalityAdm> Children
        {
            get { return GetCollection<LocalityAdm>("Children"); }
        }
        [Association("Locality-Administrative")]
        public XPCollection<Locality> Localities
        {
            get
            {
                return GetCollection<Locality>("Localities");
            }
        }
        private XPCollection<Locality> _Localities;
        public XPCollection<Locality> AllLocalities
        {
            get
            {
                if (_Localities == null)
                {
                    _Localities = new XPCollection<Locality>(Session, false);
                    CollectLocalitiesRecursive(this, _Localities);
                    _Localities.BindingBehavior = CollectionBindingBehavior.AllowNone;
                }
                return _Localities;
            }
        }
        private void CollectLocalitiesRecursive(LocalityAdm localityAdm, XPCollection<Locality> target)
        {
            foreach (Locality locality in localityAdm.Localities)
            {
                target.Add(locality);

            }
            foreach (LocalityAdm childCategory in localityAdm.Children)
            {
                CollectLocalitiesRecursive(childCategory, target);
            }
        }
        #region ITreeNode Members

        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get { return Children; }
        }

        string ITreeNode.Name
        {
            get { return Name; }
        }

        ITreeNode ITreeNode.Parent
        {
            get { return Parent; }
        }

        #endregion

    }
}
