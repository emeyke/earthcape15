using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class ForestQuarter : Locality
    {
        public ForestQuarter(Session session) : base(session) { }

        [Association("Quarter-Stands")]
        public XPCollection<ForestStand> Stands
        {
            get
            {
                return GetCollection<ForestStand>("Stands");
            }
        }
    }
}
