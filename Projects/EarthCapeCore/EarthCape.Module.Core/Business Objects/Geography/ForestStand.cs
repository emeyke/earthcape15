using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class ForestStand : Locality
    {
        public ForestStand(Session session) : base(session) { }

        // Fields...
        private ForestQuarter _Quarter;

        [Association("Quarter-Stands")]
        public ForestQuarter Quarter
        {
            get
            {
                return _Quarter;
            }
            set
            {
                SetPropertyValue("Quarter", ref _Quarter, value);
            }
        }
      
    }
}
