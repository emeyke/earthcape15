using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;

using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class LocalityAdm3 : LocalityAdm
    {
        public LocalityAdm3(Session session) : base(session) { }

    }
}
