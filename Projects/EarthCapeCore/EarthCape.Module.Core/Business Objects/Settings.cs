﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using LlamachantFramework.Module.Interfaces;

namespace EarthCape.Module.Core
{
    public class Settings : BaseObject, ISingletonBO
    {   public Settings(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

         private string _INatUserName;
        [VisibleInDetailView(false)]
         public string INatUserName
         {
             get { return _INatUserName; }
             set { SetPropertyValue<string>(nameof(INatUserName), ref _INatUserName, value); }
         }

    /*    private int? _ZenodoFileDeposition;
        public int? ZenodoFileDeposition
        {
            get { return _ZenodoFileDeposition; }
            set { SetPropertyValue<int?>(nameof(ZenodoFileDeposition), ref _ZenodoFileDeposition, value); }
        }

      /*  private int? _ZenodoSandboxFileDeposition;
        public int? ZenodoSandboxFileDeposition
        {
            get { return _ZenodoSandboxFileDeposition; }
            set { SetPropertyValue<int?>(nameof(ZenodoSandboxFileDeposition), ref _ZenodoSandboxFileDeposition, value); }
        }
        */




        private string _GbifAdminUser;
        public string GbifAdminUser
        {
            get { return _GbifAdminUser; }
            set { SetPropertyValue<string>(nameof(GbifAdminUser), ref _GbifAdminUser, value); }
        }

        private string _GbifAdminPassword;
        [ModelDefault("IsPassword", "True")]
        public string GbifAdminPassword
        {
            get { return _GbifAdminPassword; }
            set { SetPropertyValue<string>(nameof(GbifAdminPassword), ref _GbifAdminPassword, value); }
        }

        private string _GbifSandboxAdminUser="earthcape_test";
        public string GbifSandboxAdminUser
        {
            get { return _GbifSandboxAdminUser; }
            set { SetPropertyValue<string>(nameof(GbifSandboxAdminUser), ref _GbifSandboxAdminUser, value); }
        }

        private string _GbifSandboxAdminPassword="earthcape_test";
        [ModelDefault("IsPassword", "True")]
        public string GbifSandboxAdminPassword
        {
            get { return _GbifSandboxAdminPassword; }
            set { SetPropertyValue<string>(nameof(GbifSandboxAdminPassword), ref _GbifSandboxAdminPassword, value); }
        }

        private string _Host;
        public string Host
        {
            get { return _Host; }
            set { SetPropertyValue<string>(nameof(Host), ref _Host, value); }
        }


      /*  private string _DwcaUrl;
        public string DwcaUrl
        {
            get { return _DwcaUrl; }
            set { SetPropertyValue<string>(nameof(DwcaUrl), ref _DwcaUrl, value); }
        }
        */

     

       /*private string _GbifApi;
        [EditorAlias("HyperLinkPropertyEditor")]
        public string GbifApi
        {
            get { return _GbifApi; }
            set { SetPropertyValue<string>(nameof(GbifApi), ref _GbifApi, value); }
        }*/

        private string _GbifInstallationKey;
        public string GbifInstallationKey
        {
            get { return _GbifInstallationKey; }
            set { SetPropertyValue<string>(nameof(GbifInstallationKey), ref _GbifInstallationKey, value); }
        }



        private string _GbifPublisherKey;
        [Size(255)]
        public string GbifPublisherKey
        {
            get { return _GbifPublisherKey; }
            set { SetPropertyValue<string>(nameof(GbifPublisherKey), ref _GbifPublisherKey, value); }
        }

        private string _GbifEndorsingNode;
        public string GbifEndorsingNode
        {
            get { return _GbifEndorsingNode; }
            set { SetPropertyValue<string>(nameof(GbifEndorsingNode), ref _GbifEndorsingNode, value); }
        }



        private string _GbifSandboxInstallationKey;
        public string GbifSandboxInstallationKey
        {
            get { return _GbifSandboxInstallationKey; }
            set { SetPropertyValue<string>(nameof(GbifSandboxInstallationKey), ref _GbifSandboxInstallationKey, value); }
        }



        private string _GbifSandboxPublisherKey;
        [Size(255)]
        public string GbifSandboxPublisherKey
        {
            get { return _GbifSandboxPublisherKey; }
            set { SetPropertyValue<string>(nameof(GbifSandboxPublisherKey), ref _GbifSandboxPublisherKey, value); }
        }

        private string _GbifSandboxEndorsingNode;
        public string GbifSandboxEndorsingNode
        {
            get { return _GbifSandboxEndorsingNode; }
            set { SetPropertyValue<string>(nameof(GbifSandboxEndorsingNode), ref _GbifSandboxEndorsingNode, value); }
        }


        private string _ZenodoToken;
        [Size(255)]
        [ModelDefault("IsPassword", "True")]
        public string ZenodoToken
        {
            get { return _ZenodoToken; }
            set { SetPropertyValue<string>(nameof(ZenodoToken), ref _ZenodoToken, value); }
        }

        private string _ZenodoSandboxToken;
        [Size(255)]
        [ModelDefault("IsPassword", "True")]
        public string ZenodoSandboxToken
        {
            get { return _ZenodoSandboxToken; }
            set { SetPropertyValue<string>(nameof(ZenodoSandboxToken), ref _ZenodoSandboxToken, value); }
        }

     


        private License _DefaultMediaLicense;
        public License DefaultMediaLicense
        {
            get { return _DefaultMediaLicense; }
            set { SetPropertyValue<License>(nameof(DefaultMediaLicense), ref _DefaultMediaLicense, value); }
        }


        private string _ZenodoSandboxCommunities="earthcape-test";
        [Size(SizeAttribute.Unlimited)]
        public string ZenodoSandboxCommunities
        {
            get { return _ZenodoSandboxCommunities; }
            set { SetPropertyValue<string>(nameof(ZenodoSandboxCommunities), ref _ZenodoSandboxCommunities, value); }
        }

        private string _ZenodoCommunities="earthcape";
        [Size(SizeAttribute.Unlimited)]
        public string ZenodoCommunities
        {
            get { return _ZenodoCommunities; }
            set { SetPropertyValue<string>(nameof(ZenodoCommunities), ref _ZenodoCommunities, value); }
        }

        private string _INaturalistToken;
        [Size(SizeAttribute.Unlimited)]
        [ModelDefault("RowCount","1")]
        public string INaturalistToken
        {
            get { return _INaturalistToken; }
            set { SetPropertyValue<string>(nameof(INaturalistToken), ref _INaturalistToken, value); }
        }


        private bool _SortUnitBySequence=false;
        public bool SortUnitBySequence
        {
            get { return _SortUnitBySequence; }
            set { SetPropertyValue<bool>(nameof(SortUnitBySequence), ref _SortUnitBySequence, value); }
        }












    }
}