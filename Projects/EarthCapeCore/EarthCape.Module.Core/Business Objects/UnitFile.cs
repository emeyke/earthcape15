using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security.Strategy;

namespace EarthCape.Module.Core
{
  //  [DefaultClassOptions]
  //  [DefaultProperty("Text")]
    public class UnitFile : BaseObject
    {
        private string _Comment;
        private Unit _Unit;
        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser CreatedBy;
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser LastModifiedBy;

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
               }
        }
   
        
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
    
                if (SecuritySystem.CurrentUser != null)
                {
                    
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
      }
        public UnitFile(Session session) : base(session) { }
         
      
        private DateTime _RecordedOn;
       //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        public DateTime RecordedOn
        {
            get
            {
                return _RecordedOn;
            }
            set
            {
                SetPropertyValue("RecordedOn", ref _RecordedOn, value);
            }
        }

        [Association("Unit-UnitFile")]
        public Unit Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetPropertyValue("Unit", ref _Unit, value);
            }
        }

        private FileItem _File;
        [Association("File-UnitFile")]
        public FileItem File
        {
            get
            {
                return _File;
            }
            set
            {
                SetPropertyValue("File", ref _File, value);
            }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
    }

}
