using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace EarthCape.Module.Core
{
    [Appearance("Hide_NewAction_for_SequenceReleasedObject", AppearanceItemType.Action, "1=1", TargetItems = "New", Visibility = ViewItemVisibility.Hide)]
    public class EarthCapeSequenceReleasedObject : XPCustomObject, ISequenceReleasedObject
    {
        public EarthCapeSequenceReleasedObject(Session session)
            : base(session)
        {
        }
        private EarthCapeSequenceObject _sequenceObject;
        [VisibleInListView(false)]
        [Association("EarthCapeSequenceObject-XpandReleasedSequences")]
        public EarthCapeSequenceObject SequenceObject
        {
            get
            {
                return _sequenceObject;
            }
            set
            {
                SetPropertyValue("SequenceObject", ref _sequenceObject, value);
            }
        }
        private long _sequence;

        ISequenceObject ISequenceReleasedObject.SequenceObject
        {
            get { return SequenceObject; }
            set { SequenceObject = value as EarthCapeSequenceObject; }
        }

        public long Sequence
        {
            get
            {
                return _sequence;
            }
            set
            {
                SetPropertyValue("Sequence", ref _sequence, value);
            }
        }
    }
}
