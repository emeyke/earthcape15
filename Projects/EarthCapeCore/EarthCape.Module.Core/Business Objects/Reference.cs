
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;

using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DisplayProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class Reference : BaseObject, IProjectObject, IName, ICode
    {
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            if ( (DateReleased != null))
            {
                _Year = DateReleased.Year;
            }
            string auth = string.Empty;
            string s = string.Empty;
            if (!String.IsNullOrEmpty(Authors))
            {
                auth = Authors;
                if (Authors.Length > 20)
                {
                    auth = String.Format("{0}..", Authors.Substring(0, 15));
                }
            }
            s = String.Format("{0} {1} {2} {3}", auth, Year, Title, Volume);
            FullName = s;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }





        private string _Comment;

        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }



        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [ModelDefault("RowCount", "1")]
        [Size(4000)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }


        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Name
        {
            get
            {
                return Title;
            }
        }


        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }

        [Association("Projects-References")]
        public XPCollection<Project> Projects
        {
            get { return GetCollection<Project>("Projects"); }
        }

        public Reference(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
     

        private string _YearString;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string YearString
        {
            get { return _YearString; }
            set { SetPropertyValue<string>(nameof(YearString), ref _YearString, value); }
        }

        private string _SourceId;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string SourceId
        {
            get { return _SourceId; }
            set { SetPropertyValue<string>(nameof(SourceId), ref _SourceId, value); }
        }



        private string _SourceUrl;
        [ModelDefault("RowCount", "1")]
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("Reference.SourceUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string SourceUrl
        {
            get { return _SourceUrl; }
            set { SetPropertyValue<string>(nameof(SourceUrl), ref _SourceUrl, value); }
        }



        private string _SourceContributor;
        [Size(255)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string SourceContributor
        {
            get { return _SourceContributor; }
            set { SetPropertyValue<string>(nameof(SourceContributor), ref _SourceContributor, value); }
        }

        private string _CopyrightRegion;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string CopyrightRegion
        {
            get { return _CopyrightRegion; }
            set { SetPropertyValue<string>(nameof(CopyrightRegion), ref _CopyrightRegion, value); }
        }



        private string _Language;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Language
        {
            get { return _Language; }
            set { SetPropertyValue<string>(nameof(Language), ref _Language, value); }
        }



        private string _Edition;
        public string Edition
        {
            get { return _Edition; }
            set { SetPropertyValue<string>(nameof(Edition), ref _Edition, value); }
        }

        private Image _Thumbnail;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public Image Thumbnail
        {
            get { return _Thumbnail; }
            set { SetPropertyValue<Image>(nameof(Thumbnail), ref _Thumbnail, value); }
        }


        private string _CopyrightStatus;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string CopyrightStatus
        {
            get { return _CopyrightStatus; }
            set { SetPropertyValue<string>(nameof(CopyrightStatus), ref _CopyrightStatus, value); }
        }


        private string _CopyrightNotice;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        public string CopyrightNotice
        {
            get { return _CopyrightNotice; }
            set { SetPropertyValue<string>(nameof(CopyrightNotice), ref _CopyrightNotice, value); }
        }




        private string _Url;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [Size(255)]
        [ModelDefault("RowCount", "1")]
        [RuleRegularExpression("Reference.Url.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        public string Url
        {
            get { return _Url; }
            set { SetPropertyValue<string>(nameof(Url), ref _Url, value); }
        }



        private string _Title;
        [Size(2000)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        public string Title
        {
            get { return _Title; }
            set { SetPropertyValue<string>(nameof(Title), ref _Title, value); }
        }



        private string _ShortTitle;
        [Size(255)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string ShortTitle
        {
            get { return _ShortTitle; }
            set { SetPropertyValue<string>(nameof(ShortTitle), ref _ShortTitle, value); }
        }



        private DateTime _DateReleased;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public DateTime DateReleased
        {
            get { return _DateReleased; }
            set { SetPropertyValue<DateTime>(nameof(DateReleased), ref _DateReleased, value); }
        }



     


        private int _Day;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public int Day
        {
            get { return _Day; }
            set { SetPropertyValue<int>(nameof(Day), ref _Day, value); }
        }


        private int _Month;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public int Month
        {
            get { return _Month; }
            set { SetPropertyValue<int>(nameof(Month), ref _Month, value); }
        }


        private int _Year;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public int Year
        {
            get { return _Year; }
            set { SetPropertyValue<int>(nameof(Year), ref _Year, value); }
        }

        private DateTime _DateCreated;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue<DateTime>(nameof(DateCreated), ref _DateCreated, value); }
        }



        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }

        private string _Authors;
        [Size(2000)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        public string Authors
        {
            get { return _Authors; }
            set { SetPropertyValue<string>(nameof(Authors), ref _Authors, value); }
        }



        [Persistent]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [Size(255)]
        [ModelDefault("RowCount", "1")]
        public string AuthorsShort
        {
            get
            {
                string auth = Authors;
                if (!string.IsNullOrEmpty(Authors))
                    if (Authors.Length > 255)
                    {
                        auth = String.Format("{0}..", Authors.Substring(0, 250));
                    }
                return auth;
            }
        }
        [Association("Reference-Taxa")]
        public XPCollection<TaxonomicName> Taxa
        {
            get
            {
                return GetCollection<TaxonomicName>("Taxa");
            }
        }


        private string _Abstract;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        public string Abstract
        {
            get { return _Abstract; }
            set { SetPropertyValue<string>(nameof(Abstract), ref _Abstract, value); }
        }


        public override string ToString()
        {
            return FullName;
        }
        /*   [Persistent]
           [Size(255)]
           public string FullName
           {
               get
               {
                   string auth = "";
                   string s = "";
                   if (!String.IsNullOrEmpty(Authors))
                   {
                       auth = Authors;
                       if (Authors.Length > 20)
                       {
                           auth = String.Format("{0}..", Authors.Substring(0, 15));
                       }
                   }
                   s = String.Format("{0} {1} {2} {3}",auth, Year, Title, Volume);
                   return s;
               }
           }*/

        //private FileAttachment file;


        private RefType _Type;
        [VisibleInListView(true)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public RefType Type
        {
            get { return _Type; }
            set { SetPropertyValue<RefType>(nameof(Type), ref _Type, value); }
        }


        private string _Authors2;
        [Size(500)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Authors2
        {
            get { return _Authors2; }
            set { SetPropertyValue<string>(nameof(Authors2), ref _Authors2, value); }
        }


        private string _Authors3;
        [Size(500)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Authors3
        {
            get { return _Authors3; }
            set { SetPropertyValue<string>(nameof(Authors3), ref _Authors3, value); }
        }



        private string _Title2;
        [ModelDefault("RowCount", "1")]
        [Size(1000)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Title2
        {
            get { return _Title2; }
            set { SetPropertyValue<string>(nameof(Title2), ref _Title2, value); }
        }



        private string _Title3;
        [Size(1000)]
        [VisibleInListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Title3
        {
            get { return _Title3; }
            set { SetPropertyValue<string>(nameof(Title3), ref _Title3, value); }
        }



        private string _AlternativeTitle;
        [Size(1000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string AlternativeTitle
        {
            get { return _AlternativeTitle; }
            set { SetPropertyValue<string>(nameof(AlternativeTitle), ref _AlternativeTitle, value); }
        }


        private string _Date2;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        [Size(700)]
        public string Date2
        {
            get { return _Date2; }
            set { SetPropertyValue<string>(nameof(Date2), ref _Date2, value); }
        }


        private string _Jourfull;
        [VisibleInListView(true)]
        [VisibleInLookupListView(true)]
        [Size(1000)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(true)]
        public string Jourfull
        {
            get { return _Jourfull; }
            set { SetPropertyValue<string>(nameof(Jourfull), ref _Jourfull, value); }
        }


        private string _Volume;
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Volume
        {
            get { return _Volume; }
            set { SetPropertyValue<string>(nameof(Volume), ref _Volume, value); }
        }



        private string _Jourab;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Jourab
        {
            get { return _Jourab; }
            set { SetPropertyValue<string>(nameof(Jourab), ref _Jourab, value); }
        }


        private string _Pubcity;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Pubcity
        {
            get { return _Pubcity; }
            set { SetPropertyValue<string>(nameof(Pubcity), ref _Pubcity, value); }
        }



        private string _Publisher;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Publisher
        {
            get { return _Publisher; }
            set { SetPropertyValue<string>(nameof(Publisher), ref _Publisher, value); }
        }




        private string _Availability;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Availability
        {
            get { return _Availability; }
            set { SetPropertyValue<string>(nameof(Availability), ref _Availability, value); }
        }




        private string _Pubaddress;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Pubaddress
        {
            get { return _Pubaddress; }
            set { SetPropertyValue<string>(nameof(Pubaddress), ref _Pubaddress, value); }
        }


        private string _SN;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string SN
        {
            get { return _SN; }
            set { SetPropertyValue<string>(nameof(SN), ref _SN, value); }
        }



        private string _DOI;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string DOI
        {
            get { return _DOI; }
            set { SetPropertyValue<string>(nameof(DOI), ref _DOI, value); }
        }




        private string _NumberOfVolumes;
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string NumberOfVolumes
        {
            get { return _NumberOfVolumes; }
            set { SetPropertyValue<string>(nameof(NumberOfVolumes), ref _NumberOfVolumes, value); }
        }


        private string _DigitalCopyComments;
        [Size(1000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string DigitalCopyComments
        {
            get { return _DigitalCopyComments; }
            set { SetPropertyValue<string>(nameof(DigitalCopyComments), ref _DigitalCopyComments, value); }
        }


        private string _OtherID;
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string OtherID
        {
            get { return _OtherID; }
            set { SetPropertyValue<string>(nameof(OtherID), ref _OtherID, value); }
        }


        private string _Code;
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }



        private string _DateAuthority;
        [Size(500)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string DateAuthority
        {
            get { return _DateAuthority; }
            set { SetPropertyValue<string>(nameof(DateAuthority), ref _DateAuthority, value); }
        }


        private string _CallNumber;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string CallNumber
        {
            get { return _CallNumber; }
            set { SetPropertyValue<string>(nameof(CallNumber), ref _CallNumber, value); }
        }



        private string _BookPages;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string BookPages
        {
            get { return _BookPages; }
            set { SetPropertyValue<string>(nameof(BookPages), ref _BookPages, value); }
        }


        private string _Plates;
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Plates
        {
            get { return _Plates; }
            set { SetPropertyValue<string>(nameof(Plates), ref _Plates, value); }
        }

        private string _BookPlates;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string BookPlates
        {
            get { return _BookPlates; }
            set { SetPropertyValue<string>(nameof(BookPlates), ref _BookPlates, value); }
        }



        private string _Libraries;
        [Size(2000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string Libraries
        {
            get { return _Libraries; }
            set { SetPropertyValue<string>(nameof(Libraries), ref _Libraries, value); }
        }



        private string _DigitizationComment;
        [Size(2000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string DigitizationComment
        {
            get { return _DigitizationComment; }
            set { SetPropertyValue<string>(nameof(DigitizationComment), ref _DigitizationComment, value); }
        }


        private string _TaxonomicNote;
        [Size(2000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [VisibleInDetailView(false)]
        public string TaxonomicNote
        {
            get { return _TaxonomicNote; }
            set { SetPropertyValue<string>(nameof(TaxonomicNote), ref _TaxonomicNote, value); }
        }


        private string _Printst;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Printst
        {
            get { return _Printst; }
            set { SetPropertyValue<string>(nameof(Printst), ref _Printst, value); }
        }



        private string _Issue;
        [VisibleInListView(true)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Issue
        {
            get { return _Issue; }
            set { SetPropertyValue<string>(nameof(Issue), ref _Issue, value); }
        }




        private string _Page1;
        [VisibleInListView(true)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Page1
        {
            get { return _Page1; }
            set { SetPropertyValue<string>(nameof(Page1), ref _Page1, value); }
        }





        private string _Page2;
        [VisibleInListView(true)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(700)]
        [ModelDefault("RowCount", "1")]
        public string Page2
        {
            get { return _Page2; }
            set { SetPropertyValue<string>(nameof(Page2), ref _Page2, value); }
        }





        private string _Keywords;
        [VisibleInListView(true)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ModelDefault("RowCount", "1")]
        [Size(2000)]
        public string Keywords
        {
            get { return _Keywords; }
            set { SetPropertyValue<string>(nameof(Keywords), ref _Keywords, value); }
        }




        [Association("Reference-Citations")]
        public XPCollection<Citation> Citations
        {
            get
            {
                return GetCollection<Citation>("Citations");
            }
        }

    }

}
