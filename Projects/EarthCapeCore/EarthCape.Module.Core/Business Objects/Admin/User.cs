using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Data;
using System.ComponentModel;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base.Security;
using System.Security;
using System.Collections.Generic;

using DevExpress.Data.Filtering;
using Xpand.Persistent.Base.General;
using System.Drawing;
using DevExpress.ExpressApp.Model;
using Xpand.ExpressApp.Security.Core;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable), System.ComponentModel.DisplayName("User")]
    public class User : SecuritySystemUser, IPictureItem, ISequenceGeneratorUser
    {
   	   public User(Session session) : base(session) { }
     
       private String _Email;
       [RuleRegularExpression(null, "UserContext", EarthCapeModule.EmailPattern)]
       [EditorAlias("HyperLinkPropertyEditor")]
       [ModelDefault("IsEmail", "True")]
       public String Email
       {
           get
           {
               return _Email;
           }
           set
           {
               SetPropertyValue("Email", ref _Email, value);
           }
       }
       public override void AfterConstruction()
       {
           base.AfterConstruction();
        }
       public string Organization { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
      
       #region IPictureItem Members
       string IPictureItem.ID
       {
           get { return Oid.ToString(); }
       }
       Image IPictureItem.Image
       {
           get { return Photo; }
       }
       private Image _Photo;
       [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
       public Image Photo
       {
           get
           {
               return _Photo;
           }
           set
           {
               SetPropertyValue("Photo", ref _Photo, value);
           }
       }

       private string _imagePath;
       [Browsable(false)]
       public string ImagePath
       {
           get { return _imagePath; }
           set { SetPropertyValue("ImagePath", ref _imagePath, value); }
       }

       #endregion
    }
    

}
