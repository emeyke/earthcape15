#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.


using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Lab;
using System;

using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]

    [ExcelImportKeyAttribute("Name")]
    [DocumentationAttribute("Dataset description")]
    public class Dataset : BaseObject, IProjectObject, IName, ICode, IUnits
    {

        private string _MapMarker;
        [Size(SizeAttribute.Unlimited)]
        [ModelDefault("RowCount","1")]
        public string MapMarker
        {
            get { return _MapMarker; }
            set { SetPropertyValue<string>(nameof(MapMarker), ref _MapMarker, value); }
        }

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private string _GbifProjectCode;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("Caption", "GBIF project")]
        [ModelDefault("Tooltip", "GBIF project")]
        [DocumentationAttribute("Specimen, Observation or other objects that typically have Geographic, Taxonomic and Date information")]
        public string GbifProjectCode
        {
            get { return _GbifProjectCode; }
            set { SetPropertyValue<string>(nameof(GbifProjectCode), ref _GbifProjectCode, value); }
        }



        private string _Keywords;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Keywords
        {
            get { return _Keywords; }
            set { SetPropertyValue<string>(nameof(Keywords), ref _Keywords, value); }
        }


        private bool _GbifPublished;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool GbifPublished
        {
            get { return _GbifPublished; }
            set { SetPropertyValue<bool>(nameof(GbifPublished), ref _GbifPublished, value); }
        }


        private string _GbifKey;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifKey
        {
            get { return _GbifKey; }
            set { SetPropertyValue<string>(nameof(GbifKey), ref _GbifKey, value); }
        }

        private string _DwcaUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [EditorAlias("HyperLinkPropertyEditor")]
        [Size(255)]
        public string DwcaUrl
        {
            get { return _DwcaUrl; }
            set { SetPropertyValue<string>(nameof(DwcaUrl), ref _DwcaUrl, value); }
        }

        private string _ZenodoResponseDeposit;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoResponseDeposit
        {
            get { return _ZenodoResponseDeposit; }
            set { SetPropertyValue<string>(nameof(ZenodoResponseDeposit), ref _ZenodoResponseDeposit, value); }
        }

        private string _ZenodoResponseNewVersion;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoResponseNewVersion
        {
            get { return _ZenodoResponseNewVersion; }
            set { SetPropertyValue<string>(nameof(ZenodoResponseNewVersion), ref _ZenodoResponseNewVersion, value); }
        }

        private string _ZenodoResponseAddFile;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoResponseAddFile
        {
            get { return _ZenodoResponseAddFile; }
            set { SetPropertyValue<string>(nameof(ZenodoResponseAddFile), ref _ZenodoResponseAddFile, value); }
        }

        private string _GbifResponseCreate;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifResponseCreate
        {
            get { return _GbifResponseCreate; }
            set { SetPropertyValue<string>(nameof(GbifResponseCreate), ref _GbifResponseCreate, value); }
        }

        private string _ZenodoRequestMetadata;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoRequestMetadata
        {
            get { return _ZenodoRequestMetadata; }
            set { SetPropertyValue<string>(nameof(ZenodoRequestMetadata), ref _ZenodoRequestMetadata, value); }
        }


        private string _GbifRequestMetadata;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifRequestMetadata
        {
            get { return _GbifRequestMetadata; }
            set { SetPropertyValue<string>(nameof(GbifRequestMetadata), ref _GbifRequestMetadata, value); }
        }



        private string _ZenodoResponsePublish;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoResponsePublish
        {
            get { return _ZenodoResponsePublish; }
            set { SetPropertyValue<string>(nameof(ZenodoResponsePublish), ref _ZenodoResponsePublish, value); }
        }





        private string _ZenodoRecord;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoId
        {
            get { return _ZenodoRecord; }
            set { SetPropertyValue<string>(nameof(ZenodoId), ref _ZenodoRecord, value); }
        }

        private string _GbifEndpointId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string GbifEndpointId
        {
            get { return _GbifEndpointId; }
            set { SetPropertyValue<string>(nameof(GbifEndpointId), ref _GbifEndpointId, value); }
        }

        private string _ZenodoDoi;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoDoi
        {
            get { return _ZenodoDoi; }
            set { SetPropertyValue<string>(nameof(ZenodoDoi), ref _ZenodoDoi, value); }
        }



        private string _ZenodoConceptDoi;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoConceptDoi
        {
            get { return _ZenodoConceptDoi; }
            set { SetPropertyValue<string>(nameof(ZenodoConceptDoi), ref _ZenodoConceptDoi, value); }
        }



        private string _ZenodoFileId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoFileId
        {
            get { return _ZenodoFileId; }
            set { SetPropertyValue<string>(nameof(ZenodoFileId), ref _ZenodoFileId, value); }
        }





        private string _ZenodoUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string ZenodoUrl
        {
            get { return _ZenodoUrl; }
            set { SetPropertyValue<string>(nameof(ZenodoUrl), ref _ZenodoUrl, value); }
        }


        private string _ZenodoRecId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoRecId
        {
            get { return _ZenodoRecId; }
            set { SetPropertyValue<string>(nameof(ZenodoRecId), ref _ZenodoRecId, value); }
        }


        private string _GbifCitation;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string GbifCitation
        {
            get { return _GbifCitation; }
            set { SetPropertyValue<string>(nameof(GbifCitation), ref _GbifCitation, value); }
        }

        private string _GbifCitationText;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string GbifCitationText
        {
            get { return _GbifCitationText; }
            set { SetPropertyValue<string>(nameof(GbifCitationText), ref _GbifCitationText, value); }
        }



        private string _GbifUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string GbifUrl
        {
            get { return _GbifUrl; }
            set { SetPropertyValue<string>(nameof(GbifUrl), ref _GbifUrl, value); }
        }


        private string _GbifAlternativeIdentifier;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifAlternativeIdentifier
        {
            get { return _GbifAlternativeIdentifier; }
            set { SetPropertyValue<string>(nameof(GbifAlternativeIdentifier), ref _GbifAlternativeIdentifier, value); }
        }

        private string _GbifPreferredIdentifier;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifPreferredIdentifier
        {
            get { return _GbifPreferredIdentifier; }
            set { SetPropertyValue<string>(nameof(GbifPreferredIdentifier), ref _GbifPreferredIdentifier, value); }
        }



        private string _PublishedDescription;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string PublishedDescription
        {
            get { return _PublishedDescription; }
            set { SetPropertyValue<string>(nameof(PublishedDescription), ref _PublishedDescription, value); }
        }


        private Image _Image;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        public Image Image
        {
            get { return _Image; }
            set { SetPropertyValue<Image>(nameof(Image), ref _Image, value); }
        }




        private string _Type;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Type
        {
            get { return _Type; }
            set { SetPropertyValue<string>(nameof(Type), ref _Type, value); }
        }


     
        public void UpdateUnitsCount(bool forceChangeEvents)
        {
            int? oldUnitsCount = fUnitsCount;
            //This line always evaluates the given expression on the server side so it doesn't take into account uncommitted objects.
            fUnitsCount = Convert.ToInt32(Session.Evaluate<Dataset>(CriteriaOperator.Parse("Units.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
            //This line always evaluates the given expression on the client side using the objects loaded from an internal XPO cache.
            //fUnitsCount = Convert.ToInt32(Evaluate(CriteriaOperator.Parse("Units.Count"))); ;
            if (forceChangeEvents)
                OnChanged("UnitsCount", oldUnitsCount, fUnitsCount);
        }
        [Persistent("UnitsCount")]
        private int? fUnitsCount;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [PersistentAlias("fUnitsCount")]
        public int? UnitsCount
        {
            get
            {
                //  if (!IsLoading && !IsSaving && fUnitsCount == null)
                //  UpdateUnitsCount(false);
                return fUnitsCount;
            }
        }


        private string _Code;
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }



        private string _CollectedBy;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string CollectedBy
        {
            get { return _CollectedBy; }
            set { SetPropertyValue<string>(nameof(CollectedBy), ref _CollectedBy, value); }
        }



        private string _Prefix;
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Prefix
        {
            get { return _Prefix; }
            set { SetPropertyValue<string>(nameof(Prefix), ref _Prefix, value); }
        }




        private bool _Autoincrement;
        [VisibleInListView(false), VisibleInDetailView(false)]
        public bool Autoincrement
        {
            get { return _Autoincrement; }
            set { SetPropertyValue<bool>(nameof(Autoincrement), ref _Autoincrement, value); }
        }





        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public Dataset(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }


        private string _Name;
        [DocumentationAttribute("Dataset name")]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        [Association("Dataset-Broods")]
        public XPCollection<Brood> Broods
        {
            get
            {
                return GetCollection<Brood>("Broods");
            }
        }
        [Association("DatasetUnits")]
        // [NonPersistent]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
                //          return new XPCollection<Unit>(Session, new BinaryOperator("Dataset.Oid", this.Oid));
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;

            }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }



        private Project _Project;
        [Association("Project-Datasets")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


   [Association("DatasetContributions"), Aggregated]
        public XPCollection<DatasetContribution> Contributions
        {
            get
            {
                return GetCollection<DatasetContribution>("Contributions");
            }
        }
        [Association("Datasets-DatasetUsage"), Aggregated]
        public XPCollection<DatasetUsage> UsedInProjects
        {
            get
            {
                return GetCollection<DatasetUsage>("UsedInProjects");
            }
        }


        private License _License;
        public License License
        {
            get { return _License; }
            set { SetPropertyValue<License>(nameof(License), ref _License, value); }
        }


      /*   [Association("Dataset-Attachments"), Aggregated]
        public XPCollection<DatasetAttachment> Attachments
        {
            get
            {
                return GetCollection<DatasetAttachment>("Attachments");
            }
        }*/
        [Association("Sequence-Dataset")]
        [VisibleInDetailView(false)]
        public XPCollection<Sequencing> Sequencing
        {
            get
            {
                return GetCollection<Sequencing>(nameof(Sequencing));
            }
        }
    }
}
