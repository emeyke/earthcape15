using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Logistics;
using System;
using System.Collections.Generic;

using System.ComponentModel;
using System.Drawing;
using TatukGIS.NDK;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class Project : BaseObject, ITreeNode, IName, ICode, IWKT, IMapsMarker
    {

        private int? _CoordAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? CoordAccuracy
        {
            get { return _CoordAccuracy; }
            set { SetPropertyValue<int?>(nameof(CoordAccuracy), ref _CoordAccuracy, value); }
        }

        [Association("ProjectTasks")]
        public XPCollection<PersonalTask> Tasks
        {
            get { return GetCollection<PersonalTask>(nameof(Tasks)); }
        }

        private GeometryType _GeometryType= GeometryType.Point;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public GeometryType GeometryType
        {
            get { return _GeometryType; }
            set { SetPropertyValue<GeometryType>(nameof(GeometryType), ref _GeometryType, value); }
        }

        private string _ZenodoCommunity;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ZenodoCommunity
        {
            get { return _ZenodoCommunity; }
            set { SetPropertyValue<string>(nameof(ZenodoCommunity), ref _ZenodoCommunity, value); }
        }



        private string _GRSciCollInstitution;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GRSciCollInstitution
        {
            get { return _GRSciCollInstitution; }
            set { SetPropertyValue<string>(nameof(GRSciCollInstitution), ref _GRSciCollInstitution, value); }
        }




        [DevExpress.Xpo.Aggregated]
        [Association("Project-Transfers")]

        public XPCollection<MaterialTransfer> Transfers
        {
            get { return GetCollection<MaterialTransfer>(nameof(Transfers)); }
        }


        private string _GbifUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string GbifUrl
        {
            get { return _GbifUrl; }
            set { SetPropertyValue<string>(nameof(GbifUrl), ref _GbifUrl, value); }
        }

        private ProjectCountry _ProjectCountry;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Association("CountriesProjects")]
        public ProjectCountry ProjectCountry
        {
            get { return _ProjectCountry; }
            set { SetPropertyValue(nameof(ProjectCountry), ref _ProjectCountry, (ProjectCountry)value); }
        }


        private string _PostalCode;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string PostalCode
        {
            get { return _PostalCode; }
            set { SetPropertyValue<string>(nameof(PostalCode), ref _PostalCode, value); }
        }


        private string _City;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string City
        {
            get { return _City; }
            set { SetPropertyValue<string>(nameof(City), ref _City, value); }
        }

        private string _GbifEndorsingNode;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifEndorsingNode
        {
            get { return _GbifEndorsingNode; }
            set { SetPropertyValue<string>(nameof(GbifEndorsingNode), ref _GbifEndorsingNode, value); }
        }







        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public IMapsMarker GeoMap
        {
            get { return this; }
        }

        public void UpdateUnitsCount(bool forceChangeEvents)
        {
        }

        private int? _UnitsCount;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public int? UnitsCount
        {
            get { return _UnitsCount; }
            set { SetPropertyValue<int?>(nameof(UnitsCount), ref _UnitsCount, value); }
        }




#if MediumTrust
		[Persistent("Color")]
		[Browsable(false)]
		public Int32 color;
#else
        [Persistent("Color")]
        private Int32 color;
#endif
         [NonPersistent, Browsable(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public object Id
        {
            get { return Oid; }
        }


        private string _Caption;
        public string Caption
        {
            get { return _Caption; }
            set { SetPropertyValue<string>(nameof(Caption), ref _Caption, value); }
        }


        [NonPersistent, Browsable(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Int32 OleColor
        {
            get { return ColorTranslator.ToOle(Color.FromArgb(color)); }
        }
    /*    [Association("Event-Resource")]
        [VisibleInDetailView(false)]
        public XPCollection<ProjectEvent> Events
        {
            get { return GetCollection<ProjectEvent>("Events"); }
        }*/
        [NonPersistent]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Color Color
        {
            get { return Color.FromArgb(color); }
            set { SetPropertyValue("Color", ref color, value.ToArgb()); }
        }


      

        [Association("Project-ProjectUsers"), Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<ProjectUser> Users
        {
            get
            {
                return GetCollection<ProjectUser>("Users");
            }
        }



        private string _ExternalUrl;
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("Project.ExternalUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string ExternalUrl
        {
            get { return _ExternalUrl; }
            set { SetPropertyValue<string>(nameof(ExternalUrl), ref _ExternalUrl, value); }
        }



        private string _Country;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public string Country
        {
            get { return _Country; }
            set { SetPropertyValue<string>(nameof(Country), ref _Country, value); }
        }


      
        [Association("Project-Taxa")]
        [VisibleInDetailView(false)]
        public XPCollection<TaxonomicName> Taxa
        {
            get { return GetCollection<TaxonomicName>("Taxa"); }
        }

        [Association("Projects-References")]
        [VisibleInDetailView(false)]
        public XPCollection<Reference> References
        {
            get { return GetCollection<Reference>("References"); }
        }

        /*    [Association("Project-CustomFields")]
            [Aggregated]
            [VisibleInDetailView(false)]
            public XPCollection<CustomField> CustomFields
            {
                get { return GetCollection<CustomField>("CustomFields"); }
            }*/


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            color = Color.White.ToArgb();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    ;
                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                    // ProjectUser pu = new ProjectUser(Session) { Project = this, User = u };
                    // pu.Type = ProjectUserType.Admin;
                    //  pu.Save();
                    //       _Lead = u.FirstName + " " + u.LastName;
                    //       _ContactEmail = u.Email;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (String.IsNullOrEmpty(GbifPublisherKey))
                foreach (ProjectContact item in Contacts)
                {
                    item.GbifIdentifier = "";
                }
            if (SecuritySystem.CurrentUser != null)
            {
                ;
                if (Session.IsNewObject(this))
                {
                    // Role admin = CreateRoles();
                    //  u.Roles.Add(admin);
                    /*  ProjectMember mem = new ProjectMember(Session);
                      mem.ProjectMemberType = ProjectMemberType.Owner;
                      mem.User= (Session.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUserName)) as String );                
                      mem.Project = this;
                      mem.Save();*/
                }
                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        public Role CreateRoles()
        {
            // Role admin = RoleHelper.CreateProjectAdminRole(Session, this);
            //  Role editor = RoleHelper.CreateProjectDataEditorRole(Session, this);
            // Role member = RoleHelper.CreateProjectMemberRole(Session, this);
            // / return admin;
            return null;
        }


        private string _DefaultFolder;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string DefaultFolder
        {
            get { return _DefaultFolder; }
            set { SetPropertyValue<string>(nameof(DefaultFolder), ref _DefaultFolder, value); }
        }




        private MapProject _Map;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public MapProject Map
        {
            get { return _Map; }
            set { SetPropertyValue<MapProject>(nameof(Map), ref _Map, value); }
        }


     
        public Project(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }

        private string _Name;
        [Size(SizeAttribute.Unlimited)]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        [ModelDefault("RowCount", "1")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        private string _Paricipants;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Paricipants
        {
            get { return _Paricipants; }
            set { SetPropertyValue<string>(nameof(Paricipants), ref _Paricipants, value); }
        }



        private string _Geography;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Geography
        {
            get { return _Geography; }
            set { SetPropertyValue<string>(nameof(Geography), ref _Geography, value); }
        }


        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Title { get { return Name; } }


        private ProjectCategory _Category;
        [Association("ProjectCategory1")]
        public ProjectCategory Category
        {
            get { return _Category; }
            set { SetPropertyValue<ProjectCategory>(nameof(Category), ref _Category, value); }
        }


        private double _Area;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Area
        {
            get { return _Area; }
            set { SetPropertyValue<double>(nameof(Area), ref _Area, value); }
        }


        private string _CoordSource;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CoordSource
        {
            get { return _CoordSource; }
            set { SetPropertyValue<string>(nameof(CoordSource), ref _CoordSource, value); }
        }


        private double _Length;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Length
        {
            get { return _Length; }
            set { SetPropertyValue<double>(nameof(Length), ref _Length, value); }
        }



        private string _WKT;
        [Size(SizeAttribute.Unlimited)]
        public string WKT
        {
            get { return _WKT; }
            set { SetPropertyValue<string>(nameof(WKT), ref _WKT, value); }
        }




        private Decimal? _Centroid_X;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_X
        {
            get { return _Centroid_X; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_X), ref _Centroid_X, value); }
        }



        private Decimal? _Centroid_Y;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_Y
        {
            get { return _Centroid_Y; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_Y), ref _Centroid_Y, value); }
        }



        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }

        private double _Latitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Latitude
        {
            get { return _Latitude; }
            set { SetPropertyValue<double>(nameof(Latitude), ref _Latitude, value); }
        }


        private double _Longitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Longitude
        {
            get { return _Longitude; }
            set { SetPropertyValue<double>(nameof(Longitude), ref _Longitude, value); }
        }




        private Project _Parent;
        [Association("PartyHierarchy")]
        [VisibleInListView(false), VisibleInDetailView(false)]
        [VisibleInLookupListView(true)]
        public Project Parent
        {
            get { return _Parent; }
            set { SetPropertyValue<Project>(nameof(Parent), ref _Parent, value); }
        }



        [Association("PartyHierarchy")]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public XPCollection<Project> Children
        {
            get { return GetCollection<Project>("Children"); }
        }

        #region ITreeNode
        IBindingList ITreeNode.Children
        {
            get
            {
                return Children;
            }
        }
        string ITreeNode.Name
        {
            get
            {
                return Name;
            }
        }
        ITreeNode ITreeNode.Parent
        {
            get
            {
                return Parent;
            }
        }
        #endregion



        private DateTime _StartDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { SetPropertyValue<DateTime>(nameof(StartDate), ref _StartDate, value); }
        }



        private DateTime _EndDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { SetPropertyValue<DateTime>(nameof(EndDate), ref _EndDate, value); }
        }



        private string _ContactEmail;
        [RuleRegularExpression(null, "ProjectContext", EarthCapeModule.EmailPattern)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ContactEmail
        {
            get { return _ContactEmail; }
            set { SetPropertyValue<string>(nameof(ContactEmail), ref _ContactEmail, value); }
        }




        private string _Details;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Details
        {
            get { return _Details; }
            set { SetPropertyValue<string>(nameof(Details), ref _Details, value); }
        }



        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }



        private string _Address;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Address
        {
            get { return _Address; }
            set { SetPropertyValue<string>(nameof(Address), ref _Address, value); }
        }



        private string _Lead;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Lead
        {
            get { return _Lead; }
            set { SetPropertyValue<string>(nameof(Lead), ref _Lead, value); }
        }



        private string _Status;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Status
        {
            get { return _Status; }
            set { SetPropertyValue<string>(nameof(Status), ref _Status, value); }
        }



        private bool _Public;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool Public
        {
            get { return _Public; }
            set { SetPropertyValue<bool>(nameof(Public), ref _Public, value); }
        }



        [Association("Project-Datasets"), Aggregated]
        public XPCollection<Dataset> Datasets
        {
            get
            {
                return GetCollection<Dataset>("Datasets");
            }
        }
        [Association("Projects-DatasetUsage"), Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<DatasetUsage> DatasetsUsed
        {
            get
            {
                return GetCollection<DatasetUsage>("DatasetsUsed");
            }
        }


        private string _Methods;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        public string Methods
        {
            get { return _Methods; }
            set { SetPropertyValue<string>(nameof(Methods), ref _Methods, value); }
        }



        private string _Resources;
        [Size(500)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        public string Resources
        {
            get { return _Resources; }
            set { SetPropertyValue<string>(nameof(Resources), ref _Resources, value); }
        }




        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private string _Code;
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }


        private int? _StartYear;
        public int? StartYear
        {
            get { return _StartYear; }
            set { SetPropertyValue<int?>(nameof(StartYear), ref _StartYear, value); }
        }



        private string _Type;
        public string Type
        {
            get { return _Type; }
            set { SetPropertyValue<string>(nameof(Type), ref _Type, value); }
        }




        private string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { SetPropertyValue<string>(nameof(Phone), ref _Phone, value); }
        }




        private Image _Image;
        [Size(SizeAttribute.Unlimited), ValueConverter(typeof(ImageValueConverter))]
        public Image Image
        {
            get { return _Image; }
            set { SetPropertyValue<Image>(nameof(Image), ref _Image, value); }
        }



      

        private string _GbifPublisherKey;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifPublisherKey
        {
            get { return _GbifPublisherKey; }
            set { SetPropertyValue<string>(nameof(GbifPublisherKey), ref _GbifPublisherKey, value); }
        }

        private string _GbifCreatedBy;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifCreatedBy
        {
            get { return _GbifCreatedBy; }
            set { SetPropertyValue<string>(nameof(GbifCreatedBy), ref _GbifCreatedBy, value); }
        }

        private string _LogoUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string LogoUrl
        {
            get { return _LogoUrl; }
            set { SetPropertyValue<string>(nameof(LogoUrl), ref _LogoUrl, value); }
        }


        private string _ImagePath;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ImagePath
        {
            get { return _ImagePath; }
            set { SetPropertyValue<string>(nameof(ImagePath), ref _ImagePath, value); }
        }



      

        [Association("ProjectContacts")]
        [Aggregated]
        public XPCollection<ProjectContact> Contacts
        {
            get
            {
                return GetCollection<ProjectContact>("Contacts");
            }
        }
        [Association("Project-Localities")]
        public XPCollection<Locality> Localities
        {
            get
            {
                return GetCollection<Locality>("Localities");
            }
        }
        [Association("Project-LocalityVisits")]
        [VisibleInDetailView(false)]
        public XPCollection<LocalityVisit> LocalityVisits
        {
            get
            {
                return GetCollection<LocalityVisit>("LocalityVisits");
            }
        }
        [VisibleInDetailView(false)]
        [Association("Project-Attachments"), Aggregated]
        public XPCollection<ProjectAttachment> Attachments
        {
            get
            {
                return GetCollection<ProjectAttachment>("Attachments");
            }
        }


        private PersonCore _PrimaryContact;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public PersonCore PrimaryContact
        {
            get { return _PrimaryContact; }
            set { SetPropertyValue<PersonCore>(nameof(PrimaryContact), ref _PrimaryContact, value); }
        }


        private string _Province;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Province
        {
            get { return _Province; }
            set { SetPropertyValue<string>(nameof(Province), ref _Province, value); }
        }






    }
}
