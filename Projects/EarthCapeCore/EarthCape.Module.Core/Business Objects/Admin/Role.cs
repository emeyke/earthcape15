#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.

using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using Xpand.ExpressApp.Security.Core;

namespace EarthCape.Module.Core
{
    
    [DefaultClassOptions]
    [ImageName("BO_Role"), System.ComponentModel.DisplayName("Role")]
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class Role : XpandRole
    {
        public Role(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
       
       /* private Project _Project;
        [Association("Project-Roles")]
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
        private RoleType _RoleType;
        public RoleType RoleType
        {
            get
            {
                return _RoleType;
            }
            set
            {
                SetPropertyValue("RoleType", ref _RoleType, value);
            }
        }*/
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
                if (SecuritySystem.CurrentUser != null)
                {
                    User u = (Session.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                             LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
                if (string.IsNullOrEmpty(Name))
               // if (_Project != null)
               //     if (_RoleType != null)
                //        Name = _Project.Name + " " + _RoleType.ToString();
                if (string.IsNullOrEmpty(Name))
                    Name = (new Guid()).ToString();
         }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    User u = (Session.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                  }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
                }
        }
        
        [Persistent]
        [NoForeignKey]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public User CreatedBy;

        [Persistent]
        [NoForeignKey]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public User LastModifiedByUser;

        [Persistent]
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn;

        [Persistent]
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn;
        /*[Persistent]
        public String RoleName
        {
            get
            {
                if (Project!=null)
                return Project.Name+" "+RoleType.ToString();
                return "";
            }
           
        }*/
    }

}
