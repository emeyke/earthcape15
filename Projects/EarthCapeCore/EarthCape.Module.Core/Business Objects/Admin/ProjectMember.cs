using System;
using System.Linq;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using Xpand.ExpressApp.Security.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.Attributes;

namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public class ProjectMember : BaseObject,IProjectObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public ProjectMember(Session session)
            : base(session)
        {
        }
        [Persistent]
        [NoForeignKey]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public User CreatedBy;

        [Persistent]
        [NoForeignKey]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public User LastModifiedByUser;

        [Persistent]
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn;

        [Persistent]
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn;
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
                 if (SecuritySystem.CurrentUser != null)
                {
                    User u = (Session.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
      }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    User u = (Session.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        private ProjectMemberType _ProjectMemberType;
        private User _User;
        private Project _Project;
    [ProvidedAssociation("ProjectMembers")]
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }

        public User User
        {
            get
            {
                return _User;
            }
            set
            {
                SetPropertyValue("User", ref _User, value);
            }
        }
        [Size(500)]
        public ProjectMemberType ProjectMemberType
        {
            get
            {
                return _ProjectMemberType;
            }
            set
            {
                SetPropertyValue("ProjectMemberType", ref _ProjectMemberType, value);
            }
        }
    }
}
