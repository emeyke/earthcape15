using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EarthCape.Module.Core
{
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class DataPackage : Project
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public DataPackage(Session session)
            : base(session)
        {
        }

        private string _FilterUnits;
        [Size(SizeAttribute.Unlimited)]
        public string FilterUnits
        {
            get { return _FilterUnits; }
            set { SetPropertyValue<string>(nameof(FilterUnits), ref _FilterUnits, value); }
        }

        private string _FilterTaxa;
        [Size(SizeAttribute.Unlimited)]
        public string FilterTaxa
        {
            get { return _FilterTaxa; }
            set { SetPropertyValue<string>(nameof(FilterTaxa), ref _FilterTaxa, value); }
        }


        private string _FilterLocalities;
        [Size(SizeAttribute.Unlimited)]
        public string FilterLocalities
        {
            get { return _FilterLocalities; }
            set { SetPropertyValue<string>(nameof(FilterLocalities), ref _FilterLocalities, value); }
        }

        private string _FilterPeople;
        [Size(SizeAttribute.Unlimited)]
        public string FilterPeople
        {
            get { return _FilterPeople; }
            set { SetPropertyValue<string>(nameof(FilterPeople), ref _FilterPeople, value); }
        }



        private string _FilterDatasets;
        [Size(SizeAttribute.Unlimited)]
        public string FilterDatasets
        {
            get { return _FilterDatasets; }
            set { SetPropertyValue<string>(nameof(FilterDatasets), ref _FilterDatasets, value); }
        }


        private string _FilterProjects;
        [Size(SizeAttribute.Unlimited)]
        public string FilterProjects
        {
            get { return _FilterProjects; }
            set { SetPropertyValue<string>(nameof(FilterProjects), ref _FilterProjects, value); }
        }





        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

      /*  [Association("UnitsDataPackages")]
        public XPCollection<Unit> Units
        {
            get { return GetCollection<Unit>(nameof(Units)); }
        }*/



    }
}
