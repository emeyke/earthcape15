#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.


using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Logistics;
using System;

using System.Drawing;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [MapInheritance(MapInheritanceType.ParentTable)]
    [ExcelImportKeyAttribute("Name")]
    public class PersonCore : Person, IName, IEmail//,IMapsMarker
    {

        private string _Id;
        public string Id
        {
            get { return _Id; }
            set { SetPropertyValue<string>(nameof(Id), ref _Id, value); }
        }




        private bool _Deceased;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public bool Deceased
        {
            get { return _Deceased; }
            set { SetPropertyValue<bool>(nameof(Deceased), ref _Deceased, value); }
        }


        public PersonCore(Session session)
             : base(session)
        {
        }
        // Fields...
        /*  [VisibleInListView(false)]
          [VisibleInDetailView(false)]
          [VisibleInLookupListView(false)]
          public IMapsMarker Map
          {
              get { return this; }
          }*/


        private string _Title;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Title
        {
            get { return _Title; }
            set { SetPropertyValue<string>(nameof(Title), ref _Title, value); }
        }


            [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public double Latitude
        {
            get
            {
                if (Projects.Count > 0)
                    if (Projects[0] != null)
                        if (Projects[0].Project != null)
                            return Projects[0].Project.Latitude;
                return 0;
            }

        }
        [VisibleInDetailView(false)]
        [Association("Person-Tasks")]
        public XPCollection<PersonalTask> Tasks
        {
            get
            {
                return GetCollection<PersonalTask>("Tasks");
            }
        }
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public double Longitude
        {
            get
            {
                if (Projects.Count > 0)
                    if (Projects[0] != null)
                        if (Projects[0].Project != null)
                            return Projects[0].Project.Longitude;
                return 0;
            }

        }


        private string _Name;
        [Size(255)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


   

        [Association("Person-Projects")]
        [Aggregated]
        public XPCollection<ProjectContact> Projects
        {
            get
            {
                return GetCollection<ProjectContact>("Projects");
            }
        }
        [Association("Person-Datasets")]
        [Aggregated]
        public XPCollection<DatasetContribution> Datasets
        {
            get
            {
                return GetCollection<DatasetContribution>("Datasets");
            }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }



        private string _Skype;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public string Skype
        {
            get { return _Skype; }
            set { SetPropertyValue<string>(nameof(Skype), ref _Skype, value); }
        }



        private PersonCore _Referral;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public PersonCore Referral
        {
            get { return _Referral; }
            set { SetPropertyValue<PersonCore>(nameof(Referral), ref _Referral, value); }
        }


        private string _Address;

        [Size(SizeAttribute.Unlimited)]
        public string Address
        {
            get { return _Address; }
            set { SetPropertyValue<string>(nameof(Address), ref _Address, value); }
        }


   
        private string _Phone;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Phone
        {
            get { return _Phone; }
            set { SetPropertyValue<string>(nameof(Phone), ref _Phone, value); }
        }



        private string _Phone1;

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Phone1
        {
            get { return _Phone1; }
            set { SetPropertyValue<string>(nameof(Phone1), ref _Phone1, value); }
        }


        private string _Country;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Country
        {
            get { return _Country; }
            set { SetPropertyValue<string>(nameof(Country), ref _Country, value); }
        }



        private string _Affiliation;
        [Size(1000)]
        public string Affiliation
        {
            get { return _Affiliation; }
            set { SetPropertyValue<string>(nameof(Affiliation), ref _Affiliation, value); }
        }





        private string _Fax;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public string Fax
        {
            get { return _Fax; }
            set { SetPropertyValue<string>(nameof(Fax), ref _Fax, value); }
        }



        private string _Department;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Size(1000)]
        public string Department
        {
            get { return _Department; }
            set { SetPropertyValue<string>(nameof(Department), ref _Department, value); }
        }


        private string _City;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public string City
        {
            get { return _City; }
            set { SetPropertyValue<string>(nameof(City), ref _City, value); }
        }


        private string _Province;
        public string Province
        {
            get { return _Province; }
            set { SetPropertyValue<string>(nameof(Province), ref _Province, value); }
        }




        private string _ZipCode;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public string ZipCode
        {
            get { return _ZipCode; }
            set { SetPropertyValue<string>(nameof(ZipCode), ref _ZipCode, value); }
        }



        private string _Affiliation1;
        [Size(1000)]
        public string Affiliation1
        {
            get { return _Affiliation1; }
            set { SetPropertyValue<string>(nameof(Affiliation1), ref _Affiliation1, value); }
        }



      

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (Session.IsNewObject(this))
            {
                Name = string.Format("{0} {1}", FirstName, LastName);
            }
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }


        private string _Website;
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("PersonCore.Website.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string Website
        {
            get { return _Website; }
            set { SetPropertyValue<string>(nameof(Website), ref _Website, value); }
        }


      
        [Association("PersonTransfers")]
        public XPCollection<MaterialTransfer> Transfers
        {
            get { return GetCollection<MaterialTransfer>(nameof(Transfers)); }
        }

    }
}
