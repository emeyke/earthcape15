using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace EarthCape.Module.Core
{
    [DefaultProperty("Subject")]
    public class PersonalTask : BaseObject, ITask, IProjectObject, IName
    {

        private string _Name;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private string _Comment;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }



        private PersonalTask _Parent;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public PersonalTask Parent
        {
            get { return _Parent; }
            set { SetPropertyValue<PersonalTask>(nameof(Parent), ref _Parent, value); }
        }


        private Project _Project;
        [Association("ProjectTasks")]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }

        private TaskImpl task = new TaskImpl();
        private PersonCore assignedTo;
#if MediumTrust
		[Persistent("DateCompleted"), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public DateTime dateCompleted {
			get { return task.DateCompleted; }
			set {
				DateTime oldValue = task.DateCompleted;
				task.DateCompleted = value;
				OnChanged("dateCompleted", oldValue, task.DateCompleted);
			}
		}
#else
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [Persistent("DateCompleted")]
        private DateTime dateCompleted
        {
            get { return task.DateCompleted; }
            set
            {
                DateTime oldValue = task.DateCompleted;
                task.DateCompleted = value;
                OnChanged(nameof(dateCompleted), oldValue, task.DateCompleted);
            }
        }
#endif
        public PersonalTask(Session session) : base(session) { }
        protected override void OnLoading()
        {
            task.IsLoading = true;
            base.OnLoading();
        }
        protected override void OnLoaded()
        {
            base.OnLoaded();
            task.DateCompleted = dateCompleted;
            task.IsLoading = false;
        }
        [Action(ImageName = "State_Task_Completed")]
        public void MarkCompleted()
        {
            TaskStatus oldStatus = task.Status;
            task.MarkCompleted();
            OnChanged(nameof(Status), oldStatus, task.Status);
            OnChanged(nameof(PercentCompleted));
        }
        public string Subject
        {
            get { return task.Subject; }
            set
            {
                string oldValue = task.Subject;
                task.Subject = value;
                OnChanged(nameof(Subject), oldValue, task.Subject);
            }
        }
        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        public string Description
        {
            get { return task.Description; }
            set
            {
                string oldValue = task.Description;
                task.Description = value;
                OnChanged(nameof(Description), oldValue, task.Description);
            }
        }
        public DateTime DueDate
        {
            get { return task.DueDate; }
            set
            {
                DateTime oldValue = task.DueDate;
                task.DueDate = value;
                OnChanged(nameof(DueDate), oldValue, task.DueDate);
            }
        }
        public DateTime StartDate
        {
            get { return task.StartDate; }
            set
            {
                DateTime oldValue = task.StartDate;
                task.StartDate = value;
                OnChanged(nameof(StartDate), oldValue, task.StartDate);
            }
        }
        [Association("Person-Tasks")]
        public PersonCore AssignedTo
        {
            get { return assignedTo; }
            set { SetPropertyValue(nameof(AssignedTo), ref assignedTo, value); }
        }
        public TaskStatus Status
        {
            get { return task.Status; }
            set
            {
                TaskStatus oldValue = task.Status;
                task.Status = value;
                OnChanged(nameof(Status), oldValue, task.Status);
            }
        }
        public Int32 PercentCompleted
        {
            get { return task.PercentCompleted; }
            set
            {
                Int32 oldValue = task.PercentCompleted;
                task.PercentCompleted = value;
                OnChanged(nameof(PercentCompleted), oldValue, task.PercentCompleted);
            }
        }
        public DateTime DateCompleted
        {
            get { return dateCompleted; }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public override void AfterConstruction()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (XPObjectSpace.FindObjectSpaceByObject(this) != null)
                {
                    if (DevExpress.ExpressApp.SecuritySystem.CurrentUser != null)
                    {

                        CreatedByUser = SecuritySystem.CurrentUserName; ;
                        LastModifiedByUser = SecuritySystem.CurrentUserName;
                    }
                    CreatedOn = DateTime.Now;
                    LastModifiedOn = DateTime.Now;
                }
            }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            if (IsLoading) return;
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
            }
        }

    }
}
