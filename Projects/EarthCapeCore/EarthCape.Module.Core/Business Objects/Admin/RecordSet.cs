
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    //  [DefaultClassOptions]
    //  [DefaultProperty("Text")]
    [ExcelImportKeyAttribute("Name")]
    public class RecordSet : BaseObject, IUnits, IName
    {
        [Association("RecordSets-Units")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }

        private string _INatProject;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string INatProject
        {
            get { return _INatProject; }
            set { SetPropertyValue<string>(nameof(INatProject), ref _INatProject, value); }
        }



        private string _Name;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        private string _Comment;

        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


       
        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }


        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public RecordSet(Session session) : base(session) { }



    }

}
