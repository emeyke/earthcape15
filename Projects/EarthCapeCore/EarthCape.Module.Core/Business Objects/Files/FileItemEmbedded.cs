using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.Persistent.Base;
//using PivotCollectionTools.BasicCollection;
using System.ComponentModel;
using System.Collections.Generic;

using System.IO;

namespace EarthCape.Module.Core
{
   // [DefaultClassOptions]
    [FileAttachmentAttribute("File")]
   // [MapInheritance(MapInheritanceType.ParentTable)]
    public class FileItemEmbedded : FileItem
    {
        public FileItemEmbedded(Session session) : base(session) { }
        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never), NoForeignKey, ImmediatePostData]
        public FileDataEmbedded File
        {
            get { return GetPropertyValue<FileDataEmbedded>("File"); }
            set { SetPropertyValue<FileDataEmbedded>("File", value); }
        }
     
        protected override Image GetImage()
        {
            return null;
        }

        protected override FileData GetFileData()
        {
            return File;
        }
    }
}
