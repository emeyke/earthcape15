using System;
using System.Drawing;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.Validation;


namespace EarthCape.Module.Core
{
   // [DefaultClassOptions]
    [FileAttachmentAttribute("File")]
    //[MapInheritance(MapInheritanceType.ParentTable)]
    public class FileItemLinked : FileItem
    {
        public FileItemLinked(Session session) : base(session) { }

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never), NoForeignKey, ImmediatePostData]
        public FileDataLinked File
        {
            get { return GetPropertyValue<FileDataLinked>("File"); }
            set { SetPropertyValue<FileDataLinked>("File", value); }
        }
        
       /* // Fields...
        private FileDataLinked _File;
        [RuleRequiredField("FileAttachmentBaseRule", "Save", "File should be assigned")]
        [ExpandObjectMembers(ExpandObjectMembers.Never)]
        public FileDataLinked File
        {
            get
            {
                return _File;
            }
            set
            {
                SetPropertyValue("File", ref _File, value);
            }
        }*/

        protected override Image GetImage()
        {
            if (File == null) return null;
            if (!System.IO.File.Exists(File.FullName)) return null;
            if (!IsImage(File.FullName)) return null;
            return Image.FromFile(File.FullName);
        }

        protected override FileData GetFileData()
        {
            return File;
        }

        private bool IsImage(string p)
        {
            if (p.Contains(".jpg")) return true;
            if (p.Contains(".jpeg")) return true;
            if (p.Contains(".bmp")) return true;
            if (p.Contains(".png")) return true;
            return false;
        }

    }
}
