﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security.Strategy;

namespace EarthCape.Module.Core
{
  //  [DefaultClassOptions]
   [DefaultProperty("FileName")]
    [DeferredDeletion(false), OptimisticLocking(false)]
    public abstract class FileData : BaseObject, IFileData, IEmptyCheckable
    {
        protected FileData(Session session)
            : base(session) { }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser CreatedBy;
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser LastModifiedBy;

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;


            }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
                   if (SecuritySystem.CurrentUser != null)
                {
                    
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
        }
      

        public abstract void LoadFromStream(string fileName, Stream stream);

        public abstract void SaveToStream(Stream stream);

        public abstract void Clear();

        [Size(SizeAttribute.Unlimited), Custom("AllowEdit", "False")]
        public string FileName
        {
            get { return GetPropertyValue<string>("FileName"); }
            set { SetPropertyValue("FileName", value); }
        }
        
        [Persistent]
        public int Size
        {
            get { return GetPropertyValue<int>("Size"); }
            protected set { SetPropertyValue<int>("Size", value); }
        }

        public abstract bool IsEmpty { get; }

        public override string ToString()
        {
            return FileName;
        }
    }
}
