﻿// Developer Express Code Central Example:
// How to store files in the file system, instead of the database.
// 
// See Also:
// File Attachments Module Overview
// (ms-help://Xpand.XAF/CustomDocument2781.htm)
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E965

using System;
using System.IO;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;


namespace EarthCape.Module.Core {
    [Persistent]
    public class FileDataStored : FileData
    {
        private Stream tempSourceStream;
        private string tempFileName = string.Empty;
        private static object syncRoot = new object();
        //public string StorePath = String.Format("{0}\\EarthCape\\FileStore\\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        public FileDataStored(Session session) : base(session) { }

        public string RealFileName
        {
            get
            {
                if (!string.IsNullOrEmpty(FileName) && Oid != Guid.Empty)
                    return Path.Combine(EarthCapeModule.FileSystemStoreLocation, string.Format("{0}{1}", Oid, Path.GetExtension(FileName)));
                return null;
            }
        }
        public Stream GetContentStream()
        {
            if (IsEmpty)
                return null;

            var stream = new MemoryStream(Size);
            (this as IFileData).SaveToStream(stream);
            return stream;
        }

        protected virtual void SaveFileToStore()
        {
            if (!string.IsNullOrEmpty(RealFileName))
            {
                try
                {
                    using (Stream destination = File.OpenWrite(RealFileName))
                    {
                        EarthCapeModule.CopyStream(TempSourceStream, destination);
                        Size = (int)destination.Length;
                    }
                }
                catch (DirectoryNotFoundException exc)
                {
                    throw new UserFriendlyException(exc);
                }
            }
        }

        private void RemoveOldFileFromStore()
        {
            //Dennis: We need to remove the old file from the store when saving the current object.
            if (!string.IsNullOrEmpty(tempFileName) && tempFileName != RealFileName)
            {//B222892
                try
                {
                    File.Delete(tempFileName);
                    tempFileName = string.Empty;
                }
                catch (DirectoryNotFoundException exc)
                {
                    throw new UserFriendlyException(exc);
                }
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            Guard.ArgumentNotNullOrEmpty(EarthCapeModule.FileSystemStoreLocation, "FileSystemStoreLocation");
            lock (syncRoot)
            {
                if (!Directory.Exists(EarthCapeModule.FileSystemStoreLocation))
                    Directory.CreateDirectory(EarthCapeModule.FileSystemStoreLocation);
            }
            SaveFileToStore();
            RemoveOldFileFromStore();
        }

        protected override void OnDeleting()
        {
            //Dennis: We need to remove the old file from the store.
            Clear();
            base.OnDeleting();
        }

        protected override void Invalidate(bool disposing)
        {
            if (disposing && TempSourceStream != null)
            {
                TempSourceStream.Close();
                TempSourceStream = null;
            }
            base.Invalidate(disposing);
        }

        public override void Clear()
        {
            //Dennis: When clearing the file name property we need to save the name of the old file to remove it from the store in the future. You can also setup a separate service for that.
            if (string.IsNullOrEmpty(tempFileName))
                tempFileName = RealFileName;
            FileName = string.Empty;
            Size = 0;
        }

        [Browsable(false)]
        public Stream TempSourceStream
        {
            get { return tempSourceStream; }
            set
            {
                tempSourceStream = value;
                //Dennis: For Windows Forms applications.
                if (tempSourceStream is FileStream)
                {
                    try
                    {
                        tempSourceStream = File.OpenRead(((FileStream)tempSourceStream).Name);
                    }
                    catch (FileNotFoundException exc)
                    {
                        throw new UserFriendlyException(exc);
                    }
                }
            }
        }
        //Dennis: Fires when uploading a file.
        public override void LoadFromStream(string fileName, Stream source)
        {
            FileName = fileName;
            TempSourceStream = source;
            //Dennis: When assigning a new file we need to save the name of the old file to remove it from the store in the future.
            if (string.IsNullOrEmpty(tempFileName))
                tempFileName = RealFileName;
        }
        //Dennis: Fires when saving or opening a file.
        public override void SaveToStream(Stream destination)
        {
            try
            {
                if (!string.IsNullOrEmpty(RealFileName))
                {
                    if (destination == null)
                        EarthCapeModule.OpenFileWithDefaultProgram(RealFileName);
                    else
                        EarthCapeModule.CopyFileToStream(RealFileName, destination);
                }
                else if (TempSourceStream != null)
                    EarthCapeModule.CopyStream(TempSourceStream, destination);
            }
            catch (DirectoryNotFoundException exc)
            {
                throw new UserFriendlyException(exc);
            }
            catch (FileNotFoundException exc)
            {
                throw new UserFriendlyException(exc);
            }
        }
        public override bool IsEmpty
        {
            get { return FileDataHelper.IsFileDataEmpty(this) || !File.Exists(RealFileName); }
        }
    }
    
    /// <summary>
    /// This class enables you to store uploaded files in a centralized file system location instead of the database. You can configure the file system store location via the static FileStore.FolderPath property.
    /// </summary>
  /*  [DefaultProperty("FileName")]
    public class FileDataStored : BaseObject, IFileData, IEmptyCheckable {
        private Stream tempStream;
        private string tempFileName = string.Empty;
        public string StorePath = String.Format("{0}\\EarthCape\\FileStore\\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        public FileDataStored(Session session) : base(session) {
            if (Group != null)
                     if (!String.IsNullOrEmpty(Group.DefaultFolder))
                         StorePath = Group.DefaultFolder;
        }
        public string RealFileName {
            get {
                              return FileName != string.Empty ? Path.Combine(StorePath, string.Format("{0}-{1}", Oid, FileName)) : string.Empty;
             }
        }
        
        protected override void OnSaving() {
            base.OnSaving();
            Guard.ArgumentNotNullOrEmpty(StorePath, "FileStoreLocation");
            if (!Directory.Exists(StorePath))
                Directory.CreateDirectory(StorePath);
            if (tempStream != null && !string.IsNullOrEmpty(RealFileName)) {
                Stream source = null;
                //Dennis: For Windows Forms applications.
               if (tempStream is FileStream) {
                    try {
                        source = File.OpenRead(((FileStream)tempStream).Name);
                    } catch (FileNotFoundException exc) {
                        throw new UserFriendlyException(exc);
                    }
                }
                else {
                    //Dennis: For Web Forms applications.
                    source = tempStream;
                }
                try {
                    using (Stream destination = File.OpenWrite(RealFileName)) {
                        GeneralHelper.CopyStream(source, destination);
                        Size = (int)destination.Length;
                    }
                } catch (DirectoryNotFoundException exc) {
                    throw new UserFriendlyException(exc);
                } finally {
                    source.Close();
                    source = null;
                }
            }
            //Dennis: We need to remove the old file from the store when saving the current object.
            if (!string.IsNullOrEmpty(tempFileName)) {
                try {
                    File.Delete(tempFileName);
                    tempFileName = string.Empty;
                } catch (DirectoryNotFoundException exc) {
                    throw new UserFriendlyException(exc);
                }
            }
        }
        protected override void OnDeleting() {
            //Dennis: We need to remove the old file from the store.
            Clear();
            base.OnDeleting();
        }
        protected override void Invalidate(bool disposing) {
            if (disposing && tempStream != null) {
                tempStream.Close();
                tempStream = null;
            }
            base.Invalidate(disposing);
        }
        #region IFileData Members
        public void Clear() {
            //Dennis: When clearing the file name property we need to save the name of the old file to remove it from the store in the future.
            if (string.IsNullOrEmpty(tempFileName))
                tempFileName = RealFileName;
            FileName = string.Empty;
            Size = 0;
        }
        [Size(260)]
        public string FileName {
            get { return GetPropertyValue<string>("FileName"); }
            set { SetPropertyValue("FileName", value); }
        }
        //Dennis: Fires when uploading a file.
        void IFileData.LoadFromStream(string fileName, Stream source) {
            tempStream = source;
            //Dennis: When assigning a new file we need to save the name of the old file to remove it from the store in the future.
            if (string.IsNullOrEmpty(tempFileName))
                tempFileName = RealFileName;
            FileName = fileName;
        }
        //Dennis: Fires when saving or opening a file.
        void IFileData.SaveToStream(Stream destination) {
            try {
                GeneralHelper.CopyFileToStream(RealFileName, destination);
            } catch (DirectoryNotFoundException exc) {
                throw new UserFriendlyException(exc);
            } catch (FileNotFoundException exc) {
                throw new UserFriendlyException(exc);
            }
        }
        [Persistent]
        public int Size {
            get { return GetPropertyValue<int>("Size"); }
            private set { SetPropertyValue<int>("Size", value); }
        }
        #endregion
        #region IEmptyCheckable Members
        public bool IsEmpty {
            get { return FileDataHelper.IsFileDataEmpty(this) || !File.Exists(RealFileName); }
        }
        #endregion
    }*/
}