using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.Persistent.Base;
//using PivotCollectionTools.BasicCollection;
using System.ComponentModel;
using System.Collections.Generic;

using System.IO;

namespace EarthCape.Module.Core
{

    //[DefaultClassOptions]
    [FileAttachmentAttribute("File")]
   // [MapInheritance(MapInheritanceType.ParentTable)]
    public class FileItemStored : FileItem
    {
        public FileItemStored(Session session) : base(session) { }

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never), NoForeignKey, ImmediatePostData]
        public FileDataStored File
        {
            get { return GetPropertyValue<FileDataStored>("File"); }
            set { SetPropertyValue<FileDataStored>("File", value); }
        }

        protected override Image GetImage()
        {
            if (File == null)
                return null;

            var stream = File.GetContentStream();
            if (stream == null)
                return null;
            if (
                (File.FileName.ToLower().Contains(".jpeg"))
               || (File.FileName.ToLower().Contains(".jpg"))
               || (File.FileName.ToLower().Contains(".png"))
               || (File.FileName.ToLower().Contains(".tiff"))
                )
                return Image.FromStream(stream);
            return null;
        }

        protected override FileData GetFileData()
        {
            return File;
        }
    }
}
