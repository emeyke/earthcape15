using System;
using System.IO;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
namespace EarthCape.Module.Core
{
    [Persistent]
    public class FileDataEmbedded : FileData
    {
        public FileDataEmbedded(Session session) : base(session) { }
        public override void LoadFromStream(string fileName, Stream stream)
        {
            Guard.ArgumentNotNull(stream, "stream");
            Guard.ArgumentNotNullOrEmpty(fileName, "fileName");
            FileName = fileName;
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            Content = bytes;
        }
        public override void SaveToStream(Stream stream)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new InvalidOperationException();
            }
            try
            {
                stream.Write(Content, 0, Size);
                stream.Flush();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                
            }
        }

        public override void Clear()
        {
            Content = null;
            FileName = String.Empty;
        }

        [DevExpress.Xpo.Persistent, Delayed,
        ValueConverter(typeof(CompressionConverter)),
        MemberDesignTimeVisibility(false)]
        [VisibleInListView(false),VisibleInDetailView(false), EditorBrowsable(EditorBrowsableState.Never)]
        public byte[] Content
        {
            get { return GetDelayedPropertyValue<byte[]>("Content"); }
            set
            {
                //int oldSize = Size;
                if (value != null)
                {
                    Size = value.Length;
                }
                else
                {
                    Size = 0;
                }
                SetDelayedPropertyValue<byte[]>("Content", value);
                //OnChanged("Size", oldSize, size);
            }
        }
        public override bool IsEmpty
        {
            get { return string.IsNullOrEmpty(FileName); }
        }
    }
}
