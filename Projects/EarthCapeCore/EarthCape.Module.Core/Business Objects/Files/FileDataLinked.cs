﻿// Developer Express Code Central Example:
// How to store files in the file system, instead of the database.
// 
// See Also:
// File Attachments Module Overview
// (ms-help://Xpand.XAF/CustomDocument2781.htm)
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E965

using System;
using System.IO;
using DevExpress.Xpo;
using DevExpress.ExpressApp;

namespace EarthCape.Module.Core
{
    /// <summary>
    /// This class enables you to add soft links to real files instead of saving their contents to the database. It is intended for use in Windows Forms applications only.
    /// </summary>

    [Persistent]
    public class FileDataLinked : FileData
    {
        public FileDataLinked(Session session) : base(session) { }

        [Size(1000)]
        public string FullName
        {
            get
            {
                string name = String.Format("{0}\\{1}", Folder, FileName);
                //if (Project != null) if (!String.IsNullOrEmpty(Project.DefaultFolder)) name = String.Format("{0}\\{1}", Project.DefaultFolder, name);
                return name;
            }
           
        } 
 
        private string _Folder;
        [Size(700)]
        public string Folder
        {
            get { return _Folder; } 
            set { SetPropertyValue("Folder", ref _Folder, value); }
        }

        public override void Clear() {
            Size = 0;
            FileName = string.Empty;
        }

        public override void LoadFromStream(string fileName, Stream source) {
            Size = (int)source.Length;
            FileName = fileName;
            //FullName = ((FileStream)source).Name;
            Folder = Path.GetDirectoryName(((FileStream)source).Name);
        }

        public override void SaveToStream(Stream destination) {
            try {
                EarthCapeModule.CopyFileToStream(FullName, destination);
            } catch (Exception exc) {
                throw new UserFriendlyException(exc);
            }
        }

        public override bool IsEmpty {
            get { return !File.Exists(FullName); }
        }
    }
}