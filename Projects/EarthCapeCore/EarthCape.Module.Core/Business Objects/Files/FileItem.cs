using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Validation;
using System.Drawing;
using DevExpress.Xpo.Metadata;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security.Strategy;

namespace EarthCape.Module.Core
{
   // [DefaultClassOptions]
    public abstract class FileItem : BaseObject, IName, IImageWindow,  ICode, IProjectObject
    {

        [Association("Project-FileItems")]
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
        private Project _Project;
        private string _Code;
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue("Code", ref _Code, value); }
        }

        private string _CatalogNumber;
        public string CatalogNumber
        {
            get { return _CatalogNumber; }
            set { SetPropertyValue("CatalogNumber", ref _CatalogNumber, value); }
        }

        // IPictureItemitem
        public string ID
        {
            get { return Oid.ToString(); }
        }
        [DevExpress.Xpo.ValueConverterAttribute(typeof(ImageValueConverter))]
        [ImageEditor(ImageSizeMode = ImageSizeMode.Zoom,
            DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 100,
            ListViewImageEditorCustomHeight = 50)]
        public Image Image
        {
            get { return GetImage(); }
        }

        public String ImagePath
        {
            get { return ""; }
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser CreatedBy;
        [Persistent]
        [NoForeignKey]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SecuritySystemUser LastModifiedBy;

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
               if (SecuritySystem.CurrentUser != null)
                {
                    
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
           //    Project = GeneralHelper.GetCurrentProject(Session);
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;


            }
        }
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            var filedata = GetFileData();
          
            if (String.IsNullOrEmpty(Name) && filedata != null)
            {
                Name = filedata.FileName;
            }
            
            base.OnSaving();
            if (IsLoading) return;
                    if (SecuritySystem.CurrentUser != null)
                    {
                        
                        LastModifiedByUser = SecuritySystem.CurrentUserName;
                    }
                    LastModifiedOn = DateTime.Now;
     }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            var fileData = GetFileData();
            if (fileData != null)
            {
                fileData.Changed -= FileDataOnChanged;
                fileData.Changed += FileDataOnChanged;
            }
        }

        private void FileDataOnChanged(object sender, ObjectChangeEventArgs objectChangeEventArgs)
        {
            if (objectChangeEventArgs.PropertyName == "FileName")
                Name = GetFileData().FileName;
        }

        private string _Comment;

        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue("Comment", ref _Comment, value); }
        }

      
    
        public FileItem(Session session) : base(session) { }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        private string _SourceUrl;

        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("FileItem.SourceUrl.RuleRegularExpression", DefaultContexts.Save,
            @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})"
            )]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string SourceUrl
        {
            get { return _SourceUrl; }
            set { SetPropertyValue("SourceUrl", ref _SourceUrl, value); }
        }

        private string _SourceId;

        public string SourceId
        {
            get { return _SourceId; }
            set { SetPropertyValue("SourceId", ref _SourceId, value); }
        }
     
        protected abstract Image GetImage();

        protected abstract FileData GetFileData();

        [Association("File-UnitFile"),Aggregated]
        public XPCollection<UnitFile> Units
        {
            get
            {
                return GetCollection<UnitFile>("Units");
            }
        }
        [Association("File-LocalityFile"),Aggregated]
        public XPCollection<LocalityFile> Localities
        {
            get
            {
                return GetCollection<LocalityFile>("Localities");
            }
        }


    }
}
