
using System;
using DevExpress.Xpo;
namespace EarthCape.Module.Core
{
    public class IDCounter : XPCustomObject
    {
        private Project _Project;
        [Key]
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }

        private int _Count;
        public int Count
        {
            get
            {
                return _Count;
            }
            set
            {
                SetPropertyValue("Count", ref _Count, value);
            }
        } 
    }  
}
