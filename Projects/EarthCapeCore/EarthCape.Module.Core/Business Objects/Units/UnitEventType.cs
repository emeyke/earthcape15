using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [ExcelImportKeyAttribute("Name")]
    public class UnitEventType : BaseObject, IName
    {

        public UnitEventType(Session session) : base(session) { }
        // Fields...
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

     
        private string _Name;
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

      
    }



}
