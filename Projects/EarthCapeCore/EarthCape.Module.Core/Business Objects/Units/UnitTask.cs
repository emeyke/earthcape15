using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Linq;
using System.Text;

namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    [MapInheritance(MapInheritanceType.ParentTable)]
    [DefaultClassOptions]
    public class UnitTask : PersonalTask
    {
        // Herptiles-kanavki, stakanchiki Barbera, Melk. Mlek
        public UnitTask(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        private Unit _Unit;
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }
        



    }
}
