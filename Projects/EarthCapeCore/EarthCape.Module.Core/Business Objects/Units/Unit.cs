using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Lab;
using EarthCape.Module.Logistics;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Text.RegularExpressions;
using TatukGIS.NDK;
using Xpand.ExpressApp.ExcelImporter.Services;
using Xpand.Persistent.Base.General;

namespace EarthCape.Module.Core
{
   
    [VisibleInReports(true)]
    [DefaultClassOptions]
    [DefaultProperty("UnitID")]
    [DocumentationAttribute("Specimen, Observation or other objects that typically have Geographic, Taxonomic and Date information")]
    [ExcelImportKeyAttribute("UnitID")]
    public class Unit : BaseObject,ILocalityObject,ITaxonomicNameObject, IWKT, IName, IDatasetObject, IRecordSetsObject, IVectorMapsMarker, IMapsMarker, IAttachments, ISupportSequenceObject
    {


        private string _ZenodoConceptDoi;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public string ZenodoConceptDoi
        {
            get { return _ZenodoConceptDoi; }
            set { SetPropertyValue<string>(nameof(ZenodoConceptDoi), ref _ZenodoConceptDoi, value); }
        }



        private int? _ZenodoFileId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public int? ZenodoFileId
        {
            get { return _ZenodoFileId; }
            set { SetPropertyValue<int?>(nameof(ZenodoFileId), ref _ZenodoFileId, value); }
        }





        private string _ZenodoUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string ZenodoUrl
        {
            get { return _ZenodoUrl; }
            set { SetPropertyValue<string>(nameof(ZenodoUrl), ref _ZenodoUrl, value); }
        }


        private int? _ZenodoRecId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public int? ZenodoRecId
        {
            get { return _ZenodoRecId; }
            set { SetPropertyValue<int?>(nameof(ZenodoRecId), ref _ZenodoRecId, value); }
        }
        private int? _ZenodoId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(255)]
        public int? ZenodoId
        {
            get { return _ZenodoId; }
            set { SetPropertyValue<int?>(nameof(ZenodoId), ref _ZenodoId, value); }
        }

        private string _DOI;
        [Size(255)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string DOI
        {
            get { return _DOI; }
            set { SetPropertyValue<string>(nameof(DOI), ref _DOI, value); }
        }



        /*   private string _MapMarker;
           [Size(SizeAttribute.Unlimited)]
           [ModelDefault("RowCount", "1")]
           public string MapMarker
           {
               get
               {
                   if (Dataset != null) return Dataset.MapMarker;
                   return _MapMarker;
               }
               set { SetPropertyValue<string>(nameof(MapMarker), ref _MapMarker, value); }
           }*/
        [NonPersistent]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string MapLetter
        {
            get
            {
                if (!string.IsNullOrEmpty(UnitType))
                    return String.Format("https://chart.googleapis.com/chart?chst=d_map_pin_letter_withshadow&chld={0}|{1}|002EB8", UnitType[0],GeneralHelper.LetterToColor(UnitType[0].ToString()));
                return "";
            }
          }


        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Caption
        {
            get { return _UnitID; }
            set { SetPropertyValue<string>(nameof(Caption), ref _UnitID, value); }
        }

#if MediumTrust
		[Persistent("Color")]
		[Browsable(false)]
		public Int32 color;
#else
        [Persistent("Color")]
#pragma warning disable 649
        private Int32 color;
#pragma warning restore 649
#endif
        [NonPersistent, Browsable(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Int32 OleColor
        {
            get { return ColorTranslator.ToOle(Color.FromArgb(color)); }
        }

        [NonPersistent, Browsable(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public object Id
        {
            get { return Oid; }
        }
        /*    [Association("UnitsDataPackages")]
            [VisibleInDetailView(false)]
            public XPCollection<DataPackage> DataPackages
            {
                get { return GetCollection<DataPackage>(nameof(DataPackages)); }
            }*/

        [Association("InfoRequestUnits")]
        [VisibleInDetailView(false)]
        public XPCollection<InformationRequest> InformationRequests
        {
            get { return GetCollection<InformationRequest>(nameof(InformationRequests)); }
        }


     /*   [Association("UnitStocktaking"),Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<StocktakeUnit> Stocktakes
        {
            get { return GetCollection<StocktakeUnit>(nameof(Stocktakes)); }
        }*/

        private int _Count;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public int Count
        {
            get { return _Count; }
            set { SetPropertyValue<int>(nameof(Count), ref _Count, value); }
        }

        private string _CountUnit;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string CountUnit
        {
            get { return _CountUnit; }
            set { SetPropertyValue<string>(nameof(CountUnit), ref _CountUnit, value); }
        }

      /*  private StocktakeCondition _Condition;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public StocktakeCondition Condition
        {
            get { return _Condition; }
            set { SetPropertyValue<StocktakeCondition>(nameof(Condition), ref _Condition, value); }
        }*/

        private GeometryType _GeometryType = GeometryType.Point;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public GeometryType GeometryType
        {
            get { return _GeometryType; }
            set { SetPropertyValue<GeometryType>(nameof(GeometryType), ref _GeometryType, value); }
        }


        private string _ConsensusSequence;
        [ModelDefault("RowCount", "1")]
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string ConsensusSequence
        {
            get { return _ConsensusSequence; }
            set { SetPropertyValue<string>(nameof(ConsensusSequence), ref _ConsensusSequence, value); }
        }




        [Association("UnitSequences")]
        public XPCollection<Sequence> Sequences
        {
            get { return GetCollection<Sequence>(nameof(Sequences)); }
        }

  /*      private string _BoldRecordId;
       [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
         public string BoldRecordId
        {
            get { return _BoldRecordId; }
            set { SetPropertyValue<string>(nameof(BoldRecordId), ref _BoldRecordId, value); }
        }*/


        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public IMapsMarker Self
        {
            get { return this; }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public IMapsMarker Map
        {
            get { return this; }
        }

    /*    private string _BoldBin;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
       // [ModelDefault("Caption", "BIN")]
        public string BoldBin
        {
            get { return _BoldBin; }
            set { SetPropertyValue<string>(nameof(BoldBin), ref _BoldBin, value); }
        }*/


       private Accession _Accession;
        [Association("AccessionUnits")]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Accession Accession
        {
            get { return _Accession; }
            set { SetPropertyValue<Accession>(nameof(Accession), ref _Accession, value);
                if (!IsLoading && value != null)
                {
                    if (String.IsNullOrEmpty(UnitID))
                    {
                        string unitid = Accession.Name + ".1";
                        int c = 1;
                        while (Session.FindObject<Unit>(new BinaryOperator("UnitID", unitid)) != null)
                        {
                            unitid = Accession.Name + "." + c++;
                        }
                        UnitID = unitid; ;
                        UpdateInfo();
                    }
                }
            }
        }


        [Association("LoanOutUnits")]
        public XPCollection<Loan> Loans
        {
            get { return GetCollection<Loan>(nameof(Loans)); }
        }


        private string _DateVerbatim;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string DateVerbatim
        {
            get { return _DateVerbatim; }
            set { SetPropertyValue<string>(nameof(DateVerbatim), ref _DateVerbatim, value); }
        }

        private PersonCore _CollectedBy;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public PersonCore CollectedBy
        {
            get { return _CollectedBy; }
            set { SetPropertyValue<PersonCore>(nameof(CollectedBy), ref _CollectedBy, value); }
        }

        private PersonCore _RecordedBy;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public PersonCore RecordedBy
        {
            get { return _RecordedBy; }
            set { SetPropertyValue<PersonCore>(nameof(RecordedBy), ref _RecordedBy, value); }
        }

        private string _LatitudeString;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string LatitudeString
        {
            get { return _LatitudeString; }
            set { SetPropertyValue<string>(nameof(LatitudeString), ref _LatitudeString, value); }
        }

        private string _LatitudeVerbatim;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string LatitudeVerbatim
        {
            get { return _LatitudeVerbatim; }
            set { SetPropertyValue<string>(nameof(LatitudeVerbatim), ref _LatitudeVerbatim, value); }
        }

        private string _LongitudeVerbatim;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string LongitudeVerbatim
        {
            get { return _LongitudeVerbatim; }
            set { SetPropertyValue<string>(nameof(LongitudeVerbatim), ref _LongitudeVerbatim, value); }
        }



        private string _LongitudeString;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string LongitudeString
        {
            get { return _LongitudeString; }
            set { SetPropertyValue<string>(nameof(LongitudeString), ref _LongitudeString, value); }
        }




        private DateTime? _RecordedOn;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public DateTime? RecordedOn
        {
            get { return _RecordedOn; }
            set { SetPropertyValue<DateTime?>(nameof(RecordedOn), ref _RecordedOn, value); }
        }





        private Double _Weight;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Double Weight
        {
            get { return _Weight; }
            set { SetPropertyValue<Double>(nameof(Weight), ref _Weight, value); }
        }

        private CalendarEventType _EventType;
        [Association("CalendarEventUnits")]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public CalendarEventType EventType
        {
            get { return _EventType; }
            set { SetPropertyValue<CalendarEventType>(nameof(EventType), ref _EventType, value); }
        }



        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Title { get { return FullName; } }

        private BoldSent? _SentToBOLD;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public BoldSent? SentToBOLD
        {
            get { return _SentToBOLD; }
            set { SetPropertyValue<BoldSent?>(nameof(SentToBOLD), ref _SentToBOLD, value); }
        }


        private double _Price;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Price
        {
            get { return _Price; }
            set { SetPropertyValue<double>(nameof(Price), ref _Price, value); }
        }

        private int? _RecordedYear;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? RecordedYear
        {
            get { return _RecordedYear; }
            set { SetPropertyValue<int?>(nameof(RecordedYear), ref _RecordedYear, value); }
        }


        private PersonCore _FromPerson;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public PersonCore FromPerson
        {
            get { return _FromPerson; }
            set { SetPropertyValue<PersonCore>(nameof(FromPerson), ref _FromPerson, value); }
        }

        private string _References;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        [ModelDefault("RowCount","1")]
        public string References
        {
            get { return _References; }
            set { SetPropertyValue<string>(nameof(References), ref _References, value); }
        }


        private PersonCore _ToPerson;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public PersonCore ToPerson
        {
            get { return _ToPerson; }
            set { SetPropertyValue<PersonCore>(nameof(ToPerson), ref _ToPerson, value); }
        }





        private float _Value;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public float Value
        {
            get { return _Value; }
            set { SetPropertyValue<float>(nameof(Value), ref _Value, value); }
        }

        

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]

        public string Tooltip { get { return UnitID; } }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            GISHelper.UpdateAreaAndLengthFromWKT(this);
            if (Session.IsNewObject(this))
            {
                if (String.IsNullOrEmpty(UnitID))
                {
                    if (Dataset != null)
                        if (Dataset.Autoincrement == true)
                        {
                            SequenceGenerator.GenerateSequence(this);
                            //   generating = true;
                            //  generating = false;
                            UnitID = Prefix + ((ISupportSequenceObject)this).Sequence;
                        }
                }
            }
            UpdateInfo();
            if (IsLoading) return;

            //  if (!Value_WindowController.Instance().Importing)
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        private int? _RegisteredYear;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? RegisteredYear
        {
            get { return _RegisteredYear; }
            set { SetPropertyValue<int?>(nameof(RegisteredYear), ref _RegisteredYear, value); }
        }



        private Geoprivacy? _Geoprivacy;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Geoprivacy? Geoprivacy
        {
            get { return _Geoprivacy; }
            set { SetPropertyValue<Geoprivacy?>(nameof(Geoprivacy), ref _Geoprivacy, value); }
        }

        private DateTime? _LoanDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? LoanDate
        {
            get { return _LoanDate; }
            set { SetPropertyValue<DateTime?>(nameof(LoanDate), ref _LoanDate, value); }
        }

        private DateTime? _LoadDueDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? LoadDueDate
        {
            get { return _LoadDueDate; }
            set { SetPropertyValue<DateTime?>(nameof(LoadDueDate), ref _LoadDueDate, value); }
        }

        private PersonCore _LoanRecipient;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public PersonCore LoanRecipient
        {
            get { return _LoanRecipient; }
            set { SetPropertyValue<PersonCore>(nameof(LoanRecipient), ref _LoanRecipient, value); }
        }

        private DateTime? _LoanReturnDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? LoanReturnDate
        {
            get { return _LoanReturnDate; }
            set { SetPropertyValue<DateTime?>(nameof(LoanReturnDate), ref _LoanReturnDate, value); }
        }






        private long _Sequence;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ImmediatePostData(true)]
        public long Sequence
        {
            get { return _Sequence; }
            set { SetPropertyValue<long>(nameof(Sequence), ref _Sequence, value); }
        }






        private BasisOfRecord _BasisOfRecord;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public BasisOfRecord BasisOfRecord
        {
            get { return _BasisOfRecord; }
            set { SetPropertyValue<BasisOfRecord>(nameof(BasisOfRecord), ref _BasisOfRecord, value); }
        }



        [Association("UnitNotes"), Aggregated]
        public XPCollection<UnitNote> Notes
        {
            get
            {
                return GetCollection<UnitNote>("Notes");
            }
        }
   /*   [VisibleInDetailView(false)]
        [Association("UnitEvent")]
        public XPCollection<UnitsEvent> UnitEvents
        {
            get
            {
                return GetCollection<UnitsEvent>("UnitEvents");
            }
        }*/

     /*   [VisibleInDetailView(false)]
        [Association("Unit-Interactions")]
        public XPCollection<UnitEvent> Interactions
        {
            get
            {
                return GetCollection<UnitEvent>("Interactions");
            }
        }
        */

        private string _UnitID;
        [NonCloneable]
        // [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save, SkipNullOrEmptyValues = true)]
        [Indexed]
        [DocumentationAttribute(Description="Unique identifier for occurrence or specimen record. Used as a key when importing data.")]
        [DwCAttribute(Term = "occurrenceID")]
        public string UnitID
        {
            get { return _UnitID; }
            set { SetPropertyValue<string>(nameof(UnitID), ref _UnitID, value); }
        }



        private TaxonomicName _TaxonomicName;
        [Association("TaxonomicName-Units")]
        [ImmediatePostData(true)]
        public TaxonomicName TaxonomicName
        {
            get { return _TaxonomicName; }
            set { SetPropertyValue<TaxonomicName>(nameof(TaxonomicName), ref _TaxonomicName, value); }
        }




        private long _GbifKey;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public long GbifKey
        {
            get { return _GbifKey; }
            set { SetPropertyValue<long>(nameof(GbifKey), ref _GbifKey, value); }
        }


        private string _GbifOccurrenceID;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string GbifOccurrenceID
        {
            get { return _GbifOccurrenceID; }
            set { SetPropertyValue<string>(nameof(GbifOccurrenceID), ref _GbifOccurrenceID, value); }
        }



        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public TaxonomicName TaxonomicNameGbif
        {
            get
            {
                if (TaxonomicName != null)
                {
                    if (TaxonomicName.GbifValidTaxonomicName != null)
                    {
                        return TaxonomicName.GbifValidTaxonomicName;
                    }
                    return TaxonomicName;
                }
                return null;
            }
        }


        private Locality _Locality;

        [Association("Locality-Units")]
        public Locality Locality
        {
            get { return _Locality; }
            set { SetPropertyValue<Locality>(nameof(Locality), ref _Locality, value); }
        }

        private DateTime? _Date;
        public DateTime? Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime?>(nameof(Date), ref _Date, value); }
        }


        private int? _Slope;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Slope
        {
            get { return _Slope; }
            set { SetPropertyValue<int?>(nameof(Slope), ref _Slope, value); }
        }



        private SlopeOrientation? _SlopeOrientation;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SlopeOrientation? SlopeOrientation
        {
            get { return _SlopeOrientation; }
            set { SetPropertyValue<SlopeOrientation?>(nameof(SlopeOrientation), ref _SlopeOrientation, value); }
        }

        private string _Prefix;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Prefix
        {
            get
            {
                if (string.IsNullOrEmpty(_Prefix))
                {
                    if (Dataset != null) return Dataset.Prefix;

                }
                return _Prefix;
            }
            set
            {
                _Prefix = value;
                // UnitID = ((ISupportSequenceObject)this).Prefix + value.ToString();
            }
        }

        private Int32? _CollectionNumber;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Int32? CollectionNumber
        {
            get { return _CollectionNumber; }
            set { SetPropertyValue<Int32?>(nameof(CollectionNumber), ref _CollectionNumber, value); }
        }




        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(4000)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }


        private Dataset _Dataset;
        [Association("DatasetUnits")]
        [DwCAttribute("datasetName")]
        public Dataset Dataset
        {
            get { return _Dataset; }
            set { SetPropertyValue<Dataset>(nameof(Dataset), ref _Dataset, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private Habitat _Habitat;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Association("Unit-Habitat")]
        public Habitat Habitat
        {
            get { return _Habitat; }
            set { SetPropertyValue<Habitat>(nameof(Habitat), ref _Habitat, value); }
        }

        private Habitat _Microhabitat;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Habitat Microhabitat
        {
            get { return _Microhabitat; }
            set { SetPropertyValue<Habitat>(nameof(Microhabitat), ref _Microhabitat, value); }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        protected override void OnDeleting()
        {
            base.OnDeleting();
        }

        
        public override void AfterConstruction()
        {

            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.AfterConstruction();
            EPSG = 4326;
            if (Session.IsNewObject(this))
            {


                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
                if (Dataset == null)
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultDatasetName"]))
                    {
                        Dataset = Session.FindObject<Dataset>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultDatasetName"]));
                    }

                if (DerivedFrom != null)
                {
                    Locality = DerivedFrom.Locality;
                    Dataset = DerivedFrom.Dataset;
                    Store = DerivedFrom.Store;
                    Date = DerivedFrom.Date;
                    EndOn = DerivedFrom.EndOn;
                    TaxonomicName = DerivedFrom.TaxonomicName;
                    IdentifiedBy = DerivedFrom.IdentifiedBy;
                    Quantity = DerivedFrom.Quantity;
                    QuantityUnit = DerivedFrom.QuantityUnit;
                    Year = DerivedFrom.Year;
                    UnitID = String.Format("{0}-1", DerivedFrom.UnitID);
                    UnitType = DerivedFrom.UnitType;
                    OtherId = DerivedFrom.OtherId;
                    Sex = DerivedFrom.Sex;
                    BirthDate = DerivedFrom.BirthDate;
                    DeathDate = DerivedFrom.DeathDate;
                    WKT = DerivedFrom.WKT;
                }
                if (UnitAssembly != null)
                {
                    Locality = UnitAssembly.Locality;
                    Dataset = UnitAssembly.Dataset;
                    Store = UnitAssembly.Store;
                    Date = UnitAssembly.Date;
                    EndOn = UnitAssembly.EndOn;
                    TaxonomicName = UnitAssembly.TaxonomicName;
                    IdentifiedBy = UnitAssembly.IdentifiedBy;
                    Quantity = UnitAssembly.Quantity;
                    QuantityUnit = UnitAssembly.QuantityUnit;
                    Year = UnitAssembly.Year;
                    UnitType = UnitAssembly.UnitType;
                    OtherId = UnitAssembly.OtherId;
                    Sex = UnitAssembly.Sex;
                    BirthDate = UnitAssembly.BirthDate;
                    DeathDate = UnitAssembly.DeathDate;
                    WKT = UnitAssembly.WKT;

                }

            }
        }


        /* private CustomValueSet _CustomValueSet;
         [ExplicitLoading(0)]
         [VisibleInListView(false)]
         [VisibleInLookupListView(false)]
         [Aggregated, ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
         public CustomValueSet CustomValueSet
         {
             get
             {
                 return _CustomValueSet;
             }
             set
             {
                 SetPropertyValue("CustomValueSet", ref _CustomValueSet, value);
             }
         }*/



        private double _Area;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Area
        {
            get { return _Area; }
            set { SetPropertyValue<double>(nameof(Area), ref _Area, value); }
        }


        private string _CoordSource;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CoordSource
        {
            get { return _CoordSource; }
            set { SetPropertyValue<string>(nameof(CoordSource), ref _CoordSource, value); }
        }

        private int? _CoordAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? CoordAccuracy
        {
            get { return _CoordAccuracy; }
            set { SetPropertyValue<int?>(nameof(CoordAccuracy), ref _CoordAccuracy, value); }
        }



        private double _Length;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Length
        {
            get { return _Length; }
            set { SetPropertyValue<double>(nameof(Length), ref _Length, value); }
        }





        private Decimal? _Centroid_X;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_X
        {
            get { return _Centroid_X; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_X), ref _Centroid_X, value); }
        }



        private Decimal? _Centroid_Y;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_Y
        {
            get { return _Centroid_Y; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_Y), ref _Centroid_Y, value); }
        }



        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }

        private double _Latitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Latitude
        {
            get { return _Latitude; }
            set { SetPropertyValue<double>(nameof(Latitude), ref _Latitude, value); }
        }


        private double _Longitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Longitude
        {
            get { return _Longitude; }
            set { SetPropertyValue<double>(nameof(Longitude), ref _Longitude, value); }
        }


        private double _GpsPrecision;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public double GpsPrecision
        {
            get { return _GpsPrecision; }
            set { SetPropertyValue<double>(nameof(GpsPrecision), ref _GpsPrecision, value); }
        }


        private LocalityVisit _LocalityVisit;
        [Association("LocalityVisit-Units")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public LocalityVisit LocalityVisit
        {
            get { return _LocalityVisit; }
            set { SetPropertyValue<LocalityVisit>(nameof(LocalityVisit), ref _LocalityVisit, value); }
        }



        private string _Preservation;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Preservation
        {
            get { return _Preservation; }
            set { SetPropertyValue<string>(nameof(Preservation), ref _Preservation, value); }
        }



        private string _OtherId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string OtherId
        {
            get { return _OtherId; }
            set { SetPropertyValue<string>(nameof(OtherId), ref _OtherId, value); }
        }



        private string _Host;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Host
        {
            get { return _Host; }
            set { SetPropertyValue<string>(nameof(Host), ref _Host, value); }
        }



        private string _HostDetails;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string HostDetails
        {
            get { return _HostDetails; }
            set { SetPropertyValue<string>(nameof(HostDetails), ref _HostDetails, value); }
        }


        private string _LabelText;
        [Size(1000)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string LabelText
        {
            get { return _LabelText; }
            set { SetPropertyValue<string>(nameof(LabelText), ref _LabelText, value); }
        }



        [Association("Unit-Identifications")]
        [Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<UnitIdentification> Identifications
        {
            get
            {
                return GetCollection<UnitIdentification>("Identifications");
            }
        }

        private Unit _UnitAssembly;
        [Association("Unit-Subunits")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Unit UnitAssembly
        {
            get { return _UnitAssembly; }
            set { SetPropertyValue<Unit>(nameof(UnitAssembly), ref _UnitAssembly, value); }
        }



        [Association("Unit-Subunits"), Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<Unit> Subunits
        {
            get
            {
                return GetCollection<Unit>("Subunits");
            }
        }


        private bool _DoesNotExist;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool DoesNotExist
        {
            get { return _DoesNotExist; }
            set { SetPropertyValue<bool>(nameof(DoesNotExist), ref _DoesNotExist, value); }
        }


        private string _InstitutionCode;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string InstitutionCode
        {
            get { return _InstitutionCode; }
            set { SetPropertyValue<string>(nameof(InstitutionCode), ref _InstitutionCode, value); }
        }


        private string _CollectionCode;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string CollectionCode
        {
            get { return _CollectionCode; }
            set { SetPropertyValue<string>(nameof(CollectionCode), ref _CollectionCode, value); }
        }


        private DevelopmentStage? _Stage;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DevelopmentStage? Stage
        {
            get { return _Stage; }
            set { SetPropertyValue<DevelopmentStage?>(nameof(Stage), ref _Stage, value);}
        }


        private string _Island;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Island
        {
            get { return _Island; }
            set { SetPropertyValue<string>(nameof(Island), ref _Island, value); }
        }



        private SpecimenType? _Type;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public SpecimenType? Type
        {
            get { return _Type; }
            set { SetPropertyValue<SpecimenType?>(nameof(Type), ref _Type, value); }
        }



        private string _AssociatedTaxa;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string AssociatedTaxa
        {
            get { return _AssociatedTaxa; }
            set { SetPropertyValue<string>(nameof(AssociatedTaxa), ref _AssociatedTaxa, value); }
        }

        private string _AssociatedUnits;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string AssociatedUnits
        {
            get { return _AssociatedUnits; }
            set { SetPropertyValue<string>(nameof(AssociatedUnits), ref _AssociatedUnits, value); }
        }




        private TypeStatus _TypeStatus;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public TypeStatus TypeStatus
        {
            get { return _TypeStatus; }
            set { SetPropertyValue<TypeStatus>(nameof(TypeStatus), ref _TypeStatus, value); }
        }



        private Sex? _Sex;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Sex? Sex
        {
            get { return _Sex; }
            set { SetPropertyValue<Sex?>(nameof(Sex), ref _Sex, value); }
        }


        private string _Url;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("Unit.Url.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [ModelDefault("RowCount", "1")]
        public string Url
        {
            get { return _Url; }
            set { SetPropertyValue<string>(nameof(Url), ref _Url, value); }
        }




        public void UpdateInfo()
        {
            string res = "";
            foreach (UnitAttachment at in this.Attachments)
            {
                if (String.IsNullOrEmpty(res))
                    res = at.JpegUrl;
                else
                    res = res + " ," + Environment.NewLine + at.JpegUrl;
            }
            if (!String.IsNullOrEmpty(res)) AssociatedMedia = res;

            if ((!String.IsNullOrEmpty(UnitID)))
            { string val = Regex.Match(UnitID, @"(\d+)(?!.*\d)").Value;
                if (val.Length>0)
                Sequence = Convert.ToInt64(val);
            }

            if ((IdentifiedOn != null))
                IdentifiedOnYear = IdentifiedOn.Value.Year;
            if (Date != null)
            {
                    Year = Date.Value.Year;
                     Month = Date.Value.Month;
                     Day = Date.Value.Day;
            }
            else
                if (((Date == DateTime.MinValue) || (Date == null)))
            {
                if (((Year != null) && (Year != 0)))
                    if (((Month != null) && (Month != 0)))
                        if (((Day != null) && (Day != 0)))
                            try
                            {
                               // Date = new DateTime(Year.Value, Month.Value, Day.Value);
                            }
                            catch (Exception)
                            {


                            }
            }

                if (Accession != null)
            {
                if (TaxonomicName == null)
                    if (Accession.TaxonomicName != null)
                        TaxonomicName = Accession.TaxonomicName;
                if (Locality == null)
                    if (Accession.Locality != null)
                        Locality = Accession.Locality;
                if (Store == null)
                    if (Accession.Store != null)
                        Store = Accession.Store;
            }
            if (LocalityVisit != null)
            {
                // if (CollectedOrObservedByText == null)
                //   CollectedOrObservedByText = LocalityVisit.Team;
                if (Locality == null)
                    Locality = LocalityVisit.Locality;
                if (Locality1 == null)
                    Locality1 = LocalityVisit.Locality1;
                if (Date == null)
                    if (LocalityVisit.DateTimeStart!=null)
                    Date = LocalityVisit.DateTimeStart.Value;
                if (EndOn == null)
                    if (LocalityVisit.DateTimeEnd!=null)
                    EndOn = LocalityVisit.DateTimeEnd;
                if (Method == null)
                    if (LocalityVisit.SamplingMethod!=null)
                    Method = LocalityVisit.SamplingMethod;
                if (Habitat == null)
                    if (LocalityVisit.Habitat!=null)
                    Habitat = LocalityVisit.Habitat;
                // if (false)
                    // Length = LocalityVisit.Length;
            }
            FullName = String.Format("UNIT:{0}|{1}|{2}|{3}", UnitID, (TaxonomicName != null) ? TaxonomicName.FullName : string.Empty, (Locality != null) ? Locality.ToString() : string.Empty, (Date != null) ? Date.Value.Year.ToString() + Date.Value.Month.ToString() + Date.Value.Day.ToString() : string.Empty);
        }


        private DateTime? _BirthDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? BirthDate
        {
            get { return _BirthDate; }
            set { SetPropertyValue<DateTime?>(nameof(BirthDate), ref _BirthDate, value); }
        }

        private string _Source;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Source
        {
            get { return _Source; }
            set { SetPropertyValue<string>(nameof(Source), ref _Source, value); }
        }


        private int? _AuditYear;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? AuditYear
        {
            get { return _AuditYear; }
            set { SetPropertyValue<int?>(nameof(AuditYear), ref _AuditYear, value); }
        }




        private DateTime? _DeathDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? DeathDate
        {
            get { return _DeathDate; }
            set { SetPropertyValue<DateTime?>(nameof(DeathDate), ref _DeathDate, value); }
        }

        private string _DirectionOfObservation;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string DirectionOfObservation
        {
            get { return _DirectionOfObservation; }
            set { SetPropertyValue<string>(nameof(DirectionOfObservation), ref _DirectionOfObservation, value); }
        }



        public Unit(Session session) : base(session) { }

        string IName.Name
        { get { return UnitID; } }


        public override string ToString()
        {
            return FullName;
        }

        private string _IdentifiedBy;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentifiedBy
        {
            get { return _IdentifiedBy; }
            set { SetPropertyValue<string>(nameof(IdentifiedBy), ref _IdentifiedBy, value); }
        }

        private string _IdentifierEmail;
        [RuleRegularExpression(null, "IdentifierEmail", EarthCapeModule.EmailPattern)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentifierEmail
        {
            get { return _IdentifierEmail; }
            set { SetPropertyValue<string>(nameof(IdentifierEmail), ref _IdentifierEmail, value); }
        }


        private string _IdentifierInstitution;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentifierInstitution
        {
            get { return _IdentifierInstitution; }
            set { SetPropertyValue<string>(nameof(IdentifierInstitution), ref _IdentifierInstitution, value); }
        }



        private string _IdentificationConfirmedBy;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentificationConfirmedBy
        {
            get { return _IdentificationConfirmedBy; }
            set { SetPropertyValue<string>(nameof(IdentificationConfirmedBy), ref _IdentificationConfirmedBy, value); }
        }



        private DateTime? _IdentifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? IdentifiedOn
        {
            get { return _IdentifiedOn; }
            set { SetPropertyValue<DateTime?>(nameof(IdentifiedOn), ref _IdentifiedOn, value); }
        }


        private string _IdentificationComments;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentificationComments
        {
            get { return _IdentificationComments; }
            set { SetPropertyValue<string>(nameof(IdentificationComments), ref _IdentificationComments, value); }
        }

        private string _Reproduction;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Reproduction
        {
            get { return _Reproduction; }
            set { SetPropertyValue<string>(nameof(Reproduction), ref _Reproduction, value); }
        }

        private DwcDisposition _Disposition;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DwcDisposition Disposition
        {
            get { return _Disposition; }
            set { SetPropertyValue<DwcDisposition>(nameof(Disposition), ref _Disposition, value); }
        }




        private string _IdentificationMethod;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string IdentificationMethod
        {
            get { return _IdentificationMethod; }
            set { SetPropertyValue<string>(nameof(IdentificationMethod), ref _IdentificationMethod, value); }
        }



        private string _CollectedOrObservedByText;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string CollectedOrObservedByText
        {
            get { return _CollectedOrObservedByText; }
            set { SetPropertyValue<string>(nameof(CollectedOrObservedByText), ref _CollectedOrObservedByText, value); }
        }


        private string _CollectionMethod;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string CollectionMethod
        {
            get { return _CollectionMethod; }
            set { SetPropertyValue<string>(nameof(CollectionMethod), ref _CollectionMethod, value); }
        }



        private double _Distance;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Distance
        {
            get { return _Distance; }
            set { SetPropertyValue<double>(nameof(Distance), ref _Distance, value); }
        }


        private AgeClass _AgeClass;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public AgeClass AgeClass
        {
            get { return _AgeClass; }
            set { SetPropertyValue<AgeClass>(nameof(AgeClass), ref _AgeClass, value); }
        }


        private string _Weather;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Weather
        {
            get { return _Weather; }
            set { SetPropertyValue<string>(nameof(Weather), ref _Weather, value); }
        }



        private int _Males;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int Males
        {
            get { return _Males; }
            set { SetPropertyValue<int>(nameof(Males), ref _Males, value); }
        }

        private int _Broods;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int Broods
        {
            get { return _Broods; }
            set { SetPropertyValue<int>(nameof(Broods), ref _Broods, value); }
        }



        private string _Behaviour;

        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Behaviour
        {
            get { return _Behaviour; }
            set { SetPropertyValue<string>(nameof(Behaviour), ref _Behaviour, value); }
        }


        private int _Juveniles;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int Juveniles
        {
            get { return _Juveniles; }
            set { SetPropertyValue<int>(nameof(Juveniles), ref _Juveniles, value); }
        }



        private int _Females;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int Females
        {
            get { return _Females; }
            set { SetPropertyValue<int>(nameof(Females), ref _Females, value); }
        }

        private int _SexUndefined;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int SexUndefined
        {
            get { return _SexUndefined; }
            set { SetPropertyValue<int>(nameof(SexUndefined), ref _SexUndefined, value); }
        }


        private double _Age;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Age
        {
            get { return _Age; }
            set { SetPropertyValue<double>(nameof(Age), ref _Age, value); }
        }


        private Method _Method;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Method Method
        {
            get { return _Method; }
            set { SetPropertyValue<Method>(nameof(Method), ref _Method, value); }
        }



        private bool _IsDead;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool IsDead
        {
            get { return _IsDead; }
            set { SetPropertyValue<bool>(nameof(IsDead), ref _IsDead, value); }
        }


        private bool _Cf;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool Cf
        {
            get { return _Cf; }
            set { SetPropertyValue<bool>(nameof(Cf), ref _Cf, value); }
        }


        private DateTime? _CheckDate;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? CheckDate
        {
            get { return _CheckDate; }
            set { SetPropertyValue<DateTime?>(nameof(CheckDate), ref _CheckDate, value); }
        }



        private Unit _DerivedFrom;
        [Association("UnitBase-DerivedFrom")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Unit DerivedFrom
        {
            get { return _DerivedFrom; }
            set { SetPropertyValue<Unit>(nameof(DerivedFrom), ref _DerivedFrom, value); }
        }


        [Association("UnitBase-DerivedFrom")]
        public XPCollection<Unit> DerivedUnits
        {
            get
            {
                return GetCollection<Unit>("DerivedUnits");
            }
        }

        private double? _Altitude1;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Altitude1
        {
            get { return _Altitude1; }
            set { SetPropertyValue<double?>(nameof(Altitude1), ref _Altitude1, value); }
        }

        private double? _Altitude2;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double? Altitude2
        {
            get { return _Altitude2; }
            set { SetPropertyValue<double?>(nameof(Altitude2), ref _Altitude2, value); }
        }


        private int? _Year;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("DisplayFormat", "{0:F0}")]
        [ModelDefault("EditMask", "F0")]
        public int? Year
        {
            get { return _Year; }
            set { SetPropertyValue<int?>(nameof(Year), ref _Year, value); }
        }

        private int? _IdentifiedOnYear;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("DisplayFormat", "{0:F0}")]
        [ModelDefault("EditMask", "F0")]
        public int? IdentifiedOnYear
        {
            get { return _IdentifiedOnYear; }
            set { SetPropertyValue<int?>(nameof(IdentifiedOnYear), ref _IdentifiedOnYear, value); }
        }


        private int? _IdentificationConfirmedYear;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("DisplayFormat", "{0:F0}")]
        [ModelDefault("EditMask", "F0")]
        public int? IdentificationConfirmedYear
        {
            get { return _IdentificationConfirmedYear; }
            set { SetPropertyValue<int?>(nameof(IdentificationConfirmedYear), ref _IdentificationConfirmedYear, value); }
        }


        private int? _Month;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Month
        {
            get { return _Month; }
            set { SetPropertyValue<int?>(nameof(Month), ref _Month, value); }
        }


        private int? _Day1;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Day1
        {
            get { return _Day1; }
            set { SetPropertyValue<int?>(nameof(Day1), ref _Day1, value); }
        }

        private int? _Month1;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Month1
        {
            get { return _Month1; }
            set { SetPropertyValue<int?>(nameof(Month1), ref _Month1, value); }
        }


        private int? _Year1;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Year1
        {
            get { return _Year1; }
            set { SetPropertyValue<int?>(nameof(Year1), ref _Year1, value); }
        }



        private int? _Day;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Day
        {
            get { return _Day; }
            set { SetPropertyValue<int?>(nameof(Day), ref _Day, value); }
        }


        private string _WKT;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string WKT
        {
            get { return _WKT; }
            set { SetPropertyValue<string>(nameof(WKT), ref _WKT, value); }
        }




        private string _FieldID;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string FieldID
        {
            get { return _FieldID; }
            set { SetPropertyValue<string>(nameof(FieldID), ref _FieldID, value); }
        }


        private string _UnitType;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string UnitType
        {
            get { return _UnitType; }
            set { SetPropertyValue<string>(nameof(UnitType), ref _UnitType, value); }
        }


        private string _TemporaryName;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(500)]
        [ModelDefault("RowCount","1")]
        public string TemporaryName
        {
            get { return _TemporaryName; }
            set { SetPropertyValue<string>(nameof(TemporaryName), ref _TemporaryName, value); }
        }



        private DateTime? _EndOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public DateTime? EndOn
        {
            get { return _EndOn; }
            set { SetPropertyValue<DateTime?>(nameof(EndOn), ref _EndOn, value); }
        }



        private DateTime? _PreparationDate;
        public DateTime? PreparationDate
        {
            get { return _PreparationDate; }
            set { SetPropertyValue<DateTime?>(nameof(PreparationDate), ref _PreparationDate, value); }
        }



        private string _PreparedBy;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string PreparedBy
        {
            get { return _PreparedBy; }
            set { SetPropertyValue<string>(nameof(PreparedBy), ref _PreparedBy, value); }
        }



        private string _PreparationType;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string PreparationType
        {
            get { return _PreparationType; }
            set { SetPropertyValue<string>(nameof(PreparationType), ref _PreparationType, value); }
        }



        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Image Image
        {
            get
            {
                if (!IsLoading)
                    if (Attachments.Count > 0)
                        if (Attachments[0].Thumbnail != null)
                            return Attachments[0].Thumbnail;
                return null;
            }
        }
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Image Thumbnail
        {
            get
            {
                if (!IsLoading)
                    if (Attachments.Count > 0)
                        if (Attachments[0].Thumbnail != null)
                            return Attachments[0].Thumbnail;
                return null;
            }
        }


        private string _AssociatedMedia;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string AssociatedMedia
        {
            get { return _AssociatedMedia; }
            set { SetPropertyValue<string>(nameof(AssociatedMedia), ref _AssociatedMedia, value); }
        }


        private string _AccessionEna;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string AccessionEna
        {
            get { return _AccessionEna; }
            set { SetPropertyValue<string>(nameof(AccessionEna), ref _AccessionEna, value); }
        }



        private Locality _Locality1;
        [Association("Locality1-Units1")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Locality Locality1
        {
            get { return _Locality1; }
            set { SetPropertyValue<Locality>(nameof(Locality1), ref _Locality1, value); }
        }

        private Store _Store;
        [Association("Store-Units")]
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }



        private string _WhereInStore;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string WhereInStore
        {
            get { return _WhereInStore; }
            set { SetPropertyValue<string>(nameof(WhereInStore), ref _WhereInStore, value); }
        }


        private string _LocalityComment;
        [Size(SizeAttribute.Unlimited)]
        [ModelDefault("RowCount", "1")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string LocalityComment
        {
            get { return _LocalityComment; }
            set { SetPropertyValue<string>(nameof(LocalityComment), ref _LocalityComment, value); }
        }



        private string _PlaceDetails;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        public string PlaceDetails
        {
            get { return _PlaceDetails; }
            set { SetPropertyValue<string>(nameof(PlaceDetails), ref _PlaceDetails, value); }
        }



        private string _HabitatDetails;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string HabitatDetails
        {
            get { return _HabitatDetails; }
            set { SetPropertyValue<string>(nameof(HabitatDetails), ref _HabitatDetails, value); }
        }



        private string _AlternativeDate;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string AlternativeDate
        {
            get { return _AlternativeDate; }
            set { SetPropertyValue<string>(nameof(AlternativeDate), ref _AlternativeDate, value); }
        }



        private string _BiologyDetails;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string BiologyDetails
        {
            get { return _BiologyDetails; }
            set { SetPropertyValue<string>(nameof(BiologyDetails), ref _BiologyDetails, value); }
        }


        private string _QuantityUnit;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string QuantityUnit
        {
            get { return _QuantityUnit; }
            set { SetPropertyValue<string>(nameof(QuantityUnit), ref _QuantityUnit, value); }
        }


        private double _Quantity;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Quantity
        {
            get { return _Quantity; }
            set { SetPropertyValue<double>(nameof(Quantity), ref _Quantity, value); }
        }



        [Association("Unit-Duplicates")]
        [VisibleInDetailView(false)]
        [Aggregated]
        public XPCollection<UnitDuplicate> Duplicates
        {
            get
            {
                return GetCollection<UnitDuplicate>("Duplicates");
            }
        }

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public bool HasCoordinates
        {
            get { return ((Centroid_X != null) && ((Centroid_Y != null))); }
        }

        private Brood _Brood;
        [Association("BroodUnits")]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Brood Brood
        {
            get { return _Brood; }
            set { SetPropertyValue<Brood>(nameof(Brood), ref _Brood, value); }
        }

      
        private Genotype _Genotype;

        [Association("Genotype-Units")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Genotype Genotype
        {
            get { return _Genotype; }
            set { SetPropertyValue<Genotype>(nameof(Genotype), ref _Genotype, value); }
        }


        private string _Continent;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Continent
        {
            get { return _Continent; }
            set { SetPropertyValue<string>(nameof(Continent), ref _Continent, value); }
        }




        private string _CountryCode;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CountryCode
        {
            get { return _CountryCode; }
            set { SetPropertyValue<string>(nameof(CountryCode), ref _CountryCode, value); }
        }



        private String _Country;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public String Country
        {
            get { return _Country; }
            set { SetPropertyValue<String>(nameof(Country), ref _Country, value); }
        }

        private string _State;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string State
        {
            get { return _State; }
            set { SetPropertyValue<string>(nameof(State), ref _State, value); }
        }

        private string _Province;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Province
        {
            get { return _Province; }
            set { SetPropertyValue<string>(nameof(Province), ref _Province, value); }
        }

        private string _Town;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string Town
        {
            get { return _Town; }
            set { SetPropertyValue<string>(nameof(Town), ref _Town, value); }
        }

        private string _INatUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [EditorAlias("HyperLinkPropertyEditor")]
        [Size(255)]
        public string INatUrl
        {
            get { return _INatUrl; }
            set { SetPropertyValue<string>(nameof(INatUrl), ref _INatUrl, value); }
        }

        private int? _INatId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? INatId
        {
            get { return _INatId; }
            set { SetPropertyValue<int?>(nameof(INatId), ref _INatId, value); }
        }


        private string _CrossType;
        [Size(255)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CrossType
        {
            get { return _CrossType; }
            set { SetPropertyValue<string>(nameof(CrossType), ref _CrossType, value); }
        }



        private int? _Cloud;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Cloud
        {
            get { return _Cloud; }
            set { SetPropertyValue<int?>(nameof(Cloud), ref _Cloud, value); }
        }


        private int? _Wind;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? Wind
        {
            get { return _Wind; }
            set { SetPropertyValue<int?>(nameof(Wind), ref _Wind, value); }
        }


        private int _CoordinateUncertaintyInMeters;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int CoordinateUncertaintyInMeters
        {
            get { return _CoordinateUncertaintyInMeters; }
            set { SetPropertyValue<int>(nameof(CoordinateUncertaintyInMeters), ref _CoordinateUncertaintyInMeters, value); }
        }



        private string _Precipitation;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Precipitation
        {
            get { return _Precipitation; }
            set { SetPropertyValue<string>(nameof(Precipitation), ref _Precipitation, value); }
        }




        [Association("Unit-Citations")]
        [VisibleInDetailView(false)]
        public XPCollection<CitationUnit> Citations
        {
            get
            {
                return GetCollection<CitationUnit>("Citations");
            }
        }
        [VisibleInDetailView(false)]
        [Association("RecordSets-Units")]
        public XPCollection<RecordSet> RecordSets
        {
            get
            {
                return GetCollection<RecordSet>("RecordSets");
            }
        }
        [Association("Unit-Attachments")]
        public XPCollection<UnitAttachment> Attachments
        {
            get
            {
                return GetCollection<UnitAttachment>("Attachments");
            }
        }

        [Association("DNAExtractTissue"), Aggregated]
        [VisibleInDetailView(false)]
        public XPCollection<DnaExtract> DnaExtracts
        {
            get { return GetCollection<DnaExtract>(nameof(DnaExtracts)); }
        }
        [Association("Sequence-Unit")]
        [VisibleInDetailView(false)]
        public XPCollection<Sequencing> SequencingRuns
        {
            get
            {
                return GetCollection<Sequencing>(nameof(SequencingRuns));
            }
        }
     

        private string _Collection;

        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Collection
        {
            get { return _Collection; }
            set { SetPropertyValue<string>(nameof(Collection), ref _Collection, value); }
        }


        private string _InfraspecificEpithet;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string InfraspecificEpithet
        {
            get { return _InfraspecificEpithet; }
            set { SetPropertyValue<string>(nameof(InfraspecificEpithet), ref _InfraspecificEpithet, value); }
        }

        private string _SpecificEpithet;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string SpecificEpithet
        {
            get { return _SpecificEpithet; }
            set { SetPropertyValue<string>(nameof(SpecificEpithet), ref _SpecificEpithet, value); }
        }

        private string _Genus;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Genus
        {
            get { return _Genus; }
            set { SetPropertyValue<string>(nameof(Genus), ref _Genus, value); }
        }

        private string _Family;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Family
        {
            get { return _Family; }
            set { SetPropertyValue<string>(nameof(Family), ref _Family, value); }
        }
        private string _Subfamily;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Subfamily
        {
            get { return _Subfamily; }
            set { SetPropertyValue<string>(nameof(Subfamily), ref _Subfamily, value); }
        }


        private string _Order;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Order
        {
            get { return _Order; }
            set { SetPropertyValue<string>(nameof(Order), ref _Order, value); }
        }


        private string _Class;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Class
        {
            get { return _Class; }
            set { SetPropertyValue<string>(nameof(Class), ref _Class, value); }
        }

        private string _Phylum;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Phylum
        {
            get { return _Phylum; }
            set { SetPropertyValue<string>(nameof(Phylum), ref _Phylum, value); }
        }

        private string _Kingdom;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string Kingdom
        {
            get { return _Kingdom; }
            set { SetPropertyValue<string>(nameof(Kingdom), ref _Kingdom, value); }
        }












    }
}
