using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;

using System.ComponentModel;
using System.Linq;
using System.Text;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    //[MapInheritance(MapInheritanceType.ParentTable)]
    [ExcelImportKeyAttribute("Name")]
    public class CalendarEventType : BaseObject, IName, ICode
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public CalendarEventType(Session session)
            : base(session)
        {
        }

        private string _Code;
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        private string _EventTypeOriginal;
        [Size(500)]
        public string EventTypeOriginal
        {
            get { return _EventTypeOriginal; }
            set { SetPropertyValue<string>(nameof(EventTypeOriginal), ref _EventTypeOriginal, value); }
        }

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        [Association("CalendarEventUnits")]
        public XPCollection<Unit> UnitEvents
        {
            get
            {
                return GetCollection<Unit>("UnitEvents");
            }
        }
    }
}
