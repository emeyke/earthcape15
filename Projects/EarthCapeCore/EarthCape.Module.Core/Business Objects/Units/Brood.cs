
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;

using System.ComponentModel;


namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    [DefaultProperty("Code")]
    public class Brood : BaseObject, ICode, IDatasetObject, IUnits
    {

        public Brood(Session session) : base(session) { }

        private Dataset _Dataset;

        [Association("Dataset-Broods")]
        public Dataset Dataset
        {
            get { return _Dataset; }
            set { SetPropertyValue<Dataset>(nameof(Dataset), ref _Dataset, value); }
        }



        private string _Code;
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }


        private string _CrossType;
        public string CrossType
        {
            get { return _CrossType; }
            set { SetPropertyValue<string>(nameof(CrossType), ref _CrossType, value); }
        }


        private DateTime _FemaleDiedOn;
        public DateTime FemaleDiedOn
        {
            get { return _FemaleDiedOn; }
            set { SetPropertyValue<DateTime>(nameof(FemaleDiedOn), ref _FemaleDiedOn, value); }
        }





        private DateTime _MatedOn;
        public DateTime MatedOn
        {
            get { return _MatedOn; }
            set { SetPropertyValue<DateTime>(nameof(MatedOn), ref _MatedOn, value); }
        }


        private int _MatingDays;
        public int MatingDays
        {
            get { return _MatingDays; }
            set { SetPropertyValue<int>(nameof(MatingDays), ref _MatingDays, value); }
        }


        private int _LayingDays;
        public int LayingDays
        {
            get { return _LayingDays; }
            set { SetPropertyValue<int>(nameof(LayingDays), ref _LayingDays, value); }
        }

       
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private Unit _Female;
        public Unit Female
        {
            get { return _Female; }
            set { SetPropertyValue<Unit>(nameof(Female), ref _Female, value); }
        }


        private Unit _Male;
        public Unit Male
        {
            get { return _Male; }
            set { SetPropertyValue<Unit>(nameof(Male), ref _Male, value); }
        }



        private int _Eclosed;
        public int Eclosed
        {
            get { return _Eclosed; }
            set { SetPropertyValue<int>(nameof(Eclosed), ref _Eclosed, value); }
        }


        private int _Females;
        public int Females
        {
            get { return _Females; }
            set { SetPropertyValue<int>(nameof(Females), ref _Females, value); }
        }



        private int _Hatched;
        public int Hatched
        {
            get { return _Hatched; }
            set { SetPropertyValue<int>(nameof(Hatched), ref _Hatched, value); }
        }

        private int _Eggs;
        public int Eggs
        {
            get { return _Eggs; }
            set { SetPropertyValue<int>(nameof(Eggs), ref _Eggs, value); }
        }

        [Association("BroodUnits")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
    }

}
