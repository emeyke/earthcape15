using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Lab;
using System;
using TatukGIS.NDK;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DisplayProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class Store : BaseObject, IUnits, ITreeNode, IProjectObject, IName, IWKT
    {

        private int? _CoordAccuracy;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public int? CoordAccuracy
        {
            get { return _CoordAccuracy; }
            set { SetPropertyValue<int?>(nameof(CoordAccuracy), ref _CoordAccuracy, value); }
        }

        private GeometryType _GeometryType=GeometryType.Point;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public GeometryType GeometryType
        {
            get { return _GeometryType; }
            set { SetPropertyValue<GeometryType>(nameof(GeometryType), ref _GeometryType, value); }
        }

        private double _Area;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Area
        {
            get { return _Area; }
            set { SetPropertyValue<double>(nameof(Area), ref _Area, value); }
        }


        private string _CoordSource;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public string CoordSource
        {
            get { return _CoordSource; }
            set { SetPropertyValue<string>(nameof(CoordSource), ref _CoordSource, value); }
        }


        private double _Length;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public double Length
        {
            get { return _Length; }
            set { SetPropertyValue<double>(nameof(Length), ref _Length, value); }
        }





        private Decimal? _Centroid_X;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_X
        {
            get { return _Centroid_X; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_X), ref _Centroid_X, value); }
        }



        private Decimal? _Centroid_Y;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Custom("DisplayFormat", "{0:R}%")]
        [Custom("EditDisplayFormat", "{0:R}")]
        public Decimal? Centroid_Y
        {
            get { return _Centroid_Y; }
            set { SetPropertyValue<Decimal?>(nameof(Centroid_Y), ref _Centroid_Y, value); }
        }



        private int _EPSG;
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public int EPSG
        {
            get { return _EPSG; }
            set { SetPropertyValue<int>(nameof(EPSG), ref _EPSG, value); }
        }

        private double _Latitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Latitude
        {
            get { return _Latitude; }
            set { SetPropertyValue<double>(nameof(Latitude), ref _Latitude, value); }
        }


        private double _Longitude;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double Longitude
        {
            get { return _Longitude; }
            set { SetPropertyValue<double>(nameof(Longitude), ref _Longitude, value); }
        }

        private string _WKT;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string WKT
        {
            get { return _WKT; }
            set { SetPropertyValue<string>(nameof(WKT), ref _WKT, value); }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }




     

        private string _Name;
        [Size(SizeAttribute.Unlimited)]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private Project _Project;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }



        private string _Description;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue<string>(nameof(Description), ref _Description, value); }
        }


    
        [Association("Store-Units")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }
        public Store(Session session) : base(session) { }

        string address;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                SetPropertyValue("Address", ref address, value);
            }
        }

        [Persistent]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public String DisplayName
        {
            get
            {
                string n = string.Empty;
                if ((Name != null) && (Parent != null))
                {
                    Store cur_storage = this;
                    while (cur_storage.Parent != null)
                    {
                        cur_storage = cur_storage.Parent;
                        n = String.Format("{0}/{1}", cur_storage.Name, n);
                    }
                    return n + Name;
                }
                if ((Name != null) && (Parent == null))
                {
                    return Name;
                }
                return string.Empty;
            }
        }

        private Store _Parent;
        [Association("Object-Children")]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public Store Parent
        {
            get { return _Parent; }
            set { SetPropertyValue<Store>(nameof(Parent), ref _Parent, value); }
        }


       
        [Association("Object-Children")]
        [VisibleInListView(false), VisibleInDetailView(false)]
        public XPCollection<Store> Children
        {
            get { return GetCollection<Store>("Children"); }
        }
        #region ITreeNode Members

        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get { return Children; }
        }

        string ITreeNode.Name
        {
            get { return Name; }
        }

        ITreeNode ITreeNode.Parent
        {
            get { return Parent; }
        }

        #endregion

        private string _Content;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Content
        {
            get { return _Content; }
            set { SetPropertyValue<string>(nameof(Content), ref _Content, value); }
        }


        private string _Origin;
        [Size(500)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Origin
        {
            get { return _Origin; }
            set { SetPropertyValue<string>(nameof(Origin), ref _Origin, value); }
        }



        private string _Reference;
        [Size(255)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Reference
        {
            get { return _Reference; }
            set { SetPropertyValue<string>(nameof(Reference), ref _Reference, value); }
        }



        private string _Conditions;
        [Size(255)]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Conditions
        {
            get { return _Conditions; }
            set { SetPropertyValue<string>(nameof(Conditions), ref _Conditions, value); }
        }



        private string _Date;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string Date
        {
            get { return _Date; }
            set { SetPropertyValue<string>(nameof(Date), ref _Date, value); }
        }


        [Association("DNAExtractStore")]
        public XPCollection<DnaExtract> DnaExtracts
        {
            get
            {
                return GetCollection<DnaExtract>(nameof(DnaExtracts));
            }
        }
        [Association("StorePcrProducts")]
        public XPCollection<PcrDnaExtract> PcrProducts
        {
            get
            {
                return GetCollection<PcrDnaExtract>(nameof(PcrProducts));
            }
        }
    }

}
