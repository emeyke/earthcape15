using DevExpress.Persistent.BaseImpl;

using DevExpress.Xpo;
using System;




namespace EarthCape.Module.Core
{
    public class UnitDuplicate : BaseObject
    {

        public UnitDuplicate(Session session) : base(session) { }


        private Unit _Unit;
        [Association("Unit-Duplicates")]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }


        private Unit _Duplicate;
        public Unit Duplicate
        {
            get { return _Duplicate; }
            set { SetPropertyValue<Unit>(nameof(Duplicate), ref _Duplicate, value); }
        }


        private string _UnitID;
        public string UnitID
        {
            get { return _UnitID; }
            set { SetPropertyValue<string>(nameof(UnitID), ref _UnitID, value); }
        }


        private Organization _Organization;
        public Organization Organization
        {
            get { return _Organization; }
            set { SetPropertyValue<Organization>(nameof(Organization), ref _Organization, value); }
        }



        private int _Quantity;
        public int Quantity
        {
            get { return _Quantity; }
            set { SetPropertyValue<int>(nameof(Quantity), ref _Quantity, value); }
        }

        private bool _BulkPresent;
        public bool BulkPresent
        {
            get { return _BulkPresent; }
            set { SetPropertyValue<bool>(nameof(BulkPresent), ref _BulkPresent, value); }
        }



        [Association("UnitIdentification-DuplicatesSeen")]
        public XPCollection<UnitIdentification> SeenInIdentifications
        {
            get
            {
                return GetCollection<UnitIdentification>("SeenInIdentifications");
            }
        }
    }



}
