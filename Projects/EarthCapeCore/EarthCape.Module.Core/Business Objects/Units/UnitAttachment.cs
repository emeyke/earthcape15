﻿
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Lab;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Xpand.Persistent.Base.General;

namespace EarthCape.Module.Core
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class UnitAttachment : BaseObject, IName//, IPictureItem
    {

        private string _LocalPath;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string LocalPath
        {
            get { return _LocalPath; }
            set { SetPropertyValue<string>(nameof(LocalPath), ref _LocalPath, value); }
        }

        private int? _ZenodoId;
        public int? ZenodoId
        {
            get { return _ZenodoId; }
            set { SetPropertyValue<int?>(nameof(ZenodoId), ref _ZenodoId, value); }
        }

        private string _ZenodoUrl;
        [RuleRegularExpression("UnitAttachamentZenodoUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        public string ZenodoUrl
        {
            get { return _ZenodoUrl; }
            set { SetPropertyValue<string>(nameof(ZenodoUrl), ref _ZenodoUrl, value); }
        }





        /*  private FileDataEx _FileStored;
          [ImmediatePostData(true)]
          public FileDataEx FileStored
          {
              get { return _FileStored; }
              set { SetPropertyValue<FileDataEx>(nameof(FileStored), ref _FileStored, value);
                  if (_FileStored != null)
                  {
                      filedata = _FileStored;
                      if (String.IsNullOrEmpty(Name))
                          Name = Path.GetFileName(filedata.FileName);
                  }
              }
          }
          */

        private string _OCR;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string OCR
        {
            get { return _OCR; }
            set { SetPropertyValue<string>(nameof(OCR), ref _OCR, value); }
        }



        private string _BoldTraceFileId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string BoldTraceFileId
        {
            get { return _BoldTraceFileId; }
            set { SetPropertyValue<string>(nameof(BoldTraceFileId), ref _BoldTraceFileId, value); }
        }


        private int? _INatId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public int? INatId
        {
            get { return _INatId; }
            set { SetPropertyValue<int?>(nameof(INatId), ref _INatId, value); }
        }


        private string _Name;
        [Size(SizeAttribute.Unlimited)]
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        [ModelDefault("RowCount", "1")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

  
        private string _ID;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ID
        {
            get { return _ID; }
            set { SetPropertyValue<string>(nameof(ID), ref _ID, value); }
        }

        private ulong _BoldMediaId;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public ulong BoldMediaId
        {
            get { return _BoldMediaId; }
            set { SetPropertyValue<ulong>(nameof(BoldMediaId), ref _BoldMediaId, value); }
        }



        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public Image Image
        {
            get { return Thumbnail; }
        }
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public string ImagePath
        {
            get { return JpegUrl; }
        }

        private MediaDataObject _EmbeddedImage;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public MediaDataObject EmbeddedImage
        {
            get { return _EmbeddedImage; }
            set { SetPropertyValue<MediaDataObject>(nameof(EmbeddedImage), ref _EmbeddedImage, value); }
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }


        private string _FileUrl;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("UnitAttachment.FileUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [ModelDefault("RowCount", "1")]
        public string FileUrl
        {
            get { return _FileUrl; }
            set { SetPropertyValue<string>(nameof(FileUrl), ref _FileUrl, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
                Settings settings = Session.FindObject<Settings>(CriteriaOperator.Parse("1=1"));
                if (settings != null)
                    if (settings.DefaultMediaLicense != null)
                        this.License = Session.FindObject<License>(new BinaryOperator("Oid", settings.DefaultMediaLicense.Oid));
            }
        }


        private DcmiType? _DcmiType = Core.DcmiType.StillImage;
        public DcmiType? DcmiType
        {
            get { return _DcmiType; }
            set { SetPropertyValue<DcmiType?>(nameof(DcmiType), ref _DcmiType, value); }
        }



        private License _License;
        public License License
        {
            get { return _License; }
            set { SetPropertyValue<License>(nameof(License), ref _License, value); }
        }







    //    Stream stream;
      //  FileDataEx filedata;
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            if (!string.IsNullOrEmpty(JpegUrl))
            {
                var im = AttachmentHelper.GetBitmapFromUrl(JpegUrl);
                if (im != null)
                {
                    Thumbnail = AttachmentHelper.CreateThumbnailFromBitmap(im, 200, 200);
                    //  Metadata = AttachmentHelper.GetLatitude(im).ToString() + AttachmentHelper.GetLongitude(im).ToString();
                }
            }
            /*   else
                    if (filedata != null)
               {
                   using (stream = new MemoryStream())
                   {
                       if (String.IsNullOrEmpty(Name))
                       {
                           Name = filedata.FileName;
                           //   OriginalFilePath = filedata.FilePath;
                       }
                       filedata.SaveToStream(stream);
                           if (GeneralHelper.IsRecognisedImageFile(Name))
                           {
                                Thumbnail = AttachmentHelper.CreateThumbnail(stream, 100, 100);// ResizeImage(Image.FromFile(filename), 100);
                             }


                   }
               }*/
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public UnitAttachment(Session session) : base(session) { }



        private Unit _Unit;
        [Association("Unit-Attachments")]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }

        private DnaExtract _DnaExtract;
        [Association("DnaExtract-Attachments")]
        public DnaExtract DnaExtract
        {
            get { return _DnaExtract; }
            set { SetPropertyValue<DnaExtract>(nameof(DnaExtract), ref _DnaExtract, value); }
        }


        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime>(nameof(Date), ref _Date, value); }
        }



        private string _Comment;
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }





        private string _Metadata;
        [Size(SizeAttribute.Unlimited)]
        public string Metadata
        {
            get { return _Metadata; }
            set { SetPropertyValue<string>(nameof(Metadata), ref _Metadata, value); }
        }



        private string _JpegUrl;
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("UnitAttachment.JpegUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [ModelDefault("RowCount", "1")]
        public string JpegUrl
        {
            get { return _JpegUrl; }
            set { SetPropertyValue<string>(nameof(JpegUrl), ref _JpegUrl, value); }
        }
        private string _TiffUrl;
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("UnitAttachment.TiffUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [ModelDefault("RowCount", "1")]
        public string TiffUrl
        {
            get { return _TiffUrl; }
            set { SetPropertyValue<string>(nameof(TiffUrl), ref _TiffUrl, value); }
        }
        private string _RawUrl;
        [Size(SizeAttribute.Unlimited)]
        [RuleRegularExpression("UnitAttachment.RawUrl.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})")]
        [EditorAlias("HyperLinkPropertyEditor")]
        [ModelDefault("RowCount", "1")]
        public string RawUrl
        {
            get { return _RawUrl; }
            set { SetPropertyValue<string>(nameof(RawUrl), ref _RawUrl, value); }
        }



        private Image _Thumbnail;
        [DevExpress.Xpo.ValueConverterAttribute(typeof(ImageValueConverter))]
        [ImageEditor(ImageSizeMode = ImageSizeMode.Zoom,
           DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
           DetailViewImageEditorFixedHeight = 100,
           ListViewImageEditorCustomHeight = 100)]
        public Image Thumbnail
        {
            get { return _Thumbnail; }
            set { SetPropertyValue<Image>(nameof(Thumbnail), ref _Thumbnail, value); }
        }
    }

}
