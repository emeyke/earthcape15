using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Xpo;
using System;


namespace EarthCape.Module.Core
{
    public class UnitIdentification : BaseObject, IProjectObject
    {

        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


        private string _DateVerbatim;
        public string DateVerbatim
        {
            get { return _DateVerbatim; }
            set { SetPropertyValue<string>(nameof(DateVerbatim), ref _DateVerbatim, value); }
        }


        private string _IdentificationRemarks;
        [Size(SizeAttribute.Unlimited)]
        public string IdentificationRemarks
        {
            get { return _IdentificationRemarks; }
            set { SetPropertyValue<string>(nameof(IdentificationRemarks), ref _IdentificationRemarks, value); }
        }


        public UnitIdentification(Session session) : base(session) { }

        private string _IdentifiedBy;
        [Size(255)]
        public string IdentifiedBy
        {
            get { return _IdentifiedBy; }
            set { SetPropertyValue<string>(nameof(IdentifiedBy), ref _IdentifiedBy, value); }
        }



        private Unit _Unit;
        [Association("Unit-Identifications")]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }


        private TaxonomicName _TaxonomicName;
        public TaxonomicName TaxonomicName
        {
            get { return _TaxonomicName; }
            set { SetPropertyValue<TaxonomicName>(nameof(TaxonomicName), ref _TaxonomicName, value); }
        }



        private string _NameText;
        [ModelDefault("RowCount","1")]
        [Size(SizeAttribute.Unlimited)]
        public string NameText
        {
            get { return _NameText; }
            set { SetPropertyValue<string>(nameof(NameText), ref _NameText, value); }
        }


        private DateTime _DateStart = DateTime.Today;
     public DateTime DateStart
        {
            get { return _DateStart; }
            set { SetPropertyValue<DateTime>(nameof(DateStart), ref _DateStart, value); }
        }


        private DateTime _DateEnd;
        public DateTime DateEnd
        {
            get { return _DateEnd; }
            set { SetPropertyValue<DateTime>(nameof(DateEnd), ref _DateEnd, value); }
        }

        private PersonCore _Identifier;
        public PersonCore Identifier
        {
            get { return _Identifier; }
            set { SetPropertyValue<PersonCore>(nameof(Identifier), ref _Identifier, value); }
        }


        [Association("UnitIdentification-DuplicatesSeen")]
        public XPCollection<UnitDuplicate> DuplicatesSeen
        {
            get
            {
                return GetCollection<UnitDuplicate>("DuplicatesSeen");
            }
        }

    }


}
