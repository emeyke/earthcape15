using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    [ExcelImportKeyAttribute("Name")]
    public class Genotype : BaseObject, IUnits, IName
    {
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        [Association("Genotype-Units")]
        public XPCollection<Unit> Units
        {
            get
            {
                return GetCollection<Unit>("Units");
            }
        }
        public Genotype(Session session) : base(session) { }

        private string _Name;
        [Size(255)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }
    }

}
