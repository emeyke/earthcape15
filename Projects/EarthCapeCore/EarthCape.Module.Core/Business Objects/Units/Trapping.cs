using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Linq;
using System.Text;

namespace EarthCape.Module.Core
{
    //[DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class Trapping : Unit
    {
        // Herptiles-kanavki, stakanchiki Barbera, Melk. Mlek
        public Trapping(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (http://documentation.devexpress.com/#Xaf/CustomDocument2619).
        //    this.PersistentProperty = "Paid";
        //}
        // Fields...
        private string _Cycle;
        private int _NightCount;
        private string _Topography;
        private int _TotalTrapNights;
        private string _Season;
        private double _AbundancePer100TrapNights;

        public double AbundancePer100TrapNights
        {
            get
            {
                return _AbundancePer100TrapNights;
            }
            set
            {
                SetPropertyValue("AbundancePer100TrapNights", ref _AbundancePer100TrapNights, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Season
        {
            get
            {
                return _Season;
            }
            set
            {
                SetPropertyValue("Season", ref _Season, value);
            }
        }

        public int TotalTrapNights
        {
            get
            {
                return _TotalTrapNights;
            }
            set
            {
                SetPropertyValue("TotalTrapNights", ref _TotalTrapNights, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Topography
        {
            get
            {
                return _Topography;
            }
            set
            {
                SetPropertyValue("Topography", ref _Topography, value);
            }
        }

        public int NightCount
        {
            get
            {
                return _NightCount;
            }
            set
            {
                SetPropertyValue("NightCount", ref _NightCount, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Cycle
        {
            get
            {
                return _Cycle;
            }
            set
            {
                SetPropertyValue("Cycle", ref _Cycle, value);
            }
        }



    }
}
