#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.

using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using System.Drawing;
using Xpand.Persistent.Base.General;
using DevExpress.ExpressApp.ConditionalAppearance;
using Xpand.Persistent.Base;
using System.Collections.Generic;
namespace EarthCape.Module.Core
{
    public class EarthCapeSequenceObject : XPCustomObject, ISequenceObject
    {
        private string typeName;
        private long nextSequence;
        public EarthCapeSequenceObject(Session session)
            : base(session)
        {
        }
        [Key]
        [Size(1024)]
        public string TypeName
        {
            get { return typeName; }
            set { SetPropertyValue("TypeName", ref typeName, value); }
        }
        public long NextSequence
        {
            get { return nextSequence; }
            set { SetPropertyValue("NextSequence", ref nextSequence, value); }
        }

        IList<ISequenceReleasedObject> ISequenceObject.SequenceReleasedObjects
        {
            get { return new ListConverter<ISequenceReleasedObject, EarthCapeSequenceReleasedObject>(XpandReleasedSequences); }
        }

        [Association("EarthCapeSequenceObject-XpandReleasedSequences")]
        [Aggregated]
        public XPCollection<EarthCapeSequenceReleasedObject> XpandReleasedSequences
        {
            get
            {
                return GetCollection<EarthCapeSequenceReleasedObject>("XpandReleasedSequences");
            }
        }
    }
}