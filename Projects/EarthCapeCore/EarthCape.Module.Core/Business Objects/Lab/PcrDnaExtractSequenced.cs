using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    public class PcrDnaExtractSequenced : BaseObject, IName
    {
        public PcrDnaExtractSequenced(Session session)
             : base(session)
        {
        }
        ///    PlatePosition platePositionForward;
        ///    PlatePosition platePositionReverse;

        private string _Name;
        [VisibleInListView(false), VisibleInDetailView(false)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        private string _PlatePosF;
        public string PlatePosF
        {
            get { return _PlatePosF; }
            set { SetPropertyValue<string>(nameof(PlatePosF), ref _PlatePosF, value); }
        }
        private string _PlatePosR;
        public string PlatePosR
        {
            get { return _PlatePosR; }
            set { SetPropertyValue<string>(nameof(PlatePosR), ref _PlatePosR, value); }
        }



        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(500)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }



        private int _DnaLength;
        public int DnaLength
        {
            get { return _DnaLength; }
            set { SetPropertyValue<int>(nameof(DnaLength), ref _DnaLength, value); }
        }


        private string _CustomerNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerNumber
        {
            get { return _CustomerNumber; }
            set { SetPropertyValue<string>(nameof(CustomerNumber), ref _CustomerNumber, value); }
        }



        private PcrDnaExtract _PcrDnaExtract;
        [Association("PcrDnaExtract-PcrDnaExtractsSequenced")]
        [RuleRequiredField("RuleRequiredField for PcrDnaExtractSequencing.PcrDnaExtract", DefaultContexts.Save, "DNA extract amplified (PCRDnaExtract) property must not be empty.")]
        public PcrDnaExtract PcrDnaExtract
        {
            get { return _PcrDnaExtract; }
            set { SetPropertyValue<PcrDnaExtract>(nameof(PcrDnaExtract), ref _PcrDnaExtract, value); }
        }




        private bool _SequenceChecked;
        public bool SequenceChecked
        {
            get { return _SequenceChecked; }
            set { SetPropertyValue<bool>(nameof(SequenceChecked), ref _SequenceChecked, value); }
        }



        private SequencingResult? _ResultForward;
        public SequencingResult? ResultForward
        {
            get { return _ResultForward; }
            set { SetPropertyValue<SequencingResult?>(nameof(ResultForward), ref _ResultForward, value); }
        }


        private SequencingResult? _ResultReverse;
        public SequencingResult? ResultReverse
        {
            get { return _ResultReverse; }
            set { SetPropertyValue<SequencingResult?>(nameof(ResultReverse), ref _ResultReverse, value); }
        }


        private SequencingResult? _Result;
        public SequencingResult? Result
        {
            get { return _Result; }
            set { SetPropertyValue<SequencingResult?>(nameof(Result), ref _Result, value); }
        }


        private SequencingRun _SequencingRun;
        //       [ImmediatePostData(true)]
        [Association("SequencingRun-SequencingRunDnaExtrs")]
        //   [RuleRequiredField("RuleRequiredField for SequencingRunDnaExtract.SequencingRun", DefaultContexts.Save, "SequencingRun property must not be empty.")]
        public SequencingRun SequencingRun
        {
            get { return _SequencingRun; }
            set { SetPropertyValue<SequencingRun>(nameof(SequencingRun), ref _SequencingRun, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private string _Genbank;
        public string Genbank
        {
            get { return _Genbank; }
            set { SetPropertyValue<string>(nameof(Genbank), ref _Genbank, value); }
        }


        private string _ResultQuality;
        public string ResultQuality
        {
            get { return _ResultQuality; }
            set { SetPropertyValue<string>(nameof(ResultQuality), ref _ResultQuality, value); }
        }



        protected override void OnDeleting()
        {
            // Session.Delete(this.PlatePositionForward);
            // Session.Delete(this.PlatePositionReverse);
            base.OnDeleting();
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (IsDeleted) return;
            /*   if (PlatePositionForward == null)
               {
                   PlatePositionForward = new PlatePosition(Session);
                //   PlatePositionForward.SequencingRun = _SequencingRun;
                   PlatePositionForward.Save();
               }
               if (PlatePositionReverse == null)
               {
                   PlatePositionReverse = new PlatePosition(Session);
                 //  PlatePositionReverse.SequencingRun = _SequencingRun;
                   PlatePositionReverse.Save();
               }*/
            /* if (Position < 1)
              {
                  //Pcr.DnaExtrs.Load();
                  Position = Pcr.DnaExtrs.Count+1;
              }*/
            /*   if ((PcrDnaExtract != null) && (PlatePositionForward != null) && (PlatePositionReverse != null))
                   FullName = String.Format("{0}: {1},F:{2}{3}, R:{4}{5}", PcrDnaExtract.DnaExtract.Code, Result, PlatePositionForward.Row, PlatePositionForward.Col, PlatePositionReverse.Row, PlatePositionReverse.Col);
               else*/
            if ((PcrDnaExtract != null))
                FullName = String.Format("{0}: {1}", PcrDnaExtract.DnaExtract.Name, Result);
            else
                FullName = "";
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }
        private string _Sequence;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string Sequence
        {
            get { return _Sequence; }
            set { SetPropertyValue<string>(nameof(Sequence), ref _Sequence, value); }
        }


    }


}
