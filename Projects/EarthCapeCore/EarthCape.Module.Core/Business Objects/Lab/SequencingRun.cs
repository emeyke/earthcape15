using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.General;
using System.Diagnostics.CodeAnalysis;
using Xpand.ExpressApp.ExcelImporter.Services;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [ExcelImportKeyAttribute("Name")]
    public class SequencingRun : BaseObject, IName, ILabBookPage, ITask
    {
        private string _LabBook;
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("RTF")]
        public string LabBook
        {
            get { return _LabBook; }
            set { SetPropertyValue<string>(nameof(LabBook), ref _LabBook, value); }
        }



        private string _LabBookXLS;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("XLS")]
        public string LabBookXLS
        {
            get { return _LabBookXLS; }
            set { SetPropertyValue<string>(nameof(LabBookXLS), ref _LabBookXLS, value); }
        }

        private string _Plate;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Plate
        {
            get { return _Plate; }
            set { SetPropertyValue<string>(nameof(Plate), ref _Plate, value); }
        }




        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }



        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }
        public SequencingRun(Session session)
            : base(session)
        {
            Name = String.Format("Sequencing-{0}{1:yyMMdd}", SecuritySystem.CurrentUserName ?? "", DateTime.Now);
        }


        private string _Name;
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }



        private PersonCore _Owner;
        public PersonCore Owner
        {
            get { return _Owner; }
            set { SetPropertyValue<PersonCore>(nameof(Owner), ref _Owner, value); }
        }


        private string _CustomerNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerNumber
        {
            get { return _CustomerNumber; }
            set { SetPropertyValue<string>(nameof(CustomerNumber), ref _CustomerNumber, value); }
        }


        private string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { SetPropertyValue<string>(nameof(ServiceType), ref _ServiceType, value); }
        }


        private string _SequencingChemistry;
        public string SequencingChemistry
        {
            get { return _SequencingChemistry; }
            set { SetPropertyValue<string>(nameof(SequencingChemistry), ref _SequencingChemistry, value); }
        }



        [Association("SequencingRun-SequencingRunDnaExtrs"), Aggregated]
        public XPCollection<PcrDnaExtractSequenced> PcrDnaExtractsSequenced
        {
            get
            {
                return GetCollection<PcrDnaExtractSequenced>("PcrDnaExtractsSequenced");
            }
        }
        private TaskImpl task = new TaskImpl();
        private Party assignedTo;
#if MediumTrust
		[Persistent("DateCompleted"), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public DateTime dateCompleted {
			get { return task.DateCompleted; }
			set {
				DateTime oldValue = task.DateCompleted;
				task.DateCompleted = value;
				OnChanged("dateCompleted", oldValue, task.DateCompleted);
			}
		}
#else
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [Persistent("DateCompleted")]
        private DateTime dateCompleted
        {
            get { return task.DateCompleted; }
            set
            {
                DateTime oldValue = task.DateCompleted;
                task.DateCompleted = value;
                OnChanged("dateCompleted", oldValue, task.DateCompleted);
            }
        }
#endif
        protected override void OnLoading()
        {
            task.IsLoading = true;
            base.OnLoading();
        }
        protected override void OnLoaded()
        {
            base.OnLoaded();
            task.DateCompleted = dateCompleted;
            task.IsLoading = false;
        }
        [Action(ImageName = "State_Task_Completed")]
        public void MarkCompleted()
        {
            TaskStatus oldStatus = task.Status;
            task.MarkCompleted();
            OnChanged("Status", oldStatus, task.Status);
            OnChanged("PercentCompleted");
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ToolTip("Service field. Use Name field instead.")]
        public string Subject
        {
            get { return task.Subject; }
            set
            {
                string oldValue = task.Subject;
                task.Subject = value;
                OnChanged("Subject", oldValue, task.Subject);
            }
        }
        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ToolTip("Service field. Use Comment field instead.")]
        public string Description
        {
            get { return task.Description; }
            set
            {
                string oldValue = task.Description;
                task.Description = value;
                OnChanged("Description", oldValue, task.Description);
            }
        }
        public DateTime DueDate
        {
            get { return task.DueDate; }
            set
            {
                DateTime oldValue = task.DueDate;
                task.DueDate = value;
                OnChanged("DueDate", oldValue, task.DueDate);
            }
        }
        public DateTime StartDate
        {
            get { return task.StartDate; }
            set
            {
                DateTime oldValue = task.StartDate;
                task.StartDate = value;
                OnChanged("StartDate", oldValue, task.StartDate);
            }
        }
        public Party AssignedTo
        {
            get { return assignedTo; }
            set { SetPropertyValue("AssignedTo", ref assignedTo, value); }
        }
        public TaskStatus Status
        {
            get { return task.Status; }
            set
            {
                TaskStatus oldValue = task.Status;
                task.Status = value;
                OnChanged("Status", oldValue, task.Status);
            }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Int32 PercentCompleted
        {
            get { return task.PercentCompleted; }
            set
            {
                Int32 oldValue = task.PercentCompleted;
                task.PercentCompleted = value;
                OnChanged("PercentCompleted", oldValue, task.PercentCompleted);
            }
        }

        public DateTime DateCompleted
        {
            get { return dateCompleted; }
        }

    }
}
