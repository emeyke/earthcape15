using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;

namespace EarthCape.Module.Lab
{
  //  [DefaultClassOptions]
    [VisibleInReports(true)]
    public class MSdata : BaseObject,IDatasetObject
    {
        public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        } 

        public MSdata(Session session) : base(session) { }
        private Unit _Unit;
        [ProvidedAssociation("MSdata-Unit", "MsData", RelationType.OneToMany, "Aggregated")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Unit Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetPropertyValue("Unit", ref _Unit, value);
            }
        }
        private Microsatellite _MS;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Microsatellite MS
        {
            get
            {
                return _MS;
            }
            set
            {
                SetPropertyValue("MS", ref _MS, value);
            }
        }

        private string _MSName;
        public string MSName
        {
            get
            {
                return _MSName;
            }
            set
            {
                SetPropertyValue("MSName", ref _MSName, value);
            }
        }
        private string _UnitID;
        public string UnitID
        {
            get
            {
                return _UnitID;
            }
            set
            {
                SetPropertyValue("UnitID", ref _UnitID, value);
            }
        }
        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }

        private Int32 _Repeats1;
        public Int32 Repeats1
        {
            get
            {
                return _Repeats1;
            }
            set
            {
                SetPropertyValue("Repeats1", ref _Repeats1, value);
            }
        }
        private Int32 _Repeats2;
        public Int32 Repeats2
        {
            get
            {
                return _Repeats2;
            }
            set
            {
                SetPropertyValue("Repeats2", ref _Repeats2, value);
            }
        }
    
    }
 
}
