using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;
using System.ComponentModel;


namespace EarthCape.Module.Lab
{
    //[DefaultClassOptions]
    [DefaultProperty("Code")]
    public class UnitGenotyping : BaseObject, ICode
    {

        protected override void OnSaving()
        {
            base.OnSaving();
            if (Session.IsNewObject(this))
            {
                if (String.IsNullOrEmpty(Code))
                {
                    Code = string.Format("{0}{1}{2}", (Unit != null) ? Unit.UnitID : "", Plate, Well);
                }
            }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }
        public UnitGenotyping(Session session) : base(session) { }

        private Genotyping _Genotyping;
        [Association("Genotyping-GenotypedUnits")]
        public Genotyping Genotyping
        {
            get { return _Genotyping; }
            set { SetPropertyValue<Genotyping>(nameof(Genotyping), ref _Genotyping, value); }
        }


        private string _Code;
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }


        private Unit _Unit;
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }



        private string _Genotyper;
        public string Genotyper
        {
            get { return _Genotyper; }
            set { SetPropertyValue<string>(nameof(Genotyper), ref _Genotyper, value); }
        }


        private string _Plate;
        public string Plate
        {
            get { return _Plate; }
            set { SetPropertyValue<string>(nameof(Plate), ref _Plate, value); }
        }



        private string _Well;
        public string Well
        {
            get { return _Well; }
            set { SetPropertyValue<string>(nameof(Well), ref _Well, value); }
        }



        [Association("UnitGenotyping-Values"), Aggregated]
        public XPCollection<UnitGenotypingValue> Values
        {
            get
            {
                return GetCollection<UnitGenotypingValue>("Values");
            }
        }
    }


}
