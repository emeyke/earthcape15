using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;
using System.Drawing;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("Title")]
    public class ProtocolPCR : BaseObject
    {
        public ProtocolPCR(Session session)
            : base(session) 
        {
        }

        private string _Title;
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetPropertyValue("Title", ref _Title, value);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _Title = "Amplification template by " + SecuritySystem.CurrentUserName + "";
            
        }
      /*  private double _H2o;
        public double H2o
        {
            get
            {
                return _H2o;
            }
            set
            {
                SetPropertyValue("H2o", ref _H2o, value);
            }
        }
        private double _Buffer;
        public double Buffer
        {
            get
            {
                return _Buffer;
            }
            set
            {
                SetPropertyValue("Buffer", ref _Buffer, value);
            }
        }
        private double _Dntps;
        public double Dntps
        {
            get
            {
                return _Dntps;
            }
            set
            {
                SetPropertyValue("Dntps", ref _Dntps, value);
            }
        }
        private double _Taq;
        public double Taq
        {
            get
            {
                return _Taq;
            }
            set
            {
                SetPropertyValue("Taq", ref _Taq, value);
            }
        }
 
        private double _Dna;
        public double Dna
        {
            get
            {
                return _Dna;
            }
            set
            {
                SetPropertyValue("Dna", ref _Dna, value);
            }
        }
        private int _PcrInitialization;
        public int PcrInitialization
        {
            get
            {
                return _PcrInitialization;
            }
            set
            {
                SetPropertyValue("PcrInitialization", ref _PcrInitialization, value);
            }
        }
        private int _PcrDenaturation;
        public int PcrDenaturation
        {
            get
            {
                return _PcrDenaturation;
            }
            set
            {
                SetPropertyValue("PcrDenaturation", ref _PcrDenaturation, value);
            }
        }
        private int _PcrAnnealing;
        public int PcrAnnealing
        {
            get
            {
                return _PcrAnnealing;
            }
            set
            {
                SetPropertyValue("PcrAnnealing", ref _PcrAnnealing, value);
            }
        }
        private int _PcrElongation;
        public int PcrElongation
        {
            get
            {
                return _PcrElongation;
            }
            set
            {
                SetPropertyValue("PcrElongation", ref _PcrElongation, value);
            }
        }
        private int _PcrFinalElongation;
        public int PcrFinalElongation
        {
            get
            {
                return _PcrFinalElongation;
            }
            set
            {
                SetPropertyValue("PcrFinalElongation", ref _PcrFinalElongation, value);
            }
        }
        private int _PcrFinalHold;
        public int PcrFinalHold
        {
            get
            {
                return _PcrFinalHold;
            }
            set
            {
                SetPropertyValue("PcrFinalHold", ref _PcrFinalHold, value);
            }
        }
        private int _PcrTempInitialization;
        public int PcrTempInitialization
        {
            get
            {
                return _PcrTempInitialization;
            }
            set
            {
                SetPropertyValue("PcrTempInitialization", ref _PcrTempInitialization, value);
            }
        }
        private int _PcrTempDenaturation;
        public int PcrTempDenaturation
        {
            get
            {
                return _PcrTempDenaturation;
            }
            set
            {
                SetPropertyValue("PcrTempDenaturation", ref _PcrTempDenaturation, value);
            }
        }
        private int _PcrTempAnnealing;
        public int PcrTempAnnealing
        {
            get
            {
                return _PcrTempAnnealing;
            }
            set
            {
                SetPropertyValue("PcrTempAnnealing", ref _PcrTempAnnealing, value);
            }
        }
        private int _PcrTempElongation;
        public int PcrTempElongation
        {
            get
            {
                return _PcrTempElongation;
            }
            set
            {
                SetPropertyValue("PcrTempElongation", ref _PcrTempElongation, value);
            }
        }
        private int _PcrTempFinalElongation;
        public int PcrTempFinalElongation
        {
            get
            {
                return _PcrTempFinalElongation;
            }
            set
            {
                SetPropertyValue("PcrTempFinalElongation", ref _PcrTempFinalElongation, value);
            }
        }
        private int _PcrTempFinalHold;
        public int PcrTempFinalHold
        {
            get
            {
                return _PcrTempFinalHold;
            }
            set
            {
                SetPropertyValue("PcrTempFinalHold", ref _PcrTempFinalHold, value);
            }
        }
        private int _PcrCycles;
        public int PcrCycles
        {
            get
            {
                return _PcrCycles;
            }
            set
            {
                SetPropertyValue("PcrCycles", ref _PcrCycles, value);
            }
        }
      
    
    }
  

}
