using System;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using Xpand.ExpressApp.ExcelImporter.Services;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class DnaExtract : BaseObject, IUnitData,IName
    {
        [Association("DnaExtractSequences")]
        public XPCollection<Sequence> Sequences
        {
            get { return GetCollection<Sequence>(nameof(Sequences)); }
        }

        [Association("DnaExtract-Attachments"), Aggregated]
        public XPCollection<UnitAttachment> Attachments
        {
            get
            {
                return GetCollection<UnitAttachment>("Attachments");
            }
        }

        private string _BoldRecordId;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public string BoldRecordId
        {
            get { return _BoldRecordId; }
            set { SetPropertyValue<string>(nameof(BoldRecordId), ref _BoldRecordId, value); }
        }


        private string _BoldBin;
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        // [ModelDefault("Caption", "BIN")]
        public string BoldBin
        {
            get { return _BoldBin; }
            set { SetPropertyValue<string>(nameof(BoldBin), ref _BoldBin, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        [Association("DnaExtract-NGS")]
        public XPCollection<Sequencing> NGS
        {
            get
            {
                return GetCollection<Sequencing>(nameof(NGS));
            }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            FullName = Name;

            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }



        public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        }
        /*   [VisibleInLookupListView(false), VisibleInListView(false), VisibleInDetailView(false)]
           [Persistent]
           public string Name
           { get { return Code; } }*/



        private string _Name;
        [Persistent("Code")]
        [NonCloneable]
        // [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save, SkipNullOrEmptyValues = true)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


   
        public DnaExtract(Session session) : base(session) { }


        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(4000)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }


        private double _Volume;
        public double Volume
        {
            get { return _Volume; }
            set { SetPropertyValue<double>(nameof(Volume), ref _Volume, value); }
        }


        private double _TotalQuantity;
        public double TotalQuantity
        {
            get { return _TotalQuantity; }
            set { SetPropertyValue<double>(nameof(TotalQuantity), ref _TotalQuantity, value); }
        }


        private string _Preservation;
        public string Preservation
        {
            get { return _Preservation; }
            set { SetPropertyValue<string>(nameof(Preservation), ref _Preservation, value); }
        }



        private string _DoneBy;
        public string DoneBy
        {
            get { return _DoneBy; }
            set { SetPropertyValue<string>(nameof(DoneBy), ref _DoneBy, value); }
        }

        private string _ExtractionMethod;
        public string ExtractionMethod
        {
            get { return _ExtractionMethod; }
            set { SetPropertyValue<string>(nameof(ExtractionMethod), ref _ExtractionMethod, value); }
        }


        private string _ExtractionBuffer;
        public string ExtractionBuffer
        {
            get { return _ExtractionBuffer; }
            set { SetPropertyValue<string>(nameof(ExtractionBuffer), ref _ExtractionBuffer, value); }
        }


        private string _PurificationMethod;
        public string PurificationMethod
        {
            get { return _PurificationMethod; }
            set { SetPropertyValue<string>(nameof(PurificationMethod), ref _PurificationMethod, value); }
        }


        private string _QuantificationEstimateMethod;
        public string QuantificationEstimateMethod
        {
            get { return _QuantificationEstimateMethod; }
            set { SetPropertyValue<string>(nameof(QuantificationEstimateMethod), ref _QuantificationEstimateMethod, value); }
        }




        private string _RatioOfAbsorbance260_280;
        public string RatioOfAbsorbance260_280
        {
            get { return _RatioOfAbsorbance260_280; }
            set { SetPropertyValue<string>(nameof(RatioOfAbsorbance260_280), ref _RatioOfAbsorbance260_280, value); }
        }


        private string _RatioOfAbsorbance260_230;
        public string RatioOfAbsorbance260_230
        {
            get { return _RatioOfAbsorbance260_230; }
            set { SetPropertyValue<string>(nameof(RatioOfAbsorbance260_230), ref _RatioOfAbsorbance260_230, value); }
        }

        private string _Concentration;
        public string Concentration
        {
            get { return _Concentration; }
            set { SetPropertyValue<string>(nameof(Concentration), ref _Concentration, value); }
        }

        private double _ConcentrationVal;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        public double ConcentrationVal
        {
            get { return _ConcentrationVal; }
            set { SetPropertyValue<double>(nameof(ConcentrationVal), ref _ConcentrationVal, value); }
        }


        private string _Quality;
        public string Quality
        {
            get { return _Quality; }
            set { SetPropertyValue<string>(nameof(Quality), ref _Quality, value); }
        }

        private DateTime _QualityCheckDate;
        public DateTime QualityCheckDate
        {
            get { return _QualityCheckDate; }
            set { SetPropertyValue<DateTime>(nameof(QualityCheckDate), ref _QualityCheckDate, value); }
        }



        private string _ProvidedBy;
        public string ProvidedBy
        {
            get { return _ProvidedBy; }
            set { SetPropertyValue<string>(nameof(ProvidedBy), ref _ProvidedBy, value); }
        }


        private DateTime? _ExtractionDate;
        public DateTime? ExtractionDate
        {
            get { return _ExtractionDate; }
            set { SetPropertyValue<DateTime?>(nameof(ExtractionDate), ref _ExtractionDate, value); }
        }


        private double _DnaAmountInitialMl;
        public double DnaAmountInitialMl
        {
            get { return _DnaAmountInitialMl; }
            set { SetPropertyValue<double>(nameof(DnaAmountInitialMl), ref _DnaAmountInitialMl, value); }
        }


        private double _DnaAmountLeftMl;
        public double DnaAmountLeftMl
        {
            get { return _DnaAmountLeftMl; }
            set { SetPropertyValue<double>(nameof(DnaAmountLeftMl), ref _DnaAmountLeftMl, value); }
        }


        [Persistent]
        public double Left
        {
            get
            {
                if ((!this.IsLoading) && (!this.IsSaving))
                {
                    double left = DnaAmountLeftMl;
                    foreach (PcrDnaExtract item in Amplifications)
                    {
                        if (item.Pcr != null)
                            left = left - item.Pcr.Dna;
                    }
                    return left;
                }
                else return 0;
            }
        }
        Unit IUnitData.Unit
        {
            get { return Tissue; }
            set { Tissue = (Unit)value; }
        }
       
        private Unit _Tissue;
        [Association("DNAExtractTissue")]
        public Unit Tissue
        {
            get { return _Tissue; }
            set { SetPropertyValue<Unit>(nameof(Tissue), ref _Tissue, value); }
        }


        private Store _Store;
        [Association("DNAExtractStore")]
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }
        private string _StorePosition;
          public string StorePosition
        {
            get { return _StorePosition; }
            set { SetPropertyValue<string>(nameof(StorePosition), ref _StorePosition, value); }
        }



        [Association("PcrDnaExtrs"), Aggregated]
        public XPCollection<PcrDnaExtract> Amplifications
        {
            get
            {
                return GetCollection<PcrDnaExtract>("Amplifications");
            }
        }

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

    }


}
