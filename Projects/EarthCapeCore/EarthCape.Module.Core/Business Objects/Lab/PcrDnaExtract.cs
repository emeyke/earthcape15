using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    public class PcrDnaExtract : BaseObject, ISequenceObject
    {
        [Association("PcrDnaExtract-PcrDnaExtractsSequenced")]
        public XPCollection<PcrDnaExtractSequenced> Sequencing
        {
            get
            {
                return GetCollection<PcrDnaExtractSequenced>(nameof(Sequencing));
            }
        }


        private double? _Concentration;
        public double? Concentration
        {
            get { return _Concentration; }
            set { SetPropertyValue<double?>(nameof(Concentration), ref _Concentration, value); }
        }


        private double? _Dilution;
        public double? Dilution
        {
            get { return _Dilution; }
            set { SetPropertyValue<double?>(nameof(Dilution), ref _Dilution, value); }
        }

   
        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(500)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue<string>(nameof(FullName), ref _FullName, value); }
        }




        private string _Position;
        public string Position
        {
            get { return _Position; }
            set { SetPropertyValue<string>(nameof(Position), ref _Position, value); }
        }


  
        public PcrDnaExtract(Session session) : base(session) { }


        private DnaExtract _DnaExtract;
        [Association("PcrDnaExtrs")]
        [RuleRequiredField("RuleRequiredField for PcrDnaExtract.DnaExtract", DefaultContexts.Save, "DNA extract property must not be empty.")]
        public DnaExtract DnaExtract
        {
            get { return _DnaExtract; }
            set { SetPropertyValue<DnaExtract>(nameof(DnaExtract), ref _DnaExtract, value); }
        }



        private Amplification _Pcr;
        [Association("Amplification-DnaExtrs")]
        public Amplification Pcr
        {
            get { return _Pcr; }
            set { SetPropertyValue<Amplification>(nameof(Pcr), ref _Pcr, value); }
        }


    
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();

            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (DnaExtract != null)
                FullName = String.Format("{0}: {1} - {2}", Position, Result, DnaExtract.FullName);
            else
                FullName = String.Format("{0}: {1}", Position, Result);

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        private AmplificationResult? _Result;
        public AmplificationResult? Result
        {
            get { return _Result; }
            set { SetPropertyValue<AmplificationResult?>(nameof(Result), ref _Result, value); }
        }


     

        private Store _Store;
        [Association("StorePcrProducts")]
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }


        private Primer _PrimerForward;
        [Association("PrimerForward-Extraction")]
        public Primer PrimerForward
        {
            get { return _PrimerForward; }
            set { SetPropertyValue<Primer>(nameof(PrimerForward), ref _PrimerForward, value); }
        }


        private Primer _PrimerReverse;
        [Association("PrimerReverse-Extraction")]
        public Primer PrimerReverse
        {
            get { return _PrimerReverse; }
            set { SetPropertyValue<Primer>(nameof(PrimerReverse), ref _PrimerReverse, value); }
        }

        private PcrDnaExtract _RePcrSource;
        public PcrDnaExtract RePcrSource
        {
            get { return _RePcrSource; }
            set { SetPropertyValue<PcrDnaExtract>(nameof(RePcrSource), ref _RePcrSource, value); }
        }




        private string _Sequence;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string Sequence
        {
            get { return _Sequence; }
            set { SetPropertyValue<string>(nameof(Sequence), ref _Sequence, value); }
        }



        private string _SequenceForward;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string SequenceForward
        {
            get { return _SequenceForward; }
            set { SetPropertyValue<string>(nameof(SequenceForward), ref _SequenceForward, value); }
        }


        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        private string _Genbank;
        public string Genbank
        {
            get { return _Genbank; }
            set { SetPropertyValue<string>(nameof(Genbank), ref _Genbank, value); }
        }


        private string _ResultQuality;
        public string ResultQuality
        {
            get { return _ResultQuality; }
            set { SetPropertyValue<string>(nameof(ResultQuality), ref _ResultQuality, value); }
        }

        private string _SequenceReverse;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string SequenceReverse
        {
            get { return _SequenceReverse; }
            set { SetPropertyValue<string>(nameof(SequenceReverse), ref _SequenceReverse, value); }
        }


    }
  

}
