﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Sequence : BaseObject, IName
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Sequence(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [ModelDefault("RowCount", "1")]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }


        private Unit _Unit;
        [Association("UnitSequences")]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }
        private DnaExtract _DnaExtract;
        [Association("DnaExtractSequences")]
        public DnaExtract DnaExtract
        {
            get { return _DnaExtract; }
            set { SetPropertyValue<DnaExtract>(nameof(DnaExtract), ref _DnaExtract, value); }
        }


        private Gene _Marker;
        public Gene Marker
        {
            get { return _Marker; }
            set { SetPropertyValue<Gene>(nameof(Marker), ref _Marker, value); }
        }

        private string _BoldId;
        public string BoldId
        {
            get { return _BoldId; }
            set { SetPropertyValue<string>(nameof(BoldId), ref _BoldId, value); }
        }

        private string _Genbank;
        public string Genbank
        {
            get { return _Genbank; }
            set { SetPropertyValue<string>(nameof(Genbank), ref _Genbank, value); }
        }

        private string _Nucleotides;
        [VisibleInListView(false), VisibleInLookupListView(false), VisibleInDetailView(true)]
        [Size(SizeAttribute.Unlimited)]
        public string Nucleotides
        {
            get { return _Nucleotides; }
            set { SetPropertyValue<string>(nameof(Nucleotides), ref _Nucleotides, value); }
        }







    }
}