using DevExpress.Data;
using DevExpress.Data.Filtering;
using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp.Xpo;
using Xpand.ExpressApp.ExcelImporter.Services;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Lab
{
    //  [DefaultClassOptions]
    [ExcelImportKeyAttribute("Name")]
    [VisibleInReports(true)]
    public class Sequencing : BaseObject, IDatasetObject, IUnitData, ICode
    {


        private DnaExtract _DnaExtract;
        [Association("DnaExtract-NGS")]
        public DnaExtract DnaExtract
        {
            get { return _DnaExtract; }
            set { SetPropertyValue<DnaExtract>(nameof(DnaExtract), ref _DnaExtract, value); }
        }



        public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        }

        public Sequencing(Session session) : base(session) { }


        private Unit _Unit;
        [Association("Sequence-Unit")]
        public Unit Unit
        {
            get { return _Unit; }
            set { SetPropertyValue<Unit>(nameof(Unit), ref _Unit, value); }
        }




        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }


        private string _Run;
        public string Run
        {
            get { return _Run; }
            set { SetPropertyValue<string>(nameof(Run), ref _Run, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }


        public void UpdateInfo()
        {
            if ((Unit != null) && (!String.IsNullOrEmpty(Unit.UnitID)))
                UnitID = Unit.UnitID;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (Session.IsNewObject(this))
            {
            }
            UpdateInfo();
            if (IsLoading) return;

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }


        private string _LibraryPreparationMethod;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LibraryPreparationMethod
        {
            get { return _LibraryPreparationMethod; }
            set { SetPropertyValue<string>(nameof(LibraryPreparationMethod), ref _LibraryPreparationMethod, value); }
        }



        private string _LibraryPreparedBy;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LibraryPreparedBy
        {
            get { return _LibraryPreparedBy; }
            set { SetPropertyValue<string>(nameof(LibraryPreparedBy), ref _LibraryPreparedBy, value); }
        }


        private string _SequencingCentre;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SequencingCentre
        {
            get { return _SequencingCentre; }
            set { SetPropertyValue<string>(nameof(SequencingCentre), ref _SequencingCentre, value); }
        }



        private string _SequencingMachine;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SequencingMachine
        {
            get { return _SequencingMachine; }
            set { SetPropertyValue<string>(nameof(SequencingMachine), ref _SequencingMachine, value); }
        }




        private string _ReadType;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ReadType
        {
            get { return _ReadType; }
            set { SetPropertyValue<string>(nameof(ReadType), ref _ReadType, value); }
        }



        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set { SetPropertyValue<DateTime>(nameof(Date), ref _Date, value); }
        }



        private int _TotalReads;
        public int TotalReads
        {
            get { return _TotalReads; }
            set { SetPropertyValue<int>(nameof(TotalReads), ref _TotalReads, value); }
        }


        private string _Code;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get { return _Code; }
            set { SetPropertyValue<string>(nameof(Code), ref _Code, value); }
        }



        private string _UnitID;
        public string UnitID
        {
            get
            {
                return _UnitID;
            }
            set
            {
                SetPropertyValue("UnitID", ref _UnitID, value);
            }
        }

        private Dataset _Dataset;
        [Association("Sequence-Dataset")]
        public Dataset Dataset
        {
            get { return _Dataset; }
            set { SetPropertyValue<Dataset>(nameof(Dataset), ref _Dataset, value); }
        }

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

    }

}
