using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.ExcelImporter.Services;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [ExcelImportKeyAttribute("Name")]
    public class Genotyping : BaseObject, IName, IProjectObject
    {

        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }

        public Genotyping(Session session) : base(session) { }

        private Project _Project;
        public Project Project
        {
            get { return _Project; }
            set { SetPropertyValue<Project>(nameof(Project), ref _Project, value); }
        }


        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }


        [Association("Genotyping-GenotypedUnits"),Aggregated]
        public XPCollection<UnitGenotyping> GenotypedUnits
        {
            get
            {
                return GetCollection<UnitGenotyping>("GenotypedUnits");
            }
        }
        
    }
 

}
