using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;


namespace EarthCape.Module.Lab
{
    public class UnitGenotypingValue : BaseObject
    {
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }
        public UnitGenotypingValue(Session session) : base(session) { }

        private UnitGenotyping _Name;
        [Association("UnitGenotyping-Values"), Aggregated]
        public UnitGenotyping Name
        {
            get { return _Name; }
            set { SetPropertyValue<UnitGenotyping>(nameof(Name), ref _Name, value); }
        }


        private Marker _Marker;
        [Association("Marker-UnitGenotyping")]
        public Marker Marker
        {
            get { return _Marker; }
            set { SetPropertyValue<Marker>(nameof(Marker), ref _Marker, value); }
        }



        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { SetPropertyValue<string>(nameof(Value), ref _Value, value); }
        }
    }
}
