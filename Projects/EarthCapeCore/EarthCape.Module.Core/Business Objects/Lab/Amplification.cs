using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

using System.ComponentModel;
using DevExpress.Persistent.Base.General;
using System.Diagnostics.CodeAnalysis;
using Xpand.ExpressApp.ExcelImporter.Services;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Model;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [ExcelImportKeyAttribute("Name")]
    public class Amplification : BaseObject,IName,ILabBookPage,ITask
    {


        private string _LabBook;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("RTF")]
        public string LabBook
        {
            get { return _LabBook; }
            set { SetPropertyValue<string>(nameof(LabBook), ref _LabBook, value); }
        }



        private string _LabBookXLS;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("XLS")]
        public string LabBookXLS
        {
            get { return _LabBookXLS; }
            set { SetPropertyValue<string>(nameof(LabBookXLS), ref _LabBookXLS, value); }
        }







        private ProtocolPCR _Protocol;
        [Association("ProtocolPCR-Amplifications")]
        [ImmediatePostData(true)]
        public ProtocolPCR Protocol
        {
            get { return _Protocol; }
            set { SetPropertyValue<ProtocolPCR>(nameof(Protocol), ref _Protocol, value); }
        }


       
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
               }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }




        private string _CreatedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { SetPropertyValue<string>(nameof(CreatedByUser), ref _CreatedByUser, value); }
        }



        private string _LastModifiedByUser;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
        [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return _LastModifiedByUser; }
            set { SetPropertyValue<string>(nameof(LastModifiedByUser), ref _LastModifiedByUser, value); }
        }




        private DateTime _CreatedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { SetPropertyValue<DateTime>(nameof(CreatedOn), ref _CreatedOn, value); }
        }



        private DateTime _LastModifiedOn;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]
          [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { SetPropertyValue<DateTime>(nameof(LastModifiedOn), ref _LastModifiedOn, value); }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
                      if (SecuritySystem.CurrentUser != null)
                    {
                        
                        LastModifiedByUser = SecuritySystem.CurrentUserName;
                    }
                    LastModifiedOn = DateTime.Now;
         } 
        public Amplification(Session session)
            : base(session) 
        {
            Name = String.Format("{1:yyyyMMdd} - PCR-{0}", SecuritySystem.CurrentUserName ?? "", DateTime.Now);
        }

        private string _Name;
        [RuleRequiredField(DefaultContexts.Save)]
        [RuleUniqueValue(DefaultContexts.Save)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private double _Dna;
        public double Dna
        {
            get { return _Dna; }
            set { SetPropertyValue<double>(nameof(Dna), ref _Dna, value); }
        }



        /*
          private double _H2o;
          public double H2o
          {
              get
              {
                  return _H2o;
              }
              set
              {
                  SetPropertyValue("H2o", ref _H2o, value);
              }
          }
          private double _Buffer;
          public double Buffer
          {
              get
              {
                  return _Buffer;
              }
              set
              {
                  SetPropertyValue("Buffer", ref _Buffer, value);
              }
          }
          private double _Dntps;
          public double Dntps
          {
              get
              {
                  return _Dntps;
              }
              set
              {
                  SetPropertyValue("Dntps", ref _Dntps, value);
              }
          }
          private double _Taq;
          public double Taq
          {
              get
              {
                  return _Taq;
              }
              set
              {
                  SetPropertyValue("Taq", ref _Taq, value);
              }
          }

          private double _Dna;
          public double Dna
          {
              get
              {
                  return _Dna;
              }
              set
              {
                  SetPropertyValue("Dna", ref _Dna, value);
              }
          }
          private int _PcrInitialization;
          public int PcrInitialization
          {
              get
              {
                  return _PcrInitialization;
              }
              set
              {
                  SetPropertyValue("PcrInitialization", ref _PcrInitialization, value);
              }
          }
          private int _PcrDenaturation;
          public int PcrDenaturation
          {
              get
              {
                  return _PcrDenaturation;
              }
              set
              {
                  SetPropertyValue("PcrDenaturation", ref _PcrDenaturation, value);
              }
          }
          private int _PcrAnnealing;
          public int PcrAnnealing
          {
              get
              {
                  return _PcrAnnealing;
              }
              set
              {
                  SetPropertyValue("PcrAnnealing", ref _PcrAnnealing, value);
              }
          }
          private int _PcrElongation;
          public int PcrElongation
          {
              get
              {
                  return _PcrElongation;
              }
              set
              {
                  SetPropertyValue("PcrElongation", ref _PcrElongation, value);
              }
          }
          private int _PcrFinalElongation;
          public int PcrFinalElongation
          {
              get
              {
                  return _PcrFinalElongation;
              }
              set
              {
                  SetPropertyValue("PcrFinalElongation", ref _PcrFinalElongation, value);
              }
          }
          private int _PcrFinalHold;
          public int PcrFinalHold
          {
              get
              {
                  return _PcrFinalHold;
              }
              set
              {
                  SetPropertyValue("PcrFinalHold", ref _PcrFinalHold, value);
              }
          }
          private int _PcrTempInitialization;
          public int PcrTempInitialization
          {
              get
              {
                  return _PcrTempInitialization;
              }
              set
              {
                  SetPropertyValue("PcrTempInitialization", ref _PcrTempInitialization, value);
              }
          }
          private int _PcrTempDenaturation;
          public int PcrTempDenaturation
          {
              get
              {
                  return _PcrTempDenaturation;
              }
              set
              {
                  SetPropertyValue("PcrTempDenaturation", ref _PcrTempDenaturation, value);
              }
          }
          private int _PcrTempAnnealing;
          public int PcrTempAnnealing
          {
              get
              {
                  return _PcrTempAnnealing;
              }
              set
              {
                  SetPropertyValue("PcrTempAnnealing", ref _PcrTempAnnealing, value);
              }
          }
          private int _PcrTempElongation;
          public int PcrTempElongation
          {
              get
              {
                  return _PcrTempElongation;
              }
              set
              {
                  SetPropertyValue("PcrTempElongation", ref _PcrTempElongation, value);
              }
          }
          private int _PcrTempFinalElongation;
          public int PcrTempFinalElongation
          {
              get
              {
                  return _PcrTempFinalElongation;
              }
              set
              {
                  SetPropertyValue("PcrTempFinalElongation", ref _PcrTempFinalElongation, value);
              }
          }
          private int _PcrTempFinalHold;
          public int PcrTempFinalHold
          {
              get
              {
                  return _PcrTempFinalHold;
              }
              set
              {
                  SetPropertyValue("PcrTempFinalHold", ref _PcrTempFinalHold, value);
              }
          }
          private int _PcrCycles;
          public int PcrCycles
          {
              get
              {
                  return _PcrCycles;
              }
              set
              {
                  SetPropertyValue("PcrCycles", ref _PcrCycles, value);
              }
          }

          private double _PrimerForward;
          public double PrimerForward
          {
              get
              {
                  return _PrimerForward;
              }
              set
              {
                  SetPropertyValue("PrimerForward", ref _PrimerForward, value);
              }
          }
          private double _PrimerReverse;
          public double PrimerReverse
          {
              get
              {
                  return _PrimerReverse;
              }
              set
              {
                  SetPropertyValue("PrimerReverse", ref _PrimerReverse, value);
              }
          }

           private byte[] _GelImage;
          [Delayed(true)]
          [ImageEditor]
          [Size(-1)]
          public byte[] GelImage
          {
              get { return _GelImage; }
              set { SetPropertyValue("GelImage", ref _GelImage, value); }
          }

      */
        [Association("Amplification-DnaExtrs"), Aggregated]
        public XPCollection<PcrDnaExtract> DnaExtrs
        {
            get
            {
                return GetCollection<PcrDnaExtract>("DnaExtrs");
            }
        }


        private TaskImpl task = new TaskImpl();
        private Party assignedTo;
#if MediumTrust
		[Persistent("DateCompleted"), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public DateTime dateCompleted {
			get { return task.DateCompleted; }
			set {
				DateTime oldValue = task.DateCompleted;
				task.DateCompleted = value;
				OnChanged("dateCompleted", oldValue, task.DateCompleted);
			}
		}
#else
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [Persistent("DateCompleted")]
        private DateTime dateCompleted
        {
            get { return task.DateCompleted; }
            set
            {
                DateTime oldValue = task.DateCompleted;
                task.DateCompleted = value;
                OnChanged("dateCompleted", oldValue, task.DateCompleted);
            }
        }
#endif
        protected override void OnLoading()
        {
            task.IsLoading = true;
            base.OnLoading();
        }
        protected override void OnLoaded()
        {
            base.OnLoaded();
            task.DateCompleted = dateCompleted;
            task.IsLoading = false;
        }
        [Action(ImageName = "State_Task_Completed")]
        public void MarkCompleted()
        {
            TaskStatus oldStatus = task.Status;
            task.MarkCompleted();
            OnChanged("Status", oldStatus, task.Status);
            OnChanged("PercentCompleted");
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ToolTip("Service field. Use Name field instead.")]
        public string Subject
        {
            get { return task.Subject; }
            set
            {
                string oldValue = task.Subject;
                task.Subject = value;
                OnChanged("Subject", oldValue, task.Subject);
            }
        }
        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [ToolTip("Service field. Use Comment field instead.")]
        public string Description
        {
            get { return task.Description; }
            set
            {
                string oldValue = task.Description;
                task.Description = value;
                OnChanged("Description", oldValue, task.Description);
            }
        }
        public DateTime DueDate
        {
            get { return task.DueDate; }
            set
            {
                DateTime oldValue = task.DueDate;
                task.DueDate = value;
                OnChanged("DueDate", oldValue, task.DueDate);
            }
        }
        public DateTime StartDate
        {
            get { return task.StartDate; }
            set
            {
                DateTime oldValue = task.StartDate;
                task.StartDate = value;
                OnChanged("StartDate", oldValue, task.StartDate);
            }
        }
        public Party AssignedTo
        {
            get { return assignedTo; }
            set { SetPropertyValue("AssignedTo", ref assignedTo, value); }
        }
        public TaskStatus Status
        {
            get { return task.Status; }
            set
            {
                TaskStatus oldValue = task.Status;
                task.Status = value;
                OnChanged("Status", oldValue, task.Status);
            }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public Int32 PercentCompleted
        {
            get { return task.PercentCompleted; }
            set
            {
                Int32 oldValue = task.PercentCompleted;
                task.PercentCompleted = value;
                OnChanged("PercentCompleted", oldValue, task.PercentCompleted);
            }
        }
        public DateTime DateCompleted
        {
            get { return dateCompleted; }
        }

    }
  

}
