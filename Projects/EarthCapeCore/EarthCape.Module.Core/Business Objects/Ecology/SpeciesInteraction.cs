using System;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Ecology.FoodWeb
{
 
    //[DefaultClassOptions]
    public class SpeciesInteraction : BaseObject,IDatasetObject
    {
        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }
        
        public SpeciesInteraction(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        private Species _Species1;
     [Association("SpeciesInteraction1-Species")]
        public Species Species1
        {
            get
            {
                return _Species1;
            }
            set
            {
                SetPropertyValue("Species1", ref _Species1, value);
            }
        }
     /*   private InteractionRole? _Role1;
        public InteractionRole? Role1
        {
            get
            {
                return _Role1;
            }
            set
            {
                SetPropertyValue("Role1", ref _Role1, value);
            }
        }
        private InteractionRole? _Role2;
        public InteractionRole? Role2
        {
            get
            {
                return _Role2;
            }
            set
            {
                SetPropertyValue("Role2", ref _Role2, value);
            }
        }*/
        private Species _Species2;
        [Association("SpeciesInteraction2-Species")]
        public Species Species2
        {
            get
            {
                return _Species2;
            }
            set
            {
                SetPropertyValue("Species2", ref _Species2, value);
            }
        }
        private double _Value;
        public double Value
        {
            get
            {
                return _Value;
            }
            set
            {
                SetPropertyValue("Value", ref _Value, value);
            }
        }
        private FoodWeb _FoodWeb;
       [Association("FoodWeb-Interactions")]
        public FoodWeb FoodWeb
        {
            get
            {
                return _FoodWeb;
            }
            set
            {
                SetPropertyValue("FoodWeb", ref _FoodWeb, value);
            }
        }



       public bool Species1HasParent(HigherTaxon taxon)
       {
           TaxonomicName parent=Species1;
           while ((parent != null) && (parent.Oid != taxon.Oid))
           {
               parent = parent.Parent;
               if ((parent!=null) && (parent.Oid == taxon.Oid))
                   return true;
           }
           return false;
       }
    }

}
