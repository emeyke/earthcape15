using System;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;

using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Ecology.FoodWeb
{
    public enum FoodWebType
    {
        Count,
        Density
    }
   // [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class FoodWeb : BaseObject
    {
        private string _CoordSource;
        public string CoordSource
        {
            get
            {
                return _CoordSource;
            }
            set
            {
                SetPropertyValue("CoordSource", ref _CoordSource, value);
            }
        }
        
        public FoodWeb(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        protected override void OnSaving()
        {
            double count = 0;
            foreach (SpeciesInteraction item in this.Interactions)
            {
                count+=item.Value;
            }
            ICount = count;
            base.OnSaving();
        }
        private String _Name;
        public String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        private FoodWebType _FoodWebType;
        public FoodWebType FoodWebType
        {
            get
            {
                return _FoodWebType;
            }
            set
            {
                SetPropertyValue("FoodWebType", ref _FoodWebType, value);
            }
        }
        private String _TotalObservations;
        public String TotalObservations
        {
            get
            {
                return _TotalObservations;
            }
            set
            {
                SetPropertyValue("TotalObservations", ref _TotalObservations, value);
            }
        }
        private Locality _Locality;
        public Locality Locality
        {
            get
            {
                return _Locality;
            }
            set
            {
                SetPropertyValue("Locality", ref _Locality, value);
            }
        }
          [Association("FoodWeb-Interactions")]
        public XPCollection<SpeciesInteraction> Interactions
        {
            get
            {
                return GetCollection<SpeciesInteraction>("Interactions");
            }
        }
          private Double _Area;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public Double Area
          {
              get
              {
                  return _Area;
              }
              set
              {
                  SetPropertyValue("Area", ref _Area, value);
              }
          }

          private Double _Perimeter;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public Double Perimeter
          {
              get
              {
                  return _Perimeter;
              }
              set
              {
                  SetPropertyValue("Perimeter", ref _Perimeter, value);
              }
          }  private string _WKT;
 //         [Custom("PropertyEditorType", "EarthCape.Module.Core.Win.MapPlacePropertyEditor")]
          [Size(SizeAttribute.Unlimited)]
          public string WKT
          {
              get
              {
                  return _WKT;
              }
              set
              {
                  SetPropertyValue("WKT", ref _WKT, value);
              }
          }
          [Persistent]
          public double ICount;
          

          private  Decimal _Centroid_X;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public  Decimal Centroid_X
          {
              get
              {
                  return _Centroid_X;
              }
              set
              {
                  SetPropertyValue("Centroid_X", ref _Centroid_X, value);
              }
          }
          private  Decimal _Centroid_Y;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public  Decimal Centroid_Y
          {
              get
              {
                  return _Centroid_Y;
              }
              set
              {
                  SetPropertyValue("Centroid_Y", ref _Centroid_Y, value);
              }
          }
          private int _EPSG;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public int EPSG
          {
              get
              {
                  return _EPSG;
              }
              set
              {
                  SetPropertyValue("EPSG", ref _EPSG, value);
              }
          }
          private double _BBoxWest;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public double BBoxWest
          {
              get
              {
                  return _BBoxWest;
              }
              set
              {
                  SetPropertyValue("BBoxWest", ref _BBoxWest, value);
              }
          }
          private double _BBoxEast;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public double BBoxEast
          {
              get
              {
                  return _BBoxEast;
              }
              set
              {
                  SetPropertyValue("BBoxEast", ref _BBoxEast, value);
              }
          }
          private double _BBoxSouth;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public double BBoxSouth
          {
              get
              {
                  return _BBoxSouth;
              }
              set
              {
                  SetPropertyValue("BBoxSouth", ref _BBoxSouth, value);
              }
          }
          private double _BBoxNorth;
          [VisibleInDetailView(false)]
          [VisibleInListView(false)]
          public double BBoxNorth
          {
              get
              {
                  return _BBoxNorth;
              }
              set
              {
                  SetPropertyValue("BBoxNorth", ref _BBoxNorth, value);
              }
          }

    }

}
