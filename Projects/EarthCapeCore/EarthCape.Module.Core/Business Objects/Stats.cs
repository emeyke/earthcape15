#region Copyright (c) 2000-2011 Developer Express Inc.
/*
{*******************************************************************}
{                                                                   }
{       Developer Express .NET Component Library                    }
{       eXpressApp Framework                                        }
{                                                                   }
{       Copyright (c) 2000-2011 Developer Express Inc.              }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{   The entire contents of this file is protected by U.S. and       }
{   International Copyright Laws. Unauthorized reproduction,        }
{   reverse-engineering, and distribution of all or any portion of  }
{   the code contained in this file is strictly prohibited and may  }
{   result in severe civil and criminal penalties and will be       }
{   prosecuted to the maximum extent possible under the law.        }
{                                                                   }
{   RESTRICTIONS                                                    }
{                                                                   }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES           }
{   ARE CONFIDENTIAL AND PROPRIETARY TRADE                          }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS   }
{   LICENSED TO DISTRIBUTE THE PRODUCT AND ALL ACCOMPANYING .NET    }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                 }
{                                                                   }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                      }
{                                                                   }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
{   ADDITIONAL RESTRICTIONS.                                        }
{                                                                   }
{*******************************************************************}
*/
#endregion Copyright (c) 2000-2011 Developer Express Inc.

using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Drawing;
namespace EarthCape.Module.Core
{
    public class Stats : BaseObject
    {


        public Stats(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not Locality any code here or Locality it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to Locality your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Locality here your initialization code.
        }
        string userName;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                SetPropertyValue("UserName", ref userName, value);
            }
        }

        private string _ViewId;
        [Size(SizeAttribute.Unlimited)]
        public string ViewId
        {
            get { return _ViewId; }
            set { SetPropertyValue<string>(nameof(ViewId), ref _ViewId, value); }
        }



        private string _ViewCaption;
        [Size(SizeAttribute.Unlimited)]
        public string ViewCaption
        {
            get { return _ViewCaption; }
            set { SetPropertyValue<string>(nameof(ViewCaption), ref _ViewCaption, value); }
        }



        private string _ViewType;
        public string ViewType
        {
            get { return _ViewType; }
            set { SetPropertyValue<string>(nameof(ViewType), ref _ViewType, value); }
        }


        private Guid _ObjectId;
        public Guid ObjectId
        {
            get { return _ObjectId; }
            set { SetPropertyValue<Guid>(nameof(ObjectId), ref _ObjectId, value); }
        }


        private string _ObjectType;
        public string ObjectType
        {
            get { return _ObjectType; }
            set { SetPropertyValue<string>(nameof(ObjectType), ref _ObjectType, value); }
        }


        private DateTime _DateTime;
        public DateTime DateTime
        {
            get { return _DateTime; }
            set { SetPropertyValue<DateTime>(nameof(DateTime), ref _DateTime, value); }
        }


        private string _Application;
        [Size(1000)]
        public string Application
        {
            get { return _Application; }
            set { SetPropertyValue<string>(nameof(Application), ref _Application, value); }
        }



        private string _Version;
        public string Version
        {
            get { return _Version; }
            set { SetPropertyValue<string>(nameof(Version), ref _Version, value); }
        }


        [Persistent]
        public int Year
        { get { return DateTime.Year; } }
        [Persistent]
        public int Day
        { get { return DateTime.Day; } }
        [Persistent]
        public int Month
        { get { return DateTime.Month; } }
        [Persistent]
        public string YearMonth
        { get { return Year + "-" + Month; } }
    }

}
