using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.BaseImpl;
using System;
using System.Collections.Generic;



namespace EarthCape.Module.Core
{
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion) { }

        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();

            CreateMapProjects();
            CreateLicense("cc-zero", "Public Domain (CC0 1.0)", "https://creativecommons.org/publicdomain/zero/1.0/legalcode", "");
            CreateLicense("cc-by", "Creative Commons Attribution (CC-BY) 4.0", "https://creativecommons.org/licenses/by/4.0/legalcode", "");
           // CreateLicense("cc-by-sa-4.0", "Attribution-ShareAlike 4.0 International", "https://creativecommons.org/licenses/by-sa/4.0/legalcode", "");
           // CreateLicense("cc-by-nd-4.0", "Attribution-NoDerivatives 4.0 International", "https://creativecommons.org/licenses/by-nd/4.0/legalcode", "");
            CreateLicense("cc-by-nc-4.0", "Creative Commons Attribution Non Commercial (CC-BY-NC) 4.0", "https://creativecommons.org/licenses/by-nc/4.0/legalcode", "");
           // CreateLicense("cc-by-nc-nd-4.0", "Attribution-NonCommercial-NoDerivatives 4.0 International", "https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode", "");

            ObjectSpace.CommitChanges();

        }

        private void CreateLicense(string name,string title,string url,string text)
        {
            License license = ObjectSpace.FindObject<License>(new BinaryOperator("Name", name));
            if (license == null)
            {
                license = ObjectSpace.CreateObject<License>();
                license.Name = name;
                license.Title = title;
                license.Url = url;
                license.Text=text;
                license.Save();
            }
        }

        private void AddProject(string title, string epsg, string layer)
        {
            MapProject map = ObjectSpace.FindObject<MapProject>(new BinaryOperator("Name", title));
            if (map == null)
            {
                map = ObjectSpace.CreateObject<MapProject>();
                List<string> config = new List<string>();

                config.Add("[TatukGIS]");
                config.Add("CodePage=1252");
                config.Add("CS.EPSG=" + epsg);
                config.Add("OutCodePage=1252");
                config.Add(string.Empty);
                config.Add(string.Empty);
                config.Add("[TatukGIS Layer1]");
                config.Add("Active=YES");
                config.Add("Caption=" + title);
                config.Add("CodePage=1252");
                config.Add("CS.EPSG=" + epsg);
                config.Add("CurrentPage=1");
                config.Add("IncrementalPaint=YES");
                config.Add("Name=mapquest_aerialtiles");
                config.Add("OutCodePage=1252");
                config.Add(String.Format("Path={0}\\EarthCape\\Public Map Services\\{1}.ttkwp", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), title));
                config.Add("Visible=YES");
                config.Add(string.Empty);
                config.Add("[TatukGIS Hierarchy]");
                config.Add("Other=" + layer);
                config.Add(string.Empty);
                config.Add("[TatukGIS Group1]");
                config.Add("Active=YES");
                config.Add("Caption=Other");
                config.Add("CodePage=1252");
                config.Add("Name=Other");
                config.Add("OutCodePage=1252");
                config.Add("Visible=YES;");
                map.ProjectConfig = string.Join(Environment.NewLine, config.ToArray());
                map.EPSG = Convert.ToInt32(epsg);
                map.Name = title;
                map.Save();
            }
        }
        private void CreateMapProjects()
        {
            AddProject("OpenStreetMaps", "4326", "openstreetmaps");
            // AddProject("MapQuest OpenStreetMaps Tiles", "3785", "mapquest_osm");
            //AddProject("NASA BMNG", "4326", "nasa_bmng");
            //AddProject("NASA Global Moisaic", "4326", "nasa_global_mosaic");
            //AddProject("OpenStreetMaps Tiles", "4326", "opestreetmaps_osm");
            //AddProject("OpenStreetMaps WMS", "4326", "osm_wms");
            //AddProject("WorldWind", "4326", "wordlwind");
        }



        /*   private Role CreateAdministratorRole()
           {
               Role administratorRole = ObjectSpace.FindObject<Role>(new BinaryOperator("Name", "Administrator"));
               if (administratorRole == null)
               {
                   administratorRole = ObjectSpace.CreateObject<Role>();
                   administratorRole.Name = "Administrator";
                   administratorRole.RoleType = RoleType.Admin;
                   administratorRole.IsAdministrative = true;
               }

               return administratorRole;
           }*/




    }
}
