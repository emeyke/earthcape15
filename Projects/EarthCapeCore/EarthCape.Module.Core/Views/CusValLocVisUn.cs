using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core
{
  

    public class CusValLocVisUn : XPLiteObject
    {
        public CusValLocVisUn(Session session) : base(session) { }
        [Persistent("Oid"), Key(true), Browsable(false), MemberDesignTimeVisibility(false)]
        private Guid oid;
        [PersistentAlias("oid"), Browsable(false)]
        public Guid Oid
        {
            get
            {
                return oid;
            }
        }
        public string LocalityVisitName;
        public DateTime LocalityVisitDateTimeStart;
        public string LocalityVisitMethod;
        public string UnitTemporaryName;
        public double UnitQuantity;
        public string LocalityCategoryName;
        public string LocalityName;
        public string LocalityVisitCustomValueField;
        public string LocalityVisitCustomValueDataset;
        public string LocalityVisitProject;
        public double LocalityVisitCustomValueNumber;
        public DateTime LocalityVisitCustomValueDateTime;
        public string LocalityVisitCustomValueText;
        public bool LocalityVisitCustomValueBool;
    }
    /*  public class CusValLocVisUn : XPLiteObject
      {
          public CusValLocVisUn(Session session) : base(session) { }
          [Key, Persistent, Browsable(false)]
          public CusValLocVisUnKey Key;
          public string LocalityVisitName { get { return Key.LocalityVisitName; } }
          public DateTime LocalityVisitDateTimeStart { get { return Key.LocalityVisitDateTimeStart; } }
          public string LocalityVisitMethod { get { return Key.LocalityVisitMethod; } }
          public string UnitTemporaryName { get { return Key.UnitTemporaryName; } }
          public double UnitQuantity { get { return Key.UnitQuantity; } }
          public string LocalityCategoryName { get { return Key.LocalityCategoryName; } }
          public string LocalityName { get { return Key.LocalityName; } }
          public double LocalityVisitCustomValueNumber { get { return Key.LocalityVisitCustomValueNumber; } }
          public DateTime LocalityVisitCustomValueDateTime { get { return Key.LocalityVisitCustomValueDateTime; } }
          public string LocalityVisitCustomValueFieldName { get { return Key.LocalityVisitCustomValueFieldName; } }
          public string LocalityVisitCustomValueDataset { get { return Key.LocalityVisitCustomValueDataset; } }
          public string LocalityVisitProject { get { return Key.LocalityVisitProject; } }
      }
      public struct CusValLocVisUnKey
      {
          [Persistent("LVN"), Browsable(false)]
          public string LocalityVisitName;
          [Persistent("LVDTS"), Browsable(false)]
          public DateTime LocalityVisitDateTimeStart;
          [Persistent("LLVM"), Browsable(false)]
          public string LocalityVisitMethod;
          [Persistent("UTN"), Browsable(false)]
          public string UnitTemporaryName;
          [Persistent("UQ"), Browsable(false)]
          public double UnitQuantity;
          [Persistent("LCN"), Browsable(false)]
          public string LocalityCategoryName;
          [Persistent("LN"), Browsable(false)]
          public string LocalityName;
          [Persistent("LVCVN"), Browsable(false)]
           public double LocalityVisitCustomValueNumber;
          [Persistent("LVCVDT"), Browsable(false)]
          public DateTime LocalityVisitCustomValueDateTime;
          [Persistent("LVCVD"), Browsable(false)]
          public string LocalityVisitCustomValueFieldName;
          [Persistent("LocalityVisitCustomValueDataset"), Browsable(false)]
          public string LocalityVisitCustomValueDataset;
          [Persistent("LVP"), Browsable(false)]
          public string LocalityVisitProject;
      }
      */
}
