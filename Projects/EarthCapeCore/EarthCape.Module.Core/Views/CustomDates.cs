using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core
{
    public class CustomDates : XPLiteObject
    {
        public CustomDates(Session session) : base(session) { }
        [Persistent("Oid"), Key(true), Browsable(false), MemberDesignTimeVisibility(false)]
        private Guid oid;
        [PersistentAlias("oid"), Browsable(false)]
        public Guid Oid
        {
            get
            {
                return oid;
            }
        }   
        public int DayOfYear;
        public DateTime Value;
        public string EventType;
        public string TaxonomicName;
        public string Event;
        public string Dataset;
        public string Project;
        public string DatasetDescription;
        public string ProjectDescription;
    }
 
}
