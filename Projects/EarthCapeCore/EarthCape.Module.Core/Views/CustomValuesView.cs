using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core
{
    public class CustomValuesView : XPLiteObject
    {
        public CustomValuesView(Session session) : base(session) { }
        [Persistent("Oid"), Key(true), Browsable(false), MemberDesignTimeVisibility(false)]
        private Guid oid;
        [PersistentAlias("oid"), Browsable(false)]
        public Guid Oid
        {
            get
            {
                return oid;
            }
        }  public string CustomValueString;
        public double CustomValueNumber;
        public DateTime CustomValueDateTime;
        public bool CustomValueBoolean;
        public string CustomFieldName;
        public string CustomValueDataset;
        public string CustomFieldProject;
        public string UnitID;
        public string LocalityName;
        //[VisibleInListView(false)]
        //[VisibleInDetailView(false)]
        //public Project CustomFieldProjectOid;
    }
  
}
