using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core
{
    public class CusValCntPerPrjView : XPLiteObject
    {
        public CusValCntPerPrjView(Session session) : base(session) { }
        [Persistent("Project"), Key(true), Browsable(false), MemberDesignTimeVisibility(false)]
        private string project;
        [PersistentAlias("project"), Browsable(false)]
        public string Project
        {
            get
            {
                return project;
            }
        }   
        //public string Project;
        public int ValueCount;
    }
}
