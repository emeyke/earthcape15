﻿using DevExpress.ExpressApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Core
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DwCAttribute : Attribute
    {
        private bool _IsDwcDynamicProperty;
        private string _Term;

        public DwCAttribute(string value) { _Term = value; }
        public DwCAttribute(bool value) { _IsDwcDynamicProperty = value; }
        public DwCAttribute() { _IsDwcDynamicProperty = false; _Term = ""; }

        public bool IsDwcDynamicProperty
        {
            get { return _IsDwcDynamicProperty; }
            set { _IsDwcDynamicProperty = value; }
        }
        public string Term
        {
            get { return _Term; }
            set { _Term = value; }
        }
    }
    [KeyProperty("Id")]
    public interface IModelDwCNode : IModelNode
    {
        string Id { get; set; }
        bool IsDwcDynamicProperty { get; set; }
        string Term { get; set; }

    }
    public interface IModelDwCExtension : IModelNode
    {
        IModelDwCNode DwC { get; }
    }



}
