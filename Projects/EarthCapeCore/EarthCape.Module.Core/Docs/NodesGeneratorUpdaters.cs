﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Model.NodeGenerators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Core
{
    public class ModelBOModelClassNodesGeneratorUpdater : ModelNodesGeneratorUpdater<ModelBOModelClassNodesGenerator>
    {
        public override void UpdateNode(ModelNode node)
        {
            // IModelBOModel boModel = node as IModelBOModel;
            if (node is IModelBOModel)
                foreach (IModelClass modelClass in node.Nodes)
                {
                    if (modelClass.TypeInfo.FindAttributes<DocumentationAttribute>().Count() > 0)
                    {
                        DocumentationAttribute attr = modelClass.TypeInfo.FindAttributes<DocumentationAttribute>().First();
                        if (attr != null)
                            if (!String.IsNullOrEmpty(attr.Description))
                                modelClass.GetNode("Documentation").SetValue("Description", attr.Description);
                    }
                    foreach (IModelMember item in modelClass.OwnMembers)
                    {
                        if (item.MemberInfo.FindAttributes<DocumentationAttribute>().Count() > 0)
                        {
                            DocumentationAttribute attr = item.MemberInfo.FindAttributes<DocumentationAttribute>().First();
                            if (attr != null)
                                if (!String.IsNullOrEmpty(attr.Description))
                                {
                                    item.GetNode("Documentation").SetValue("Description", attr.Description);
                                    if (string.IsNullOrEmpty(item.ToolTip))
                                        item.ToolTip = attr.Description;
                                }
                        }
                    }                  
                }
        }
    }
    public class ModelBOModelMemberNodesGeneratorUpdater : ModelNodesGeneratorUpdater<ModelBOModelMemberNodesGenerator>
    {
        public override void UpdateNode(ModelNode node)
        {
            // IModelBOModel boModel = node as IModelBOModel;
            if (node is IModelMember)
                foreach (IModelClass modelClass in node.Nodes)
                {
                    foreach (IModelMember item in modelClass.OwnMembers)
                    {
                        foreach (DwCAttribute attr in item.MemberInfo.FindAttributes<DwCAttribute>())
                        {
                            if (attr != null)
                            {
                                if (true)
                                {
                                    item.GetNode("DwC").SetValue("IsDwcDynamicProperty", attr.IsDwcDynamicProperty);
                                }
                                if (!String.IsNullOrEmpty(attr.Term))
                                    item.GetNode("DwC").SetValue("Term", attr.Term);
                            }
                        }
                    }
                }
            if (node is IModelListView)
            {
                foreach (IModelColumn item in (node as IModelListView).Columns)
                {
                    foreach (DocumentationAttribute attr in item.ModelMember.MemberInfo.FindAttributes<DocumentationAttribute>())
                    {
                        if (attr != null)
                            if (!String.IsNullOrEmpty(attr.Description))
                            {
                                item.GetNode("Documentation").SetValue("Description", attr.Description);
                                if (string.IsNullOrEmpty(item.ToolTip))
                                    item.ToolTip = attr.Description;
                            } else
                            {
                            //    item.GetNode("Documentation").SetValue("Description", item.ModelMember.GetNode("Documentation").GetValue<String>("Description"));

                            }
                    }

                }
            }
        }
    }

}
