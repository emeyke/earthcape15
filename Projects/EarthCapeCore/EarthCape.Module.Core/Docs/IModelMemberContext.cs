﻿using DevExpress.ExpressApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Core
{
    public interface IModelColumnContext : IModelNode
    {
        [Category("Publish")]
        string Context { get; set; }
        [Category("Publish")]
        string URL { get; set; }
    }
    [ModelInterfaceImplementor(typeof(IModelColumnContext), "ModelColumn")]
    public interface IModelCommonMemberViewItemContext : IModelColumnContext
    {

    }


}
