﻿using DevExpress.ExpressApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Core
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DocumentationAttribute : Attribute
    {
        private string _Description;

        public DocumentationAttribute(string value) { _Description = value; }
        public DocumentationAttribute() { _Description = string.Empty; }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    }

  
    [KeyProperty("Id")]
    public interface IModelDocNode : IModelNode
    {
        string Id { get; set; }
        [Localizable(true)]
        [Description]
        string Description { get; set; }
        [Localizable(true)]
        string QuickStart { get; set; }
    }
    public interface IModelDocExtension : IModelNode
    {
        IModelDocNode Documentation { get; }
    }

    

    
}
