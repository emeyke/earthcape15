﻿namespace EarthCape.Module.Core.Controllers
{
    partial class IRecordSets_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddToRecordSets = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddToRecordSets
            // 
            this.popupWindowShowAction_AddToRecordSets.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddToRecordSets.CancelButtonCaption = null;
            this.popupWindowShowAction_AddToRecordSets.Caption = "Add to record sets";
            this.popupWindowShowAction_AddToRecordSets.Category = "Tools";
            this.popupWindowShowAction_AddToRecordSets.ConfirmationMessage = null;
            this.popupWindowShowAction_AddToRecordSets.Id = "popupWindowShowAction_AddToRecordSets";
            this.popupWindowShowAction_AddToRecordSets.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddToRecordSets.ToolTip = null;
            this.popupWindowShowAction_AddToRecordSets.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddToRecordSets_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddToRecordSets.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToRecordSets_Execute);
            // 
            // IRecordSets_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddToRecordSets);
            this.TargetObjectType = typeof(EarthCape.Module.Core.IRecordSetsObject);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddToRecordSets;
    }
}
