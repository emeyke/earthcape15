using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.Core
{
    public partial class TaxonomicTools_ViewController : ViewController
    {
        public TaxonomicTools_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_AddToClassification_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
           /* ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
             CollectionSource ds = new CollectionSource(os, typeof(TaxonClassification));
            e.DialogController.SaveOnAccept = false;
            e.View = Application.CreateListView("TaxonClassification_LookupListView", ds, false);
            e.View.Caption = "Select target classification";
     */
        }

        private void popupWindowShowAction_AddToClassification_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            /*ObjectSpace os = (ObjectSpace)View.ObjectSpace;
            TaxonClassification classification = os.FindObject < TaxonClassification>(new BinaryOperator("Oid",((TaxonClassification)(((ListView)e.PopupWindow.View).CurrentObject)).Oid));
          foreach (TaxonomicName item in View.SelectedObjects)
            {
                TaxonClassified cl = null;
                cl = os.FindObject<TaxonClassified>(CriteriaOperator.And(new BinaryOperator("TaxonomicName.Oid", item.Oid),new BinaryOperator("Classification.Oid",classification.Oid)));
                if (cl == null)
                {
                    cl = os.FindObject<TaxonClassified>(CriteriaOperator.And(new BinaryOperator("TaxonomicName.Oid", item.Oid), new BinaryOperator("Classification.Oid", classification.Oid)), true);
                    if (cl == null)
                    {
                        cl = os.CreateObject<TaxonClassified>();
                        cl.TaxonomicName = item;
                        cl.Classification = classification;
                        cl.Save();
                    }
                }
          }*/
        }

        private void popupWindowShowAction_TaxonAddToList_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
     /*       ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            CollectionSource ds = new CollectionSource(os, typeof(TaxonomicList));
            e.DialogController.SaveOnAccept = false;
            e.View = Application.CreateListView("TaxonomicList_LookupListView", ds, false);
            e.View.Caption = "Select target lists";*/
        }

        private void popupWindowShowAction_TaxonAddToList_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
       /*     ObjectSpace os = (ObjectSpace)View.ObjectSpace;
            foreach (TaxonomicList _list in ((ListView)e.PopupWindow.View).SelectedObjects)
            {
                TaxonomicList list = os.FindObject<TaxonomicList>(new BinaryOperator("Oid", (_list).Oid));
                foreach (TaxonomicName item in View.SelectedObjects)
                {
                    list.Names.Add(item);
                    list.Save();
                }
            }
            os.CommitChanges();*/
        }

        private void simpleAction_ShowUnitsForNames_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //todo - doesnt work    
             ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
                 List<Guid> list = new List<Guid>();
                foreach (TaxonomicName name in View.SelectedObjects)
                {
                    list.Add(name.Oid);
                }
                InOperator inOperator = new InOperator("TaxonomicName.Oid", list);
                CollectionSource source = new CollectionSource(os, typeof(Unit),true,CollectionSourceMode.Normal);
                source.Criteria[""] = inOperator;
                ListView view = Application.CreateListView("Unit_ListView", source, false);
                e.ShowViewParameters.CreatedView=view;
                e.ShowViewParameters.NewWindowTarget = NewWindowTarget.MdiChild;
                e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
              //  ((ListView)e.ShowViewParameters.CreatedView).CollectionSource.Criteria[""] = inOperator;
        }
    }
}
