namespace EarthCape.Module.Core.Controllers
{
    partial class SetFilter_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_SetFilter = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_SetFilter
            // 
            this.popupWindowShowAction_SetFilter.AcceptButtonCaption = null;
            this.popupWindowShowAction_SetFilter.CancelButtonCaption = null;
            this.popupWindowShowAction_SetFilter.Caption = "Set filter";
            this.popupWindowShowAction_SetFilter.Category = "Tools";
            this.popupWindowShowAction_SetFilter.ConfirmationMessage = null;
            this.popupWindowShowAction_SetFilter.Id = "popupWindowShowAction_SetFilter";
            this.popupWindowShowAction_SetFilter.ToolTip = null;
            this.popupWindowShowAction_SetFilter.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_SetFilter_CustomizePopupWindowParams);
            this.popupWindowShowAction_SetFilter.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_SetFilter_Execute);
            // 
            // SetFilter_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_SetFilter);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_SetFilter;

    }
}
