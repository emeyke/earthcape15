using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using EarthCape.Module;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.Core
{
    public partial class DeriveSamples_ViewController : ViewController
    {
        public DeriveSamples_ViewController()
        {
           InitializeComponent();
            RegisterActions(components);
        }

        private void popupAction_Derive_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ObjectSpace os = (ObjectSpace)e.PopupWindow.View.ObjectSpace;
                 DerivedSamples derivedSamples = ((DerivedSamples)e.PopupWindow.View.CurrentObject);
                foreach (Unit item in View.SelectedObjects)
                {
                    for (int i = 0; i < derivedSamples.NumberOfParts - 1; i++)
                    {
                        Unit newUnit = os.CreateObject<Unit>();
                      /*  if (derivedSamples.TemplateUnit != null)
                            foreach (XPMemberInfo memberInfo in os.Session.GetClassInfo(derivedSamples.TemplateUnit.GetType()).PersistentProperties)
                            {
                                if (memberInfo is DevExpress.Xpo.Metadata.Helpers.ServiceField)
                                    continue;
                                memberInfo.SetValue(newUnit, memberInfo.GetValue(derivedSamples.TemplateUnit));
                            }*/
                        newUnit.UnitType = derivedSamples.UnitType;
                        newUnit.DerivedFrom = os.FindObject<Unit>(new BinaryOperator("Oid", item.Oid));
                        if (derivedSamples.OwnerGroup != null)
                            newUnit.Group = derivedSamples.OwnerGroup;
                        if (derivedSamples.SharedWithGroup != null)
                            newUnit.Groups.Add(derivedSamples.SharedWithGroup);
                        if (derivedSamples.Store != null)
                            newUnit.Store = derivedSamples.Store;
                        if (derivedSamples.IDGenerator != null)
                            newUnit.AccessionNumber = CollectionsRoutines.GenerateID(derivedSamples.IDGenerator,newUnit);
                      /*  if (newUnit.AccessionNumber == item.AccessionNumber)
                            newUnit.AccessionNumber = newUnit.AccessionNumber + Convert.ToString(i);*/
                        newUnit.Save();
                    }
                }
               // CollectionSource ds = new CollectionSource(os, View.ObjectTypeInfo.Type);
             //   e.PopupWindow.SetView(Application.CreateListView(Application.FindListViewId(View.ObjectTypeInfo.Type), ds, false));
                os.CommitChanges();
        }

        private void popupAction_Derive_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            DetailView dv = Application.CreateDetailView(os, os.CreateObject<DerivedSamples>());
            e.View = dv;
        }
    }
}
