using System;
using System.Linq;
using DevExpress.ExpressApp;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using Xpand.ExpressApp.Security.Registration;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.Core
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class UserPasswordRestore_ViewController : ViewController
    {
        public UserPasswordRestore_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType = typeof(RestorePasswordParameters);
        }

        void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            User user = View.ObjectSpace.FindObject<User>(new BinaryOperator("Email", ((RestorePasswordParameters)View.CurrentObject).Email));
            if (user != null)
            {
              /*  user.
                string newPassword = GeneralHelper.CreatePassword(6, 2);
                user.SetPassword(newPassword);
                Emailer.SendRestorePassword(user.Email, user.UserName, newPassword,"", Emailer.GetSmtp(), Emailer.PasswordRestoreEmailTemplate);
           */ }
            else
            { }
         
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            View.ObjectSpace.Committing += ObjectSpace_Committing;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
       

        
    }
}
