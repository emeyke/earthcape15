using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Security;

using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace EarthCape.Module.Core
{
    public partial class IName_ViewController : ViewController
    {
        public IName_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            //   this.Activated += IName_ViewController_Activated;
        }

        void IName_ViewController_Activated(object sender, EventArgs e)
        {
            if (popupWindowShowAction_FilterByNameList != null)
            {
                popupWindowShowAction_FilterByNameList.Active.SetItemValue(string.Empty, false);
                NewObjectViewController newObjContr = Frame.GetController<NewObjectViewController>();
                if (newObjContr != null)
                {
                    if (newObjContr.NewObjectAction.Active.ResultValue == true)
                        popupWindowShowAction_FilterByNameList.Active.SetItemValue(string.Empty, true);
                }
            }
            ViewControlsCreated += IName_ViewController_ViewControlsCreated;
        }
        private void IName_ViewController_ViewControlsCreated(object sender, EventArgs e)
        {
            if ((simpleAction_FindDuplicates.Active) && (View.ObjectTypeInfo != null))
            {
                simpleAction_FindDuplicates.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                popupWindowShowAction_MergeTo.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }

        private void popupWindowShowAction_FilterByNameList_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            NameList list = (NameList)e.PopupWindow.View.CurrentObject;
            GeneralHelper.FilterByName(list.Names, (ListView)View);
        }


        private void popupWindowShowAction_FilterByNameList_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            NameList list = os.CreateObject<NameList>();
            DetailView dv = Application.CreateDetailView(os, list);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;

        }

        private void popupWindowShowAction_ShareByList_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            /*  IObjectSpace os = Application.CreateObjectSpace();
              ShareNameList list = (ShareNameList)e.PopupWindow.View.CurrentObject;
              string[] words = list.Names.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
              Collection<String> words_left = new Collection<string>();
              if (!list.NotInTheList)
              {
                  foreach (String item in words)
                  {
                      object obj = os.FindObject(View.ObjectTypeInfo.Type, new BinaryOperator("Name", item), false);
                      if (obj == null)
                          words_left.Add(item);
                      else
                      {
                          foreach (ViewItem viewitem in ((DetailView)e.PopupWindow.View).Items)
                          {
                              if (viewitem is ListPropertyEditor)
                              {
                                  foreach (Group group in ((ListPropertyEditor)viewitem).ListView.SelectedObjects)
                                  {
                                      Group _group = os.FindObject<Group>(new BinaryOperator("Oid", Project.Oid));
                                      ((BaseObject)obj).Groups.Add(_group);
                                      ((BaseObject)obj).Save();
                                  }
                              }
                          }

                      }
                  }
                 // if (os.IsModified)
                      os.CommitChanges();
              }
              else
              {
                  Collection<Group> groups = new Collection<Group>();
                  foreach (ViewItem viewitem in ((DetailView)e.PopupWindow.View).Items)
                  {
                      if (viewitem is ListPropertyEditor)
                      {
                          foreach (Group group in ((ListPropertyEditor)viewitem).ListView.SelectedObjects)
                          {
                              Group _group = View.ObjectSpace.FindObject<Group>(new BinaryOperator("Oid", Project.Oid));
                              groups.Add(_group);
                          }
                      }
                  }
                  foreach (IName item in View.SelectedObjects)
                  {
                      if (Array.IndexOf(words, item.Name) < 0)
                          foreach (Group group in groups)
                          {
                              ((BaseObject)item).Groups.Add(Project);
                              ((BaseObject)item).Save();

                          }
                  }
                //  if (View.ObjectSpace.IsModified)
                      View.ObjectSpace.CommitChanges();

              }
              */
        }

        private void popupWindowShowAction_ShareByList_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            /* XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
             ShareNameList list = os.CreateObject<ShareNameList>();
             DetailView dv = Application.CreateDetailView(os, list);
             e.View = dv;
             */
        }

        private void popupWindowShowAction_FieldsByList_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            /*     XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                 FieldList list = os.CreateObject<FieldList>();
                 DetailView dv = Application.CreateDetailView(os, list);
                 dv.ViewEditMode = ViewEditMode.Edit;
                 e.View = dv;
                 if ((Frame is NestedFrame) && (((NestedFrame)Frame).ViewItem.View.CurrentObject != null))
                 {
                     if (((NestedFrame)Frame).ViewItem.View.ObjectTypeInfo.Type == typeof(Project))
                         list.Project = os.FindObject<Project>(new BinaryOperator("Oid",((Project)((NestedFrame)Frame).ViewItem.View.CurrentObject).Oid));
                 }*/
        }


        private void popupWindowShowAction_MergeTo_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            // CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os, View.ObjectTypeInfo.Type, false);
            e.View = view;

        }



        private void popupWindowShowAction_MergeTo_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IName obj = (IName)e.PopupWindow.View.SelectedObjects[0];
            IObjectSpace os = View.ObjectSpace;
            /*  if (obj is CustomField)
             {
                 MergeCustomField(obj, os);
             }*/
            if (obj is TaxonomicName)
            {
                MergeTaxonomicName(obj, os);
            }
            os.CommitChanges();
            os.Refresh();
            foreach (BaseObject oldObj in View.SelectedObjects)
            {
                if (oldObj.Oid != obj.Oid)
                {
                    oldObj.Delete();
                }
            }
            os.CommitChanges();
        }

        private void MergeTaxonomicName(IName obj, IObjectSpace os)
        {
            TaxonomicName taxon = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", obj.Oid));
            foreach (TaxonomicName oldtaxon in View.SelectedObjects)
            {
                if (oldtaxon.Oid != taxon.Oid)
                {
                    foreach (Unit unit in oldtaxon.Units)
                    {
                        unit.TaxonomicName = taxon;
                        unit.Save();
                    }
                    //todo
                    /* if (taxon.NoteSet == null)
                         taxon.NoteSet = os.CreateObject<NoteSet>();
                     if (oldtaxon.NoteSet != null)
                     {
                         foreach (Note note in oldtaxon.NoteSet.Notes)
                         { taxon.NoteSet.Notes.Add(note); }
                     }
                     if (oldtaxon.FileSet != null)
                     {
                         foreach (FileItem file in oldtaxon.FileSet.Files)
                         { taxon.FileSet.Files.Add(file); }
                     }*/
                    //customdata
                    /* foreach (CustomValue cval in oldtaxon.CustomData){
                         cval.AssociatedTaxonomicName = taxon;
                         cval.Save();
                     }*/
                    //synonyms
                    foreach (Synonymy syn in oldtaxon.Synonyms)
                    {
                        syn.TaxonomicName = taxon;
                        syn.Save();
                    }

                    //children
                    foreach (TaxonomicName child in oldtaxon.Children)
                    {
                        child.Parent = taxon;
                        child.Save();
                    }

                    taxon.Save();
                    //           oldfield.Delete();
                }
            }
        }

        private void simpleAction_FindDuplicates_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Session currentSession = ((XPObjectSpace)View.ObjectSpace).Session;
            Session newSession = new Session(currentSession.DataLayer);
            SelectedData data = null;
            if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(Unit)))
            {
                data = newSession.ExecuteQuery(@"
select 
UnitID,Count(*)
from Unit where GCRecord is null
Group By UnitID
having Count(*) > 1
");
            }
            else
            {
                string objtype = View.ObjectTypeInfo.Name;
                data = newSession.ExecuteQuery(string.Format("select Name,Count(*) from {0} where GCRecord is null Group By Name having Count(*) > 1", objtype));
            }
            CriteriaOperator filter = null;
            foreach (SelectStatementResult item in data.ResultSet)
            {
                foreach (SelectStatementResultRow row in item.Rows)
                {
                    var value = row.Values[0];
                    if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(Unit)))
                    {
                        if (ReferenceEquals(filter, null))
                            filter = new BinaryOperator("UnitID", value);
                        else
                            filter = CriteriaOperator.Or(filter, new BinaryOperator("UnitID", value));
                    }
                    else
                    {
                        if (ReferenceEquals(filter, null))
                            filter = new BinaryOperator("Name", value);
                        else
                            filter = CriteriaOperator.Or(filter, new BinaryOperator("Name", value));
                    }
                }
            }
            ((ListView)View).CollectionSource.Criteria[string.Empty] = filter;
        }
        /*      private void MergeCustomField(IName obj, IObjectSpace os)
              {
                  CustomField field = os.FindObject<CustomField>(new BinaryOperator("Oid", obj.Oid));
                  foreach (CustomField oldfield in View.SelectedObjects)
                  {
                      if (oldfield.Oid != field.Oid)
                      {
                          foreach (CustomValue val in oldfield.Values)
                          {
                              val.Category = field;
                              val.Save();
                          }
                          //todo
                         /* if (field.NoteSet == null)
                              field.NoteSet = os.CreateObject<NoteSet>();
                          if (oldfield.NoteSet != null)
                          {
                              foreach (Note note in oldfield.NoteSet.Notes)
                              { field.NoteSet.Notes.Add(note); }
                          }
                          if (oldfield.FileSet != null)
                          {
                              foreach (FileItem file in oldfield.FileSet.Files)
                              { field.FileSet.Files.Add(file); }
                          }*/
        // field.Save();
        //      oldfield.Delete();
        //    }
        //}

        //}
    }
}
