using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.SystemModule;

namespace EarthCape.Module.Core
{
    public partial class SelectUnitCriteria_ViewController : ViewController
    {
        public SelectUnitCriteria_ViewController()
        {
            //InitializeComponent();
            //RegisterActions(components);
        }

        private void popupWindowShowAction_SelectCriteria_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            CriteriaOperator cr = ((SearchView)e.PopupWindow.View.CurrentObject).Criteria;
            ((ListView)View).CollectionSource.Criteria["Filter"] = cr;
            RefreshController refreshController = Frame.GetController<RefreshController>();
            if (refreshController != null)
            {
                if (refreshController.RefreshAction.Active.ResultValue == true)
                    refreshController.RefreshAction.DoExecute();
            }
        }

        private void popupWindowShowAction_SelectCriteria_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            SearchView obj = os.CreateObject<SearchView>();
            obj.DataType = View.ObjectTypeInfo.Type;
            DetailView view = Application.CreateDetailView(os,obj);
            e.View = view;
   
        }
    }
}
