using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Logistics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using TatukGIS.NDK;

namespace EarthCape.Module.Core
{
    public partial class UnitRoutines_ViewController : ViewController
    {
        public UnitRoutines_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
             this.TargetObjectType = typeof(Unit);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            bool active=SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write));
            if ((this.simpleAction_ConvertStringCoordinates.Active) && (View.ObjectTypeInfo != null))
                simpleAction_ConvertStringCoordinates.Active.SetItemValue("SecurityAllowance", active);
            if ((this.simpleAction_UpdateInfoUnit.Active) && (View.ObjectTypeInfo != null))
                simpleAction_UpdateInfoUnit.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_AddTask.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_AddTask.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_AddUnit.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_AddUnit.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_DeriveUnits.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_DeriveUnits.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_MoveToAccession.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_MoveToAccession.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_Rename.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_Rename.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_SplitAccession.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_SplitAccession.Active.SetItemValue("SecurityAllowance", active);
            if ((this.popupWindowShowAction_StockTake.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_StockTake.Active.SetItemValue("SecurityAllowance", active);
          }

        private void popupWindowShowAction_Rename_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            // CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
            e.View = view;

        }

        private void popupWindowShowAction_Rename_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            foreach (Unit item in View.SelectedObjects)
            {
                item.TaxonomicName = ((XPObjectSpace)View.ObjectSpace).FindObject<TaxonomicName>(new BinaryOperator("Oid", ((TaxonomicName)e.PopupWindow.View.CurrentObject).Oid));
                item.Save();
            }
            ((XPObjectSpace)View.ObjectSpace).CommitChanges();
            RefreshController refreshController = Frame.GetController<RefreshController>();
            if (refreshController != null)
            {
                if (refreshController.RefreshAction.Active.ResultValue == true)
                    refreshController.RefreshAction.DoExecute();
            }
        }
        private void popupWindowShowAction_AddUnit_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = true;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
            e.View = view;

        }

        private void popupWindowShowAction_AddUnit_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            //     e.CanCloseWindow = false;
            TaxonomicName name = (TaxonomicName)e.PopupWindow.View.CurrentObject;
            XPObjectSpace os = (XPObjectSpace)e.PopupWindow.View.ObjectSpace;
            Unit unit = os.CreateObject<Unit>();
            unit.TaxonomicName = name;
            unit.Date = DateTime.Now;
            Frame.SetView(Application.CreateDetailView(os, unit, false));
        }

        private void simpleAction_GenerateCentroids_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }

        private void simpleAction_ShowLocalities_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ArrayList locs = new ArrayList();
            foreach (Unit unit in View.SelectedObjects)
            {
                if (unit.Locality != null)
                {
                    if (locs.IndexOf(os.GetKeyValue(unit.Locality)) == -1)
                        locs.Add(os.GetKeyValue(unit.Locality));
                }
            }
            //    loc_source.Criteria["Selected"] = new InOperator("Oid", locs);
            //    Frame.SetView(Application.CreateListView("Locality_ListView", loc_source,true));
            CollectionSource newCollectionSource = new CollectionSource(Application.CreateObjectSpace(), typeof(Locality));
            newCollectionSource.Criteria["SearchResults"] = new InOperator("Oid", locs);
            e.ShowViewParameters.CreatedView = Application.CreateListView("Locality_ListView", newCollectionSource, true); ;
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.Separate;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
        }

        private void simpleAction_ShowTaxa_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ArrayList taxa = new ArrayList();
            CollectionSource taxa_source = new CollectionSource(os, typeof(TaxonomicName));
            foreach (Unit unit in View.SelectedObjects)
            {
                if (unit.TaxonomicName != null)
                    if (taxa.IndexOf(os.GetKeyValue(unit.TaxonomicName)) == -1)
                        taxa.Add(os.GetKeyValue(unit.TaxonomicName));

            }
            //  taxa_source.Criteria["Selected"] = new InOperator("Oid", taxa);
            // Frame.SetView(Application.CreateListView(Application.FindListViewId(typeof(TaxonomicName)), taxa_source, true));

            CollectionSource newCollectionSource = new CollectionSource(Application.CreateObjectSpace(), typeof(TaxonomicName));
            newCollectionSource.Criteria["SearchResults"] = new InOperator("Oid", taxa);
            e.ShowViewParameters.CreatedView = Application.CreateListView("TaxonomicName_ListView", newCollectionSource, true); ;
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.Separate;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
        }

        private void simpleAction_FindDuplicates_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Session currentSession = ((XPObjectSpace)View.ObjectSpace).Session;
            Session newSession = new Session(currentSession.DataLayer);
            SelectedData data = null;
            if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(Unit)))
            {
                data = newSession.ExecuteQuery(@"
select 
UnitID,Count(*)
from Unit
Group By UnitID
having Count(*) > 1
");
            }
            else
            {
                string objtype = View.ObjectTypeInfo.Name;
                data = newSession.ExecuteQuery(string.Format("select Name,Count(*) from {0} Group By Name having Count(*) > 1", objtype));
            }
            CriteriaOperator filter = null;
            foreach (SelectStatementResult item in data.ResultSet)
            {
                foreach (SelectStatementResultRow row in item.Rows)
                {
                    var value = row.Values[0];
                    if (ReferenceEquals(filter, null))
                        filter = new BinaryOperator("Name", value);
                    else
                        filter = CriteriaOperator.Or(filter, new BinaryOperator("Name", value));
                }
            }
            ((ListView)View).CollectionSource.Criteria[string.Empty] = filter;
        }

        private void simpleAction_AccessionEna_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }

        private void SimpleAction_UpdateInfo_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Unit name in View.SelectedObjects)
            {
                name.UpdateInfo();
                name.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void SimpleAction_ConvertStringCoordinates_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Unit unit in View.SelectedObjects)
            {
                int deg = 0;
                int min = 0;
                int sec = 0;
                int frac = 0;
                int sign = 0;
                try
                {
                    TGIS_Utils.GisDecodeLatitude(TGIS_Utils.GisStrToLatitude(unit.LatitudeVerbatim), ref deg, ref min, ref sec, ref frac, ref sign);
                    unit.Latitude = ((double)deg + ((double)min / 60) + (((double)sec + GeneralHelper.FractionToDouble("0." + frac.ToString())) / 3600));
                    unit.LatitudeString = String.Format("{0}�{1}'{2}{4}\" {3}", deg, min, sec, sign > 0 ? "N" : "S", frac > 0 ? "." + frac.ToString() : "");
                }
                catch (Exception)
                {
                    unit.Latitude =0;
                    unit.LatitudeString = "";
                }

                try
                {
                    TGIS_Utils.GisDecodeLongitude(TGIS_Utils.GisStrToLongitude(unit.LongitudeVerbatim), ref deg, ref min, ref sec, ref frac, ref sign);
                    unit.Longitude = ((double)deg + ((double)min / 60) + (((double)sec+GeneralHelper.FractionToDouble("0."+frac.ToString())) / 3600));
                    unit.LongitudeString = String.Format("{0}�{1}'{2}{4}\" {3}", deg, min, sec, sign > 0 ? "E" : "W",frac>0 ? "."+frac.ToString() : "");
                }
                catch (Exception)
                {
                    unit.Longitude = 0;
                    unit.LongitudeString ="";
                }
                unit.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_SplitAccession_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            SplitAccessionOptions options = os.CreateObject<SplitAccessionOptions>();
            DetailView view = Application.CreateDetailView(os, options, false);
            e.View = view;
        }

        public class MyCloner : Cloner
        {
            public override void CopyMemberValue(
                XPMemberInfo memberInfo, IXPSimpleObject sourceObject, IXPSimpleObject targetObject)
            {
                if (!memberInfo.IsAssociation)
                {
                    base.CopyMemberValue(memberInfo, sourceObject, targetObject);
                }
            }
        }

        private void popupWindowShowAction_SplitAccession_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            SplitAccessionOptions options = (SplitAccessionOptions)e.PopupWindowViewCurrentObject;
            Accession accession = null;
            Accession parent = (Accession)(((NestedFrame)Frame).ViewItem.View.CurrentObject);
            if (options.CloneFromCurrent)
            {
                var cloner = new Cloner();
                accession = (Accession)cloner.CloneTo(parent, typeof(Accession));
            }
            else
            {
                accession = ObjectSpace.FindObject<Accession>(new BinaryOperator("Oid", options.Accession.Oid));
            }
            foreach (Unit unit in View.SelectedObjects)
            {
                unit.Accession = accession;
                unit.Save();
            }
        }

        private void popupWindowShowAction_DeriveUnits_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)ObjectSpace);
            XPObjectSpace os = (XPObjectSpace)View.ObjectSpace;
            // IObjectSpace os = Application.CreateObjectSpace();
            CreateDerivedOptions options = ((CreateDerivedOptions)e.PopupWindow.View.CurrentObject);
            // int pos = 0;

            ArrayList objs = new ArrayList();
            foreach (object item in View.SelectedObjects)
            {
                objs.Add(os.GetKeyValue(item));

            }
            ArrayList orgs = new ArrayList();
            XPCollection<Unit> coll = new XPCollection<Unit>(((XPObjectSpace)ObjectSpace).Session, new InOperator("Oid", objs));
            coll.Sorting = new SortingCollection(coll, new SortProperty("UnitID", SortingDirection.Ascending));
            coll.Load();

            int objects = View.SelectedObjects.Count + options.DerivedPerUnit;
            int totalcount = 0;
            foreach (Unit unit in coll)
            {
                int c = 0;
                for (int i = 1; i <= options.DerivedPerUnit; i++)
                {
                    totalcount++;
                    c += 1;
                    string unitid = unit.UnitID + options.Suffix + c;
                    Unit derived = os.CreateObject<Unit>();
                    while (os.FindObject<Unit>(new BinaryOperator("UnitID", unitid)) != null)
                    {
                        unitid = unit.UnitID + options.Suffix + c++;
                    }
                    derived.DerivedFrom = unit;
                    if (!String.IsNullOrEmpty(unit.UnitID))
                    {
                        derived.UnitID = unitid;
                    }
                    else
                    {
                        derived.UnitID = String.Format("{0:yyMMdd}{1}{2}", unit.Oid, options.Suffix, i);
                    }
                    if (options.Store != null)
                        derived.Store = os.FindObject<Store>(new BinaryOperator("Oid", options.Store.Oid));
                    if (options.Accession != null)
                        derived.Accession = os.FindObject<Accession>(new BinaryOperator("Oid", options.Accession.Oid));
                    derived.Save();
                   
                }
            }
            // CollectionSource ds = new CollectionSource(os, View.ObjectTypeInfo.Type);
            //   e.PopupWindow.SetView(Application.CreateListView(Application.FindListViewId(View.ObjectTypeInfo.Type), ds, false));
            os.CommitChanges();
            MessageOptions options1 = new MessageOptions();
            options1.Duration = 5000;
            options1.Message = string.Format("{0} derived units created", totalcount);
            options1.Type = InformationType.Info;
            options1.Web.Position = InformationPosition.Top;
            options1.Win.Caption = "Units created";
            options1.Win.Type = WinMessageType.Toast;
            Application.ShowViewStrategy.ShowMessage(options1);
        }

        private void popupWindowShowAction_DeriveUnits_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            CreateDerivedOptions obj = os.CreateObject<CreateDerivedOptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }

        private void popupWindowShowAction_MoveToAccession_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            MoveToAccessionOptions obj = os.CreateObject<MoveToAccessionOptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }

        private void popupWindowShowAction_MoveToAccession_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            MoveToAccessionOptions options = (MoveToAccessionOptions)e.PopupWindowViewCurrentObject;
            XPObjectSpace os = (XPObjectSpace)View.ObjectSpace;
            foreach (Unit unit in View.SelectedObjects)
            {
                unit.Accession = os.FindObject<Accession>(new BinaryOperator("Oid",options.Accession.Oid));
                unit.Save();
            }
        }

        private void popupWindowShowAction_AddTask_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)View.ObjectSpace;
            UnitTask tem = (UnitTask)e.PopupWindowViewCurrentObject;
            //  ((UnitTask)e.PopupWindowViewCurrentObject).Save();
            //e.PopupWindowView.ObjectSpace.CommitChanges();
            //UnitTask task = os.FindObject<UnitTask>(new BinaryOperator("Oid", ((UnitTask)e.PopupWindowViewCurrentObject).Oid));
            foreach (Unit unit in View.SelectedObjects)
            {
                UnitTask task = os.CreateObject<UnitTask>();
                task.Unit = os.FindObject<Unit>(new BinaryOperator("Oid", unit.Oid));
                task.Status = tem.Status;
                task.StartDate = tem.StartDate;
                task.DueDate = tem.DueDate;
                task.Description = tem.Description;
                task.PercentCompleted = tem.PercentCompleted;
                if (tem.AssignedTo != null)
                    task.AssignedTo = os.FindObject<PersonCore>(new BinaryOperator("Oid", tem.AssignedTo.Oid));
                task.Save();
            }
            os.CommitChanges();
        }

        private void popupWindowShowAction_AddTask_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            UnitTask obj = os.CreateObject<UnitTask>();
            DetailView dv = Application.CreateDetailView(os,"UnitTask_DetailView_Bulk",true, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = false;
            e.View = dv;

        }

        private void popupWindowShowAction_StockTake_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
          /*  IObjectSpace os = Application.CreateObjectSpace();
            StocktakeOptions options = os.CreateObject<StocktakeOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
            */
        }

        private void popupWindowShowAction_StockTake_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)View.ObjectSpace;
        /*    StocktakeOptions options=((StocktakeOptions)e.PopupWindowViewCurrentObject);
            foreach (Unit unit in View.SelectedObjects)
            {
                StocktakeUnit take = os.CreateObject<StocktakeUnit>();
                take.Done = true;
                take.Unit = unit;
                if (options.Count != null)
                    take.Count = options.Count.Value;
                if (options.Store != null)
                    take.Store = os.FindObject<Store>(new BinaryOperator("Oid", options.Store.Oid));
                if (!String.IsNullOrEmpty(options.Comment))
                    take.Comment = options.Comment;
                take.Save();
            }
            os.CommitChanges();*/
        }
    }
}
