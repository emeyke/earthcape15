EarthCape.Module.Core
{
    partial class DeriveSamples_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupAction_Derive = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupAction_Derive
            // 
            this.popupAction_Derive.AcceptButtonCaption = null;
            this.popupAction_Derive.CancelButtonCaption = null;
            this.popupAction_Derive.Caption = "Derive samples";
            this.popupAction_Derive.Category = "RecordEdit";
            this.popupAction_Derive.Id = "simpleAction_Derive";
            this.popupAction_Derive.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupAction_Derive.Tag = null;
            this.popupAction_Derive.TargetObjectType = typeof(EarthCape.Module.Unit);
            this.popupAction_Derive.ToolTip = "Creates derived units for selected units with configurables parameters";
            this.popupAction_Derive.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupAction_Derive_Execute);
            this.popupAction_Derive.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupAction_Derive_CustomizePopupWindowParams);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupAction_Derive;
    }
}
