using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;

namespace EarthCape.Module.Core
{
    public partial class GroupFilter_ViewController : ViewController
    {
        public GroupFilter_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.Activated += new EventHandler(GroupFilter_ViewController_Activated);
            //this.Deactivating += new EventHandler(GroupFilter_ViewController_Deactivating);
        }

       void GroupFilter_ViewController_Activated(object sender, EventArgs e)
        {
            if (View.CurrentObject == null) return;
            this.simpleAction_GroupFilter.Active.SetItemValue("", true);
            if ((View != null) && (View.IsRoot))
            {
                if (View is ListView)
                {
                    if (!typeof(Project).IsAssignableFrom(((ListView)View).CollectionSource.ObjectTypeInfo.Type))
                    {
                        this.simpleAction_GroupFilter.Active.SetItemValue("", false);
                        return;
                    }
                }
                if (View is DetailView)
                {
                    if (!typeof(Project).IsAssignableFrom(((DetailView)View).CurrentObject.GetType()))
                    {
                        this.simpleAction_GroupFilter.Active.SetItemValue("", false);
                        return;
                    }
                   // ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
                   // SecurityUser u = (os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as SecurityUser);
                    if (Value_WindowController.Instance()!=null)
                    if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Project))&&(typeof(BaseObject).IsAssignableFrom(View.ObjectTypeInfo.Type)))
                    {
                        this.simpleAction_GroupFilter.Active.SetItemValue("",
                           !((Value_WindowController.Instance().Project == ((BaseObject)(((DetailView)View).CurrentObject)).Oid.ToString()) ||
                       ((View.ObjectSpace).IsNewObject((View as DetailView).CurrentObject))));
                    }
                    else
                    {
                        this.simpleAction_GroupFilter.Active.SetItemValue("", false);
                    }

                    /* if ((u.MasterObject.Oid == ((MasterObject)(((DetailView)View).CurrentObject)).Oid) &&
                         (!((ObjectSpace)View.ObjectSpace).IsNewObject((View as DetailView).CurrentObject)))
                     {
                         if (((DetailView)View).CurrentObject is Group)
                         {
                             ViewEditMode vm = ((DetailView)View).ViewEditMode;
                             DictionaryNode node = Application.Info.GetChildNode("Views").FindChildNode("ID", "GroupMaster_DetailView");
                             View.SetInfo(node);
                             DetailView v = Application.CreateDetailView(os, u, true);
                             Frame.SetView(v);
                             ((DetailView)View).ViewEditMode = vm;
                         }
                     }*/
                }
            }
        }

    
        private void simpleAction_GroupFilter_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = View.ObjectSpace;
            IObjectSpace os1 = Application.CreateObjectSpace();
          //  SecurityUser u = (os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as SecurityUser);
            Project Project=((Project)(View.CurrentObject));
            Value_WindowController.Instance().Project = Project.Oid.ToString();
           // GroupFilterSetRemove_ViewController  SetGroupFilterController = Frame.GetController<GroupFilterSetRemove_ViewController>();
            //GeneralHelper.UpdateAppPanel(Project,View);
            // if (Project.DefaultCustomization != null)
           // {
               // GeneralHelperWin.ReloadModel(Project.DefaultCustomization, (WinApplication)Application);
               // ((WinApplication)Application).Restart();
              //  ((WinApplication)Application).EditModel();
                //if (!((WinApplication)Application).ShowViewStrategy.CloseAllWindows())
                  //   ((WinApplication)Application).ShowViewStrategy = ((WinApplication)Application).CreateShowViewStrategy();
                //    ((WinApplication)Application).ShowViewStrategy.ShowStartupWindow();
               
            //// }
            //SetGroupFilterController.popupWindowShowAction_SetGroupFilter.Caption = ((Group)(View.CurrentObject)).Name.ToUpper();
            //View dv = View;
            /*Group obj = os1.FindObject<Group>(new BinaryOperator("Oid", ((Group)(View.CurrentObject)).Oid));
            dv = Application.CreateDetailView(os1, "Group_DetailView", true, obj);
            if (dv != null)*/
              //  e.ShowViewParameters.CreatedView = dv;
            RefreshController refreshController = Frame.GetController<RefreshController>();
            if (refreshController != null)
            {
                if (refreshController.RefreshAction.Active.ResultValue == true)
                    refreshController.RefreshAction.DoExecute();
            }
            /* IWindowTemplate rootFrame = ((Control)View.Control).Page as IWindowTemplate;
                   if (rootFrame != null)
                   {
                       DetailView dv = null;
                       if ((u.MasterObject is SecurityUser))
                       {
                           SecurityUser obj = os1.FindObject<SecurityUser>(new BinaryOperator("Oid", ((SecurityUser)(View.CurrentObject)).Oid));
                           if (u.Oid == obj.Oid)
                               dv = Application.CreateDetailView(os1, "User_Home_Own_DetailView", true, obj);
                           else
                               dv = Application.CreateDetailView(os1, "User_Home_DetailView", true, obj);
                       }
                       if ((u.MasterObject is Group))
                       {
                           Group obj = os1.FindObject<Group>(new BinaryOperator("Oid", ((Group)(View.CurrentObject)).Oid));
                           if (Routines.UserIsGroupMember(u.Oid, obj.Oid, os))

                               dv = Application.CreateDetailView(os1, "Group_Home_Own_DetailView", true, obj);
                           else
                               dv = Application.CreateDetailView(os1, "User_Home_DetailView", true, obj);
                       }
                       if (dv != null)
                           e.ShowViewParameters.CreatedView = dv;
                   }*/
        }
    }
}
