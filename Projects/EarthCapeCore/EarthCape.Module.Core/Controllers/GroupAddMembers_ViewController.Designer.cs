namespace EarthCape.Module.Core
{
    partial class GroupAddMembers_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddMembers = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddMembers
            // 
            this.popupWindowShowAction_AddMembers.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddMembers.CancelButtonCaption = null;
            this.popupWindowShowAction_AddMembers.Caption = "Add members";
            this.popupWindowShowAction_AddMembers.Category = "RecordEdit";
            this.popupWindowShowAction_AddMembers.Id = "popupWindowShowAction_AddMembers";
            this.popupWindowShowAction_AddMembers.ImageName = "BO_Department";
            this.popupWindowShowAction_AddMembers.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddMembers.Tag = null;
            this.popupWindowShowAction_AddMembers.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.popupWindowShowAction_AddMembers.TargetObjectType = typeof(EarthCape.Module.Core.Group);
            this.popupWindowShowAction_AddMembers.ToolTip = "Add members to curently selected groups or projects";
            this.popupWindowShowAction_AddMembers.TypeOfView = null;
            this.popupWindowShowAction_AddMembers.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddMembers_Execute);
            this.popupWindowShowAction_AddMembers.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddMembers_CustomizePopupWindowParams);
            // 
            // GroupAddMembers_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Module.Group);

        }

        #endregion

        public DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddMembers;



    }
}
