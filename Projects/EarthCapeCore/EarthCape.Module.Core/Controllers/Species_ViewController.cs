using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Xpo;

using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace EarthCape.Module.Core
{
    public partial class Species_ViewController : ViewController
    {
        public Species_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if ((this.popupWindowShowAction_MergeToSpecies.Active) && (View.ObjectTypeInfo != null))
                popupWindowShowAction_MergeToSpecies.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            if ((this.simpleAction_MergeDuplicates.Active) && (View.ObjectTypeInfo != null))
                simpleAction_MergeDuplicates.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        private void popupWindowShowAction_MergeToSpecies_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Species sp = os.FindObject<Species>(new BinaryOperator("Oid", ((Species)e.PopupWindow.View.CurrentObject).Oid));
            foreach (Species item in View.SelectedObjects)
            {
                Species sp1 = os.FindObject<Species>(new BinaryOperator("Oid", item.Oid));
                Collection<Unit> units = new Collection<Unit>();
                foreach (Unit unit in sp1.Units)
                {
                    units.Add(unit);
                }
                foreach (Unit unit in units)
                {
                    unit.TaxonomicName = sp;
                    unit.Save();
                }
            }
            os.CommitChanges();
        }

        private void popupWindowShowAction_MergeToSpecies_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os, typeof(Species), false);
            e.View = view;

        }
        private static void MoveUnits(Species item, Species item1)
        {
            Collection<Unit> coll = new Collection<Unit>();
            foreach (Unit unit in item1.Units)
            {
                coll.Add(unit);
            }
            foreach (Unit unit1 in coll)
            {
                unit1.TaxonomicName = item;
                unit1.Save();
            }
        }
        /*  
           private static void MoveNotes(Species item, Species item1)
           {
               foreach (SpeciesNote linkedItem in item1.Notes)
               {
                   item.Notes.Add(linkedItem);
               }
           }

           private static void MoveCustomData(Species item, Species item1)
           {
               Collection<CustomValue> coll = new Collection<CustomValue>();
               foreach (CustomValue CustomValue in item1.CustomData)
               {
                   coll.Add(CustomValue);
               }
               foreach (CustomValue CustomValue1 in coll)
               {
                   CustomValue1.Species = item;
                   CustomValue1.Save();
               }
           }
           private static void MoveLinkedItems(Species item, Species item1)
           {

               foreach (FileItem linkedItem in item1.Files)
               {
                   item.Files.Add(linkedItem);
               }
           }*/


        private void simpleAction_MergeDuplicates_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

            XPCollection<Species> coll = new XPCollection<Species>(uow);
            foreach (Species item in coll)
            {
                if (!item.IsDeleted)
                    foreach (Species item1 in View.SelectedObjects)
                    {
                        if (item.FullName == item1.FullName)
                            if (item.Oid != item1.Oid)
                            {
                                if (!item1.IsDeleted)
                                {
                                    MoveUnits(item, item1);
                                    //  MoveSpeciesVisits(item, item1);
                                    // MoveNotes(item, item1);
                                    //  MoveLinkedItems(item, item1);
                                    // MoveCustomData(item, item1);
                                    //MoveTasks(item, item1);
                                    if (!string.IsNullOrEmpty(item1.Comment))
                                        if (!string.IsNullOrEmpty(item.Comment))
                                            item.Comment = String.Format("{0}\r\n{1}", item.Comment, item1.Comment);
                                        else
                                            item.Comment = item1.Comment;

                                    if (item1.Category != null)
                                        if (item.Category == null)
                                            item.Category = item1.Category;

                                    if (item1.ShortName != null)
                                        if (item.ShortName == null)
                                            item.ShortName = item1.ShortName;
                                    item1.Delete();
                                }
                            }
                    }

            }
            uow.CommitChanges();
        }
    }
}
