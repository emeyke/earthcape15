using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.Core
{
    public partial class NewCustomValueFromTagViaUnitBarCode_ViewController : ViewController
    {
        public NewCustomValueFromTagViaUnitBarCode_ViewController()
        {
            ///InitializeComponent();
            //RegisterActions(components);
            //this.Activated += new EventHandler(NewCustomValueFromTagViaUnitBarCode_ViewController_Activated);
        }

        void NewCustomValueFromTagViaUnitBarCode_ViewController_Activated(object sender, EventArgs e)
        {
            parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Active.SetItemValue("", false);
            ListView nestedView = (ListView)View;
            if (nestedView.CollectionSource is PropertyCollectionSource)
            {
               // if (((PropertyCollectionSource)(nestedView.CollectionSource)).MasterObject != null)
                 //   if (typeof(Tag).IsAssignableFrom(((PropertyCollectionSource)(nestedView.CollectionSource)).MasterObject.GetType()))
                    {
                       // parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Active.SetItemValue("", true);
                        //tag = ((PropertyCollectionSource)sender).MasterObject as Tag;
                        PropertyCollectionSource collectionSource = (PropertyCollectionSource)nestedView.CollectionSource;
                        Type masterObjectType = collectionSource.MasterObjectType;
                        collectionSource.MasterObjectChanged += new EventHandler(collectionSource_MasterObjectChanged);
                    }
            }
        }
        CustomField tag = null;
        private void parametrizedAction_NewCustomValueFromTagViaUnitBarCode_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            XPObjectSpace objectSpace = (XPObjectSpace)Application.CreateObjectSpace();
            string paramValue = e.ParameterCurrentValue as string;
            if (!string.IsNullOrEmpty(paramValue))
            {
                paramValue = "%" + paramValue + "%";
            }

            Unit unit = (Unit)objectSpace.FindObject(typeof(Unit),
                new BinaryOperator("UnitID", paramValue, BinaryOperatorType.Like));
            if (unit != null)
            {
                CustomValueType tag_val_type = tag.ValueType;
                CustomValue CustomValue = null;
              
                if (tag.ValueType == CustomValueType.String)
                    CustomValue = (CustomValueString)objectSpace.CreateObject(typeof(CustomValueString));
                if (tag.ValueType == CustomValueType.Number)
                    CustomValue = (CustomValueNumber)objectSpace.CreateObject(typeof(CustomValueNumber));
                if (tag.ValueType == CustomValueType.Boolean)
                    CustomValue = (CustomValueBoolean)objectSpace.CreateObject(typeof(CustomValueBoolean));
                if (tag.ValueType == CustomValueType.DateTime)
                    CustomValue = (CustomValueDateTime)objectSpace.CreateObject(typeof(CustomValueDateTime));
               //  if (tag.ValueType == CustomValueType.Term)
                 //   CustomValue = (CustomValueTerm)objectSpace.CreateObject(typeof(CustomValueTerm));
                if (tag.ValueType == CustomValueType.Text)
                    CustomValue = (CustomValueText)objectSpace.CreateObject(typeof(CustomValueText));
               // if (tag.ValueType == CustomValueType.StringList)
                 ///   CustomValue = (CustomValueStringList)objectSpace.CreateObject(typeof(CustomValueStringList));
                //if (tag.ValueType == CustomValueType.Event)
                  //  CustomValue = (CustomValueEvent)objectSpace.CreateObject(typeof(CustomValueEvent));
                if (CustomValue == null) return;
                CustomField tag1 = (CustomField)objectSpace.FindObject(typeof(CustomField), new BinaryOperator("Oid", tag.Oid));
              //  CustomValue.ObjectTagged = unit;
                CustomValue.Category = tag1;
                e.ShowViewParameters.CreatedView = Application.CreateDetailView(objectSpace, CustomValue);
            }

        }

          private void collectionSource_MasterObjectChanged(object sender, EventArgs e)
        {
            if (((PropertyCollectionSource)sender).MasterObject != null)
            {
                if (typeof(CustomField).IsAssignableFrom(((PropertyCollectionSource)sender).MasterObject.GetType()))
                {
                    tag = ((PropertyCollectionSource)sender).MasterObject as CustomField;
                    parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Active.SetItemValue("", true);
                   
                }
            }
        }
    }
}
