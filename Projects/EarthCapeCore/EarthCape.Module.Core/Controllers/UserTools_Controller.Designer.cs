namespace EarthCape.Module.Core
{
    partial class UserTools_Controller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AssignRolesToUser = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AssignRolesToUser
            // 
            this.popupWindowShowAction_AssignRolesToUser.AcceptButtonCaption = null;
            this.popupWindowShowAction_AssignRolesToUser.CancelButtonCaption = null;
            this.popupWindowShowAction_AssignRolesToUser.Caption = "Assign roles";
            this.popupWindowShowAction_AssignRolesToUser.Category = "View";
            this.popupWindowShowAction_AssignRolesToUser.ConfirmationMessage = null;
            this.popupWindowShowAction_AssignRolesToUser.Id = "popupWindowShowAction_AssignRolesToUser";
            this.popupWindowShowAction_AssignRolesToUser.ImageName = "BO_Role";
            this.popupWindowShowAction_AssignRolesToUser.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AssignRolesToUser.Shortcut = null;
            this.popupWindowShowAction_AssignRolesToUser.Tag = null;
            this.popupWindowShowAction_AssignRolesToUser.TargetObjectsCriteria = null;
            this.popupWindowShowAction_AssignRolesToUser.TargetViewId = null;
            this.popupWindowShowAction_AssignRolesToUser.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_AssignRolesToUser.ToolTip = null;
            this.popupWindowShowAction_AssignRolesToUser.TypeOfView = null;
            this.popupWindowShowAction_AssignRolesToUser.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AssignRolesToUser_CustomizePopupWindowParams);
            this.popupWindowShowAction_AssignRolesToUser.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AssignRolesToUser_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AssignRolesToUser;
    }
}
