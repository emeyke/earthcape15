namespace EarthCape.Module.Core
{
    partial class TaxonomicTools_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddToClassification = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ShowUnitsForNames = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_AddToClassification
            // 
            this.popupWindowShowAction_AddToClassification.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddToClassification.CancelButtonCaption = null;
            this.popupWindowShowAction_AddToClassification.Caption = "Add to classification";
            this.popupWindowShowAction_AddToClassification.Category = "temp";
            this.popupWindowShowAction_AddToClassification.Id = "popupWindowShowAction_AddToClassification";
            this.popupWindowShowAction_AddToClassification.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddToClassification.Tag = null;
            this.popupWindowShowAction_AddToClassification.TargetObjectType = typeof(EarthCape.Module.Core.TaxonomicName);
            this.popupWindowShowAction_AddToClassification.TypeOfView = null;
            this.popupWindowShowAction_AddToClassification.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToClassification_Execute);
            this.popupWindowShowAction_AddToClassification.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddToClassification_CustomizePopupWindowParams);
            // 
            // simpleAction_ShowUnitsForNames
            // 
            this.simpleAction_ShowUnitsForNames.Caption = "Show units";
            this.simpleAction_ShowUnitsForNames.Id = "simpleAction_ShowUnitsForNames";
            this.simpleAction_ShowUnitsForNames.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ShowUnitsForNames.Tag = null;
            this.simpleAction_ShowUnitsForNames.TargetObjectType = typeof(EarthCape.Module.Core.TaxonomicName);
            this.simpleAction_ShowUnitsForNames.TypeOfView = null;
            this.simpleAction_ShowUnitsForNames.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowUnitsForNames_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddToClassification;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowUnitsForNames;
    }
}
