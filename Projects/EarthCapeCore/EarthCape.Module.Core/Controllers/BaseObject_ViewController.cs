using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;

using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace EarthCape.Module.Core
{
    public partial class BaseObject_ViewController : ViewController
    {
        public BaseObject_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            ViewControlsCreated += Import_ViewController_ViewControlsCreated;
        }

        private void Import_ViewController_ViewControlsCreated(object sender, EventArgs e)
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing))
            {
                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
            }
            if (View.ObjectTypeInfo != null)
            {
                if (popupWindowShowAction_MoveToDataset.Active)
                    popupWindowShowAction_MoveToDataset.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                    new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
                if (popupWindowShowAction_MoveToProject.Active)
                    popupWindowShowAction_MoveToProject.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(
                            new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }


        private void popupWindowShowAction_MoveToProject_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Project name = View.ObjectSpace.FindObject<Project>(new BinaryOperator("Oid", ((Project)(e.PopupWindow.View.CurrentObject)).Oid));

            foreach (IProjectObject item in View.SelectedObjects)
            {
                item.Project = name;
                ((XPCustomObject)item).Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_MoveToProject_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(Project), false);
            e.View = view;

        }

        private void popupWindowShowAction_MoveToDataset_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(Dataset), false);
            e.View = view;

        }

        private void popupWindowShowAction_MoveToDataset_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Dataset name = View.ObjectSpace.FindObject<Dataset>(new BinaryOperator("Oid", ((Dataset)(e.PopupWindow.View.CurrentObject)).Oid));

            foreach (IDatasetObject item in View.SelectedObjects)
            {
                item.Dataset = name;
                ((XPCustomObject)item).Save();
            }
            View.ObjectSpace.CommitChanges();

        }
    }
}