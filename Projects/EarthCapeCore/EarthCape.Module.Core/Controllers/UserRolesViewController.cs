using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Xpand.ExpressApp.Security.Core;

namespace EarthCape.Module.Core
{
    public partial class UserRolesViewController : ViewController
    {
        public UserRolesViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.Activated += UserRolesViewController_Activated;
            this.TargetViewType = ViewType.DetailView;
            TargetObjectType=typeof(User);
        }
        List<Role> addedRolesList=null;
        void UserRolesViewController_Activated(object sender, EventArgs e)
        {
            addedRolesList=new List<Role>();
            ((User)(View.CurrentObject)).Roles.CollectionChanged += Roles_CollectionChanged;
            View.ObjectSpace.Committing += ObjectSpace_Committing;
        }

        void ObjectSpace_Committing(object sender, CancelEventArgs e)
        {
         //   Emailer.SendRoleChanges(((User)(View.CurrentObject)),addedRolesList);
        }

        
      //  todo collect/remove new objects in a list and send only on commit
        void Roles_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
           if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
           {
               Role newRole = (Role)e.ChangedObject;
               if (newRole != null)
               {
                   addedRolesList.Add(newRole);
                   //Emailer.SendRoleAssignedToUser(((User)(View.CurrentObject)), newRole, Emailer.EmailTemplateUserRoleAdded);
               }
           }
        }

       

        void propertyCollectionSource_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
        }
    }
}
