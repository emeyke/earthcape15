﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IRecordSets_ViewController : ViewController
    {
        public IRecordSets_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if ((this.popupWindowShowAction_AddToRecordSets.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_AddToRecordSets.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }          // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_AddToRecordSets_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(RecordSet), false);
            e.View = view;

        }

        private void popupWindowShowAction_AddToRecordSets_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            foreach (RecordSet recset in e.PopupWindow.View.SelectedObjects)
            {
                RecordSet recset1 = View.ObjectSpace.FindObject<RecordSet>(new BinaryOperator("Oid", recset.Oid));

                foreach (IRecordSetsObject item in View.SelectedObjects)
                {
                    if (item is Unit)
                    {
                        recset1.Units.Add((Unit)item);
                    }
                }
                recset1.Save();
            }
            View.ObjectSpace.CommitChanges();


        }
    }
}
