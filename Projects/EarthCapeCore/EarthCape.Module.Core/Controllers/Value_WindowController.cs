using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using System.Web;

namespace EarthCape.Module.Core
{
    public partial class Value_WindowController : WindowController
    {

        private bool _Importing;
        private string _Client;
        public static GetApplicationInstanceCallback GetApplicationInstance;
        public static GetRequestCallback GetRequest;

        public static Value_WindowController Instance()
        {
            if (GetApplicationInstance == null)
            {
                return null;
            }
            else
            {
                XafApplication application = GetApplicationInstance();
                if (application != null)
                    if (application.MainWindow != null)
                        return application.MainWindow.GetController<Value_WindowController>();
                return null;
            }
        }
        public static Value_WindowController Instance(SimpleAction sender)
        {
            if (GetApplicationInstance == null)
            {
                return null;
            }
            else
            {
                XafApplication application = sender.Application;
                if (application != null)
                    if (application.MainWindow != null)
                        return application.MainWindow.GetController<Value_WindowController>();
                return null;
            }
        }
        public delegate XafApplication GetApplicationInstanceCallback();
        public delegate HttpRequest GetRequestCallback();
        public XafApplication GetApp()
        {
            XafApplication application = GetApplicationInstance();
            return application;
        }
        public HttpRequest Request()
        {
            return GetRequest();
        }
        public Project GetGroup(UnitOfWork uow)
        {
            Project Project = null;
            if (!String.IsNullOrEmpty(this.Project))
                Project = uow.FindObject<Project>(new BinaryOperator("Oid", this.Project));
            if (Project == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    Project = uow.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                }

            }
            return Project;
        }
        public Project GetGroup(IObjectSpace os)
        {
            Project Project = null;
            if (!String.IsNullOrEmpty(this.Project))
                Project = os.FindObject<Project>(new BinaryOperator("Oid", this.Project));
            if (Project == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    Project = os.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                    /*if (Project == null)
                    {
                        Project = os.CreateObject<Project>();
                        Project.Name = ConfigurationManager.AppSettings["DefaultProjectName"];
                         Project.Save();
                       // os.CommitChanges();
                    }*/
                }

            }
            return Project;
        }
        public Dataset GetDataset(IObjectSpace os)
        {
            Dataset Dataset = null;
            if (!String.IsNullOrEmpty(this.Dataset))
                Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", this.Dataset));
            if (Dataset == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultDatasetName"]))
                {
                    Dataset = os.FindObject<Dataset>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultDatasetName"]));
                    /* if (Dataset == null) 
                     { 
                         Dataset = os.CreateObject<Dataset>();
                         Dataset.Name = ConfigurationManager.AppSettings["DefaultDatasetName"];
                         Dataset.Project = this.GetGroup(os);
                         Dataset.Save();
                         os.CommitChanges();
                     }*/
                }

            }
            return Dataset;
        }
        public Project GetGroup(Session se)
        {
            Project Project = null;
            if (!String.IsNullOrEmpty(this.Project))
            {
                Project = se.FindObject<Project>(PersistentCriteriaEvaluationBehavior.BeforeTransaction, new BinaryOperator("Oid", this.Project));
                if (Project == null)
                    Project = se.FindObject<Project>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Oid", this.Project));
            }

            if (Project == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    se.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                }

            }
            return Project;
        }
        private string _Project;
        public string Project
        {
            get
            {
                return _Project;
            }
            set
            {
                _Project = value;
            }
        }

        public string Client
        {
            get
            {
                return _Client;
            }
            set
            {
                _Client = value;
            }
        }

        public bool Importing
        {
            get
            {
                return _Importing;
            }
            set
            {
                _Importing = value;
            }
        }
        private bool _Doc = false;
        public bool Doc
        {
            get
            {
                return _Doc;
            }
            set
            {
                _Doc = value;
            }
        }
        private string _Dataset;
        public string Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                _Dataset = value;
            }
        }


    }
}
