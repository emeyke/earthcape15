namespace EarthCape.Module.Std
{
    partial class Locality_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.parametrizedAction_FindLocalityByName = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.simpleAction_MergeDuplicateLocalitiesByName = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // parametrizedAction_FindLocalityByName
            // 
            this.parametrizedAction_FindLocalityByName.Caption = " ";
            this.parametrizedAction_FindLocalityByName.Category = "";
            this.parametrizedAction_FindLocalityByName.ConfirmationMessage = null;
            this.parametrizedAction_FindLocalityByName.Id = "parametrizedAction_FindByName";
            this.parametrizedAction_FindLocalityByName.ImageName = null;
            this.parametrizedAction_FindLocalityByName.NullValuePrompt = null;
            this.parametrizedAction_FindLocalityByName.ShortCaption = "Find locality";
            this.parametrizedAction_FindLocalityByName.Shortcut = null;
            this.parametrizedAction_FindLocalityByName.Tag = null;
            this.parametrizedAction_FindLocalityByName.TargetObjectsCriteria = null;
            this.parametrizedAction_FindLocalityByName.TargetViewId = null;
            this.parametrizedAction_FindLocalityByName.ToolTip = null;
            this.parametrizedAction_FindLocalityByName.TypeOfView = null;
            this.parametrizedAction_FindLocalityByName.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.parametrizedAction_FindByName_Execute);
            // 
            // simpleAction_MergeDuplicateLocalitiesByName
            // 
            this.simpleAction_MergeDuplicateLocalitiesByName.Caption = "Merge duplicates";
            this.simpleAction_MergeDuplicateLocalitiesByName.Category = "Tools";
            this.simpleAction_MergeDuplicateLocalitiesByName.ConfirmationMessage = "This will merge all the duplicate localities to the first one of them and deletes" +
    " the duplicate records. Proceed?";
            this.simpleAction_MergeDuplicateLocalitiesByName.Id = "simpleAction_MergeDuplicateLocalitiesByName";
            this.simpleAction_MergeDuplicateLocalitiesByName.ImageName = null;
            this.simpleAction_MergeDuplicateLocalitiesByName.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_MergeDuplicateLocalitiesByName.Shortcut = null;
            this.simpleAction_MergeDuplicateLocalitiesByName.Tag = null;
            this.simpleAction_MergeDuplicateLocalitiesByName.TargetObjectsCriteria = null;
            this.simpleAction_MergeDuplicateLocalitiesByName.TargetObjectType = typeof(EarthCape.Module.Core.Locality);
            this.simpleAction_MergeDuplicateLocalitiesByName.TargetViewId = null;
            this.simpleAction_MergeDuplicateLocalitiesByName.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_MergeDuplicateLocalitiesByName.ToolTip = "Merges all the duplicate localities to the first one of them";
            this.simpleAction_MergeDuplicateLocalitiesByName.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_MergeDuplicateLocalitiesByName.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_MergeDuplicateLocalitiesByName_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_FindLocalityByName;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MergeDuplicateLocalitiesByName;
    }
}
