using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Person_ViewController : ViewController
    {
        public Person_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if ((this.popupWindowShowAction_AddToProjectContacts.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_AddToProjectContacts.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }          // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_AddToProjectContacts_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(Project), false);
            e.View = view;

        }

        private void popupWindowShowAction_AddToProjectContacts_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            foreach (PersonCore per in View.SelectedObjects)
            {
                foreach (Project prj in e.PopupWindowViewSelectedObjects)
                {
                    ProjectContact pc = os.FindObject<ProjectContact>(CriteriaOperator.And(new BinaryOperator("Project.Oid", prj.Oid), new BinaryOperator("Person.Oid", per.Oid)));
                    if (pc == null)
                    {
                        pc = os.CreateObject<ProjectContact>();
                        pc.Project = os.FindObject<Project>(new BinaryOperator("Oid", prj.Oid));
                        pc.Person = os.FindObject<PersonCore>(new BinaryOperator("Oid", per.Oid));
                        pc.Save();
                    }
                }
            }
            os.CommitChanges();

        }
    }
}
