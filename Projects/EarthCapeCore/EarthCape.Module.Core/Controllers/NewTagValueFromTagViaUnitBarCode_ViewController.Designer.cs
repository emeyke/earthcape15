namespace EarthCape.Module.Core
{
    partial class NewCustomValueFromTagViaUnitBarCode_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // parametrizedAction_NewCustomValueFromTagViaUnitBarCode
            // 
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Caption = "BARCODE:";
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Category = "ObjectsCreation";
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Id = "parametrizedAction_NewCustomValueFromTagViaUnitBarCode";
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Tag = null;
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.TypeOfView = null;
            this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.parametrizedAction_NewCustomValueFromTagViaUnitBarCode_Execute);
            // 
            // NewCustomValueFromTagViaUnitBarCode_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Module.Core.CustomValue);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_NewCustomValueFromTagViaUnitBarCode;
    }
}
