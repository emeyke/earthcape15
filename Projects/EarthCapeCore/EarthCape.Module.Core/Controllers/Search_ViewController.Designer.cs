﻿namespace EarthCape.Module.Core.Controllers
{
    partial class Search_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SimpleAction_Search = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SimpleAction_Search
            // 
            this.SimpleAction_Search.Caption = "Search";
            this.SimpleAction_Search.ConfirmationMessage = null;
            this.SimpleAction_Search.Id = "SimpleAction_Search";
            this.SimpleAction_Search.ToolTip = null;
            this.SimpleAction_Search.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_Search_Execute);
            // 
            // Search_ViewController
            // 
            this.Actions.Add(this.SimpleAction_Search);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SimpleAction_Search;
    }
}
