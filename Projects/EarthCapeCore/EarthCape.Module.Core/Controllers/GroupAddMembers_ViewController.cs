using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Data.Filtering;
using EarthCape.Module;
using DevExpress.ExpressApp.SystemModule;

namespace EarthCape.Module.Core
{
    public partial class GroupAddMembers_ViewController : ViewController
    {
        public GroupAddMembers_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

 
        private void popupWindowShowAction_AddMembers_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            AddMembers(e);
        }

        private void AddMembers(PopupWindowShowActionExecuteEventArgs e)
        {
            if (e.PopupWindow.View.SelectedObjects.Count > 0)
            {
                ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
                if (View is ListView)
                {
                    ListView lv = (ListView)View;
                    foreach (Group grp in lv.SelectedObjects)
                    {
                        foreach (User cusr in e.PopupWindow.View.SelectedObjects)
                        {
                            Guid groupguid = grp.Oid;
                            Guid userguid = cusr.Oid;
                            User usr = os.FindObject<User>(new BinaryOperator("Oid", userguid));
                               AddGroupMember(os, os.FindObject<Group>(new BinaryOperator("Oid", groupguid)), usr);
                        }
                        os.CommitChanges();
                        //SyncHelper.SyncGroup(grp, SyncHelper.MyObjectSpace().Session,new Collection<Guid>());
                    }
                }
                if (View is DetailView)
                {
                    DetailView dv = (DetailView)View;
                    Group grp = (Group)(dv.CurrentObject);
                    foreach (User cusr in e.PopupWindow.View.SelectedObjects)
                    {
                        Guid groupguid = grp.Oid;
                        Guid userguid = cusr.Oid;
                        User usr = os.FindObject<User>(new BinaryOperator("Oid", userguid));
                        /*if (usr == null)
                        {
                            usr = (User)SyncHelper.SyncUser(1,cusr, os,new Collection<Guid>());
                        }*/
                        AddGroupMember(os, os.FindObject<Group>(new BinaryOperator("Oid", groupguid)), usr);
                    }
                    os.CommitChanges();
                }
            }
            
            RefreshController refreshController = Frame.GetController<RefreshController>();
            if (refreshController != null)
            {
                if (refreshController.RefreshAction.Active.ResultValue == true)
                    refreshController.RefreshAction.DoExecute();
            }
        }
        private void AddGroupMember(ObjectSpace os, Group grp, User usr) 
        {
            if ((grp == null) || (usr == null)) return;
            if (os.FindObject<GroupMembership>(CriteriaOperator.And(
                new BinaryOperator("Group.Oid", grp.Oid), 
                new BinaryOperator("Member.Oid", usr.Oid)/*, 
                new BinaryOperator("EndOn",DateTime.MinValue,BinaryOperatorType.Greater)*/)) == null) 
            {
               GroupMembership grm= os.CreateObject<GroupMembership>();
               grm.Group = grp;
               grm.Member = usr;
               grm.Save();
            }
        }
        private void popupWindowShowAction_AddMembers_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            PopupParams(e, os);

        }

        private void PopupParams(CustomizePopupWindowParamsEventArgs e, ObjectSpace os)
        {
             e.DialogController.SaveOnAccept = false;
            ListView view= Application.CreateListView(os,typeof(User),false);
           e.View = view;
        }

        private void popupWindowShowAction_AddMembersFromCloud_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            AddMembers(e);
        }

        private void popupWindowShowAction_AddMembersFromCloud_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
         }
  
      }
}
