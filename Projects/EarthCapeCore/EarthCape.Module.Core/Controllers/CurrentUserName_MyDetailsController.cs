﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Security;

namespace EarthCape.Module.Core
{
    public partial class CurrentUserName_MyDetailsController : MyDetailsController
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            MyDetailsAction.Caption = SecuritySystem.CurrentUserName;
        }
    }
}
