namespace EarthCape.Module.Core
{
    partial class GroupFilterSetRemove_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_SetFilter = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_SetFilter
            // 
            this.simpleAction_SetFilter.Caption = "Set filter";
            this.simpleAction_SetFilter.ConfirmationMessage = null;
            this.simpleAction_SetFilter.Id = "simpleAction_SetFilter";
            this.simpleAction_SetFilter.ImageName = "Action_Filter";
            this.simpleAction_SetFilter.Shortcut = null;
            this.simpleAction_SetFilter.Tag = null;
            this.simpleAction_SetFilter.TargetObjectsCriteria = null;
            this.simpleAction_SetFilter.TargetViewId = null;
            this.simpleAction_SetFilter.ToolTip = null;
            this.simpleAction_SetFilter.TypeOfView = null;
            this.simpleAction_SetFilter.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_SetFilter_Execute);
            // 
            // GroupFilterSetRemove_ViewController
            // 
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_SetFilter;
    }
}
