namespace EarthCape.Module.Core.Controllers
{
    partial class TaxonomicName_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_UpdateInfo = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_UpdateInfo
            // 
            this.simpleAction_UpdateInfo.Caption = "Update info";
            this.simpleAction_UpdateInfo.Category = "Tools";
            this.simpleAction_UpdateInfo.ConfirmationMessage = null;
            this.simpleAction_UpdateInfo.Id = "simpleAction_UpdateInfo";
            this.simpleAction_UpdateInfo.ToolTip = null;
            this.simpleAction_UpdateInfo.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateInfo_Execute);
            // 
            // TaxonomicName_ViewController
            // 
            this.Actions.Add(this.simpleAction_UpdateInfo);
            this.TargetObjectType = typeof(EarthCape.Module.Core.TaxonomicName);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateInfo;
    }
}
