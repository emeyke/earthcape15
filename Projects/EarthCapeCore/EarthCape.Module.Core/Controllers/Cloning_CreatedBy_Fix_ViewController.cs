using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.ExpressApp.CloneObject;
using DevExpress.ExpressApp.Security;


namespace EarthCape.Module.Core.Win
{
    public partial class Cloning_CreatedBy_Fix_ViewController : ViewController 
    {
        public Cloning_CreatedBy_Fix_ViewController()
        {
            InitializeComponent();
           RegisterActions(components);
           Activated += new EventHandler(Cloning_CreatedBy_Fix_ViewController_Activated);
        }

        void Cloning_CreatedBy_Fix_ViewController_Activated(object sender, EventArgs e)
        {
            Frame.GetController<CloneObjectViewController>().CustomShowClonedObject += new EventHandler<CustomShowClonedObjectEventArgs>(Cloning_CreatedBy_Fix_ViewController_CustomShowClonedObject); ;
        }

        void Cloning_CreatedBy_Fix_ViewController_CustomShowClonedObject(object sender, CustomShowClonedObjectEventArgs e)
        {
            /*  Cloner cloner = new Cloner();
              object sourceObject =e.SourceObject;// (IXPSimpleObject)View.CurrentObject;
              object clonedObject =e.ClonedObject;// (IXPSimpleObject)View.CurrentObject;
              XPObjectSpace targetObjectSpace =(XPObjectSpace)e.TargetObjectSpace;*/
            if (typeof(BaseObject).IsAssignableFrom(e.ClonedObject.GetType()))
            {
                if ((SecuritySystem.CurrentUser != null) && (((User)(e.TargetObjectSpace.GetObject(SecuritySystem.CurrentUser)))) != null)
                {
                    User u = (User)(e.TargetObjectSpace.GetObject(SecuritySystem.CurrentUser));
                         (e.ClonedObject as BaseObject).CreatedByUser = SecuritySystem.CurrentUserName;;
                }
                else (e.ClonedObject as BaseObject).CreatedBy = null;

                (e.ClonedObject as BaseObject).CreatedOn = DateTime.Now;
            }
            /*  IXPSimpleObject rootSessionCurrentObject = objectSpace.GetObject(currentObject) as IXPSimpleObject;
              if (rootSessionCurrentObject != null)
              {
                  string viewId = Application.FindDetailViewId(rootSessionCurrentObject.GetType());
                  if (!string.IsNullOrEmpty(viewId))
                  {
                      object clonedObj = cloner.CloneTo(rootSessionCurrentObject,View.ObjectTypeInfo.Type);
                      //>>
                      if (clonedObj is BaseObject)
                      {
                          User u = (User)(objectSpace.GetObject(SecuritySystem.CurrentUser));
                          if (u != null)
                              (clonedObj as BaseObject).CreatedByUser = SecuritySystem.CurrentUserName;;
                          else (clonedObj as BaseObject).CreatedBy = null;

                          (clonedObj as BaseObject).CreatedOn = DateTime.Now;
                      }
                      //>>
                      objectSpace.SetModified(clonedObj);
                      Frame.SetView(Application.CreateDetailView(objectSpace, clonedObj));
                  }
                  else
                  {
                      if (View is ListView)
                      {
                          ((ListView)View).CollectionSource.Add(cloner.CloneTo(currentObject as IXPSimpleObject, View.ObjectTypeInfo.Type));
                      }
                  }
              }*/
        }

        private void simpleAction_Clone_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
       /*     Cloner cloner = new Cloner();
            IXPSimpleObject currentObject = (IXPSimpleObject)View.CurrentObject;
            XPObjectSpace objectSpace = (XPObjectSpace)Application.GetObjectSpaceToShowViewFrom(Frame);
            object clonedObj = cloner.CloneTo(currentObject, View.ObjectTypeInfo.Type);
            e.ShowViewParameters.TargetWindow = TargetWindow.Default;
            e.ShowViewParameters.CreatedView = Application.CreateDetailView(objectSpace,clonedObj);*/
        }
  
    }
}
