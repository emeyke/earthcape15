using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.ExpressApp.SystemModule;
using Xpand.ExpressApp.Security.Core;

namespace EarthCape.Module.Core
{
    public partial class UserTools_Controller : ViewController
    {
        public UserTools_Controller()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(User);
            TargetViewId = "xx";
        }

        private void popupWindowShowAction_AssignRolesToUser_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            List<Role> addedRolesList = new List<Role>();
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
            {
                foreach (User user in View.SelectedObjects)
                {
                    User user1 = uow.FindObject<User>(new BinaryOperator("Oid",user.Oid));
                    foreach (Role role in e.PopupWindow.View.SelectedObjects)
                    {
                        user1.Roles.Add(uow.FindObject<Role>(new BinaryOperator("Oid", role.Oid)));
                        addedRolesList.Add(role);
                    }
                } 
                uow.CommitChanges();
               // Emailer.SendRoleChanges(((User)(View.CurrentObject)), addedRolesList);
            }
            RefreshController contr = Frame.GetController<RefreshController>();
            if (contr != null)
                contr.RefreshAction.DoExecute();
        }

        private void popupWindowShowAction_AssignRolesToUser_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = true;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(Role), false);
            e.View = view;

        }

  
    }
}
