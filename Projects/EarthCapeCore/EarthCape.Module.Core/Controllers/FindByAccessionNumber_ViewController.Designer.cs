namespace EarthCape.Module.Core
{
    partial class FindByUnitID_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.parametrizedAction_FindUnitByUnitID = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // parametrizedAction_FindUnitByUnitID
            // 
            this.parametrizedAction_FindUnitByUnitID.Caption = " ";
            this.parametrizedAction_FindUnitByUnitID.Category = "";
            this.parametrizedAction_FindUnitByUnitID.ConfirmationMessage = null;
            this.parametrizedAction_FindUnitByUnitID.Id = "parametrizedAction_FindUnitByUnitID";
            this.parametrizedAction_FindUnitByUnitID.NullValuePrompt = null;
            this.parametrizedAction_FindUnitByUnitID.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.parametrizedAction_FindUnitByUnitID.ShortCaption = "Find unit";
            this.parametrizedAction_FindUnitByUnitID.TargetViewType = DevExpress.ExpressApp.ViewType.DashboardView;
            this.parametrizedAction_FindUnitByUnitID.ToolTip = "Finds unit by its UnitID and opens the form. Suitable to be used with a barcode s" +
    "canner.";
            this.parametrizedAction_FindUnitByUnitID.TypeOfView = typeof(DevExpress.ExpressApp.DashboardView);
            this.parametrizedAction_FindUnitByUnitID.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.parametrizedAction1_Execute);
            // 
            // FindByUnitID_ViewController
            // 
            this.Actions.Add(this.parametrizedAction_FindUnitByUnitID);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_FindUnitByUnitID;
    }
}
