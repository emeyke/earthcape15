namespace EarthCape.Module.Core
{
    partial class IUnits_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ShowUnits = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ShowUnits
            // 
            this.simpleAction_ShowUnits.Caption = "Show units";
            this.simpleAction_ShowUnits.Category = "RecordEdit";
            this.simpleAction_ShowUnits.ConfirmationMessage = null;
            this.simpleAction_ShowUnits.Id = "simpleAction_ShowUnits";
            this.simpleAction_ShowUnits.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ShowUnits.ToolTip = null;
            this.simpleAction_ShowUnits.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowUnits_Execute);
            // 
            // IUnits_ViewController
            // 
            this.Actions.Add(this.simpleAction_ShowUnits);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowUnits;
    }
}
