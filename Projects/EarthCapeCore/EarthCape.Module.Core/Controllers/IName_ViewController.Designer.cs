namespace EarthCape.Module.Core
{
    partial class IName_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_FilterByNameList = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ShareByList = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_MergeTo = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_FindDuplicates = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_FilterByNameList
            // 
            this.popupWindowShowAction_FilterByNameList.AcceptButtonCaption = null;
            this.popupWindowShowAction_FilterByNameList.CancelButtonCaption = null;
            this.popupWindowShowAction_FilterByNameList.Caption = "Filter list";
            this.popupWindowShowAction_FilterByNameList.Category = "Tools";
            this.popupWindowShowAction_FilterByNameList.ConfirmationMessage = null;
            this.popupWindowShowAction_FilterByNameList.Id = "popupWindowShowAction_FilterByNameList";
            this.popupWindowShowAction_FilterByNameList.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_FilterByNameList.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_FilterByNameList.ToolTip = null;
            this.popupWindowShowAction_FilterByNameList.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_FilterByNameList.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_FilterByNameList_CustomizePopupWindowParams);
            this.popupWindowShowAction_FilterByNameList.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_FilterByNameList_Execute);
            // 
            // popupWindowShowAction_ShareByList
            // 
            this.popupWindowShowAction_ShareByList.AcceptButtonCaption = null;
            this.popupWindowShowAction_ShareByList.CancelButtonCaption = null;
            this.popupWindowShowAction_ShareByList.Caption = "popupWindowShowAction_ShareByList";
            this.popupWindowShowAction_ShareByList.ConfirmationMessage = null;
            this.popupWindowShowAction_ShareByList.Id = "popupWindowShowAction_ShareByList";
            this.popupWindowShowAction_ShareByList.TargetViewId = "xx";
            this.popupWindowShowAction_ShareByList.ToolTip = null;
            // 
            // popupWindowShowAction_MergeTo
            // 
            this.popupWindowShowAction_MergeTo.AcceptButtonCaption = null;
            this.popupWindowShowAction_MergeTo.CancelButtonCaption = null;
            this.popupWindowShowAction_MergeTo.Caption = "Merge to...";
            this.popupWindowShowAction_MergeTo.Category = "Tools";
            this.popupWindowShowAction_MergeTo.ConfirmationMessage = null;
            this.popupWindowShowAction_MergeTo.Id = "popupWindowShowAction_MergeTo";
            this.popupWindowShowAction_MergeTo.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MergeTo.TargetViewId = "CustomField_ListView";
            this.popupWindowShowAction_MergeTo.ToolTip = null;
            this.popupWindowShowAction_MergeTo.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MergeTo_CustomizePopupWindowParams);
            this.popupWindowShowAction_MergeTo.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MergeTo_Execute);
            // 
            // simpleAction_FindDuplicates
            // 
            this.simpleAction_FindDuplicates.Caption = "Find duplicates";
            this.simpleAction_FindDuplicates.Category = "Tools";
            this.simpleAction_FindDuplicates.ConfirmationMessage = null;
            this.simpleAction_FindDuplicates.Id = "simpleAction_FindDuplicates";
            this.simpleAction_FindDuplicates.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_FindDuplicates.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_FindDuplicates.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_FindDuplicates_Execute);
            // 
            // IName_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_FilterByNameList);
            this.Actions.Add(this.popupWindowShowAction_ShareByList);
            this.Actions.Add(this.popupWindowShowAction_MergeTo);
            this.Actions.Add(this.simpleAction_FindDuplicates);
            this.TargetObjectType = typeof(EarthCape.Module.Core.IName);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_FilterByNameList;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ShareByList;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MergeTo;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_FindDuplicates;
    }
}
