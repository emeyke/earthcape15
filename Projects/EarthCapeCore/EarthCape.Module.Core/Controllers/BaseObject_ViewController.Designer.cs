namespace  EarthCape.Module.Core
{
    partial class BaseObject_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_MoveToProject = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_MoveToDataset = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_MoveToProject
            // 
            this.popupWindowShowAction_MoveToProject.AcceptButtonCaption = null;
            this.popupWindowShowAction_MoveToProject.CancelButtonCaption = null;
            this.popupWindowShowAction_MoveToProject.Caption = "Move to project";
            this.popupWindowShowAction_MoveToProject.Category = "Tools";
            this.popupWindowShowAction_MoveToProject.ConfirmationMessage = null;
            this.popupWindowShowAction_MoveToProject.Id = "popupWindowShowAction_MoveToProject";
            this.popupWindowShowAction_MoveToProject.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MoveToProject.TargetObjectType = typeof(EarthCape.Module.Core.IProjectObject);
            this.popupWindowShowAction_MoveToProject.ToolTip = "Move selected records to another project";
            this.popupWindowShowAction_MoveToProject.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MoveToProject_CustomizePopupWindowParams);
            this.popupWindowShowAction_MoveToProject.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MoveToProject_Execute);
            // 
            // popupWindowShowAction_MoveToDataset
            // 
            this.popupWindowShowAction_MoveToDataset.AcceptButtonCaption = null;
            this.popupWindowShowAction_MoveToDataset.CancelButtonCaption = null;
            this.popupWindowShowAction_MoveToDataset.Caption = "Move to dataset";
            this.popupWindowShowAction_MoveToDataset.Category = "Tools";
            this.popupWindowShowAction_MoveToDataset.ConfirmationMessage = null;
            this.popupWindowShowAction_MoveToDataset.Id = "popupWindowShowAction_MoveToDataset";
            this.popupWindowShowAction_MoveToDataset.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MoveToDataset.TargetObjectType = typeof(EarthCape.Module.Core.IDatasetObject);
            this.popupWindowShowAction_MoveToDataset.ToolTip = "Move selected records to another dataset";
            this.popupWindowShowAction_MoveToDataset.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MoveToDataset_CustomizePopupWindowParams);
            this.popupWindowShowAction_MoveToDataset.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MoveToDataset_Execute);
            // 
            // BaseObject_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_MoveToProject);
            this.Actions.Add(this.popupWindowShowAction_MoveToDataset);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MoveToProject;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MoveToDataset;
    }
}