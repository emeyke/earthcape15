namespace EarthCape.Module.Core
{
    partial class SelectUnitCriteria_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_SelectCriteria = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_SelectCriteria
            // 
            this.popupWindowShowAction_SelectCriteria.AcceptButtonCaption = null;
            this.popupWindowShowAction_SelectCriteria.CancelButtonCaption = null;
            this.popupWindowShowAction_SelectCriteria.Caption = "popupWindowShowAction_SelectCriteria";
            this.popupWindowShowAction_SelectCriteria.Category = "Filter";
            this.popupWindowShowAction_SelectCriteria.Id = "popupWindowShowAction_SelectCriteria";
            this.popupWindowShowAction_SelectCriteria.Tag = null;
            this.popupWindowShowAction_SelectCriteria.TypeOfView = null;
            this.popupWindowShowAction_SelectCriteria.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_SelectCriteria_Execute);
            this.popupWindowShowAction_SelectCriteria.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_SelectCriteria_CustomizePopupWindowParams);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_SelectCriteria;
    }
}
