using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class SetFilter_ViewController : ViewController
    {
        public SetFilter_ViewController()
        {
            //InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_SetFilter_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
          //  e.DialogController.SaveOnAccept = false;
            SetFilterParams par = os.CreateObject<SetFilterParams>();
            if (!String.IsNullOrEmpty(Value_WindowController.Instance().Dataset))
            {
                Dataset ds=os.FindObject<Dataset>(new BinaryOperator("Name", Value_WindowController.Instance().Dataset));
                if (ds != null)
                    par.Dataset = ds;
            }
            else
            {
                if (!String.IsNullOrEmpty(Value_WindowController.Instance().Project))
                {
                    Project ds = os.FindObject<Project>(new BinaryOperator("Name", Value_WindowController.Instance().Project));
                    if (ds != null)
                        par.Project = ds;
                }
            }
       DetailView view = Application.CreateDetailView(os, par);
            e.View = view;

        }

        private void popupWindowShowAction_SetFilter_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Dataset ds = ((SetFilterParams)e.PopupWindowViewCurrentObject).Dataset;
            Project prj = ((SetFilterParams)e.PopupWindowViewCurrentObject).Project;
            if (ds != null)
                Value_WindowController.Instance().Dataset = ds.Name;
            else
                Value_WindowController.Instance().Dataset = "";
    
            if (prj != null)
                Value_WindowController.Instance().Project = prj.Name;
            else
                Value_WindowController.Instance().Project = "";
            //((ListView)View).ObjectSpace.ReloadCollection(((ListView)View).CollectionSource.Collection);
            Frame.SetControllersActive("", false);
            Frame.SetControllersActive("", true);
        }
    }
}
