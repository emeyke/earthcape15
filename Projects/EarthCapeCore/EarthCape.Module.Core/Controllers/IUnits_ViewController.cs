using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;

using System;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace EarthCape.Module.Core
{
    public partial class IUnits_ViewController : ViewController
    {
        public IUnits_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(IUnits);
        }

        private void simpleAction_ShowUnits_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ArrayList keys = new ArrayList();
            foreach (IUnits item in View.SelectedObjects)
            {
                //keys.Add(os.GetKeyValue(os.FindObject<BaseObject>(new BinaryOperator("Oid", ((BaseObject)item).Oid))));
                keys.Add(os.GetKeyValue(item));
            }
            CollectionSource cs = new CollectionSource(os, typeof(Unit));
            string filter = string.Empty;
            if (typeof(TaxonomicName).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "TaxonomicName.Oid";
            if (typeof(Locality).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Locality.Oid";
            if (typeof(Store).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Store.Oid";
            if (typeof(Dataset).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Dataset.Oid";
            if (typeof(Brood).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Brood.Oid";
            if (typeof(Genotype).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Genotype.Oid";
            if (typeof(Habitat).IsAssignableFrom(View.ObjectTypeInfo.Type))
                filter = "Habitat.Oid";
            // if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(MaterialTransfer))) todo
            //    filter = "Store.Oid";
            cs.Criteria["Units"] = new InOperator(filter, keys);
            Frame.SetView(Application.CreateListView("Unit_ListView", cs, true));
        }
    }
}
