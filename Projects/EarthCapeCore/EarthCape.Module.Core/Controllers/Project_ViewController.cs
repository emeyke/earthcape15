﻿using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace EarthCape.Module.Core
{
    public partial class Project_ViewController : ViewController
    {
        public Project_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void simpleAction_CreateRoles_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Project project in View.SelectedObjects)
            {
                project.CreateRoles();
            }
            View.ObjectSpace.CommitChanges();
        }
    }
}
