namespace EarthCape.Module.Core.Controllers
{
    partial class Dataset_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_GetContributors = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_UseInProjects = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_DatasetAddContributors = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_GenerateViews = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_GetContributors
            // 
            this.simpleAction_GetContributors.Caption = "Get contributors";
            this.simpleAction_GetContributors.Category = "Tools";
            this.simpleAction_GetContributors.ConfirmationMessage = null;
            this.simpleAction_GetContributors.Id = "simpleAction_GetContributors";
            this.simpleAction_GetContributors.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_GetContributors.ToolTip = null;
            this.simpleAction_GetContributors.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction1_Execute);
            // 
            // popupWindowShowAction_UseInProjects
            // 
            this.popupWindowShowAction_UseInProjects.AcceptButtonCaption = null;
            this.popupWindowShowAction_UseInProjects.CancelButtonCaption = null;
            this.popupWindowShowAction_UseInProjects.Caption = "Use in projects";
            this.popupWindowShowAction_UseInProjects.Category = "Tools";
            this.popupWindowShowAction_UseInProjects.ConfirmationMessage = null;
            this.popupWindowShowAction_UseInProjects.Id = "popupWindowShowAction_UseInProjects";
            this.popupWindowShowAction_UseInProjects.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_UseInProjects.TargetObjectType = typeof(EarthCape.Module.Core.Dataset);
            this.popupWindowShowAction_UseInProjects.ToolTip = "Adds selected datasets to the list of used datasets in selected projects";
            this.popupWindowShowAction_UseInProjects.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_UseInProjects_CustomizePopupWindowParams);
            this.popupWindowShowAction_UseInProjects.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_UseInProjects_Execute);
            // 
            // popupWindowShowAction_DatasetAddContributors
            // 
            this.popupWindowShowAction_DatasetAddContributors.AcceptButtonCaption = null;
            this.popupWindowShowAction_DatasetAddContributors.CancelButtonCaption = null;
            this.popupWindowShowAction_DatasetAddContributors.Caption = "Add contributors";
            this.popupWindowShowAction_DatasetAddContributors.Category = "Tools";
            this.popupWindowShowAction_DatasetAddContributors.ConfirmationMessage = null;
            this.popupWindowShowAction_DatasetAddContributors.Id = "popupWindowShowAction_DatasetAddContributors";
            this.popupWindowShowAction_DatasetAddContributors.ToolTip = null;
            this.popupWindowShowAction_DatasetAddContributors.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_DatasetAddContributors_CustomizePopupWindowParams);
            this.popupWindowShowAction_DatasetAddContributors.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_DatasetAddContributors_Execute);
            // 
            // popupWindowShowAction_GenerateViews
            // 
            this.popupWindowShowAction_GenerateViews.AcceptButtonCaption = null;
            this.popupWindowShowAction_GenerateViews.CancelButtonCaption = null;
            this.popupWindowShowAction_GenerateViews.Caption = "Generate views";
            this.popupWindowShowAction_GenerateViews.Category = "Tools";
            this.popupWindowShowAction_GenerateViews.ConfirmationMessage = null;
            this.popupWindowShowAction_GenerateViews.Id = "popupWindowShowAction_GenerateViews";
            this.popupWindowShowAction_GenerateViews.TargetViewId = "";
            this.popupWindowShowAction_GenerateViews.ToolTip = null;
            this.popupWindowShowAction_GenerateViews.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_GenerateViews_CustomizePopupWindowParams);
            this.popupWindowShowAction_GenerateViews.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_GenerateViews_Execute);
            // 
            // Dataset_ViewController
            // 
            this.Actions.Add(this.simpleAction_GetContributors);
            this.Actions.Add(this.popupWindowShowAction_UseInProjects);
            this.Actions.Add(this.popupWindowShowAction_DatasetAddContributors);
            this.Actions.Add(this.popupWindowShowAction_GenerateViews);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Dataset);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GetContributors;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_UseInProjects;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DatasetAddContributors;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GenerateViews;
    }
}
