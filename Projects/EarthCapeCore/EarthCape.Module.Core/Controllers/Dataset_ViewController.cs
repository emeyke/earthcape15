using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Dataset_ViewController : ViewController
    {
        public Dataset_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if ((this.popupWindowShowAction_UseInProjects.Active) && (View.ObjectTypeInfo != null))
            {
                bool granted = SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write));
                popupWindowShowAction_UseInProjects.Active.SetItemValue("SecurityAllowance", granted);
                popupWindowShowAction_GenerateViews.Active.SetItemValue("SecurityAllowance", granted);
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction1_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ArrayList locs = new ArrayList();
            foreach (Dataset ds in View.SelectedObjects)
            {
                foreach (DatasetContribution dsc in ds.Contributions)
                {


                    if (dsc.Person != null)
                    {
                        if (locs.IndexOf(os.GetKeyValue(dsc.Person)) == -1)
                            locs.Add(os.GetKeyValue(dsc.Person));
                    }
                }
            }
            CollectionSource newCollectionSource = new CollectionSource(Application.CreateObjectSpace(), typeof(PersonCore));
            newCollectionSource.Criteria["SearchResults"] = new InOperator("Oid", locs);
            e.ShowViewParameters.CreatedView = Application.CreateListView("PersonCore_ListView", newCollectionSource, true); ;
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.Separate;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;

        }

        private void popupWindowShowAction_UseInProjects_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            IObjectSpace os = Application.CreateObjectSpace();
            foreach (Project item in e.PopupWindow.View.SelectedObjects)
            {
                Project prj = os.FindObject<Project>(new BinaryOperator("Oid", item.Oid));
                foreach (Dataset ds in View.SelectedObjects)
                {
                    if (os.FindObject<DatasetUsage>(CriteriaOperator.And(
                        new BinaryOperator("Project", prj.Oid),
                        new BinaryOperator("Dataset", ds.Oid))) == null)
                    {
                        DatasetUsage use = os.CreateObject<DatasetUsage>();
                        use.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", ds.Oid));
                        use.Project = prj;
                        use.Save();
                    }
                }
            }
            os.CommitChanges();
        }

        private void popupWindowShowAction_UseInProjects_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(Project), false);
            e.View = view;
        }

        private void PopupWindowShowAction_DatasetAddContributors_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            IObjectSpace os = Application.CreateObjectSpace();
            foreach (PersonCore item in e.PopupWindow.View.SelectedObjects)
            {
                PersonCore person = os.FindObject<PersonCore>(new BinaryOperator("Oid",item.Oid));
                foreach (Dataset ds in View.SelectedObjects)
                {
                    if (os.FindObject<DatasetContribution>(CriteriaOperator.And(
                        new BinaryOperator("Dataset", ds.Oid),
                        new BinaryOperator("Person", person.Oid))) == null)
                    {
                        DatasetContribution use = os.CreateObject<DatasetContribution>();
                        use.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", ds.Oid));
                        use.Person = person;
                        use.Save();
                    }
                }
            }
            os.CommitChanges();
        }

        private void PopupWindowShowAction_DatasetAddContributors_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            //CollectionSource ds = new CollectionSource(os, typeof(TaxonomicName));
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(PersonCore), false);
            e.View = view;
        }

        private void PopupWindowShowAction_GenerateViews_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            CreateViewsOptions options = ((CreateViewsOptions)e.PopupWindow.View.CurrentObject);
            foreach (Dataset item in View.SelectedObjects)
            {
                ListView view = GeneralHelper.CreateModelViews(Application.Model, View.ObjectSpace, item, View.ObjectTypeInfo);

            }
            //    Frame.SetView(e.PopupWindow.View);
        }

        private void PopupWindowShowAction_GenerateViews_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            CreateViewsOptions obj = os.CreateObject<CreateViewsOptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }
    }
}
