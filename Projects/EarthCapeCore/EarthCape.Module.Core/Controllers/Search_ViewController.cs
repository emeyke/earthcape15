﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Search_ViewController : ViewController
    {
        public Search_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetObjectType = typeof(Search);
            TargetViewType = ViewType.DetailView;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_Search_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Search search = ((Search)View.CurrentObject);
            IObjectSpace os = Application.CreateObjectSpace();
            View.ObjectSpace.CommitChanges();
             if (!String.IsNullOrEmpty(search.UnitID))
            {
                Unit unit = os.FindObject<Unit>(new BinaryOperator("UnitID", search.UnitID));
                if (unit != null)
                {
                    DetailView dv = Application.CreateDetailView(os, unit, false);
                    e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                    e.ShowViewParameters.CreatedView = dv;
                }
            }
            else
            {

                if (!String.IsNullOrEmpty(search.UnitIDList))
                {
                     ListView dv = Application.CreateListView(os, typeof(Unit), true);
                    GeneralHelper.FilterByName(search.UnitIDList, dv);
                    e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                    e.ShowViewParameters.CreatedView = dv;
                   }
                else
                {
                    if (!String.IsNullOrEmpty(search.TaxonomicName) && !String.IsNullOrEmpty(search.LocalityName))
                    {
                        ListView dv = Application.CreateListView(os, typeof(Unit), true);
                        dv.CollectionSource.Criteria[string.Empty] = CriteriaOperator.And(
                            new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty("Locality.Name"), search.LocalityName),
                            new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty("TaxonomicName.FullName"), search.TaxonomicName));
                        e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                        e.ShowViewParameters.CreatedView = dv;
                       }
                    else
                    {
                        if (!String.IsNullOrEmpty(search.TaxonomicName))
                        {
                            ListView dv = Application.CreateListView(os, typeof(Unit), true);
                            dv.CollectionSource.Criteria[string.Empty] =new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty("TaxonomicName.FullName"), search.TaxonomicName);
                            e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                            e.ShowViewParameters.CreatedView = dv;
                          }
                        else
                        {
                            if (!String.IsNullOrEmpty(search.LocalityName))
                            {
                                ListView dv = Application.CreateListView(os, typeof(Unit), true);
                                dv.CollectionSource.Criteria[string.Empty] = new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty("Locality.Name"), search.LocalityName);
                                e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                                e.ShowViewParameters.CreatedView = dv;
                               }
                            else
                            {


                            }
                        }
                    }
                }
            }
        }
    }
}
