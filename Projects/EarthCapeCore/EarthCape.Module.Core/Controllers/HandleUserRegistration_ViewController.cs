using System;
using System.Linq;
using DevExpress.ExpressApp;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using Xpand.ExpressApp.Security.Registration;

namespace EarthCape.Module.Core
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class HandleUserRegistration_ViewController : ViewController
    {
        public HandleUserRegistration_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType = typeof(RegisterUserParameters);
        }

        void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Emailer.SendRegistrationToUser("", "", ((RegisterUserParameters)View.CurrentObject).Email, ((RegisterUserParameters)View.CurrentObject).UserName, "", Emailer.GetSmtp(), Emailer.EmailTemplateNewRegistrationToUser);
         
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            View.ObjectSpace.Committing += ObjectSpace_Committing;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
       

        
    }
}
