namespace EarthCape.Module.Core
{
    partial class Species_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_MergeToSpecies = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_MergeDuplicates = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_MergeToSpecies
            // 
            this.popupWindowShowAction_MergeToSpecies.AcceptButtonCaption = null;
            this.popupWindowShowAction_MergeToSpecies.CancelButtonCaption = null;
            this.popupWindowShowAction_MergeToSpecies.Caption = "Merge To...";
            this.popupWindowShowAction_MergeToSpecies.Category = "Tools";
            this.popupWindowShowAction_MergeToSpecies.ConfirmationMessage = null;
            this.popupWindowShowAction_MergeToSpecies.Id = "popupWindowShowAction_MergeToSpecies";
            this.popupWindowShowAction_MergeToSpecies.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MergeToSpecies.ToolTip = null;
            this.popupWindowShowAction_MergeToSpecies.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MergeToSpecies_CustomizePopupWindowParams);
            this.popupWindowShowAction_MergeToSpecies.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MergeToSpecies_Execute);
            // 
            // simpleAction_MergeDuplicates
            // 
            this.simpleAction_MergeDuplicates.Caption = "Merge duplicates";
            this.simpleAction_MergeDuplicates.Category = "Tools";
            this.simpleAction_MergeDuplicates.ConfirmationMessage = "Find and merge all duplicates?";
            this.simpleAction_MergeDuplicates.Id = "simpleAction_MergeDuplicates";
            this.simpleAction_MergeDuplicates.TargetObjectType = typeof(EarthCape.Module.Core.Species);
            this.simpleAction_MergeDuplicates.ToolTip = null;
            this.simpleAction_MergeDuplicates.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_MergeDuplicates_Execute);
            // 
            // Species_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_MergeToSpecies);
            this.Actions.Add(this.simpleAction_MergeDuplicates);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Species);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MergeToSpecies;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MergeDuplicates;
    }
}
