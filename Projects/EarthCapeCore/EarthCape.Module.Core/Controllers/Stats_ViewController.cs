﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EarthCape.Module.Core;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Stats_ViewController : ViewController
    {
        public Stats_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (View.ObjectSpace is XPObjectSpace)
            {
                if (((XPObjectSpace)View.ObjectSpace).Session.DataLayer!=null)
                using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
                {
                    if (View != null)
                    {
                        Stats stats = new Stats(uow);
                        stats.ViewId = View.Id;
                        if (View.ObjectTypeInfo != null)
                            stats.ObjectType = View.ObjectTypeInfo.Name;
                        stats.ViewCaption = View.Caption;
                        stats.DateTime = DateTime.Now;
                        stats.UserName = SecuritySystem.CurrentUserName;
                        stats.Application = Application.Title;
                            stats.Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                             if (View.CurrentObject != null)
                                if (typeof(BaseObject).IsAssignableFrom(View.ObjectTypeInfo.Type))
                                    stats.ObjectId = ((BaseObject)View.CurrentObject).Oid;
                        stats.Save();
                        uow.CommitChanges();
                    }
                }
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
