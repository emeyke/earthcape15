using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace EarthCape.Module.Core
{
    public partial class FindByUnitID_ViewController : ViewController
    {
        public FindByUnitID_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void parametrizedAction1_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            XPObjectSpace objectSpace = (XPObjectSpace)Application.CreateObjectSpace();
            string paramValue = e.ParameterCurrentValue as string;
            if (string.IsNullOrEmpty(paramValue)) return;
            object obj = null;
            Unit unit = (Unit)objectSpace.FindObject(typeof(Unit), new BinaryOperator("UnitID", paramValue));
            if (unit != null)
            {
                obj = unit;
            }
            /*if (View.ObjectTypeInfo != null)
                if (typeof(Unit).IsAssignableFrom(View.ObjectTypeInfo.Type))
                {
                 }*/
            if (obj == null)
            {
                obj = objectSpace.FindObject(typeof(Unit), new BinaryOperator("UnitID", paramValue, BinaryOperatorType.Equal));
            }
            if (obj != null)
            {
                e.ShowViewParameters.CreatedView = Application.CreateDetailView(objectSpace, obj);
            }
        }
    }
}
