using System;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;



namespace EarthCape.Module.Core
{
    public partial class GroupFilterSetRemove_ViewController : ViewController
    {
        public GroupFilterSetRemove_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            //this.Activated += SetGroupFilter_ViewController_Activated;
        }
      /*  protected override void OnFrameAssigned()
        {
            base.OnFrameAssigned();
            Frame.GetController<Xpand.ExpressApp.AdditionalViewControlsProvider.Logic.AdditionalViewControlsRuleViewController>().LogicRuleExecuting += new EventHandler<LogicRuleExecutingEventArgs<IAdditionalViewControlsRule>>(GroupFilter_ViewController_LogicRuleExecuting); ;
        }*/
      /*  object hintPanel = null;
        void GroupFilter_ViewController_LogicRuleExecuting(object sender, LogicRuleExecutingEventArgs<IAdditionalViewControlsRule> e)
        {
          //  hintPanel = ((AdditionalViewControlsRule)e.LogicRuleInfo.Rule).Control;
           // if (hintPanel != null)
            {
                   GeneralHelper.UpdateAppPanel(Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace),View);
           
            }
        }*/
     
        void SetGroupFilter_ViewController_Activated(object sender, EventArgs e)
        {
            if (Value_WindowController.Instance() != null)
            {
                if (string.IsNullOrEmpty(Value_WindowController.Instance().Project))
                {
                    Value_WindowController.Instance().Project = "assigning";
                    this.Activated -= SetGroupFilter_ViewController_Activated;
                    simpleAction_SetFilter.DoExecute();
                }
            }
       }


        private void simpleAction_SetFilter_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            IObjectSpace objectSpace = ((SimpleAction)sender).Application.CreateObjectSpace();
            CollectionSource collsource = new CollectionSource(objectSpace, typeof(Project));
            e.ShowViewParameters.CreatedView = ((SimpleAction)sender).Application.CreateListView("Group_LookupListView", collsource, true);
            e.ShowViewParameters.CreatedView.ControlsCreated += new EventHandler(CreatedView_ControlsCreated);
            
            /* if (((XPCollection)collsource.Collection).Count == 1)
        {
            Group Project = (Group)((XPCollection)collsource.Collection)[0];
            Value_WindowController.Instance((SimpleAction)sender).Project = Project.Oid.ToString();
            GeneralHelperWin.UpdateAppPanel(Project, View);
        }
        else
        {*/
            /*    User user = objectSpace.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), objectSpace.GetKeyValue(SecuritySystem.CurrentUser)) as User;
                if (objectSpace.FindObject<Group>(CriteriaHelper.GetGroupsCriteria(user)) != null)
                {
                    e.ShowViewParameters.CreatedView = ((SimpleAction)sender).Application.CreateListView("Group_LookupListView", collsource, true);
                    e.ShowViewParameters.CreatedView.ControlsCreated += new EventHandler(CreatedView_ControlsCreated);
                }
                else
                {
                    Group Project = objectSpace.CreateObject<Group>();
                    Project.Name = String.Format(((ObjectSpace)ObjectSpace).Session.DataLayer.Connection.Database, user.FullName);
                    Project.GroupType = GroupType.Project;
                    Project.Save();
                    objectSpace.CommitChanges();
                    Value_WindowController.Instance((SimpleAction)sender).Project = Project.Oid.ToString();
                    GeneralHelper.UpdateAppPanel(Project, View);
                    // e.ShowViewParameters.CreatedView = ((SimpleAction)sender).Application.CreateDetailView(objectSpace, "Group_DetailViewSimple", false, Project);
                    //objsp1 = objectSpace;
                }*/
                e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
                var dialogController = new DialogController();
                e.ShowViewParameters.Controllers.Add(dialogController);
                dialogController.AcceptAction.Execute += AcceptAction_Execute;
                dialogController.SaveOnAccept = true;
                dialogController.CancelAction.Execute += CancelAction_Execute;
            //}
        }
        IObjectSpace objsp1;
        void CreatedView_ControlsCreated(object sender, EventArgs e)
        {
            Value_WindowController.Instance().Project = "";
        }

        void AcceptAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            User user = os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            Project Project =null;
            if (e.SelectedObjects.Count > 0)
            {
                Project = (Project)e.SelectedObjects[0];
                if (objsp1 != null)
                    if (objsp1.IsNewObject(Project))
                    {
                        Project.Save();
                        objsp1.CommitChanges();
                    }
            }
            if (Project != null)
                Value_WindowController.Instance((SimpleAction)sender).Project = Project.Oid.ToString();
          //  GeneralHelper.UpdateAppPanel(Project, View);
            /*DataAccess_ViewController dataAccessController = Frame.GetController<DataAccess_ViewController>();
                  if (dataAccessController != null)
                  {
                      dataAccessController.DataAccess_ViewController_ActivatedExtracted(View);
                  }*/
            //   if (Project.DefaultCustomization != null)
            //       GeneralHelperWin.ReloadModel(Project.DefaultCustomization, (WinApplication)Application);
          /*  if (View is ListView)
            {
                ((ListView)View).CollectionSource.Criteria[""] = CriteriaHelper.DataAccess_FilterView(((ListView)View).ObjectTypeInfo.Type, user);
                RefreshController refreshController = Frame.GetController<RefreshController>();
                if (refreshController != null)
                {
                    if (refreshController.RefreshAction.Active.ResultValue == true)
                        refreshController.RefreshAction.DoExecute();
                }
            }*/
       }
        void CancelAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            
          //  if (String.IsNullOrEmpty(Value_WindowController.Instance((SimpleAction)sender).Project))
         //   ((WinApplication)Application).Exit();
        }

        private void popupWindowShowAction_SetFilter_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os, typeof(Project), false);
            e.DialogController.SaveOnAccept = false;
            e.View = view;
 
        }

        private void popupWindowShowAction_SetFilter_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os =Application.CreateObjectSpace();
            User user = os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            Project Project = (Project)e.PopupWindow.View.CurrentObject;
            if (Project != null)
            {
                Value_WindowController.Instance((SimpleAction)sender).Project = Project.Oid.ToString();
              //  GeneralHelper.UpdateAppPanel(Project, View);
            }
        }
    }
}
