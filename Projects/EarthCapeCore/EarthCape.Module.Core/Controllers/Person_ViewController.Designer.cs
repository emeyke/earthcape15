namespace EarthCape.Module.Core.Controllers
{
    partial class Person_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddToProjectContacts = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddToProjectContacts
            // 
            this.popupWindowShowAction_AddToProjectContacts.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddToProjectContacts.CancelButtonCaption = null;
            this.popupWindowShowAction_AddToProjectContacts.Caption = "Add to project contacts";
            this.popupWindowShowAction_AddToProjectContacts.Category = "Tools";
            this.popupWindowShowAction_AddToProjectContacts.ConfirmationMessage = null;
            this.popupWindowShowAction_AddToProjectContacts.Id = "popupWindowShowAction_AddToProjectContacts";
            this.popupWindowShowAction_AddToProjectContacts.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddToProjectContacts.ToolTip = null;
            this.popupWindowShowAction_AddToProjectContacts.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddToProjectContacts_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddToProjectContacts.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToProjectContacts_Execute);
            // 
            // Person_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddToProjectContacts);
            this.TargetObjectType = typeof(EarthCape.Module.Core.PersonCore);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddToProjectContacts;
    }
}
