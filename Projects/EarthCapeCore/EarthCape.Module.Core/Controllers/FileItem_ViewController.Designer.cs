namespace EarthCape.Module.Core
{
    partial class FileItem_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_MapLayerFormFileItem = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_MapLayerFormFileItem
            // 
            this.popupWindowShowAction_MapLayerFormFileItem.AcceptButtonCaption = null;
            this.popupWindowShowAction_MapLayerFormFileItem.CancelButtonCaption = null;
            this.popupWindowShowAction_MapLayerFormFileItem.Caption = "Make map layer";
            this.popupWindowShowAction_MapLayerFormFileItem.Category = "Tools";
            this.popupWindowShowAction_MapLayerFormFileItem.Id = "popupWindowShowAction_MapLayerFormFileItem";
            this.popupWindowShowAction_MapLayerFormFileItem.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MapLayerFormFileItem.Tag = null;
            this.popupWindowShowAction_MapLayerFormFileItem.TargetObjectType = typeof(EarthCape.Module.FileItem);
            this.popupWindowShowAction_MapLayerFormFileItem.TypeOfView = null;
            this.popupWindowShowAction_MapLayerFormFileItem.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MapLayerFormFileItem_Execute);
            this.popupWindowShowAction_MapLayerFormFileItem.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MapLayerFormFileItem_CustomizePopupWindowParams);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MapLayerFormFileItem;


    }
}
