namespace EarthCape.Module.Core
{
    partial class UnitRoutines_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_UpdateInfoUnit = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ConvertStringCoordinates = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_SplitAccession = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_DeriveUnits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_MoveToAccession = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_AddTask = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_StockTake = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_AddUnit = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_Rename = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ShowLocalities = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ShowTaxa = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_UpdateInfoUnit
            // 
            this.simpleAction_UpdateInfoUnit.Caption = "Update Info";
            this.simpleAction_UpdateInfoUnit.Category = "Tools";
            this.simpleAction_UpdateInfoUnit.ConfirmationMessage = null;
            this.simpleAction_UpdateInfoUnit.Id = "simpleAction_UpdateInfoUnit";
            this.simpleAction_UpdateInfoUnit.ToolTip = null;
            this.simpleAction_UpdateInfoUnit.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_UpdateInfo_Execute);
            // 
            // simpleAction_ConvertStringCoordinates
            // 
            this.simpleAction_ConvertStringCoordinates.Caption = "Convert string coordinates";
            this.simpleAction_ConvertStringCoordinates.Category = "Tools";
            this.simpleAction_ConvertStringCoordinates.ConfirmationMessage = "This will attempt to convert LatitudeString/LongitudeString fields into Latitude/" +
    "Longitude values and overwrite exisiting values for selected records. Proceed?";
            this.simpleAction_ConvertStringCoordinates.Id = "simpleAction_ConvertStringCoordinates";
            this.simpleAction_ConvertStringCoordinates.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ConvertStringCoordinates.ToolTip = null;
            this.simpleAction_ConvertStringCoordinates.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_ConvertStringCoordinates_Execute);
            // 
            // popupWindowShowAction_SplitAccession
            // 
            this.popupWindowShowAction_SplitAccession.AcceptButtonCaption = null;
            this.popupWindowShowAction_SplitAccession.CancelButtonCaption = null;
            this.popupWindowShowAction_SplitAccession.Caption = "Split accession";
            this.popupWindowShowAction_SplitAccession.Category = "Edit";
            this.popupWindowShowAction_SplitAccession.ConfirmationMessage = null;
            this.popupWindowShowAction_SplitAccession.Id = "popupWindowShowAction_SplitAccession";
            this.popupWindowShowAction_SplitAccession.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.popupWindowShowAction_SplitAccession.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_SplitAccession.TargetViewId = "Accession_Units_ListView";
            this.popupWindowShowAction_SplitAccession.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.popupWindowShowAction_SplitAccession.ToolTip = null;
            this.popupWindowShowAction_SplitAccession.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_SplitAccession_CustomizePopupWindowParams);
            this.popupWindowShowAction_SplitAccession.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_SplitAccession_Execute);
            // 
            // popupWindowShowAction_DeriveUnits
            // 
            this.popupWindowShowAction_DeriveUnits.AcceptButtonCaption = null;
            this.popupWindowShowAction_DeriveUnits.CancelButtonCaption = null;
            this.popupWindowShowAction_DeriveUnits.Caption = "Derive units";
            this.popupWindowShowAction_DeriveUnits.Category = "ObjectsCreation";
            this.popupWindowShowAction_DeriveUnits.ConfirmationMessage = null;
            this.popupWindowShowAction_DeriveUnits.Id = "popupWindowShowAction_DeriveUnits";
            this.popupWindowShowAction_DeriveUnits.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_DeriveUnits.ToolTip = null;
            this.popupWindowShowAction_DeriveUnits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_DeriveUnits_CustomizePopupWindowParams);
            this.popupWindowShowAction_DeriveUnits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_DeriveUnits_Execute);
            // 
            // popupWindowShowAction_MoveToAccession
            // 
            this.popupWindowShowAction_MoveToAccession.AcceptButtonCaption = null;
            this.popupWindowShowAction_MoveToAccession.CancelButtonCaption = null;
            this.popupWindowShowAction_MoveToAccession.Caption = "Link to accession";
            this.popupWindowShowAction_MoveToAccession.Category = "Edit";
            this.popupWindowShowAction_MoveToAccession.ConfirmationMessage = null;
            this.popupWindowShowAction_MoveToAccession.Id = "popupWindowShowAction_MoveToAccession";
            this.popupWindowShowAction_MoveToAccession.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MoveToAccession.ToolTip = null;
            this.popupWindowShowAction_MoveToAccession.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MoveToAccession_CustomizePopupWindowParams);
            this.popupWindowShowAction_MoveToAccession.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MoveToAccession_Execute);
            // 
            // popupWindowShowAction_AddTask
            // 
            this.popupWindowShowAction_AddTask.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddTask.CancelButtonCaption = null;
            this.popupWindowShowAction_AddTask.Caption = "Add task";
            this.popupWindowShowAction_AddTask.Category = "RecordEdit";
            this.popupWindowShowAction_AddTask.ConfirmationMessage = null;
            this.popupWindowShowAction_AddTask.Id = "popupWindowShowAction_AddTask";
            this.popupWindowShowAction_AddTask.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddTask.ToolTip = null;
            this.popupWindowShowAction_AddTask.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddTask_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddTask.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddTask_Execute);
            // 
            // popupWindowShowAction_StockTake
            // 
            this.popupWindowShowAction_StockTake.AcceptButtonCaption = null;
            this.popupWindowShowAction_StockTake.CancelButtonCaption = null;
            this.popupWindowShowAction_StockTake.Caption = "Stocktake";
            this.popupWindowShowAction_StockTake.Category = "RecordEdit";
            this.popupWindowShowAction_StockTake.ConfirmationMessage = null;
            this.popupWindowShowAction_StockTake.Id = "popupWindowShowAction_StockTake";
            this.popupWindowShowAction_StockTake.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_StockTake.TargetViewId = "xx";
            this.popupWindowShowAction_StockTake.ToolTip = null;
            this.popupWindowShowAction_StockTake.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_StockTake_CustomizePopupWindowParams);
            this.popupWindowShowAction_StockTake.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_StockTake_Execute);
            // 
            // popupWindowShowAction_AddUnit
            // 
            this.popupWindowShowAction_AddUnit.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddUnit.CancelButtonCaption = null;
            this.popupWindowShowAction_AddUnit.Caption = "New unit by name";
            this.popupWindowShowAction_AddUnit.Category = "Tools";
            this.popupWindowShowAction_AddUnit.ConfirmationMessage = null;
            this.popupWindowShowAction_AddUnit.Id = "popupWindowShowAction_AddUnit";
            this.popupWindowShowAction_AddUnit.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.popupWindowShowAction_AddUnit.TargetViewId = "x";
            this.popupWindowShowAction_AddUnit.ToolTip = "Creates new units by for selected taxonomic names";
            this.popupWindowShowAction_AddUnit.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddUnit_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddUnit.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddUnit_Execute);
            // 
            // popupWindowShowAction_Rename
            // 
            this.popupWindowShowAction_Rename.AcceptButtonCaption = null;
            this.popupWindowShowAction_Rename.CancelButtonCaption = null;
            this.popupWindowShowAction_Rename.Caption = "Rename";
            this.popupWindowShowAction_Rename.Category = "Tools";
            this.popupWindowShowAction_Rename.ConfirmationMessage = "Change name(s)?";
            this.popupWindowShowAction_Rename.Id = "popupWindowShowAction_Rename";
            this.popupWindowShowAction_Rename.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_Rename.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.popupWindowShowAction_Rename.ToolTip = "Change taxonomic name for selected units";
            this.popupWindowShowAction_Rename.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_Rename_CustomizePopupWindowParams);
            this.popupWindowShowAction_Rename.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_Rename_Execute);
            // 
            // simpleAction_ShowLocalities
            // 
            this.simpleAction_ShowLocalities.Caption = "Show localities";
            this.simpleAction_ShowLocalities.Category = "Tools";
            this.simpleAction_ShowLocalities.ConfirmationMessage = null;
            this.simpleAction_ShowLocalities.Id = "simpleAction_ShowLocalities";
            this.simpleAction_ShowLocalities.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ShowLocalities.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.simpleAction_ShowLocalities.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_ShowLocalities.ToolTip = null;
            this.simpleAction_ShowLocalities.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_ShowLocalities.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowLocalities_Execute);
            // 
            // simpleAction_ShowTaxa
            // 
            this.simpleAction_ShowTaxa.Caption = "Show taxa";
            this.simpleAction_ShowTaxa.Category = "Tools";
            this.simpleAction_ShowTaxa.ConfirmationMessage = null;
            this.simpleAction_ShowTaxa.Id = "simpleAction_ShowTaxa";
            this.simpleAction_ShowTaxa.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ShowTaxa.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.simpleAction_ShowTaxa.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_ShowTaxa.ToolTip = null;
            this.simpleAction_ShowTaxa.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_ShowTaxa.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowTaxa_Execute);
            // 
            // UnitRoutines_ViewController
            // 
            this.Actions.Add(this.simpleAction_UpdateInfoUnit);
            this.Actions.Add(this.simpleAction_ConvertStringCoordinates);
            this.Actions.Add(this.popupWindowShowAction_SplitAccession);
            this.Actions.Add(this.popupWindowShowAction_DeriveUnits);
            this.Actions.Add(this.popupWindowShowAction_MoveToAccession);
            this.Actions.Add(this.popupWindowShowAction_AddTask);
            this.Actions.Add(this.popupWindowShowAction_StockTake);
            this.Actions.Add(this.popupWindowShowAction_AddUnit);
            this.Actions.Add(this.popupWindowShowAction_Rename);
            this.Actions.Add(this.simpleAction_ShowLocalities);
            this.Actions.Add(this.simpleAction_ShowTaxa);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_Rename;
        public DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddUnit;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowLocalities;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowTaxa;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateInfoUnit;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ConvertStringCoordinates;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_SplitAccession;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DeriveUnits;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MoveToAccession;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddTask;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_StockTake;
    }
}
