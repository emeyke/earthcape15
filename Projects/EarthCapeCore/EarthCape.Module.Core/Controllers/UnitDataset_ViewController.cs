﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class UnitDataset_ViewController : ViewController
    {
        public UnitDataset_ViewController()
        {
            InitializeComponent();
           // TargetViewId = "XX";
            this.TargetObjectType = typeof(Unit);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
          protected override void OnActivated()
        {
            base.OnActivated();
            NewObjectViewController newController = Frame.GetController<NewObjectViewController>();
            if (newController != null)
                newController.ObjectCreated += new EventHandler<ObjectCreatedEventArgs>(NewObjectViewController_ObjectCreated);
            // Perform various tasks depending on the target View.
        }

        private void NewObjectViewController_ObjectCreated(object sender, ObjectCreatedEventArgs e)
        {

            string id = View.Model.Id;
            Guid guid;
            if (id.IndexOf("EarthCape_") == 0)
                if (Guid.TryParse(id.Substring(id.LastIndexOf('_') + 1), out guid))
                {
                    Dataset ds =e.ObjectSpace.FindObject<Dataset>(new BinaryOperator("Oid", guid));
                    if (ds != null)
                        ((Unit)(e.CreatedObject)).Dataset = ds;
                }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

     

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
