using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.Core

{
    public partial class FileItem_ViewController : ViewController
    {
        public FileItem_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_MapLayerFormFileItem_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ObjectSpace os=(ObjectSpace)Application.CreateObjectSpace();
            Map map = null;
            if (((MapLayerFromSelectedFiles)(e.PopupWindow.View.CurrentObject)).AddToMap != null)
            { 
                map=os.FindObject<Map>(new BinaryOperator("Oid", ((MapLayerFromSelectedFiles)(e.PopupWindow.View.CurrentObject)).AddToMap.Oid)); 
            }
            foreach (FileItem item in View.SelectedObjects)
            {
                MapLayerFileItem maplayer = os.CreateObject<MapLayerFileItem>();
                maplayer.FileItem = os.FindObject<FileItem>(new BinaryOperator("Oid", item.Oid));
                maplayer.Category = map;
                maplayer.Save();
            }
            os.CommitChanges();
            if (map != null)
            {
                DetailView dv = Application.CreateDetailView(os, map, false);
                e.ShowViewParameters.TargetWindow = TargetWindow.Default;
                e.ShowViewParameters.CreatedView = dv;
            }
        }

        private void popupWindowShowAction_MapLayerFormFileItem_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace obs = (ObjectSpace)Application.CreateObjectSpace();
            MapLayerFromSelectedFiles param = obs.CreateObject<MapLayerFromSelectedFiles>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;

        }

    
   
     }
}
