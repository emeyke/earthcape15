using DevExpress.ExpressApp.Actions;
namespace EarthCape.Module.Core

{
    partial class GroupFilter_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_GroupFilter = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_GroupFilter
            // 
            this.simpleAction_GroupFilter.Caption = "Filter by current";
            this.simpleAction_GroupFilter.Category = "View";
            this.simpleAction_GroupFilter.ConfirmationMessage = null;
            this.simpleAction_GroupFilter.Id = "simpleAction_GroupFilter";
            this.simpleAction_GroupFilter.ImageName = "Action_Filter";
            this.simpleAction_GroupFilter.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_GroupFilter.Shortcut = null;
            this.simpleAction_GroupFilter.Tag = null;
            this.simpleAction_GroupFilter.TargetObjectsCriteria = null;
            this.simpleAction_GroupFilter.TargetObjectType = typeof(EarthCape.Module.Core.Project);
            this.simpleAction_GroupFilter.TargetViewId = null;
            this.simpleAction_GroupFilter.ToolTip = "Makes selected group a \"Current group\"";
            this.simpleAction_GroupFilter.TypeOfView = null;
            this.simpleAction_GroupFilter.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_GroupFilter_Execute);

        }

        #endregion

        public SimpleAction simpleAction_GroupFilter;

    }
}
