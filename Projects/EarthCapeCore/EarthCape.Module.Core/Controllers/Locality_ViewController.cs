using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;
using System.Collections.ObjectModel;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.Std
{
    public partial class Locality_ViewController : ViewController
    {
        public Locality_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
          //  TargetObjectType = typeof(Locality);
            
        }

        private void parametrizedAction_FindByName_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            string paramValue = e.ParameterCurrentValue as string;
            if (string.IsNullOrEmpty(paramValue)) return;
            IObjectSpace objectSpace;
            object obj = null;
            if ((View.ObjectTypeInfo != null) && (typeof(Locality).IsAssignableFrom(View.ObjectTypeInfo.Type)) && (View is DetailView))
            {
                objectSpace = View.ObjectSpace;
                obj = objectSpace.FindObject(typeof(Locality), new BinaryOperator("Name", paramValue, BinaryOperatorType.Equal));
                View.CurrentObject = obj;
            }
            else
            {
                objectSpace = Application.CreateObjectSpace();
                obj = objectSpace.FindObject(typeof(Locality), new BinaryOperator("Name", paramValue, BinaryOperatorType.Equal));

                if (obj != null)
                {
                    XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                    obj = os.FindObject(typeof(Locality), new BinaryOperator("Oid", ((Locality)obj).Oid));
                    DetailView view = Application.CreateDetailView(os, obj);
                    e.ShowViewParameters.CreatedView = view;
                }

                else
                {
                    throw new Exception(paramValue + " not found!");
                }
            }
        }

        private static void MoveUnits(Locality item, Locality item1)
        {
            Collection<Unit> coll = new Collection<Unit>();
            foreach (Unit unit in item1.Units)
            {
                coll.Add(unit);
            }
            foreach (Unit unit1 in coll)
            {
                unit1.Locality = item;
                unit1.Save();
            }
        }
        private static void MoveLocalityVisits(Locality item, Locality item1)
        {

            foreach (LocalityVisit linkedItem in item1.LocalityVisits)
            {
                item.LocalityVisits.Add(linkedItem);
            }
        }
        private static void MoveNotes(Locality item, Locality item1)
        {
            foreach (LocalityNote linkedItem in item1.Notes)
            {
                item.Notes.Add(linkedItem);
            }
        }
     
        private static void MoveCustomData(Locality item, Locality item1)
        {
            Collection<CustomValue> coll = new Collection<CustomValue>();
            foreach (CustomValue CustomValue in item1.CustomData)
            {
                coll.Add(CustomValue);
            }
            foreach (CustomValue CustomValue1 in coll)
            {
                CustomValue1.Locality = item;
                CustomValue1.Save();
            }
        }
        private static void MoveLinkedItems(Locality item, Locality item1)
        {

            if (item1.FileSet != null)
            foreach (FileItem linkedItem in item1.FileSet.Files)
            {
                if (item.FileSet == null) item.FileSet = new FileSet(item.Session);
                item.FileSet.Files.Add(linkedItem);
            }
        }
        private void simpleAction_MergeDuplicateLocalitiesByName_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Locality item in View.SelectedObjects)
            {
                if (!item.IsDeleted)
                foreach (Locality item1 in View.SelectedObjects)
                {
                    if (item.Name==item1.Name)
                    if (item.Oid != item1.Oid)
                    {
                        if (!item1.IsDeleted)
                        {
                            MoveUnits(item, item1);
                            MoveLocalityVisits(item, item1);
                            MoveNotes(item, item1);
                            MoveLinkedItems(item, item1);
                            MoveCustomData(item, item1);
                            //MoveTasks(item, item1);
                            if (!string.IsNullOrEmpty(item1.Comment))
                                if (!string.IsNullOrEmpty(item.Comment))
                                    item.Comment = String.Format("{0}\r\n{1}", item.Comment, item1.Comment);
                                else
                                    item.Comment =item1.Comment;
                            if (!string.IsNullOrEmpty(item1.WKT))
                                if (string.IsNullOrEmpty(item.WKT))
                                    item.WKT = item1.WKT;
                            if (item1.EPSG > 0)
                                if (item1.EPSG < 1)
                                    item.EPSG = item1.EPSG;
                            if (item1.Category !=null)
                                if (item.Category==null)
                                    item.Category = item1.Category;
                            item1.Delete();
                        }
                    }
                }
               
            }
            View.ObjectSpace.CommitChanges();
        }
    }
}
