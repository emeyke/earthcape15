
using DevExpress.ExpressApp;

using DevExpress.Persistent.Base;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;

namespace EarthCape.Module.Core
{
    public class Hints
    {

        public const string LocalizationHint =
  @"You can easily localize your application so that a UI, control-specific messages and captions, property names and enumeration values use the same language. 
The default language is used when the PreferredLanguage attribute of the Application Model's Application node is set to ""(Default Language)"". Values in the default language correspond to the ones that are used in the Model Editor when the Language editor in the XAF Model Editor toolbox is set to ""(Default Language)"". 
The German language is used when the PreferredLanguage attribute of the Application Model's Application node is set to ""de"". Values in the German language correspond to the ones that are used in the Model Editor when the Language editor in the XAF Model Editor toolbox is set to ""de"".
The default formatting options (see the DecimalProperty and DateTime properties in the current View) correspond to the ones that are set in the current user's operating system or passed by the Internet browser.
The German formatting options (see the DecimalProperty and DateTime properties in the current View) correspond to the ones that are set in the German format culture. 
Note that the CurrencyProperty's values always use the $ currency symbol, because the application's module handles the Application.CustomizeFormattingCulture event to fix this formatting option.";

        public const string FullAccessObjectHint =
            @"Objects in this View are fully accessible by any user.";
        public const string ProtectedContentObjectHint =
            @"Objects in this View are inaccessible for the ""John"" user.";
        public const string ReadOnlyObjectHint =
            @"Objects in this View are read-only for the ""John"" user.";
        public const string IrremovableObjectHint =
            @"Objects in this View cannot be removed by the ""John"" user.";
        public const string UncreatableObjectHint =
            @"Objects in this View cannot be created by the ""John"" user.";

        public const string MemberLevelSecurityObjectHint =
            @"This View demonstrates an object that is partially accessible for the ""John"" user. Some object members are read-only or totally inaccessible for him.";

        public const string ObjectLevelSecurityObject =
            @"This View demonstrates objects of the same type that have various access levels for the ""John"" user. There are read-only, irremovable, inaccessible and fully accessible objects.";

        public const string LogonWindowHeaderHint =
@"This application demonstrates the Complex Security Strategy in action. There are two predefined users in this demo. ""Sam"" is an administrator who has a full access to demo objects, as well as an option to change permissions for other users. ""John"" is a user with certain predefined restrictions.";
    }

    public static class NavigationGroups
    {
        public const string ClassLevelSecurity = "Class-level security";
        public const string MemberLevelSecurity = "Member-level security";
        public const string ObjectLevelSecurity = "Object-level security";
    }
}
