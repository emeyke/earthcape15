namespace EarthCape.Module.Core
{
    public enum TypeStatus
    {
        Verified,
        NotVerified
    }
}
