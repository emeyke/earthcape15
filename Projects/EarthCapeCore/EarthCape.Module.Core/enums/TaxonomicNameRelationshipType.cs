﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module
{
    public enum TaxonRelationshipType
    {
        IsSynonymFor,// The current TaxonomicName is considered a synonym of the target TaxonomicName. This is an ambiguous relationship. It can mean: 1) a nomenclatural relationship where all that is implied is that the type of the current TaxonomicName is included in the target circumscription. This is more precisely expressed as a specimen circumscription (for heterotypic synonyms) or as TaxonName basionym relationships (for homotypic synonyms) 2) a TaxonomicName relationship where some part of (or all of) the current TaxonomicName is included in the target circumscription. This is more precisely expressed using the Dataset relationships such as 'is congruent to'. The IsSynonymFor term is principally intended for handling legacy data. 
        IsTeleomorphOf,// TaxonomicName 1 is the teleomorph or meiotic reproductive stage in a pleomorphic life cycle in which TaxonomicName 2 is the asexual or mitotic reproductive stage. 
        IsVernacularFor,// The current TaxonomicName is used as a vernacular TaxonomicName, at least in part, for the target TaxonomicName. This kind of relationship should not be used to express any form of Dataset relationship (e.g. overlaps, is congruent with, includes). Consider using vernacular type relationships along with Dataset type relationships to avoid any ambiguity. 
        IsAmbiregnalOf,// The two Taxa are considered to represent the same TaxonomicName (i.e. they are congruent) but their names governed by different nomenclatural codes. 
        IsAnamorphOf,// TaxonomicName 1 is the asexual or mitotic reproductive stage in a pleomorphic life cycle in which TaxonomicName 2 is the teleomorph or meiotic reproductive stage . 
        HasSynonym,// The target TaxonomicName is considered a synonym of the current TaxonomicName. This is an ambiguous relationship. It can mean: 1) a nomenclatural relationship where all that is implied is that the type of the target TaxonomicName is included in the current circumscription. This is more precisely expressed as a specimen circumscription (for heterotypic synonyms) or as TaxonName basionym relationships (for homotypic synonyms) 2) a TaxonomicName relationship where some part of (or all of) the target TaxonomicName is included in the current circumscription. This is more precisely expressed using the Dataset relationships such as 'is congruent to'. The HasSynonym term is principally intended for handling legacy data. 
        HasVernacular,// The target TaxonomicName is used as a vernacular TaxonomicName, at least in part, for the current TaxonomicName. This kind of relationship should not be used to express any form of Dataset relationship (e.g. overlaps, is congruent with, includes). Consider using vernacular type relationships along with Dataset type relationships to avoid any ambiguity. 
        IsChildTaxonOf,// Hierarchical Relationship: TaxonomicName 1 is a member of lower taxonomic rank of TaxonomicName 2 
        IsParentTaxonOf// Hierarchical Relationship: TaxonomicName 1 includes TaxonomicName 2 as a lower-ranked member. 
        /* DoesNotInclude,// Dataset Relationship: TaxonomicName 2 is not a subset of TaxonomicName 1 
         DoesNotOverlap,// Dataset Relationship: Taxa 1 and 2 have no members/children in common 
         Excludes,// Dataset Relationship: TaxonomicName 1 does not overlap or include TaxonomicName 2 
         Includes,// Dataset Relationship: TaxonomicName 2 is a subset of TaxonomicName 1 
         IsCongruentTo,// Dataset Relationship: The extent of TaxonomicName 1 is (essentially) identical to TaxonomicName 2 
          IsIncludedIn,// Dataset Relationship: TaxonomicName 1 is a subset of TaxonomicName 2 
         IsNotCongruentTo,// Dataset Relationship: The extent of TaxonomicName 1 is not identical to TaxonomicName 2 
         IsNotIncludedIn,// Dataset Relationship: TaxonomicName 1 is not a subset of TaxonomicName 2 
             Overlaps,// Dataset Relationship: Taxa 1 and 2 share members/children in common 
       IsFemaleParentOf,// Hybrid Relationship: TaxonomicName 1 is genetic mother of TaxonomicName 2  
         IsFirstParentOf,// Hybrid Relationship: TaxonomicName 1 is genetic parent (1) of TaxonomicName 2 
         IsHybridChildOf,// Hybrid Relationship: TaxonomicName 2 is a genetic parent of TaxonomicName 1 
         IsHybridParentOf,// Hybrid Relationship: TaxonomicName 1 is genetic parent of TaxonomicName 2 
           IsMaleParentOf,// Hybrid Relationship: TaxonomicName 1 is genetic father of TaxonomicName 2 
         IsSecondParentOf,// Hybrid Relationship: TaxonomicName 1 is genetic parent (2) of TaxonomicName 2. 
  */
    }
}
