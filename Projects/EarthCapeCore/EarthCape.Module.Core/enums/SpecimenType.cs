using System;

namespace EarthCape.Module.Core
{
    public enum SpecimenType
    {
        Holotype,// When a single specimen is clearly designated in the original description, this specimen is known as the holotype of that species. The holotype is typically placed in a major museum, or similar well-known public collection, so that it is freely available for later examination by other biologists.
        Paratype,// � Any additional specimen other than the holotype, listed in the type series, where the original description designated a holotype. These are not name-bearing types.
        Neotype,// � A specimen later selected to serve as the single type specimen when an original holotype has been lost or destroyed, or where the original author never cited a specimen.
        Syntype,// � Any of two or more specimens listed in a species description where a holotype was not designated; historically, syntypes were often explicitly designated as such, and under the present Code this is a requirement, but modern attempts to publish species description based on syntypes are generally frowned upon by practicing taxonomists, and most are gradually being replaced by lectotypes. Those that still exist are still considered name-bearing types.
        Lectotype,// � A specimen later selected to serve as the single type specimen for species originally described from a set of syntypes.
        Paralectotype,// � Any additional specimen from among a set of syntypes, after a lectotype has been designated from among them. These are not name-bearing types.
        Hapantotype,// �A special case in Protistans where the type consists of two or more specimens of "directly related individuals representing distinct stages in the life cycle"; these are collectively treated as a single entity, and lectotypes cannot be designated from among them.
        Type,
        Isoepitype,
        Isolectotype,
        Isoneotype,
        Isotype

    }
}
