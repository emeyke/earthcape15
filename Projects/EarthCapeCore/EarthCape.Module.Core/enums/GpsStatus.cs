using System;

namespace EarthCape.Module.Core
{
    public enum GpsStatus
    {
        DOP,
        Fix2D,
        Fix3D,
        Fix3Dplus
    }
}
