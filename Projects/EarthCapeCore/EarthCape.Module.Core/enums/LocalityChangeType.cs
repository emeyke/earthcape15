﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module
{
    public enum PlaceChangeType
    {
        Removed,
        SplitFrom,
        RenamedTo,
        MergedTo
    }
}
