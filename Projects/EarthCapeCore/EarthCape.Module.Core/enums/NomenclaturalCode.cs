﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module
{
    public enum NomenclaturalCode
    {
        Undefined,
        Bacteriological,// Rules that govern the naming of bacteria TaxonomicName 
        ICBN,// International Code of Botanical Nomenclature 
        ICNCP,// International Code of Cultivated Plants 
        ICZN,// International Code of Zoological Nomenclature 
        Viral// Rules that govern the names of viral TaxonomicName 
    }
}
