﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthCape.Module.Core
{
    public enum DwcDisposition
    {
        InCollection,
        Missing,
        VoucherElsewhere,
        DuplicatesElsewhere
    }
    public enum GeometryType
    {
        Point,
        Polygon,
        Line
    }

    public enum ProjectContactType
    {
        POINT_OF_CONTACT,
        TECHNICAL_POINT_OF_CONTACT,
        ADMINISTRATIVE_POINT_OF_CONTACT,
        METADATA_AUTHOR,
        PRINCIPAL_INVESTIGATOR,
        AUTHOR,
        ORIGINATOR
    }
    public enum Contributor
    {
        publisher,
        author,
        maintainer,
        wrangler,
        contributor
    }
    public enum Geoprivacy
    {
        Open,
        Obscured,
        Private
    }
    public enum DatasetContactType
    {
        POINT_OF_CONTACT,
        ADMINISTRATIVE_POINT_OF_CONTACT,
        METADATA_AUTHOR,
        ORIGINATOR
    }
    public enum BoldSent
    {
        whole,
        part
    }
    public enum DcmiType
    {
        Collection, Dataset, Event, Image, InteractiveResource, MovingImage, PhysicalObject, Service, Software, Sound, StillImage, Text
    }
    public enum BasisOfRecord
    {
        PreservedSpecimen ,
        FossilSpecimen,
        LivingSpecimen,
        MaterialSample,
        Event,
        HumanObservation,
        MachineObservation,
        Taxon,
        Occurrence
    }
    public enum TransferStatus
    {
        Undefined,
        Returned,
        NotReturned,
        ReturnedPartially
    }
    public enum IfFieldExists
    {
        DoNotImport,
        Overwrite,
        Import
    }
    public enum DataProvider
    {
        AceOleDb12,
        MicrosoftJetOleDBb4
    }

    public enum SlopeOrientation
    {
        N, NE, E, SE, S, SW, W, NW
    }

    public enum FileType
    {
        Image,
        Sound,
        Video,
        Data,
        Text
    }
    public enum ServerTypeExpress
    {
        SqlCE,
        Firebird,
        MySql,
        PostgreSql,
        SQLite,
        EarthCapeCloud
    }
    public enum ServerType
    {
        SqlCE,
        Advantage,
        Firebird,
        DB2,
        MSSql,
        MySql,
        Oracle,
        ODP,
        Pervasive,
        PostgreSql,
        NexusDB,
        Asa,
        Ase,
        SQLite,
        VistaDB3,
        XML,
        Remoting
    }


    public enum GroupStatus
    {
        Active,
        Archived,
        NotActive
    }

    /*   public enum InteractionRole
       {
           Host,
           Parasite,
           Predator,
           Prey,
           Plant,
           Herbivore
       }*/

    public enum TargetDataType
    {
        Any,
        Units,
        Localities,
        Taxons
    }
    public enum DevelopmentStage
    {
        Larva,
        Pupa,
        Imago,
        Adult,
        Egg,
        Juvenile,
        Tadpole,
        Spawn,
        Unknown
    }
    public enum Sex
    {
        Female,
        Male,
        Unknown,
        NotSpecified
    }

    public enum GroupType
    {
        Project,
        ResearchGroup,
        WorkingGroup,
        ScientificSociety,
        Survey,
        Experiment,
        Meeting,
        Conference,
        Museum,
        University,
        School,
        Laboratory
    }
    public enum PropertyValueType
    {
        String,
        Integer,
        Float
    }
    /*   public enum SetType
       {
           Units,
           ImageGallery,
           UnitsCollection,
           Blog,
           SpeciesList,
           FileSet,
           Forum,
           InteractionDataset,
           MsDataset,
           ReferenceList,
           SequenceDataset,
           SnpDataset,
           TagDataset,
           TaxonsClassification,
           TaxonsUsage
       }*/
    public enum ProjectMemberType
    {
        Member,
        GroupLeader,
        PhdStudent,
        MscStudent,
        Student,
        Secretary,
        Technician,
        Assistant,
        Collaborator,
        PrincipalInvestigator,
        FieldWorker,
        Director,
        Coordinator,
        Owner,
        LabTechnician,
        SeniorResearcher,
        ResearchAssistant,
        VisitingResearcher,
        PostdoctoralResearcher,
        Researcher,
        TechnicalSupport,
        HelpDesk
    }

    public enum AcademicDegree
    {
        BSc,
        MSc,
        PhD
    }


}
