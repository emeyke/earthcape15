using System;

namespace EarthCape.Module.Core
{
    public enum RoleType
    {
        General,
        ProjectAdmin,
        ProjectDataEditor,
        ProjectMember,
        OrganizationMember,
        OrganizationAdmin,
        Admin
    }
}
