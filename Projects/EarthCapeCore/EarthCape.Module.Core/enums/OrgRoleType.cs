using System;

namespace EarthCape.Module.Core
{
    public enum OrgRoleType
    {
        OrganizationMember,
        OrganizationAdmin,
    }
}
