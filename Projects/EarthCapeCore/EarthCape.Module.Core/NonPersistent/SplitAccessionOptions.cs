using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class SplitAccessionOptions : XPCustomObject
    {
        public SplitAccessionOptions(Session session) : base(session) { }


        private Accession _Accession;
        public Accession Accession
        {
            get { return _Accession; }
            set { SetPropertyValue<Accession>(nameof(Accession), ref _Accession, value); }
        }

        private bool _CopyAccessionDataFromCurrentIfNew = true;
        public bool CopyAccessionDataFromCurrentIfNew
        {
            get { return _CopyAccessionDataFromCurrentIfNew; }
            set { SetPropertyValue<bool>(nameof(CopyAccessionDataFromCurrentIfNew), ref _CopyAccessionDataFromCurrentIfNew, value); }
        }

        private bool _CloneFromCurrent;
        public bool CloneFromCurrent
        {
            get { return _CloneFromCurrent; }
            set { SetPropertyValue<bool>(nameof(CloneFromCurrent), ref _CloneFromCurrent, value); }
        }


        private bool _MoveSelectedUnits=true;
        public bool MoveSelectedUnits
        {
            get { return _MoveSelectedUnits; }
            set { SetPropertyValue<bool>(nameof(MoveSelectedUnits), ref _MoveSelectedUnits, value); }
        }




    }

}
