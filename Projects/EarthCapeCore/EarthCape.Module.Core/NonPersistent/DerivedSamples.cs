using System;

using DevExpress.Xpo;
using EarthCape.Module;


namespace EarthCape.Module.Core 
{
    public enum StoreDimensionType
    {Letters,Numbers 
    }
   
    [NonPersistent]
    public class DerivedSamples : XPCustomObject
    {
        public DerivedSamples(Session session) : base(session) { }
        private int _NumberOfParts=1;
        public int NumberOfParts
        {
            get
            {
                return _NumberOfParts;
            }
            set
            {
                SetPropertyValue("NumberOfParts", ref _NumberOfParts, value);
            }
        }
        private Store _Store;
        public Store Store
        {
            get
            {
                return _Store;
            }
            set
            {
                SetPropertyValue("Store", ref _Store, value);
            }
        }
        private StoreDimensionType _StoreDimensionY=StoreDimensionType.Letters;
        public StoreDimensionType StoreDimensionY
        {
            get
            {
                return _StoreDimensionY;
            }
            set
            {
                SetPropertyValue("StoreDimensionY", ref _StoreDimensionY, value);
            }
        }
        private StoreDimensionType _StoreDimensionX=StoreDimensionType.Numbers;
        public StoreDimensionType StoreDimensionX
        {
            get
            {
                return _StoreDimensionX;
            }
            set
            {
                SetPropertyValue("StoreDimensionX", ref _StoreDimensionX, value);
            }
        }
        private int _DimensionX=12;
        public int DimensionX
        {
            get
            {
                return _DimensionX;
            }
            set
            {
                SetPropertyValue("DimensionX", ref _DimensionX, value);
            }
        }
        private int _DimensionY=12;
        public int DimensionY
        {
            get
            {
                return _DimensionY;
            }
            set
            {
                SetPropertyValue("DimensionY", ref _DimensionY, value);
            }
        }
        private IDGenerator _IDGenerator;
        public IDGenerator IDGenerator
        {
            get
            {
                return _IDGenerator;
            }
            set
            {
                SetPropertyValue("IDGenerator", ref _IDGenerator, value);
            }
        }
        private Group _OwnerGroup;
        public Group OwnerGroup
        {
            get
            {
                return _OwnerGroup;
            }
            set
            {
                SetPropertyValue("OwnerGroup", ref _OwnerGroup, value);
            }
        }
        private Group _SharedWithGroup;
        public Group SharedWithGroup
        {
            get
            {
                return _SharedWithGroup;
            }
            set
            {
                SetPropertyValue("SharedWithGroup", ref _SharedWithGroup, value);
            }
        }
        private string _UnitType;
        public string UnitType
        {
            get
            {
                return _UnitType;
            }
            set
            {
                SetPropertyValue("UnitType", ref _UnitType, value);
            }
        }
   /*     private Unit _TemplateUnit;
        public Unit TemplateUnit
        {
            get
            {
                return _TemplateUnit;
            }
            set
            {
                SetPropertyValue("TemplateUnit", ref _TemplateUnit, value);
            }
        }*/
    }

}
