using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;

namespace EarthCape.Module
{
    [NonPersistent]
    public class RecordVisitWindow : XPCustomObject
    {
        public RecordVisitWindow(Session session) : base(session) { }
        private Place _RecordVisitToPlace;
        public Place RecordVisitToPlace
        {
            get
            {
                return _RecordVisitToPlace;
            }
            set
            {
                SetPropertyValue("RecordVisitToPlace", ref _RecordVisitToPlace, value);
            }
        }
        private bool _RecordGpsTracks=false;
        [VisibleInDetailView(false)]
        public bool RecordGpsTracks
        {
            get
            {
                return _RecordGpsTracks;
            }
            set
            {
                SetPropertyValue("RecordGpsTracks", ref _RecordGpsTracks, value);
            }
        }
        private string _PlaceVisitComment;
        public string PlaceVisitComment
        {
            get
            {
                return _PlaceVisitComment;
            }
            set
            {
                SetPropertyValue("PlaceVisitComment", ref _PlaceVisitComment, value);
            }
        }
        private string _PlaceNote;
        public string PlaceNote
        {
            get
            {
                return _PlaceNote;
            }
            set
            {
                SetPropertyValue("PlaceNote", ref _PlaceNote, value);
            }
        }

    }

}
