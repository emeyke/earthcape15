using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;

namespace EarthCape.Module
{
    [NonPersistent]
    public class ShareNameList : XPCustomObject
    {
        public ShareNameList(Session session) : base(session) { }
        private string _Names;
        [Size(SizeAttribute.Unlimited)]
        public string Names
        {
            get
            {
                return _Names;
            }
            set
            {
                SetPropertyValue("Names", ref _Names, value);
            }
        }
        private bool _NotInTheList;
        public bool NotInTheList
        {
            get
            {
                return _NotInTheList;
            }
            set
            {
                SetPropertyValue("NotInTheList", ref _NotInTheList, value);
            }
        }
         public XPCollection<Group> Groups
        {
            get
            {
                return new XPCollection<Group>(Session);
            }
        }
    }

}
