using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class ExtractMetadataOptions : XPCustomObject
    {
        public ExtractMetadataOptions(Session session) : base(session) { }

        private Boolean _CopyCoordinatesToUnit=true;
        public Boolean CopyCoordinatesToUnit
        {
            get { return _CopyCoordinatesToUnit; }
            set { SetPropertyValue<Boolean>(nameof(CopyCoordinatesToUnit), ref _CopyCoordinatesToUnit, value); }
        }

            
    }

}
