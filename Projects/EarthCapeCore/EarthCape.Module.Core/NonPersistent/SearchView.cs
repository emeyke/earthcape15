
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;

using DevExpress.Persistent.Base;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class SearchView : XPCustomObject
    {
        public SearchView(Session session) : base(session) { }
        private string criteria;
        [CriteriaOptionsAttribute("DataType")]
        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        [VisibleInListView(true)]
        [Custom("RowCount", "20")]
        public string Criteria
        {
            get { return criteria; }
            set
            {
                SetPropertyValue<string>("Criteria", ref criteria, value);
            }
        }
        private Type _DataType;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [TypeConverter(typeof(LocalizedClassInfoTypeConverter))]
        [ImmediatePostData, NonPersistent]
        public Type DataType
        {
            get
            {
                return _DataType;
            }
            set
            {
                SetPropertyValue("DataType", ref _DataType, value);

            }
        }

        /* [VisibleInListView(false),VisibleInDetailView(false)]
         [TypeConverter(typeof(LocalizedClassInfoTypeConverter))]
         [ImmediatePostData, NonPersistent]
         public Type DataType
         {
             get
             {
                 if (objectTypeName != null)
                 {
                     return DevExpress.Persistent.Base.ReflectionHelper.GetType(objectTypeName);
                 }
                 return null;
             }
             set
             {
                 string stringValue = value == null ? null : value.FullName;
                 string savedObjectTypeName = ObjectTypeName;
                 try
                 {
                     if (stringValue != objectTypeName)
                     {
                         ObjectTypeName = stringValue;
                     }
                 }
                 catch (Exception)
                 {
                     ObjectTypeName = savedObjectTypeName;
                 }
                 criteria = null;
             }
         }*/
    }

}
