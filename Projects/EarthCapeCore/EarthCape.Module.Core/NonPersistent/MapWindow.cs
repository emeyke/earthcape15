using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;
using DevExpress.Data.Filtering;

namespace EarthCape.Module
{
    [NonPersistent]
    public class MapWindow : XPCustomObject
    {
        public MapWindow(Session session) : base(session) { }
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public MapProject MapProject;
        public string Name
        {
            get
            {
                if (MapProject!=null)
                return MapProject.Name;
                return "";
            }
         }
        /*   [VisibleInDetailView(false)]
     private Unit _UnitTemplate;
        [ImmediatePostData(true)]
        public Unit UnitTemplate
        {
            get
            {
                return _UnitTemplate;
            }
            set
            {
                SetPropertyValue("UnitTemplate", ref _UnitTemplate, value);
            }
        }*/
     /*   public override void AfterConstruction()
        {
            UnitTemplate = new Unit(Session);
            UnitTemplate.Date = DateTime.Now;
            //UnitTemplate.Locality = loc;
           // User user = (User)Session.FindObject<User>(new BinaryOperator("Oid", ((User)SecuritySystem.CurrentUser).Oid));// GetObjectByKey(SecuritySystem.CurrentUser.GetType(), Session.GetKeyValue(SecuritySystem.CurrentUser));
           // UnitTemplate.CollectedOrObservedBy.Add(user);
            UnitTemplate.CollectedOrObservedByText = ((User)SecuritySystem.CurrentUser).FullName;
            UnitTemplate.Group = (Value_WindowController.Instance().GetGroup(Session));
            if (UnitTemplate.Group != null)
                if (UnitTemplate.Group.DefaultNames.Count>0)
            {
                UnitTemplate.TaxonomicName = UnitTemplate.Group.DefaultNames[0];
                UnitTemplate.AccessionNumber = UnitTemplate.Group.UnitPrefix + UnitTemplate.Group.UnitPrefixSeparator;
            }
            base.AfterConstruction();
        }*/
    }

}
