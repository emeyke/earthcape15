using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;

namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class SelectIName : XPCustomObject
    {
        public SelectIName(Session session) : base(session) { }
        public IName MergeTo;
       
    }

}
