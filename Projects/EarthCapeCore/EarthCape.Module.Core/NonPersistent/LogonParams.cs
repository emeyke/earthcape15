using System;

using DevExpress.ExpressApp.Security;
using System.ComponentModel;
using DevExpress.Persistent.Validation;
using EarthCape.Module;
using DevExpress.Xpo;

namespace EarthCape.Logon
{
 
    [NonPersistent]
    public class LogonParams : AuthenticationStandardLogonParameters
    {
        public LogonParams() : base() { }
     
        private string firstName;
        [RuleRequiredField("LogonParamsFirstNameRule", "Save", "First name cannot be empty")]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        private string lastName;
        [RuleRequiredField("LogonParamsLastNameRule", "Save", "Last name cannot be empty")]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        private string email;
        [RuleRequiredField("LogonParamsEmailRule", "Save", "Email cannot be empty")]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string _Organization;
        [RuleRequiredField("LogonParamsOrgRule", "Save", "Organization cannot be empty")]
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        private ServerType _ServerType = ServerType.MSSql;
        public ServerType ServerType
        {
            get { return _ServerType; }
            set { _ServerType = value; }
        }
        private string _Host;
        public string Host
        {
            get { return _Host; }
            set { _Host = value; }
        }
        private string _DatabaseName;
        public string DatabaseName
        {
            get { return _DatabaseName; }
            set { _DatabaseName = value; }
        }
        private string _AdditionalParameters;
        public string AdditionalParameters
        {
            get { return _AdditionalParameters; }
            set { _AdditionalParameters = value; }
        }

        private string _DatabaseUser;
        public string DatabaseUser
        {
            get { return _DatabaseUser; }
            set { _DatabaseUser = value; }
        }
        private string _DatabasePassword;
        [PasswordPropertyTextAttribute]
        public string DatabasePassword
        {
            get { return _DatabasePassword; }
            set { _DatabasePassword = value; }
        }
        private string _NewUser;
        public string NewUser
        {
            get { return _NewUser; }
            set { _NewUser = value; }
        }
        private string _NewPassword;
        [Custom("IsPassword", "True")]
        public string NewPassword
        {
            get { return _NewPassword; }
            set { _NewPassword = value; }
        }
    }
}
