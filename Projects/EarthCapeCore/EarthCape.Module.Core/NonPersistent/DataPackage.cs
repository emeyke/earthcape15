﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Core
{

    public class DataPackageRoot
    {
        public string profile { get; set; }
        public List<DataPackageResource> resources { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public List<DataPackageLicense> licenses { get; set; }
        public string description { get; set; }
        public string homepage { get; set; }
        public string version { get; set; }
        public List<DataPackageContributor> contributors { get; set; }
    }

    public class DataPackageResource
    {
        public string name { get; set; }
        public string path { get; set; }
        public string profile { get; set; }
        public DataPackageSchema schema { get; set; }
        public string description { get; set; }
    }

    public class DataPackageSchema
    {
        public List<DataPackageField> fields { get; set; }
    }

    public class DataPackageField
    {
        public string name { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string format { get; set; }
        public string description { get; set; }
    }


    public class DataPackageLicense
    {
        public string name { get; set; }
        public string title { get; set; }
        public string path { get; set; }
    }

    public class DataPackageContributor
    {
        public string title { get; set; }
        public string role { get; set; }
        public string organisation { get; set; }
        public string email { get; set; }
        public string path { get; set; }
    }

}
