using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class MoveToAccessionOptions : XPCustomObject
    {
        public MoveToAccessionOptions(Session session) : base(session) { }


        private Accession _Accession;
        public Accession Accession
        {
            get { return _Accession; }
            set { SetPropertyValue<Accession>(nameof(Accession), ref _Accession, value); }
        }

        private bool _OpenTargetAccession = true;
        public bool OpenTargetAccession
        {
            get { return _OpenTargetAccession; }
            set { SetPropertyValue<bool>(nameof(OpenTargetAccession), ref _OpenTargetAccession, value); }
        }




    }

}
