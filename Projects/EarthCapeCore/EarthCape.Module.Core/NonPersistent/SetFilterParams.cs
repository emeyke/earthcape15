using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class SetFilterParams : XPCustomObject
    {
        public SetFilterParams(Session session) : base(session) { }
        private Dataset _Dataset;
        private Project _Project;
        [ImmediatePostData]
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
                if (Dataset != null)
                    if (Dataset.Project != Project)
                        Dataset = null;
            }
        }

        [ImmediatePostData]
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
                if (Dataset != null)
                    if (Dataset.Project != null)
                        Project = Dataset.Project;
            }
        }
    }

}
