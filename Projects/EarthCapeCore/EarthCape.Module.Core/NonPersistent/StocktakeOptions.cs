using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class StocktakeOptions : XPCustomObject
    {
        public StocktakeOptions(Session session) : base(session) { }



        private StocktakeCondition _Condition;
        public StocktakeCondition Condition
        {
            get { return _Condition; }
            set { SetPropertyValue<StocktakeCondition>(nameof(Condition), ref _Condition, value); }
        }


        private int? _Count;
        public int? Count
        {
            get { return _Count; }
            set { SetPropertyValue<int?>(nameof(Count), ref _Count, value); }
        }


        private bool _Done=true;
        public bool Done
        {
            get { return _Done; }
            set { SetPropertyValue<bool>(nameof(Done), ref _Done, value); }
        }

        private string _Comment;
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get { return _Comment; }
            set { SetPropertyValue<string>(nameof(Comment), ref _Comment, value); }
        }




        private Store _Store;
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }





    }

}
