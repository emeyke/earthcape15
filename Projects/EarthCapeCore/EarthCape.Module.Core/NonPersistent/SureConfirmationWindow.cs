using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;

namespace EarthCape.Module
{
    [NonPersistent]
    public class SureConfirmationWindow : XPCustomObject
    {
        public SureConfirmationWindow(Session session) : base(session) { }
      
    }

}
