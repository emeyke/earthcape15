using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using EarthCape.Module;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class NameList : XPCustomObject
    {
        public NameList(Session session) : base(session) { }
        private string _Names;
        [Size(SizeAttribute.Unlimited)]
        public string Names
        {
            get
            {
                return _Names;
            }
            set
            {
                SetPropertyValue("Names", ref _Names, value);
            }
        }
    }

}
