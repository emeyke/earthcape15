using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.Security;
using EarthCape.Module.Objects;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    [ModelDefault("Caption", "Register User")]
    [ImageName("BO_User")]
    public class RegisterUserParameters : ILogonActionParameters {
        public const string ValidationContext = "RegisterUserContext";
        [RuleRequiredField(null, ValidationContext)]
        public string Organization { get; set; }
        [RuleRequiredField(null, ValidationContext)]
        public string UserName { get; set; }
        [RuleRequiredField(null, ValidationContext)]
        public string FirstName { get; set; }
        [RuleRequiredField(null, ValidationContext)]
        public string LastName { get; set; }
        [RuleRequiredField(null, ValidationContext)]
        public string Password { get; set; }
        [RuleRequiredField(null, ValidationContext)]
        [RuleRegularExpression(null, ValidationContext, EarthCapeObjectsModule.EmailPattern)]
        public string Email { get; set; }
        public void Process(IObjectSpace objectSpace) {
            User user = objectSpace.FindObject<User>(new BinaryOperator("UserName", UserName));
            if (user != null)
                throw new ArgumentException("The login with the entered UserName or Email was already registered within the system");
            else
                Updater.CreateUser(objectSpace,FirstName,LastName, UserName, Email, Password,Organization, false);
          //  throw new UserFriendlyException("A new user has successfully been registered");
        }
    }
}
