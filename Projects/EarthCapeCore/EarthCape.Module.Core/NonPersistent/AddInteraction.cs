using System;
using System.ComponentModel;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo.Metadata;
using System.Collections.Generic;

namespace EarthCape.Module
{
    [NonPersistent]
    public class AddInteraction : XPCustomObject
    {
        public AddInteraction(Session session) : base(session) { }
        private bool _Modify = false;
        public bool Modify
        {
            get
            {
                return _Modify;
            }
            set
            {
                SetPropertyValue("Modify", ref _Modify, value);
            }
        }
        private String _Unit1;
        public String Unit1
        {
            get
            {
                return _Unit1;
            }
            set
            {
                SetPropertyValue("Unit1", ref _Unit1, value);
            }
        }
        private TaxonomicName _Taxon;
        public TaxonomicName TaxonomicName
        {
            get
            {
                return _Taxon;
            }
            set
            {
                SetPropertyValue("TaxonomicName", ref _Taxon, value);
            }
        }
        private Group _Group;
        public Group Group
        {
            get
            {
                return _Group;
            }
            set
            {
                 _Group = value; 
            }
        }
        private Store _Store;
        public Store Store
        {
            get
            {
                return _Store;
            }
            set
            {
                SetPropertyValue("Store", ref _Store, value);
            }
        }
        private DateTime _DateTime;
        [ValueConverter(typeof(Xpand.Xpo.Converters.ValueConverters.XpandUtcDateTimeConverter))]
        public DateTime DateTime
        {
            get
            {
                return _DateTime;
            }
            set
            {
                SetPropertyValue("DateTime", ref _DateTime, value);
            }
        }

        private int _Quantity;
        public int Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetPropertyValue("Quantity", ref _Quantity, value);
            }
        }
        private string _AccessionNumber;
        public string AccessionNumber
        {
            get
            {
                return _AccessionNumber;
            }
            set
            {
                SetPropertyValue("AccessionNumber", ref _AccessionNumber, value);
            }
        }
    /*    private IList<UnsavedInteraction> _UnsavedRecords;
        public IList<UnsavedInteraction> UnsavedRecords
        {
            get { return _UnsavedRecords; }
        }*/

    }
 /*   public class UnsavedInteraction
    {
        public string AccessionNumber { get; set; }
        public Guid TaxonomicName { get; set; }
    }*/

}
