
using DevExpress.ExpressApp;

using DevExpress.Persistent.Base;

using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.ComponentModel;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class MapWindowToImage : XPCustomObject
    {
        public MapWindowToImage(Session session) : base(session) { }
        private int _Height = 1000;
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                SetPropertyValue("Height", ref _Height, value);
            }
        }
        private int _Width = 1000;
        public int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                SetPropertyValue("Width", ref _Width, value);
            }
        }
        private int _Compression = 100;
        public int Compression
        {
            get
            {
                return _Compression;
            }
            set
            {
                SetPropertyValue("Compression", ref _Compression, value);
            }
        }
        private int _Dpi = 300;
        public int Dpi
        {
            get
            {
                return _Dpi;
            }
            set
            {
                SetPropertyValue("Dpi", ref _Dpi, value);
            }
        }
    }

}
