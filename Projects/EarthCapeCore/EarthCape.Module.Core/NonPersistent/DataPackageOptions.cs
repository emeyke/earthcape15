using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class DataPackageOptions : XPCustomObject
    {
        public DataPackageOptions(Session session) : base(session) { }


        private string _Name = String.Format("datapackage-{0:yy-MM-dd-hh-mm-ss}",DateTime.Now);
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue<string>(nameof(Name), ref _Name, value); }
        }

        private string _CoreViews="DataPackage_Unit_ListView";
        [Size(SizeAttribute.Unlimited)]
        public string CoreViews
        {
            get { return _CoreViews; }
            set { SetPropertyValue<string>(nameof(CoreViews), ref _CoreViews, value); }
        }

        private string _ReferenceViews=String.Format("DataPackage_TaxonomicName_ListView{0}DataPackage_Locality_ListView", Environment.NewLine);
        [Size(SizeAttribute.Unlimited)]
        public string ReferenceViews
        {
            get { return _ReferenceViews; }
            set { SetPropertyValue<string>(nameof(ReferenceViews), ref _ReferenceViews, value); }
        }

        private bool _ExportExcel=true;
        public bool ExportExcel
        {
            get { return _ExportExcel; }
            set { SetPropertyValue<bool>(nameof(ExportExcel), ref _ExportExcel, value); }
        }

        private bool _ExportDataPackage=true;
        public bool ExportDataPackage
        {
            get { return _ExportDataPackage; }
            set { SetPropertyValue<bool>(nameof(ExportDataPackage), ref _ExportDataPackage, value); }
        }







    }

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            