using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using EarthCape.Module.Logistics;
using System;

namespace EarthCape.Module.Core
{
    [NonPersistent]
    public class CreateDerivedOptions : XPCustomObject
    {
        public CreateDerivedOptions(Session session) : base(session) { }


        private int _DerivedPerUnit=1;
        public int DerivedPerUnit
        {
            get { return _DerivedPerUnit; }
            set { SetPropertyValue<int>(nameof(DerivedPerUnit), ref _DerivedPerUnit, value); }
        }

        private Accession _Accession;
        public Accession Accession
        {
            get { return _Accession; }
            set { SetPropertyValue<Accession>(nameof(Accession), ref _Accession, value); }
        }

        private bool _CopyAccessionDataFromCurrentIfNew=true;
        public bool CopyAccessionDataFromCurrentIfNew
        {
            get { return _CopyAccessionDataFromCurrentIfNew; }
            set { SetPropertyValue<bool>(nameof(CopyAccessionDataFromCurrentIfNew), ref _CopyAccessionDataFromCurrentIfNew, value); }
        }

        private bool _OpenTargetAccession=true;
        public bool OpenTargetAccession
        {
            get { return _OpenTargetAccession; }
            set { SetPropertyValue<bool>(nameof(OpenTargetAccession), ref _OpenTargetAccession, value); }
        }

        private Store _Store;
        public Store Store
        {
            get { return _Store; }
            set { SetPropertyValue<Store>(nameof(Store), ref _Store, value); }
        }

        private string _Suffix="-";
        public string Suffix
        {
            get { return _Suffix; }
            set { SetPropertyValue<string>(nameof(Suffix), ref _Suffix, value); }
        }






    }

}
