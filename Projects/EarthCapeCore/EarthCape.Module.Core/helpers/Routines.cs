using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Utils;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.OleDb;
using DevExpress.ExpressApp.Updating;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System.Drawing;
using System.IO;

namespace EarthCape.Module
{

    public class Routines
    {
        public static string GetShortPartyName(Party item)
        {
            if (item is User) return ((User)item).FirstName;
            else if (!string.IsNullOrEmpty(((Group)item).ShortName)) return ((Group)item).ShortName;
            else return ((Group)item).Name;
        }

    /*    public static void ShareSet(Dataset ds, Group p, ObjectSpace os, bool isOwner)
        {
            if ((ds == null) || (p == null)) return;
            Sharing sh = os.Session.FindObject<Sharing>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Dataset.Oid", ds.Oid), new BinaryOperator("Group.Oid", p.Oid)));
            if (sh==null)
             sh = os.FindObject<Sharing>(CriteriaOperator.And(new BinaryOperator("Dataset.Oid", ds.Oid), new BinaryOperator("Group.Oid", p.Oid)));
            if (sh == null)
            {
                sh = os.CreateObject<Sharing>();
                sh.Dataset = ds;
                sh.Group = (Group)p;
                sh.Save();
            }
            //os.CommitChanges();
        }*/
        public static User GetUser(ObjectSpace os, Session se, string p)
        {
            User c = se.FindObject<User>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", p));
            if (c == null)
                c = os.FindObject<User>(new BinaryOperator("FullName", p));
            if (c == null)
            {
                string[] si = p.Split(' ');
                if (si[0] == "") { return null; }
                c = os.CreateObject<User>();
                c.FirstName = si[0];
                if (si.Length > 1)
                {
                    c.LastName = si[1];
                }
                c.Save();
                //os.CommitChanges();
            }
            return c;
        }
        public static Group GetGroup(ObjectSpace os, Session se, string project)
        {
            Group prj = se.FindObject<Group>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", project));
            if (prj == null)
                prj = os.FindObject<Group>(new BinaryOperator("Name", project));
            if (prj == null)
            {
                prj = os.CreateObject<Group>();
                prj.Name = project;
                //prj.Name = project;
                prj.Save();
                //os.CommitChanges();
            }
            return prj;
        }
        /*   public static Person StringToContact(ObjectSpace objectSpace, Session se, string strContact)
           {
               string lastname = strContact;
               string firstname = "";
               if (strContact.IndexOf(",") > -1)
               {
                   lastname = strContact.Substring(0, strContact.IndexOf(","));
                   if (strContact.IndexOf(",") < (strContact.Length - 3))
                   {
                       firstname = strContact.Substring(strContact.IndexOf(",") + 2, strContact.Length - strContact.IndexOf(",") - 2);
                   }
               }
               else
               {
                   if (strContact.IndexOf(" ") > -1)
                   {
                       firstname = strContact.Substring(0, strContact.IndexOf(" "));
                       lastname = strContact.Substring(strContact.IndexOf(" ") + 2, strContact.Length - strContact.IndexOf(" ") - 2);

                   }
               }
               Person cont;
               if (firstname == "")
               {
                   cont = se.FindObject<Person>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("LastName", lastname));
                   if (cont == null)
                       cont = objectSpace.FindObject<Person>(new BinaryOperator("LastName", lastname));
               }
               else
               {
                   cont = se.FindObject<Person>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", firstname + " " + lastname));
                   if (cont == null)
                       cont = objectSpace.FindObject<Person>(new BinaryOperator("FullName", firstname + " " + lastname));
               }
               if (cont == null)
               {
                   cont = objectSpace.CreateObject<Person>();
                   cont.FirstName = firstname;
                   cont.LastName = lastname;
                   cont.Save();
                   //objectSpace.CommitChanges();
               }
               return cont;
           }*/
        public static Party GetOrg(ObjectSpace objectSpace, Session se, string p)
        {
            Group c = se.FindObject<Group>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", p));
            if (c == null)
                c = objectSpace.FindObject<Group>(new BinaryOperator("FullName", p));
            if (c == null)
            {
                c = objectSpace.CreateObject<Group>();
                c.Name = p;
                c.Save();
                //objectSpace.CommitChanges();
            }
            return c;
        }
        public static Term GetTerm(ObjectSpace objectSpace, Session se, string p)
        {
            Term s = se.FindObject<Term>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
            if (s == null)
                s = objectSpace.FindObject<Term>(new BinaryOperator("Word", p));
            if (s == null)
            {
                s = objectSpace.CreateObject<Term>();
                s.Word = p;
                s.Save();
                //objectSpace.CommitChanges();
            }
            return s;
        }
        /*public static void ClassifyTaxon(ObjectSpace os, Session se, TaxonomicName TaxonomicName, Term parent, TaxonClassification cl)
        {
            CriteriaOperator cr_term = CriteriaOperator.And(
                new BinaryOperator("TaxonomicName.Oid", TaxonomicName.Oid),
                new BinaryOperator("Classification.Oid", cl.Oid)
                );
            CriteriaOperator cr_parent = CriteriaOperator.And(
                  new BinaryOperator("TaxonomicName.Oid", TaxonomicName.Oid),
                  new BinaryOperator("Classification.Oid", cl.Oid)
                  );
            TaxonClassified cl_parent = null;
            TaxonClassified cl_term = null;
            if ((se.FindObject<TaxonClassified>(PersistentCriteriaEvaluationBehavior.InTransaction, cr_parent) == null)
                && (se.FindObject<TaxonClassified>(cr_parent) == null))
            {
                cl_parent = os.CreateObject<TaxonClassified>();
                cl_parent.TaxonomicName = TaxonomicName;
                //cl_parent.Datasets.Add(cl);
                cl_parent.Save();
            }
            if ((se.FindObject<TaxonClassified>(PersistentCriteriaEvaluationBehavior.InTransaction, cr_term) == null)
                   && (se.FindObject<TaxonClassified>(cr_term) == null))
            {
                cl_term = os.CreateObject<TaxonClassified>();
                cl_term.TaxonomicName = TaxonomicName;
                if (cl_parent != null)
                    cl_term.Parent = cl_parent;
                //cl_term.Datasets.Add(cl);
                cl_term.Save();
                //objectSpace.CommitChanges();                
            }
        }*/
    
        /*    public static TagCategoryInt32 GetTagCategoryInt32(ObjectSpace objectSpace, string p)
      {
          TagCategoryInt32 s = objectSpace.FindObject<TagCategoryInt32>(new BinaryOperator("Word", p));
          if (s == null)
          {
              s = objectSpace.CreateObject<TagCategoryInt32>();
              s.Word = p;
              s.Save();
              objectSpace.CommitChanges();
          }
          return s;
      }*/

        public static bool UserIsGroupMember(Guid userOid, Guid groupOid, ObjectSpace os)
        {
            return (os.FindObject<GroupMembership>(CriteriaOperator.And(new BinaryOperator("Member.Oid", userOid), new BinaryOperator("Group.Oid", groupOid))) != null);
        }

    
     /*   public static TGIS_LayerAbstract GISCreateFileLayer(FileAttachmentMap file)
        {
             FileData fileData = file.File;
            string tempDirectory = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("B"));
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + Path.GetExtension(fileData.FileName));
            FileStream stream = new FileStream(tempFileName, FileMode.CreateNew);
            fileData.SaveToStream(stream);
            stream.Close();
            TGIS_LayerAbstract ll = TGIS_Utils.GisCreateLayer(Path.GetFileName(tempFileName), tempFileName);
            ll.Name = file.Oid.ToString();
            ll.Caption = file.File.FileName;         
            return ll;
        }*/
    /* 
        }*/




        internal static int GetGroupChildrenDepth(Group group, int depth)
        {
      //      Collection<int> list = new Collection<int>();
            foreach (Group item in group.Children)
            {
                int cur_depth=GetGroupChildrenDepth(item,depth+1);
                if (cur_depth > depth) depth = cur_depth;
  //              list.Add(cur_depth);
            }
            return depth;
        }
    }
}
