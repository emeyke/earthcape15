using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Utils;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using EarthCape.Module;
using EarthCape.Module.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;

namespace EarthCape.Module.Core
{
    public class UnitRoutines
    {
        /*   public static string GenerateID(IDGenerator gen,Unit unit)
           {
               string temp = "";
               gen.IDParts.Sorting = new SortingCollection(gen.IDParts, new SortProperty("Order", SortingDirection.Ascending));
               int? running=null;
               IDPartRunningNumber part = null;
               foreach (IDPart item in gen.IDParts)
               {
                   if (temp != "") temp = temp + gen.PartSeparator;
                   if (item is IDPartOldID)
                       if (unit.DerivedFrom != null)
                           temp = temp + unit.DerivedFrom.AccessionNumber;
                   if (item is IDPartStore)
                       if (unit.Store != null)
                           temp = temp + unit.Store.Name;
                   if (item is IDPartGroupDefaultPrefix)
                       if (unit.Project != null)
                       {
                           temp = temp + unit.Project.UnitPrefix;
                       }
                   if (item is IDPartText)
                       temp = temp + ((IDPartText)item).Text;
                   if (item is IDPartRunningNumber)
                   {
                       running = ((IDPartRunningNumber)item).StartingNumber;
                       part = (IDPartRunningNumber)item;
                       temp = temp + running;
                     }
               }
               if (part != null)
               {
                   part.StartingNumber += 1;
                   part.Save();
               }
               if (!string.IsNullOrEmpty(gen.ReplaceSpaceWith))
                   temp = temp.Replace(" ", gen.ReplaceSpaceWith);
               return temp;
           }*/
        /*public static Sampling GetSampling(ObjectSpace os, Session se, string location, PlaceArea l, string date1, string date2)
        {
            PlaceArea loc;
            if (l == null)
            {
                loc = PlaceRoutines.GetPlace(os,se, location);
            }
            else
            {
                loc = l;
            }
            Sampling t = se.FindObject<Sampling>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("DisplayName", loc.Name + " (" + date1 + ")"));
            if (t == null)
                t = os.FindObject<Sampling>(new BinaryOperator("DisplayName", loc.Name + " (" + date1 + ")"));
            if (t == null)
            {
                t = os.CreateObject<Sampling>();
                t.Place = loc;

                if (date1 != "")
                {
                    t.Date = DateTime.Parse(date1);
                }
                if (date2 != "")
                {
                    t.EndOn = DateTime.Parse(date2);
                }
                t.Save();
                //os.CommitChanges();
            }
            return t;
        }*/
        /* public static UnitCreationEvent GetUnitCreationEvent(ObjectSpace os,Session se, string personCreated, string date1)
         {
             UnitCreationEvent t = se.FindObject<UnitCreationEvent>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("DisplayName", personCreated + " (" + date1 + ")"));
             if (t == null)
              t = os.FindObject<UnitCreationEvent>(new BinaryOperator("DisplayName", personCreated + " (" + date1 + ")"));
             if (t == null)
             {
                 t = os.CreateObject<UnitCreationEvent>();
                 if (date1 != "")
                 {
                     t.Date = DateTime.Parse(date1);
                 }
                 if (personCreated != "")
                 {
                     //t.ResponsibleParty = Routines.GetUser(os, se, personCreated);
                 }
                 t.Save();
                 //os.CommitChanges();
             }
             return t;
         }*/
        /*    public static UnitType GetUnitType(ObjectSpace objectSpace, Session se, string p)
            {
                UnitType unit_type = se.FindObject<UnitType>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
                if (unit_type == null)
                    unit_type = objectSpace.FindObject<UnitType>(new BinaryOperator("Word", p));
                if (unit_type == null)
                {
                    unit_type = objectSpace.CreateObject<UnitType>();
                    unit_type.Word = p;
                    unit_type.Save();
                    //objectSpace.CommitChanges();
                }
                return unit_type;
            }*/
        /*   public static UnitMedium GetMedium(ObjectSpace objectSpace,Session se, string p)
           {
               UnitMedium storage_medium = se.FindObject<UnitMedium>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
               if (storage_medium == null)
              storage_medium = objectSpace.FindObject<UnitMedium>(new BinaryOperator("Word", p));
               if (storage_medium == null)
               {
                   storage_medium = objectSpace.CreateObject<UnitMedium>();
                   storage_medium.Word = p;
                   storage_medium.Save();
                   //objectSpace.CommitChanges();
               }
               return storage_medium;
           }*/
        /*  public static string GetUnitLocalityClassificationString(Locality loc, Session se, Project project)
          {
              if (project.DefaultLocalityClassification==null) return "";
              LocalityClassification classif = project.DefaultLocalityClassification;
              LocalityClassified locclass = se.FindObject<LocalityClassified>(CriteriaOperator.And(new BinaryOperator("Locality.Oid", loc.Oid), new BinaryOperator("Category.Classification.Oid", classif.Oid)));
              if (locclass == null) return "";
              if (locclass.Category == null) return "";
              LocalityClassificationItem classitem = locclass.Category;
              if (classitem.Locality == null) return "";
              string result = classitem.Locality.Name;
              while (classitem.Parent!=null)
              {
                  classitem=classitem.Parent;
                  result = String.Format("{0}/{1}", classitem.Locality.Name, result);
              }
              return result;
          }*/
        public static Store GetUnitCollection(IObjectSpace objectSpace, Session se, string p, Project project)
        {
            Store storage = se.FindObject<Store>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", p));
            if (storage == null)
                storage = objectSpace.FindObject<Store>(new BinaryOperator("Name", p));
            if (storage == null)
            {
                storage = objectSpace.CreateObject<Store>();
                storage.Name = p;
                //storage.RecordOwner = recordOwner;
                storage.Save();
                //if (project != null)
                //if (storage.Datasets.IndexOf(ds)<0)
                //storage.Datasets.Add(ds);
                //objectSpace.CommitChanges();
            }
            return storage;
        }
        /*   public static DevStage GetStage(ObjectSpace objectSpace,Session se, string p)
           {
               DevStage stage = se.FindObject<DevStage>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
               if (stage == null)
              stage = objectSpace.FindObject<DevStage>(new BinaryOperator("Word", p));
               if (stage == null)
               {
                   stage = objectSpace.CreateObject<DevStage>();
                   stage.Word = p;
                   stage.Save();
                   //objectSpace.CommitChanges();
               }
               return stage;
           }
           public static DevStageZoo GetStageZoo(ObjectSpace objectSpace,Session se, string p)
           {
               DevStageZoo s = se.FindObject<DevStageZoo>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
               if (s == null)
                   s = objectSpace.FindObject<DevStageZoo>(new BinaryOperator("Word", p));
               if (s == null)
               {
                   s = objectSpace.CreateObject<DevStageZoo>();
                   s.Word = p;
                   s.Save();
                   //objectSpace.CommitChanges();
               }
               return s;
           }
           public static DevStageBot GetStageBot(ObjectSpace objectSpace,Session se, string p)
           {
               DevStageBot s = se.FindObject<DevStageBot>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
               if (s == null)
             s = objectSpace.FindObject<DevStageBot>(new BinaryOperator("Word", p));
               if (s == null)
               {
                   s = objectSpace.CreateObject<DevStageBot>();
                   s.Word = p;
                   s.Save();
                   //objectSpace.CommitChanges();
               }
               return s;
           }
           public static DevStageMyco GetStageMyco(ObjectSpace objectSpace,Session se, string p)
           {
               DevStageMyco s = se.FindObject<DevStageMyco>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", p));
               if (s == null)
                s = objectSpace.FindObject<DevStageMyco>(new BinaryOperator("Word", p));
               if (s == null)
               {
                   s = objectSpace.CreateObject<DevStageMyco>();
                   s.Word = p;
                   s.Save();
                   //objectSpace.CommitChanges();
               }
               return s;
           }*/
    }
}
