﻿using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;

namespace EarthCape.Module.Core
{
    public class AttachmentHelper
    {
        public static Bitmap GenerateThumbnailFromUrl(string Url)
        {
            if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
            {
                WebRequest req = WebRequest.Create(Url);
                WebResponse response = req.GetResponse();
                Stream stream = response.GetResponseStream();
                if (stream != null)
                {
                    return AttachmentHelper.CreateThumbnail(stream, 100, 100);
                }

            }
            return null;
        }

        public static float? GetLatitude(Image targetImg)
        {
            try
            {
                //Property Item 0x0001 - PropertyTagGpsLatitudeRef
                PropertyItem propItemRef = targetImg.GetPropertyItem(1);
                //Property Item 0x0002 - PropertyTagGpsLatitude
                PropertyItem propItemLat = targetImg.GetPropertyItem(2);
                return ExifGpsToFloat(propItemRef, propItemLat);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
        public static float? GetLongitude(Image targetImg)
        {
            try
            {
                ///Property Item 0x0003 - PropertyTagGpsLongitudeRef
                PropertyItem propItemRef = targetImg.GetPropertyItem(3);
                //Property Item 0x0004 - PropertyTagGpsLongitude
                PropertyItem propItemLong = targetImg.GetPropertyItem(4);
                return ExifGpsToFloat(propItemRef, propItemLong);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
        private static float ExifGpsToFloat(PropertyItem propItemRef, PropertyItem propItem)
        {
            uint degreesNumerator = BitConverter.ToUInt32(propItem.Value, 0);
            uint degreesDenominator = BitConverter.ToUInt32(propItem.Value, 4);
            float degrees = degreesNumerator / (float)degreesDenominator;

            uint minutesNumerator = BitConverter.ToUInt32(propItem.Value, 8);
            uint minutesDenominator = BitConverter.ToUInt32(propItem.Value, 12);
            float minutes = minutesNumerator / (float)minutesDenominator;

            uint secondsNumerator = BitConverter.ToUInt32(propItem.Value, 16);
            uint secondsDenominator = BitConverter.ToUInt32(propItem.Value, 20);
            float seconds = secondsNumerator / (float)secondsDenominator;

            float coorditate = degrees + (minutes / 60f) + (seconds / 3600f);
            string gpsRef = System.Text.Encoding.ASCII.GetString(new byte[1] { propItemRef.Value[0] }); //N, S, E, or W
            if (gpsRef == "S" || gpsRef == "W")
                coorditate = 0 - coorditate;
            return coorditate;
        }

        public static PropertyItem[] GetMetadata(string Url)
        {
            if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
            {
                WebRequest req = WebRequest.Create(Url);
                WebResponse response = req.GetResponse();
                Stream stream = response.GetResponseStream();
                if (stream != null)
                {
                    Bitmap image = new Bitmap(stream);
                    return image.PropertyItems;
                }

            }

            return null;

        }
   
        public static Bitmap GetBitmapFromUrl(string Url)
        {
           // return null;
            //if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
            {
                WebRequest req = WebRequest.Create(Url);
                try
                {
                    WebResponse response = req.GetResponse();
                    Stream stream = response.GetResponseStream();
                    if (stream != null)
                    {
                        return new Bitmap(stream);
                    }
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                { }

                return null;

            }
        }
        /*        public static IEnumerable<MetadataExtractor.Directory> ExtractMetadata(string Url)
                {
                    if ((GeneralHelper.IsRecognisedImageFile(Url)) && (!String.IsNullOrEmpty(Url)))
                    {
                        HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(Url);
                        WebResponse myResp = myReq.GetResponse();

                        byte[] b = null;
                        using (Stream stream = myResp.GetResponseStream())
                        {
                            MemoryStream ms = new MemoryStream();
                            int count = 0;
                            do
                            {
                                byte[] buf = new byte[1024];
                                count = stream.Read(buf, 0, 1024);
                                ms.Write(buf, 0, count);
                            } while (stream.CanRead && count > 0);
                            return ImageMetadataReader.ReadMetadata(ms);
                        }

                    }
                    return null;
                }
        */
        public static Bitmap CreateThumbnail(Stream lcFilename, int lnWidth, int lnHeight)
        {
            Bitmap loBMP = new Bitmap(lcFilename);
            CreateThumbnailFromBitmap(loBMP, lnWidth, lnHeight);
            return loBMP;
        }
        public static Bitmap CreateThumbnailFromBitmap(Bitmap loBMP, int lnWidth, int lnHeight)
        {
            try
            {
                ImageFormat loFormat = loBMP.RawFormat;

                decimal lnRatio;
                int lnNewWidth = 0;
                int lnNewHeight = 0;

                //*** If the image is smaller than a thumbnail just return it
                if (loBMP.Width < lnWidth && loBMP.Height < lnHeight)
                    return loBMP;

                if (loBMP.Width > loBMP.Height)
                {
                    lnRatio = (decimal)lnWidth / loBMP.Width;
                    lnNewWidth = lnWidth;
                    decimal lnTemp = loBMP.Height * lnRatio;
                    lnNewHeight = (int)lnTemp;
                }
                else
                {
                    lnRatio = (decimal)lnHeight / loBMP.Height;
                    lnNewHeight = lnHeight;
                    decimal lnTemp = loBMP.Width * lnRatio;
                    lnNewWidth = (int)lnTemp;
                }
                Bitmap bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);

                loBMP.Dispose();
                return bmpOut;
            }
            catch
            {
                return null;
            }
        }



    }
}
