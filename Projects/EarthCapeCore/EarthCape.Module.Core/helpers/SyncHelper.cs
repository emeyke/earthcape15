﻿
using System;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Xpo.DB;
using EarthCape.Module;
using EarthCape.Interfaces;
using System.Diagnostics;


namespace EarthCape.Sync
{
 
    
    public class SyncHelper
    {
        public static void SyncUpGroup(Group gr)
        {
            IDataLayer dataLayer = SyncHelper.GetDataLayer(SyncHelper.GetCloudString());
            using (UnitOfWork uow = new UnitOfWork(dataLayer))
            {
                SyncHelper.SyncGeneral(gr, uow);
                foreach (GroupMembership grm in gr.GroupMemberships)
                {
                    SyncHelper.SyncGeneral(grm, uow);
                }
             /*   foreach (TaxonomicName name in gr.DefaultNames)
                {
                    SyncHelper.SyncGeneral(name, uow);
         
                }*/
                uow.CommitChanges();
            }            
        }
        public static void SyncThis(BaseObject obj)
        {
            IDataLayer dataLayer = SyncHelper.GetDataLayer(SyncHelper.GetCloudString());
            using (UnitOfWork uow = new UnitOfWork(dataLayer))
            {
                SyncHelper.SyncGeneral(obj, uow);
                uow.CommitChanges();
            }

        }
        public static XPCustomObject SyncGeneral(XPCustomObject source, UnitOfWork uow)
        {
                        XPCustomObject target=null;
try
            {
                if (!(typeof(BaseObject).IsAssignableFrom(source.GetType()))) return null;
    
                if (source == null) return null;


                //check if object already exists
                if (typeof(BaseObject).IsAssignableFrom(source.GetType()))
                    target = (BaseObject)uow.FindObject(source.GetType(), new BinaryOperator("Oid", ((BaseObject)source).Oid));
                else
                    target = null;
               /* if ((typeof(AuditDataItemPersistent).IsAssignableFrom(source.GetType())))
                    target = (AuditDataItemPersistent)uow.FindObject(source.GetType(), new BinaryOperator("Oid", ((AuditDataItemPersistent)source).Oid));
                if ((typeof(AuditedObjectWeakReference).IsAssignableFrom(source.GetType())))
                    target = (AuditedObjectWeakReference)uow.FindObject(source.GetType(), new BinaryOperator("Oid", ((AuditedObjectWeakReference)source).Oid));
    */
                //check if object already loaded
                if ((typeof(BaseObject).IsAssignableFrom(source.GetType())))
                    target = (BaseObject)uow.FindObject(PersistentCriteriaEvaluationBehavior.InTransaction, source.GetType(), new BinaryOperator("Oid", ((BaseObject)source).Oid));
              /*  if ((typeof(AuditDataItemPersistent).IsAssignableFrom(source.GetType())))
                    target = (AuditDataItemPersistent)uow.FindObject(PersistentCriteriaEvaluationBehavior.InTransaction, source.GetType(), new BinaryOperator("Oid", ((AuditDataItemPersistent)source).Oid));
                if ((typeof(AuditedObjectWeakReference).IsAssignableFrom(source.GetType())))
                    target = (AuditedObjectWeakReference)uow.FindObject(PersistentCriteriaEvaluationBehavior.InTransaction, source.GetType(), new BinaryOperator("Oid", ((AuditedObjectWeakReference)source).Oid));
    */
             
                //create
                if (target == null)
                {
                    XPClassInfo targetitemClassInfo = uow.GetClassInfo(source.GetType());
                    target = (XPCustomObject)targetitemClassInfo.CreateObject(uow);
                    //if (typeof(BaseObject).IsAssignableFrom(target.GetType()))
                    //((BaseObject)target).Syncing = true;
                    target.Save();
                    //uow.CommitChanges();
                   // SyncSimplePropsGeneral(source, target, uow);
                    /* if (typeof(UnitBase).IsAssignableFrom(source.GetType()))
                     {
                         foreach (XPMemberInfo m in targetitemClassInfo.CollectionProperties)
                         {
                             if ((m.Name == "TagValues") || (m.Name == "Notes") || (m.Name == "Interactions") || (m.Name == "Interactions1"))
                             {
                                 XPBaseCollection targetCollection = (XPBaseCollection)m.GetValue(target);
                                 XPBaseCollection sourceCollection = (XPBaseCollection)m.GetValue(source);
                                 ArrayList array = new ArrayList(sourceCollection);
                                 foreach (BaseObject obj in array)
                                 {
                                     targetCollection.BaseAdd(SyncGeneral(obj, uow));
                                 }
                             }
                         }
                     }*/
                }
                else
                {
                   // SyncSimplePropsGeneral(source, target, uow);
                    if (typeof(IAudited).IsAssignableFrom(source.GetType()))
                    {
                     //   SyncAuditedProps((BaseObject)source, target, uow);
                    }
                }
              /*  if (typeof(IAudited).IsAssignableFrom(source.GetType()))
                {
                    XPCollection<AuditDataItemPersistent> audit = GetAuditTrail(MyObjectSpace(((BaseObject)source).Session.DataLayer), (BaseObject)source);
                    if (audit != null)
                    {
                        audit.Sorting = new SortingCollection(audit, new SortProperty("ModifiedOn", SortingDirection.Descending));
                        audit.Load();
                        foreach (AuditDataItemPersistent item in audit)
                        {
                            SyncGeneral(item, uow);
                        }
                    }
                }*/

            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                
            }
            return target;
        }
   /*     static public XPCollection<AuditDataItemPersistent> GetAuditTrail(ObjectSpace os, BaseObject obj)
        {
            AuditedObjectWeakReference auditObjectWR =os.FindObject<AuditedObjectWeakReference>(
                new GroupOperator(
                    new BinaryOperator("TargetType", os.Session.GetObjectType(obj)),
                    new BinaryOperator("TargetKey", AuditedObjectWeakReference.KeyToString(os.Session.GetKeyValue(obj)))
                ));
            if (auditObjectWR != null)
            {
                //auditObjectWR.AuditDataItems.BindingBehavior = CollectionBindingBehavior.AllowNone;
                return auditObjectWR.AuditDataItems;
            }
            else
                return null;
        }
        public static void SyncSimplePropsGeneral(XPCustomObject source, XPCustomObject target, UnitOfWork uow)
        {
            foreach (XPMemberInfo memberInfo in uow.GetClassInfo(target.GetType()).PersistentProperties)
            {
                // if (memberInfo is DevExpress.Xpo.Metadata.Helpers.ServiceField)
                     //continue;
                if (memberInfo.GetValue(source) == null)
                {
                    memberInfo.SetValue(target, null);
                    continue;
                }
                SetValue(source, target, uow, memberInfo);

            }
             target.Save();
        }

        public static void SyncAuditedProps(BaseObject source, XPCustomObject target, UnitOfWork uow)
        {
            XPCollection<AuditDataItemPersistent> audit = GetAuditTrail(MyObjectSpace(source.Session.DataLayer), source);
            if (audit == null) return;
            audit.Sorting = new SortingCollection(audit, new SortProperty("ModifiedOn", SortingDirection.Descending));
            audit.Load();
             XPCollection<AuditDataItemPersistent> auditTarget = AuditedObjectWeakReference.GetAuditTrail(target.Session, target);
            if (auditTarget != null)
            {
                auditTarget.Sorting = new SortingCollection(auditTarget, new SortProperty("ModifiedOn", SortingDirection.Descending));
                auditTarget.Load();
            }
            foreach (XPMemberInfo memberInfo in uow.GetClassInfo(target.GetType()).PersistentProperties)
            {
                 audit.Filter = new BinaryOperator("PropertyName", memberInfo.Name);
                audit.TopReturnedObjects = 1;
               
                if (auditTarget != null)
                {
                    auditTarget.Filter = new BinaryOperator("PropertyName", memberInfo.Name);
                    auditTarget.TopReturnedObjects = 1;
                }

                if (audit.Count > 0)
                {
                    if ((auditTarget != null) && (auditTarget.Count > 0))
                    {
                        if (audit[0].ModifiedOn > auditTarget[0].ModifiedOn)
                        {
                            SetValue(source, target, uow, memberInfo);
                        }
                    }
                    else
                    {
                        SetValue(source, target, uow, memberInfo);
                    }
                }
              }
            target.Save();
            uow.CommitChanges();
         
        }*/

        private static void SetValue(XPCustomObject source, XPCustomObject target, UnitOfWork uow, XPMemberInfo memberInfo)
        {
            object valSource = memberInfo.GetValue(source);
            if ((memberInfo.ReferenceType == null))
            {
                memberInfo.SetValue(target, valSource);
            }
            else
            {
                if (valSource == null)
                { memberInfo.SetValue(target, null); }
                else
                {
                    if ((typeof(User).IsAssignableFrom(source.GetType())) &&
                                  (typeof(User).IsAssignableFrom(valSource.GetType())))
                    {
                        if (((User)source).Oid == ((User)valSource).Oid)
                        {
                            memberInfo.SetValue(target, target);
                            return;
                        }
                    }
                    else
                        if (!typeof(XPObjectType).IsAssignableFrom(memberInfo.ReferenceType.ClassType))
                            memberInfo.SetValue(target, SyncGeneral((XPCustomObject)valSource, uow));
                }
            }
        }


        public static ObjectSpace MyObjectSpace(IDataLayer dataLayer)
        {
            ObjectSpace objectSpace = new ObjectSpace(new UnitOfWork(dataLayer), XafTypesInfo.Instance);
          
            return objectSpace;
        }

        public static string GetCloudString()
        {
            return MSSqlConnectionProvider.GetConnectionString("x72yzdxojv.database.windows.net", "earthcape","Delphius78","earthcape");
          //return MSSqlConnectionProvider.GetConnectionString(".\\SQLEXPRESS,1433", "users_111111");
            //return "tcp://64.151.111.133:2635/earthcape.rem";

         }

        public static UnitOfWork CloudUnitOfWork()
        {
            UnitOfWork unitOfWork = null;
            try
            {
                IDataLayer dataLayer = GetDataLayer(GetCloudString());
                unitOfWork = new UnitOfWork(dataLayer);
            }
            catch
            {
                throw (new Exception("Unable connect to EarthCape server"));
            }
            return unitOfWork;
        }

        public static string GetConnectionString(ServerType parametersServerType, string parametersHost, string parametersDatabaseUser, string parametersDatabasePassword, string parametersDatabaseName,string parametersAdditional)
        {
            string connstr;
            if (parametersServerType == ServerType.SqlCE)
                connstr = MSSqlCEConnectionProvider.GetConnectionString(parametersDatabaseName);
            else
                connstr = "";
            if (parametersServerType == ServerType.MySql)
                connstr = MySqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.Oracle)
                connstr = OracleConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword);
            if (parametersServerType == ServerType.MSSql)
                if ((string.IsNullOrEmpty(parametersDatabaseUser)) && (string.IsNullOrEmpty(parametersDatabasePassword)))
                    connstr = MSSqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseName);
                else
                {
                    connstr = MSSqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
                }
            if (parametersServerType == ServerType.Advantage)
                connstr = AdvantageConnectionProvider.GetConnectionString(parametersDatabaseName);
            if (parametersServerType == ServerType.Asa)
                connstr = AsaConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.Ase)
                connstr = AseConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.DB2)
                connstr = DB2ConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.Firebird)
                connstr = FirebirdConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.ODP)
                connstr = ODPConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword);
            if (parametersServerType == ServerType.Pervasive)
                connstr = PervasiveSqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.PostgreSql)
                connstr = PostgreSqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerType.SQLite)
                connstr = SQLiteConnectionProvider.GetConnectionString(parametersDatabaseName);
            if (parametersServerType == ServerType.VistaDB3)
                connstr = VistaDBConnectionProvider.GetConnectionString(parametersDatabaseName);
            if (parametersServerType == ServerType.XML)
                connstr = String.Format("XpoProvider=XmlDataSet;Data Source={0}", parametersDatabaseName);
            // if (parametersServerType == ServerType.EarthCapeCloud)
             //   connstr = "tcp://64.151.111.133:2635/earthcape.rem";
            //connstr = GetCloudString();
            if (parametersServerType == ServerType.Remoting)
                connstr = parametersHost;
           
            if (!string.IsNullOrEmpty(parametersAdditional))
                connstr = String.Format("{0};{1}", connstr, parametersAdditional);

            return connstr;
        }
   /*     public static User SyncUser(User user, ObjectSpace targetObjectSpace, Collection<Guid> processed, StringWriter errors)
        {
            if (user == null) return null;
            bool doBreak = false;
            if (processed.IndexOf(user.Oid) > -1)
                return (User)FindOb(user, targetObjectSpace, out doBreak, processed, errors, false);
            User targetUser = (User)FindOb(user, targetObjectSpace, out doBreak, processed, errors, true);
            return targetUser;
        }*/

        public static string GetConnectionStringExpress(ServerTypeExpress parametersServerType, string parametersHost, string parametersDatabaseUser, string parametersDatabasePassword, string parametersDatabaseName)
        {
            string connstr;
            if (parametersServerType == ServerTypeExpress.SqlCE)
                connstr = MSSqlCEConnectionProvider.GetConnectionString(parametersDatabaseName);
            else
                connstr = "";
            if (parametersServerType == ServerTypeExpress.MySql)
                connstr = MySqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerTypeExpress.Firebird)
                connstr = FirebirdConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerTypeExpress.PostgreSql)
                connstr = PostgreSqlConnectionProvider.GetConnectionString(parametersHost, parametersDatabaseUser, parametersDatabasePassword, parametersDatabaseName);
            if (parametersServerType == ServerTypeExpress.SQLite)
                connstr = SQLiteConnectionProvider.GetConnectionString(parametersDatabaseName);
          /*  if (parametersServerType == ServerTypeExpress.EarthCapeCloud)
                connstr = "tcp://64.151.111.133:2635/earthcape.rem";*/

            return connstr;
        }

        public static IDataLayer GetDataLayer(string connstr)
        {
            return XpoDefault.GetDataLayer(connstr, XafTypesInfo.XpoTypeInfoSource.XPDictionary, AutoCreateOption.DatabaseAndSchema);
        }

  

        public static void DownloadUser(string p, Session localSession)
        {
            if (string.IsNullOrEmpty(p))
                return;
            UnitOfWork uow = CloudUnitOfWork();
            User remoteUser = uow.FindObject<User>(new BinaryOperator("UserName", p));
            XPClassInfo remoteitemClassInfo = uow.GetClassInfo(typeof(User));
            XPClassInfo localitemClassInfo = localSession.GetClassInfo(typeof(User));
            User localUser = (User)localitemClassInfo.CreateObject(localSession);
            foreach (XPMemberInfo m in remoteitemClassInfo.PersistentProperties)
            {
                if (m is DevExpress.Xpo.Metadata.Helpers.ServiceField)
                    continue;
                object val = null;
                if ((m.ReferenceType != null))
                {
                }
                else
                {
                    val = m.GetValue(remoteUser);
                    m.SetValue(localUser, val);
                }
            }
        }
    }
}
