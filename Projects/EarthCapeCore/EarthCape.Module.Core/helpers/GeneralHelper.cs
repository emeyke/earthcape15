using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ViewVariantsModule;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using EarthCape.Module.Lab;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;
using Xpand.Persistent.Base.General;
using Xpand.Persistent.Base.RuntimeMembers.Model;
using ListView = DevExpress.ExpressApp.ListView;
using System.Drawing;

namespace EarthCape.Module.Core
{
    public class GeneralHelper
    {
        public static bool IsNullableEnum(Type t)
        {
            return t.IsGenericType &&
                   t.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                   t.GetGenericArguments()[0].IsEnum;
        }

        public static string LetterToColor(string fullName)
        {
            if (string.IsNullOrEmpty(fullName)) return "3498DB";
            try
            {
                 string letters = "abcdefghijklmnopqrstuvwxyz";
                string[] colors = new[]
                {
                    "008000", "0000FF", "800080", "800080", "FF00FF", "008080", "FFFF00", "808080", "00FFFF",
                    "000080",
                    "800000", "FF3939", "7F7F00", "C0C0C0", "FF6347", "FFE4B5", "33023", "B8860B", "C04000",
                    "6B8E23", "CD853F", "C0C000", "228B22", "D2691E", "808000", "20B2AA", "F4A460", "00C000",
                    "8FBC8B", "B22222", "843A05", "C00000"
                };
                return colors[letters.IndexOf(fullName)];
            }
            catch
            {
            }

            return "3498DB";
        }
        public static Color HexStringToColor(string hexColor)
        {
            string hc = ExtractHexDigits(hexColor);
            if (hc.Length != 6)
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("hexColor is not exactly 6 digits.");
                return Color.Empty;
            }
            string r = hc.Substring(0, 2);
            string g = hc.Substring(2, 2);
            string b = hc.Substring(4, 2);
            Color color = Color.Empty;
            try
            {
                int ri
                   = Int32.Parse(r, System.Globalization.NumberStyles.HexNumber);
                int gi
                   = Int32.Parse(g, System.Globalization.NumberStyles.HexNumber);
                int bi
                   = Int32.Parse(b, System.Globalization.NumberStyles.HexNumber);
                color = Color.FromArgb(ri, gi, bi);
            }
            catch
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("Conversion failed.");
                return Color.Empty;
            }
            return color;
        }
        /// <summary>
        /// Extract only the hex digits from a string.
        /// </summary>
        public static string ExtractHexDigits(string input)
        {
            // remove any characters that are not digits (like #)
            Regex isHexDigit
               = new Regex("[abcdefABCDEF\\d]+", RegexOptions.Compiled);
            string newnum = "";
            foreach (char c in input)
            {
                if (isHexDigit.IsMatch(c.ToString()))
                    newnum += c.ToString();
            }
            return newnum;
        }

        public static bool IsRecognisedImageFile(string fileName)
        {
            string targetExtension = System.IO.Path.GetExtension(fileName);
            if (String.IsNullOrEmpty(targetExtension))
                return false;
            else
                targetExtension = "*" + targetExtension.ToLowerInvariant();

            List<string> recognisedImageExtensions = new List<string>();

            foreach (System.Drawing.Imaging.ImageCodecInfo imageCodec in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
                recognisedImageExtensions.AddRange(imageCodec.FilenameExtension.ToLowerInvariant().Split(";".ToCharArray()));

            foreach (string extension in recognisedImageExtensions)
            {
                if (extension.Equals(targetExtension))
                {
                    return true;
                }
            }
            return false;
        }

        public static void FilterByName(string list, DevExpress.ExpressApp.ListView view)
        {
            string[] words = list.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            CriteriaOperator filter = null;
            Type type = view.ObjectTypeInfo.Type;
            foreach (String item in words)
            {
                if (typeof(Unit).IsAssignableFrom(type))
                {
                    if (ReferenceEquals(filter, null))
                        filter = new BinaryOperator("UnitID", item);
                    else
                        filter = CriteriaOperator.Or(filter, new BinaryOperator("UnitID", item));
                }
                else
                    if (typeof(TaxonomicName).IsAssignableFrom(type))
                {
                    if (ReferenceEquals(filter, null))
                        filter = CriteriaOperator.Or(new BinaryOperator("FullName", item), new BinaryOperator("ShortName", item));
                    else
                        filter = CriteriaOperator.Or(filter, new BinaryOperator("FullName", item), new BinaryOperator("ShortName", item));
                }
                else
                  if (!typeof(ICode).IsAssignableFrom(type))
                {
                    if (ReferenceEquals(filter, null))
                        filter = new BinaryOperator("Name", item);
                    else
                        filter = CriteriaOperator.Or(filter, new BinaryOperator("Name", item));
                }
                else
               if (ReferenceEquals(filter, null))
                    filter = CriteriaOperator.Or(new BinaryOperator("Name", item), new BinaryOperator("Code", item));
                else
                    filter = CriteriaOperator.Or(filter, new BinaryOperator("Name", item), new BinaryOperator("Code", item));
            }
            /*   IObjectSpace os =Application.CreateObjectSpace();
                  CollectionSource cs = new CollectionSource(os, typeof(Unit));
                      cs.Criteria["Units"] = filter;
                      e.ShowViewParameters.CreatedView = Application.CreateListView(Application.FindListViewId(typeof(Unit)), cs, true);
                      e.ShowViewParameters.NewWindowTarget = NewWindowTarget.Default;*/
            view.CollectionSource.Criteria[string.Empty] = filter;
        }

        public static Gene GetMarker(string markercode, IObjectSpace os)
        {
            Gene g = os.FindObject<Gene>(new BinaryOperator("Name", markercode));
            if (g == null)
            {
                g = os.CreateObject<Gene>();
                g.Name = markercode;
                g.Save();
            }
            return g;
        }

        public static void CreateViews(XPObjectSpace ObjectSpace)
        {
            Session currentSession = ObjectSpace.Session;
            Session newSession = new Session(currentSession.DataLayer);

            DropViewAndTable(newSession, "CustomDates");
            DropViewAndTable(newSession, "CustomValuesView");
            DropViewAndTable(newSession, "CusValLocVisUn");
            DropViewAndTable(newSession, "CusValCntPerPrjView");



            try
            {
                newSession.ExecuteNonQuery(@"
CREATE VIEW [dbo].[CustomDates]
AS
SELECT     dbo.CustomValue.DateTimeValue AS Value, dbo.CustomValue.Oid, dbo.CustomField.Name AS EventType, dbo.TaxonomicName.FullName AS TaxonomicName, 
                      dbo.CustomValue.CustomFieldTaxonomicName AS Event, dbo.Dataset.Name AS Dataset, { fn DAYOFYEAR(dbo.CustomValue.DateTimeValue) } AS DayOfYear, 
                      dbo.Project.Name AS Project, dbo.Dataset.Description AS DatasetDescription, dbo.Project.Description AS ProjectDescription
FROM         dbo.CustomValue INNER JOIN
                      dbo.Dataset ON dbo.CustomValue.Dataset = dbo.Dataset.Oid INNER JOIN
                      dbo.Project ON dbo.Dataset.Project = dbo.Project.Oid LEFT OUTER JOIN
                      dbo.TaxonomicName ON dbo.CustomValue.TaxonomicName = dbo.TaxonomicName.Oid LEFT OUTER JOIN
                      dbo.CustomField ON dbo.CustomValue.Category = dbo.CustomField.Oid
");

            }
            catch (Exception)
            {

            }
            try
            {
                newSession.ExecuteNonQuery(@"
CREATE VIEW [dbo].[CustomValuesView]
AS
SELECT     dbo.CustomValue.Oid, dbo.Project.Name AS CustomFieldProject, dbo.Project.Oid AS CustomFieldProjectOid, dbo.Dataset.Name AS CustomValueDataset, 
                      dbo.CustomField.Name AS CustomFieldName, dbo.CustomValue.Oid AS CustomValue, dbo.Locality.Name AS LocalityName, dbo.Unit.UnitID, 
                      dbo.CustomValue.StringValue AS CustomValueString, dbo.CustomValue.NumberValue AS CustomValueNumber, 
                      dbo.CustomValue.DateTimeValue AS CustomValueDateTime, dbo.CustomValue.BoolValue AS CustomValueBoolean
FROM         dbo.CustomValue INNER JOIN
                      dbo.CustomField ON dbo.CustomValue.Category = dbo.CustomField.Oid INNER JOIN
                      dbo.Project ON dbo.CustomField.Project = dbo.Project.Oid INNER JOIN
                      dbo.Dataset ON dbo.CustomValue.Dataset = dbo.Dataset.Oid LEFT OUTER JOIN
                      dbo.Unit ON dbo.CustomValue.AssociatedUnit = dbo.Unit.Oid LEFT OUTER JOIN
                      dbo.Locality ON dbo.Unit.Country = dbo.Locality.Oid AND dbo.Unit.Locality1 = dbo.Locality.Oid AND dbo.Unit.Locality = dbo.Locality.Oid AND 
                      dbo.CustomValue.AssociatedLocality = dbo.Locality.Oid

");
            }
            catch (Exception)
            {

            }
            try
            {
                newSession.ExecuteNonQuery(@"
CREATE VIEW [dbo].[CusValLocVisUn]
AS
SELECT     dbo.LocalityVisit.Name AS LocalityVisitName, dbo.LocalityVisit.DateTimeStart AS LocalityVisitDateTimeStart, dbo.LocalityVisit.Method AS LocalityVisitMethod, 
                      dbo.CustomValue.Oid, dbo.Unit.TemporaryName AS UnitTemporaryName, dbo.Unit.Quantity AS UnitQuantity, Locality_1.Name AS LocalityCategoryName, 
                      dbo.Locality.Name AS LocalityName, dbo.Dataset.Name AS LocalityVisitCustomValueDataset, dbo.Project.Name AS LocalityVisitProject, 
                      dbo.CustomField.Name AS LocalityVisitCustomValueField
FROM         dbo.Locality AS Locality_1 RIGHT OUTER JOIN
                      dbo.Locality ON Locality_1.Oid = dbo.Locality.Category RIGHT OUTER JOIN
                      dbo.LocalityVisit INNER JOIN
                      dbo.Project ON dbo.LocalityVisit.Project = dbo.Project.Oid ON dbo.Locality.Oid = dbo.LocalityVisit.Locality FULL OUTER JOIN
                      dbo.Dataset INNER JOIN
                      dbo.CustomValue ON dbo.Dataset.Oid = dbo.CustomValue.Dataset FULL OUTER JOIN
                      dbo.CustomField ON dbo.CustomValue.Category = dbo.CustomField.Oid ON dbo.LocalityVisit.Oid = dbo.CustomValue.AssociatedLocalityVisit FULL OUTER JOIN
                      dbo.Unit ON dbo.LocalityVisit.Oid = dbo.Unit.LocalityVisit
WHERE     (NOT (dbo.CustomValue.Oid IS NULL))
");
            }
            catch (Exception)
            {

            }
            try
            {
                newSession.ExecuteNonQuery(@"
CREATE VIEW [dbo].[CusValCntPerPrjView]
AS
SELECT     CustomFieldProject AS Project, COUNT(CustomValue) AS ValueCount
FROM         CustomValuesView
GROUP BY CustomFieldProject
");

            }
            catch (Exception)
            {

            }
        }
        public static double FractionToDouble(string fraction)
        {
            double result;

            if (double.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (double)a / b;
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return a + (double)b / c;
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }

        internal static ListView CreateModelViews(IModelApplication application, IObjectSpace objectSpace,object obj, ITypeInfo objectTypeInfo)
        {
            var modelDifferenceObject = objectSpace.FindObject<ModelDifferenceObject>(new BinaryOperator("Name", application.Title));
            if (modelDifferenceObject != null)
            {
                modelDifferenceObject.ModifyModel(modelDifferenceObject, modelApplication => CreateNewListView(application,  objectSpace,obj, objectTypeInfo, modelApplication, modelDifferenceObject));
                modelDifferenceObject.Save();

                objectSpace.CommitChanges();


            }
            return null;
        }

        private static void CreateNewListView(IModelApplication application, IObjectSpace objectSpace, object obj, ITypeInfo objectTypeInfo, IModelApplication modelApplication, ModelDifferenceObject modelDifferenceObject)
        {
            string listviewcaption = ((Dataset)obj).Name;
            string formviewcaption = ((Dataset)obj).Name;
            string listviewname = String.Format("EarthCape_Unit_ListView_{0}", ((Dataset)obj).Oid);
            string formviewname = String.Format("EarthCape_Unit_DetailView_{0}", ((Dataset)obj).Oid);
            IModelListView list = null;
            IModelDetailView form = null;
            IModelViews modelViews = application.Views;
            IModelListView units = (IModelListView)modelViews["Unit_ListView"];
            if (modelViews[listviewname] == null)
            {
                list = modelViews.AddNode<IModelListView>(listviewname);
                list.AllowEdit = true;
                list.IsGroupPanelVisible = true;
                list.IsFooterVisible = true;
                list.Caption = listviewcaption;
                list.Criteria = CriteriaOperator.ToString(new BinaryOperator("Dataset.Oid", ((Dataset)obj).Oid.ToString()));
                IModelVariants variantsNode = ((IModelViewVariants)units).Variants;
                IModelVariant variant = variantsNode.AddNode<IModelVariant>(((Dataset)obj).Oid.ToString());
                variant.View = list;
                variant.Caption = listviewcaption;
            }
            else
                list = (IModelListView)modelViews[listviewname];

            if (modelViews[formviewname] == null)
            {
                form = modelViews.AddNode<IModelDetailView>(formviewname);
            }
            else
                form = (IModelDetailView)modelViews[formviewname];


            list.ModelClass = application.BOModel["EarthCape.Module.Core.Unit"];
            form.ModelClass = application.BOModel["EarthCape.Module.Core.Unit"];
            IModelBOModelClassMembers members = list.ModelClass.OwnMembers;
            members.OrderBy(f => f.Name);
            GetData(objectSpace, (Dataset)obj);
            list.DetailView = form;
            int index = 0;
            foreach (KeyValuePair<string, int> item in fieldList)
            {
                if ((item.Value > 2) && (list.Columns[item.Key] == null))
                {
                    IModelColumn col = list.Columns.AddNode<IModelColumn>();
                    col.PropertyName = item.Key;
                    col.Id = item.Key;
                    if (Exclude(item.Key))
                        col.Index = -1;
                    else
                    col.Index = index;
                    IModelPropertyEditor field = form.Items.AddNode<IModelPropertyEditor>();
                    field.PropertyName = item.Key;
                    field.Index = index;
                    ((IModelViewItem)field).Id = item.Key;

                    IModelViewLayout layout = form.Layout;
                    IModelLayoutGroup mainGroup = layout.GetNode(ModelDetailViewLayoutNodesGenerator.MainLayoutGroupName) as IModelLayoutGroup;
                    if (mainGroup == null)
                        mainGroup = layout.AddNode<IModelLayoutGroup>("Main"); ;
                    if (!Exclude(item.Key))
                    {
                        IModelLayoutViewItem myItem = mainGroup.AddNode<IModelLayoutViewItem>(item.Key);
                        myItem.Index = index;
                        myItem.ViewItem = field;
                    }
                    index++;
                }
            }

            modelDifferenceObject.XmlContent = modelApplication.Xml();

        }

        private static bool Exclude(object id)
        {
            string[] list = { "CreatedByUser", "CreatedOn", "LastModifiedByUser", "LastModifiedOin", "Oid", "FullName" };
            return list.Contains(id);
        }

        private static void GetData(IObjectSpace os, Dataset dataset)
        {
           fieldList = new Dictionary<string,int>();
            Session session = ((XPObjectSpace)os).Session;
            SelectedData data = session.ExecuteQuery(
                    @"DECLARE @Table SYSNAME = 'Unit'; 
SELECT REVERSE(STUFF(REVERSE((SELECT 'SELECT ''' + name 
                                     + ''' AS [Column], COUNT(DISTINCT(' 
                                     + QUOTENAME(name) + ')) AS [Count] FROM ' 
                                     + QUOTENAME(@Table) +' where [Dataset]=''"+dataset.Oid+@"'''+ ' UNION ' 
                              FROM   sys.columns 
                              WHERE  object_id = Object_id(@Table)
                              FOR XML PATH (''))), 1, 7, ';')); ");
            data = session.ExecuteQuery(data.ResultSet[0].Rows[0].Values[0].ToString().Replace(";"," order by [Count] desc;"));

       
            foreach (SelectStatementResultRow item in data.ResultSet[0].Rows)
            {
                fieldList.Add(item.Values[0].ToString(), (int)item.Values[1]);
            }

        }

        private static Dictionary<string,int> fieldList;

        public static void DropViewAndTable(Session newSession, string name)
        {
            try
            {
                newSession.ExecuteNonQuery(@"DROP VIEW [dbo].[" + name + "] ");
            }
            catch (Exception)
            {

            }
            try
            {
                newSession.ExecuteNonQuery(@"DROP TABLE [dbo].[" + name + "] ");
            }
            catch (Exception)
            {

            }
        }
        public static void DropTable(Session newSession, string name)
        {
            try
            {
                newSession.ExecuteNonQuery(@"DROP TABLE [dbo].[" + name + "] ");
            }
            catch (Exception)
            {

            }
        }

        public static bool IsCreatable(DevExpress.ExpressApp.DC.ITypeInfo typeInfo)
        {
            {
                for (int i = 0; i < Value_WindowController.Instance().Application.Model.GetNode("CreatableItems").NodeCount; i++)
                {
                    IModelCreatableItem node = (IModelCreatableItem)Value_WindowController.Instance().Application.Model.GetNode("CreatableItems").GetNode(i);
                    if (node.ClassName.Split('.').Last() == typeInfo.Name) return true;
                }
                return false;
            }
        }
        public static string Utf8ToUtf16(string utf8String)
        {
            // Get UTF8 bytes by reading each byte with ANSI encoding
            byte[] utf8Bytes = Encoding.Default.GetBytes(utf8String);

            // Convert UTF8 bytes to UTF16 bytes
            byte[] utf16Bytes = Encoding.Convert(Encoding.UTF8, Encoding.Unicode, utf8Bytes);

            // Return UTF16 bytes as UTF16 string
            return Encoding.Unicode.GetString(utf16Bytes);
        }

        public static void ShowMessage(string v)
        {
            throw new NotImplementedException();
        }

        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("Salt Is Not A Password");

        /*      public static string EncryptString(System.Security.SecureString input)
              {
                  byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                      System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                      entropy,
                      System.Security.Cryptography.DataProtectionScope.CurrentUser);
                  return Convert.ToBase64String(encryptedData);
              }

              public static SecureString DecryptString(string encryptedData)
              {
                  try
                  {
                      byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                          Convert.FromBase64String(encryptedData),
                          entropy,
                          System.Security.Cryptography.DataProtectionScope.CurrentUser);
                      return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
                  }
                  catch
                  {
                      return new SecureString();
                  }
              }

              public static SecureString ToSecureString(string input)
              {
                  SecureString secure = new SecureString();
                  foreach (char c in input)
                  {
                      secure.AppendChar(c);
                  }
                  secure.MakeReadOnly();
                  return secure;
              }

              public static string ToInsecureString(SecureString input)
              {
                  string returnValue = string.Empty;
                  IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
                  try
                  {
                      returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
                  }
                  finally
                  {
                      System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
                  }
                  return returnValue;
              }
        
              public static IObjectSpace MyObjectSpace(IDataLayer dataLayer)
              {
                  IObjectSpace objectSpace = new XPObjectSpace(new UnitOfWork(dataLayer), XafTypesInfo.Instance);

                  return objectSpace;
              }*/

     
        public static IDataLayer GetDataLayer(string connstr)
        {
            return XpoDefault.GetDataLayer(connstr, XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary, AutoCreateOption.DatabaseAndSchema);
        }

      

        public static int? GetFirstIndex(string[] inputArray, string searchTerm)
        {

            if (inputArray == null)

                throw new ArgumentNullException("inputArray");



            int? firstIndex = null;

            for (int i = 0; i < inputArray.Length; i++)
            {

                if (inputArray[i] == searchTerm)
                {

                    firstIndex = i;

                    break;

                }

            }

            return firstIndex;

        }

        /*    public static CustomValue CreateCustomValue(BaseObject obj, string tag, double value, IObjectSpace os, bool updateValues)
             {
                 CustomField _tag = os.FindObject<CustomField>(new BinaryOperator("Name", tag), false);
                 if (_tag == null)
                     _tag = os.FindObject<CustomField>(new BinaryOperator("Name", tag), true);
                 if (_tag == null)
                 {
                     _tag = os.CreateObject<CustomField>();
                     _tag.Name = tag;
                     _tag.Save();
                 }
                 CustomValueNumber val = null;
                 if (updateValues)
                 {
                     if (obj is Unit)
                         val = os.FindObject<CustomValueNumber>(CriteriaOperator.And(new BinaryOperator("Category.Oid", _tag.Oid), new BinaryOperator("AssociatedUnit.Oid", obj.Oid)), false);
                     if (obj is Locality)
                         val = os.FindObject<CustomValueNumber>(CriteriaOperator.And(new BinaryOperator("Category.Oid", _tag.Oid), new BinaryOperator("AssociatedLocality.Oid", obj.Oid)), false);
                     if (obj is TaxonomicName)
                         val = os.FindObject<CustomValueNumber>(CriteriaOperator.And(new BinaryOperator("Category.Oid", _tag.Oid), new BinaryOperator("AssociatedTaxonomicName.Oid", obj.Oid)), false);
                     if (obj is LocalityVisit)
                         val = os.FindObject<CustomValueNumber>(CriteriaOperator.And(new BinaryOperator("Category.Oid", _tag.Oid), new BinaryOperator("AssociatedLocalityVisit.Oid", obj.Oid)), false);
                 }
                 if (val == null)
                 {
                     val = os.CreateObject<CustomValueNumber>();
                 }
                 val.Category = _tag;
                 val.NumberValue = value;
              //   val.ObjectTagged = os.FindObject<BaseObject>(new BinaryOperator("Oid",obj.Oid));
                 val.Save();
                 return val;
             }
     */
        public static TaxonomicCategory GetTaxonomicCategory(UnitOfWork uow, IObjectSpace os,Session session, string rank)
        {
            TaxonomicCategory TaxonomicCategory =null;
            bool isnew = false;
            if (uow != null)
            {
                TaxonomicCategory = uow.FindObject<TaxonomicCategory>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", rank));
                if (TaxonomicCategory == null)
                {
                    TaxonomicCategory = new TaxonomicCategory(uow);
                    isnew = true;
                }
            }
            if (os != null)
            {
                TaxonomicCategory = os.FindObject<TaxonomicCategory>(new BinaryOperator("Name", rank));
                if (TaxonomicCategory == null)
                {
                    TaxonomicCategory = os.CreateObject<TaxonomicCategory>();
                    isnew = true;
                }
            }
            if (session != null)
            {
                TaxonomicCategory = session.FindObject<TaxonomicCategory>(new BinaryOperator("Name", rank));
                if (TaxonomicCategory == null)
                {
                    TaxonomicCategory = new TaxonomicCategory(session);
                    isnew = true;
                }
            }
            if (isnew)
            {
                TaxonomicCategory.Name = rank;
                TaxonomicCategory.Save();
            }
            return TaxonomicCategory;
        }
      

        public static Species GetSpecies(IObjectSpace uow, string p, bool IsShortName)
        {
            Species sp = null;
            if (IsShortName == false)
                sp = uow.FindObject<Species>(new BinaryOperator("FullName", p), true);
            else
            {
                sp = uow.FindObject<Species>(new BinaryOperator("ShortName", p), true);
                if (sp == null)
                    sp = uow.FindObject<Species>(new BinaryOperator("FullName", p), true);
            }
            if (sp == null)
            {
                sp = uow.CreateObject<Species>();
                if (IsShortName == false)
                    sp.Name = p;
                else
                    sp.ShortName = p;
                sp.UpdateInfo();
                sp.Save();
            }
            return sp;
        }
        /*      public static CustomField GetTag(UnitOfWork uow, string p)
              {
                  CustomField sp = uow.FindObject<CustomField>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", p));
                  if (sp == null)
                  {
                      sp = new CustomField(uow);
                      sp.Name = p;
                      sp.Save();
                  }
                  return sp;
              }
              */
        public static Locality GetLocality(UnitOfWork uow, string p)
        {
            Locality sp = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", p));
            if (sp == null)
            {
                sp = new Locality(uow);
                sp.Name = p;
                sp.Save();
            }
            return sp;
        }

        public static Dataset GetDataset(IObjectSpace os,UnitOfWork uow, string name, string project)
        {
            Dataset Dataset = null;
            if (!String.IsNullOrEmpty(name))
            {
                if (os!=null)
                Dataset = os.FindObject<Dataset>(new BinaryOperator("Name", name));
                else
                    Dataset = uow.FindObject<Dataset>(new BinaryOperator("Name", name));

            }
            if (Dataset == null)
            {
                if (os != null)
                {
                    Dataset = os.CreateObject<Dataset>();
                }
                else
                    Dataset = new Dataset(uow);
                Dataset.Name = name;
                if (!String.IsNullOrEmpty(project))
                Dataset.Project = GetProject(os,uow, project);
                Dataset.Save();
                // os.CommitChanges();


            }
            return Dataset;
        }
        public static PersonCore StringToContact(IObjectSpace objectSpace, string strContact)
        {
            string lastname = strContact;
            string firstname = string.Empty;
            if (strContact.IndexOf(",") > -1)
            {
                lastname = strContact.Substring(0, strContact.IndexOf(","));
                if (strContact.IndexOf(",") < (strContact.Length - 3))
                {
                    firstname = strContact.Substring(strContact.IndexOf(",") + 2, strContact.Length - strContact.IndexOf(",") - 2);
                }
            }
            else
            {
                if (strContact.IndexOf(" ") > -1)
                {
                    firstname = strContact.Substring(0, strContact.IndexOf(" "));
                    lastname = strContact.Substring(strContact.IndexOf(" ") + 1, strContact.Length - strContact.IndexOf(" ") - 1);

                }
            }
            PersonCore cont;
            if (firstname == string.Empty)
            {
                cont = objectSpace.FindObject<PersonCore>(new BinaryOperator("LastName", lastname));
            }
            else
            {
                cont = objectSpace.FindObject<PersonCore>(new BinaryOperator("FullName", firstname + " " + lastname));
            }
            if (cont == null)
            {
                cont = objectSpace.CreateObject<PersonCore>();
                cont.FirstName = firstname;
                cont.LastName = lastname;
                cont.Save();
                //objectSpace.CommitChanges();
            }
            return cont;
        }
        public static Project GetProject(IObjectSpace os, UnitOfWork uow, string name)
        {
            Project Project = null;
            if (!String.IsNullOrEmpty(name))
            {
                if (os != null)
                    Project = os.FindObject<Project>(new BinaryOperator("Name", name));
                else
                    Project = uow.FindObject<Project>(new BinaryOperator("Name", name));

            }
            if (Project == null)
            {
                if (os != null)
                {
                    Project = os.CreateObject<Project>();
                }
                else
                    Project = new Project(uow);
                Project.Name = name;
                Project.Save();
             }
            return Project;
        }

        public static int ReadBytesSize = 0x1000;


        public static double GetDouble(string p)
        {
            try
            {
                return Convert.ToDouble(p.Replace(",", "."));

            }
            catch (Exception)
            {
                try
                {
                    return Convert.ToDouble(p.Replace(".", ","));

                }
                catch (Exception)
                {
                    return 0;

                }
            }
            finally
            {

            }
        }

        /* todo  public static void ReloadModel(ModelDifferenceObject modelDifferenceObject, XafApplication app)
            {

                var modelApplicationBase = ((ModelApplicationBase)app.Model);
                var userDiff = modelApplicationBase.LastLayer;
                while (modelApplicationBase.LastLayer.Id != "Unchanged Master Part")
                {
                    modelApplicationBase.RemoveLayer(modelApplicationBase.LastLayer);
                }
                var modelApplication = modelApplicationBase.CreatorInstance.CreateModelApplication();
                modelApplication.Id = "After Setup";
                modelApplicationBase.AddLayer(modelApplication);
                modelDifferenceObject.GetModel((ModelApplicationBase)app.Model);
                modelApplicationBase.AddLayer(userDiff);
                app.ShowViewStrategy.CloseAllWindows();
                winApplication.ShowViewStrategy.ShowStartupWindow();
            }*/

        /* public static void UpdateAppPanel(Group Project, View view)
          {
              foreach (var typeInfo in XafTypesInfo.Instance.PersistentTypes)
              {
                  var additionalViewControlsRules = LogicRuleManager<IAdditionalViewControlsRule>.Instance[typeInfo];
                  foreach (var additionalViewControlsRule in additionalViewControlsRules)
                  {
                      if (additionalViewControlsRule.Id == "b7fb3659-cf93-4550-bb3b-05897faae8e0")
                      {
                          //  HintPanel acap = ((HintPanel)((IAdditionalViewControlsRule)additionalViewControlsRule));
                          // if (acap == null) return;
                          // ISupportAppeareance appear = ((ISupportAppeareance)((AdditionalViewControlsRule)additionalViewControlsRule).Control);
                          if (Project != null)
                          {
                              string txt = Project.FullName;// String.Format("{0} for {1}", Project.FullName, view.Caption); ;
                              additionalViewControlsRule.Message = txt;
                              additionalViewControlsRule.FontSize = 20;
                              // appear.FontSize = 20;
                              // appear.ForeColor = Color.RoyalBlue;
                              additionalViewControlsRule.ForeColor = Color.RoyalBlue;
                              // acap.Text = txt;
                              //   acap.Update();
                          }

                          // acap.Refresh();
                          break;
                      }
                  }
              }
          }
    */

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]", invalidChars);
            return Regex.Replace(name, invalidReStr, "_");
        }
        public static bool NewVersionAvailable(Version CurrentVersion)
        {
            WebRequest request = WebRequest.Create("http://www.earthcape.com/version.txt");
            WebResponse response = request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                Version NewestVersion = new Version(reader.ReadLine());
                string Link = reader.ReadLine();
                if (NewestVersion > CurrentVersion)
                    return true;
            }
            return false;
        }




        public static string Clean(string someString)
        {
            char[] BAD_CHARS = new char[] { '!', '@', '#', '$', '%', '_', '\'', '"', '*', '(', ')', '"' };
            someString = someString.Trim(BAD_CHARS);
            return someString;
        }

        public static Project GetCurrentProject(IObjectSpace os)
        {
            Project prj = null;
            if (prj == null)
            {
                prj = Value_WindowController.Instance().GetGroup(os);
            }
            if (prj == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    prj = os.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                }
            }
            return prj;
        }
        public static Project GetCurrentProject(Session os)
        {
            Project prj = null;
            if (prj == null)
            {
                prj = Value_WindowController.Instance().GetGroup(os);
            }
            if (prj == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    prj = os.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                }
            }
            return prj;
        }
        public static Dataset GetCurrentDataset(Session os)
        {
            Dataset prj = null;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultDatasetName"]))
            {
                prj = os.FindObject<Dataset>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultDatasetName"]));
            }
            return prj;
        }
        public static Dataset GetCurrentDataset(IObjectSpace os)
        {
            Dataset prj = null;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultDatasetName"]))
            {
                prj = os.FindObject<Dataset>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultDatasetName"]));
            }
            return prj;
        }

        public static string CreatePassword(int Lenght, int NonAlphaNumericChars)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            string allowedNonAlphaNum = "!@#$%^&*()_-+=[{]};:<>|./?";
            Random rd = new Random();

            if (NonAlphaNumericChars > Lenght || Lenght <= 0 || NonAlphaNumericChars < 0)
                throw new ArgumentOutOfRangeException();

            char[] pass = new char[Lenght];
            int[] pos = new int[Lenght];
            int i = 0, j = 0, temp = 0;
            bool flag = false;

            //Random the position values of the pos array for the string Pass
            while (i < Lenght - 1)
            {
                j = 0;
                flag = false;
                temp = rd.Next(0, Lenght);
                for (j = 0; j < Lenght; j++)
                    if (temp == pos[j])
                    {
                        flag = true;
                        j = Lenght;
                    }

                if (!flag)
                {
                    pos[i] = temp;
                    i++;
                }
            }

            //Random the AlphaNumericChars
            for (i = 0; i < Lenght - NonAlphaNumericChars; i++)
                pass[i] = allowedChars[rd.Next(0, allowedChars.Length)];

            //Random the NonAlphaNumericChars
            for (i = Lenght - NonAlphaNumericChars; i < Lenght; i++)
                pass[i] = allowedNonAlphaNum[rd.Next(0, allowedNonAlphaNum.Length)];

            //Set the sorted array values by the pos array for the rigth posistion
            char[] sorted = new char[Lenght];
            for (i = 0; i < Lenght; i++)
                sorted[i] = pass[pos[i]];

            string Pass = new String(sorted);

            return Pass;
        }


    }
}
