using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Actions;
using EarthCape.Interfaces;
using DevExpress.Persistent.Base.General;

namespace EarthCape.Module
{
    public class CriteriaHelper
    {

        public static CriteriaOperator GroupMembershipCanEdit(User user)
        {
            return CriteriaOperator.Or(
                //new BinaryOperator("CreatedBy.Oid", user.Oid),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(
                  new BinaryOperator("Member.Oid", user.Oid),
                  new BinaryOperator("IsAdmin", true)))
            );
        }
        public static CriteriaOperator TaxonDeleteEdit(User user)
        {
            //fix
            return CriteriaOperator.Parse("1=0");

            
            return CriteriaOperator.Or(
                //new BinaryOperator("CreatedBy.Oid", user.Oid),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(
                  new BinaryOperator("Member.Oid", user.Oid),
                  new BinaryOperator("IsAdmin", true)))
        );
        }
        public static CriteriaOperator GroupMembershipCanDelete(User user)
        {
            return CriteriaOperator.Or(
                //new BinaryOperator("CreatedBy.Oid", user.Oid),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(
                  new BinaryOperator("Member.Oid", user.Oid),
                  new BinaryOperator("IsAdmin", true)))
        );
        }
        public static CriteriaOperator GroupCanAddMembers(User user)
        {
            //return CriteriaOperator.Parse("1=1");
            return new ContainsOperator("GroupMemberships", CriteriaOperator.And(
            new BinaryOperator("Member.Oid", user.Oid),
                    CriteriaOperator.Or(
                new BinaryOperator("CanAddMembers", true),
                new BinaryOperator("IsAdmin", true))
        ));
        }
        public static CriteriaOperator GroupCanDeleteEdit(User user)
        {
            return
                CriteriaOperator.Or(
                new BinaryOperator("CreatedBy.Oid", user.Oid),
                new ContainsOperator("GroupMemberships", CriteriaOperator.And(
                   new BinaryOperator("Member.Oid", user.Oid),
                   new BinaryOperator("IsAdmin", true)
            ))
                );
        }
        public static CriteriaOperator TaskCanDeleteEdit(User user)
        {
            return new BinaryOperator("CreatedBy.Oid", user.Oid);
        }
        public static CriteriaOperator SetCanDelete(User user)
        {
            return new BinaryOperator("Owner.Oid", user.Oid);
        }

        public static CriteriaOperator UnitCanEdit(User user)
        {
            return CriteriaOperator.Or(
                new BinaryOperator("CreatedBy.Oid", user.Oid),
                 new ContainsOperator("Group.GroupMemberships",
                                  CriteriaOperator.And(
                                    new BinaryOperator("Member.Oid", user.Oid),
                                    CriteriaOperator.Or(
                                        new BinaryOperator("IsAdmin", true),
                                        new BinaryOperator("UnitEdit", true)
                            )
                           )
                                    )
            );


        }

        public static CriteriaOperator UnitCanDelete(User user)
        {
            return CriteriaOperator.Or(
                      new BinaryOperator("CreatedBy.Oid", user.Oid),
                       new ContainsOperator("Group.GroupMemberships",
                                        CriteriaOperator.And(
                                          new BinaryOperator("Member.Oid", user.Oid),
                                          CriteriaOperator.Or(
                                              new BinaryOperator("IsAdmin", true),
                                              new BinaryOperator("UnitDelete", true)
                                  )
                                 )
                                          )
                  );
        }
        public static CriteriaOperator UnitCanAdd(User user)
        {
            return CriteriaOperator.Or(
                      new BinaryOperator("CreatedBy.Oid", user.Oid),
                       new ContainsOperator("Group.GroupMemberships",
                                        CriteriaOperator.And(
                                          new BinaryOperator("Member.Oid", user.Oid),
                                          CriteriaOperator.Or(
                                              new BinaryOperator("IsAdmin", true),
                                              new BinaryOperator("UnitAdd", true)
                                  )
                                 )
                                          )
                  );
        }
        /*    public static CriteriaOperator SetCanEdit(User user)
            {
                return CriteriaOperator.Or(
                     new BinaryOperator("Owner.Oid", user.Oid),
                         CriteriaOperator.And(

                             new ContainsOperator("Owner.<Group>GroupMemberships",
                             CriteriaOperator.And(
                                 new BinaryOperator("Member.Oid", user.Oid),
                                 CriteriaOperator.Or(
                                     new BinaryOperator("IsAdmin", true),
                                     new BinaryOperator("CanEdit", true)
                                 )
                             )
                             ),
                             new ContainsOperator("Sharings",
                                 CriteriaOperator.And(
                                     new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)),
                                     new BinaryOperator("CanEdit", true)
                          ))
                           ));
            }*/

        public static CriteriaOperator MessageInboxCriteria(User user)
        {
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                     return new ContainsOperator("Recipients",
                                new BinaryOperator("Party.Oid", new Guid(Value_WindowController.Instance().Group)));
           }
            return CriteriaOperator.Parse("1=0");
        }

   /*     public static CriteriaOperator UnitCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new ContainsOperator("Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                  new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true)))),
                  new ContainsOperator("Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))
                  )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                new ContainsOperator("Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true)))),
                 new ContainsOperator("Group.Parent.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true))))
                )
                  ;
            }
            return cr;
        }*/

        public static CriteriaOperator InteractionCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new BinaryOperator("Unit1.CreatedBy.Oid", user.Oid),
                   new BinaryOperator("Unit2.CreatedBy.Oid", user.Oid)
                  )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                CriteriaOperator cr1 = CriteriaOperator.Or(
                new ContainsOperator("Unit1.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                new ContainsOperator("Unit1.Group.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true)))),
                 new ContainsOperator("Unit1.Group.Parent.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true))))
                )
                  ;
                CriteriaOperator cr2 = CriteriaOperator.Or(
                    new ContainsOperator("Unit2.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                    new ContainsOperator("Unit2.Group.GroupMemberships", CriteriaOperator.And(
                                    new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                    new BinaryOperator("Member.Oid", user.Oid),
                                    CriteriaOperator.Or(
                                        new BinaryOperator("UnitView", true),
                                        new BinaryOperator("IsAdmin", true)))),
                     new ContainsOperator("Unit2.Group.Parent.GroupMemberships", CriteriaOperator.And(
                                    new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                    new BinaryOperator("Member.Oid", user.Oid),
                                    CriteriaOperator.Or(
                                        new BinaryOperator("UnitView", true),
                                        new BinaryOperator("IsAdmin", true))))
                    );
                    cr=CriteriaOperator.Or(cr1,cr2);
                     
            }
            return cr;
        }
 

        public static CriteriaOperator TaxonCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new ContainsOperator("Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                  new ContainsOperator("Groups", new ContainsOperator("IsTaxonomicAuthorityForGroups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))))
                //new ContainsOperator("ClassifiedAs", new ContainsOperator("Classification.DefaultClassificationForGroups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))))
                    )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr =
                new ContainsOperator("Groups",
                  CriteriaOperator.Or(
                  new ContainsOperator("GroupMemberships",
                      CriteriaOperator.And(
                        new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                        new BinaryOperator("Member.Oid", user.Oid)
                        )
                        ),
                  new ContainsOperator("IsTaxonomicAuthorityForGroups", new ContainsOperator("GroupMemberships",
                      CriteriaOperator.And(
                         new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                        new BinaryOperator("Member.Oid", user.Oid)
                      )))

                        )

                )
                  ;
            }
            return cr;
        }

     /*   public static CriteriaOperator TagValueCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid)
                    )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = new BinaryOperator("ObjectTagged",CriteriaHelper.DataAccess_FilterView(view.ObjectTypeInfo.Type, user));
                  ;
            }
            /* cr = CriteriaOperator.Or(
                   new BinaryOperator("CreatedBy.Oid", user.Oid),
                   new ContainsOperator("Unit.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                   new ContainsOperator("Unit.Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true)))),
                   new ContainsOperator("Unit.Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))
                   )
                   ;

             if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
             {
                 cr = CriteriaOperator.Or(
                 new ContainsOperator("Unit.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                   new ContainsOperator("Unit.Group.GroupMemberships", CriteriaOperator.And(
                                 new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                 new BinaryOperator("Member.Oid", user.Oid),
                                 CriteriaOperator.Or(
                                     new BinaryOperator("UnitView", true),
                                     new BinaryOperator("IsAdmin", true)))),
                     new ContainsOperator("Unit.Group.Parent.GroupMemberships", CriteriaOperator.And(
                                 new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                 new BinaryOperator("Member.Oid", user.Oid),
                                 CriteriaOperator.Or(
                                     new BinaryOperator("UnitView", true),
                                     new BinaryOperator("IsAdmin", true))))
                                     )
                   ;
             }*/
        /*    return cr;
        }*/

        public static CriteriaOperator SetCommonItemCriteria(User user)
        {
            return CriteriaOperator.Parse("1=0");
        }
        public static CriteriaOperator GetGroupMembershipsCriteria(User user)
        {
            return new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid));
        }
        public static CriteriaOperator GetTasksCriteria(User user)
        {
            return CriteriaOperator.Or(
                new BinaryOperator("CreatedBy.Oid", user.Oid),
                new ContainsOperator("AssignedTo",
                    CriteriaOperator.Or(
                        new BinaryOperator("Oid", user.Oid),
                         new ContainsOperator("<Group>GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)))
                    )
                );

            //return new BinaryOperator("Group", new ContainsOperator(GetGroupsCriteria(user, os).ToString(), new BinaryOperator("Member", user)));
        }
        public static CriteriaOperator GetGroupInvitationCriteria(User user)
        {
            return CriteriaOperator.Or(
                new BinaryOperator("Member.Oid", user.Oid),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), new BinaryOperator("IsAdmin", true)))
            );
        }
        public static void ListViewEditCriteria(Type objtype, User user, ref string cr_Edit, ref string cr_Delete, ref string cr_gamc)
        {
            if (typeof(BaseObject).IsAssignableFrom(objtype))
            {
                //  ((ListView)View).CollectionSource.Criteria[""] = (new BinaryOperator("CreatedByUser", user));
                cr_Edit = (new BinaryOperator("CreatedByUser", user)).ToString();
                cr_Delete = (new BinaryOperator("CreatedByUser", user)).ToString();
                if (typeof(Unit).IsAssignableFrom(objtype))
                {
                    // CriteriaHelper.UnitCriteria(user);
                    cr_Edit = CriteriaHelper.UnitCanEdit(user).ToString(); ;
                    cr_Delete = CriteriaHelper.UnitCanDelete(user).ToString(); ;
                }
                if (typeof(Party).IsAssignableFrom(objtype))
                {
                    //    ((ListView)View).CollectionSource.Criteria[""] = CriteriaOperator.Parse("1=1");
                    if (typeof(User).IsAssignableFrom(objtype))
                    {
                        //      ((ListView)View).CollectionSource.Criteria[""] = CriteriaHelper.GetUsersCriteria(user);
                        cr_Edit = CriteriaOperator.Parse("1=0").ToString(); ; // (new BinaryOperator("Oid", user.Oid.ToString())).ToString();
                        cr_Delete = CriteriaOperator.Parse("1=0").ToString(); ; // (new BinaryOperator("Oid", user.Oid.ToString())).ToString();
                    }
                    if (typeof(Group).IsAssignableFrom(objtype))
                    {
                        //      ((ListView)View).CollectionSource.Criteria[""] = CriteriaHelper.GetGroupsCriteria(user);
                        cr_Edit = CriteriaHelper.GroupCanDeleteEdit(user).ToString();
                        cr_Delete = CriteriaHelper.GroupCanDeleteEdit(user).ToString();
                        cr_gamc = CriteriaHelper.GroupCanAddMembers(user).ToString();

                    }
                }
                if (typeof(GroupMembership).IsAssignableFrom(objtype))
                {
                    //  ((ListView)View).CollectionSource.Criteria[""] = CriteriaHelper.GetGroupMembershipsCriteria(user); ;//todo, CriteriaHelper.GetGroupMembershipsCriteria(user, ObjectSpace);
                    cr_Edit = CriteriaHelper.GroupMembershipCanEdit(user).ToString();
                    cr_Delete = CriteriaOperator.Parse("1=0").ToString();// CriteriaHelper.GroupMembershipCanDelete(user).ToString();

                    //todo
                }
                /*if (typeof(Sharing).IsAssignableFrom(View.ObjectType))
                {
                  //  ((ListView)View).CollectionSource.Criteria[""] = CriteriaHelper.GetSharingCriteria(user); ;//todo
                    //todo
                }*/
            }
            else {
                cr_Edit = CriteriaOperator.Parse("1=0").ToString();
                cr_Delete = CriteriaOperator.Parse("1=0").ToString();// CriteriaHelper.GroupMembershipCanDelete(user).ToString();
            }
        }

        public static bool CanEditCurrentObject(View view, ObjectSpace os, User u)
        {
            //CriteriaOperator cr = null;
            CriteriaOperator cr =  CriteriaOperator.Parse("1=0");
            if (typeof(Group).IsAssignableFrom(view.CurrentObject.GetType()))
                cr = CriteriaOperator.And(CriteriaHelper.GroupCanDeleteEdit(u), new BinaryOperator("Oid", ((Group)(view.CurrentObject)).Oid));
            if (typeof(GroupMembership).IsAssignableFrom(view.CurrentObject.GetType()))
                cr = CriteriaOperator.And(CriteriaHelper.GroupMembershipCanEdit(u), new BinaryOperator("Oid", ((GroupMembership)(view.CurrentObject)).Oid));
            if (typeof(User).IsAssignableFrom(view.CurrentObject.GetType()))
                return (u.Oid == ((User)(view.CurrentObject)).Oid);
            if (typeof(BaseObject).IsAssignableFrom(view.CurrentObject.GetType()))
            {
           //     cr = CriteriaOperator.And(new BinaryOperator("CreatedBy.Oid", u.Oid), new BinaryOperator("Oid", ((BaseObject)(view.CurrentObject)).Oid));
                    cr = CriteriaOperator.And(CriteriaHelper.GetBaseObjectEditDeleteCriteria(u), new BinaryOperator("Oid", ((BaseObject)(view.CurrentObject)).Oid));
             }
            else return true;
            return (os.FindObject(view.CurrentObject.GetType(), cr) != null);
        }


        private static CriteriaOperator ConnectionStringCanEdit(User u)
        {
            return new BinaryOperator("CreatedBy.Oid", u.Oid);
        }

        public static CriteriaOperator GetMakeInvitationCodeCriteria(User user)
        {
            return
                new ContainsOperator("GroupMemberships",
                    CriteriaOperator.And(
                        new BinaryOperator("Member.Oid", user.Oid),
                        new BinaryOperator("IsAdmin", true)));
        }
        public static CriteriaOperator GetRequestMembershipCriteria(User user)
        {
            return new BinaryOperator("CanRequest", true);
        }
        public static CriteriaOperator GetGroupsCriteria(User user)
        {
            CriteriaOperator CurrentUserGroupCriteria = CriteriaOperator.Or(
                new ContainsOperator("GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)),
               new ContainsOperator("Parent.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid))
                );
            return CurrentUserGroupCriteria;

        }
        public static CriteriaOperator GetSharingCriteria(User user)
        {
            CriteriaOperator cr = CriteriaOperator.Or(
                 new BinaryOperator("Dataset.Owner.Oid", user.Oid),
                new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)));
            return cr;
        }
        public static CriteriaOperator DataAccess_FilterView(Type objectType, User user)
        {
            CriteriaOperator cr = null;
            if (typeof(BaseObject).IsAssignableFrom(objectType))
            {
                cr = null;// (new BinaryOperator("CreatedBy.Oid", user.Oid));
                if ((typeof(IUnitData).IsAssignableFrom(objectType)))
                {
                    cr = CriteriaHelper.UnitDataCriteria(user);
                    //todo
                }

                /*  
                  if (typeof(ICategorizedItem).IsAssignableFrom(objectType))
                  {
                      if (!ReferenceEquals(cr,null))
                          cr = CriteriaOperator.Or(CriteriaHelper.GetCategorizedCriteria(user), cr);
                      else
                          cr = CriteriaHelper.GetCategorizedCriteria(user);
                      //if (typeof(TagValue).IsAssignableFrom(objectType))
                          //cr =CriteriaOperator.And(cr, CriteriaHelper.TagValueCriteria(user));
                  }*/

                if (typeof(BaseObject).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetBaseObjectViewCriteria(user);
                }
                if (typeof(Shipment).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetLoanLinkedViewCriteria(user);
                }
                /* if (typeof(Unit).IsAssignableFrom(objectType))
                      if (!ReferenceEquals(cr,null))
                          cr = CriteriaOperator.Or(CriteriaHelper.UnitCriteria(user), cr);
                      else
                          cr = CriteriaHelper.UnitCriteria(user);*/
                if (typeof(Interaction).IsAssignableFrom(objectType))
                    cr = CriteriaHelper.InteractionCriteria(user);
                if (typeof(Message).IsAssignableFrom(objectType))
                    cr = CriteriaHelper.MessageCriteria(user);
                /**/
                if (typeof(MaterialTransfer).IsAssignableFrom(objectType))
                {
                    //          cr = CriteriaHelper.MaterialTransferCriteria(user);
                }
                /* if (typeof(TaxonomicName).IsAssignableFrom(objectType))
                 {
                     cr = CriteriaHelper.TaxonCriteria(user);
                 }*/
                /*   if (typeof(HigherTaxonRef).IsAssignableFrom(objectType))
                   {
                       cr = CriteriaHelper.GetHigherTaxonRefCriteria(user);
                   }*/
                if (typeof(Term).IsAssignableFrom(objectType))
                {
                    //cr = CriteriaHelper.TaxonCriteria(user);
                }
                /*    if (typeof(Sharing).IsAssignableFrom(objectType))
                   {
                       cr = CriteriaHelper.GetSharingCriteria(user); ;//todo
                   }*/

                /*if (typeof(Store).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetStoreCriteria(user);
                }
                if (typeof(Report).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetReportCriteria(user);
                }*/
            }
            else {
                if (typeof(Group).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetGroupsCriteria(user);
                }
                if (typeof(User).IsAssignableFrom(objectType))
                {
                    cr = null;// CriteriaHelper.GetUsersCriteria(user);
                }
                if (typeof(GroupMembership).IsAssignableFrom(objectType))
                {
                    cr = CriteriaHelper.GetGroupMembershipsCriteria(user); //todo, CriteriaHelper.GetGroupMembershipsCriteria(user, ObjectSpace);
                }
            }
            return cr;
        }

        private static CriteriaOperator TagValueCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("Unit.CreatedBy.Oid", user.Oid),
                  new ContainsOperator("Unit.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                  new ContainsOperator("Unit.Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true)))),
                  new ContainsOperator("Unit.Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))
                  )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                new ContainsOperator("Unit.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))),
                new ContainsOperator("Unit.Group.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true)))),
                 new ContainsOperator("Unit.Group.Parent.GroupMemberships", CriteriaOperator.And(
                                new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)),
                                new BinaryOperator("Member.Oid", user.Oid),
                                CriteriaOperator.Or(
                                    new BinaryOperator("UnitView", true),
                                    new BinaryOperator("IsAdmin", true))))
                )
                  ;
            }
            return cr;
        }

        private static CriteriaOperator GetStoreCriteria(User user)
        {
            CriteriaOperator cr = CriteriaOperator.Or(
                    new BinaryOperator("CreatedBy.Oid", user.Oid),
                    new ContainsOperator("Groups", new ContainsOperator("GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)))/*,
                    new ContainsOperator("Groups", new ContainsOperator("Parent.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid))
                
            )*/
               );
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = new ContainsOperator("Groups", new BinaryOperator("Oid", new Guid(Value_WindowController.Instance().Group)));
            }
            return cr;
        }
    /*    private static CriteriaOperator GetReportCriteria(User user)
        {
            CriteriaOperator cr = CriteriaOperator.Or(
                                           new BinaryOperator("CreatedBy.Oid", user.Oid),
                                          CriteriaOperator.Or(
                new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)),
               new ContainsOperator("Group.Parent.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid))
                ));






            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)),
               new ContainsOperator("Group.Parent.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid))
                );
            }
            return cr;
        }

        */
        public static CriteriaOperator GetBaseObjectEditDeleteCriteria(User u)
        {
            //todo decide if only owner group admin can edit delete
            return CriteriaOperator.Or(
                new BinaryOperator("CreatedBy.Oid", u.Oid),
                new ContainsOperator("Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))),
                new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true))),
                 new ContainsOperator("Groups", new ContainsOperator("Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))),
                new ContainsOperator("Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true))),
                 new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))),
                new ContainsOperator("Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true))),
                 new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))),
                new ContainsOperator("Group.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true))),
                 new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))),
                new ContainsOperator("Group.Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", u.Oid), new BinaryOperator("IsAdmin", true)))
   );
        }


        public static CriteriaOperator GetBaseObjectViewCriteria(User user)
        {
            //todo 4 hierarchy levels support
            CriteriaOperator cr = new BinaryOperator("CreatedBy.Oid", user.Oid);
            
            if (Value_WindowController.Instance() != null)
                if (Value_WindowController.Instance() != null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
                    {
                        cr = CriteriaOperator.Or(
                              new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                              new ContainsOperator("Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                        new ContainsOperator("Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                            new ContainsOperator("Groups", new ContainsOperator("Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                           new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                            new ContainsOperator("Group.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                           new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                            new ContainsOperator("Group.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                           new ContainsOperator("Groups", new ContainsOperator("Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                            new ContainsOperator("Group.Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))
             );
                    }
            return cr;
        }
        private static CriteriaOperator GetLoanLinkedViewCriteria(User user)
        {
            //todo 4 hierarchy levels support
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                new BinaryOperator("CreatedBy.Oid", user.Oid),
               new ContainsOperator("Loan.Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                    new ContainsOperator("Loan.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
             new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                new ContainsOperator("Loan.Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                new ContainsOperator("Loan.Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                new ContainsOperator("Loan.Group.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                new ContainsOperator("Loan.Group.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))
                    )
                  ;
            if (Value_WindowController.Instance()!=null)
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                      new ContainsOperator("Loan.Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                      new ContainsOperator("Loan.Group.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                new ContainsOperator("Loan.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                    new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                   new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                    new ContainsOperator("Loan.Group.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                   new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                    new ContainsOperator("Loan.Group.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                   new ContainsOperator("Loan.Groups", new ContainsOperator("Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                    new ContainsOperator("Loan.Group.Parent.Parent.Parent.Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))
     );
            }
            return cr;
        }
        private static CriteriaOperator GetCategorizedCriteria(User user)
        {
          CriteriaOperator cr = null;
           cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid)
                    )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                  new ContainsOperator("Category.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))),
                  new ContainsOperator("Category.Groups", new ContainsOperator("Parent.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))))
   );
            }
            return cr;
        }
        private static CriteriaOperator MaterialTransferCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new ContainsOperator("ReceivingParty.<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                  new BinaryOperator("ReceivingParty.<User>Oid", user.Oid),
                  new ContainsOperator("SendingParty.<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid))),
                  new BinaryOperator("SendingParty.<User>Oid", user.Oid)
                   )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                    new ContainsOperator("ReceivingParty.<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))),
                    new ContainsOperator("SendingParty.<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid)))
     );
            }
            return cr;
        }

        private static CriteriaOperator MessageCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new ContainsOperator("Recipients", new ContainsOperator("<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid)))),
                  new ContainsOperator("Recipients", new BinaryOperator("<User>Oid", user.Oid))
                    )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr =
                    new ContainsOperator("Recipients", new ContainsOperator("<Group>GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid))))
         ;
            }
            return cr;
        }
        private static CriteriaOperator GetTagCriteria(User user)
        {
            CriteriaOperator cr = CriteriaOperator.Or(
         new ContainsOperator("Groups", GetGroupsCriteria(user)),
                  new BinaryOperator("CreatedBy.Oid", user.Oid));
            return cr;
        }

        private static CriteriaOperator UnitDataCriteria(User user)
        {

            CriteriaOperator cr = CriteriaOperator.Or(
                   new BinaryOperator("CreatedBy.Oid", user.Oid),
                   new BinaryOperator("Unit.CreatedBy.Oid", user.Oid),
                   new ContainsOperator("Unit.Group.GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true)))),
                   new ContainsOperator("Unit.Store.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), CriteriaOperator.Or(new BinaryOperator("UnitView", true), new BinaryOperator("IsAdmin", true))))));
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = CriteriaOperator.Or(
                     new ContainsOperator("Unit.Store.Groups", new BinaryOperator("Oid", new Guid(Value_WindowController.Instance().Group))),
                      new BinaryOperator("Unit.Group.Oid", new Guid(Value_WindowController.Instance().Group)));
            }
            return cr;
        }


        public static CriteriaOperator GetUsersCriteria(User user)
        {
            CriteriaOperator cr = null;
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = new ContainsOperator("MemberOf", new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)));
            }
            else
            {
                cr = new ContainsOperator("MemberOf", new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)));
            }
            return cr;
        }
        public static CriteriaOperator GetSetsCriteria(User user)
        {
            CriteriaOperator cr = CriteriaOperator.Or(
                                        new BinaryOperator("Owner.Oid", user.Oid),
                                       new ContainsOperator("Sharings", CriteriaOperator.And(
                                                 new ContainsOperator("Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)))
                                           ));

            return cr;
        }

        /*      public static CriteriaOperator ItemsFromSetCriteria(Dataset Dataset)
              {

                  return new ContainsOperator("Datasets", new BinaryOperator("Oid", Dataset.Oid));
              }*/

        public static CriteriaOperator IsSetOwner(Party p)
        {
            return new BinaryOperator("Owner.Oid", p.Oid);
        }

        internal static object TagValueCanDeleteEdit(User user)
        {
            return CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                      new ContainsOperator("Unit.Group.GroupMemberships", new BinaryOperator("Member.Oid", user.Oid)));
        }


        internal static object SharingCanDeleteEdit(User user)
        {
            return
               CriteriaOperator.Or(
               new BinaryOperator("CreatedBy.Oid", user.Oid),
               new ContainsOperator("Group.GroupMemberships", CriteriaOperator.And(
                  new BinaryOperator("Member.Oid", user.Oid),
                  new BinaryOperator("IsAdmin", true)
           ))
               );
        }


        public static CriteriaOperator GetHigherTaxonRefCriteria(User user)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.Or(
                  new BinaryOperator("CreatedBy.Oid", user.Oid),
                  new ContainsOperator("Classification.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Member.Oid", user.Oid), new BinaryOperator("IsAdmin", true))))
                  )
                  ;

            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
                cr = new ContainsOperator("Classification.Groups", new ContainsOperator("GroupMemberships", CriteriaOperator.And(new BinaryOperator("Group.Oid", new Guid(Value_WindowController.Instance().Group)), new BinaryOperator("Member.Oid", user.Oid), new BinaryOperator("IsAdmin", true))))
                  ;
            }
            return cr;
        }


   /*     public static CriteriaOperator GetCurrentClassifiedSpecies(User user)
        {
            CriteriaOperator cr = null;
            if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
            {
               cr = CriteriaOperator.And(
                           new ContainsOperator("Classification.Groups", null))
                       ;
            }
            return null;
        }*/
    }
}
