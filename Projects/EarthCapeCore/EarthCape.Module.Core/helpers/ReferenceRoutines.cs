using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace EarthCape.References
{
    public class ReferenceRoutines
    {
        //     Collection<String> a1, a2, a3, kw, n1, n2, ad;
        public static string[] ArrayOfTagCategorys = new string[46] { "TY  - ", "ID  - ", "T1  - ", "TI  - ", "CT  - ", "T2  - ", "T3  - ", "BT  - ", "AU  - ", "A1  - ", "A2  - ", "ED  - ", "A3  - ", "KW  - ", "Y1  - ", "PY  - ", "Y2  - ", "N1  - ", "AB  - ", "N2  - ", "RP  - ", "JF  - ", "JO  - ", "JA  - ", "J1  - ", "J2  - ", "VL  - ", "IS  - ", "CP  - ", "SP  - ", "EP  - ", "CY  - ", "PB  - ", "SN  - ", "AD  - ", "AV  - ", "M1  - ", "M2  - ", "M3  - ", "U1  - ", "U2  - ", "U3  - ", "U4  - ", "U5  - ", "UR  - ", "ER  - " };
        public static void ImportRIS(String fn, UnitOfWork uow, string fn1)
        {
            string ty = string.Empty, t1 = string.Empty, t2 = string.Empty, t3 = string.Empty, id = string.Empty, bt = string.Empty, Y1 = string.Empty, Y2 = string.Empty, RP = string.Empty, JF = string.Empty, JO = string.Empty, JA = string.Empty, J1 = string.Empty, J2 = string.Empty, VOL = string.Empty, IS_ = string.Empty, SP = string.Empty, EP = string.Empty, CY = string.Empty, PB = string.Empty, SN = string.Empty, AV = string.Empty, M1 = string.Empty, M2 = string.Empty, M3 = string.Empty, U1 = string.Empty, U2 = string.Empty, U3 = string.Empty, U4 = string.Empty, U5 = string.Empty, UR;
            String line;
            string tg, vl;
            Collection<string> a1 = new Collection<string>();
            Collection<string> a2 = new Collection<string>();
            Collection<string> a3 = new Collection<string>();
            Collection<string> kw = new Collection<string>();
            Collection<string> n1 = new Collection<string>();
            Collection<string> n2 = new Collection<string>();
            Collection<string> ad = new Collection<string>();

            StreamReader sr = new StreamReader(fn);
            line = sr.ReadLine();
            tg = line;
            while (line != null)
            {
                if ((line != string.Empty) && (line.Length > 5) && (isTagCategory(line.Substring(0, 6))))
                {
                    tg = line.Substring(0, 6);
                    vl = line.Substring(6, line.Length - 6);
                    if ((vl.Length > 255) && (tg != "AU  - ") && (tg != "A2  - ") && (tg != "ED  - ") && (tg != "A3  - ") && (tg != "KW  - ") && (tg != "N1  - ") && (tg != "N2  - ") && (tg != "AB  - ") && (tg != "AB  - ") && (tg != "AD  - "))
                    {
                        vl = vl.Substring(0, 255);
                    }
                    if ((vl.Length > 100) && ((tg == "Y1  - ") || (tg == "Y2  - ")))
                    {
                        vl = vl.Substring(0, 100);
                    }

                    if (tg == "T1  - ") { t1 = vl; }
                    if (tg == "TY  - ") { ty = vl; }
                    if (tg == "CT  - ") { t1 = vl; }
                    if (tg == "TI  - ") { t1 = vl; }
                    if (tg == "T2  - ") { t2 = vl; }
                    if (tg == "T3  - ") { t3 = vl; }
                    if (tg == "ID  - ") { id = vl; }
                    if (tg == "BT  - ") { bt = vl; }
                    if (tg == "A1  - ")
                    {
                        a1.Add(vl);
                    }
                    if (tg == "AU  - ")
                    {
                        a1.Add(vl);
                    }
                    if (tg == "A2  - ")
                    {
                        a2.Add(vl);
                    }
                    if (tg == "ED  - ")
                    {
                        a2.Add(vl);
                    }
                    if (tg == "A3  - ")
                    {
                        a3.Add(vl);
                    }
                    if (tg == "KW  - ")
                    {
                        char[] delimiterChars = { ',', ',', '.', ';' };
                        string[] words = vl.Split(delimiterChars);
                        foreach (string word in words)
                        {
                            kw.Add(word.Trim());
                        }
                    }
                    if (tg == "N1  - ")
                    {
                        n1.Add(vl);
                    }
                    if (tg == "AB  - ")
                    {
                        n1.Add(vl);
                    }
                    if (tg == "N2  - ")
                    {
                        n2.Add(vl);
                    }
                    if (tg == "AD  - ")
                    {
                        ad.Add(vl);
                    }
                    if (tg == "Y1  - ") { Y1 = vl; }
                    if (tg == "PY  - ") { Y1 = vl; }
                    if (tg == "Y2  - ") { Y2 = vl; }
                    if (tg == "RP  - ") { RP = vl; }
                    if (tg == "JF  - ") { JF = vl; }
                    if (tg == "JO  - ") { JA = vl; }
                    if (tg == "JA  - ") { JA = vl; }
                    if (tg == "J1  - ") { J1 = vl; }
                    if (tg == "J2  - ") { J2 = vl; }
                    if (tg == "VL  - ") { VOL = vl; }
                    if (tg == "IS  - ") { IS_ = vl; }
                    if (tg == "CP  - ") { IS_ = vl; }
                    if (tg == "SP  - ") { SP = vl; }
                    if (tg == "EP  - ") { EP = vl; }
                    if (tg == "CY  - ") { CY = vl; }
                    if (tg == "PB  - ") { PB = vl; }
                    if (tg == "SN  - ") { SN = vl; }
                    if (tg == "AV  - ") { AV = vl; }
                    if (tg == "M1  - ") { M1 = vl; }
                    if (tg == "M2  - ") { M2 = vl; }
                    if (tg == "M3  - ") { M3 = vl; }
                    if (tg == "U1  - ") { U1 = vl; }
                    if (tg == "U2  - ") { U2 = vl; }
                    if (tg == "U3  - ") { U3 = vl; }
                    if (tg == "U4  - ") { U4 = vl; }
                    if (tg == "U5  - ") { U5 = vl; }
                    if (tg == "UR  - ") { UR = vl; }
                    string aut = string.Empty;
                    if ((tg == "ER  - ") && (t1 != string.Empty))
                    {
                        if (a1 != null)
                        {
                            for (int a = 0; a < a1.Count; a++)
                            {
                                string strContact = a1[a];
                                if (strContact != string.Empty)
                                {
                                    if (a == 0)
                                        aut = a1[a];
                                    else
                                        aut = aut + ", " + a1[a];
                                    // Person cont = Routines.StringToContact(objectSpace, se, strContact);
                                    //r.AuthorUsers.Add(cont);
                                }
                            }
                        }

                        Reference r = uow.FindObject<Reference>(PersistentCriteriaEvaluationBehavior.InTransaction,
                                                CriteriaOperator.And(
                                new BinaryOperator("Title", t1),
                                new BinaryOperator("Title2", t2),
                                new BinaryOperator("Title3", t3),
                                new BinaryOperator("Authors", aut)
                                )
     );
                        if (r == null)
                            r = uow.FindObject<Reference>(
                                                    CriteriaOperator.And(
                                new BinaryOperator("Title", t1),
                                new BinaryOperator("Title2", t2),
                                new BinaryOperator("Title3", t3),
                                new BinaryOperator("Authors", aut)
                                )
     );
                        if (r == null)
                        {
                            r = new Reference(uow);// objectSpace.CreateObject<Reference>();
                            r.Authors = aut;
                            r.Title = t1;
                            r.Title2 = t2;
                            r.Title3 = t3;
                            r.Pubcity = PB;
                            r.SN = SN;
                            r.Volume = VOL;
                            r.Issue = IS_;
                            r.Page1 = SP;
                            r.Page2 = EP;
                            r.Jourfull = JF;
                            try
                            {
                                r.Year = Convert.ToInt16(Y1);

                            }
                            catch (Exception)
                            {


                            }
                            r.Date2 = Y2;
                            r.Printst = RP;
                            r.Jourab = JA;
                            //  r.Jourab_u1 = J1;
                            // r.Jourab_u2 = J2;
                            r.Availability = AV;

                            if (n1 != null)
                                for (int i = 0; i < n1.Count; i++)
                                {
                                    if (r.Abstract == null)
                                    {
                                        r.Abstract = n1[i];
                                    }
                                    else
                                    {
                                        r.Abstract = r.Abstract + "\r\n" + n1[i];
                                    }
                                }
                            if (n2 != null)
                                for (int d = 0; d < n2.Count; d++)
                                {
                                    if (r.Description == null)
                                    {
                                        r.Description = n2[d];
                                    }
                                    else
                                    {
                                        r.Description = r.Description + "\r\n" + n2[d];
                                    }
                                }
                            if (ad != null)
                                for (int a = 0; a < ad.Count; a++)
                                {
                                    if (r.Abstract == null)
                                    {
                                        r.Abstract = ad[a];
                                    }
                                    else
                                    {
                                        r.Abstract = r.Abstract + "\r\n" + ad[a];
                                    }
                                }
                            if (kw != null)
                                for (int a = 0; a < kw.Count; a++)
                                {
                                    if (kw[a] != string.Empty)
                                    {
                                        /*  Term cont = uow.FindObject<Term>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", kw[a]));
                                          if (cont == null)
                                              cont = uow.FindObject<Term>(new BinaryOperator("Word", kw[a]));
                                          if (cont == null)
                                          {
                                              cont = new Term(uow);// uow.CreateObject<Term>();
                                              cont.Word = kw[a];
                                              cont.Save();
                                          }*/
                                        //   r.TagCategorys.Add(cont);
                                    }
                                }
                        }
                        //r.Datasets.Add(Dataset);
                        r.Save();

                        ty = string.Empty; t1 = string.Empty; t2 = string.Empty; t3 = string.Empty; id = string.Empty; bt = string.Empty; Y1 = string.Empty; Y2 = string.Empty; RP = string.Empty; JF = string.Empty; JO = string.Empty; JA = string.Empty; J1 = string.Empty; J2 = string.Empty; VOL = string.Empty; IS_ = string.Empty; SP = string.Empty; EP = string.Empty; CY = string.Empty; PB = string.Empty; SN = string.Empty; AV = string.Empty; M1 = string.Empty; M2 = string.Empty; M3 = string.Empty; U1 = string.Empty; U2 = string.Empty; U3 = string.Empty; U4 = string.Empty; U5 = string.Empty; UR = string.Empty;
                        a1.Clear();
                        a2.Clear();
                        a3.Clear();
                        kw.Clear();
                        n1.Clear();
                        n2.Clear();
                        ad.Clear();
                        try
                        {
                            uow.CommitChanges();
                        }
                        catch (Exception)
                        {
                            //  uow.RollbackTransaction();

                        }
                    }
                }
                else
                {
                    vl = line;
                    if (tg == "N1  - ") { n1.Add(vl); }
                    if (tg == "AB  - ") { n1.Add(vl); }
                    if (tg == "N2  - ") { n2.Add(vl); }
                    if (tg == "AD  - ") { ad.Add(vl); }
                }
                line = sr.ReadLine();
                vl = string.Empty;
            }
            sr.Close();
            uow.CommitChanges();
        }

        /*import od atomized citation
         
             private void ImportRIS(string fn)
        {
            String line;
            XPObjectSpace objectSpace = (XPObjectSpace)Application.CreateObjectSpace();
            Session se = objectSpace.Session;
            string tg, vl;
            Collection<string> a1 = new Collection<string>();
            Collection<string> a2 = new Collection<string>();
            Collection<string> a3 = new Collection<string>();
            Collection<string> kw = new Collection<string>();
            Collection<string> n1 = new Collection<string>();
            Collection<string> n2 = new Collection<string>();
            Collection<string> ad = new Collection<string>();

            StreamReader sr = new StreamReader(fn);
            line = sr.ReadLine();
            tg = line;
            while (line != null)
            {
                if ((line != "") && (line.Length > 5) && (isTagCategory(line.Substring(0, 6))))
                {
                    tg = line.Substring(0, 6);
                    vl = line.Substring(6, line.Length - 6);
                    if ((vl.Length > 255) && (tg != "AU  - ") && (tg != "A2  - ") && (tg != "ED  - ") && (tg != "A3  - ") && (tg != "KW  - ") && (tg != "N1  - ") && (tg != "N2  - ") && (tg != "AB  - ") && (tg != "AB  - ") && (tg != "AD  - "))
                    {
                        vl = vl.Substring(0, 255);
                    }
                    if ((vl.Length > 100) && ((tg == "Y1  - ") || (tg == "Y2  - ")))
                    {
                        vl = vl.Substring(0, 100);
                    }

                    if (tg == "T1  - ") { t1 = vl; }
                    if (tg == "TY  - ") { ty = vl; }
                    if (tg == "CT  - ") { t1 = vl; }
                    if (tg == "TI  - ") { t1 = vl; }
                    if (tg == "T2  - ") { t2 = vl; }
                    if (tg == "T3  - ") { t3 = vl; }
                    if (tg == "ID  - ") { id = vl; }
                    if (tg == "BT  - ") { bt = vl; }
                    if (tg == "A1  - ")
                    {
                        a1.Add(vl);
                    }
                    if (tg == "AU  - ")
                    {
                        a1.Add(vl);
                    }
                    if (tg == "A2  - ")
                    {
                        a2.Add(vl);
                    }
                    if (tg == "ED  - ")
                    {
                        a2.Add(vl);
                    }
                    if (tg == "A3  - ")
                    {
                        a3.Add(vl);
                    }
                    if (tg == "KW  - ")
                    {
                        char[] delimiterChars = { ',', ',', '.', ';' };
                        string[] words = vl.Split(delimiterChars);
                        foreach (string word in words)
                        {
                            kw.Add(word.Trim());
                        }
                    }
                    if (tg == "N1  - ")
                    {
                        n1.Add(vl);
                    }
                    if (tg == "AB  - ")
                    {
                        n1.Add(vl);
                    }
                    if (tg == "N2  - ")
                    {
                        n2.Add(vl);
                    }
                    if (tg == "AD  - ")
                    {
                        ad.Add(vl);
                    }
                    if (tg == "Y1  - ") { Y1 = vl; }
                    if (tg == "PY  - ") { Y1 = vl; }
                    if (tg == "Y2  - ") { Y2 = vl; }
                    if (tg == "RP  - ") { RP = vl; }
                    if (tg == "JF  - ") { JF = vl; }
                    if (tg == "JO  - ") { JA = vl; }
                    if (tg == "JA  - ") { JA = vl; }
                    if (tg == "J1  - ") { J1 = vl; }
                    if (tg == "J2  - ") { J2 = vl; }
                    if (tg == "VL  - ") { VOL = vl; }
                    if (tg == "IS  - ") { IS_ = vl; }
                    if (tg == "CP  - ") { IS_ = vl; }
                    if (tg == "SP  - ") { SP = vl; }
                    if (tg == "EP  - ") { EP = vl; }
                    if (tg == "CY  - ") { CY = vl; }
                    if (tg == "PB  - ") { PB = vl; }
                    if (tg == "SN  - ") { SN = vl; }
                    if (tg == "AV  - ") { AV = vl; }
                    if (tg == "M1  - ") { M1 = vl; }
                    if (tg == "M2  - ") { M2 = vl; }
                    if (tg == "M3  - ") { M3 = vl; }
                    if (tg == "U1  - ") { U1 = vl; }
                    if (tg == "U2  - ") { U2 = vl; }
                    if (tg == "U3  - ") { U3 = vl; }
                    if (tg == "U4  - ") { U4 = vl; }
                    if (tg == "U5  - ") { U5 = vl; }
                    if (tg == "UR  - ") { UR = vl; }
                    if (tg == "ER  - ")
                    {
                        if ((ty == "THES") || (ty == "BOOK") || (ty == "GEN") || (ty == "CHAP") || (ty == "JOUR") || (ty == "RPT") || (ty == "MGZN") || (ty == "NEWS"))
                        {
                            Reference r = null;
                            if (ty == "BOOK")
                            {
                                r = objectSpace.CreateObject<Book>();
                                if (bt != "")
                                    r.Title = bt;
                                if (t1 != "")
                                    r.Title = t1;
                            }
                            if (ty == "GEN")
                            {
                                r = objectSpace.CreateObject<Reference>();
                                r.Title = t1;
                            }
                            if (ty == "CHAP")
                            {
                                r = objectSpace.CreateObject<BookChapter>();
                                if (t2 != "")
                                    (r as BookChapter).Book = MediaRoutines.GetBook(objectSpace, se, bt);
                                r.Title = t1;
                            }
                            if (ty == "THES")
                            {
                                r = objectSpace.CreateObject<Thesis>();
                                r.Title = t1;
                            }
                            if (ty == "RPT")
                            {
                                r = objectSpace.CreateObject<Report>();
                                r.Title = t1;
                            }
                            if (ty == "JOUR")
                            {
                                r = objectSpace.CreateObject<ArticleJournal>();
                                    if (JF != "")
                                        (r as ArticleJournal).Journal = MediaRoutines.GetJournal(objectSpace, se, JF);

                                (r as ArticleJournal).Volume = VOL;
                                (r as ArticleJournal).Issue = IS_;
                                r.Title = t1;
                                /*MatchCollection pages = Regex.Matches(SP, @"(\d+\.?\d*|\.\d+)");

                                if (pages.Count > 0)
                                {
                                    (r as Article).Page1 = Convert.ToInt32(pages[0]);
                                }
                                if (pages.Count > 1)
                                {
                                    (r as Article).Page2 = Convert.ToInt32(pages[1]);
                                }
                            }
                            if (ty == "MGZN")
                            {
                                r = objectSpace.CreateObject<ArticleMagazine>();
                                    if (bt != "")
                                        (r as ArticleMagazine).Magazine = MediaRoutines.GetMagazine(objectSpace, se, bt);

                                (r as ArticleMagazine).Volume = VOL;
                                (r as ArticleMagazine).Issue = IS_;
                                r.Title = t1;
                            }
                            if (ty == "NEWS")
                            {
                                r = objectSpace.CreateObject<ArticleNewspaper>();
                                    if (bt != "")
                                        (r as ArticleNewspaper).Newspaper = MediaRoutines.GetNewspaper(objectSpace, se, bt);

                                (r as ArticleNewspaper).Volume = VOL;
                                (r as ArticleNewspaper).Issue = IS_;
                                r.Title = t1;
                            }
                            if (!string.IsNullOrEmpty(Y1))
                            r.Year =Convert.ToInt16(Y1);
                            /*r.Title = t1;
                                 r.Title2 = t2;
                                 r.Title3 = t3;
                                r.Pubcity = PB;
                                 r.SN = SN;
                                   r.Jourfull = JF;
                                 r.Year = Y1;
                                 r.Date2 = Y2;
                                 r.Printst = RP;
                                 r.Jourab = JA;
                                 r.Jourab_u1 = J1;
                                 r.Jourab_u2 = J2;
                                 r.Availability = AV;
                                 r.M1 = M1;
                                 r.M2 = M2;
                                 r.M3 = M3;
                                 r.U1 = U1;
                                 r.U2 = U2;
                                 r.U3 = U3;
                                 r.U4 = U4;
                                 r.U5 = U5;


                            r.UserId = id;

                            if (n1 != null)
                                for (int i = 0; i < n1.Count; i++)
                                {
                                    if (r.Comment == null)
                                    {
                                        r.Comment = n1[i];
                                    }
                                    else
                                    {
                                        r.Comment = r.Comment + "\r\n" + n1[i];
                                    }
                                }
                            if (n2 != null)
                                for (int d = 0; d < n2.Count; d++)
                                {
                                    if (r.Description == null)
                                    {
                                        r.Description = n2[d];
                                    }
                                    else
                                    {
                                        r.Description = r.Description + "\r\n" + n2[d];
                                    }
                                }
                            if (ad != null)
                                for (int a = 0; a < ad.Count; a++)
                                {
                                    if (r.Comment == null)
                                    {
                                        r.Comment = ad[a];
                                    }
                                    else
                                    {
                                        r.Comment = r.Comment + "\r\n" + ad[a];
                                    }
                                }
                            if (a1 != null)
                            {
                                string aut = "";
                                for (int a = 0; a < a1.Count; a++)
                                {
                                    string strContact = a1[a];
                                    if (strContact != "")
                                    {
                                        if (a == 0)
                                            aut = a1[a];
                                        else
                                            aut = aut + ", " + a1[a];
                                        // Person cont = Routines.StringToContact(objectSpace, se, strContact);
                                        //r.AuthorUsers.Add(cont);
                                    }
                                }
                                r.Authors = aut;
                            }
                            if (kw != null)
                                for (int a = 0; a < kw.Count; a++)
                                {
                                    if (kw[a] != "")
                                    {
                                        Term cont = se.FindObject<Term>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Word", kw[a]));
                                        if (cont == null)
                                            cont = objectSpace.FindObject<Term>(new BinaryOperator("Word", kw[a]));
                                        if (cont == null)
                                        {
                                            cont = objectSpace.CreateObject<Term>();
                                            cont.Word = kw[a];
                                            cont.Save();
                                        }
                                        r.TagCategorys.Add(cont);
                                    }
                                }
                            r.Save();
                           }
                        ty = ""; t1 = ""; t2 = ""; t3 = ""; id = ""; bt = ""; Y1 = ""; Y2 = ""; RP = ""; JF = ""; JO = ""; JA = ""; J1 = ""; J2 = ""; VOL = ""; IS_ = ""; SP = ""; EP = ""; CY = ""; PB = ""; SN = ""; AV = ""; M1 = ""; M2 = ""; M3 = ""; U1 = ""; U2 = ""; U3 = ""; U4 = ""; U5 = ""; UR = "";
                        a1.Clear();
                        a2.Clear();
                        a3.Clear();
                        kw.Clear();
                        n1.Clear();
                        n2.Clear();
                        ad.Clear();
                    }
                }
                else
                {
                    vl = line;
                    if (tg == "N1  - ") { n1.Add(vl); }
                    if (tg == "AB  - ") { n1.Add(vl); }
                    if (tg == "N2  - ") { n2.Add(vl); }
                    if (tg == "AD  - ") { ad.Add(vl); }
                }
                line = sr.ReadLine();
                vl = "";
                try
                {
                    objectSpace.CommitChanges();
                }
                finally { }
            }
            sr.Close();
            objectSpace.CommitChanges();
        }

         */

        public static bool isTagCategory(string p)
        {
            foreach (string s in ArrayOfTagCategorys)
            {
                if (s == p)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
