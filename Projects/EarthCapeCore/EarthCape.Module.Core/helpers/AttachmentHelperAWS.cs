﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace EarthCapePro.Module
{
    public class AttachmentHelperAWS
    {
        public static void SaveToS3(Attachment attach, AttachmentStorageAmazonS3 storage, Stream stream, string Key)
        {
            string bucket = storage.Bucket;
            var client = new AmazonS3Client(GetAmazonCredentials(storage), RegionEndpoint.EUWest1);
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = bucket,
                Key = Key,
                InputStream = stream,
                CannedACL = S3CannedACL.PublicRead
            };
            GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
            {
                BucketName = bucket,
                Key = Key,
                Expires = DateTime.Now.AddHours(1),
                Protocol = Protocol.HTTP
            };
            string url = client.GetPreSignedURL(request1);
            url = url.Remove(url.IndexOf('?'));
            attach.Url = url;
            client.PutObject(request);
        }


        public static BasicAWSCredentials GetAmazonCredentials(AttachmentStorageAmazonS3 attachmentStorageAmazonS3)
        {
            return new BasicAWSCredentials(attachmentStorageAmazonS3.AwsAccessKey, attachmentStorageAmazonS3.AwsSecretKey);
        }

    }
}
