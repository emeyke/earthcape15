﻿using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;

using System.Data;
using System.Text;

namespace EarthCape.Taxonomy
{
    public class TaxonomyHelper
    {
        public static Genus GetGenus(UnitOfWork uow, Project Project, string genus_string, string author, int year)
        {
            Genus genus = null;
            genus = uow.FindObject<Genus>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", genus_string));
            if (genus == null)
            {
                genus = new Genus(uow);
                genus.Name = genus_string;
                genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow,null,null, "Genus");
                genus.AuthorshipYear = year;
                genus.Authorship = author;
                if (Project != null)
                {
                    genus.Project = Project;
                }
                genus.Save();
            }
            else
            {
                if (Project != null)
                {
                    genus.Project = Project;
                    genus.Save();
                }
            }
            return genus;
        }
        public static Genus GetGenus(IObjectSpace uow, Project Project, string genus_string, string author, int year)
        {
            Genus genus = null;
            genus = uow.FindObject<Genus>(new BinaryOperator("Name", genus_string), true);
            if (genus == null)
            {
                genus = uow.CreateObject<Genus>();
                genus.Name = genus_string;
                genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,uow,null, "Genus");
                genus.AuthorshipYear = year;
                genus.Authorship = author;
                if (Project != null)
                {
                    genus.Project = Project;
                }
                genus.Save();
            }
            else
            {
                if (Project != null)
                {
                    genus.Project = Project;
                    genus.Save();
                }
            }
            return genus;
        }
        public static void ParseBinomial(IObjectSpace uow, ref Species species, ref Genus genus, string auth, string year, string parse_binomial, string sep, Project Project)
        {
            if (parse_binomial.Contains(sep))
            {
                var gen = parse_binomial.Split(new string[] { sep }, 2, StringSplitOptions.None)[0].Trim();
                var sp = parse_binomial.Split(new string[] { sep }, 2, StringSplitOptions.None)[1].Trim();
                var n = TaxonomyHelper.GetTaxon(null, uow, gen, sp, auth);
                if (n is Species)
                {
                    species = (Species)n;
                    genus = species.Genus;
                    genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,uow,null, "Genus");
                    genus.Save();
                }
                if (n is Genus)
                {
                    genus = (Genus)n;
                    genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,uow,null, "Genus");
                    genus.Save();
                }
            }
        }
        public static void ParseBinomial(UnitOfWork uow, ref Species species, ref Genus genus, string auth, string year, string parse_binomial, string sep, Project Project)
        {
            if (parse_binomial.Contains(sep))
            {
                var gen = parse_binomial.Split(new string[] { sep }, 2, StringSplitOptions.None)[0].Trim();
                var sp = parse_binomial.Split(new string[] { sep }, 2, StringSplitOptions.None)[1].Trim();
                var n = TaxonomyHelper.GetTaxon(null, uow, gen, sp, auth);
                if (n is Species)
                {
                    species = (Species)n;
                    genus = species.Genus;
                    genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow,null,null, "Genus");
                    genus.Save();
                }
                if (n is Genus)
                {
                    genus = (Genus)n;
                    genus.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow,null,null, "Genus");
                    genus.Save();
                }
            }
        }
        public static string GetAuth(Species species)
        {
            string auth;
            if (!String.IsNullOrEmpty(species.Authorship))
            {
                auth = species.Authorship;
            }
            else
            {
                auth = string.Empty;
            }
            if (species.AuthorshipYear > 0)
            {
                auth = String.Format("{0} {1}", auth, species.AuthorshipYear);
            }
            return auth;
        }

        public static TaxonomicName GetTaxon(Project project, UnitOfWork uow, string genus, string species, string authoryear)
        {
            Genus t1 = null;
            if (!string.IsNullOrEmpty(genus))
            {
                t1 = uow.FindObject<Genus>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", genus));
                if (t1 == null)
                {
                    t1 = uow.FindObject<Genus>(new BinaryOperator("Name", genus));
                }
                if (t1 != null)
                {
                    if (string.IsNullOrEmpty(species))
                    {
                        return t1;
                    }
                }
                else
                {
                    t1 = new Genus(uow);
                    t1.Name = genus;
                    t1.Project = project;
                    t1.UpdateInfo();
                    t1.Save();
                }
            }
            Species spec = null;
            if (!string.IsNullOrEmpty(species))
            {
                spec = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", species), new BinaryOperator("Genus.Name", genus)));
                if (spec == null)
                {
                    spec = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", (!string.IsNullOrEmpty(genus)) ? genus + " " + species : species));
                }
                if (spec != null)
                {
                    return spec;
                }
                spec = new Species(uow);
                spec.Name = species;
                spec.Genus = t1;
                spec.Project = project;
                spec.UpdateInfo();
                spec.Save();
            }
            if (spec != null)
            {
                return spec;
            }
            if (t1 != null)
            {
                return t1;
            }
            return null;
        }
        public static TaxonomicName GetTaxon(Project project, IObjectSpace uow, string genus, string species, string authoryear)
        {
            Genus t1 = null;
            if (!string.IsNullOrEmpty(genus))
            {
                t1 = uow.FindObject<Genus>(new BinaryOperator("Name", genus), true);
                if (t1 != null)
                {
                    if (string.IsNullOrEmpty(species))
                    {
                        return t1;
                    }
                }
                else
                {
                    t1 = uow.CreateObject<Genus>();
                    t1.Name = genus;
                    t1.Project = project;
                    t1.UpdateInfo();
                    t1.Save();
                }
            }
            Species spec = null;
            if (!string.IsNullOrEmpty(species))
            {
                spec = uow.FindObject<Species>(CriteriaOperator.And(new BinaryOperator("Name", species), new BinaryOperator("Genus.Name", genus)), true);
                if (spec != null)
                {
                    return spec;
                }
                spec = uow.CreateObject<Species>();
                spec.Name = species;
                spec.Genus = t1;
                spec.Project = project;
                spec.Save();
            }
            if (spec != null)
            {
                return spec;
            }
            if (t1 != null)
            {
                return t1;
            }
            return null;
        }
        public static string SetSize(string ThumbnailUrl, int p)
        {
            var pos = ThumbnailUrl.IndexOf("svc.scale=") + "svc.scale=".Length;
            var result = ThumbnailUrl.Substring(0, pos);
            return result + Convert.ToString(p);
        }

        public static TaxonomicCategory FindRank(XPObjectSpace objectSpace, string ra)
        {
            var rank = objectSpace.FindObject<TaxonomicCategory>(new BinaryOperator("Word", ra));
            if (rank == null)
            {
                rank = objectSpace.FindObject<TaxonomicCategory>(new BinaryOperator("Word", "<Unspecified>"));
            }
            return rank;
        }



        internal static void GniParse(XPObjectSpace objectSpace, TaxonomicName item, CollectionSource ds)
        {
        }

        internal static void UBioXML(TaxonomicName item)
        {
        }

        public static void UBio(TaxonomicName item)
        {
        }


        internal static TaxonomicName NameBankObjectToName(UnitOfWork uow, int? p)
        {
            using (var dataSet = new DataSet())
            {
                try
                {
                    dataSet.ReadXml(String.Format("http://www.ubio.org/webservices/service.php?function=namebank_object&namebankID={0}&keyCode=8661de3b5221b8b61a50f6a0863ea10a6b73b6b6", p.ToString()), XmlReadMode.InferSchema);
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                }
                var table = dataSet.Tables["results"];
                if (table == null)
                {
                    return null;
                }
                foreach (DataRow row in table.Rows)
                {
                    if (table.Columns.IndexOf("rankName") > -1)
                    {
                        var nameString = Encoding.Default.GetString(Convert.FromBase64String(row["nameString"].ToString()));
                        if (row["rankName"].ToString() == "Species")
                        {
                            Species name;
                            var cr = CriteriaOperator.And(new BinaryOperator("Name", nameString), new BinaryOperator("UbioNameBankId", p));
                            name = uow.FindObject<Species>(cr);
                            if (name == null)
                            {
                                name = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                            }
                            if (name == null)
                            {
                                name = new Species(uow)
                                {
                                    Name = nameString
                                };
                                name.Save();
                            }
                            if (table.Columns.IndexOf("parentID") > -1)
                            {
                                name.Genus = (Genus)NameBankObjectToName(uow, Convert.ToInt32(row["parentID"].ToString()));
                            }
                            return name;
                        }
                        if (row["rankName"].ToString() == "Genus")
                        {
                            Genus name;
                            var cr = CriteriaOperator.And(new BinaryOperator("Name", nameString), new BinaryOperator("UbioNameBankId", p));
                            name = uow.FindObject<Genus>(cr);
                            if (name == null)
                            {
                                name = uow.FindObject<Genus>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                            }
                            if (name == null)
                            {
                                name = new Genus(uow)
                                {
                                    Name = nameString
                                };
                                name.Save();
                            }
                            return name;
                        }
                        if (row["rankName"].ToString() == "Family")
                        {
                            Family name;
                            var cr = CriteriaOperator.And(new BinaryOperator("Name", nameString), new BinaryOperator("UbioNameBankId", p));
                            name = uow.FindObject<Family>(cr);
                            if (name == null)
                            {
                                name = uow.FindObject<Family>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                            }
                            if (name == null)
                            {
                                name = new Family(uow)
                                {
                                    Name = nameString
                                };
                                name.Save();
                            }
                            return name;
                        }
                    }
                }
            }
            return null;
        }

        internal static void GetUBioData(TaxonomicName TaxonomicName)
        {
        }
    }
}
