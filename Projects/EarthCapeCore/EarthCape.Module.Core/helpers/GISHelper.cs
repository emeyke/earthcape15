using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using TatukGIS.NDK;


namespace EarthCape.Module.Core
{
    public class GISHelper
    {
        public static CriteriaOperator GetMapCriteria(TGIS_Extent extent)
        {

            double xmin = extent.XMin;
            double xmax = extent.XMax;
            double ymin = extent.YMin;
            double ymax = extent.YMax;

            CriteriaOperator cr1 = new BinaryOperator("Centroid_X", xmin, BinaryOperatorType.Greater);
            CriteriaOperator cr2 = new BinaryOperator("Centroid_Y", ymin, BinaryOperatorType.Greater);
            CriteriaOperator cr3 = new BinaryOperator("Centroid_Y", ymax, BinaryOperatorType.Less);
            CriteriaOperator cr4 = new BinaryOperator("Centroid_X", xmax, BinaryOperatorType.Less);
            CriteriaOperator cr = CriteriaOperator.And(cr1, cr2, cr3, cr4);
            return cr;
        }

        public static void ConvertToEPSG(IWKT iwkt, int sourceEPSG, int targetEPSG)
        {
            if (iwkt == null) return;
            if (String.IsNullOrEmpty(iwkt.WKT)) return;
            TGIS_CSCoordinateSystem sourceCS = TGIS_Utils.CSGeographicCoordinateSystemList.ByEPSG(sourceEPSG);
            if (sourceCS == null)
                sourceCS = TGIS_Utils.CSProjectedCoordinateSystemList.ByEPSG(sourceEPSG);
            if (sourceCS == null) return;
            TGIS_CSCoordinateSystem targetCS = TGIS_Utils.CSGeographicCoordinateSystemList.ByEPSG(targetEPSG);
            if (targetCS == null)
                targetCS = TGIS_Utils.CSProjectedCoordinateSystemList.ByEPSG(targetEPSG);
            if (targetCS == null) return;
            TGIS_Shape shp1 = TGIS_Utils.GisCreateShapeFromWKT(iwkt.WKT);
            TGIS_Shape shp2 = TGIS_Utils.GisCreateReprojectedShape(shp1, sourceCS, targetCS);
            iwkt.WKT = shp2.ExportToWKT();
            iwkt.EPSG = targetEPSG;
            UpdateAreaAndLengthFromWKT(iwkt);
        }
        public static void GenerateGridForExtent(TGIS_Extent ext, TGIS_LayerVector ll, IObjectSpace os, double width, double height, string prefix, TGIS_Shape selectedShape, int margin, bool clipToShape)
        {
            double xmin = ext.XMin - width * margin;
            double xmax = ext.XMax + width * margin;
            double ymin = ext.YMin - height * margin;
            double ymax = ext.YMax + height * margin;
            int xcells = Convert.ToInt32((xmax - xmin) / width);
            int ycells = Convert.ToInt32((ymax - ymin) / height);
            double xshift = xmin;
            double yshift = ymin;
            int count = 0;
            for (int i = 1; i < ycells + 2; i++)
            {
                for (int j = 1; j < xcells + 2; j++)
                {
                    count += 1;
                    TGIS_ShapePolygon shp = new TGIS_ShapePolygon();

                    shp.AddPart();
                    TGIS_Point ptg = new TGIS_Point(xshift, yshift);
                    shp.AddPoint(ptg);
                    ptg = new TGIS_Point(xshift + width, yshift);
                    shp.AddPoint(ptg);
                    ptg = new TGIS_Point(xshift + width, yshift + height);
                    shp.AddPoint(ptg);
                    ptg = new TGIS_Point(xshift, yshift + height);
                    shp.AddPoint(ptg);
                    if (((selectedShape.IsCommonPoint(shp)) && clipToShape) || ((selectedShape != null) && (!clipToShape)))
                    {
                        string shp_name = string.Empty;
                        if (string.IsNullOrEmpty(prefix))
                            shp_name = String.Format("{0}-{1}", j, i);
                        else
                            shp_name = String.Format("{0}-{1}", prefix, count);
                        Locality loc = os.CreateObject<Locality>();
                        loc.Name = shp_name;
                        TGIS_Shape grid_shp = ll.AddShape(shp);
                        loc.WKT = shp.ExportToWKT();
                        GISHelper.UpdateAreaAndLengthFromWKT(loc);
                        loc.Save();
                        grid_shp.SetField("Label", loc.Name);
                        grid_shp.SetField("ECID", loc.Oid.ToString());
                        grid_shp.SetField("ObjectType", "Locality");
                    }

                    xshift = xshift + width;
                }
                xshift = xmin;
                yshift = yshift + height;
            }
            ll.SaveAll();
        }

        public static void LayerParams(TGIS_LayerVector ll)
        {
            AddFields(ll);
            //   ll.Params.Labels.Height = -20;
            //  ll.Params.Labels.Width = -100;
            ll.Params.Labels.Position =
                    //TGIS_LabelPositions.gisLabelPositionFlow |
                    TGIS_LabelPositions.gisLabelPositionUpRight |
                    TGIS_LabelPositions.gisLabelPositionDownLeft |
                    TGIS_LabelPositions.gisLabelPositionDownRight |
                    TGIS_LabelPositions.gisLabelPositionDownLeft;
            if (ll.Name == "Localities")
            {
                ll.Params.Labels.Field = "Locality";
                ll.Params.Labels.Color = Color.Yellow;
                ll.Params.Labels.FontColor = Color.Black;
                ll.Params.Line.Width = 70;
                //  ll.Params.Line.Style = TGIS_PenStyle.gisPsDashDotDot;
                ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsBDiagonal;
                ll.Params.Line.Color = Color.Red;
                ll.Params.Area.Color = Color.Blue;
                ll.Params.Area.OutlineWidth = 30;
                ll.Params.Area.OutlineColor = Color.Red;
                ll.Params.Marker.Color = Color.Blue;
                ll.Params.Marker.Size = 150;
                ll.Params.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleTriangleUp;
                ll.Params.Marker.OutlineColor = Color.Black;
                ll.Params.Marker.OutlineWidth = 10;
            }
            if (ll.Name == "Store")
            {
                ll.Params.Labels.Field = "Name";
                ll.Params.Labels.Color = Color.Yellow;
                ll.Params.Labels.FontColor = Color.Black;
                ll.Params.Line.Width = 70;
                //  ll.Params.Line.Style = TGIS_PenStyle.gisPsDashDotDot;
                ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsBDiagonal;
                ll.Params.Line.Color = Color.Red;
                ll.Params.Area.Color = Color.Green;
                ll.Params.Area.OutlineWidth = 30;
                ll.Params.Area.OutlineColor = Color.Red;
                ll.Params.Marker.Color = Color.Blue;
                ll.Params.Marker.Size = 150;
                ll.Params.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleTriangleUp;
                ll.Params.Marker.OutlineColor = Color.Black;
                ll.Params.Marker.OutlineWidth = 10;
            }
            if (ll.Name == "Units")
            {
                ll.Params.Labels.Color = Color.Yellow;
                ll.Params.Labels.Field = "UnitID";
                ll.Params.Labels.FontColor = Color.Black;
                //    ll.Transparency = 40;
                ll.Params.Line.Width = 70;
                //  ll.Params.Line.Style = TGIS_PenStyle.gisPsDashDotDot;
                ll.Params.Line.Color = Color.Red;
                ll.Params.Area.Color = Color.Blue;
                ll.Params.Area.OutlineWidth = 30;
                ll.Params.Area.OutlineColor = Color.Green;
                ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsBDiagonal;
                ll.Params.Marker.Color = Color.Lime;
                ll.Params.Marker.Size = 130;
                ll.Params.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleCircle; ;
                ll.Params.Marker.OutlineColor = Color.Black;
                ll.Params.Marker.OutlineWidth = 20;
            }
        }
        public static void AddFields(TGIS_LayerVector ll)
        {
            if (ll.FindField("ECID") == -1)
                ll.AddField("ECID", TGIS_FieldType.gisFieldTypeString, 30, 0);
            if (ll.FindField("ID") == -1)
                ll.AddField("ID", TGIS_FieldType.gisFieldTypeString, 30, 0);
            if (ll.FindField("Label") == -1)
                ll.AddField("Label", TGIS_FieldType.gisFieldTypeString, 30, 0);
            if (ll.FindField("OwnGeometry") == -1)
                ll.AddField("OwnGeometry", TGIS_FieldType.gisFieldTypeBoolean, 1, 0);
            if (ll.FindField("Edited") == -1)
                ll.AddField("Edited", TGIS_FieldType.gisFieldTypeBoolean, 1, 0);
            if (ll.FindField("Units") == -1)
                ll.AddField("Units", TGIS_FieldType.gisFieldTypeNumber, 6, 0);
            if (ll.FindField("Locality") == -1)
                ll.AddField("Locality", TGIS_FieldType.gisFieldTypeString, 100, 0);
            if (ll.FindField("TaxonomicName") == -1)
                ll.AddField("TaxonomicName", TGIS_FieldType.gisFieldTypeString, 100, 0);
            if (ll.FindField("Project") == -1)
                ll.AddField("Project", TGIS_FieldType.gisFieldTypeString, 100, 0);
            if (ll.FindField("Dataset") == -1)
                ll.AddField("Dataset", TGIS_FieldType.gisFieldTypeString, 100, 0);
            if (ll.FindField("UnitID") == -1)
                ll.AddField("UnitID", TGIS_FieldType.gisFieldTypeString, 30, 0);
            if (ll.FindField("Date") == -1)
                ll.AddField("Date", TGIS_FieldType.gisFieldTypeDate, 20, 0);
            if (ll.FindField("ObjectType") == -1)
                ll.AddField("ObjectType", TGIS_FieldType.gisFieldTypeString, 20, 0);
            if (ll.FindField("dummy") == -1)
                ll.AddField("dummy", TGIS_FieldType.gisFieldTypeString, 1, 0);

        }
        public static TGIS_Shape ConvertEPSG(int SourceEpsg, TGIS_Shape shp2, int TargetEpsg)
        {
            TGIS_CSCoordinateSystem sourceCS = TGIS_Utils.CSGeographicCoordinateSystemList.ByEPSG(SourceEpsg);
            if (sourceCS == null)
                sourceCS = TGIS_Utils.CSProjectedCoordinateSystemList.ByEPSG(SourceEpsg);
            if (sourceCS == null)
            {
                return null;
            }
            TGIS_CSCoordinateSystem targetCS = TGIS_Utils.CSGeographicCoordinateSystemList.ByEPSG(TargetEpsg);
            if (targetCS == null)
                targetCS = TGIS_Utils.CSProjectedCoordinateSystemList.ByEPSG(TargetEpsg);
            if (targetCS == null)
            {
                return null;
            }
            return TGIS_Utils.GisCreateReprojectedShape(shp2, sourceCS, targetCS);
        }
        /*   public static void MapUnits(Collection<Unit> coll, TGIS_LayerVector ll)
{
    foreach (Unit item in coll)
    {
        AddShapeFromWKT(item, ll);
    }
}*/

        public static TGIS_Shape AddShapeFromWKT(string wkt, TGIS_LayerVector ll, string ID, Guid Oid, string label, int SourceEpsg, bool centroidOnly, bool ownGeometry, string objectType)
        {
            TGIS_Shape shp = null;
            for (int i = 0; i < ll.Items.Count; i++)
            {
                TGIS_Shape tmp_shp = (TGIS_Shape)ll.Items[i];
                string item_oid = Oid.ToString();
                string shp_oid = tmp_shp.GetField("ID").ToString();
                if (shp_oid == item_oid)
                {
                    shp = (TGIS_Shape)ll.Items[i];
                    return null;
                }

            }
            if (!string.IsNullOrEmpty(wkt))
            {
                AddFields(ll);
                if (centroidOnly)
                {
                    if ((wkt.StartsWith("POLYGON")) || (wkt.StartsWith("MULTIPOLYGON")) || (wkt.StartsWith("LINESTRING")))
                    {
                        TGIS_ShapeComplex shp1 = new TGIS_ShapeComplex();
                        shp1.ImportFromWKT(wkt);
                        wkt = String.Format("POINT ({0} {1})", shp1.Centroid().X.ToString().Replace(",", "."), shp1.Centroid().Y.ToString().Replace(",", "."));
                    }
                }
                TGIS_Shape shp2 = TGIS_Utils.GisCreateShapeFromWKT(wkt);
                if (SourceEpsg > 0)
                    if (ll.CS.EPSG > 0)
                        if (SourceEpsg != ll.CS.EPSG)
                        {
                            int TargetEpsg = ll.CS.EPSG;
                            shp2 = ConvertEPSG(SourceEpsg, shp2, TargetEpsg);
                        }
                // ll.AddShape(shp2);
                if ((wkt.StartsWith("POINT")) || (wkt.StartsWith("MULTIPOINT")))
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                if ((wkt.StartsWith("POLYGON")) || (wkt.StartsWith("MULTIPOLYGON")))
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePolygon);
                if (wkt.StartsWith("LINESTRING"))
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypeArc);
                if (shp2 == null) return null;
                wkt = shp2.ExportToWKT();
                if (shp == null) return null;
                shp.ImportFromWKT(wkt);
                shp.SetField("ID", ID);
                shp.SetField("ECID", Oid.ToString());
                shp.SetField("Label", label);
                shp.SetField("OwnGeometry", ownGeometry);
                shp.SetField("Edited", false);
                shp.SetField("ObjectType", objectType);
                //ll.SaveAll();
                //              ll.SaveData();
                // ll.SaveAll();
            }
           


            return shp;
        }

        public static TGIS_ParamsSectionVector GISRandomizeParams()
        {
            Random rand = new Random();
            TGIS_ParamsSectionVector pms = new TGIS_ParamsSectionVector();
            pms.Marker.Pattern = TGIS_BrushStyle.gisBsSolid;
            pms.Marker.OutlineStyle = TGIS_PenStyle.gisPsSolid;
            pms.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleCircle;
            pms.Marker.OutlineWidth = 1;
            pms.Marker.Size = 100;
            pms.Marker.OutlineColor = Color.Black;
            pms.Marker.Color = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
            pms.Area.OutlineStyle = TGIS_PenStyle.gisPsSolid;
            pms.Area.OutlineWidth = 1;
            pms.Area.OutlineColor = Color.Black;
            pms.Area.Color = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
            pms.Line.Pattern = TGIS_BrushStyle.gisBsSolid;
            pms.Line.OutlineStyle = TGIS_PenStyle.gisPsSolid;
            pms.Line.Style = TGIS_PenStyle.gisPsSolid;
            pms.Line.OutlineWidth = 1;
            pms.Line.Width = 1;
            pms.Line.OutlineColor = Color.Black;
            pms.Line.Color = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
            return pms;
        }
        /*    public static Collection<TGIS_LayerAbstract> LoadLayers(MapProject map)
          {
              Collection<TGIS_LayerAbstract> lls = new Collection<TGIS_LayerAbstract>();
              foreach (MapLayer mll in map.Layers)
              {
                  TGIS_LayerVector ll = new TGIS_LayerVector();
                  lls.Add(ll);
                  GISHelper.MapLayer(mll, ll);
                  ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsClear;
                  ll.Params.Area.OutlineColor = Color.Silver;
              }
              return lls;
          }*/
        /*   public static Collection<TGIS_LayerAbstract> MakeMap(Group Project, IWKT obj)
           {
               Collection<TGIS_LayerAbstract> lls = null;
               if (Project != null)
               {
                   if (Project.Map != null)
                   {
                       lls = LoadLayers(Project.Map);
                   }
               }
               if (lls == null) lls = new Collection<TGIS_LayerAbstract>();
               if (!String.IsNullOrEmpty(obj.WKT))
               {
                   TGIS_LayerVector ll = null;
                   ll = new TGIS_LayerVector();
                   ll.Name = obj.ToString();
                   ll.AddField("Oid", TGIS_FieldType.gisFieldTypeString, 30, 0);

                   TGIS_Shape shp = TGIS_Utils.GisCreateShapeFromWKT(obj.WKT);
                   if (shp != null)
                   {
                       ll.AddShape(shp);
                   }
                   lls.Add(ll);
               }
               return lls;
           }*/


        public static TGIS_LayerVector MapCurrentIWKT(TGIS_LayerVector ll, IWKT item, bool centroidOnly, int groupEpsg)
        {
        
            if (ll.FindFirst(ll.Extent, "ID=" + item.Oid.ToString()) == null)
                if (!String.IsNullOrEmpty(item.WKT))
                {
                    if (ll.CS.EPSG < 1) ll.SetCSByEPSG(item.EPSG);
                    int itemEPSG = 0;
                    if (item.EPSG > 0)
                        itemEPSG = item.EPSG;
                    if (groupEpsg > 0)
                        itemEPSG = groupEpsg;
                    AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.ToString(), itemEPSG, centroidOnly, true, GISHelper.GetLayerObjectType(item.GetType()));
                }
                else
                    if (!((item.Centroid_X == 0) && (item.Centroid_Y == 0)))
                {
                    AddShapeFromWKT(String.Format("POINT({0} {1})", item.Centroid_X.ToString().Replace(",", "."), item.Centroid_Y.ToString().Replace(",", ".")), ll, item.Oid.ToString(), item.Oid, item.Name, item.EPSG, false, false, GISHelper.GetLayerObjectType(item.GetType()));
                }

            return ll;
        }


        /*    public static void DoMapLayerVector(TGIS_LayerVector ll,MapLayerVector mll,string name)
            {
                foreach (Locality item in mll.Localities)
                {
                    MapLocality(ll, item,false);
                }

            }
            public static TGIS_LayerAbstract DoMapLayerFileItem(MapLayerFileItem mll,string name)
            {
                string path = mll.FileItem.FileData.GetFileName();
                TGIS_LayerAbstract ll = TGIS_Utils.GisCreateLayer(name, path);
                return ll;
                // MapUnits(((WorkItem)(mll.WorkItem)).Units, ll);

            }*/

        /*     public static void MapUnits(XPCollection<Unit> units, TGIS_LayerVector ll, TGIS_LayerVector llSummary, MapSummaryType summaryType)
             {
                 int maxcount = 0;
                 Collection<String> coll = new Collection<string>();
                 foreach (Unit item in units)
                 {
                     maxcount = PlotUnit(ll, llSummary, summaryType, maxcount, coll, item);
                 }
                 ApplyParams(llSummary, summaryType, maxcount);
             }*/
        /*      public static void MapUnits2(XPCollection units, TGIS_LayerVector ll, TGIS_LayerVector llSummary, MapSummaryType summaryType)
              {
                  int maxcount = 0;
                  Collection<String> coll = new Collection<string>();
                  foreach (Unit item in units)
                  {
                      maxcount = PlotUnit(ll, llSummary, summaryType, maxcount, coll, item);
                  }
                  ApplyParams(llSummary, summaryType, maxcount);
              }*/

        public static void ApplyParams(TGIS_LayerVector llSummary, int maxcount)
        {
            if (llSummary == null) return;
            llSummary.Params.Area.Color = TGIS_Utils.GIS_RENDER_COLOR();
            //llSummary.Params.Render.Expression = summaryType.ToString();
            llSummary.Params.Render.StartColor = Color.Silver;
            llSummary.Params.Render.ColorDefault = Color.Transparent;
            llSummary.Params.Render.MinVal = 1;
            llSummary.Params.Render.Zones = 5;
            llSummary.Params.Render.MaxVal = maxcount;
            llSummary.Params.Render.EndColor = Color.Black;
        }

        /*   public static void MapUnits1(XPCollection<Unit> units, TGIS_LayerVector ll, TGIS_LayerVector llSummary, MapSummaryType summaryType)
           {
               int maxcount = 0;
               Collection<String> coll = new Collection<string>();
               foreach (Unit item in units)
               {
                   maxcount = PlotUnit(ll, llSummary, summaryType, maxcount, coll, item);
               }
               ApplyParams(llSummary, summaryType, maxcount);
           }*/

        public static string GetLayerObjectType(Type t)
        {
            if (t == typeof(Store))
                return "Store";
            if (t == typeof(Unit))
                return "Unit";
            if (t == typeof(Locality))
                return "Locality";
            if (t == typeof(Project))
                return "Project";
            if (t == typeof(LocalityVisit))
                return "LocalityVisit";
            return "";
        }
        public static string GetLayerName(Type t)
        {
            if (t == typeof(Store))
                return "Stores";
            if (t == typeof(Unit))
                return "Units";
            if (t == typeof(Locality))
                return "Localities";
            if (t == typeof(Project))
                return "Projects";
            if (t == typeof(LocalityVisit))
                return "Locality visits";
            return "";
        }

        public static void PlotIWKT(TGIS_LayerVector ll, TGIS_LayerVector llSummary, IWKT item, Project Project, bool centroidOnly)
        {
            string wkt = string.Empty;
            bool ownGeometry = true;
            if (!string.IsNullOrEmpty(item.WKT))
                wkt = item.WKT;
            else
                return;
            /*if (item is Unit)
                if (((Unit)item).Locality != null)
                    if (!string.IsNullOrEmpty(((Unit)item).Locality.WKT))
                    {
                        ownGeometry = false;
                        wkt = ((Unit)item).Locality.WKT;
                    } */
            if (string.IsNullOrEmpty(wkt))
            {
                return;
            }
            if (string.IsNullOrEmpty(wkt)) return;
            if (wkt == string.Empty) return;
            int itemEPSG = item.EPSG;
            Project gr = null;
            Dataset ds = null;
            TGIS_Shape shp1 = GISHelper.AddShapeFromWKT(wkt, ll, item.Oid.ToString(), item.Oid, item.ToString(), itemEPSG, centroidOnly, ownGeometry,GISHelper.GetLayerObjectType(item.GetType()));
            if (shp1 == null) return;
            //  shp1.SetField("ECID", item.Oid);
            if (item is Locality)
            {
                //  shp1.SetField("ECID", ((Locality)item).Oid);
                shp1.SetField("Locality", ((Locality)item).Name);
                shp1.SetField("ObjectType", "Locality");
                gr = ((Locality)item).Project;
            }
            if (item is Unit)
            {
                if (((Unit)item).Locality != null)
                    shp1.SetField("Locality", ((Unit)item).Locality.Name);
                if (((Unit)item).TaxonomicName != null)
                    shp1.SetField("TaxonomicName", ((Unit)item).TaxonomicName.FullName);
                shp1.SetField("Date", ((Unit)item).Date);
                shp1.SetField("UnitID", ((Unit)item).UnitID);
                //  shp1.SetField("ECID", ((Unit)item).Oid);
                shp1.SetField("ObjectType", "Unit");
                if (((Unit)item).Dataset != null)
                {
                    ds = ((Unit)item).Dataset;
                    gr = ((Unit)item).Dataset.Project;
                }
            }
            if (gr != null) if (gr.EPSG > 0) itemEPSG = gr.EPSG;
            if (item.EPSG > 0) itemEPSG = item.EPSG;
            //AddShapeTags(item,shp1, ll);
            if (gr != null)
                shp1.SetField("Project", gr.Name);
            if (ds != null)
                shp1.SetField("Dataset", ds.Name);


            /*   if (llSummary != null)
               {
                   if (llSummary.FindField("SpCount") == -1)
                       llSummary.AddField("SpCount", TGIS_FieldType.gisFieldTypeNumber, 10, 0);
                   if (llSummary.FindField("LocCount") == -1)
                       llSummary.AddField("LocCount", TGIS_FieldType.gisFieldTypeNumber, 10, 0);
                   if (llSummary.FindField("OccCount") == -1)
                       llSummary.AddField("OccCount", TGIS_FieldType.gisFieldTypeNumber, 10, 0);
                   TGIS_Shape shp = llSummary.FindFirst(llSummary.Extent);
                   while (shp != null)
                   {
                       if (shp.Intersect(shp1))
                       {
                           if (typeof(Unit).IsAssignableFrom(item.GetType()))
                           {
                               if (((Unit)item).TaxonomicName != null)
                               {
                                   string id = String.Format("{0}/{1}", shp.Uid, ((Unit)item).TaxonomicName.Oid);
                                   string field_name = "SpCount";
                                   UpdateSummaryCount(coll, shp, id, field_name);
                                   string fname = ((Unit)item).TaxonomicName.ToString();
                                   if (llSummary.FindField(fname) == -1)
                                       llSummary.AddField(fname, TGIS_FieldType.gisFieldTypeNumber, 10, 0);
                                   SetFieldInfo(shp, fname);
                               }
                               if (((Unit)item).Locality != null)
                               {
                                   string id = String.Format("{0}/{1}/{2}/{3}", shp.Uid, ((Unit)item).Locality.Oid, ((Unit)item).Date.ToString(), ((Unit)item).Year.ToString());
                                   string field_name = "LocCount";
                                   UpdateSummaryCount(coll, shp, id, field_name);
                               }
                               SetFieldInfo(shp, "OccCount");
                           }

                       }
                       shp = llSummary.FindNext();
                   }
               }*/
        }

        private static void SetFieldInfo(TGIS_Shape shp, string field_name)
        {
            if (Convert.ToInt16((shp.GetField(field_name))) > 0)
            {
                int count = Convert.ToInt16(shp.GetField(field_name)) + 1;
                shp.SetField(field_name, count);
            }
            else
            {
                shp.SetField(field_name, 1);
            }
        }


        private static void UpdateSummaryCount(Collection<String> coll, TGIS_Shape shp, string id, string field_name)
        {
            if (coll.IndexOf(id) == -1)
            {
                SetFieldInfo(shp, field_name);
                coll.Add(id);
            }
        }



        public static TGIS_Point GetWKTCentroid(string WKT)
        {
            TGIS_Shape shp = TGIS_Utils.GisCreateShapeFromWKT(WKT);
            if (shp == null) return new TGIS_Point();
            return shp.Centroid();
        }

        public static void CreateCentroidXY(IWKT iWKT)
        {
            if (!String.IsNullOrEmpty(iWKT.WKT))
            {
                TGIS_Point ptg = GISHelper.GetWKTCentroid(iWKT.WKT);
                iWKT.Centroid_X = Convert.ToDecimal(ptg.X);
                iWKT.Centroid_Y = Convert.ToDecimal(ptg.Y);

            }
        }



        public static void UpdateAreaAndLengthFromWKT(IWKT iwkt)
        {
            if (iwkt == null) return;

            if (string.IsNullOrEmpty(iwkt.WKT))
            {
                if ((iwkt.Latitude != 0) && (iwkt.Longitude != 0))
                {
                    iwkt.WKT = String.Format("POINT({0} {1})", iwkt.Longitude, iwkt.Latitude);
                    iwkt.Centroid_X = Convert.ToDecimal(iwkt.Longitude);
                    iwkt.Centroid_Y = Convert.ToDecimal(iwkt.Latitude);
                    iwkt.EPSG = 4326;
                }
            }
            else
            {

                if ((iwkt.WKT.StartsWith("POLYGON")) || (iwkt.WKT.StartsWith("MULTIPOLYGON")))
                {
                    TGIS_ShapePolygon shp = new TGIS_ShapePolygon();
                    {
                        shp.ImportFromWKT(iwkt.WKT);
                        if (shp != null)
                        {
                            iwkt.Area = shp.Area();
                            iwkt.Length = shp.Length();
                            iwkt.Centroid_X = Convert.ToDecimal(shp.Centroid().X);
                            iwkt.Centroid_Y = Convert.ToDecimal(shp.Centroid().Y);
                        }
                    }
                }
                if ((iwkt.WKT.StartsWith("POINT")))
                {
                    TGIS_ShapePoint shp = new TGIS_ShapePoint();
                    {
                        shp.ImportFromWKT(iwkt.WKT);
                        if (shp != null)
                        {
                            iwkt.Centroid_X = Convert.ToDecimal(shp.Centroid().X);
                            iwkt.Centroid_Y = Convert.ToDecimal(shp.Centroid().Y);
                        }
                    }
                }
                if ((iwkt.WKT.StartsWith("LINE")))
                {
                    TGIS_ShapeArc shp = new TGIS_ShapeArc();
                    {
                        shp.ImportFromWKT(iwkt.WKT);
                        if (shp != null)
                        {
                            iwkt.Area = 0;
                            iwkt.Length = shp.Length();
                            iwkt.Centroid_X = Convert.ToDecimal(shp.Centroid().X);
                            iwkt.Centroid_Y = Convert.ToDecimal(shp.Centroid().Y);
                        }
                    }
                }
            }

        }

        public static void CreateLatLong(IWKT iwkt)
        {
            TGIS_Shape shp = null;
            if ((iwkt == null) || (string.IsNullOrEmpty(iwkt.WKT))) return;
            if ((iwkt.WKT.StartsWith("POLYGON")) || (iwkt.WKT.StartsWith("MULTIPOLYGON")))
                shp = new TGIS_ShapePolygon();
            if ((iwkt.WKT.StartsWith("POINT")))
                shp = new TGIS_ShapePoint();
            if ((iwkt.WKT.StartsWith("LINE")))
                shp = new TGIS_ShapeArc();
            if (shp == null) return;

            shp.ImportFromWKT(iwkt.WKT);
            if (shp == null) return;
            if (!(iwkt.EPSG > 0)) iwkt.EPSG = 4326;
            TGIS_CSCoordinateSystem cs_in = TGIS_CSFactory.ByEPSG(iwkt.EPSG);
            TGIS_CSCoordinateSystem cs_out = TGIS_CSFactory.ByEPSG(4326);

            TGIS_Point pt_out = cs_out.FromCS(cs_in, shp.Centroid());
            iwkt.Latitude = Convert.ToDouble(pt_out.Y);
            iwkt.Longitude = Convert.ToDouble(pt_out.X);


        }
    }
}
