using System;
using System.Collections.Generic;

using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Configuration;


namespace EarthCape.Module.Core
{
    public class Emailer
    {


        public static string EmailTemplateNewRegistrationToAdmin = @"
New user registration:


Organization: ##User.Org##

User name: ##User.UserName##

First name: ##User.FirstName##

Last name: ##User.LastName##

Email: ##User.Email##


Please go to ##SiteUrl## to assign additional roles to user.

##SiteName##

##UserRegistrationMailToAdmin##
";
        
        public static string EmailTemplateNewRegistrationToUser = @"
##User.UserName##, thank you for creating EarthCape account to login to ##SiteName##.

Your current access level will depend on the default settings of ##SiteName## database.

##SiteUrl##

##SiteName## administrator

##UserRegistrationMailToAdmin##
";

        public static string EmailTemplateUserRoleAdded = @"
##User.FirstName##, you have new roles assigned to you:

##User.Role##

##SiteUrl##

##SiteName## administrator

##UserRegistrationMailToAdmin##
";

        public static string PasswordRestoreEmailTemplate = @"
##User.UserName##, your password has been reset.
New temporary password is:

##User.Password##

##SiteUrl##

##SiteName## administrator
";


        public static void SendRoleChanges(User user, List<Role> addedRolesList)
        {
            if (addedRolesList != null)
            {
                if (addedRolesList.Count > 0)
                {
                    Emailer.SendRoleListAssignedToUser(user, addedRolesList, Emailer.EmailTemplateUserRoleAdded);
                    addedRolesList.Clear();
                }
            }
        }
        
        public static SmtpClient GetSmtp()
        {
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["AdminMailServer"], Convert.ToInt32(ConfigurationManager.AppSettings["AdminMailServerPort"]));
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["AdminMailServerUser"], ConfigurationManager.AppSettings["AdminMailServerPassword"]);
            smtpClient.Credentials = credentials;
            return smtpClient;
        }
       
        public static void SendRegistrationToAdmin(string firstName, string lastName, string email, string userName,string org, SmtpClient smtpClient, string EmailTemplateText)
        {
            string siteUrl = "";
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]))
                return;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteUrl"]))
                siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string SiteName = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]))
                SiteName = ConfigurationManager.AppSettings["SiteName"];


            MailMessage mailMsg1 = new MailMessage();
            mailMsg1.From = new MailAddress(email, firstName + " " + lastName);
            mailMsg1.To.Add(new MailAddress(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"], SiteName));
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserRegistrationMailToAdminCC"]))
                mailMsg1.CC.Add(new MailAddress(ConfigurationManager.AppSettings["UserRegistrationMailToAdminCC"], SiteName + " cc"));
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserRegistrationMailToAdminBCC"]))
                mailMsg1.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["UserRegistrationMailToAdminBCC"], SiteName + " bcc"));
            mailMsg1.Subject = "new EarthCape registration";
            EmailTemplateText = EmailTemplateText
                .Replace("##User.Org##", org)
                .Replace("##User.Email##", email)
                .Replace("##User.FirstName##", firstName)
                .Replace("##User.LastName##", lastName)
                .Replace("##User.UserName##", userName)
                .Replace("##SiteUrl##", siteUrl)
                .Replace("##SiteName##", SiteName)
                .Replace("##UserRegistrationMailToAdmin##", ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]);
            string text1 = EmailTemplateText;
            string html1 = EmailTemplateText;
            mailMsg1.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text1, null, MediaTypeNames.Text.Plain));
            //mailMsg1.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html1, null, MediaTypeNames.Text.Html));


            smtpClient.Send(mailMsg1);
        }

        public static void SendRegistrationToUser(string firstName, string lastName, string email, string userName,string org, SmtpClient smtpClient, string EmailTemplateText)
        {
            string siteUrl = "";
            string SiteAdminEmail = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteUrl"]))
                siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string SiteName = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]))
                SiteName = ConfigurationManager.AppSettings["SiteName"];
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]))
                SiteAdminEmail = ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"];
            else
                return;

            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(email, firstName + " " + lastName));
            mailMsg.From = new MailAddress(SiteAdminEmail, string.Format("{0} ({1})",SiteName,SiteAdminEmail));
            mailMsg.Subject = SiteName +" account registration";
            EmailTemplateText = EmailTemplateText
                  .Replace("##User.Org##", org)
                  .Replace("##User.Email##", email)
                  .Replace("##User.FirstName##", firstName)
                  .Replace("##User.LastName##", lastName)
                  .Replace("##User.UserName##", userName)
                  .Replace("##SiteUrl##", siteUrl)
                  .Replace("##SiteName##", SiteName)
                  .Replace("##UserRegistrationMailToAdmin##", ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]);
            string text1 = EmailTemplateText;
            string html1 = EmailTemplateText;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text1, null, MediaTypeNames.Text.Plain));
            //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html1, null, MediaTypeNames.Text.Html));

            smtpClient.Send(mailMsg);
        }
        public static void SendRestorePassword(string email, string userName, string userPassword, string org, SmtpClient smtpClient, string EmailTemplateText)
        {
            string siteUrl = "";
            string SiteAdminEmail = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteUrl"]))
                siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string SiteName = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]))
                SiteName = ConfigurationManager.AppSettings["SiteName"];
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]))
                SiteAdminEmail = ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"];
            else
                return;

            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(email, userName));
            mailMsg.From = new MailAddress(SiteAdminEmail, string.Format("{0} ({1})", SiteName, SiteAdminEmail));
            mailMsg.Subject = SiteName + " password reset";
            EmailTemplateText = EmailTemplateText
                    .Replace("##User.UserName##", userName)
                .Replace("##User.Password##", userPassword)
                  .Replace("##SiteUrl##", siteUrl)
                  .Replace("##SiteName##", SiteName)
                  .Replace("##UserRegistrationMailToAdmin##", ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"]);
            string text1 = EmailTemplateText;
            string html1 = EmailTemplateText;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text1, null, MediaTypeNames.Text.Plain));
            //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html1, null, MediaTypeNames.Text.Html));

            smtpClient.Send(mailMsg);
        }
   
        public static void SendRoleAssignedToUser(User user, Role role, string EmailTemplateText)
        {
            if (user.Email == null) return;
            string siteUrl = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteUrl"]))
                siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string SiteName = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]))
                SiteName = ConfigurationManager.AppSettings["SiteName"];


            SmtpClient smtpClient = GetSmtp();

            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(user.Email, user.FirstName + " " + user.LastName));
            mailMsg.From = new MailAddress(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"], ConfigurationManager.AppSettings["SiteName"]);
            mailMsg.Subject = string.Format("Role assigned ({0})", ConfigurationManager.AppSettings["SiteName"]);
            EmailTemplateText = EmailTemplateText
                   .Replace("##User.Email##", user.Email)
                   .Replace("##User.FirstName##", user.FirstName)
                   .Replace("##User.LastName##", user.LastName)
                   .Replace("##User.UserName##", user.UserName)
                   .Replace("##SiteUrl##", siteUrl)
                   .Replace("##SiteName##", SiteName)
                   .Replace("##UserRegistrationMailToAdmin##", ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"])
                   .Replace("##User.Role##", role.Name);
            string text1 = EmailTemplateText;
            string html1 = EmailTemplateText;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text1, null, MediaTypeNames.Text.Plain));
            //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html1, null, MediaTypeNames.Text.Html));

            smtpClient.Send(mailMsg);
        }

        public static void SendRoleListAssignedToUser(User user, List<Role> list, string EmailTemplateText)
        {
            string rolelist="";
            foreach (Role role in list)
            {
                if (string.IsNullOrEmpty(rolelist))
                    rolelist = role.Name;
                else
                    rolelist = rolelist + ", " + role.Name;
            }
            if (user.Email == null) return;
            string siteUrl = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteUrl"]))
                siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            string SiteName = "";
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteName"]))
                SiteName = ConfigurationManager.AppSettings["SiteName"];


            SmtpClient smtpClient = GetSmtp();

            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(user.Email, user.FirstName + " " + user.LastName));
            mailMsg.From = new MailAddress(ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"], ConfigurationManager.AppSettings["SiteName"]);
            mailMsg.Subject = string.Format("[{0}] changes in security settings", ConfigurationManager.AppSettings["SiteName"]);
            EmailTemplateText = EmailTemplateText
                   .Replace("##User.Email##", user.Email)
                   .Replace("##User.FirstName##", user.FirstName)
                   .Replace("##User.LastName##", user.LastName)
                   .Replace("##User.UserName##", user.UserName)
                   .Replace("##SiteUrl##", siteUrl)
                   .Replace("##SiteName##", SiteName)
                   .Replace("##UserRegistrationMailToAdmin##", ConfigurationManager.AppSettings["UserRegistrationMailToAdmin"])
                   .Replace("##User.Role##", rolelist);
            string text1 = EmailTemplateText;
            string html1 = EmailTemplateText;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text1, null, MediaTypeNames.Text.Plain));
            //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html1, null, MediaTypeNames.Text.Html));

            smtpClient.Send(mailMsg);
        }


    }
}
