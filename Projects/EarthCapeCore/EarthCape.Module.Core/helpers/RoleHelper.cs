using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Actions;

using DevExpress.Persistent.Base.General;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Security;
using System.Reflection;
using Xpand.ExpressApp.Security.Permissions;
using Xpand.ExpressApp.Security.Core;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;

namespace EarthCape.Module.Core
{
    public class RoleHelper
    {

        public static Role GetAdministratorRole(IObjectSpace os)
        {
            Role administratorRole = os.FindObject<Role>(new BinaryOperator("Name", "Administrator"));
            if (administratorRole == null)
            {
                administratorRole = os.CreateObject<Role>();
                administratorRole.Name = "Administrator";
              //  administratorRole.RoleType = RoleType.Admin;
                administratorRole.IsAdministrative = true;
            }
            return administratorRole;
        }


        public static Role CreateDefaultRole(IObjectSpace os, Session session)
        {
            Role defaultRole = (os != null) ? os.FindObject<Role>(new BinaryOperator("Name", "Default")) : session.FindObject<Role>(new BinaryOperator("Name", "Default"));
            if (defaultRole == null)
            {
                defaultRole = (os != null) ? os.CreateObject<Role>() : new Role(session);
                defaultRole.Name = "Default";

                //todo
                // AnonymousLoginPermission permission1 = new AnonymousLoginPermission(Modifier.Allow);
                //defaultRole
                CreateDefaultPermissions(os, session, defaultRole);

                defaultRole.Save();
                if (os != null)
                    os.CommitChanges();
            }
            //project
          
            return defaultRole;
        }
        public static Role CreateNewUserRole(IObjectSpace os, Session session)
        {
            Role defaultRole = (os != null) ? os.FindObject<Role>(new BinaryOperator("Name", "new user")) : session.FindObject<Role>(new BinaryOperator("Name", "new user"));
            if (defaultRole == null)
            {
                defaultRole = (os != null) ? os.CreateObject<Role>() : new Role(session);
                defaultRole.Name = "new user";

                //todo
                // AnonymousLoginPermission permission1 = new AnonymousLoginPermission(Modifier.Allow);
                //defaultRole
                CreateDefaultPermissions(os, session, defaultRole);

                defaultRole.Save();
                if (os != null)
                    os.CommitChanges();
            }
            //project
         
            return defaultRole;
        }

        private static void CreateDefaultPermissions(IObjectSpace os, Session session, Role defaultRole)
        {
            CriteriaOperator cr = new BinaryOperator("TargetType", typeof(Project));
            SecuritySystemTypePermissionObject permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(Project);
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject ProjectOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                ProjectOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                ProjectOwnPermission.AllowRead = true;
                ProjectOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(ProjectOwnPermission);

                SecuritySystemObjectPermissionsObject ProjectPublicPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                ProjectPublicPermission.Criteria = "[Public] = True";
                ProjectPublicPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(ProjectPublicPermission);
            }

            //user

            cr = new BinaryOperator("TargetType", typeof(User));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(User);
                permObject.AllowNavigate = true;
                permObject.AllowRead = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject UserOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                UserOwnPermission.Criteria = "[UserName] = CurrentUserId()";
                UserOwnPermission.AllowRead = true;
                UserOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(UserOwnPermission);

                SecuritySystemObjectPermissionsObject myDetailsPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                myDetailsPermission.Criteria = "[Oid] = CurrentUserId()";
                myDetailsPermission.AllowNavigate = false;
                myDetailsPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(myDetailsPermission);


                SecuritySystemMemberPermissionsObject ownPasswordPermission = (os != null) ? os.CreateObject<SecuritySystemMemberPermissionsObject>() : new SecuritySystemMemberPermissionsObject(session);
                ownPasswordPermission.Members = "ChangePasswordOnFirstLogon; StoredPassword";
                ownPasswordPermission.AllowWrite = true;
                permObject.MemberPermissions.Add(ownPasswordPermission);

            }
            //dataset
            cr = new BinaryOperator("TargetType", typeof(Dataset));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(Dataset);
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject DatasetPublicPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                DatasetPublicPermission.Criteria = "[Project.Public] = True";
                DatasetPublicPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(DatasetPublicPermission);

                SecuritySystemObjectPermissionsObject DatasetOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                DatasetOwnPermission.Criteria = "[Project.CreatedBy.Oid] = CurrentUserId()";
                DatasetOwnPermission.AllowRead = true;
                DatasetOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(DatasetOwnPermission);
            }
            //xpand
            cr = new BinaryOperator("TargetType", typeof(ModelDifferenceObject));
            SecuritySystemTypePermissionObject permission = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permission == null)
            {
                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(ModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(AspectObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(PersistentApplication);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(RoleModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(UserModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);


            }


            //CustomField
            cr = new BinaryOperator("TargetType", typeof(CustomField));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(CustomField);
                // securityCustomFieldPermissions.AllowRead = true;
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject CustomFieldProjectPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                CustomFieldProjectPermission.Criteria = "[Project.CreatedBy.Oid] = CurrentUserId()";
                CustomFieldProjectPermission.AllowRead = true;
                CustomFieldProjectPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(CustomFieldProjectPermission);

                SecuritySystemObjectPermissionsObject CustomFieldProjectPublicPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                CustomFieldProjectPublicPermission.Criteria = "[Project.Public] = True";
                CustomFieldProjectPublicPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(CustomFieldProjectPublicPermission);

                SecuritySystemObjectPermissionsObject CustomFieldOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                CustomFieldOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                CustomFieldOwnPermission.AllowRead = true;
                CustomFieldOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(CustomFieldOwnPermission);
            }
            //NoteSet
            cr = new BinaryOperator("TargetType", typeof(NoteSet));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(NoteSet);
                permObject.AllowRead = true;
                permObject.AllowWrite = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
            }
            //Note
            cr = new BinaryOperator("TargetType", typeof(Note));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(Note);
                permObject.AllowRead = true;
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
                SecuritySystemObjectPermissionsObject NoteOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                NoteOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                NoteOwnPermission.AllowRead = true;
                NoteOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(NoteOwnPermission);


            }

            //FileSet
            cr = new BinaryOperator("TargetType", typeof(FileSet));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(FileSet);
                permObject.AllowRead = true;
                permObject.AllowWrite = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
            }
            //FileItem
            cr = new BinaryOperator("TargetType", typeof(FileItem));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(FileItem);
                // securityFileItemPermissions.AllowRead = true;
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject FileItemOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileItemOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileItemOwnPermission.AllowRead = true;
                FileItemOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(FileItemOwnPermission);

                //FileDataEmbedded
                SecuritySystemTypePermissionObject securityFileDataEmbeddedPermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                securityFileDataEmbeddedPermissions.TargetType = typeof(FileDataEmbedded);
                // securityFileDataEmbeddedPermissions.AllowRead = true;
                securityFileDataEmbeddedPermissions.AllowNavigate = true;
                securityFileDataEmbeddedPermissions.AllowCreate = true;
                defaultRole.TypePermissions.Add(securityFileDataEmbeddedPermissions);

                SecuritySystemObjectPermissionsObject FileDataEmbeddedOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileDataEmbeddedOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileDataEmbeddedOwnPermission.AllowRead = true;
                FileDataEmbeddedOwnPermission.AllowWrite = true;
                securityFileDataEmbeddedPermissions.ObjectPermissions.Add(FileDataEmbeddedOwnPermission);

                SecuritySystemTypePermissionObject securityFileItemEmbeddedPermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                securityFileItemEmbeddedPermissions.TargetType = typeof(FileItemEmbedded);
                // securityFileItemEmbeddedPermissions.AllowRead = true;
                securityFileItemEmbeddedPermissions.AllowNavigate = true;
                securityFileItemEmbeddedPermissions.AllowCreate = true;
                defaultRole.TypePermissions.Add(securityFileItemEmbeddedPermissions);

                SecuritySystemObjectPermissionsObject FileItemEmbeddedOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileItemEmbeddedOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileItemEmbeddedOwnPermission.AllowRead = true;
                FileItemEmbeddedOwnPermission.AllowWrite = true;
                securityFileItemEmbeddedPermissions.ObjectPermissions.Add(FileItemEmbeddedOwnPermission);
            }




            //Role
            SecuritySystemTypePermissionObject securityRolePermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityRolePermissions.TargetType = typeof(Role);
            defaultRole.TypePermissions.Add(securityRolePermissions);
            SecuritySystemObjectPermissionsObject defaultRolePermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
            defaultRolePermission.Criteria = "[Project.CreatedBy.Oid] = CurrentUserId()";
            defaultRolePermission.AllowNavigate = true;
            defaultRolePermission.AllowRead = true;
            defaultRolePermission.AllowWrite = true;
            securityRolePermissions.ObjectPermissions.Add(defaultRolePermission);


            SecuritySystemTypePermissionObject securityTypePermissionObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityTypePermissionObject.TargetType = typeof(SecuritySystemTypePermissionObject);
            securityTypePermissionObject.AllowWrite = true;
            defaultRole.TypePermissions.Add(securityTypePermissionObject);

            SecuritySystemTypePermissionObject securityObjectPermissionObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityObjectPermissionObject.TargetType = typeof(SecuritySystemObjectPermissionsObject);
            securityObjectPermissionObject.AllowWrite = true;
            defaultRole.TypePermissions.Add(securityObjectPermissionObject);
        }
        public static Role CreateBasicRole(IObjectSpace os, Session session)
        {
            Role defaultRole = (os != null) ? os.FindObject<Role>(new BinaryOperator("Name", "Basic")) : session.FindObject<Role>(new BinaryOperator("Name", "Basic"));
            if (defaultRole == null)
            {
                defaultRole = (os != null) ? os.CreateObject<Role>() : new Role(session);
                defaultRole.Name = "Basic";

                //todo
                // AnonymousLoginPermission permission1 = new AnonymousLoginPermission(Modifier.Allow);
                //defaultRole
            }
            //project
            CriteriaOperator cr = new BinaryOperator("TargetType", typeof(Project));
            SecuritySystemTypePermissionObject permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(Project);
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject ProjectOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                ProjectOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                ProjectOwnPermission.AllowRead = true;
                ProjectOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(ProjectOwnPermission);

                SecuritySystemObjectPermissionsObject ProjectPublicPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                ProjectPublicPermission.Criteria = "[Public] = True";
                ProjectPublicPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(ProjectPublicPermission);
            }

            //user

            cr = new BinaryOperator("TargetType", typeof(User));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(User);
                permObject.AllowNavigate = true;
                permObject.AllowRead = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject UserOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                UserOwnPermission.Criteria = "[UserName] = CurrentUserId()";
                UserOwnPermission.AllowRead = true;
                UserOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(UserOwnPermission);

                SecuritySystemObjectPermissionsObject myDetailsPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                myDetailsPermission.Criteria = "[Oid] = CurrentUserId()";
                myDetailsPermission.AllowNavigate = false;
                myDetailsPermission.AllowRead = true;
                permObject.ObjectPermissions.Add(myDetailsPermission);


                SecuritySystemMemberPermissionsObject ownPasswordPermission = (os != null) ? os.CreateObject<SecuritySystemMemberPermissionsObject>() : new SecuritySystemMemberPermissionsObject(session);
                ownPasswordPermission.Members = "ChangePasswordOnFirstLogon; StoredPassword";
                ownPasswordPermission.AllowWrite = true;
                permObject.MemberPermissions.Add(ownPasswordPermission);

            }
          
            //xpand
            cr = new BinaryOperator("TargetType", typeof(ModelDifferenceObject));
            SecuritySystemTypePermissionObject permission = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permission == null)
            {
                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(ModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(AspectObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(PersistentApplication);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(RoleModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);

                permission = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permission.TargetType = typeof(UserModelDifferenceObject);
                permission.AllowNavigate = false;
                permission.AllowCreate = true;
                permission.AllowRead = true;
                permission.AllowWrite = true;
                defaultRole.TypePermissions.Add(permission);


            }


            //NoteSet
            cr = new BinaryOperator("TargetType", typeof(NoteSet));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(NoteSet);
                permObject.AllowRead = true;
                permObject.AllowWrite = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
            }
            //Note
            cr = new BinaryOperator("TargetType", typeof(Note));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(Note);
                permObject.AllowRead = true;
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
                SecuritySystemObjectPermissionsObject NoteOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                NoteOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                NoteOwnPermission.AllowRead = true;
                NoteOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(NoteOwnPermission);


            }

            //FileSet
            cr = new BinaryOperator("TargetType", typeof(FileSet));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(FileSet);
                permObject.AllowRead = true;
                permObject.AllowWrite = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);
            }
            //FileItem
            cr = new BinaryOperator("TargetType", typeof(FileItem));
            permObject = (os != null) ? os.FindObject<SecuritySystemTypePermissionObject>(cr) : session.FindObject<SecuritySystemTypePermissionObject>(cr);
            if (permObject == null)
            {
                permObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                permObject.TargetType = typeof(FileItem);
                // securityFileItemPermissions.AllowRead = true;
                permObject.AllowNavigate = true;
                permObject.AllowCreate = true;
                defaultRole.TypePermissions.Add(permObject);

                SecuritySystemObjectPermissionsObject FileItemOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileItemOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileItemOwnPermission.AllowRead = true;
                FileItemOwnPermission.AllowWrite = true;
                permObject.ObjectPermissions.Add(FileItemOwnPermission);

                //FileDataEmbedded
                SecuritySystemTypePermissionObject securityFileDataEmbeddedPermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                securityFileDataEmbeddedPermissions.TargetType = typeof(FileDataEmbedded);
                // securityFileDataEmbeddedPermissions.AllowRead = true;
                securityFileDataEmbeddedPermissions.AllowNavigate = true;
                securityFileDataEmbeddedPermissions.AllowCreate = true;
                defaultRole.TypePermissions.Add(securityFileDataEmbeddedPermissions);

                SecuritySystemObjectPermissionsObject FileDataEmbeddedOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileDataEmbeddedOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileDataEmbeddedOwnPermission.AllowRead = true;
                FileDataEmbeddedOwnPermission.AllowWrite = true;
                securityFileDataEmbeddedPermissions.ObjectPermissions.Add(FileDataEmbeddedOwnPermission);

                SecuritySystemTypePermissionObject securityFileItemEmbeddedPermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
                securityFileItemEmbeddedPermissions.TargetType = typeof(FileItemEmbedded);
                // securityFileItemEmbeddedPermissions.AllowRead = true;
                securityFileItemEmbeddedPermissions.AllowNavigate = true;
                securityFileItemEmbeddedPermissions.AllowCreate = true;
                defaultRole.TypePermissions.Add(securityFileItemEmbeddedPermissions);

                SecuritySystemObjectPermissionsObject FileItemEmbeddedOwnPermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
                FileItemEmbeddedOwnPermission.Criteria = "[CreatedBy.Oid] = CurrentUserId()";
                FileItemEmbeddedOwnPermission.AllowRead = true;
                FileItemEmbeddedOwnPermission.AllowWrite = true;
                securityFileItemEmbeddedPermissions.ObjectPermissions.Add(FileItemEmbeddedOwnPermission);
            }




            //Role
            SecuritySystemTypePermissionObject securityRolePermissions = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityRolePermissions.TargetType = typeof(Role);
            defaultRole.TypePermissions.Add(securityRolePermissions);
            SecuritySystemObjectPermissionsObject defaultRolePermission = (os != null) ? os.CreateObject<SecuritySystemObjectPermissionsObject>() : new SecuritySystemObjectPermissionsObject(session);
            defaultRolePermission.Criteria = "[Project.CreatedBy.Oid] = CurrentUserId()";
            defaultRolePermission.AllowNavigate = true;
            defaultRolePermission.AllowRead = true;
            defaultRolePermission.AllowWrite = true;
            securityRolePermissions.ObjectPermissions.Add(defaultRolePermission);


            SecuritySystemTypePermissionObject securityTypePermissionObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityTypePermissionObject.TargetType = typeof(SecuritySystemTypePermissionObject);
            securityTypePermissionObject.AllowWrite = true;
            defaultRole.TypePermissions.Add(securityTypePermissionObject);

            SecuritySystemTypePermissionObject securityObjectPermissionObject = (os != null) ? os.CreateObject<SecuritySystemTypePermissionObject>() : new SecuritySystemTypePermissionObject(session);
            securityObjectPermissionObject.TargetType = typeof(SecuritySystemObjectPermissionsObject);
            securityObjectPermissionObject.AllowWrite = true;
            defaultRole.TypePermissions.Add(securityObjectPermissionObject);

            defaultRole.Save();
            if (os != null)
                os.CommitChanges();

            return defaultRole;
        }
   
   /*     public static Role CreateProjectAdminRole(Session session, Project Project)
        {
            Role role = session.FindObject<Role>(CriteriaOperator.And(new BinaryOperator("Project.Oid", Project.Oid), new BinaryOperator("RoleType", RoleType.ProjectAdmin)));
            if (role == null)
            {
                role = new Role(session);
              //  role.Project = Project;
               // role.RoleType = RoleType.ProjectAdmin;
                ShortenProjectName(Project, role);


               
                SecuritySystemTypePermissionObject securityRolePermissions = new SecuritySystemTypePermissionObject(session);
                securityRolePermissions.TargetType = typeof(Role);
                securityRolePermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityRolePermissions);

                SecuritySystemObjectPermissionsObject defaultRolePermission =  new SecuritySystemObjectPermissionsObject(session);
                defaultRolePermission.Criteria = "[Project.Oid] = '" + Project.Oid.ToString() + "'";
                defaultRolePermission.AllowNavigate = true;
                defaultRolePermission.AllowRead = true;
                //defaultRolePermission.AllowWrite = false;
                securityRolePermissions.ObjectPermissions.Add(defaultRolePermission);                    
       

                SecuritySystemTypePermissionObject securityDatasetPermissions = new SecuritySystemTypePermissionObject(session);
                securityDatasetPermissions.TargetType = typeof(Dataset);
                securityDatasetPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityDatasetPermissions);

               
                SecuritySystemTypePermissionObject securityProjectPermissions = new SecuritySystemTypePermissionObject(session);
                securityProjectPermissions.TargetType = typeof(Project);
                securityProjectPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityProjectPermissions);

                SecuritySystemObjectPermissionsObject ProjectPermission = new SecuritySystemObjectPermissionsObject(session);
                ProjectPermission.Criteria = "[Oid] = '" + Project.Oid.ToString() + "'"; ;
                ProjectPermission.AllowNavigate = true;
                ProjectPermission.AllowRead = true;
                ProjectPermission.AllowWrite = true;
                securityProjectPermissions.ObjectPermissions.Add(ProjectPermission);

                //AddData(session,Project);
                 role.Save();
              if (Value_WindowController.Instance() != null)
                foreach (ModuleBase mod in Value_WindowController.Instance().Application.Modules)
                {
                    if (mod.Name.Contains("EarthCape"))
                        foreach (Type type in mod.GetExportedTypes())
                        {
                            if ((typeof(IDatasetObject).IsAssignableFrom(type)))
                                AddAdminObjectPermissions(session, type, role, "[Dataset.Project.Oid] = '" + Project.Oid.ToString() + "'");
                            if ((typeof(IProjectObject).IsAssignableFrom(type)))
                                AddAdminObjectPermissions(session, type, role, "[Project.Oid] = '" + Project.Oid.ToString() + "'");
                        }
                }
}
            role.Save();
           
            return role;
        }
/*

        private static void ShortenProjectName(Project Project, Role role)
        {
            role.Name = (Project.Name.Length > 10) ? String.Format("[{0}..] ", Project.Name.Substring(0, 10)) : Project.Name + role.RoleType.ToString();
        }
        /*     public static OrgRole CreateOrganizationAdminRole(Session session, Organization Organization)
{
    OrgRole OrgRole = session.FindObject<OrgRole>(CriteriaOperator.And(new BinaryOperator("Organization.Oid", Organization.Oid), new BinaryOperator("RoleType", OrgRoleType.OrganizationAdmin)));
    if (OrgRole == null)
    {
        OrgRole = new OrgRole(session);
        OrgRole.Organization = Organization;
        OrgRole.RoleType = OrgRoleType.OrganizationAdmin;
        OrgRole.Name = Guid.NewGuid().ToString();


        if (Value_WindowController.Instance() != null)
            foreach (ModuleBase mod in Value_WindowController.Instance().Application.Modules)
        {
            if (mod.Name.Contains("EarthCape"))
                foreach (Type type in mod.GetExportedTypes())
                {
                      if ((typeof(IOrganizationObject).IsAssignableFrom(type)))
                        AddAdminObjectPermissions(session, type, OrgRole, "[Organization.Oid] = '" + Organization.Oid.ToString() + "'");
                }
        }

        SecuritySystemTypePermissionObject securityDatasetPermissions = new SecuritySystemTypePermissionObject(session);
        securityDatasetPermissions.TargetType = typeof(Dataset);
        securityDatasetPermissions.AllowNavigate = true;
        OrgRole.TypePermissions.Add(securityDatasetPermissions);


        SecuritySystemTypePermissionObject securityOrganizationPermissions = new SecuritySystemTypePermissionObject(session);
        securityOrganizationPermissions.TargetType = typeof(Organization);
        securityOrganizationPermissions.AllowNavigate = true;
        OrgRole.TypePermissions.Add(securityOrganizationPermissions);

        SecuritySystemObjectPermissionsObject OrganizationPermission = new SecuritySystemObjectPermissionsObject(session);
        OrganizationPermission.Criteria = "[Oid] = '" + Organization.Oid.ToString() + "'"; ;
        OrganizationPermission.AllowNavigate = true;
        OrganizationPermission.AllowRead = true;
        OrganizationPermission.AllowWrite = true;
        securityOrganizationPermissions.ObjectPermissions.Add(OrganizationPermission);

        //AddData(session,Organization);
        OrgRole.Organization = Organization;
        OrgRole.RoleType = OrgRoleType.OrganizationAdmin;
        OrgRole.Save();
    }
    return OrgRole;
}*/
        
     /*   public static Role CreateProjectMemberRole(Session session, Project Project)
        {
            Role role = session.FindObject<Role>(CriteriaOperator.And(new BinaryOperator("Project.Oid", Project.Oid), new BinaryOperator("RoleType", RoleType.ProjectAdmin)));
            if (role == null)
            {
                role = new Role(session);
                role.Project = Project;
                role.RoleType = RoleType.ProjectMember;
                ShortenProjectName(Project, role);

                string baseCriteria = "[Dataset.Project.Oid] = '" + Project.Oid.ToString() + "'";

              
                SecuritySystemTypePermissionObject securityDatasetPermissions = new SecuritySystemTypePermissionObject(session);
                securityDatasetPermissions.TargetType = typeof(Dataset);
                securityDatasetPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityDatasetPermissions);


                SecuritySystemTypePermissionObject securityProjectPermissions = new SecuritySystemTypePermissionObject(session);
                securityProjectPermissions.TargetType = typeof(Project);
                securityProjectPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityProjectPermissions);

                SecuritySystemObjectPermissionsObject ProjectPermission = new SecuritySystemObjectPermissionsObject(session);
                ProjectPermission.Criteria = "[Oid] = '" + Project.Oid.ToString() + "'"; ;
                ProjectPermission.AllowNavigate = true;
                ProjectPermission.AllowRead = true;
                securityProjectPermissions.ObjectPermissions.Add(ProjectPermission);

                //AddData(session,Project);
                role.Project = Project;
                role.RoleType = RoleType.ProjectMember;
                role.Save();
                if (Value_WindowController.Instance() != null)
                    foreach (ModuleBase mod in Value_WindowController.Instance().Application.Modules)
                    {
                        if (mod.Name.Contains("EarthCape"))
                            foreach (Type type in mod.GetExportedTypes())
                            {
                                if ((typeof(IDatasetObject).IsAssignableFrom(type)))
                                    AddReadObjectPermissions(session, type, role, "[Dataset.Project.Oid] = '" + Project.Oid.ToString() + "'");
                                if ((typeof(IProjectObject).IsAssignableFrom(type)))
                                    AddReadObjectPermissions(session, type, role, "[Project.Oid] = '" + Project.Oid.ToString() + "'");
                            }
                    }
                role.Save();
            }
            return role;
        }
        */
     /*   public static OrgRole CreateOrganizationMemberRole(Session session, Organization Organization)
        {
            OrgRole role = session.FindObject<OrgRole>(CriteriaOperator.And(new BinaryOperator("Organization.Oid", Organization.Oid), new BinaryOperator("RoleType", OrgRoleType.OrganizationAdmin)));
            if (role == null)
            {
                role = new OrgRole(session);
                role.Name = Guid.NewGuid().ToString();

                string baseCriteria = "[Dataset.Project.Organization.Oid] = '" + Organization.Oid.ToString() + "'";
                if (Value_WindowController.Instance()!=null)
                foreach (ModuleBase mod in Value_WindowController.Instance().Application.Modules)
                {
                    if (mod.Name.Contains("EarthCape"))
                        foreach (Type type in mod.GetExportedTypes())
                        {
                           if ((typeof(IOrganizationObject).IsAssignableFrom(type)))
                                AddReadObjectPermissions(session, type, role, "[Organization.Oid] = '" + Organization.Oid.ToString() + "'");
                         }
                }

                SecuritySystemTypePermissionObject securityDatasetPermissions = new SecuritySystemTypePermissionObject(session);
                securityDatasetPermissions.TargetType = typeof(Dataset);
                securityDatasetPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityDatasetPermissions);


                SecuritySystemTypePermissionObject securityOrganizationPermissions = new SecuritySystemTypePermissionObject(session);
                securityOrganizationPermissions.TargetType = typeof(Organization);
                securityOrganizationPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityOrganizationPermissions);

                SecuritySystemObjectPermissionsObject OrganizationPermission = new SecuritySystemObjectPermissionsObject(session);
                OrganizationPermission.Criteria = "[Oid] = '" + Organization.Oid.ToString() + "'"; ;
                OrganizationPermission.AllowNavigate = true;
                OrganizationPermission.AllowRead = true;
                securityOrganizationPermissions.ObjectPermissions.Add(OrganizationPermission);

                //AddData(session,Organization);
                role.Organization = Organization;
                role.RoleType = OrgRoleType.OrganizationMember;
                role.Save();
            }
            return role;
        }  */   

  /*      public static Role CreateProjectDataEditorRole(Session session, Project Project)
        {
            Role role = session.FindObject<Role>(CriteriaOperator.And(new BinaryOperator("Project.Oid", Project.Oid), new BinaryOperator("RoleType", RoleType.ProjectDataEditor)));
            if (role == null)
            {
                role = new Role(session);
                role.Project = Project;
                role.RoleType = RoleType.ProjectDataEditor;
                ShortenProjectName(Project, role);

                string baseCriteria = "[Dataset.Project.Oid] = '" + Project.Oid.ToString() + "'";


               
      
                SecuritySystemTypePermissionObject securityDatasetPermissions = new SecuritySystemTypePermissionObject(session);
                securityDatasetPermissions.TargetType = typeof(Dataset);
                securityDatasetPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityDatasetPermissions);
         
                SecuritySystemTypePermissionObject securityProjectPermissions = new SecuritySystemTypePermissionObject(session);
                securityProjectPermissions.TargetType = typeof(Project);
                securityProjectPermissions.AllowNavigate = true;
                role.TypePermissions.Add(securityProjectPermissions);

                SecuritySystemObjectPermissionsObject ProjectPermission = new SecuritySystemObjectPermissionsObject(session);
                ProjectPermission.Criteria = "[Oid] = '" + Project.Oid.ToString() + "'"; ;
                ProjectPermission.AllowNavigate = true;
                ProjectPermission.AllowRead = true;
                securityProjectPermissions.ObjectPermissions.Add(ProjectPermission);

                //AddData(session, Project);
                role.Save();
                if (Value_WindowController.Instance() != null)
                    foreach (ModuleBase mod in Value_WindowController.Instance().Application.Modules)
                    {
                        if (mod.Name.Contains("EarthCape"))
                            foreach (Type type in mod.GetExportedTypes())
                            {
                                if ((typeof(IDatasetObject).IsAssignableFrom(type)))
                                    AddReadWriteObjectPermissions(session, type, role, "[Dataset.Project.Oid] = '" + Project.Oid.ToString() + "'");
                                if ((typeof(IProjectObject).IsAssignableFrom(type)))
                                    AddReadWriteObjectPermissions(session, type, role, "[Project.Oid] = '" + Project.Oid.ToString() + "'");
                            }
                    }
                role.Save();
            }
            return role;

       
        }
        */
        public static void AddData(Session session, Project Project)
        {
           /* Group group1 = new Group(session);
            group1.Name = "Group1";
            group1.Save();*/

            Dataset dataset1 = new Dataset(session);
            dataset1.Name = "Dataset-" + Project.Name;
            dataset1.Project = Project;
            dataset1.Save();

            Species sp1 = new Species(session);
            sp1.Name = "sp1";
            sp1.Project = Project;
            sp1.Save();
            Species sp2 = new Species(session);
            sp2.Name = "sp2";
            sp2.Project = Project;
            sp2.Save();
            Species sp3 = new Species(session);
            sp3.Name = "sp3";
            sp3.Project = Project;
            sp3.Save();

            Unit unit1 = new Unit(session);
            unit1.UnitID = "Unit1-" + Project.Name;
            unit1.TaxonomicName = sp1;
            unit1.Dataset = dataset1;
            unit1.Save();

            Unit unit2 = new Unit(session);
            unit2.UnitID = "Unit2-" + Project.Name;
            unit2.TaxonomicName = sp2;
            unit2.Dataset = dataset1;
            unit2.Save();
            Unit unit3 = new Unit(session);
            unit3.UnitID = "Unit3-" + Project.Name;
            unit3.TaxonomicName = sp3;
            unit3.Dataset = dataset1;
            unit3.Save();

       
           /* Group group2 = new Group(session);
            group2.Name = "Group2";
            group2.Save();

            Dataset dataset2 = new Dataset(session);
            dataset2.Name = "Dataset2";
            dataset2.Project = group2;
            dataset2.Save();

            Unit unit2 = new Unit(session);
            unit2.UnitID = "Unit2";
            unit2.Dataset = dataset2;
            unit2.Save();*/

        }

        public static void AddAdminObjectPermissions(Session session, Type type, Role role, string baseCriteria)
        {
            foreach (SecuritySystemTypePermissionObject perm in role.TypePermissions)
                if (perm.TargetType == type)
                    return;
            if (type.Name == "")
                return;
            SecuritySystemTypePermissionObject securityUnitPermissions = new SecuritySystemTypePermissionObject(session);
            securityUnitPermissions.TargetType = type;
            securityUnitPermissions.AllowNavigate = true;
            role.TypePermissions.Add(securityUnitPermissions);

            SecuritySystemObjectPermissionsObject UnitPermission = new SecuritySystemObjectPermissionsObject(session);
            UnitPermission.Criteria = baseCriteria;
            UnitPermission.AllowNavigate = true;
            UnitPermission.AllowRead = true;
            UnitPermission.AllowWrite = true;
            UnitPermission.AllowDelete = true;
            securityUnitPermissions.ObjectPermissions.Add(UnitPermission);

        }
        public static void AddReadObjectPermissions(Session session, Type type, Role role, string baseCriteria)
        {
            foreach (SecuritySystemTypePermissionObject perm in role.TypePermissions)
                if (perm.TargetType == type)
                    return;
            SecuritySystemTypePermissionObject securityUnitPermissions = new SecuritySystemTypePermissionObject(session);
            securityUnitPermissions.TargetType = type;
            securityUnitPermissions.AllowNavigate = true;
            role.TypePermissions.Add(securityUnitPermissions);

            SecuritySystemObjectPermissionsObject UnitPermission = new SecuritySystemObjectPermissionsObject(session);
            UnitPermission.Criteria = baseCriteria;
            UnitPermission.AllowNavigate = true;
            UnitPermission.AllowRead = true;
            securityUnitPermissions.ObjectPermissions.Add(UnitPermission);

        }
        public static void AddReadWriteObjectPermissions(Session session, Type type, Role role, string baseCriteria)
        {
            foreach (SecuritySystemTypePermissionObject perm in role.TypePermissions)
                if (perm.TargetType == type)
                    return;
            SecuritySystemTypePermissionObject securityUnitPermissions = new SecuritySystemTypePermissionObject(session);
            securityUnitPermissions.TargetType = type;
            securityUnitPermissions.AllowNavigate = true;
            role.TypePermissions.Add(securityUnitPermissions);

            SecuritySystemObjectPermissionsObject UnitPermission = new SecuritySystemObjectPermissionsObject(session);
            UnitPermission.Criteria = baseCriteria;
            UnitPermission.AllowNavigate = true;
            UnitPermission.AllowRead = true;
            UnitPermission.AllowWrite = true;
            securityUnitPermissions.ObjectPermissions.Add(UnitPermission);

        }

    }
}
