﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.Objects
{
    public class AllMemberOperator : ICustomFunctionOperator
    {
        public string Name
        {
            get { return "AllMember"; }
        }
        public object Evaluate(params object[] operands)
        {
            return DateTime.Today.AddDays(-7);
        }
        public Type ResultType(params Type[] operands)
        {
            return typeof(DateTime);
        }
        static AllMemberOperator()
        {
            AllMemberOperator instance = new AllMemberOperator();
            if (CriteriaOperator.GetCustomFunction(instance.Name) == null)
            {
                CriteriaOperator.RegisterCustomFunction(instance);
            }
        }
        public static void Register() { }
    }
}
