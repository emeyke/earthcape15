using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Updating;
using DevExpress.Persistent.Base;
using DevExpress.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Xpand.ExpressApp.Security.Registration;

namespace EarthCape.Module.Core
{
    public sealed partial class EarthCapeModule : ModuleBase
    {
        public override void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
        {
            base.ExtendModelInterfaces(extenders);
            extenders.Add<IModelMember, IModelDwCExtension>();
            extenders.Add<IModelColumn, IModelDocExtension>();
            extenders.Add<IModelMember, IModelDocExtension>();
            extenders.Add<IModelClass, IModelDocExtension>();
            extenders.Add<IModelListView, IModelDocExtension>();
            extenders.Add<IModelDetailView, IModelDocExtension>();
        //    extenders.Add<IModelColumn, IModelColumnContext>();
        }
        public override void AddGeneratorUpdaters(ModelNodesGeneratorUpdaters updaters)
        {
            base.AddGeneratorUpdaters(updaters);
            updaters.Add(new ModelBOModelClassNodesGeneratorUpdater());
            updaters.Add(new ModelBOModelMemberNodesGeneratorUpdater());
        }
        public static int ReadBytesSize = 0x1000;
         public const string EmailPattern = @"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";
        public static string FileSystemStoreLocation = ((!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultFileStore"]))) ? (ConfigurationManager.AppSettings["DefaultFileStore"]) : String.Format("{0}FileData", PathHelper.GetApplicationFolder());
        public static string DefaultStorageLocation = ((!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultStorage"]))) ? (ConfigurationManager.AppSettings["DefaultStorage"]) : String.Format("{0}Files", PathHelper.GetApplicationFolder());
        public EarthCapeModule()
        {
            InitializeComponent();
        }
     
        public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB)
        {
            ModuleUpdater updater = new Updater(objectSpace, versionFromDB);
            return new ModuleUpdater[] { updater };
        }
        private void Application_CustomProcessShortcut(object sender, CustomProcessShortcutEventArgs e)
        {
            IModelListView view = Application.Model.Views[e.Shortcut.ViewId] as IModelListView;
            if (view != null)
            {
                Type t = XafTypesInfo.Instance.FindTypeInfo(view.AsObjectView.ModelClass.Name).Type;
                if (t.IsAssignableFrom(typeof(Search)))
                {
                    IObjectSpace space = Application.CreateObjectSpace(t);
                    //object obj = space.FindObject(t, null, true);
                    //if (obj == null)
                    object obj = space.CreateObject(t);

                    e.View = Application.CreateDetailView(space, obj);
                    e.Handled = true;
                }
            }
        }

        public static void CopyFileToStream(string sourceFileName, Stream destination)
        {
            if (string.IsNullOrEmpty(sourceFileName) || destination == null) return;
            using (Stream source = File.OpenRead(sourceFileName))
                CopyStream(source, destination);
        }
        public static void OpenFileWithDefaultProgram(string sourceFileName)
        {
            Guard.ArgumentIsNotNullOrEmpty(sourceFileName, "sourceFileName");
            System.Diagnostics.Process.Start(sourceFileName);
        }
        public static void CopyStream(Stream source, Stream destination)
        {
            if (source == null || destination == null) return;
            byte[] buffer = new byte[ReadBytesSize];
            int read = 0;
            while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                destination.Write(buffer, 0, read);
        }
        public override void Setup(XafApplication application)
        {
            base.Setup(application);
             application.SetupComplete += ApplicationOnSetupComplete;
            application.CustomProcessShortcut += Application_CustomProcessShortcut;
        }
        private void ApplicationOnSetupComplete(object sender, EventArgs eventArgs)
        {
            if (!(SecuritySystem.Instance is ServerSecurityClient))
            {
                var securityStrategy = ((SecurityStrategy)SecuritySystem.Instance);
                securityStrategy.AnonymousAllowedTypes.Add(typeof(RegisterUserParameters));
                securityStrategy.AnonymousAllowedTypes.Add(typeof(RestorePasswordParameters));
            }
        }
        /*   public override void CustomizeTypesInfo(DevExpress.ExpressApp.DC.ITypesInfo typesInfo)
           {
               base.CustomizeTypesInfo(typesInfo);
               XPDictionary xpDictionary = DevExpress.ExpressApp.Xpo.XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
               XPMemberInfo xpDictionaryGetClassInfoFindMember = xpDictionary.GetClassInfo(typeof(XPObjectType)).FindMember("TypeName");
               if (xpDictionaryGetClassInfoFindMember == null)
               {
                   xpDictionaryGetClassInfoFindMember.RemoveAttribute(typeof(SizeAttribute));
                   xpDictionaryGetClassInfoFindMember.AddAttribute(new SizeAttribute(191));
                //   iTypeName_XPObjectType
                   xpDictionaryGetClassInfoFindMember.
                   XafTypesInfo.Instance.RefreshInfo(typeof(XPObjectType));
               }
           }*/
    }
}
