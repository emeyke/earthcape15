﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.Base;

namespace EarthCape.Module.Core
{
    public class MyClass : ShowNavigationItemController
    {
        protected override bool HasRights(ChoiceActionItem item, IModelViews views)
        {
            ViewShortcut shortcut = (ViewShortcut)item.Data;
            IModelView view = views[shortcut.ViewId];
            if (view == null)
            {
                throw new ArgumentException(String.Format("Cannot find the '{0}' view specified by the shortcut: {1}", shortcut.ViewId, shortcut.ToString()));
            }
            Type type = (view is IModelObjectView) ? ((IModelObjectView)view).ModelClass.TypeInfo.Type : null;
            if (type != null)
            {
                if (!string.IsNullOrEmpty(shortcut.ObjectKey) && !shortcut.ObjectKey.StartsWith(CriteriaWrapper.ParameterPrefix))
                {
                    try
                    {
                        using (IObjectSpace objectSpace = CreateObjectSpace(type))
                        {
                            object targetObject = objectSpace.GetObjectByKey(type, objectSpace.GetObjectKey(type, shortcut.ObjectKey));
                            return DataManipulationRight.CanRead(type, null, targetObject, null, objectSpace) && DataManipulationRight.CanNavigate(type, targetObject, objectSpace);
                        }
                    }
                    catch { }
                }
                else
                {
                    return DataManipulationRight.CanNavigate(type, null, null);
                }
            }
            return true;
        }

    }
}
