﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing;
using DevExpress.ExpressApp.Model;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelScaleItem : IModelViewItem { }

    [DetailViewItemAttribute(typeof(IModelScaleItem))]
    public class MapScaleDetailViewItem : ViewItem
    {
        public MapScaleDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }
        protected override object CreateControlCore()
        {
            TGIS_ControlScale map = new TGIS_ControlScale();
            map.BorderStyle = BorderStyle.None;
            map.Width = 100;
            map.Name = "scale";

            return map;
        }
    }


}
  



