﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing;
using DevExpress.ExpressApp.Model;
using TatukGIS.NDK.WinForms;
using DevExpress.Utils.Controls;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelScaleItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelScaleItem))]
    public class MapScaleDetailViewItem : ViewItem
    {
        public MapScaleDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }
        protected override object CreateControlCore()
        {
            TGIS_ControlScale1 map = new TGIS_ControlScale1();
           // map.BorderStyle = BorderStyle.None;
            map.Name = "scale";

            return map;
        }
    }
    public partial class TGIS_ControlScale1 : TGIS_ControlScale, IXtraResizableControl {
        public TGIS_ControlScale1()
        {
         }
  #region IXtraResizableControl Members

    public event EventHandler Changed;

    bool IXtraResizableControl.IsCaptionVisible {
        get { return true; }
    }

    Size IXtraResizableControl.MaxSize {
        get { return new Size(1000, 100); }
    }

    Size IXtraResizableControl.MinSize {
        get { return new Size(10, 10); }
    }

    #endregion

    protected virtual void OnChanged(){
        Changed?.Invoke(this, EventArgs.Empty);
    }
    }

    
}
  



