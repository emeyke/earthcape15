﻿using System;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using System.Windows.Forms;
using TatukGIS.NDK.WinForms;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelGPSItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelGPSItem))]
    public class MapGPSDetailViewItem : ViewItem
    {
        public MapGPSDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }
        protected override object CreateControlCore()
        {

            TGIS_GpsNmea map = new TGIS_GpsNmea();
            map.BorderStyle = BorderStyle.None;
            map.Width = 100;
            map.Height = 180;
            map.Name = "gps";

            return map;
        }
    }

}
  



