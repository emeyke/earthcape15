﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing;
using DevExpress.ExpressApp.Model;
using TatukGIS.NDK.WinForms;


namespace EarthCape.Module.Win
{
    public interface IModelLegendItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelLegendItem))]
    public class MapLegendDetailViewItem : ViewItem
    {
        public MapLegendDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }
        protected override object CreateControlCore()
        {


            TGIS_ControlLegend map = new TGIS_ControlLegend();
            map.ReverseOrder = false;
            map.BorderStyle = BorderStyle.FixedSingle;
            map.Width = 100;
            map.Name = "legend";

            return map;
        }
    }

 }
  



