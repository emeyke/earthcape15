﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing;
using DevExpress.ExpressApp.Model;
using TatukGIS.NDK.WinForms;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelImageMapItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelImageMapItem))]
    public class ImageMapDetailViewItem : ViewItem
    {
        public ImageMapDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id)
        {
            CreateControl();
        }

        protected override object CreateControlCore()
        {

            TGIS_ViewerWnd map = new TGIS_ViewerWnd();
            map.Height = 800;
            map.Mode = TGIS_ViewerMode.gisZoom;
            map.BorderStyle = BorderStyle.FixedSingle;
            map.Width = 800;
            map.Name = "gis";


            return map;
        }
    }


}




