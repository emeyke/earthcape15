using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using TatukGIS.NDK;
using DevExpress.ExpressApp.Editors;

namespace EarthCape.Module.GIS.Win
{
    public partial class Place_ViewController : ViewController
    {
        public Place_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            Activated += Place_ViewController_Activated;
        }

        void Place_ViewController_Activated(object sender, EventArgs e)
        {
            caption=popupWindowShowAction_RecordVisit.Caption;
        }

        private void popupWindowShowAction_RecordVisit_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            if (record_view != null)
            {
                os = (ObjectSpace)Application.CreateObjectSpace();
                SureConfirmationWindow obj = os.CreateObject<SureConfirmationWindow>();
                e.DialogController.CancelAction.Active.SetItemValue("", false);
                e.View = Application.CreateDetailView(os, obj); ;
                //record_view = null;
            }
            else
            {
                os = (ObjectSpace)Application.CreateObjectSpace();
                visit_params = os.CreateObject<RecordVisitWindow>();
                visit = os.CreateObject<PlaceVisit>();
                Place place = os.FindObject<Place>(new BinaryOperator("Oid", ((Place)(View.CurrentObject)).Oid));
                visit.Place = place;
                visit.StartOn = DateTime.Now;
                visit.Save();
                //os.CommitChanges();
                visit_params.RecordVisitToPlace = place;
                record_view = Application.CreateDetailView(os, visit_params);
                record_view.Tag = View;
                e.View = record_view;
            }
        }
        Timer timer = new Timer();
        int timer_tick;
        PlaceVisit visit;
        DevExpress.ExpressApp.View record_view;
        RecordVisitWindow visit_params;
        ObjectSpace os;
        TGIS_GpsNmea GPS;
        string caption="";
        private void popupWindowShowAction_RecordVisit_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            if (e.PopupWindow.View.ObjectTypeInfo.Type == typeof(RecordVisitWindow))
            {
                timer.Stop();
                    timer.Interval = 1000;
                    timer.Tick += timer_Tick;
                    timer.Start();
            }
            if (e.PopupWindow.View.ObjectTypeInfo.Type == typeof(SureConfirmationWindow))
            {
                os = (ObjectSpace)Application.CreateObjectSpace();
                visit = os.FindObject<PlaceVisit>(new BinaryOperator("Oid", visit.Oid));
                Place place = os.FindObject<Place>(new BinaryOperator("Oid", visit_params.RecordVisitToPlace.Oid));
                visit.EndOn = DateTime.Now;
                TimeSpan ts =visit.EndOn-visit.StartOn;//new TimeSpan( TimeSpan.FromMilliseconds(timer_tick);
                visit.Subject = String.Format("Visit to place {0}, for {1} h {2} m, {3} s", place.ToString(), ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                if (!String.IsNullOrEmpty(visit_params.PlaceVisitComment))
                    visit.Comment = visit_params.PlaceVisitComment;
                visit.Save();
                if (!String.IsNullOrEmpty(visit_params.PlaceNote))
                {
                    Note note = os.CreateObject<Note>();
                    note.Text = visit_params.PlaceNote;
                    note.Save();
                    place.Notes.Add(note);
                }
                timer.Stop();
                visit.Save();
                os.CommitChanges();
                popupWindowShowAction_RecordVisit.Caption = caption;
             //   os.CommitChanges();
                record_view = null;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (visit_params.RecordGpsTracks)
            {
                DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                GPS = GISHelperWin.GetGPS(mapview);
                if (GPS == null)
                    GISHelperWin.GetGPS((DetailView)View);
                if (GPS != null)
                {
                    GISHelperWin.AddToWKT(visit, TGIS_Utils.GisPoint(GPS.Longitude, GPS.Latitude));
                }
            }
            timer_tick+=timer.Interval;
            TimeSpan ts = TimeSpan.FromMilliseconds(timer_tick);
            popupWindowShowAction_RecordVisit.Caption = String.Format("Recording {0}:{1}:{2} (press to stop)", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
        }
    }
}
