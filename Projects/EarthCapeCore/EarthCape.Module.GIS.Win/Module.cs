using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;

namespace EarthCape.Module.GIS.Win
{
    public sealed partial class EarthCapeGISWinModule : ModuleBase
    {
        public static bool UpdateMap = true;
        public EarthCapeGISWinModule()
        {
            InitializeComponent();
        }
    }
}
