using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.GIS.Win
{
    public partial class Locality_ViewController : ViewController
    {
        public Locality_ViewController()
        {
            /*InitializeComponent();
            RegisterActions(components);
            Activated += Locality_ViewController_Activated;*/
        }

        void Locality_ViewController_Activated(object sender, EventArgs e)
        {
            caption=popupWindowShowAction_RecordVisit.Caption;
        }

        private void popupWindowShowAction_RecordVisit_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
         /*   if (record_view != null)
            {
                os = (XPObjectSpace)Application.CreateObjectSpace();
                SureConfirmationWindow obj = os.CreateObject<SureConfirmationWindow>();
                e.DialogController.CancelAction.Active.SetItemValue("", false);
                e.View = Application.CreateDetailView(os, obj); ;
                //record_view = null;
            }
            else
            {
                os = (XPObjectSpace)Application.CreateObjectSpace();
                visit_params = os.CreateObject<RecordVisitWindow>();
                visit = os.CreateObject<LocalityVisit>();
                Locality Locality = os.FindObject<Locality>(new BinaryOperator("Oid", ((Locality)(View.CurrentObject)).Oid));
                visit.Locality = Locality;
                visit.StartOn = DateTime.Now;
                visit.Save();
                //os.CommitChanges();
                visit_params.RecordVisitToPlace = Locality;
                record_view = Application.CreateDetailView(os, visit_params);
                record_view.Tag = View;
                e.View = record_view;
            }
        */}
        Timer timer = new Timer();
        // int timer_tick;
        // LocalityVisit visit;
        // DevExpress.ExpressApp.View record_view;
       // RecordVisitWindow visit_params;
        // XPObjectSpace os;
        // TGIS_GpsNmea GPS;
        string caption="";
        private void popupWindowShowAction_RecordVisit_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
           /* if (e.PopupWindow.View.ObjectTypeInfo.Type == typeof(RecordVisitWindow))
            {
                timer.Stop();
                    timer.Interval = 1000;
                    timer.Tick += timer_Tick;
                    timer.Start();
            }
            if (e.PopupWindow.View.ObjectTypeInfo.Type == typeof(SureConfirmationWindow))
            {
                os = (XPObjectSpace)Application.CreateObjectSpace();
                visit = os.FindObject<LocalityVisit>(new BinaryOperator("Oid", visit.Oid));
                Locality Locality = os.FindObject<Locality>(new BinaryOperator("Oid", visit_params.RecordVisitToPlace.Oid));
                visit.EndOn = DateTime.Now;
                TimeSpan ts =visit.EndOn-visit.StartOn;//new TimeSpan( TimeSpan.FromMilliseconds(timer_tick);
                visit.Subject = String.Format("Visit to Locality {0}, for {1} h {2} m, {3} s", Locality.ToString(), ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
                if (!String.IsNullOrEmpty(visit_params.LocalityVisitComment))
                    visit.Comment = visit_params.LocalityVisitComment;
                visit.Save();
                if (!String.IsNullOrEmpty(visit_params.PlaceNote))
                {
                    Note note = os.CreateObject<Note>();
                    note.Text = visit_params.PlaceNote;
                    note.Save();
                    Locality.Notes.Add(note);
                }
                timer.Stop();
                visit.Save();
                os.CommitChanges();
                popupWindowShowAction_RecordVisit.Caption = caption;
             //   os.CommitChanges();
                record_view = null;
            }*/
        }

        void timer_Tick(object sender, EventArgs e)
        {
          /*  if (visit_params.RecordGpsTracks)
            {
                DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                GPS = GISHelperWin.GetGPS(mapview);
                if (GPS == null)
                    GISHelperWin.GetGPS((DetailView)View);
                if (GPS != null)
                {
                    GISHelperWin.AddToWKT(visit, TGIS_Utils.GisPoint(GPS.Longitude, GPS.Latitude));
                }
            }
            timer_tick+=timer.Interval;
            TimeSpan ts = TimeSpan.FromMilliseconds(timer_tick);
            popupWindowShowAction_RecordVisit.Caption = String.Format("Recording {0}:{1}:{2} (press to stop)", ts.Hours.ToString(), ts.Minutes.ToString(), ts.Seconds.ToString());
        */}
    }
}
