using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Editors;
using System.Collections.ObjectModel;

using System.IO;
using System.Collections.Generic;
using TatukGIS.NDK;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using System.Configuration;
using DevExpress.Persistent.Base;

namespace EarthCape.Module.GIS.Win
{
    public partial class IWKT_ViewController : ViewController
    {
        public IWKT_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType = typeof(IWKT);
            //            this.TargetViewType = ViewType.ListView;
            this.Activated += IWKT_ViewController_Activated;
        }

        void IWKT_ViewController_Activated(object sender, EventArgs e)
        {
            if (View is DetailView)
            {
                View.ControlsCreated += View_ControlsCreated;
                View.CurrentObjectChanged += View_CurrentObjectChanged;
            }
        }
      
        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            if (EarthCapeGISWinModule.UpdateMap)
                UpdateControl();
        }

        

       
        private void UpdateControl()
        {
            if (View.CurrentObject == null) return;
            if (!(View.CurrentObject is IWKT)) return;
            TGIS_LayerAbstract extent = null;
            IWKT iwkt = (IWKT)View.CurrentObject;
            Project project = null;
            if (iwkt is IProjectObject)
                project = (iwkt as IProjectObject).Project;
            if (iwkt is EarthCape.Module.Core.IDatasetObject)
                if ((iwkt as EarthCape.Module.Core.IDatasetObject).Dataset != null)
                    project = (iwkt as EarthCape.Module.Core.IDatasetObject).Dataset.Project;
            if (project != null)
                if (project.Map != null)
                    map = project.Map;
            // if (String.IsNullOrEmpty(((MapProject)View.CurrentObject).GetWKT())) return;
            DetailView view = GISHelperWin.GetMapWindowDetalView((DetailView)View);
            if (view == null) return;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow(view);
            if (gis == null) return;

            // if (map != null) { ((MapWindow)view.CurrentObject).MapProject = map; }
           
            TGIS_LayerVector ll = GISHelperWin.CreateLocUnitsLayers(gis, GISHelper.GetLayerName(iwkt.GetType()));


           GISHelperWin.UpdateControl(extent, gis, iwkt, map);
         //         GISHelper.LayerParams(ll);
            //  gis.Invalidate();

        }


        //  gis.SaveAll();
        /* if (map != null)
             if (map.EPSG > 0)
                 gis.SetCSByEPSG(map.EPSG);
         gis.RecalcExtent();
         gis.Refresh();
         gis.Invalidate();
         if (gis.Items.Count > 0)
         {
             legend.GIS_Layer = gis.Get(((TGIS_LayerAbstract)gis.Items[0]).Name);
         }
         mapview.Refresh();
         if (extent != null)
             gis.VisibleExtent = extent.Extent;
         gis.MouseClick += gis_MouseClick;
         gis.MouseDoubleClick += gis_MouseDoubleClick;
         gis.EditorChange += gis_EditorChange;
         //legend.LayerSelect += new TGIS_LayerEvent(legend_LayerSelect);
  */

        //}
        void View_ControlsCreated(object sender, EventArgs e)
        {
            DetailView view = GISHelperWin.GetMapWindowDetalView((DetailView)sender);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow(view);
            Project Project = null;
            if (Value_WindowController.Instance() != null)
                Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
            if (Project == null)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                {
                    Project = View.ObjectSpace.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                }

            }
            if (Project != null)
                if (Project.Map != null)
                {
                    if (view != null)
                    {
                        GISHelperWin.OpenMapProject(gis, Project.Map);

                    }
                }
            UpdateControl();
      /*      if (gis != null)
            {
                if (typeof(IUnits).IsAssignableFrom(View.ObjectTypeInfo.GetType()))
                IUnitsMapHook((DetailView)View);
            }*/
        }
        private void IUnitsMapHook(DetailView view)
        {
            DevExpress.ExpressApp.ListView units_view = null;
            foreach (ViewItem item in (view).Items)
            {
                ListPropertyEditor units_editor = item as ListPropertyEditor;
                if (units_editor != null)
                {
                    if (units_editor.ListView != null)
                    {
                        object listEditor = units_editor.ListView.Editor;
                        if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                            units_view = units_editor.ListView;
                    }
                }
            }
            if (units_view != null)
            {
                units_view.SelectionChanged += units_view_CurrentObjectChanged;
            }
        }
        void units_view_CurrentObjectChanged(object sender, EventArgs e)
        {
            DetailView view = GISHelperWin.GetMapWindowDetalView((DetailView)View);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow(view);
            if (gis == null) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            if (ll == null) return;
            ll.DeselectAll();
            foreach (Unit item in (sender as DevExpress.ExpressApp.ListView).SelectedObjects)
            {
                TGIS_Shape shp = ll.FindFirst(gis.VisibleExtent, String.Format("ECID='{0}'", item.Oid.ToString()));
                if (shp != null)
                    shp.IsSelected = true;
            }
        }

        private MapProject map = null;
        private void simpleAction_MapUnits_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            MapWindow mapw = obs.CreateObject<MapWindow>();
            //  mapw.Name = ((MapProject)e.PopupWindow.View.CurrentObject).Name;
            if ((e.PopupWindow.View).CurrentObject != null)
                map = obs.FindObject<MapProject>(new BinaryOperator("Oid", ((MapProject)(e.PopupWindow.View).CurrentObject).Oid));
            // mapw.MapProject = map;
            DetailView dv = Application.CreateDetailView(obs, mapw, true);
            dv.ControlsCreated += dv_ControlsCreated;
            //Frame.SetView(dv);
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.MdiChild;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
            e.ShowViewParameters.CreatedView = dv;




        }



        void dv_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)sender);
            XPObjectSpace os = (XPObjectSpace)((DetailView)sender).ObjectSpace;
            Project Project = Value_WindowController.Instance().GetGroup(os);
            if ((map != null) && (gis.Items.Count == 0))
            {
                GISHelperWin.OpenMapProject(gis, map);

            }
            string llname = "";
            if (typeof(Locality).IsAssignableFrom(View.ObjectTypeInfo.Type))
                llname = "Localities";
            if (typeof(Store).IsAssignableFrom(View.ObjectTypeInfo.Type))
                llname = "Stores";
            if (typeof(Project).IsAssignableFrom(View.ObjectTypeInfo.Type))
                llname = "Projects";
            if (typeof(LocalityVisit).IsAssignableFrom(View.ObjectTypeInfo.Type))
                llname = "Locality visits";
            if (typeof(Unit).IsAssignableFrom(View.ObjectTypeInfo.Type))
                llname = "Units";
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get(llname);
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = llname;
                ll.CS = gis.CS;
                GISHelper.LayerParams(ll);

                gis.Add(ll);
            }
            GISHelper.AddFields(ll);
       //     Collection<String> coll = new Collection<string>();
            foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotIWKT(ll, null, item, Project, false);
            }
            ll.SaveAll();
            /*foreach (IWKT item in View.SelectedObjects)
            {
                TGIS_Shape shp = GISHelper.AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.Name, gis.CS.EPSG, false,true);
            }*/
            gis.VisibleExtent = ll.Extent;

            //GISHelperWin.UpdateMap(os, gis, Project, View.SelectedObjects);

        }


        /*  void dv_ControlsCreated(object sender, EventArgs e)
          {
              TGIS_ViewerWnd gis=GISHelperWin.MapObject(sender as DetailView);
              TGIS_LayerVector ll=new TGIS_LayerVector();
              gis.Add(ll);
              XPCollection coll=(XPCollection)(((ListView)View).CollectionSource.Collection);
              GISHelper.MapUnits2(coll,ll,(TGIS_LayerVector)gis.Get("MapSummaryLayer"),map.SummaryType);
          }*/

        private void simpleAction_MapUnits_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(obs, typeof(MapProject), false);
            e.View = view;

        }



        private void popupWindowShowAction_GE_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IWKTExport2GEOptions options = (IWKTExport2GEOptions)e.PopupWindow.View.CurrentObject;
            TGIS_ViewerWnd gis = new TGIS_ViewerWnd();
            Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
            MapProject map = null;
            if (Project != null)
                if (Project.Map != null)
                    map = Project.Map;
            TGIS_LayerVector llSummary;
            Collection<String> coll;
            TGIS_LayerVector ll;
            int epsg = 4326;
            GISHelperWin.PreparePlotUnit((XPObjectSpace)View.ObjectSpace, gis, epsg, out llSummary, out coll, out ll);
            GISHelper.AddFields(ll);
            foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotIWKT(ll, llSummary, item, Project, options.CentroidOnly);
            }

            /*  ll.SaveData();
              ll.SaveAll();
              gis.VisibleExtent = ll.Extent;
              gis.Invalidate(); */
            TGIS_LayerVector layer = (TGIS_LayerVector)gis.Items[0];
            /* if (Project != null)
                 if (Project.Map != null)
                     if (Project.Map.EPSG > 0)
                         layer.SetCSByEPSG(Project.Map.EPSG);*/
            GISHelperWin.ExportToGE(layer);
        }

        private void popupWindowShowAction_GE_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {

            IObjectSpace os = Application.CreateObjectSpace();
            IWKTExport2GEOptions options = os.CreateObject<IWKTExport2GEOptions>();
            e.View = Application.CreateDetailView(os, options);
        }

        private void popupWindowShowAction_ToVectorMapFile_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IWKTExport2VectorOptions options = (IWKTExport2VectorOptions)e.PopupWindow.View.CurrentObject;
            TGIS_ViewerWnd gis = new TGIS_ViewerWnd();
            Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
            MapProject map = null;
            if (Project != null)
                if (Project.Map != null)
                    map = Project.Map;
            TGIS_LayerVector llSummary;
            Collection<String> coll;
            TGIS_LayerVector ll;
            int epsg = 0;
            if (Project != null) if (Project.EPSG > 0) epsg = Project.EPSG;
            if (map != null) if (map.EPSG > 0) epsg = map.EPSG;
            if (options.TargetEpsg > 0)
                epsg = options.TargetEpsg;

            GISHelperWin.PreparePlotUnit((XPObjectSpace)View.ObjectSpace, gis, epsg, out llSummary, out coll, out ll);
            GISHelper.AddFields(ll);

            foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotIWKT(ll, llSummary, item, Project, options.CentroidOnly);
            }

            /*  ll.SaveData();
              ll.SaveAll();
              gis.VisibleExtent = ll.Extent;
              gis.Invalidate(); */
            TGIS_LayerVector layer = (TGIS_LayerVector)gis.Items[0];
            /* if (Project != null)
                 if (Project.Map != null)
                     if (Project.Map.EPSG > 0)
                         layer.SetCSByEPSG(Project.Map.EPSG);*/
            GISHelperWin.ExportToVector(layer, -1);
        }

        private void popupWindowShowAction_ToVectorMapFile_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            IWKTExport2VectorOptions options = os.CreateObject<IWKTExport2VectorOptions>();
            e.View = Application.CreateDetailView(os, options);

        }

        private void simpleAction_UpdateAreaAndLengthFromGeometry_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IWKT iwkt in View.SelectedObjects)
            {
                GISHelper.UpdateAreaAndLengthFromWKT(iwkt);
                iwkt.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_ConvertToEPSG_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ConvertToEPSG options = os.CreateObject<ConvertToEPSG>();
            e.View = Application.CreateDetailView(os, "ConvertToEPSG_DetailView", false, options);

        }

        private void popupWindowShowAction_ConvertToEPSG_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ConvertToEPSG options = ((ConvertToEPSG)e.PopupWindow.View.CurrentObject);
            foreach (IWKT iwkt in View.SelectedObjects)
            {
                if (options.AssignEPSGWithoutConversion)
                {
                    iwkt.EPSG = options.TargetEPSG;
                    iwkt.Save();
                }
                else
                {
                    int s_epsg = options.SourceEPSG;
                    if (iwkt.EPSG > 0) s_epsg = iwkt.EPSG;
                    if ((s_epsg > 0) && options.TargetEPSG > 0)
                    {
                        GISHelper.ConvertToEPSG(iwkt, s_epsg, options.TargetEPSG);
                        iwkt.Save();
                    }

                }
            }
            View.ObjectSpace.CommitChanges();

        }

        private void popupWindowShowAction_DistanceMatrix_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            DistanceMatrix options = ((DistanceMatrix)e.PopupWindow.View.CurrentObject);
            IWKT iwkt = null;
            IWKT iwkt1 = null;

            int viewSelectedObjectsCount = View.SelectedObjects.Count;
            // string line = "";
            // string[,] array = new string[viewSelectedObjectsCount + 1, viewSelectedObjectsCount + 1];
            string path = String.Format("{0}\\Output\\distance{1}.txt", PathHelper.GetApplicationFolder(), DateTime.Now.ToString().Replace(":", "").Replace(" ", ""));
            if (!Directory.Exists(String.Format("{0}\\Output\\", PathHelper.GetApplicationFolder())))
                Directory.CreateDirectory(String.Format("{0}\\Output\\", PathHelper.GetApplicationFolder()));
            using (TextWriter tw = new StreamWriter(path))
            {
                for (int i = 0; i < viewSelectedObjectsCount; i++)
                {
                    iwkt = (IWKT)View.SelectedObjects[i];
                    // array[i, 0] = iwkt.Name;

                    for (int j = i; j < viewSelectedObjectsCount; j++)
                    {
                        if (i != j)
                        {
                            iwkt1 = (IWKT)View.SelectedObjects[j];
                            //array[0, j] = iwkt1.Name;
                            //if (j>0)
                            TGIS_Shape shp = TGIS_Utils.GisCreateShapeFromWKT(iwkt.WKT);
                            TGIS_Shape shp1 = TGIS_Utils.GisCreateShapeFromWKT(iwkt1.WKT);
                            TGIS_Point ptg = shp.Centroid();
                            TGIS_Point ptg1 = shp1.Centroid();
                            double distance = TGIS_Utils.GisPoint2Point(ptg, ptg1);
                            tw.WriteLine(String.Format("{0}\t{1}\t{2}", iwkt.Name, iwkt1.Name, distance));
                        }
                    }
                }
            }

        }

        private void popupWindowShowAction_DistanceMatrix_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            DistanceMatrix options = os.CreateObject<DistanceMatrix>();
            e.View = Application.CreateDetailView(os, options, false);

        }

        private void simpleAction_UpdateLatLongFromWKT_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IWKT iwkt in View.SelectedObjects)
            {
                GISHelper.CreateLatLong(iwkt);
                iwkt.Save();
            }
            View.ObjectSpace.CommitChanges();
        }
    }
}