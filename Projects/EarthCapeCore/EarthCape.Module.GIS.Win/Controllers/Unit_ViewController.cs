using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Data.Filtering;
using TatukGIS.NDK.WinForms;
using System.Collections.ObjectModel;
using TatukGIS.NDK;

namespace EarthCape.Module.GIS.Win
{
    public partial class Unit_ViewController : ViewController
    {
        private MapProject map = null;
        public Unit_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(Unit);
        }

        private void popupWindowShowAction_MapTracks_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            MapTracksOptions options = os.CreateObject<MapTracksOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }

        private void popupWindowShowAction_MapTracks_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            MapTracksOptions options = (MapTracksOptions)e.PopupWindow.View.CurrentObject;
            MapWindow mapw = obs.CreateObject<MapWindow>();
            map = obs.FindObject<MapProject>(new BinaryOperator("Oid", ((MapProject)options.MapProject).Oid));
            DetailView dv = Application.CreateDetailView(obs, mapw, true);
            dv.ControlsCreated += dv_ControlsCreated;
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.MdiChild;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
            e.ShowViewParameters.CreatedView = dv;
        }

        void dv_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)sender);
            XPObjectSpace os = (XPObjectSpace)((DetailView)sender).ObjectSpace;
            Project Project = GeneralHelper.GetCurrentProject(os);
            if ((map != null) && (gis.Items.Count == 0))
            {
                GISHelperWin.OpenMapProject(gis, map);

            }
            string llname = "UnitTracks";
            
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get(llname);
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = llname;
                ll.CS = gis.CS;
                ll.Params.Labels.Field = "UnitID";
                if (map != null)
                    ll.SetCSByEPSG(map.EPSG);
                GISHelper.AddFields(ll);
                GISHelper.LayerParams(ll);
                gis.Add(ll);
            }
            GISHelper.AddFields(ll);
            Collection<String> coll = new Collection<string>();
            foreach (Unit unit in View.SelectedObjects)
            {
                GISHelperWin.PlotTracks(View.ObjectSpace, ll, unit);
            }
            ll.SaveAll();
        }
    }
}
