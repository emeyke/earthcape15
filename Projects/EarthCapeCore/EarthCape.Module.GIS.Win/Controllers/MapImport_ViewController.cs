﻿using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK;
using System.IO;
using EarthCape.Module.Core;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapImport_ViewController : ViewController
    {
        public MapImport_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            Activated += new EventHandler(MapImport_ViewController_Activated);
            this.TargetViewNesting = Nesting.Root;
        }
        void MapImport_ViewController_Activated(object sender, EventArgs e)
        {

        }
        private void simpleAction1_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //todo runs out of memory on country.shp, slow
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Title = "Select file";
            dlgOpen.Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileType.gisFileTypeVector, false);
            dlgOpen.DefaultExt = "txt";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = false;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);
                //XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
               // User u = (uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), uow.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                string name = Path.GetFileName(dlgOpen.FileName);
            /*    MapLayerVector llw = new MapLayerVector(uow);// os.CreateObject<MapLayerVector>();
                llw.Name = name;
                llw.Save();*/
                TGIS_LayerVector ll = null;
                if (Path.GetExtension(dlgOpen.FileName.ToLower()) == ".kml")
                    ll = new TGIS_LayerKML();
                if (Path.GetExtension(dlgOpen.FileName.ToLower()) == ".shp")
                    ll = new TGIS_LayerSHP();
                TGIS_Viewer gis = new TGIS_Viewer();
                ll.Name = "temp";
                ll.Path = dlgOpen.FileName;
                gis.Add(ll);
                TGIS_Shape shp = ll.FindFirst(ll.Extent);
                while (shp != null)
                {
                    shp = shp.MakeEditable();
                    if (shp != null)
                    {
                        string n = "";
                        //if (shp.GetField("NAME") != null) n = GeneralHelper.Utf8ToUtf16(shp.GetField("NAME").ToString()).Replace(" район","");
                        if (shp.GetField("ID") != null) n = shp.GetField("ID").ToString();
                        if (shp.GetField("NAME") != null) n = shp.GetField("NAME").ToString();
                        string shpArea = Convert.ToString(shp.Area());
                        string shpWKT = shp.ExportToWKT();
                        CreateLocality(null, uow, n,"", shpArea, "", "", "", shpWKT, true, "",true,true,"");
                      }
                    uow.CommitChanges();
                    shp = ll.FindNext();
                }
              //  llw.Save();
                uow.CommitChanges();
            }
        }

        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, string parent, string area, string alt1, string date, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT, string admin)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                if (admin == "")
                {
                    loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                    if (loc == null)
                        loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                }
 
            }
            if (loc == null)
            {
                if (createnew)
                {
                    if (admin == "")
                    {
                        loc = new Locality(uow);
                    }
                     loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
                /* Note newnote = new Note(uow, note);
                 if (!String.IsNullOrEmpty(date)) 
                 newnote.RecordedOn = DateTime.Parse(date);
                 newnote.Save();
                 loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }

        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, LocalityAdm parent, string area, string alt1, string date, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                //if (parent ==null)
                //{
                loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                if (loc == null)
                    loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                /*}else{

                    loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Category.Oid", parent.Oid)));
                }*/
            }
            if (loc == null)
            {
                if (createnew)
                {
                    loc = new Locality(uow);
                    loc.Category = parent;
                    loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
                /* Note newnote = new Note(uow, note);
                 if (!String.IsNullOrEmpty(date)) 
                 newnote.RecordedOn = DateTime.Parse(date);
                 newnote.Save();
                 loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }

        private void popupWindowShowAction_ImportVector_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportVectorOptions options = (ImportVectorOptions)e.PopupWindow.View.CurrentObject;
        
        }

        private void popupWindowShowAction_ImportVector_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ImportVectorOptions options = os.CreateObject<ImportVectorOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }

    }
}
