namespace EarthCape.Module.GIS.Win
{
    partial class MapWindow_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 =
                new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 =
                new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.simpleAction_ExportVectorLayer = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ExportToImage =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_MapFullExtent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_AddLayer = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.parametrizedAction_WMS = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.simpleAction_LayerExtent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ExportToGE = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_DowloadAndMapGbifForExtent =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_MapLocalities = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ShowMapObject = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_NewLocality =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_MapShowUnits = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_NewUnit =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_RemoveLayer = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_GetLocalitiesFromView = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.parametrizedAction_FindAndMapLocalities =
                new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.singleChoiceAction_ViewMode = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.simpleAction_Select = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_Zoom = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_Edit = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_Pan = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ExportToVector =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ExportVectorLayer.Caption = "Export vector layer";
            this.simpleAction_ExportVectorLayer.Category = "Export";
            this.simpleAction_ExportVectorLayer.ConfirmationMessage = null;
            this.simpleAction_ExportVectorLayer.Id = "simpleAction_ExportSummary";
            this.simpleAction_ExportVectorLayer.ToolTip = null;
            this.simpleAction_ExportVectorLayer.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_ExportVectorLayer_Execute);
            this.popupWindowShowAction_ExportToImage.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportToImage.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportToImage.Caption = "Export to image";
            this.popupWindowShowAction_ExportToImage.Category = "Export";
            this.popupWindowShowAction_ExportToImage.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportToImage.Id = "popupWindowShowAction_ExportToImage";
            this.popupWindowShowAction_ExportToImage.ToolTip = null;
            this.popupWindowShowAction_ExportToImage.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_ExportToImage_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportToImage.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_ExportToImage_Execute);
            this.simpleAction_MapFullExtent.Caption = "Full extent";
            this.simpleAction_MapFullExtent.Category = "View";
            this.simpleAction_MapFullExtent.ConfirmationMessage = null;
            this.simpleAction_MapFullExtent.Id = "simpleAction_MapFullExtent";
            this.simpleAction_MapFullExtent.ToolTip = null;
            this.simpleAction_MapFullExtent.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_MapFullExtent_Execute);
            this.simpleAction_AddLayer.Caption = "Add layer";
            this.simpleAction_AddLayer.Category = "View";
            this.simpleAction_AddLayer.ConfirmationMessage = null;
            this.simpleAction_AddLayer.Id = "simpleAction_AddLayer";
            this.simpleAction_AddLayer.ImageName = "Action_LinkUnlink_Link";
            this.simpleAction_AddLayer.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_AddLayer.ToolTip = null;
            this.simpleAction_AddLayer.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_AddLayer_Execute);
            this.parametrizedAction_WMS.Caption = "Open Web Map Service (WMS)";
            this.parametrizedAction_WMS.Category = "View";
            this.parametrizedAction_WMS.ConfirmationMessage = null;
            this.parametrizedAction_WMS.Id = "parametrizedAction_WMS";
            this.parametrizedAction_WMS.NullValuePrompt = null;
            this.parametrizedAction_WMS.ShortCaption = "Add WMS";
            this.parametrizedAction_WMS.TargetViewId = "xx";
            this.parametrizedAction_WMS.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.parametrizedAction_WMS.ToolTip = null;
            this.parametrizedAction_WMS.Execute +=
                new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(
                    this.parametrizedAction_WMS_Execute);
            this.simpleAction_LayerExtent.Caption = "Layer extent";
            this.simpleAction_LayerExtent.Category = "View";
            this.simpleAction_LayerExtent.ConfirmationMessage = null;
            this.simpleAction_LayerExtent.Id = "simpleAction_LayerExtent";
            this.simpleAction_LayerExtent.ToolTip = null;
            this.simpleAction_LayerExtent.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_LayerExtent_Execute);
            this.simpleAction_ExportToGE.Caption = "Google Earth";
            this.simpleAction_ExportToGE.Category = "Export";
            this.simpleAction_ExportToGE.ConfirmationMessage = null;
            this.simpleAction_ExportToGE.Id = "simpleAction_ExportToGE";
            this.simpleAction_ExportToGE.ImageName = "google_earth_link";
            this.simpleAction_ExportToGE.ToolTip = null;
            this.simpleAction_ExportToGE.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ExportToGE_Execute);
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.AcceptButtonCaption = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.CancelButtonCaption = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Caption = "GBIF data";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Category = "OpenObject";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ConfirmationMessage = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Id =
                "popupWindowShowAction_DowloadAndMapGbifForExtent";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ImageName = "gbif_icon";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.PaintStyle =
                DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.TargetViewId = "xxx";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ToolTip =
                "Downloads GBIF data for current map extent";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_DowloadAndMapGbifForExtent_CustomizePopupWindowParams);
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_DowloadAndMapGbifForExtent_Execute);
            this.simpleAction_MapLocalities.Caption = "Map localities";
            this.simpleAction_MapLocalities.Category = "View";
            this.simpleAction_MapLocalities.ConfirmationMessage = null;
            this.simpleAction_MapLocalities.Id = "simpleAction_MapLocalities";
            this.simpleAction_MapLocalities.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_MapLocalities.ToolTip = null;
            this.simpleAction_MapLocalities.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_MapLocalities_Execute);
            this.simpleAction_ShowMapObject.Caption = "Show";
            this.simpleAction_ShowMapObject.Category = "View";
            this.simpleAction_ShowMapObject.ConfirmationMessage = null;
            this.simpleAction_ShowMapObject.Id = "simpleAction_ShowMapObject";
            this.simpleAction_ShowMapObject.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_ShowMapObject.ToolTip = null;
            this.simpleAction_ShowMapObject.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_ShowMapObject_Execute);
            this.popupWindowShowAction_NewLocality.AcceptButtonCaption = null;
            this.popupWindowShowAction_NewLocality.CancelButtonCaption = null;
            this.popupWindowShowAction_NewLocality.Caption = "New locality";
            this.popupWindowShowAction_NewLocality.Category = "View";
            this.popupWindowShowAction_NewLocality.ConfirmationMessage = null;
            this.popupWindowShowAction_NewLocality.Id = "popupWindowShowAction_NewLocality";
            this.popupWindowShowAction_NewLocality.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_NewLocality.ToolTip = null;
            this.popupWindowShowAction_NewLocality.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_NewLocality_CustomizePopupWindowParams);
            this.popupWindowShowAction_NewLocality.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_NewLocality_Execute);
            this.simpleAction_MapShowUnits.Caption = "Map units";
            this.simpleAction_MapShowUnits.Category = "View";
            this.simpleAction_MapShowUnits.ConfirmationMessage = null;
            this.simpleAction_MapShowUnits.Id = "simpleAction_MapShowUnits";
            this.simpleAction_MapShowUnits.ToolTip = null;
            this.simpleAction_MapShowUnits.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_MapUnits_Execute);
            this.popupWindowShowAction_NewUnit.AcceptButtonCaption = null;
            this.popupWindowShowAction_NewUnit.CancelButtonCaption = null;
            this.popupWindowShowAction_NewUnit.Caption = "New unit";
            this.popupWindowShowAction_NewUnit.Category = "View";
            this.popupWindowShowAction_NewUnit.ConfirmationMessage = null;
            this.popupWindowShowAction_NewUnit.Id = "popupWindowShowAction_NewUnit";
            this.popupWindowShowAction_NewUnit.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_NewUnit.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.popupWindowShowAction_NewUnit.ToolTip = null;
            this.popupWindowShowAction_NewUnit.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.popupWindowShowAction_NewUnit.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_NewUnit_CustomizePopupWindowParams);
            this.popupWindowShowAction_NewUnit.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_NewUnit_Execute);
            this.simpleAction_RemoveLayer.Caption = "Remove layer";
            this.simpleAction_RemoveLayer.Category = "View";
            this.simpleAction_RemoveLayer.ConfirmationMessage = "Remove selected layer?";
            this.simpleAction_RemoveLayer.Id = "simpleAction_RemoveLayer";
            this.simpleAction_RemoveLayer.ImageName = "Action_LinkUnlink_Unlink";
            this.simpleAction_RemoveLayer.ToolTip = null;
            this.simpleAction_RemoveLayer.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_RemoveLayer_Execute);
            this.simpleAction_GetLocalitiesFromView.Caption = "Get localities";
            this.simpleAction_GetLocalitiesFromView.Category = "View";
            this.simpleAction_GetLocalitiesFromView.ConfirmationMessage = null;
            this.simpleAction_GetLocalitiesFromView.Id = "simpleAction_GetLocalitiesFromView";
            this.simpleAction_GetLocalitiesFromView.ToolTip =
                "Shows a table that lists all localities that fit in the current map view";
            this.simpleAction_GetLocalitiesFromView.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_GetLocalitiesFromView_Execute);
            this.parametrizedAction_FindAndMapLocalities.Caption = " ";
            this.parametrizedAction_FindAndMapLocalities.Category = "Tools";
            this.parametrizedAction_FindAndMapLocalities.ConfirmationMessage = null;
            this.parametrizedAction_FindAndMapLocalities.Id = "parametrizedAction_FindAndMapLocalities";
            this.parametrizedAction_FindAndMapLocalities.NullValuePrompt = null;
            this.parametrizedAction_FindAndMapLocalities.ShortCaption = "Find locality";
            this.parametrizedAction_FindAndMapLocalities.ToolTip = null;
            this.parametrizedAction_FindAndMapLocalities.Execute +=
                new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(
                    this.parametrizedAction_FindAndMapLocalities_Execute);
            this.singleChoiceAction_ViewMode.Caption = "View";
            this.singleChoiceAction_ViewMode.Category = "View";
            this.singleChoiceAction_ViewMode.ConfirmationMessage = null;
            this.singleChoiceAction_ViewMode.Id = "singleChoiceAction_ViewMode";
            choiceActionItem1.Caption = "2D";
            choiceActionItem1.Id = "2D";
            choiceActionItem1.ImageName = null;
            choiceActionItem1.Shortcut = null;
            choiceActionItem1.ToolTip = null;
            choiceActionItem2.Caption = "3D";
            choiceActionItem2.Id = "3D";
            choiceActionItem2.ImageName = null;
            choiceActionItem2.Shortcut = null;
            choiceActionItem2.ToolTip = null;
            this.singleChoiceAction_ViewMode.Items.Add(choiceActionItem1);
            this.singleChoiceAction_ViewMode.Items.Add(choiceActionItem2);
            this.singleChoiceAction_ViewMode.ShowItemsOnClick = true;
            this.singleChoiceAction_ViewMode.TargetViewId = "xx";
            this.singleChoiceAction_ViewMode.ToolTip = null;
            this.singleChoiceAction_ViewMode.Execute +=
                new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(
                    this.singleChoiceAction_ViewMode_Execute);
            this.simpleAction_Select.Caption = "Select";
            this.simpleAction_Select.Category = "View";
            this.simpleAction_Select.ConfirmationMessage = null;
            this.simpleAction_Select.Id = "simpleAction_Select";
            this.simpleAction_Select.ImageName = "SelectLocate";
            this.simpleAction_Select.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.simpleAction_Select.ToolTip = null;
            this.simpleAction_Select.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Select_Execute);
            this.simpleAction_Zoom.Caption = "Zoom";
            this.simpleAction_Zoom.Category = "View";
            this.simpleAction_Zoom.ConfirmationMessage = null;
            this.simpleAction_Zoom.Id = "simpleAction_Zoom";
            this.simpleAction_Zoom.ImageName = "Zoom";
            this.simpleAction_Zoom.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.simpleAction_Zoom.ToolTip = null;
            this.simpleAction_Zoom.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Zoom_Execute);
            this.simpleAction_Edit.Caption = "Edit";
            this.simpleAction_Edit.Category = "View";
            this.simpleAction_Edit.ConfirmationMessage = null;
            this.simpleAction_Edit.Id = "simpleAction_Edit";
            this.simpleAction_Edit.ImageName = "Edit";
            this.simpleAction_Edit.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.simpleAction_Edit.ToolTip = null;
            this.simpleAction_Edit.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Edit_Execute);
            this.simpleAction_Pan.Caption = "Pan";
            this.simpleAction_Pan.Category = "View";
            this.simpleAction_Pan.ConfirmationMessage = null;
            this.simpleAction_Pan.Id = "simpleAction_Pan";
            this.simpleAction_Pan.ImageName = "DragEx";
            this.simpleAction_Pan.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.simpleAction_Pan.ToolTip = null;
            this.simpleAction_Pan.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Pan_Execute);
            this.popupWindowShowAction_ExportToVector.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportToVector.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportToVector.Caption = "Export to vector";
            this.popupWindowShowAction_ExportToVector.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportToVector.Id = "popupWindowShowAction_ExportToVector";
            this.popupWindowShowAction_ExportToVector.ToolTip = null;
            this.popupWindowShowAction_ExportToVector.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_ExportToVector_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportToVector.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_ExportToVector_Execute);
            this.Actions.Add(this.simpleAction_ExportVectorLayer);
            this.Actions.Add(this.popupWindowShowAction_ExportToImage);
            this.Actions.Add(this.simpleAction_MapFullExtent);
            this.Actions.Add(this.simpleAction_AddLayer);
            this.Actions.Add(this.parametrizedAction_WMS);
            this.Actions.Add(this.simpleAction_LayerExtent);
            this.Actions.Add(this.simpleAction_ExportToGE);
            this.Actions.Add(this.popupWindowShowAction_DowloadAndMapGbifForExtent);
            this.Actions.Add(this.simpleAction_MapLocalities);
            this.Actions.Add(this.simpleAction_ShowMapObject);
            this.Actions.Add(this.popupWindowShowAction_NewLocality);
            this.Actions.Add(this.simpleAction_MapShowUnits);
            this.Actions.Add(this.popupWindowShowAction_NewUnit);
            this.Actions.Add(this.simpleAction_RemoveLayer);
            this.Actions.Add(this.simpleAction_GetLocalitiesFromView);
            this.Actions.Add(this.parametrizedAction_FindAndMapLocalities);
            this.Actions.Add(this.singleChoiceAction_ViewMode);
            this.Actions.Add(this.simpleAction_Select);
            this.Actions.Add(this.simpleAction_Zoom);
            this.Actions.Add(this.simpleAction_Edit);
            this.Actions.Add(this.simpleAction_Pan);
            this.Actions.Add(this.popupWindowShowAction_ExportToVector);
            this.TargetObjectType = typeof(EarthCape.Module.GIS.MapWindow);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ExportVectorLayer;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportToImage;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MapFullExtent;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_AddLayer;
        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_WMS;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_LayerExtent;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ExportToGE;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DowloadAndMapGbifForExtent;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MapLocalities;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowMapObject;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_NewLocality;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MapShowUnits;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_NewUnit;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_RemoveLayer;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GetLocalitiesFromView;
        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_FindAndMapLocalities;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_ViewMode;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Select;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Zoom;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Edit;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Pan;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportToVector;
    }
}
