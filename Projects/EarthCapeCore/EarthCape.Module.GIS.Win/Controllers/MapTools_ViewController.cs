using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using TatukGIS.NDK;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System.Windows.Forms;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapTools_ViewController : ViewController
    {
        public MapTools_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_CreateGrid_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            MapTools_CreateGrid pms=(MapTools_CreateGrid)(e.PopupWindow.View.CurrentObject);
            IObjectSpace os = View.ObjectSpace;
            double xmin = gis.VisibleExtent.XMin;
            double xmax = gis.VisibleExtent.XMax;
            double ymin = gis.VisibleExtent.YMin;
            double ymax = gis.VisibleExtent.YMax;

            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Localities");
            TGIS_LayerVector ll_objects = null;
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Localities";
                gis.Add(ll);
            }
            GISHelper.AddFields(ll);
            GISHelper.LayerParams(ll);
            if (pms.ClipToSelectedObjects)
            {
                if (legend.GIS_Layer is TGIS_LayerVector)
                    if ((legend.GIS_Layer as TGIS_LayerVector).GetSelectedCount() > 0)
                    { ll_objects = (TGIS_LayerVector)legend.GIS_Layer; }
                TGIS_Shape shp_sel = ll_objects.FindFirst(ll_objects.Extent, "GIS_Selected = True");
                while (shp_sel != null)
                {
                    GISHelper.GenerateGridForExtent(shp_sel.Extent, ll, os, pms.Width, pms.Height, shp_sel.GetField(pms.SelectedObjectsNameField).ToString(),shp_sel,pms.MarginCells,pms.ClipToShape);
                    shp_sel = ll_objects.FindNext();
                }
            }
            else
            {
                int xcells = Convert.ToInt32((xmax - xmin) / pms.Width);
                int ycells = Convert.ToInt32((ymax - ymin) / pms.Height);
                double xshift = xmin;
                double yshift = ymin;
                if (MessageBox.Show(String.Format("Create {0} cells?", xcells * ycells), "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int count = 0;
                    for (int i = 0; i < ycells - 1; i++)
                    {
                        for (int j = 0; j < xcells - 1; j++)
                        {
                            count += 1;
                            TGIS_ShapePolygon shp = new TGIS_ShapePolygon();

                            shp.AddPart();
                            TGIS_Point ptg = new TGIS_Point(xshift, yshift);
                            shp.AddPoint(ptg);
                            ptg = new TGIS_Point(xshift + pms.Width, yshift);
                            shp.AddPoint(ptg);
                            ptg = new TGIS_Point(xshift + pms.Width, yshift + pms.Height);
                            shp.AddPoint(ptg);
                            ptg = new TGIS_Point(xshift, yshift + pms.Height);
                            shp.AddPoint(ptg);
                            bool DoNotAddShape = false;
                            string shp_name = String.Format("{0}-{1}", j, i);
                            if (ll_objects != null)
                            {
                                DoNotAddShape = true;
                                TGIS_Shape shp_sel = ll_objects.FindFirst(ll_objects.Extent, "GIS_Selected = True");
                                while (shp_sel != null)
                                {
                                    if ((shp_sel.IsCommonPoint(shp)))
                                    //     if ((shp_sel.Overlap(shp)) || (shp_sel.Cross(shp)) || (shp_sel.Contains(shp)))
                                    {
                                        shp_name = String.Format("{0}-{1}", shp_sel.GetField(pms.SelectedObjectsNameField), count);
                                        DoNotAddShape = false;
                                        break;
                                    }
                                    shp_sel = ll_objects.FindNext();
                                }
                            }
                            if (!DoNotAddShape)
                            {
                                Locality loc = os.CreateObject<Locality>();
                                loc.Name = shp_name;
                                ll.AddShape(shp).SetField("Label", loc.Name);
                                loc.WKT = shp.ExportToWKT();
                                loc.Save();
                                GISHelper.UpdateAreaAndLengthFromWKT(loc);
                            }

                            xshift = xshift + pms.Width;
                        }
                        xshift = xmin;
                        yshift = yshift + pms.Height;
                    }
                    legend.GIS_Layer = ll;
                    gis.VisibleExtent = ll.Extent;
                    gis.Invalidate();
                }
            }
        }

        private void popupWindowShowAction_CreateGrid_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            MapTools_CreateGrid obj = os.CreateObject<MapTools_CreateGrid>();
            e.View = Application.CreateDetailView(os, obj);
       
        }
    }
}
