using System;

using DevExpress.ExpressApp;

using DevExpress.Data.Filtering;
using System.Windows.Forms;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using EarthCape.Module.GIS;
using TatukGIS.NDK;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapProject_ViewController : ViewController
    {
        public MapProject_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(MapProject);
           }
 
        public void SaveEdits()
        {
            TGIS_ViewerWnd gis = null;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            if (mapview == null) return;
            gis = GISHelperWin.GetGisWndInMapWindow(mapview);



            if (MessageBox.Show("Save edits?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                gis.SaveAll();
                foreach (TGIS_Shape shape in shapes)
                {
                    if (true)
                    {
                        BaseObject obj = null;
                        if (shape.GetField("ObjectType").ToString() == "Locality")
                            obj = ObjectSpace.FindObject<Locality>(new BinaryOperator("Oid", shape.Tag.ToString()));
                        if (shape.GetField("ObjectType").ToString() == "Unit")
                            obj = ObjectSpace.FindObject<Unit>(new BinaryOperator("Oid", shape.Tag.ToString()));
                        if (shape.GetField("ObjectType").ToString() == "LocalityVisit")
                            obj = ObjectSpace.FindObject<Unit>(new BinaryOperator("LocalityVisit", shape.Tag.ToString()));
                        if (obj == null)
                        {
                            return;
                        }
                        if (obj != null)
                        {
                            if (obj is IWKT)
                            {
                                IWKT iwkt = (IWKT)obj;
                                string wkt = shape.ExportToWKT();
                                iwkt.WKT = wkt;
                            }
                        }
                    }
                }
                gis.Invalidate(); //checking
            }
            else
            {
                //  editlayer.RevertAll();
                //editlayer.Viewer.Editor.();
                gis.RevertAll();
                gis.Editor.EndEdit();

            }
            shapes.Clear();
            /*
          TGIS_ViewerWnd gis = null;
            DetailView mapview = MapHostView((DetailView)View);
            if (mapview == null) return;
            gis = GISHelperWin.GetGisWnd(mapview);
          

            gis.SaveAll();
            //editlayer.SaveAll();
            foreach (TGIS_LayerAbstract item in gis.Items)
            {
                if (item is TGIS_LayerVector)
                {
                    ((TGIS_LayerVector)item).Items
                }
            }
            if (editlayer.Name == "IWKT")
            {
                TGIS_Shape shp = (TGIS_Shape)editlayer.Items[0];
                string wkt = shp.ExportToWKT();
                IWKT loc = (IWKT)(((NestedFrame)Frame).ViewItem.CurrentObject);
                loc.WKT = wkt;
                loc.Save();
            }

            return;

            if (MessageBox.Show("Save edits?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                editlayer.SaveAll();
                if (editlayer.Name == "IWKT")
                {
                    TGIS_Shape shp = (TGIS_Shape)editlayer.Items[0];
                    string wkt = shp.ExportToWKT();
                    IWKT loc = (IWKT)(((NestedFrame)Frame).ViewItem.CurrentObject);
                    loc.WKT = wkt;
                    loc.Save();
                }
                editlayer.Viewer.Update(); //checking
            }
            else
            {
                //  editlayer.RevertAll();
                //editlayer.Viewer.Editor.();
                editlayer.Viewer.Editor.RevertShape();
                editlayer.Viewer.Editor.EndEdit();

            }*/
        }
   
        List<TGIS_Shape> shapes = new List<TGIS_Shape>();


        TGIS_ViewerWnd gis_wnd = null;
        private void simpleAction_LoadMap_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            MapWindow mapw = obs.CreateObject<MapWindow>();
            mapw.MapProject = obs.FindObject<MapProject>(new BinaryOperator("Oid", ((MapProject)View.CurrentObject).Oid));
            DetailView dv = Application.CreateDetailView(obs, mapw, true);
            dv.ControlsCreated += new EventHandler(dv_ControlsCreated);
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.MdiChild;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
            e.ShowViewParameters.CreatedView = dv;
            e.ShowViewParameters.CreateAllControllers = true;
        }

    

        void dv_ControlsCreated(object sender, EventArgs e)
        {
            DetailView dv = (DetailView)sender;
            gis_wnd=GISHelperWin.GetGisWndInMapWindow(dv);
          //  gis_wnd.EditorChange += gis_EditorChange;
            GISHelperWin.OpenMapProject(gis_wnd, (MapProject)View.CurrentObject);
        }   
    }
}
