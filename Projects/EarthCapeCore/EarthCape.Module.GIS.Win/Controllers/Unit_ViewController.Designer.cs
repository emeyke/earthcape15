namespace EarthCape.Module.GIS.Win
{
    partial class Unit_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_MapTracks = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_MapTracks
            // 
            this.popupWindowShowAction_MapTracks.AcceptButtonCaption = null;
            this.popupWindowShowAction_MapTracks.CancelButtonCaption = null;
            this.popupWindowShowAction_MapTracks.Caption = "Map tracks";
            this.popupWindowShowAction_MapTracks.Category = "Tools";
            this.popupWindowShowAction_MapTracks.ConfirmationMessage = null;
            this.popupWindowShowAction_MapTracks.Id = "popupWindowShowAction_MapTracks";
            this.popupWindowShowAction_MapTracks.ImageName = null;
            this.popupWindowShowAction_MapTracks.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_MapTracks.Shortcut = null;
            this.popupWindowShowAction_MapTracks.Tag = null;
            this.popupWindowShowAction_MapTracks.TargetObjectsCriteria = null;
            this.popupWindowShowAction_MapTracks.TargetViewId = null;
            this.popupWindowShowAction_MapTracks.ToolTip = null;
            this.popupWindowShowAction_MapTracks.TypeOfView = null;
            this.popupWindowShowAction_MapTracks.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_MapTracks_CustomizePopupWindowParams);
            this.popupWindowShowAction_MapTracks.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_MapTracks_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_MapTracks;
    }
}
