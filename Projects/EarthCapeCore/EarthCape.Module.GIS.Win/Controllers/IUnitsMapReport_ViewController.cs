
 using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Reports;
using DevExpress.XtraReports.UI;
using TatukGIS.NDK;
using System.Drawing;
 using System.Drawing.Imaging;

using DevExpress.Data.Filtering;
using System.Collections.ObjectModel;
using DevExpress.XtraPrinting;
 using DevExpress.Xpo;
 using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
 using EarthCape.Module.Std;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.ReportsV2.Win;

namespace EarthCape.Module.GIS.Win
{
    public class IUnitsMapReport_ViewController : ViewController 
    {
        private void FindMap(XRControl control)
        {
            foreach (XRControl item in control.Controls)
            {
                if (item is XRMapPictureBox)
                    item.BeforePrint += map_BeforePrint;
                else
                    if (item.Controls.Count > 0)
                        FindMap(item);
            }
        }
        private void Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRControl control = (XRControl)sender;
            while (!(control is XafReport) && (control != null))
            {
                control = control.Parent;
            }
            if (!(control is XafReport)) return;
            XafReport rep = (XafReport)(control);
            FindMap(control);
            XRMapPictureBox map = (XRMapPictureBox)rep.FindControl("mapPictureBox1", false);
            if (map != null)
            {
                map.BeforePrint += map_BeforePrint;
            }
                    }
        private void controller_CustomShowPreview(object sender, CustomShowPreviewEventArgs e)
        {
    //        e.Report.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Report_BeforePrint);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
           WinReportServiceController controller = Frame.GetController<WinReportServiceController>();
            if (controller != null)
            {
                controller.CustomShowPreview += new EventHandler<CustomShowPreviewEventArgs>(controller_CustomShowPreview);
               //    controller.CreateCustomDesignForm +=new EventHandler<CreateCustomDesignFormEventArgs>(controller_CreateCustomDesignForm); 
            }
        }
    /*   protected override void OnDeactivating()
        {
            WinReportServiceController controller = Frame.GetController<WinReportServiceController>();
            if (controller != null)
            {
                controller.CustomShowPreview -= new EventHandler<CustomShowPreviewEventArgs>(controller_CustomShowPreview);
            }
            base.OnDeactivating();
        }*/
        
        
        public IUnitsMapReport_ViewController()
        {
            
        }
     
        void map_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //SecurityUser p = ((ObjectSpace)View.ObjectSpace).GetObjectByKey(SecuritySystem.CurrentUser.GetType(), ((ObjectSpace)View.ObjectSpace).GetKeyValue(SecuritySystem.CurrentUser)) as SecurityUser;
            XRControl control = (XRControl)sender;
            XRMapPictureBox mapBox = ((XRMapPictureBox)sender);
            mapBox.Sizing = ImageSizeMode.StretchImage;
            while (!(control is XafReport) && (control != null))
            {
                control = control.Parent;
            }
            if (!(control is XafReport)) return;
            XafReport rep = (XafReport)(control);
            object obj = rep.GetCurrentRow();
            if (obj == null) return;
            using (TGIS_ViewerWnd gis = new TGIS_ViewerWnd())
            {
                MapProject map = null;
                gis.Width = mapBox.Width;
                gis.Height = mapBox.Height;
                TGIS_LayerVector ll = null;
                TGIS_LayerVector llSummary = null;
                Project Project = null;
                if (Value_WindowController.Instance()!=null)
                    Project = Value_WindowController.Instance().GetGroup((IObjectSpace)View.ObjectSpace);              
                if (Project != null)
                    if (Project.Map != null)
                    {
                        map = Project.Map;
                        GISHelperWin.OpenMapProject(gis, map);
                      }
                if (typeof(IUnits).IsAssignableFrom(obj.GetType()))
                {
                    ll = new TGIS_LayerVector();
                    gis.Add(ll);
                    int maxcount = 0;
                    Collection<String> coll = new Collection<String>();
                    IUnits obj1 = (IUnits)obj;// View.ObjectSpace.GetKeyValue(((BaseObject)obj));
                    using (XPCollection<Unit> units = new XPCollection<Unit>(obj1.Units, CriteriaOperator.Parse(mapBox.UnitCriteria)))
                    {
                        /* if (!String.IsNullOrEmpty(mapBox.UnitCriteria))
                                            {
                    		                        
                                               obj1.Units.Load();
                                                obj1.Units.Criteria = CriteriaOperator.Parse(mapBox.UnitCriteria);// CriteriaOperator.And(((IUnits)obj).Units.Criteria, CriteriaOperator.Parse(mapBox.UnitCriteria));
                                            } */
                        foreach (Unit ub in units)
                            //obj1.Units)
                            if (map != null)
                            {
                                GISHelper.PlotIWKT( ll, llSummary, ub,Project,false);
                                GISHelper.ApplyParams(llSummary, maxcount);
                            }
                            else
                                GISHelper.PlotIWKT(ll, null, ub,Project,false);
                    }
                    // GISHelper.MapUnits(((IUnits)obj).Units, ll, llSummary, user.MasterObject.DefaultMap.SummaryType);
                }
                
                if (ll != null)
                {
                    ll.Params = GISHelper.GISRandomizeParams();
                    //gis.Update();
                    //gis.VisibleExtent = ll.Extent;
                    Bitmap _bmp = new Bitmap(mapBox.Width, mapBox.Height, PixelFormat.Format24bppRgb);
                    //_bmp.Save("c:\\users\\evgeniy\\documents\\temp\\_bmp.bmp");
                    gis.PrintBmp(ref _bmp);
                    mapBox.Image = _bmp;
                    // _bmp.Dispose();
                }
                else
                    mapBox.Image = null;
            }
            //_bmp.Dispose();
   
        }
    }
}
