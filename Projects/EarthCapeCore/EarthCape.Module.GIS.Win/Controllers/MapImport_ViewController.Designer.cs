namespace EarthCape.Module.GIS.Win
{
    partial class MapImport_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ImportVectorMap = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ImportVector =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ImportVectorMap.Caption = "Import vector map";
            this.simpleAction_ImportVectorMap.Category = "Tools";
            this.simpleAction_ImportVectorMap.ConfirmationMessage = null;
            this.simpleAction_ImportVectorMap.Id = "simpleAction_ImportVectorMap";
            this.simpleAction_ImportVectorMap.TargetObjectType = typeof(EarthCape.Module.Core.Locality);
            this.simpleAction_ImportVectorMap.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_ImportVectorMap.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_ImportVectorMap.ToolTip =
                ("Imports locality objects from vector file and assigns them to selected group/proj" + "" + "ect");
            this.simpleAction_ImportVectorMap.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_ImportVectorMap.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction1_Execute);
            this.popupWindowShowAction_ImportVector.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportVector.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportVector.Caption = "Import from vector";
            this.popupWindowShowAction_ImportVector.Category = "Tools";
            this.popupWindowShowAction_ImportVector.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportVector.Id = "popupWindowShowAction_ImportVector";
            this.popupWindowShowAction_ImportVector.TargetViewId = "x";
            this.popupWindowShowAction_ImportVector.ToolTip = null;
            this.popupWindowShowAction_ImportVector.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_ImportVector_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportVector.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_ImportVector_Execute);
            this.Actions.Add(this.simpleAction_ImportVectorMap);
            this.Actions.Add(this.popupWindowShowAction_ImportVector);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImportVectorMap;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportVector;
   }
}
