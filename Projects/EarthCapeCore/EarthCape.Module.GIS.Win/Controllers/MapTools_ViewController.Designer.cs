namespace EarthCape.Module.GIS.Win
{
    partial class MapTools_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_CreateGrid = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_CreateGrid
            // 
            this.popupWindowShowAction_CreateGrid.AcceptButtonCaption = null;
            this.popupWindowShowAction_CreateGrid.CancelButtonCaption = null;
            this.popupWindowShowAction_CreateGrid.Caption = "Create grid";
            this.popupWindowShowAction_CreateGrid.Category = "Tools";
            this.popupWindowShowAction_CreateGrid.ConfirmationMessage = null;
            this.popupWindowShowAction_CreateGrid.Id = "popupWindowShowAction_CreateGrid";
            this.popupWindowShowAction_CreateGrid.ImageName = null;
            this.popupWindowShowAction_CreateGrid.Shortcut = null;
            this.popupWindowShowAction_CreateGrid.Tag = null;
            this.popupWindowShowAction_CreateGrid.TargetObjectsCriteria = null;
            this.popupWindowShowAction_CreateGrid.TargetViewId = null;
            this.popupWindowShowAction_CreateGrid.ToolTip = "Creates vector grid with user defined parameters and stores each cell as a locali" +
                "ty";
            this.popupWindowShowAction_CreateGrid.TypeOfView = null;
            this.popupWindowShowAction_CreateGrid.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_CreateGrid_CustomizePopupWindowParams);
            this.popupWindowShowAction_CreateGrid.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_CreateGrid_Execute);
            // 
            // MapTools_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Module.GIS.MapWindow);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_CreateGrid;
    }
}
