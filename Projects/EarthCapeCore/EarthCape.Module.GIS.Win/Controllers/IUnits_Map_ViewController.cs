﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Utils.Behaviors;
using DevExpress.Utils.DragDrop;
using EarthCape.Module.Core;
using TatukGIS.NDK;
using TatukGIS.NDK.WinForms;

namespace EarthCape.Module.GIS.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IUnits_Map_ViewController : ViewController
    {
        public IUnits_Map_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        IObjectSpace uow = null;
        protected override void OnActivated()
        {
            base.OnActivated();
            uow = View.ObjectSpace;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
            IUnitsMapHook((DetailView)View);
            MapProject map = null;
            // if (String.IsNullOrEmpty(((MapProject)View.CurrentObject).GetWKT())) return;
            DetailView view = GISHelperWin.GetMapWindowDetalView((DetailView)View);
            if (view == null) return;
            gis = GISHelperWin.GetGisWndInMapWindow(view);
            if (gis == null) return;
            if (map == null)
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultMapProject"]))
                {
                    map = View.ObjectSpace.FindObject<MapProject>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultMapProject"]));
                }
            if (map != null)
                GISHelperWin.OpenMapProject(gis, map);
            UpdateControl();
            View.CurrentObjectChanged += View_CurrentObjectChanged;
        }
        private void UpdateControl()
        {
            if (View.CurrentObject == null) return;
            if (!(View.CurrentObject is IUnits)) return;
            IUnits iunits = (IUnits)View.CurrentObject;
            Project project = null;
            if (iunits is IProjectObject)
                project = (iunits as IProjectObject).Project;
            if (iunits is EarthCape.Module.Core.IDatasetObject)
                if ((iunits as EarthCape.Module.Core.IDatasetObject).Dataset != null)
                    project = (iunits as EarthCape.Module.Core.IDatasetObject).Dataset.Project;

            // if (map != null) { ((MapWindow)view.CurrentObject).MapProject = map; }



            var units = ((IUnits)View.CurrentObject).Units;// uow.CreateCollection(((IUnits)this).Units., cr);// new XPCollection<Locality>(uow, cr);
            GISHelperWin.UpdateMap(View.ObjectSpace, gis, null, units);
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            GISHelper.LayerParams(ll);
            ll.RecalcExtent();
            gis.VisibleExtent = ll.Extent;
            gis.Refresh();
        }
        TGIS_ViewerWnd gis = null;
        private void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            ((TGIS_LayerVector)(gis.Get("Units"))).Items.Clear();
            UpdateControl();
        }

        private void IUnitsMapHook(DetailView view)
        {
            DevExpress.ExpressApp.ListView units_view = null;
            foreach (ViewItem item in (view).Items)
            {
                ListPropertyEditor units_editor = item as ListPropertyEditor;
                if (units_editor != null)
                {
                    if (units_editor.ListView != null)
                    {
                        object listEditor = units_editor.ListView.Editor;
                        if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                        {
                            units_view = units_editor.ListView;
                        }
                    }
                }
            }
            if (units_view != null)
            {
                units_view.SelectionChanged += units_view_CurrentObjectChanged;
            }
        }


        void units_view_CurrentObjectChanged(object sender, EventArgs e)
        {
                if (gis == null) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            if (ll == null) return;
            ll.DeselectAll();
            foreach (Unit item in (sender as DevExpress.ExpressApp.ListView).SelectedObjects)
            {
                TGIS_Shape shp = ll.FindFirst(gis.VisibleExtent, String.Format("ECID='{0}'", item.Oid.ToString()));
                if (shp != null)
                    shp.IsSelected = true;
            }
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
