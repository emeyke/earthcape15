using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK;
using System.Drawing;
using System.IO;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;

using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraGrid.Views.Grid;
using System.Collections;
using System.Collections.ObjectModel;
using TatukGIS.NDK.WinForms;
using EarthCape.Taxonomy;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using System.Configuration;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapWindow_ViewController : ViewController
    {
        bool ctrlPressed = false;
        //   private TGIS_ShapeArc currShape;
        // TGIS_LayerVector lgps;
        IObjectSpace uow = null;
        bool CreateNewUnitShapeFlag = false;
        Unit CreateNewUnitShapeUnit = null;
        MapNewUnit CreateNewUnitShapeMapUnit = null;


        public MapWindow_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            Activated += MapWindow_ViewController_Activated;
            //Deactivating += MapWindow_ViewController_Deactivating;
        }

        void MapWindow_ViewController_Deactivating(object sender, EventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            View.ControlsCreated -= View_ControlsCreated;
        }

        void MapWindow_ViewController_Activated(object sender, EventArgs e)
        {
            uow = View.ObjectSpace;
            DetailView parentview = null;
            if (Frame is NestedFrame)
                parentview = (DetailView)((NestedFrame)Frame).ViewItem.View;
            else
                parentview = (DetailView)View;
            View.ControlsCreated += View_ControlsCreated;
            if (parentview != null)
            {
                parentview.ObjectSpace.Committing += obs_Committing;
                parentview.ObjectSpace.RollingBack += new EventHandler<System.ComponentModel.CancelEventArgs>(ObjectSpace_RollingBack);
                parentview.ObjectSpace.Committed += new EventHandler(ObjectSpace_Committed);
            }

            /*  TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
                      if (gis != null)
                      {
                          gis.MouseClick += gis_MouseClick;
                          gis.MouseDoubleFClick += gis_MouseDoubleClick;
                      }*/
            //simpleAction_GISstopediting.Active.SetItemValue("isediting", false);
            //singleChoiceAction_MapMode.SelectedIndex = 1;
            //cap = popupWindowShowAction_RecordUnitByName.Caption;
            //popupWindowShowAction_RecordUnitByName.Active.SetItemValue("Active", (Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace)!=null));

        }

        void ObjectSpace_RollingBack(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DetailView mapview = (DetailView)View;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow(mapview);
            gis.Editor.RevertShape();
        }

        void ObjectSpace_Committed(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void WriteConfig()
        {
            if (View.CurrentObject == null) return;
            if (((MapWindow)View.CurrentObject).MapProject == null) return;
            TGIS_ViewerWnd gis_wnd = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            string tempDirectory = Path.GetTempPath();
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
            //// gis.ProjectFile.UseRelativePath = false;
            gis_wnd.SaveProjectAs(tempFileName, false);
            // gis.ProjectFile.UseRelativePath = false;
            // gis.SaveProject();
            // gis.
            using (StreamReader streamReader = new StreamReader(tempFileName))
            {
                ((MapWindow)View.CurrentObject).MapProject.ProjectConfig = streamReader.ReadToEnd();
                ((MapWindow)View.CurrentObject).MapProject.Save();
                streamReader.Close();
            }
        }
        void obs_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // e.Cancel = true;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis != null)
            {
                TGIS_LayerVector ll = null;
                if (gis.Get("Stores") != null)
                {
                    ll = (TGIS_LayerVector)gis.Get("Stores");
                }
                if (gis.Get("Localities") != null)
                {
                    ll = (TGIS_LayerVector)gis.Get("Localities");
                }
                if (gis.Get("Units") != null)
                {
                    ll = (TGIS_LayerVector)gis.Get("Units");
                }
                if (gis.Get("Projects") != null)
                {
                    ll = (TGIS_LayerVector)gis.Get("Projects");
                }
                if (ll == null) return;
                TGIS_Shape shp = ll.FindFirst(ll.Extent);
                while (shp != null)
                {
                    if (Convert.ToBoolean(shp.GetField("Edited")) == true)
                        if (Convert.ToBoolean(shp.GetField("OwnGeometry")) == true)
                            UpdateEditedShape(shp, (IObjectSpace)sender);
                    shp = ll.FindNext();
                }
                WriteConfig();
                gis.Mode = TGIS_ViewerMode.gisSelect;
                if (gis.Editor != null)
                    gis.Editor.EndEdit();
                //singleChoiceAction_MapMode.SelectedIndex = 0;
                gis.Mode = TGIS_ViewerMode.gisSelect;
                gis.Invalidate();
            }
        }
        //string cap = "";
        void legend_LayerSelect(object _sender, TGIS_LayerEventArgs _e)
        {
            if (_e.Layer == null) return;
            simpleAction_ExportVectorLayer.Active[""] = (_e.Layer is TGIS_LayerVector);
            simpleAction_ExportToGE.Active[""] = (_e.Layer is TGIS_LayerVector);
            simpleAction_RemoveLayer.Active[""] = (_e.Layer != null);
        }

        void gis_MouseClick(object sender, MouseEventArgs e)
        {
            // separate from mousedown for editing so that the shape is selected properly
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);

            //clipboard coordinates
            Point pt = new Point(e.X, e.Y);
            TGIS_Point ptg = gis.ScreenToMap(pt);
            // Clipboard.SetText(String.Format("POINT({0} {1})",Math.Round(ptg.X,2),Math.Round(ptg.Y,2)));

            if (legend.GIS_Layer == null) return;
            //if (gis.Get("IWKT") != null)
            //    legend.GIS_Layer = gis.Get("IWKT");
            if (!((gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector))) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name);
            TGIS_Shape shp = ll.Locate(ptg, 5 / gis.Zoom, true);
            if (gis.Mode == TGIS_ViewerMode.gisEdit)
            {
                if (shp != null)
                {
                    if (gis.Editor.CurrentShape == null)
                        if (Convert.ToBoolean(shp.GetField("OwnGeometry")) == true)
                            try
                            {
                                gis.Editor.EditShape(shp, 0);
                                shp.SetField("Edited", true);
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {

                            }
                        else
                        {
                            MessageBox.Show("This is not shape's own geometry (e.g. Unit geometry borrowed from Locality)");
                        }
                }
            }
        }

        void View_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            gis.MouseClick += gis_MouseClick;
            //  gis.MouseDoubleClick += new MouseEventHandler(gis_MouseDoubleClick);
            gis.MouseDoubleClick += gis_MouseDoubleClick;
            gis.LayerAdd += new TatukGIS.NDK.TGIS_LayerEvent(gis_LayerAdd);
            gis.LayerDelete += gis_changed;
            gis.Editor.Change += new EventHandler(Editor_Change);
            gis.ZoomChange += gis_ZoomChange;
            gis.VisibleExtentChange += gis_ZoomChange;
            gis.KeyDownEx += new KeyEventHandler(gis_KeyDownEx);
            gis.KeyUpEx += new KeyEventHandler(gis_KeyUpEx);
            gis.MouseDown += new MouseEventHandler(gis_MouseDown);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            legend.Scroll += legend_Scroll;
            TGIS_ControlScale scale = GISHelperWin.GetGisScale((DetailView)View);
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            simpleAction_ExportVectorLayer.Active[""] = false; ;
            simpleAction_ExportToGE.Active[""] = false; ;
            //simpleAction_GPSLocateMe.Active[""] = false; ;

            if (scale != null)
                scale.GIS_Viewer = gis;
            if (legend != null)
                legend.GIS_Viewer = gis;
            if (legend != null)
            {
                legend.LayerParamsChange += gis_changed;
                if (legend.GIS_Layer != null)
                {
                    simpleAction_ExportVectorLayer.Active[""] = (gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector);
                    simpleAction_ExportToGE.Active[""] = (gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector);
                }
            }
          //  if ((Frame is NestedFrame) && (((NestedFrame)Frame).ViewItem.View.CurrentObject != null))
            {
                Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
                if (Project == null)
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultProjectName"]))
                    {
                        Project = View.ObjectSpace.FindObject<Project>(new BinaryOperator("Name", ConfigurationManager.AppSettings["DefaultProjectName"]));
                    }

                }
                if (Project != null)
                {
                    if (Project.Map != null)
                    {
                        if ((gis.Items.Count == 0))
                        {
                            GISHelperWin.OpenMapProject(gis, Project.Map);

                        }
                    }
                }
            }
            //   simpleAction_GPSLocateMe.Active[""] = (GPS != null);
    
        }

        

        void legend_Scroll(object sender, ScrollEventArgs e)
        {
            ((TGIS_ControlLegend)sender).Update();
        }

        void Editor_Change(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            DetailView view = null;
            if (Frame is NestedFrame)
                view = (DetailView)((NestedFrame)Frame).ViewItem.View;
            else
                view = (DetailView)View;
            //DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            if (gis.Editor == null) return;
            TGIS_Shape shp = ((TGIS_Shape)gis.Editor.CurrentShape);
            if (!shp.GeometryChanged) return;
            string oid = shp.GetField("ECID").ToString();
            Guid guid = new Guid(oid);
            if ((!String.IsNullOrEmpty(oid)))
                if (view != null)
                {
                    IWKT obj = null;
                    if (shp.GetField("ObjectType").ToString() == "Store")
                        obj = view.ObjectSpace.FindObject<Store>(new BinaryOperator("Oid", guid), true);
                    if (shp.GetField("ObjectType").ToString() == "Locality")
                        obj = view.ObjectSpace.FindObject<Locality>(new BinaryOperator("Oid", guid), true);
                    if (shp.GetField("ObjectType").ToString() == "Unit")
                        obj = view.ObjectSpace.FindObject<Unit>(new BinaryOperator("Oid", guid), true);
                    if (shp.GetField("ObjectType").ToString() == "Project")
                        obj = view.ObjectSpace.FindObject<Project>(new BinaryOperator("Oid", guid), true);
                    if (shp.GetField("ObjectType").ToString() == "LocalityVisit")
                        obj = view.ObjectSpace.FindObject<LocalityVisit>(new BinaryOperator("Oid", guid), true);
                    if (obj != null)
                    {
                        //((XPObjectSpace)view.ObjectSpace).SetModified(obj);
                        ((IWKT)obj).WKT = shp.ExportToWKT();
                    }
                }
        }

        void gis_LayerAdd(object _sender, TatukGIS.NDK.TGIS_LayerEventArgs _e)
        {
            Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
            if ((Project != null) && (_e.Layer.CS.EPSG < 1))
            {
                _e.Layer.SetCSByEPSG(Project.EPSG);
            }
            //  if (View!=null)
            //  if (View.CurrentObject!=null)
            // View.ObjectSpace.SetModified(View.CurrentObject);

        }

        void gis_MouseDown(object sender, MouseEventArgs e)
        {
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            Point pt = new Point(e.X, e.Y);
            TGIS_Point ptg = gis.ScreenToMap(pt);
            if (gis.Mode == TGIS_ViewerMode.gisSelect)
            {
                try
                {
                    Clipboard.SetText((String.Format("POINT({0} {1})", ptg.X, ptg.Y)).Replace(",", "."));
                }
                catch (Exception)
                {

                }
            }
            if (legend.GIS_Layer == null) return;
            if (!((gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector))) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name);
            TGIS_Shape shp = ll.Locate(ptg, 5 / gis.Zoom, true);
            if (gis.Mode == TGIS_ViewerMode.gisSelect)
            {
                if (shp != null)
                {
                    shp = shp.MakeEditable();
                    if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                    {
                        shp.IsSelected = !shp.IsSelected;
                        shp.Invalidate();
                    }
                    else
                    {
                        ll.DeselectAll();
                        shp.IsSelected = true;// !shp.IsSelected;
                    }
                }
                else
                {
                    ll.DeselectAll();
                }
                if (Frame is NestedFrame)
                {
                    DetailView view = (DetailView)((NestedFrame)Frame).ViewItem.View;
                    DevExpress.ExpressApp.ListView units_view = null;
                    foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                            if (units_editor.ListView != null)
                            {
                                if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                    units_view = units_editor.ListView;
                                GISHelperWin.SelectUnitOnMap(ll, units_view);
                            }
                    }
                }
            }
        }

        void gis_KeyUpEx(object sender, KeyEventArgs e)
        {
            if (ctrlPressed)
                ctrlPressed = false;
        }
        void gis_KeyDownEx(object sender, KeyEventArgs e)
        {
            if (e.Control)
                ctrlPressed = true;
        }

        private void UpdateEditedShape(TGIS_Shape gisEditorCurrentShape, IObjectSpace os)
        {
            // ((XPObjectSpace)View.ObjectSpace).SetModified(View.CurrentObject);
            if (os == null)
                os = ObjectSpace;
            DetailView mapview = (DetailView)View;// GISHelperWin.MapHostView((DetailView)View);
            string oid = gisEditorCurrentShape.GetField("ECID").ToString();
            gisEditorCurrentShape.SetField("Edited", false);
            Guid guid = new Guid(oid);
            if (!String.IsNullOrEmpty(oid))
            {
                BaseObject obj = null;
                if (gisEditorCurrentShape.GetField("ObjectType").ToString() == "Locality")
                    obj = os.FindObject<Locality>(new BinaryOperator("Oid", guid));
                if (gisEditorCurrentShape.GetField("ObjectType").ToString() == "Project")
                    obj = os.FindObject<Project>(new BinaryOperator("Oid", guid));
                if (gisEditorCurrentShape.GetField("ObjectType").ToString() == "Store")
                    obj = os.FindObject<Store>(new BinaryOperator("Oid", guid));
                if (gisEditorCurrentShape.GetField("ObjectType").ToString() == "Unit")
                    obj = os.FindObject<Unit>(new BinaryOperator("Oid", guid));
                if (gisEditorCurrentShape.GetField("ObjectType").ToString() == "LocalityVisit")
                    obj = os.FindObject<Unit>(new BinaryOperator("LocalityVisit", guid));
                if (obj == null)
                {
                    return;
                }
                if (obj != null)
                {
                    if (obj is IWKT)
                    {
                        IWKT iwkt = (IWKT)obj;
                        string wkt = gisEditorCurrentShape.ExportToWKT();
                        if (gisEditorCurrentShape is TGIS_ShapePolygon)
                            iwkt.Area = ((TGIS_ShapePolygon)gisEditorCurrentShape).Area();
                        iwkt.WKT = wkt;
                        GISHelper.CreateCentroidXY(iwkt);
                        GISHelper.UpdateAreaAndLengthFromWKT(iwkt);
                    }
                }
            }
        }


        void gis_ZoomChange(object sender, EventArgs e)
        {
            /*   TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
               if (legend!=null)
               legend.Update();
               TGIS_ControlScale scale = GISHelperWin.GetGisScale((DetailView)View);
               if (scale!=null)
                   scale.Update();*/
            //     if (View.CurrentObject != null)
            //       View.ObjectSpace.SetModified(View.CurrentObject);
        }

        void gis_changed(object _sender, TGIS_LayerEventArgs _e)
        {
            //    if (View!=null)
            //    if (View.CurrentObject!=null)
            //   View.ObjectSpace.SetModified(View.CurrentObject);
        }




        private void simpleAction_ExportSummary_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
        }


        private void popupWindowShowAction_ExportToImage_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog { Title = "Save map image to file", Filter = "JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg|Tag Image File Format (*.tif;*.tiff)|*.tif|Portable Network Graphic (*.png)|*.png|Window Bitmap (*.bmp)|*.bmp", DefaultExt = "jpg" })
            {
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
                    MapWindowToImage par = ((MapWindowToImage)e.PopupWindow.View.CurrentObject);
                    gis.Invalidate();
                    gis.ExportToImage(dlgSave.FileName, gis.VisibleExtent, par.Width, par.Height, par.Compression, 0, par.Dpi);
                }
            }

        }

        private void popupWindowShowAction_ExportToImage_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            MapWindowToImage param = obs.CreateObject<MapWindowToImage>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;

        }

        private void singleChoiceAction_MapMode_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {

            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            try
            {
                if (gis.Editor != null)
                    if (gis.Editor.InEdit) gis.Editor.EndEdit();
            }
            finally
            {

            }
            gis.Cursor = Cursors.Default;
            if (e.SelectedChoiceActionItem.Caption == "Select")
                gis.Mode = TGIS_ViewerMode.gisSelect;
            if (e.SelectedChoiceActionItem.Caption == "Zoom")
                gis.Mode = TGIS_ViewerMode.gisZoom;
            if (e.SelectedChoiceActionItem.Caption == "Pan")
                gis.Mode = TGIS_ViewerMode.gisDrag;
            if (e.SelectedChoiceActionItem.Caption == "Edit")
            {
                gis.Mode = TGIS_ViewerMode.gisEdit;
                gis.Invalidate();
            }
            if (e.SelectedChoiceActionItem.Caption == "Create new unit")
            {
                gis.Cursor = Cursors.SizeAll;
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
            }
        }

        private void simpleAction_MapFullExtent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            gis.FullExtent();
        }



        private void simpleAction_AddLayer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            using (OpenFileDialog dlgOpen = new OpenFileDialog
            {
                Title = "Open layer from file",
                Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileType.gisFileTypeAll, false),
                //  CheckFileExists=false,
                Multiselect = true
            })
            {
                if (dlgOpen.ShowDialog() == DialogResult.OK)
                {
                    TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
                    foreach (string item in dlgOpen.FileNames)
                    {
                        string llname = Path.GetFileNameWithoutExtension(item);
                        TGIS_LayerAbstract ll = TGIS_Utils.GisCreateLayer(llname, item);
                        gis.Add(ll);
                    }
                    gis.Invalidate();
                }
            }
        }

        private void parametrizedAction_WMS_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            string llname = (String)e.ParameterCurrentValue;
            TGIS_LayerWMS ll = (TGIS_LayerWMS)TGIS_Utils.GisCreateLayer(llname, llname);
            gis.Add(ll);
            gis.Invalidate();
        }

        private void simpleAction_LayerExtent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (legend.GIS_Layer != null)
                gis.VisibleExtent = gis.Get(legend.GIS_Layer.Name).Extent;

        }

        private void simpleAction_ExportToGE_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (!(gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector)) return;
            GISHelperWin.ExportToGE((TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name));

        }

        private void simpleAction_GISstopediting_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis.Editor.InEdit)
            {
                MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                contr.SaveEdits();
                //simpleAction_GISstopediting.Active.SetItemValue("isediting", false);
            }
            //TODO NULL EXCETION HERE gis.Editor.EndEdit();

            //       Object frame = ((DetailView)View).Control;
            // ((DetailView)View).
        }

        private void singleChoiceAction_AddUnit_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (((SingleChoiceAction)e.Action).SelectedIndex == 0) //none
            {
                gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.CursorForUserDefined = gis.CursorForZoom;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 1) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 2) //Point by GPS point
            {
                Project gr = (Value_WindowController.Instance()).GetGroup((XPObjectSpace)View.ObjectSpace);
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll != null)
                {
                    DetailView view = (DetailView)((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.View;
                    MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    TGIS_GpsNmea gps = GISHelperWin.GetGPS(view);
                    if (!gps.Active)
                    {
                        MessageBox.Show("GPS not active!");
                        ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                        return;
                    }
                    ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                    DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
                    // MapWindow_ViewController MapWindow_contr = editor.Frame.GetController<MapWindow_ViewController>();
                    DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                    if (contr == null) return;
                    //TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                    TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                    if (!(gis.CS.EPSG > 0))
                        if (gr.EPSG > 0)
                        {
                            TGIS_CSCoordinateSystem cs = new TGIS_CSCoordinateSystem(gr.EPSG, "");
                            ptg = cs.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                        }

                    IWKT iwkt = (IWKT)(((NestedFrame)Frame).ViewItem.CurrentObject);
                    //MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
                    //  Session se = mapWindow.Session;

                    Unit newunit = ((XPObjectSpace)View.ObjectSpace).CreateObject<Unit>();
                    string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                    newunit.WKT = wkt;
                    newunit.EPSG = gis.CS.EPSG;
                    newunit.Save();
                    GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                    ((IUnits)iwkt).Units.Add(newunit);

                    GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.UnitID, newunit.EPSG, false, true, "Unit");
                    /* TGIS_ShapePoint shp = (TGIS_ShapePoint)ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                             shp.AddPart();
                             shp.AddPoint(ptg);*/
                    foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                        {
                            object listEditor = units_editor.ListView.Editor;
                            if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                units_editor.Refresh();
                        }
                    }
                    gis.Invalidate();

                }
                else
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                }
                // singleChoiceAction_AddUnit.SelectedIndex = 0;
                //singleChoiceAction_MapMode.SelectedIndex = 1;
                gis.Mode = TGIS_ViewerMode.gisZoom;

                //MessageBox.Show("Unit added");
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 3) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 4) //locality outline by doubleclicking
            {
                //singleChoiceAction_MapMode.SelectedIndex = 3;
                gis.Mode = TGIS_ViewerMode.gisEdit;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }

        }

        private void singleChoiceAction_AddLocality_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (((SingleChoiceAction)e.Action).SelectedIndex == 0) //none
            {
                gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.CursorForUserDefined = gis.CursorForZoom;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 1) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 2) //Point by GPS point
            {
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll != null)
                {
                    DetailView view = (DetailView)((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.View;
                    MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    TGIS_GpsNmea gps = GISHelperWin.GetGPS(view);
                    if (!gps.Active)
                    {
                        MessageBox.Show("GPS not active!");
                        ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                        return;
                    }
                    ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                    DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
                    MapWindow_ViewController MapWindow_contr = editor.Frame.GetController<MapWindow_ViewController>();
                    DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                    if (contr == null) return;
                    TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));

                    IWKT iwkt = (IWKT)View.CurrentObject;// (IWKT)(((NestedFrame)Frame).ViewItem.CurrentObject);
                    MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
                    Session se = mapWindow.Session;

                    Unit newunit = ((XPObjectSpace)View.ObjectSpace).CreateObject<Unit>();
                    string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                    newunit.WKT = wkt;
                    newunit.Save();
                    GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                    ((IUnits)iwkt).Units.Add(newunit);

                    GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.UnitID, newunit.EPSG, false, true, "Unit");
                    /* TGIS_ShapePoint shp = (TGIS_ShapePoint)ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                        shp.AddPart();
                        shp.AddPoint(ptg);*/
                    foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                        {
                            object listEditor = units_editor.ListView.Editor;
                            if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                units_editor.Refresh();
                        }
                    }
                    gis.Invalidate();
                }
                else
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                }
                //singleChoiceAction_MapMode.SelectedIndex = 1;
                gis.Mode = TGIS_ViewerMode.gisZoom;

                MessageBox.Show("Unit added");
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 3) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
        }

        private void popupWindowShowAction_DowloadAndMapGbifForExtent_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            /*    new Exception("not implemented");
                TaxonomicName name=(e.PopupWindow.View.CurrentObject as DowloadAndMapGbifForExtentParameters).Taxon;
                   TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
                   TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("GBIF");
                   if (ll == null)
                   {
                       ll = new TGIS_LayerVector();
                       gis.Add(ll);
                   }
                   TaxonomyHelper.GBIF(name, gis.VisibleExtent,ll);*/
        }

        private void popupWindowShowAction_DowloadAndMapGbifForExtent_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            /*  XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
              DowloadAndMapGbifForExtentParameters param = obs.CreateObject<DowloadAndMapGbifForExtentParameters>();
              DetailView dv = Application.CreateDetailView(obs, param);
              e.View = dv;*/

        }
        void gis_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TGIS_GpsNmea gps = GISHelperWin.GetGPS((DetailView)View);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_LayerVector ll_units = (TGIS_LayerVector)gis.Get("Units");
            TGIS_LayerVector ll_places = (TGIS_LayerVector)gis.Get("Localities");
            TGIS_LayerVector ll_stores = (TGIS_LayerVector)gis.Get("Stores");
            Point pt = new Point(e.X, e.Y);
            TGIS_Point ptg = gis.ScreenToMap(pt);
            if (ll_units == null)
            {
                ll_units = new TGIS_LayerVector();
                ll_units.Name = "Units";
                GISHelper.LayerParams(ll_units);
                if (gis.CS.EPSG > 0)
                    ll_units.SetCSByEPSG(gis.CS.EPSG);
                gis.Add(ll_units);
                GISHelper.AddFields(ll_units);
            }

            if (Frame is NestedFrame)
            {
                DetailView view = (DetailView)((NestedFrame)Frame).ViewItem.View;
                if (gis.Mode == TGIS_ViewerMode.gisEdit)
                {
                    if (view.CurrentObject != null)
                        if (typeof(IWKT).IsAssignableFrom(view.CurrentObject.GetType()))
                            if (String.IsNullOrEmpty(((IWKT)view.CurrentObject).WKT))
                            {
                                CreateNewShape(gis, ll_units, ((IWKT)view.CurrentObject).GeometryType, ((IWKT)view.CurrentObject), ptg, GISHelper.GetLayerObjectType(view.CurrentObject.GetType()));
                            }
                    if (typeof(IUnits).IsAssignableFrom(view.CurrentObject.GetType()))
                    {
                        ListPropertyEditor units_editor = null;
                        foreach (ViewItem item in view.Items)
                        {
                            if (item is ListPropertyEditor)
                                if ((item as ListPropertyEditor).ListView != null)
                                {
                                    if ((item as ListPropertyEditor).ListView.ObjectTypeInfo.Type == typeof(Unit))
                                    {
                                        units_editor = item as ListPropertyEditor;
                                    }
                                }
                        }
                        if (units_editor != null)
                        {
                            Unit unit = (Unit)units_editor.ListView.SelectedObjects[0];
                            CreateNewShape(gis, ll_units, unit.GeometryType, unit, ptg, GISHelper.GetLayerObjectType(typeof(Unit)));
                        }
                    }
                }
                else
                { }
            }
            if (CreateNewUnitShapeFlag)
            {
                try
                {
                    CreateNewUnitShape(gis, ll_units, CreateNewUnitShapeMapUnit.GeometryType, CreateNewUnitShapeUnit, ptg);
                    CreateNewUnitShapeUnit.Save();
                    //View.ObjectSpace.CommitChanges();
                    //View.ObjectSpace.Refresh();
                    EarthCapeGISWinModule.UpdateMap = false;
                    if (Frame is NestedFrame)
                    {

                        DetailView view = (DetailView)((NestedFrame)Frame).ViewItem.View;
                        ((BaseObject)view.CurrentObject).Save();
                        view.ObjectSpace.CommitChanges();
                        foreach (ViewItem item in view.Items)
                        {
                            ListPropertyEditor units_editor = item as ListPropertyEditor;
                            if (units_editor != null)
                                if (units_editor.ListView != null)
                                {
                                    if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                    {
                                        item.Refresh();
                                        //units_editor.ListView.ObjectSpace.Refresh();
                                        // units_editor.ListView.CurrentObject = CreateNewUnitShapeUnit;// units_editor.ListView.ObjectSpace.GetObjectByKey<Unit>(CreateNewUnitShapeUnit);
                                    }
                                }
                        }

                    }
                }
                finally
                {
                    // gis.Mode = TGIS_ViewerMode.gisSelect;
                    //  gis.Editor.EditShape(shp1, 0);
                    //singleChoiceAction_MapMode.SelectedIndex = 0;
                    CreateNewUnitShapeFlag = false;
                    EarthCapeGISWinModule.UpdateMap = true;
                }
            }

            /*  XPObjectSpace os = Application.CreateObjectSpace();
            DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
            ShowViewParameters svpInternal = new ShowViewParameters();
            svpInternal.CreatedView = view;
            svpInternal.TargetWindow = TargetWindow.NewModalWindow;
            svpInternal.Context = TemplateContext.PopupWindow;
            svpInternal.CreateAllControllers=true;
            Application.ShowViewStrategy.ShowView(svpInternal, new ShowViewSource(null, null));*/
        }



        private void simpleAction_MapLocalities_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            // User user = (User)(Application.CreateObjectSpace()).GetObject(SecuritySystem.CurrentUser);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            CriteriaOperator cr = GISHelper.GetMapCriteria(gis.VisibleExtent);
            /*if (uow == null)
            {
                uow = new UnitOfWork(((XPObjectSpace)Application.CreateObjectSpace()).Session.DataLayer);
            }*/
            IList places = uow.CreateCollection(typeof(Locality), cr);// new XPCollection<Locality>(uow, cr);
            //todo uow is null
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Localities");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Localities";
                gis.Add(ll);
            }
            int count = 0;
            foreach (Locality item in places)
            {
                TGIS_Shape shp = null;

                shp = GISHelper.AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.Name, gis.CS.EPSG, false, true, "Locality");
                if (shp != null)
                {
                    count += 1;
                    shp.SetField("Locality", item.Name);
                    shp.SetField("ObjectType", "Locality");
                }
                /*TGIS_Shape shp = ll.FindFirst(ll.Extent, "ID=" + item.Oid.ToString());
                if (shp==null)
                shp = GISHelper.AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.Name, gis.CS.EPSG, false, true);*/

            }
            //MessageBox.Show(count + " localities mapped.");
            legend.GIS_Layer = ll;
            GISHelper.AddFields(ll);
            GISHelper.LayerParams(ll);
            ll.RecalcExtent();
            ll.RecalcProjectedExtent();
            ll.SaveAll();
            gis.Invalidate();
            //gis.VisibleExtent = ll.Extent;

        }

        private void simpleAction_ShowMapObject_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (!(gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector)) return;
            TGIS_LayerVector ll = (gis.Get(legend.GIS_Layer.Name) as TGIS_LayerVector);
            gis.Mode = TGIS_ViewerMode.gisSelect;
            if (gis.Editor != null)
                gis.Editor.EndEdit();
            ///singleChoiceAction_MapMode.SelectedIndex = 0;
            gis.Mode = TGIS_ViewerMode.gisSelect;
           IObjectSpace os = Application.CreateObjectSpace();// View.ObjectSpace;
            os.Committed += new EventHandler(os_Committed);
            TGIS_Shape shp = ll.FindFirst(ll.Extent, "GIS_Selected = True");

            if (legend.GIS_Layer.Name == "Localities")
            {
                if (ll.GetSelectedCount() == 1)
                {
                    Locality loc = os.FindObject<Locality>(new BinaryOperator("Oid", shp.GetField("ECID")));
                    DevExpress.ExpressApp.DetailView detview = Application.CreateDetailView(os, loc, true);
                    e.ShowViewParameters.CreatedView = detview;

                }
                if (ll.GetSelectedCount() > 1)
                {
                    ArrayList keys = new ArrayList();
                    while (shp != null)
                    {
                        keys.Add(os.GetKeyValue(os.FindObject<Locality>(new BinaryOperator("Oid", shp.GetField("ECID")))));
                        shp = ll.FindNext();
                    }
                    CollectionSource cs = new CollectionSource(os, typeof(Locality));
                    cs.Criteria["Locs"] = new InOperator("Oid", keys);
                    e.ShowViewParameters.CreatedView = Application.CreateListView(Application.FindListViewId(typeof(Locality)), cs, true);
                }
            }
            if (legend.GIS_Layer.Name == "Units")
            {
                if (ll.GetSelectedCount() == 1)
                {
                    Unit loc = os.FindObject<Unit>(new BinaryOperator("Oid", shp.GetField("ECID")));
                    DevExpress.ExpressApp.DetailView detview = Application.CreateDetailView(os, loc, true);
                    e.ShowViewParameters.CreatedView = detview;

                }
                if (ll.GetSelectedCount() > 1)
                {
                    ArrayList keys = new ArrayList();
                    while (shp != null)
                    {
                        keys.Add(os.GetKeyValue(os.FindObject<Unit>(new BinaryOperator("Oid", shp.GetField("ECID")))));
                        shp = ll.FindNext();
                    }
                    CollectionSource cs = new CollectionSource(os, typeof(Unit));
                    cs.Criteria["units"] = new InOperator("Oid", keys);
                    e.ShowViewParameters.CreatedView = Application.CreateListView(Application.FindListViewId(typeof(Unit)), cs, true);
                }
            }//    GISHelperWin.ExportToGE((TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name));
            View.ObjectSpace.CommitChanges();
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;

        }

        void os_Committed(object sender, EventArgs e)
        {
            // View.ObjectSpace.Refresh();    
        }

        private void popupWindowShowAction_NewLocality_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            DetailView dv = Application.CreateDetailView(os, os.CreateObject<MapNewLocality>());
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (gis.Get("Localities") != null)
                legend.GIS_Layer = gis.Get("Localities");
            gis.Mode = TGIS_ViewerMode.gisSelect;
            if (gis.Editor != null)
                gis.Editor.EndEdit();
            //singleChoiceAction_MapMode.SelectedIndex = 0;
            gis.Mode = TGIS_ViewerMode.gisSelect;
            foreach (BaseObject item in os.GetObjectsToSave(false))
            {
                item.Save();
            }
            os.CommitChanges();
            e.View = dv;

        }

        private void popupWindowShowAction_NewLocality_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = View.ObjectSpace;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Localities");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Localities";
                gis.Add(ll);
            }
            GISHelper.AddFields(ll);
            GISHelper.LayerParams(ll);
            ll.DeselectAll();
            if (gis.Get("Localities") != null)
                legend.GIS_Layer = gis.Get("Localities");
            //  ll.SaveAll();
            MapNewLocality maploc = (MapNewLocality)(e.PopupWindow.View.CurrentObject);
            Locality loc = os.CreateObject<Locality>();
            loc.Name = maploc.Name;
            TGIS_Shape shp = null;
            TGIS_Shape shp1 = null;
            if (maploc.GeometryType == GeometryType.Polygon)
            {
                shp = new TGIS_ShapePolygon();
                TGIS_Extent extent = gis.VisibleExtent;
                double delta = (extent.XMax - extent.XMin) / 30;
                TGIS_Point ptg = new TGIS_Point(extent.XMin + (extent.XMax - extent.XMin) / 2, extent.YMin + (extent.YMax - extent.YMin) / 2);

                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y + delta));
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y + delta));
            }
            if (maploc.GeometryType == GeometryType.Line)
            {
                shp = new TGIS_ShapeArc();
                TGIS_Extent extent = gis.VisibleExtent;
                double delta = (extent.XMax - extent.XMin) / 30;
                TGIS_Point ptg = new TGIS_Point(extent.XMin + (extent.XMax - extent.XMin) / 2, extent.YMin + (extent.YMax - extent.YMin) / 2);
                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
            }
            if (maploc.GeometryType == GeometryType.Point)
            {
                shp = new TGIS_ShapePoint();
                TGIS_Extent extent = gis.VisibleExtent;
                double delta = (extent.XMax - extent.XMin) / 30;
                TGIS_Point ptg = new TGIS_Point(extent.XMin + (extent.XMax - extent.XMin) / 2, extent.YMin + (extent.YMax - extent.YMin) / 2);
                shp.AddPart();
                shp.AddPoint(ptg);
            }
            if (shp != null)
            {
                shp1 = ll.AddShape(shp);
                loc.WKT = shp.ExportToWKT();
                GISHelper.CreateCentroidXY(loc);
                GISHelper.UpdateAreaAndLengthFromWKT(loc);
                loc.Save();
                os.CommitChanges();
                shp1.SetField("ECID", loc.Oid.ToString());
                shp1.SetField("Label", loc.Name);
                shp1.SetField("Locality", loc.Name);
                shp1.SetField("OwnGeometry", true);
                shp1.SetField("ObjectType", "Locality");
                shp1.SetField("Edited", true);
                shp1.IsSelected = true;
                //  gis.SaveAll();
                gis.Mode = TGIS_ViewerMode.gisEdit;
                //singleChoiceAction_MapMode.SelectedIndex=3;
                gis.Editor.EditShape(shp1, 0);
                GISHelper.LayerParams(ll);
                gis.Invalidate();
            }
        }

        private void simpleAction_MapUnits_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            double xmin = gis.VisibleExtent.XMin;
            double xmax = gis.VisibleExtent.XMax;
            double ymin = gis.VisibleExtent.YMin;
            double ymax = gis.VisibleExtent.YMax;

            CriteriaOperator cr1 = new BinaryOperator("Centroid_X", xmin, BinaryOperatorType.Greater);
            CriteriaOperator cr2 = new BinaryOperator("Centroid_Y", ymin, BinaryOperatorType.Greater);
            CriteriaOperator cr3 = new BinaryOperator("Centroid_Y", ymax, BinaryOperatorType.Less);
            CriteriaOperator cr4 = new BinaryOperator("Centroid_X", xmax, BinaryOperatorType.Less);
            CriteriaOperator cr = CriteriaOperator.And(cr1, cr2, cr3, cr4);
            /*if (uow == null)
            {
                uow = new UnitOfWork(((XPObjectSpace)Application.CreateObjectSpace()).Session.DataLayer);
            }*/

            IList units = uow.CreateCollection(typeof(Unit), cr);// new XPCollection<Locality>(uow, cr);
            //XPCollection<Unit> units = new XPCollection<Unit>(uow, cr);
            //todo uow is null
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Units";
                gis.Add(ll);
                GISHelper.AddFields(ll);
            }
            Project gr = null;
            if (Value_WindowController.Instance() != null)
                gr = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
            GISHelperWin.UpdateMap(View.ObjectSpace, gis, gr, units);
            legend.GIS_Layer = ll;
            GISHelper.LayerParams(ll);
            ll.RecalcExtent();
            ll.RecalcProjectedExtent();
            gis.Update();

            // gis.VisibleExtent = ll.Extent;
            /*foreach (Unit item in units)
                {
                    if (!string.IsNullOrEmpty(item.WKT))
                    {
                        GISHelper.AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.FullName, gis.CS.EPSG, false);
                        gis.Invalidate();
                    }
                    else
                    {
                        if ((item.Centroid_X != null) && (item.Centroid_Y != null))
                        {
                            TGIS_Shape shp1 = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                            shp1.AddPoint(new TGIS_Point(item.Centroid_X, item.Centroid_Y));
                            shp1.SetField("ID", item.Oid.ToString());
                            shp1.SetField("ECID", item.Oid.ToString());
                            shp1.SetField("Label", item.FullName);
                            gis.Invalidate();
                        }

                    }
                }*/
        }

        private void popupWindowShowAction_NewUnit_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            MapNewUnit mapNewUnit = os.CreateObject<MapNewUnit>();
            if (Frame is NestedFrame)
            {
                DetailView view = (DetailView)((NestedFrame)Frame).ViewItem.View;
                if (view.ObjectTypeInfo.Type == typeof(Locality))
                    if (view.CurrentObject != null)
                        mapNewUnit.Locality = os.FindObject<Locality>(new BinaryOperator("Oid", ((Locality)view.CurrentObject).Oid));
            }
            DetailView dv = Application.CreateDetailView(os, mapNewUnit);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);

            gis.Mode = TGIS_ViewerMode.gisSelect;
            if (gis.Editor != null)
            {
                if (gis.Editor.CurrentShape != null)
                {
                    ((TGIS_Shape)gis.Editor.CurrentShape).Layer.DeselectAll();
                    ((TGIS_Shape)gis.Editor.CurrentShape).IsSelected = true;
                    UpdateEditedShape(((TGIS_Shape)gis.Editor.CurrentShape), null);
                    View.ObjectSpace.CommitChanges();
                }
                gis.Editor.EndEdit();
            }
            //singleChoiceAction_MapMode.SelectedIndex = 0;
            gis.Mode = TGIS_ViewerMode.gisSelect;
            /*   foreach (BaseObject item in os.GetObjectsToSave(false))
               {
                   item.Save();
               }
               ;*/
            if (legend.GIS_Layer != null)
            {
                if (legend.GIS_Layer.Name == "Localities")
                {
                    if (((TGIS_LayerVector)legend.GIS_Layer).GetSelectedCount() == 1)
                    {
                        TGIS_Shape shp = ((TGIS_LayerVector)legend.GIS_Layer).FindFirst(((TGIS_LayerVector)legend.GIS_Layer).Extent, "GIS_Selected = True");
                        if (shp != null)
                            try
                            {
                                mapNewUnit.Locality = os.FindObject<Locality>(new BinaryOperator("Oid", shp.GetField("ECID")));
                                /* if (mapNewUnit.Locality != null)
                                 {
                                     mapNewUnit.Position = NewUnitPosition.NoShape;
                                 }*/
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(String.Format("Cannot find object  {0} in the database (generated error: {1})", shp.GetField("ECID"), ex.Message));
                            }

                    }
                }
            }
            if (gis.Get("Units") != null)
                legend.GIS_Layer = gis.Get("Units");
            e.View = dv;

        }

        private void popupWindowShowAction_NewUnit_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = View.ObjectSpace;
            if (Frame is NestedFrame)
            {
                DetailView view = (DetailView)((NestedFrame)Frame).ViewItem.View;
                os = view.ObjectSpace;
            }
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Units";
                gis.Add(ll);
                GISHelper.AddFields(ll);
            }
            if (gis.Get("Units") != null)
                legend.GIS_Layer = gis.Get("Units");
            GISHelper.AddFields(ll);
            GISHelper.LayerParams(ll);
            //  ll.SaveAll();
            MapNewUnit mapunit = (MapNewUnit)(e.PopupWindow.View.CurrentObject);
            Unit unit = null;
            if (mapunit.ExistingUnit != null)
            { unit = os.FindObject<Unit>(new BinaryOperator("Oid", mapunit.ExistingUnit.Oid)); }
            else
                unit = os.CreateObject<Unit>();
            if (mapunit.Dataset != null)
                unit.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", mapunit.Dataset));
            if (mapunit.TaxonomicName != null)
                unit.TaxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", mapunit.TaxonomicName.Oid));
            if (mapunit.Locality != null)
                unit.Locality = os.FindObject<Locality>(new BinaryOperator("Oid", mapunit.Locality.Oid));
            unit.Date = mapunit.Date;
            unit.Quantity = mapunit.Quantity;
            if (!String.IsNullOrEmpty(mapunit.UnitType))
                unit.UnitType = mapunit.UnitType;
            if (!String.IsNullOrEmpty(mapunit.Host))
                unit.Host = mapunit.Host;
            if (!String.IsNullOrEmpty(mapunit.UnitID))
                unit.UnitID = mapunit.UnitID;
            if (gis.CS.EPSG > 0) unit.EPSG = gis.CS.EPSG; else if (ll.CS.EPSG > 0) unit.EPSG = ll.CS.EPSG;
            unit.Save();
            os.CommitChanges();
            if (mapunit.Position != NewUnitPosition.DoubleClickOnTheMapForPostion)
            {
                if (mapunit.Position != NewUnitPosition.NoShape)
                {
                    TGIS_Extent extent = gis.VisibleExtent;
                    NewUnitPosition pos = mapunit.Position;
                    TGIS_Point ptg = new TGIS_Point(extent.XMin + (extent.XMax - extent.XMin) / 2, extent.YMin + (extent.YMax - extent.YMin) / 2);
                    /*if (pos == NewUnitPosition.CurrentGPSPosition)
                    {
                    /*    if (!ReferenceEquals(lastPointMap, null))
                        {
                            CreateNewUnitShape(gis, ll, mapunit.GeometryType, unit, ptg);
                            GISHelper.CreateCentroidXY(unit);
                            GISHelper.UpdateAreaAndLengthFromWKT(unit);
                        }
                    }*/
                }
            }
            else
            {
                //singleChoiceAction_MapMode.SelectedIndex = 3;
                gis.Mode = TGIS_ViewerMode.gisEdit;
                CreateNewUnitShapeUnit = unit;
                CreateNewUnitShapeMapUnit = mapunit;
                CreateNewUnitShapeFlag = true;
            }
        }
        private void CreateNewUnitShape(TGIS_ViewerWnd gis, TGIS_LayerVector ll, GeometryType geom, Unit unit, TGIS_Point ptg)
        {
            TGIS_Extent extent = gis.VisibleExtent;
            double delta = (extent.XMax - extent.XMin) / 30;
            TGIS_Shape shp = null;
            TGIS_Shape shp1 = null;
            if (geom == GeometryType.Polygon)
            {
                shp = new TGIS_ShapePolygon();

                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y + delta));
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y + delta));
            }
            if (geom == GeometryType.Line)
            {
                shp = new TGIS_ShapeArc();
                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
            }
            if (geom == GeometryType.Point)
            {
                shp = new TGIS_ShapePoint();
                shp.AddPart();
                shp.AddPoint(ptg);
            }
            if (shp != null)
            {
                //  shp1 = ll.AddShape(shp);
                unit.WKT = shp.ExportToWKT();
                GISHelper.CreateCentroidXY(unit);
                GISHelper.UpdateAreaAndLengthFromWKT(unit);
                unit.Save();
                View.ObjectSpace.CommitChanges();
                GISHelper.LayerParams(ll);
                shp1 = GISHelper.AddShapeFromWKT(unit.WKT, ll, unit.Oid.ToString(), unit.Oid, unit.FullName, gis.CS.EPSG, false, true, "Unit");
                if (shp1 == null) return;
                shp1.SetField("ECID", unit.Oid.ToString());
                shp1.SetField("Label", !String.IsNullOrEmpty(unit.UnitID) ? unit.UnitID + "|" : "" + unit.TaxonomicName != null ? unit.TaxonomicName.Name + "|" : "");
                shp1.SetField("OwnGeometry", true);
                shp1.SetField("Edited", true);
                shp1.SetField("UnitID", unit.UnitID);
                shp1.SetField("ObjectType", "Unit");
                ll.DeselectAll();
                if (shp1 is TGIS_ShapePoint)
                {
                    shp1.IsSelected = true;
                    //  gis.SaveAll();
                    //singleChoiceAction_MapMode.SelectedIndex = 0;
                    gis.Mode = TGIS_ViewerMode.gisSelect;
                    //  gis.Editor.EditShape(shp1, 0);
                }
                else
                {
                    //  singleChoiceAction_MapMode.SelectedIndex = 3;
                    gis.Mode = TGIS_ViewerMode.gisEdit;
                    gis.Editor.EditShape(shp1, 0);
                }
                gis.Update();
            }
        }
        private void CreateNewShape(TGIS_ViewerWnd gis, TGIS_LayerVector ll, GeometryType geom, IWKT unit, TGIS_Point ptg, string objectType)
        {
            TGIS_Extent extent = gis.VisibleExtent;
            double delta = (extent.XMax - extent.XMin) / 30;
            TGIS_Shape shp = null;
            TGIS_Shape shp1 = null;
            if (geom == GeometryType.Polygon)
            {
                shp = new TGIS_ShapePolygon();

                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y + delta));
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y + delta));
            }
            if (geom == GeometryType.Line)
            {
                shp = new TGIS_ShapeArc();
                shp.AddPart();
                shp.AddPoint(new TGIS_Point(ptg.X - delta, ptg.Y - delta));
                shp.AddPoint(new TGIS_Point(ptg.X + delta, ptg.Y - delta));
            }
            if (geom == GeometryType.Point)
            {
                shp = new TGIS_ShapePoint();
                shp.AddPart();
                shp.AddPoint(ptg);
            }
            if (shp != null)
            {
                //  shp1 = ll.AddShape(shp);
                unit.WKT = shp.ExportToWKT();
                GISHelper.CreateCentroidXY(unit);
                GISHelper.UpdateAreaAndLengthFromWKT(unit);
                unit.Save();
                View.ObjectSpace.CommitChanges();
                GISHelper.LayerParams(ll);
                shp1 = GISHelper.AddShapeFromWKT(unit.WKT, ll, unit.Oid.ToString(), unit.Oid, unit.Name, gis.CS.EPSG, false, true, objectType);
                if (shp1 == null) return;
                shp1.SetField("ECID", unit.Oid.ToString());
                shp1.SetField("OwnGeometry", true);
                shp1.SetField("Edited", true);
                shp1.SetField("ObjectType", objectType);
                ll.DeselectAll();
                if (shp1 is TGIS_ShapePoint)
                {
                    shp1.IsSelected = true;
                    //  gis.SaveAll();
                    //singleChoiceAction_MapMode.SelectedIndex = 0;
                    gis.Mode = TGIS_ViewerMode.gisSelect;
                    //  gis.Editor.EditShape(shp1, 0);
                }
                else
                {
                    //  singleChoiceAction_MapMode.SelectedIndex = 3;
                    gis.Mode = TGIS_ViewerMode.gisEdit;
                    gis.Editor.EditShape(shp1, 0);
                }
                gis.Update();
            }
        }
        private void simpleAction_RemoveLayer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            gis.Delete(legend.GIS_Layer.Name);
            legend.Update();
            gis.Invalidate();
        }

        private void simpleAction_GetLocalitiesFromView_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            //User user = (User)(Application.CreateObjectSpace()).GetObject(SecuritySystem.CurrentUser);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            CriteriaOperator cr = GISHelper.GetMapCriteria(gis.VisibleExtent);
            CollectionSource collSource = new CollectionSource(Application.CreateObjectSpace(), typeof(Locality));
            // ((XPCollection)collSource.Collection).Criteria = cr;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(Application.FindListViewId(typeof(Locality)), collSource, true);
            view.CollectionSource.Criteria["MappedLocalities"] = cr;
            e.ShowViewParameters.CreatedView = view;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }

        private void parametrizedAction_FindAndMapLocalities_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            //User user = (User)(Application.CreateObjectSpace()).GetObject(SecuritySystem.CurrentUser);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
             
            CriteriaOperator cr = CriteriaOperator.Or(
                new FunctionOperator("Like", new OperandProperty("Name"), new OperandValue(e.ParameterCurrentValue)),
                new FunctionOperator("Category.Name", new OperandProperty("Name"), new OperandValue(e.ParameterCurrentValue)),
                new FunctionOperator("Category.Parent.Name", new OperandProperty("Name"), new OperandValue(e.ParameterCurrentValue)),
                new FunctionOperator("Category.Parent.parent.Name", new OperandProperty("Name"), new OperandValue(e.ParameterCurrentValue))
            );
            /*if (uow == null)
            {
                uow = new UnitOfWork(((XPObjectSpace)Application.CreateObjectSpace()).Session.DataLayer);
            }*/
            IList places = uow.CreateCollection(typeof(Locality), cr);// new XPCollection<Locality>(uow, cr);
            //XPCollection<Locality> places = new XPCollection<Locality>(uow, cr);
            //todo uow is null
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Localities");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Localities";
                gis.Add(ll);
                legend.GIS_Layer = ll;
                GISHelper.LayerParams(ll);
                GISHelper.AddFields(ll);
            }
            Collection<TGIS_Extent> extents = new Collection<TatukGIS.NDK.TGIS_Extent>();
            if (places.Count == 0) throw new Exception("Locality not found!");
            Locality item = (Locality)places[0];
            TGIS_Shape shp = GISHelper.AddShapeFromWKT(item.WKT, ll, item.Oid.ToString(), item.Oid, item.Name, gis.CS.EPSG, false, true, "Locality");
            shp.SetField("Locality", item.Name);
            shp.SetField("ObjectType", "Locality");
            //     shp.Flash();
            /*     if (shp != null)
                 {
                     extents.Add(shp.Extent);
                  }*/

            //ll.RecalcExtent();
            //ll.RecalcProjectedExtent();
            gis.VisibleExtent = shp.Extent;
            /* TGIS_Extent ext=extents[0];
           for (int i = 1; i < extents.Count-1; i++)
           {
               ext = TGIS_Utils.GisCommonExtent(ext, extents[i]);
           }
           foreach (TGIS_Extent item in extents)
           {
                
           }gis.VisibleExtent = ext;*/
            gis.Invalidate();

        }


        private void singleChoiceAction_ViewMode_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (e.SelectedChoiceActionItem.Caption == "2D")
                gis.View3D = false;
            if (e.SelectedChoiceActionItem.Caption == "3D")
                if (gis.View3DCapable() == true)
                {
                    gis.View3D = true;
                    // gis.Viewer3D.Mode = TatukGIS.NDK.WinForms.TGIS_Viewer3D.gis3DModeCameraPosition;
                    /*      break;
                      case 1: GIS.Viewer3D.Mode = TatukGIS.NDK.WinForms.TGIS_Viewer3DMode.gis3DModeCameraXYZ;
                          break;
                      case 2: GIS.Viewer3D.Mode = TatukGIS.NDK.WinForms.TGIS_Viewer3DMode.gis3DModeCameraRotation;
                          break;
                      case 3: GIS.Viewer3D.Mode = TatukGIS.NDK.WinForms.TGIS_Viewer3DMode.gis3DModeSunPosition;
                          break;
                      case 4: GIS.Viewer3D.Mode = TatukGIS.NDK.WinForms.TGIS_Viewer3DMode.gis3DModeZoom;*/
                }
                else throw new Exception("Not suported");

        }

        private void simpleAction_Select_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);

            gis.Mode = TGIS_ViewerMode.gisSelect;
        }

        private void simpleAction_Zoom_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);

            gis.Mode = TGIS_ViewerMode.gisZoom;

        }

        private void simpleAction_Edit_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            /*try
            {
                if (gis.Editor != null)
                    if (gis.Editor.InEdit) gis.Editor.EndEdit();
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }*/
            gis.Mode = TGIS_ViewerMode.gisEdit;
            gis.Invalidate();
        }

        private void simpleAction_Pan_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);

            gis.Mode = TGIS_ViewerMode.gisDrag;

        }

        private void simpleAction_ExportVectorLayer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
        }

        private void popupWindowShowAction_ExportToVector_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IWKTExport2VectorOptions options = (IWKTExport2VectorOptions)e.PopupWindow.View.CurrentObject;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (!(gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector)) return;
            GISHelperWin.ExportToVector((TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name), options.TargetEpsg);

        }

        private void popupWindowShowAction_ExportToVector_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            IWKTExport2VectorOptions options = os.CreateObject<IWKTExport2VectorOptions>();
            e.View = Application.CreateDetailView(os, options);

        }



        /*    private void popupWindowShowAction_RecordUnitByName_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
            {
                if (popupWindowShowAction_RecordUnitByName.Tag == null)
                {
                    string name = ((TaxonomicName)(e.PopupWindow.View.CurrentObject)).Name;
                    if (name.Length > 9) name = name.Substring(0, 10) + "...";
                    popupWindowShowAction_RecordUnitByName.Caption = name;
                    popupWindowShowAction_RecordUnitByName.Tag = ((TaxonomicName)e.PopupWindow.View.CurrentObject).Oid;
                }
                else
                {
                    popupWindowShowAction_RecordUnitByName.Caption = cap;
                    popupWindowShowAction_RecordUnitByName.Tag = null;
    
                }
            
            }

            private void popupWindowShowAction_RecordUnitByName_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
            {
                    XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                    DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
                    e.View = view;
            }

            private void popupWindowShowAction_RecordUnitByName_Cancel(object sender, EventArgs e)
            {
                popupWindowShowAction_RecordUnitByName.Caption = cap;
                popupWindowShowAction_RecordUnitByName.Tag = null;
      
            }
            */


    }
}