namespace EarthCape.Module.GIS.Win
{
    partial class IWKT_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_MapUnits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_GE = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ToVectorMapFile = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_UpdateAreaAndLengthFromGeometry = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ConvertToEPSG = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_DistanceMatrix = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_UpdateLatLongFromWKT = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_MapUnits
            // 
            this.simpleAction_MapUnits.AcceptButtonCaption = null;
            this.simpleAction_MapUnits.CancelButtonCaption = null;
            this.simpleAction_MapUnits.Caption = "Map";
            this.simpleAction_MapUnits.Category = "Tools";
            this.simpleAction_MapUnits.ConfirmationMessage = null;
            this.simpleAction_MapUnits.Id = "simpleAction_MapUnits";
            this.simpleAction_MapUnits.ImageName = "GeoPointMap";
            this.simpleAction_MapUnits.ToolTip = null;
            this.simpleAction_MapUnits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.simpleAction_MapUnits_CustomizePopupWindowParams);
            this.simpleAction_MapUnits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.simpleAction_MapUnits_Execute);
            // 
            // popupWindowShowAction_GE
            // 
            this.popupWindowShowAction_GE.AcceptButtonCaption = null;
            this.popupWindowShowAction_GE.CancelButtonCaption = null;
            this.popupWindowShowAction_GE.Caption = "Google Earth";
            this.popupWindowShowAction_GE.Category = "Export";
            this.popupWindowShowAction_GE.ConfirmationMessage = null;
            this.popupWindowShowAction_GE.Id = "popupWindowShowAction_GE";
            this.popupWindowShowAction_GE.ImageName = "google_earth_link";
            this.popupWindowShowAction_GE.ToolTip = null;
            this.popupWindowShowAction_GE.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_GE_CustomizePopupWindowParams);
            this.popupWindowShowAction_GE.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_GE_Execute);
            // 
            // popupWindowShowAction_ToVectorMapFile
            // 
            this.popupWindowShowAction_ToVectorMapFile.AcceptButtonCaption = null;
            this.popupWindowShowAction_ToVectorMapFile.CancelButtonCaption = null;
            this.popupWindowShowAction_ToVectorMapFile.Caption = "Export to vector";
            this.popupWindowShowAction_ToVectorMapFile.Category = "Export";
            this.popupWindowShowAction_ToVectorMapFile.ConfirmationMessage = null;
            this.popupWindowShowAction_ToVectorMapFile.Id = "popupWindowShowAction_ToVectorMapFile";
            this.popupWindowShowAction_ToVectorMapFile.ImageName = "";
            this.popupWindowShowAction_ToVectorMapFile.ToolTip = null;
            this.popupWindowShowAction_ToVectorMapFile.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ToVectorMapFile_CustomizePopupWindowParams);
            this.popupWindowShowAction_ToVectorMapFile.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ToVectorMapFile_Execute);
            // 
            // simpleAction_UpdateAreaAndLengthFromGeometry
            // 
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Caption = "Recalculate area/length from WKT";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Category = "Tools";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.ConfirmationMessage = "This will (re)caculate area, perimeter (length), centroid, and bounding box for s" +
    "elected objects based on Geometry (WKT) field and overwrite existing values. Pro" +
    "ceed?";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Id = "simpleAction_UpdateAreaAndLengthFromGeometry";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.ToolTip = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateAreaAndLengthFromGeometry_Execute);
            // 
            // popupWindowShowAction_ConvertToEPSG
            // 
            this.popupWindowShowAction_ConvertToEPSG.AcceptButtonCaption = null;
            this.popupWindowShowAction_ConvertToEPSG.CancelButtonCaption = null;
            this.popupWindowShowAction_ConvertToEPSG.Caption = "Convert EPSG";
            this.popupWindowShowAction_ConvertToEPSG.Category = "Tools";
            this.popupWindowShowAction_ConvertToEPSG.ConfirmationMessage = "Convert geometry for all selected records?";
            this.popupWindowShowAction_ConvertToEPSG.Id = "popupWindowShowAction_ConvertToEPSG";
            this.popupWindowShowAction_ConvertToEPSG.ToolTip = "This action will convert the geometry nformation to a specified projection using " +
    "exisiting or specified projection as a source";
            this.popupWindowShowAction_ConvertToEPSG.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ConvertToEPSG_CustomizePopupWindowParams);
            this.popupWindowShowAction_ConvertToEPSG.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ConvertToEPSG_Execute);
            // 
            // popupWindowShowAction_DistanceMatrix
            // 
            this.popupWindowShowAction_DistanceMatrix.AcceptButtonCaption = null;
            this.popupWindowShowAction_DistanceMatrix.CancelButtonCaption = null;
            this.popupWindowShowAction_DistanceMatrix.Caption = "Distance matrix";
            this.popupWindowShowAction_DistanceMatrix.Category = "Tools";
            this.popupWindowShowAction_DistanceMatrix.ConfirmationMessage = null;
            this.popupWindowShowAction_DistanceMatrix.Id = "popupWindowShowAction_DistanceMatrix";
            this.popupWindowShowAction_DistanceMatrix.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_DistanceMatrix.ToolTip = null;
            this.popupWindowShowAction_DistanceMatrix.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_DistanceMatrix_CustomizePopupWindowParams);
            this.popupWindowShowAction_DistanceMatrix.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_DistanceMatrix_Execute);
            // 
            // simpleAction_UpdateLatLongFromWKT
            // 
            this.simpleAction_UpdateLatLongFromWKT.Caption = "Recalculate Lat/Long from WKT";
            this.simpleAction_UpdateLatLongFromWKT.Category = "Tools";
            this.simpleAction_UpdateLatLongFromWKT.ConfirmationMessage = "This will (re)caculate Lat/Long for selected objects based on Geometry (WKT) fiel" +
    "d and overwrite existing values. Proceed?";
            this.simpleAction_UpdateLatLongFromWKT.Id = "simpleAction_UpdateLatLongFromWKT";
            this.simpleAction_UpdateLatLongFromWKT.ToolTip = null;
            this.simpleAction_UpdateLatLongFromWKT.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateLatLongFromWKT_Execute);
            // 
            // IWKT_ViewController
            // 
            this.Actions.Add(this.simpleAction_MapUnits);
            this.Actions.Add(this.popupWindowShowAction_GE);
            this.Actions.Add(this.popupWindowShowAction_ToVectorMapFile);
            this.Actions.Add(this.simpleAction_UpdateAreaAndLengthFromGeometry);
            this.Actions.Add(this.popupWindowShowAction_ConvertToEPSG);
            this.Actions.Add(this.popupWindowShowAction_DistanceMatrix);
            this.Actions.Add(this.simpleAction_UpdateLatLongFromWKT);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction simpleAction_MapUnits;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GE;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ToVectorMapFile;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateAreaAndLengthFromGeometry;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ConvertToEPSG;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DistanceMatrix;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateLatLongFromWKT;
    }
}
