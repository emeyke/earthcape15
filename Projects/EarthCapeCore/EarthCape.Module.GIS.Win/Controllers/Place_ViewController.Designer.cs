namespace EarthCape.Module.GIS.Win
{
    partial class Place_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_RecordVisit = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_RecordVisit
            // 
            this.popupWindowShowAction_RecordVisit.AcceptButtonCaption = null;
            this.popupWindowShowAction_RecordVisit.CancelButtonCaption = null;
            this.popupWindowShowAction_RecordVisit.Caption = "Record visit";
            this.popupWindowShowAction_RecordVisit.Category = "Tools";
            this.popupWindowShowAction_RecordVisit.ConfirmationMessage = null;
            this.popupWindowShowAction_RecordVisit.Id = "popupWindowShowAction_Record";
            this.popupWindowShowAction_RecordVisit.ImageName = null;
            this.popupWindowShowAction_RecordVisit.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.popupWindowShowAction_RecordVisit.Shortcut = null;
            this.popupWindowShowAction_RecordVisit.Tag = null;
            this.popupWindowShowAction_RecordVisit.TargetObjectsCriteria = null;
            this.popupWindowShowAction_RecordVisit.TargetViewId = null;
            this.popupWindowShowAction_RecordVisit.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.popupWindowShowAction_RecordVisit.ToolTip = null;
            this.popupWindowShowAction_RecordVisit.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.popupWindowShowAction_RecordVisit.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_RecordVisit_Execute);
            this.popupWindowShowAction_RecordVisit.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_RecordVisit_CustomizePopupWindowParams);
            // 
            // Place_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Module.Place);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_RecordVisit;
    }
}