namespace EarthCape.Module.GIS.Win
{
    partial class MapProject_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            this.simpleAction_LoadMap = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_LoadMap.Caption = "Load map";
            this.simpleAction_LoadMap.Category = "OpenObject";
            this.simpleAction_LoadMap.ConfirmationMessage = null;
            this.simpleAction_LoadMap.Id = "afcaef52-ba77-44a6-b95f-823049b887fb";
            this.simpleAction_LoadMap.ImageName = "google_earth_link";
            this.simpleAction_LoadMap.SelectionDependencyType =
                DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_LoadMap.Shortcut = null;
            this.simpleAction_LoadMap.Tag = null;
            this.simpleAction_LoadMap.TargetObjectsCriteria = null;
            this.simpleAction_LoadMap.TargetViewId = null;
            this.simpleAction_LoadMap.ToolTip = null;
            this.simpleAction_LoadMap.TypeOfView = null;
            this.simpleAction_LoadMap.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_LoadMap_Execute);
        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_LoadMap;
    }
}
