using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using EarthCape.Module;
using System.Collections.ObjectModel;
using EarthCape.Interfaces;
using EarthCape.Module.GIS;
using EarthCape.Module.Win;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapTaxon_ViewController : ViewController
    {
        public MapTaxon_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType = typeof(TaxonomicName);
            this.TargetViewType = ViewType.DetailView;
            this.Activated += new EventHandler(MapTaxon_ViewController_Activated);
            Deactivating += new EventHandler(MapTaxon_ViewController_Deactivating);
        }

        void MapTaxon_ViewController_Deactivating(object sender, EventArgs e)
        {
            View.ControlsCreated -= new EventHandler(View_ControlsCreated);
            View.CurrentObjectChanged -= new EventHandler(View_CurrentObjectChanged);
 
        }

        void MapTaxon_ViewController_Activated(object sender, EventArgs e)
        {
            View.ControlsCreated += new EventHandler(View_ControlsCreated);
            View.CurrentObjectChanged += new EventHandler(View_CurrentObjectChanged);
        }

        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            UpdateInfo();
        }

        void View_ControlsCreated(object sender, EventArgs e)
        {
            UpdateInfo();
        }

        private void UpdateInfo()
        {
            foreach (DetailViewItem item in ((DetailView)View).Items)
            {
                if (item is MapDetailViewItem)
                {
                    User p = ((ObjectSpace)View.ObjectSpace).GetObjectByKey(SecuritySystem.CurrentUser.GetType(), ((ObjectSpace)View.ObjectSpace).GetKeyValue(SecuritySystem.CurrentUser)) as User;
                    MapDetailViewItem mapitem = (MapDetailViewItem)item;
                    MapProject map = null;
                    TaxonomicName taxname = (View.CurrentObject as TaxonomicName);
                    TGIS_ViewerWnd Gis = (mapitem.Control as TGIS_ViewerWnd);
                    Gis.Items.Clear();
                    Group group = null;
                    if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
                    {
                        group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
                        if (group.Map != null)
                        {
                            map = group.Map;
                            GISHelperWin.OpenMapProject(Gis, map);
                        }
                    }
                    TGIS_LayerVector ll = null;
                    if (Gis.Get("TaxonomicName") != null)
                        ll = (TGIS_LayerVector)Gis.Get("TaxonomicName");
                    else
                    {
                        ll = new TGIS_LayerVector();
                        ll.Name = "TaxonomicName";
                        Gis.Add(ll);
                        ll.AddField("Oid", TGIS_FieldType.gisFieldTypeString, 30, 0);
                        //ll.Caption = "TaxonomicName: " + loc.ToString();
                    }
                    ll.Params = GISHelper.GISRandomizeParams();

                    TGIS_LayerVector llSummary = null;
                     Collection<String> coll = new Collection<String>();
                    if (taxname!=null)
                    foreach (Unit ub in taxname.Units)
                    {
                        if (map != null)
                        {
                            GISHelper.PlotUnit(coll, ll, llSummary, ub,group,false);
                            GISHelper.ApplyParams(llSummary, 0);
                        }
                        else
                            GISHelper.PlotUnit(coll, ll, null, ub,group,false);
                    }
                    //GISHelper.MapUnits1(taxname.Units, ll, (TGIS_LayerVector)Gis.Get("MapSummaryLayer"), MapSummaryType.OccurrenceCount);
                   /*foreach (Unit unit in taxname.Units)
                    {
                        if (!String.IsNullOrEmpty(unit.WKT))
                        {
                           

                            // if (ll.FindFirst(ll.Extent, "Oid=" + loc.Oid.ToString()) == null)
                            //{
                            TGIS_Shape shp = GISHelper.AddShapeFromWKT(unit, ll);   //TGIS_Utils.GisCreateShapeFromWKT(loc.WKT);
                            if (shp != null)
                            {
                                ll.AddShape(shp);
                                //shp.SetField("Oid", loc.Oid.ToString());
                            }
                        }   
                    }*/

                    Gis.FullExtent();
                    Gis.Update();
                }
            }
        }

  
    }
}
