﻿using System;
using TatukGIS.NDK;
using System.Drawing;
using EarthCape.Module;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp;
using System.Collections.ObjectModel;
using EarthCape.Interfaces;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using EarthCape.Module.GIS;
using EarthCape.Module.Win;

namespace EarthCape.Module.GIS.Win
{
    public class GISHelperWin
    {
        public static DetailView MapHostView(DetailView view)
        {

            if (view == null) return null;
            foreach (DetailViewItem item in view.Items)
            {
                if (typeof(DetailPropertyEditor).IsAssignableFrom(item.GetType()))
                {
                    DetailPropertyEditor editor = (DetailPropertyEditor)item;

                    string propname = editor.PropertyName;
                    if (propname == "MapWindow")
                    {
                        return (DetailView)editor.Frame.View;
                    }
                }
            }
            return null;
        }
        
        public static void AddToWKT(IWKT obj, TGIS_Point ptg)
        { 
            TGIS_Shape shp = new TGIS_ShapeArc();
            if (!String.IsNullOrEmpty(obj.WKT))
            {
                shp.ImportFromWKT(obj.WKT);
            }
        shp.AddPoint(ptg);
        obj.WKT = shp.ExportToWKT();
        }
        public static void PreparePlotUnit(ObjectSpace os, TGIS_ViewerWnd gis, MapProject map,int EPSG, out TGIS_LayerVector llSummary, out Collection<String> coll, out TGIS_LayerVector ll)
        {
            //   User user = (User)SecuritySystem.CurrentUser;
            //MapProject map=null;
            llSummary = null;
            coll = new Collection<String>();
            if ((TGIS_LayerVector)gis.Get("Units") != null)
            {
                ll = (TGIS_LayerVector)gis.Get("Units");
            }
            else
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Units";
                ll.Params.Labels.Field = "Label";
                ll.SetCSByEPSG(EPSG);
                gis.Add(ll);
            } 
        /*    if (EPSG > 0)
            gis.SetCSByEPSG(EPSG);
            gis.Update();*/
            if ((map != null) && (gis.Items.Count == 0))
            {
                GISHelperWin.OpenMapProject(gis, map);
                gis.Update();
          
            }
 
        }
    /*    public static TGIS_LayerAbstract MapLayer(TGIS_ViewerWnd gis,MapLayer mll, string name)
        {
            TGIS_LayerAbstract ll = null;
            if (mll is MapLayerVector)
            {
                ll = new TGIS_LayerVector();
                ((TGIS_LayerVector)ll).DefaultShapeType = TGIS_ShapeType.gisShapeTypeUnknown;
                gis.Add(ll);
                foreach (Locality item in ((MapLayerVector)mll).Localities)
                {
                    GISHelper.MapLocality((TGIS_LayerVector)ll, item,false);
                }
            }
            if (mll is MapLayerFileItem)
            {
                ll = GISHelper.DoMapLayerFileItem((MapLayerFileItem)mll, name);
                if (ll != null)
                {
                    gis.Add(ll);
                }
            }
            if (ll != null)
            {
                if (mll.MaxScale > 0)
                    ll.Params.MaxScale = mll.MaxScale;
                if (mll.MinScale > 0)
                    ll.Params.MinScale = mll.MinScale;
                if (mll.EPSG > 0)
                    ll.SetCSByEPSG(mll.EPSG);
            }
            return ll;
        }*/
    /*    public static void LoadLayers(TGIS_ViewerWnd gis, MapProject map)
        {
            if (map==null) return;
            if (map.EPSG > 0) gis.SetCSByEPSG(map.EPSG);
            foreach (MapLayer mll in map.Layers)
            {
                if (gis.Get(mll.Name) == null)
                {
                    TGIS_LayerAbstract ll = GISHelperWin.MapLayer(gis,mll, mll.Name);
                    if (ll!=null)
                    if (ll.CS.EPSG < 1)
                        if (map.EPSG > 0)
                            ll.SetCSByEPSG(map.EPSG);
                }
                //ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsClear;
                //ll.Params.Area.OutlineColor = Color.Silver;
            }
            if (map.SummaryLayer != null)
            {
                 GISHelperWin.MapLayer(gis,map.SummaryLayer, "MapSummaryLayer");
                /*ll1.Params.Area.OutlineStyle = TGIS_PenStyle.gisPsClear;
                ll1.Params.Area.Pattern = TGIS_BrushStyle.gisBsSolid;
                ll1.Transparency = 50;
                ll1.Params.Area.OutlineColor = Color.Silver;
                ll1.SaveAll();*/
            //}
            /*
             //ssssss
             //here should layers from the Report.WorkItem.DefaultMap
             OpenFileDialog dlgOpen = new OpenFileDialog();
             dlgOpen.Title = "Select file";
             dlgOpen.Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileTypes.gisFileTypeAll, false);
             dlgOpen.CheckFileExists = true;
             dlgOpen.ShowReadOnly = false;
             dlgOpen.Multiselect = true;
             if (dlgOpen.ShowDialog() == DialogResult.OK)
             {

                 foreach (string item in dlgOpen.FileNames)
                 {
                     TGIS_LayerAbstract ll1 = TGIS_Utils.GisCreateLayer(Path.GetFileName(dlgOpen.FileName), dlgOpen.FileName);
                     //layers.Add(ll1);
                 }
             }
        }*/
        public static void ExportToGE(TGIS_LayerVector layer)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog
            {
                Title = "Export vector layer to Google Earth",
                Filter = "Google Earth|*.KML",
                DefaultExt = "KML"
            })
            {
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    TGIS_LayerVector ll1 = (TGIS_LayerVector)TGIS_Utils.GisCreateLayer("", dlgSave.FileName);
                    ll1.SetCSByEPSG(4326);
                    ll1.ImportLayer(layer, layer.Extent, TGIS_ShapeType.gisShapeTypeUnknown, "", false);
                    ll1 = null;


                    if (File.Exists(dlgSave.FileName))
                    {
                        Process.Start(dlgSave.FileName);
                    }
                    else
                    {
                        throw new Exception("Something went wrong during KML export! File was not created.");
                    }
                }
            }
        }
        public static void ExportToVector(TGIS_LayerVector ll)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog
            {
                Title = "Export vector layer to file",
                Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileTypes.gisFileTypeVector, true),
                DefaultExt = "shp"
            })
            {
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                     TGIS_LayerVector ll1 = (TGIS_LayerVector)TGIS_Utils.GisCreateLayer("", dlgSave.FileName);
                    ll.SaveAll();
                    ll.SaveData();
                    ll1.ImportLayer(ll, ll.Extent, TGIS_ShapeType.gisShapeTypeUnknown, "", false);
                    ll1.SaveAll();
                    ll1.SaveData();
                }
            }
        }
   
   /*     internal static TGIS_ViewerWnd MapObject(DetailView View)
        {
            TGIS_ViewerWnd gis = null;
            foreach (DetailViewItem item in ((DetailView)View).Items)
            {
                if (item is MapDetailViewItem)
                {
                    MapDetailViewItem mapitem = (MapDetailViewItem)item;
                    gis = (mapitem.Control as TGIS_ViewerWnd);
                    MapProject map = (View.CurrentObject as Map);
                    foreach (MapLayer mapLayer in map.Layers)
                    {
                        if (mapLayer is MapLayerVector)
                        {
                            gis.Add(GISHelper.DoMapLayerVector((MapLayerVector)mapLayer, mapLayer.Name));
                        }
                    }
                    gis.FullExtent();
                    gis.Update();

                }
            }
            return gis;
        }
        */

        internal static TGIS_ViewerWnd GetGisWnd(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (DetailViewItem item in detailView.Items)
            {
                if (item is MapDetailViewItem)
                {
                    return (item.Control as TGIS_ViewerWnd);
                }
            }
            return null;
        }
        internal static TGIS_ViewerWnd GetImageWnd(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (DetailViewItem item in detailView.Items)
            {
                if (item is ImageMapDetailViewItem)
                {
                    return (item.Control as TGIS_ViewerWnd);
                }
            }
            return null;
        }
        
        internal static TGIS_GpsNmea GetGPS(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (DetailViewItem item in detailView.Items)
            {
                if (item is MapGPSDetailViewItem)
                {
                    return (item.Control as TGIS_GpsNmea);
                }
            }
            return null;
        }
        internal static TGIS_ControlLegend GetGisLegend(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (DetailViewItem item in detailView.Items)
            {
                if (item is MapLegendDetailViewItem)
                {
                    return (item.Control as TGIS_ControlLegend);
                }
            }
            return null;
        }
        internal static TGIS_ControlScale GetGisScale(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (DetailViewItem item in detailView.Items)
            {
                if (item is MapScaleDetailViewItem)
                {
                    return (item.Control as TGIS_ControlScale);
                }
            }
            return null;
        }

        internal static void OpenMapProject(TGIS_ViewerWnd gis, MapProject map)
        {
            string tempDirectory = Path.GetTempPath();
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
            System.IO.File.WriteAllText(tempFileName, map.ProjectConfig);
            gis.Open(tempFileName);
        }
    }
}
