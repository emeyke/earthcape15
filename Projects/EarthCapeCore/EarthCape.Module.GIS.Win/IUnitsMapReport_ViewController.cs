
 using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Reports;
using DevExpress.XtraReports.UI;
using TatukGIS.NDK;
using System.Drawing;
using System.Drawing.Imaging;
using DevExpress.ExpressApp.Reports.Win;
using EarthCape.Interfaces;
using DevExpress.Data.Filtering;
using System.Collections.ObjectModel;
using DevExpress.XtraPrinting;
using DevExpress.ExpressApp.Actions;
using EarthCape.Module.GIS;
using System.Drawing.Design;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.Xpo;

namespace EarthCape.Module.GIS.Win
{
    public class IUnitsMapReport_ViewController : ViewController 
    {
        private void FindMap(XRControl control)
        {
            foreach (XRControl item in control.Controls)
            {
                if (item is XRMapPictureBox)
                    item.BeforePrint += map_BeforePrint;
                else
                    if (item.Controls.Count > 0)
                        FindMap(item);
            }
        }
        private void Report_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRControl control = (XRControl)sender;
            while (!(control is XafReport) && (control != null))
            {
                control = control.Parent;
            }
            if (!(control is XafReport)) return;
            XafReport rep = (XafReport)(control);
            FindMap(control);
           /* XRMapPictureBox map = (XRMapPictureBox)rep.FindControl("mapPictureBox1", false);
            if (map != null)
            {
                map.BeforePrint += map_BeforePrint;
            }
            */
          /*  XRBarCode1 barcode = (XRBarCode1)e.Report.FindControl("barCode11", false);
            if (barcode != null)
                barcode.BeforePrint += barcode_BeforePrint;*/
        
            ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            CriteriaOperator userCriteria;
            User p = os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            if (rep.CriteriaWrapper != null)
                userCriteria = rep.CriteriaWrapper.CriteriaOperator;
            else
                userCriteria = null;
            CriteriaOperator cr = null;
            User user = (User)(Application.CreateObjectSpace()).GetObject(SecuritySystem.CurrentUser);
            cr = CriteriaHelper.DataAccess_FilterView(rep.DataType, user);
            if (userCriteria != null)
                rep.Filtering.Filter = CriteriaOperator.And(userCriteria, cr).ToString();
            else
                rep.FilterString =cr.ToString();
             //e.Report.DataSource = os.CreateCollection(e.Report.DataType, userCriteria & cr); 
        }
        private void controller_CustomShowPreview(object sender, CustomShowPreviewEventArgs e)
        {
            e.Report.BeforePrint += new System.Drawing.Printing.PrintEventHandler(Report_BeforePrint);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
           WinReportServiceController controller = Frame.GetController<WinReportServiceController>();
            if (controller != null)
            {
                controller.CustomShowPreview += new EventHandler<CustomShowPreviewEventArgs>(controller_CustomShowPreview);
               //    controller.CreateCustomDesignForm +=new EventHandler<CreateCustomDesignFormEventArgs>(controller_CreateCustomDesignForm); 
            }
        }
       protected override void OnDeactivating()
        {
            WinReportServiceController controller = Frame.GetController<WinReportServiceController>();
            if (controller != null)
            {
                controller.CustomShowPreview -= new EventHandler<CustomShowPreviewEventArgs>(controller_CustomShowPreview);
            }
            base.OnDeactivating();
        }
        
        
        public IUnitsMapReport_ViewController()
        {
            
        }
     
        void map_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            User p = ((ObjectSpace)View.ObjectSpace).GetObjectByKey(SecuritySystem.CurrentUser.GetType(), ((ObjectSpace)View.ObjectSpace).GetKeyValue(SecuritySystem.CurrentUser)) as User;
            XRControl control = (XRControl)sender;
            XRMapPictureBox mapBox = ((XRMapPictureBox)sender);
            mapBox.Sizing = ImageSizeMode.StretchImage;
            while (!(control is XafReport) && (control != null))
            {
                control = control.Parent;
            }
            if (!(control is XafReport)) return;
            XafReport rep = (XafReport)(control);
            object obj = rep.GetCurrentRow();
            if (obj == null) return;
            using (TGIS_ViewerWnd gis = new TGIS_ViewerWnd())
            {
                MapProject map = null;
                gis.Width = mapBox.Width;
                gis.Height = mapBox.Height;
                TGIS_LayerVector ll = null;
                TGIS_LayerVector llSummary = null;
                Group group = null;
                if (Value_WindowController.Instance()!=null) if ((!String.IsNullOrEmpty(Value_WindowController.Instance().Group)))
                    group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
                if (group != null)
                    if (group.Map != null)
                    {
                        map = group.Map;
                        GISHelperWin.OpenMapProject(gis, map);
                      }
                if (typeof(IUnits).IsAssignableFrom(obj.GetType()))
                {
                    ll = new TGIS_LayerVector();
                    gis.Add(ll);
                    int maxcount = 0;
                    Collection<String> coll = new Collection<String>();
                    IUnits obj1=(IUnits)View.ObjectSpace.FindObject<BaseObject>(new BinaryOperator("Oid", ((BaseObject)obj).Oid));
                    using (XPCollection<Unit> units = new XPCollection<Unit>(obj1.Units, CriteriaOperator.Parse(mapBox.UnitCriteria)))
                    {
                        /* if (!String.IsNullOrEmpty(mapBox.UnitCriteria))
                                            {
                    		                        
                                               obj1.Units.Load();
                                                obj1.Units.Criteria = CriteriaOperator.Parse(mapBox.UnitCriteria);// CriteriaOperator.And(((IUnits)obj).Units.Criteria, CriteriaOperator.Parse(mapBox.UnitCriteria));
                                            } */
                        foreach (Unit ub in units)
                            //obj1.Units)
                            if (map != null)
                            {
                                GISHelper.PlotUnit(coll, ll, llSummary, ub,group,false);
                                GISHelper.ApplyParams(llSummary, maxcount);
                            }
                            else
                                GISHelper.PlotUnit(coll, ll, null, ub,group,false);
                    }
                    // GISHelper.MapUnits(((IUnits)obj).Units, ll, llSummary, user.MasterObject.DefaultMap.SummaryType);
                }
                
                if (ll != null)
                {
                    ll.Params = GISHelper.GISRandomizeParams();
                    gis.Update();
                    gis.FullExtent();
                    Bitmap _bmp = new Bitmap(mapBox.Width, mapBox.Height, PixelFormat.Format24bppRgb);
                    //_bmp.Save("c:\\users\\evgeniy\\documents\\temp\\_bmp.bmp");
                    gis.PrintBmp(ref _bmp);
                    mapBox.Image = _bmp;
                    // _bmp.Dispose();
                }
                else
                    mapBox.Image = null;
            }
            //_bmp.Dispose();
   
        }
    }
}
