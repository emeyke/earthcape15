using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module
{
   
    [NonPersistent]
    public class IWKTExport2VectorOptions : XPCustomObject
    {
        public IWKTExport2VectorOptions(Session session) : base(session) { }
        private bool _CentroidOnly;
        public bool CentroidOnly
        {
            get
            {
                return _CentroidOnly;
            }
            set
            {
                SetPropertyValue("CentroidOnly", ref _CentroidOnly, value);
            }
        }
        private int _TargetEpsg;
        public int TargetEpsg
        {
            get
            {
                return _TargetEpsg;
            }
            set
            {
                SetPropertyValue("TargetEpsg", ref _TargetEpsg, value);
            }
        }
    }

}
