using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using DevExpress.Persistent.Base;


namespace EarthCape.Module.GIS.Win
{
    [NonPersistent]
    public class MapNewUnit : XPCustomObject
    {
        public MapNewUnit(Session session) : base(session) { }
        private Unit _ExistingUnit;
        [ImmediatePostData]
        public Unit ExistingUnit
        {
            get
            {
                return _ExistingUnit;
            }
            set
            {
                SetPropertyValue("ExistingUnit", ref _ExistingUnit, value);
                if (_ExistingUnit != null)
                {
                    if (String.IsNullOrEmpty(UnitID))
                    UnitID = _ExistingUnit.UnitID;
                    if (TaxonomicName==null)
                        TaxonomicName = _ExistingUnit.TaxonomicName;
                    if (Locality == null)
                        Locality = _ExistingUnit.Locality;
                 }
            }
        }
        private GeometryType _GeometryType=GeometryType.Point;
        public GeometryType GeometryType
        {
            get
            {
                return _GeometryType;
            }
            set
            {
                SetPropertyValue("GeometryType", ref _GeometryType, value);
            }
        }
        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }
        private string _UnitID;
        public string UnitID
        {
            get
            {
                return _UnitID;
            }
            set
            {
                SetPropertyValue("UnitID", ref _UnitID, value);
            }
        }
        private NewUnitPosition _Position=NewUnitPosition.DoubleClickOnTheMapForPostion;
        public NewUnitPosition Position
        {
            get
            {
                return _Position;
            }
            set
            {
                SetPropertyValue("Position", ref _Position, value);
            }
        }
     
        private TaxonomicName _TaxonomicName;
        public TaxonomicName TaxonomicName
        {
            get
            {
                return _TaxonomicName;
            }
            set
            {
                SetPropertyValue("TaxonomicName", ref _TaxonomicName, value);
            }
        }
        private Locality _Locality;
        public Locality Locality
        {
            get
            {
                return _Locality;
            }
            set
            {
                SetPropertyValue("Locality", ref _Locality, value);
            }
        }
        private DateTime _Date=DateTime.Now;
        public DateTime Date
        {
            get
            {
                return _Date;
            }
            set
            {
                SetPropertyValue("Date", ref _Date, value);
            }
        }
        private double _Quantity=1;
        [Custom("DisplayFormat", "{0:D}")]
        public double Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetPropertyValue("Quantity", ref _Quantity, value);
            }
        }
        private string _Host;
        public string Host
        {
            get
            {
                return _Host;
            }
            set
            {
                SetPropertyValue("Host", ref _Host, value);
            }
        }
        private string _UnitType;
        public string UnitType
        {
            get
            {
                return _UnitType;
            }
            set
            {
                SetPropertyValue("UnitType", ref _UnitType, value);
            }
        }
    }

}
