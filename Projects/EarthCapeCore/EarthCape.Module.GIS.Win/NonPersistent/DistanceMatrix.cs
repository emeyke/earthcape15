using System;

using DevExpress.Xpo;


namespace EarthCape.Module.Core
{
    public enum OutputType
    {
        TextFile
    }
    [NonPersistent]
    public class DistanceMatrix : XPCustomObject
    {
        public DistanceMatrix(Session session) : base(session) { }
        private MatrixType _Type = MatrixType.Pairs;
        public MatrixType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                SetPropertyValue("Type", ref _Type, value);
            }
        }
        private OutputType _OutputType=OutputType.TextFile;
        public OutputType OutputType
        {
            get
            {
                return _OutputType;
            }
            set
            {
                SetPropertyValue("OutputType", ref _OutputType, value);
            }
        }
    }

}
