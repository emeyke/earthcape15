using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using DevExpress.Persistent.Base;


namespace EarthCape.Module.GIS.Win
{
    [NonPersistent]
    public class MapTracksOptions : XPCustomObject
    {
        public MapTracksOptions(Session session) : base(session) { }
        private MapProject _MapProject;
        public MapProject MapProject
        {
            get
            {
                return _MapProject;
            }
            set
            {
                SetPropertyValue("MapProject", ref _MapProject, value);
            }
        }
       /* private bool _OrderByDate=true;
        public bool OrderByDate
        {
            get
            {
                return _OrderByDate;
            }
            set
            {
                SetPropertyValue("OrderByDate", ref _OrderByDate, value);
            }
        }*/
    }

}
