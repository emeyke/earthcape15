using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module
{
   
    [NonPersistent]
    public class ConvertToEPSG : XPCustomObject
    {
        public ConvertToEPSG(Session session) : base(session) { }
        private bool _AssignEPSGWithoutConversion;
        public bool AssignEPSGWithoutConversion
        {
            get
            {
                return _AssignEPSGWithoutConversion;
            }
            set
            {
                SetPropertyValue("AssignEPSGWithoutConversion", ref _AssignEPSGWithoutConversion, value);
            }
        }
        private int _SourceEPSG;
        public int SourceEPSG
        {
            get
            {
                return _SourceEPSG;
            }
            set
            {
                SetPropertyValue("SourceEPSG", ref _SourceEPSG, value);
            }
        }
        private int _TargetEpsg;
        public int TargetEPSG
        {
            get
            {
                return _TargetEpsg;
            }
            set
            {
                SetPropertyValue("TargetEPSG", ref _TargetEpsg, value);
            }
        }
    }

}
