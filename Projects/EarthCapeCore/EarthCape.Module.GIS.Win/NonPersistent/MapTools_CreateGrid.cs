using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module.GIS.Win
{

   
    [NonPersistent]
    public class MapTools_CreateGrid : XPCustomObject
    {
        public MapTools_CreateGrid(Session session) : base(session) { }
        private double _Width;
        public double Width
        {
            get
            {
                return _Width;
            }
            set
            {
                SetPropertyValue("Width", ref _Width, value);
            }
        }
        private double _Height;
        public double Height
        {
            get
            {
                return _Height;
            }
            set
            {
                SetPropertyValue("Height", ref _Height, value);
            }
        }
       /* private bool _OnlyForSelectedObjects;
        public bool OnlyForSelectedObjects
        {
            get
            {
                return _OnlyForSelectedObjects;
            }
            set
            {
                SetPropertyValue("OnlyForSelectedObjects", ref _OnlyForSelectedObjects, value);
            }
        }*/
        private bool _ClipToSelectedObjects;
        public bool ClipToSelectedObjects
        {
            get
            {
                return _ClipToSelectedObjects;
            }
            set
            {
                SetPropertyValue("ClipToSelectedObjects", ref _ClipToSelectedObjects, value);
            }
        }
        private bool _ClipToShape;
        public bool ClipToShape
        {
            get
            {
                return _ClipToShape;
            }
            set
            {
                SetPropertyValue("ClipToShape", ref _ClipToShape, value);
            }
        }
        private string _SelectedObjectsNameField="Locality";
        public string SelectedObjectsNameField
        {
            get
            {
                return _SelectedObjectsNameField;
            }
            set
            {
                SetPropertyValue("SelectedObjectsNameField", ref _SelectedObjectsNameField, value);
            }
        }
       
        private int _MarginCells=0;
        public int MarginCells
        {
            get
            {
                return _MarginCells;
            }
            set
            {
                SetPropertyValue("MarginCells", ref _MarginCells, value);
            }
        }
    }

}
