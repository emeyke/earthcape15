using System;

namespace EarthCape.Module.GIS.Win
{
    public enum NewUnitPosition
    {
        CurrentMapViewCenter,
        CurrentGPSPosition,
        DoubleClickOnTheMapForPostion,
        NoShape
    }
}
