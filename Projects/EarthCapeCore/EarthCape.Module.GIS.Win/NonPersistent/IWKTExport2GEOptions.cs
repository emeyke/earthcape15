using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module
{
   
    [NonPersistent]
    public class IWKTExport2GEOptions : XPCustomObject
    {
        public IWKTExport2GEOptions(Session session) : base(session) { }
        private bool _CentroidOnly;
        public bool CentroidOnly
        {
            get
            {
                return _CentroidOnly;
            }
            set
            {
                SetPropertyValue("CentroidOnly", ref _CentroidOnly, value);
            }
        }
    
    }

}
