﻿using System;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp;
using System.Collections.ObjectModel;

using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using EarthCape.Module.GIS;
using EarthCape.Module.Win;
using System.Collections;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Data.Filtering;
using TatukGIS.NDK;
using DevExpress.ExpressApp.Win.Templates;
using TatukGIS.NDK.WinForms;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp.Win.Templates.Bars;

namespace EarthCape.Module.GIS.Win
{
    public class GISHelperWin
    {
        public static DetailView MapHostView(DetailView view)
        {

            if (view == null) return null;
            foreach (ViewItem item in view.Items)
            {
                if (typeof(DashboardViewItem).IsAssignableFrom(item.GetType()))
                {
                    DashboardViewItem editor = (DashboardViewItem)item;

                    if ((((DashboardViewItem)item).InnerView).ObjectTypeInfo.Type.IsAssignableFrom(typeof(MapWindow)))
                        return (DetailView)(((DashboardViewItem)item).View);

                }
            }
            return null;
        }

        public static void AddToWKT(IWKT obj, TGIS_Point ptg)
        {
            TGIS_Shape shp = new TGIS_ShapeArc();
            if (!String.IsNullOrEmpty(obj.WKT))
            {
                shp.ImportFromWKT(obj.WKT);
            }
            shp.AddPoint(ptg);
            obj.WKT = shp.ExportToWKT();
        }


        public static void UpdateControl(TGIS_LayerAbstract extent, TGIS_ViewerWnd gis, IWKT iwkt, MapProject map)
        {
            TGIS_LayerVector ll = null; ;
            if (iwkt is Unit)
                ll = (TGIS_LayerVector)gis.Get("Units");
            if (iwkt is Locality)
                ll = (TGIS_LayerVector)gis.Get("Localities");
            if (iwkt is Store)
                ll = (TGIS_LayerVector)gis.Get("Stores");
            if (iwkt is LocalityVisit)
                ll = (TGIS_LayerVector)gis.Get("LocalityVisit");
            if (iwkt is Project)
                ll = (TGIS_LayerVector)gis.Get("Projects");
            if (ll == null)
            {
                ll = new TGIS_LayerVector();
                ll.DefaultShapeType = TGIS_ShapeType.gisShapeTypePolygon;
                if (gis.CS.EPSG > 0)
                    ll.SetCSByEPSG(gis.CS.EPSG);
                else
                    if (map != null)
                    ll.SetCSByEPSG(map.EPSG);
                if (iwkt.EPSG > 0)
                    // if (ll.CS.EPSG < 1)
                    ll.SetCSByEPSG(iwkt.EPSG);
                if (iwkt is Unit)
                    ll.Name = "Units";
                if (iwkt is Locality)
                    ll.Name = "Localities";
                if (iwkt is Store)
                    ll.Name = "Stores";
                if (iwkt is Project)
                    ll.Name = "Projects";
                if (iwkt is LocalityVisit)
                    ll.Name = "Locality visits";
                ll.Transparency = 70;
                ll.Caption = iwkt.ToString();
                // ll.Transparency = 50;
                gis.Add(ll);
                extent = ll;
            }
            else
            {
                ll.Items.Clear();
            }
            // ll.Caption = iwkt.ToString();
            GISHelper.AddFields(ll);
            if (ll.FindField("ID") == -1)
                ll.AddField("ID", TGIS_FieldType.gisFieldTypeString, 30, 0);
            if (ll.FindField("Label") == -1)
                ll.AddField("Label", TGIS_FieldType.gisFieldTypeString, 30, 0);
            int epsg = 0;
            Project project = null;
            if (iwkt is IProjectObject)
                project = (iwkt as IProjectObject).Project;
            if (iwkt is EarthCape.Module.Core.IDatasetObject)
                if ((iwkt as EarthCape.Module.Core.IDatasetObject).Dataset != null)
                    project = (iwkt as EarthCape.Module.Core.IDatasetObject).Dataset.Project;
            if (project != null)
                if (project.EPSG > 0)
                    epsg = project.EPSG;
            GISHelper.MapCurrentIWKT(ll, iwkt, false, epsg);
            GISHelper.LayerParams(ll);
            // ll.SaveAll();
            //ll.RecalcExtent();
            gis.VisibleExtent = ll.Extent;
            gis.Invalidate();
        }
        /*      public static void PlotTracks(IObjectSpace os, TGIS_LayerVector ll,  Unit unit)
              {
                  unit.Tracks.Sorting = new SortingCollection(unit.Tracks, new SortProperty("DateTime", SortingDirection.Ascending));
                  unit.Tracks.Load();

                  TGIS_Point ptg;
                  TGIS_ShapeArc shp = (TGIS_ShapeArc)ll.CreateShape(TGIS_ShapeType.gisShapeTypeArc);
                  shp.SetField("ECID", unit.Oid.ToString());
                  shp.SetField("Label", unit.UnitID);
                  if (unit.Dataset != null)
                  {
                      shp.SetField("Dataset", unit.Dataset.Name);
                      if (unit.Dataset.Project != null)
                      {
                          shp.SetField("Project", unit.Dataset.Project.Name);
                      }
                  }
                  shp.SetField("ObjectType", "UnitTrack");
                  if (unit.Locality != null)
                      shp.SetField("Locality", unit.Locality.Name);
                  if (unit.TaxonomicName != null)
                      shp.SetField("TaxonomicName", unit.TaxonomicName.FullName);
                  shp.SetField("OwnGeometry", false);
                  shp.SetField("Edited", false);
                  shp.AddPart();
                  int? epsg = null;
                  foreach (UnitTrack track in unit.Tracks)
                  {
                      if (epsg == null)
                          epsg = track.EPSG;
                      if ((track.X != null) && (track.Y != null))
                      {
                          ptg = new TGIS_Point(Convert.ToDouble(track.X), Convert.ToDouble(track.Y));
                          shp.AddPoint(ptg);
                      }
                  }
                  if (epsg == null)
                      if (unit.EPSG > 0)
                          epsg = unit.EPSG;
                  if (epsg > 0)
                      if (ll.CS.EPSG > 0)
                          if (epsg != ll.CS.EPSG)
                          {
                              int TargetEpsg = ll.CS.EPSG;
                              shp = (TGIS_ShapeArc)GISHelper.ConvertEPSG(Convert.ToInt32(epsg), shp, TargetEpsg);
                          }
              }
          */
        public static TGIS_LayerVector CreateLocUnitsLayers(TGIS_ViewerWnd gis, string layername)
        {
            TGIS_LayerVector ll;
            if (string.IsNullOrEmpty(layername)) return null;
            if ((TGIS_LayerVector)gis.Get(layername) != null)
            {
                ll = (TGIS_LayerVector)gis.Get(layername);
                //        ll.Items.Clear();
            }
            else
            {

                ll = new TGIS_LayerVector();
                ll.Name = layername;
                gis.Add(ll);
                GISHelper.AddFields(ll);
            }

            return ll;
        }

        public static void UpdateMap(IObjectSpace os, TGIS_ViewerWnd gis, Project Project, IList list)
        {
            /* if map=null;
                        if (Project != null)
                            if (Project.Map != null)
                                map = Project.Map;*/
            TGIS_LayerVector llSummary;
            Collection<String> coll;
            TGIS_LayerVector ll;
            int epsg = 0;
            GISHelperWin.PreparePlotUnit(os, gis,  epsg, out llSummary, out coll, out ll);
            /*   if (Project != null) if (Project.EPSG > 0) epsg = Project.EPSG;
               if (epsg > 0)
                   ll.SetCSByEPSG(epsg);*/
            // gis.Add(ll);
          //  TGIS_LayerVector ll = GISHelperWin.CreateLocUnitsLayers(gis, GISHelper.GetLayerName(iunits.GetType()));
            if (list != null)
            {
                foreach (IWKT item in list)
                {
                    GISHelper.PlotIWKT(ll, llSummary, item, Project, false);
                }
                //      if (map != null) if (map.EPSG > 0) epsg = map.EPSG;
                //    if (epsg > 0)
                //      ll.SetCSByEPSG(epsg);

                // ll.SaveData();
                //  gis.SaveAll();
                //  gis.Invalidate();

                /*  TGIS_ControlCSSystem dlg;
                  TGIS_HelpEvent he;
          
                  TGIS_ControlCSSystem dlg = new TGIS_ControlCSSystem();
          
                  he = null;
                  if (dlg.Execute(gis.CS, he) == DialogResult.OK)
                      gis.CS. = dlg.CS;
              */
            }
        }

        public static void PreparePlotUnit(IObjectSpace os, TGIS_ViewerWnd gis, int EPSG, out TGIS_LayerVector llSummary, out Collection<String> coll, out TGIS_LayerVector ll)
        {
            //   User user = (User)SecuritySystem.CurrentUser;
            //MapProject map=null;
          //  if ((map != null) && (gis.Items.Count == 0))
            {
           //     GISHelperWin.OpenMapProject(gis, map);

            }
            llSummary = null;
            coll = new Collection<String>();
            if ((TGIS_LayerVector)gis.Get("Units") != null)
            {
                ll = (TGIS_LayerVector)gis.Get("Units");
            }
            else
            {
                ll = new TGIS_LayerVector();
                ll.Name = "Units";
                ll.Params.Labels.Field = "UnitID";
                ll.SetCSByEPSG(EPSG);
                gis.Add(ll);
                GISHelper.AddFields(ll);
            }
            /*    if (EPSG > 0)
                      gis.SetCSByEPSG(EPSG);
                      gis.Invalidate();*/


        }

        public static void ExportToGE(TGIS_LayerVector ll)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog
            {
                Title = "Export vector layer to Google Earth",
                Filter = "Google Earth|*.KML",
                DefaultExt = "KML"
            })
            {

                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    TGIS_LayerVector ll1 = (TGIS_LayerVector)TGIS_Utils.GisCreateLayer("", dlgSave.FileName);
                    ll.SetCSByEPSG(4326);
                    ll.SaveAll();
                    ll.SaveAll();
                    ll.SaveData();
                    ll1.ImportLayer(ll, ll.Extent, TGIS_ShapeType.gisShapeTypeUnknown, "", false);
                    ll1.SaveAll();
                    ll1.SaveData();
                    if (File.Exists(dlgSave.FileName))
                    {
                        Process.Start(dlgSave.FileName);
                    }
                    else
                    {
                        throw new Exception("Something went wrong during KML export! File was not created.");
                    }
                }



            }
        }
        public static void ExportToVector(TGIS_LayerVector ll, int EPSG)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog
            {
                Title = "Export vector layer to file",
                Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileType.gisFileTypeVector, true),
                DefaultExt = "shp"
            })
            {
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    TGIS_LayerVector ll1 = (TGIS_LayerVector)TGIS_Utils.GisCreateLayer("", dlgSave.FileName);
                    if (EPSG > 0)
                        ll.SetCSByEPSG(EPSG);
                    ll.SaveAll();
                    ll.SaveAll();
                    ll.SaveData();
                    ll1.ImportLayer(ll, ll.Extent, TGIS_ShapeType.gisShapeTypeUnknown, "", false);
                    ll1.SaveAll();
                    ll1.SaveData();
                }
            }
        }

        /*     internal static TGIS_ViewerWnd MapObject(DetailView View)
             {
                 TGIS_ViewerWnd gis = null;
                 foreach (ViewItem item in ((DetailView)View).Items)
                 {
                     if (item is MapDetailViewItem)
                     {
                         MapDetailViewItem mapitem = (MapDetailViewItem)item;
                         gis = (mapitem.Control as TGIS_ViewerWnd);
                         MapProject map = (View.CurrentObject as Map);
                         foreach (MapLayer mapLayer in map.Layers)
                         {
                             if (mapLayer is MapLayerVector)
                             {
                                 gis.Add(GISHelper.DoMapLayerVector((MapLayerVector)mapLayer, mapLayer.Name));
                             }
                         }
                         gis.FullExtent();
                         gis.Invalidate();

                     }
                 }
                 return gis;
             }
             */

        public static TGIS_ViewerWnd GetGisWndInMapWindow(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (ViewItem item in detailView.Items)
            {
                if (item is MapDetailViewItem)
                {
                    return (item.Control as TGIS_ViewerWnd);
                }
            }
            return null;
        }
        public static TGIS_ViewerWnd GetImageWnd(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (ViewItem item in detailView.Items)
            {
                if (item is ImageMapDetailViewItem)
                {
                    return (item.Control as TGIS_ViewerWnd);
                }
            }
            return null;
        }
        public static void SelectUnitOnMap(TGIS_LayerVector ll, DevExpress.ExpressApp.ListView units_view)
        {
            if (units_view != null)
            {
                GridListEditor list = units_view.Editor as GridListEditor;
                GridView grid = list.GridView;
                for (int i = 0; i <= grid.RowCount - 1; i++)
                {
                    grid.UnselectRow(i);
                }

                foreach (TGIS_Shape shp_ in ll.Items)
                {
                    if (shp_.IsSelected)
                    {
                        string ecid = shp_.GetField("ECID").ToString();
                        for (int i = 0; i <= grid.RowCount - 1; i++)
                        {
                            Unit unit = (Unit)grid.GetRow(i);
                            if (unit.Oid.ToString() == (ecid))
                            {
                                grid.SelectRow(i);
                                units_view.CurrentObject = units_view.ObjectSpace.FindObject<Unit>(new BinaryOperator("Oid", new Guid(ecid)));

                            }
                        }
                    }
                }
            }
        }
        public static TGIS_GpsNmea GetGPS(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (ViewItem item in detailView.Items)
            {
                if (item is MapGPSDetailViewItem)
                {
                    return (item.Control as TGIS_GpsNmea);
                }
            }
            return null;
        }
        public static TGIS_ControlLegend GetGisLegend(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (ViewItem item in detailView.Items)
            {
                if (item is MapLegendDetailViewItem)
                {
                    return (item.Control as TGIS_ControlLegend);
                }
            }
            return null;
        }
        public static TGIS_ControlScale GetGisScale(DetailView detailView)
        {
            if (detailView == null) return null;
            foreach (ViewItem item in detailView.Items)
            {
                if (item is MapScaleDetailViewItem)
                {
                    return (item.Control as TGIS_ControlScale);
                }
            }
            return null;
        }

        public static void OpenMapProject(TGIS_ViewerWnd gis, MapProject map)
        {
            if (gis == null) return;
            string tempDirectory = Path.GetTempPath();
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
            System.IO.File.WriteAllText(tempFileName, map.ProjectConfig);
            //gis.ProjectFile.UseRelativePath = false;
            gis.Open(tempFileName);
            gis.ProjectFile.UseRelativePath = false;
        }

        internal static DetailView GetMapWindowDetalView(DetailView dv)
        {
            foreach (ViewItem item in dv.Items)
            {
                if (item is DashboardViewItem)
                    if (((DashboardViewItem)item).InnerView != null)
                        if ((((DashboardViewItem)item).InnerView).ObjectTypeInfo.Type.IsAssignableFrom(typeof(MapWindow)))
                            if (((NestedFrameTemplateV2)((DashboardViewItem)item).Control).Visible)
                                return (DetailView)((DashboardViewItem)item).InnerView;
                            else
                                return null;
            }
            return null;
        }



    }
}
