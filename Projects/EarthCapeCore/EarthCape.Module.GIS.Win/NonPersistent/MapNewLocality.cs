using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module.GIS.Win
{

 
    [NonPersistent]
    public class MapNewLocality : XPCustomObject
    {
        public MapNewLocality(Session session) : base(session) { }
        private GeometryType _GeometryType;
        public GeometryType GeometryType
        {
            get
            {
                return _GeometryType;
            }
            set
            {
                SetPropertyValue("GeometryType", ref _GeometryType, value);
            }
        }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
    }

}
