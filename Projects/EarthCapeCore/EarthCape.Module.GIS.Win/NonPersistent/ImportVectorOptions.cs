using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;


namespace EarthCape.Module.GIS.Win
{

 
    [NonPersistent]
    public class ImportVectorOptions : XPCustomObject
    {
        public ImportVectorOptions(Session session) : base(session) { }
       
    }

}
