namespace EarthCape.Module.GIS.Win
{
    partial class MapImport_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ImportVectorMap = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ImportVectorMap
            // 
            this.simpleAction_ImportVectorMap.Caption = "Import vector map";
            this.simpleAction_ImportVectorMap.Category = "Tools";
            this.simpleAction_ImportVectorMap.Id = "simpleAction_ImportVectorMap";
            this.simpleAction_ImportVectorMap.Tag = null;
            this.simpleAction_ImportVectorMap.ToolTip = "Imports locality objects from vector file and assigns them to selected group/proj" +
                "ect";
            this.simpleAction_ImportVectorMap.TypeOfView = null;
            this.simpleAction_ImportVectorMap.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction1_Execute);
            // 
            // MapImport_ViewController
            // 
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImportVectorMap;
   }
}
