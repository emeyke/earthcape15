using System;

using DevExpress.ExpressApp;
using EarthCape.Interfaces;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using DevExpress.ExpressApp.SystemModule;
using System.Drawing;
using System.Drawing.Imaging;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using EarthCape.Module.GIS;
using DevExpress.XtraGrid;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapProject_ViewController : ViewController
    {
        public MapProject_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(MapProject);
            TargetViewType = ViewType.DetailView;
            Activated += MapProject_ViewController_Activated;
            Deactivating += MapProject_ViewController_Deactivating;
        }
        private MapWindow mapWindow;
        private Cloner cloner;
        private TGIS_ViewerMode mode;
        private TGIS_ShapePolygon editing_unit;
        private Unit currentUnit;
        //private TGIS_LayerVector editlayer = null;

        void MapProject_ViewController_Deactivating(object sender, EventArgs e)
        {
            if (View is DetailView)
            {
                View.CurrentObjectChanged -= View_CurrentObjectChanged;
                View.ControlsCreated -= View_ControlsCreated;
            }
        }
        public void SaveEdits()
        {
            TGIS_ViewerWnd gis = null;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            if (mapview == null) return;
            gis = GISHelperWin.GetGisWnd(mapview);



            if (MessageBox.Show("Save edits?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                gis.SaveAll();
                foreach (TGIS_Shape shape in shapes)
                {
                    if (shape.Tag != null)
                    {
                        BaseObject obj = ObjectSpace.FindObject<BaseObject>(new BinaryOperator("Oid", shape.Tag.ToString()));
                        if (obj != null)
                        {
                            if (obj is IWKT)
                            {
                                IWKT iwkt = (IWKT)obj;
                                string wkt = shape.ExportToWKT();
                                iwkt.WKT = wkt;
                            }
                        }
                    }
                }
                gis.Update(); //checking
            }
            else
            {
                //  editlayer.RevertAll();
                //editlayer.Viewer.Editor.();
                gis.RevertAll();
                gis.Editor.EndEdit();

            }
            shapes.Clear();
            /*
          TGIS_ViewerWnd gis = null;
            DetailView mapview = MapHostView((DetailView)View);
            if (mapview == null) return;
            gis = GISHelperWin.GetGisWnd(mapview);
          

            gis.SaveAll();
            //editlayer.SaveAll();
            foreach (TGIS_LayerAbstract item in gis.Items)
            {
                if (item is TGIS_LayerVector)
                {
                    ((TGIS_LayerVector)item).Items
                }
            }
            if (editlayer.Name == "IWKT")
            {
                TGIS_Shape shp = (TGIS_Shape)editlayer.Items[0];
                string wkt = shp.ExportToWKT();
                IWKT loc = (IWKT)(((NestedFrame)Frame).DetailViewItem.CurrentObject);
                loc.WKT = wkt;
                loc.Save();
            }

            return;

            if (MessageBox.Show("Save edits?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                editlayer.SaveAll();
                if (editlayer.Name == "IWKT")
                {
                    TGIS_Shape shp = (TGIS_Shape)editlayer.Items[0];
                    string wkt = shp.ExportToWKT();
                    IWKT loc = (IWKT)(((NestedFrame)Frame).DetailViewItem.CurrentObject);
                    loc.WKT = wkt;
                    loc.Save();
                }
                editlayer.Viewer.Update(); //checking
            }
            else
            {
                //  editlayer.RevertAll();
                //editlayer.Viewer.Editor.();
                editlayer.Viewer.Editor.RevertShape();
                editlayer.Viewer.Editor.EndEdit();

            }*/
        }

        void MapProject_ViewController_Activated(object sender, EventArgs e)
        {
            if (View.CurrentObject == null)
                return;
            cloner = new Cloner();
            mapWindow = ((MapProject)(View.CurrentObject)).MapWindow;
            if (View is DetailView)
            {
                View.CurrentObjectChanged += View_CurrentObjectChanged;
                View.ControlsCreated += View_ControlsCreated;
                ((ObjectSpace)View.ObjectSpace).Committing += ObjectSpace_Committing;
                ((ObjectSpace)View.ObjectSpace).Committed += ObjectSpace_Committed;
                ((ObjectSpace)View.ObjectSpace).RollingBack += ObjectSpace_RollingBack;
                foreach (DetailViewItem item in ((DetailView)View).Items)
                {
                    if (item is ListPropertyEditor)
                    {
                        if (((ListPropertyEditor)item).ListView!=null)
                        if ((((ListPropertyEditor)item).ListView).ObjectTypeInfo.Type.IsAssignableFrom(typeof(Unit)))
                        {
                            (((ListPropertyEditor)item).ListView).SelectionChanged += MapProject_ViewController_UnitObjectChanged;
                        }
                    }
                }
            }

        }

        void ObjectSpace_Committed(object sender, EventArgs e)
        {
           /* TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            try
            {
                if (gis.Editor != null)
                    if (gis.Editor.InEdit) gis.Editor.EndEdit();
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }*/
            RefreshController contr = Frame.GetController<RefreshController>();
            if (contr != null)
                contr.RefreshAction.DoExecute();
         /*   DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr1 = editor.Frame.GetController<MapWindow_ViewController>();
            if (contr1 == null)
            {
                contr1.singleChoiceAction_AddUnit.SelectedIndex = 0;
                contr1.singleChoiceAction_MapMode.SelectedIndex = 1;
            }
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
            gis.Mode = TGIS_ViewerMode.gisZoom;
            gis.Update();
             try
             {
                  if (gis.Editor!=null){
                 gis.SaveAll();
                 gis.SaveData();
                 gis.Editor.EndEdit();}
                 
             }
             catch (Exception ex)
             {
                 
             }
             finally
             {
                 
             }
        */
        }

        void ObjectSpace_RollingBack(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RefreshController contr = Frame.GetController<RefreshController>();
            if (contr != null)
                contr.RefreshAction.DoExecute();
            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr1 = editor.Frame.GetController<MapWindow_ViewController>();
            contr1.singleChoiceAction_AddUnit.SelectedIndex = 0;
            contr1.singleChoiceAction_MapMode.SelectedIndex = 1;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);


            try
            {
                if (gis.Editor != null) gis.Editor.EndEdit();

            }
            catch (Exception ex)
            {

            }
            finally
            { }

        }

        void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
            if (contr == null) return;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
            MapProject imapwin = (MapProject)View.CurrentObject;
            if (imapwin != null)
            {
                string tempDirectory = Path.GetTempPath();
                Directory.CreateDirectory(tempDirectory);
                string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
                gis.ProjectFile.UseRelativePath = false;
                gis.SaveProjectAs(tempFileName);
                using (StreamReader streamReader = new StreamReader(tempFileName))
                {
                    imapwin.ProjectConfig = streamReader.ReadToEnd();
                    streamReader.Close();
                }
            }
        }
        void gis_MouseClick(object sender, MouseEventArgs e)
        {


            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
            if (contr == null) return;
            Session se = mapWindow.Session;


             DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
             TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
             TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
             if (legend.GIS_Layer == null) return;
            //if (gis.Get("IWKT") != null)
            //    legend.GIS_Layer = gis.Get("IWKT");
            if (!((gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector))) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name);
            Point pt = new Point(e.X, e.Y);
            TGIS_Point ptg = gis.ScreenToMap(pt);
            TGIS_Shape shp = ll.Locate(ptg, 5 / gis.Zoom, true);
            if (gis.Mode == TGIS_ViewerMode.gisSelect)
            {
                if (shp == null)
                {
                    ll.DeselectAll();
                    return;
                }
                else
                {
                    shp.IsSelected = true;
                }
                DetailView view = (DetailView)View;
                DevExpress.ExpressApp.ListView units_view = null;
                foreach (ViewItem item in view.Items)
                {
                    ListPropertyEditor units_editor = item as ListPropertyEditor;
                    if (units_editor != null)
                    {
                        object listEditor = units_editor.ListView.Editor;
                        if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                            units_view = units_editor.ListView;
                    }
                }
                GridListEditor list = units_view.Editor as GridListEditor;
                GridView grid = list.GridView;
                for (int i = 0; i < grid.RowCount - 1; i++)
                {
                    grid.UnselectRow(i);
                }
                foreach (TGIS_Shape shp_ in ll.Items)
                {
                    if (shp_.IsSelected)
                    {
                        string ecid = shp_.GetField("ECID").ToString();
                        for (int i = 0; i < grid.RowCount - 1; i++)
                        {
                            Unit unit = (Unit)grid.GetRow(i);
                            if (unit.Oid.ToString() == (ecid))
                            {
                                grid.SelectRow(i);
                            }
                        }
                    }
                }
            }
            if (gis.Mode == TGIS_ViewerMode.gisEdit)
            {
                if (shp != null)
                {
                    if (gis.Editor.CurrentShape==null)
                     try
                    {
                        gis.Editor.EditShape(shp, 0);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    finally
                    {
                        
                    }
                }
            }
            if (gis.Mode == TGIS_ViewerMode.gisSelect)
            {
                ll.DeselectAll();
                if (shp != null)
                {
                    shp.IsSelected = true;
                }
            }
            /*   if (gis.Mode == TGIS_ViewerMode.gisUserDefined)
               {
                   if (contr.singleChoiceAction_AddUnit.SelectedIndex == 3)
                   {
                       if (editingshape != null)
                       {
                           editingshape.AddPoint(ptg);
                           gis.Update();
                       }
                   }
               }*/
            gis.Update();
            /* if (shp!=null){
                 if (e.Button)
             if Shift <> [ssCtrl..ssLeft] then begin
               ll.deselectAll;
             end;
             if shp.IsSelected then begin
         //         ShapeSelected:=shp.createcopy;
         //         bThemeCopyShape.Enabled:=true;
               shp := shp.makeeditable;
               shp.IsSelected := False;
               shp.Invalidate;
               ll.GetShape(shp.uid);
             end else begin
         //         ShapeSelected:=shp.createcopy;
         //         bThemeCopyShape.Enabled:=true;
               shp := shp.makeeditable;
               shp.IsSelected := True;
               shp.Invalidate;
               ll.GetShape(shp.uid);
             end;
           end else begin
             ll.deselectAll;
           end;  
             }*/
        }


        void MapProject_ViewController_UnitObjectChanged(object sender, EventArgs e)
        {
            if (sender == null) return;
             TGIS_ViewerWnd gis = null;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            if (mapview == null) return;
            gis = GISHelperWin.GetGisWnd(mapview);
            if (gis == null) return;
            TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
            if (ll == null) return;
            ll.DeselectAll();
            currentUnit = null;
            foreach (Unit item in (sender as DevExpress.ExpressApp.ListView).SelectedObjects)
            {
                TGIS_Shape shp = ll.FindFirst(gis.VisibleExtent, String.Format("ECID='{0}'", item.Oid));
                if (shp != null)
                    shp.IsSelected = true;
            }
            if ((sender as DevExpress.ExpressApp.ListView).SelectedObjects.Count > 0)
            {
                currentUnit = (Unit)(sender as DevExpress.ExpressApp.ListView).SelectedObjects[0];
            }
            gis.Update();
        }

        void View_ControlsCreated(object sender, EventArgs e)
        {
    //        UpdateControl();
            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
            if (contr == null) return;
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
            MapProject imapwin = (MapProject)View.CurrentObject;

            GISHelperWin.OpenMapProject(gis,imapwin);
        }

        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            //UpdateControl();
        }
        void gis_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            if (contr == null) return;
            Point pt = new Point(e.X, e.Y);
            TGIS_Point ptg = gis.ScreenToMap(pt);
            IWKT iwkt = (IWKT)View.CurrentObject;// (IWKT)(((NestedFrame)Frame).DetailViewItem.CurrentObject);
            MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
            Session se = mapWindow.Session;

            if (contr.singleChoiceAction_AddUnit.SelectedIndex == 1)
            {
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll == null)
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                    return;
                }
                Unit newunit = ((ObjectSpace)View.ObjectSpace).CreateObject<Unit>(); ;
                if (currentUnit != null)
                {
                    if (currentUnit.TaxonomicName != null)
                    {
                        newunit.TaxonomicName = currentUnit.TaxonomicName;
                    }
                    newunit.UnitType = currentUnit.UnitType;
                }
                if (newunit == null) return;

                string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                newunit.WKT = wkt;
                newunit.Save();
                GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                ((IUnits)iwkt).Units.Add(newunit);
                GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.AccessionNumber, newunit.EPSG,false);
                gis.Update();
                contr.singleChoiceAction_AddUnit.SelectedIndex = 0;
                contr.singleChoiceAction_MapMode.SelectedIndex = 1;
                gis.Mode = TGIS_ViewerMode.gisZoom;
            }
            if (contr.singleChoiceAction_AddUnit.SelectedIndex == 3)
            {
                //if (legend.GIS_Layer == null) return;
                // if (!(gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector)) return;
                //TGIS_LayerVector ll = (TGIS_LayerVector)(gis.Get(legend.GIS_Layer.Name));
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                gis.Mode = TGIS_ViewerMode.gisEdit;
                legend.GIS_Layer = ll;
                contr.singleChoiceAction_MapMode.SelectedIndex = 3;
                contr.singleChoiceAction_AddUnit.SelectedIndex = 0;
                gis.Mode = TGIS_ViewerMode.gisEdit;
                Unit newunit = ((ObjectSpace)View.ObjectSpace).CreateObject<Unit>(); ;
                if (currentUnit != null)
                {
                    if (currentUnit.TaxonomicName != null)
                    {
                        newunit.TaxonomicName = currentUnit.TaxonomicName;
                    }
                    newunit.UnitType = currentUnit.UnitType;
                }
                if (newunit == null) return;


                Point pt1 = gis.MapToScreen(ptg);
                pt1.X += 50;
                TGIS_Point ptg1 = gis.ScreenToMap(pt1);

                Point pt2 = pt1;
                pt2.Y += 50;
                TGIS_Point ptg2 = gis.ScreenToMap(pt2);

                Point pt3 = pt2;
                pt3.X -= 50;
                TGIS_Point ptg3 = gis.ScreenToMap(pt3);


                string wkt = String.Format("POLYGON(({0} {1}, {2} {3}, {4} {5}, {6} {7}))", ptg.X, ptg.Y, ptg1.X, ptg1.Y, ptg2.X, ptg2.Y, ptg3.X, ptg3.Y);
                newunit.WKT = wkt;
                newunit.Save();
                GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                ((IUnits)iwkt).Units.Add(newunit);
                TGIS_ShapePolygon editingshape = (TGIS_ShapePolygon)GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.AccessionNumber, newunit.EPSG,false);
                DetailView view = (DetailView)View;
                foreach (ViewItem item in view.Items)
                {
                    ListPropertyEditor units_editor = item as ListPropertyEditor;
                    if (units_editor != null)
                    {
                        object listEditor = units_editor.ListView.Editor;
                        if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                            units_editor.Refresh();
                    }
                }

                gis.Update();
            }

            if (contr.singleChoiceAction_AddUnit.SelectedIndex == 4)
            {
                if (!typeof(IWKT).IsAssignableFrom(View.ObjectTypeInfo.Type)) {
                    contr.singleChoiceAction_MapMode.SelectedIndex = 1;
                    contr.singleChoiceAction_AddUnit.SelectedIndex = 0;
                    return;
                }
                IWKT obj = (IWKT)View.CurrentObject;
               //TGIS_LayerVector ll = (TGIS_LayerVector)(gis.Get(obj.ToString()));
                 TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("IWKT");
                 legend.GIS_Layer = ll;
                 if (ll == null) return;
                gis.Mode = TGIS_ViewerMode.gisEdit;
                contr.singleChoiceAction_MapMode.SelectedIndex = 3;
                contr.singleChoiceAction_AddUnit.SelectedIndex = 0;
                gis.Mode = TGIS_ViewerMode.gisEdit;

                Point pt1 = gis.MapToScreen(ptg);
                pt1.X += 50;
                TGIS_Point ptg1 = gis.ScreenToMap(pt1);

                Point pt2 = pt1;
                pt2.Y += 50;
                TGIS_Point ptg2 = gis.ScreenToMap(pt2);

                Point pt3 = pt2;
                pt3.X -= 50;
                TGIS_Point ptg3 = gis.ScreenToMap(pt3);


                string wkt = String.Format("POLYGON(({0} {1}, {2} {3}, {4} {5}, {6} {7}))", ptg.X, ptg.Y, ptg1.X, ptg1.Y, ptg2.X, ptg2.Y, ptg3.X, ptg3.Y);
                GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                TGIS_ShapePolygon editingshape = (TGIS_ShapePolygon)GISHelper.AddShapeFromWKT(wkt, ll, obj.Oid.ToString(), obj.Oid, obj.ToString(), obj.EPSG,false);
                // contr.singleChoiceAction_AddUnit.SelectedIndex = 0;
                //contr.singleChoiceAction_MapMode.SelectedIndex = 1;
                //gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.Update();
            }
        }

     /*   private void UpdateControl()
        {
            if (View.CurrentObject == null) return;
            TGIS_LayerAbstract extent=null;

            // if (String.IsNullOrEmpty(((MapProject)View.CurrentObject).GetWKT())) return;
            DetailView view = (DetailView)View;
            foreach (DetailViewItem item in view.Items)
            {
                if (typeof(DetailPropertyEditor).IsAssignableFrom(item.GetType()))
                {
                    DetailPropertyEditor editor = (DetailPropertyEditor)item;

                    string propname = editor.PropertyName;
                    if (propname == "MapWindow")
                    {
                        DetailView mapview = (DetailView)editor.Frame.View;
                        MapProject map = null;
                        Group group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
                        if (group != null)
                            map = group.Map;
                        TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
                        TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
                        if (gis == null) return;
                        GISHelperWin.OpenMapProject(gis, map);
                       
                    
                        if (typeof(IWKT).IsAssignableFrom(View.ObjectTypeInfo.Type))
                        {
                          
                            IWKT iwkt = (IWKT)(View.CurrentObject);
                            TGIS_LayerVector loclayer = (TGIS_LayerVector)gis.Get("IWKT");
                            if (gis.Get("IWKT") == null)
                            {
                                loclayer = new TGIS_LayerVector();
                                loclayer.DefaultShapeType = TGIS_ShapeType.gisShapeTypePolygon;
                                if (gis.CS.EPSG > 0)
                                    loclayer.SetCSByEPSG(gis.CS.EPSG);
                                else
                                    if (map != null)
                                        loclayer.SetCSByEPSG(map.EPSG);
                                if (iwkt.EPSG > 0)
                                    if (loclayer.CS.EPSG < 1)
                                        loclayer.SetCSByEPSG(iwkt.EPSG);
                                loclayer.Name = "IWKT";
                                loclayer.Transparency = 50;
                                gis.Add(loclayer);
                                extent = loclayer;
                            }
                            else
                            {
                                loclayer.Items.Clear();
                            }
                            loclayer.Caption = iwkt.ToString();
                            if (loclayer.FindField("ID") == -1)
                                loclayer.AddField("ID", TGIS_FieldType.gisFieldTypeString, 30, 0);
                            if (loclayer.FindField("Label") == -1)
                                loclayer.AddField("Label", TGIS_FieldType.gisFieldTypeString, 30, 0);
                            GISHelper.MapLocality(loclayer, iwkt,false);
                            loclayer.Params.Labels.Field = "Label";
                            // loclayer.SaveAll();
                            loclayer.RecalcExtent();
                            TGIS_LayerVector ll;
                            if ((TGIS_LayerVector)gis.Get("Units") != null)
                            {
                                ll = (TGIS_LayerVector)gis.Get("Units");
                                ll.Items.Clear();
                            }
                            else
                            {

                                ll = new TGIS_LayerVector();
                                ll.Name = "Units";
                                 //ll.PaintShape += new TGIS_PaintShapeEvent(ll_PaintShape);
                                ll.Transparency = 60;
                                ll.Params.Labels.Position =
                                    TGIS_LabelPositions.gisLabelPositionFlow |
                                    TGIS_LabelPositions.gisLabelPositionUpRight |
                                    TGIS_LabelPositions.gisLabelPositionDownLeft |
                                    TGIS_LabelPositions.gisLabelPositionDownRight |
                                    TGIS_LabelPositions.gisLabelPositionDownLeft;
                                 if (gis.CS.EPSG > 0)
                                    ll.SetCSByEPSG(gis.CS.EPSG);
                                else
                                    if (map != null)
                                        ll.SetCSByEPSG(map.EPSG);
                                gis.Add(ll);
                               //  ll.SaveAll();
                                //gis.VisibleExtent=ll.Extent;
                               // gis.Update();
                            }
                            if (typeof(IUnits).IsAssignableFrom(iwkt.GetType()))
                                foreach (Unit unit in ((IUnits)iwkt).Units)
                                {
                                    if (!string.IsNullOrEmpty(unit.WKT))
                                    {
                                        TGIS_Shape shp = GISHelper.AddShapeFromWKT(unit.WKT, ll, unit.Oid.ToString(), unit.Oid, unit.AccessionNumber, unit.EPSG,false);

                                    }
                                }
                          //  gis.Update();
                            ll.Params.Labels.Field = "Label";
                     
                        }
                      
                        if (View.CurrentObject is Map)
                        {
                            foreach (MapLayer layer in ((Map)(View.CurrentObject)).Layers)
                            {
                                if (gis.Get(layer.Name) == null)
                                    GISHelperWin.MapLayer(gis, layer, layer.Name);
                            }
                            if (((Map)(View.CurrentObject)).EPSG > 0) gis.SetCSByEPSG(((Map)(View.CurrentObject)).EPSG);
                            gis.FullExtent();
                        }
                        if (View.CurrentObject is MapLayer)
                        {
                            MapLayer maplayer = (View.CurrentObject as MapLayer);
                            TGIS_LayerAbstract ll = null;
                            if (maplayer is MapLayer)
                                if (gis.Get(maplayer.Name) == null)
                                {
                                    GISHelperWin.MapLayer(gis, maplayer, maplayer.Name);
                                    if (((MapLayer)(View.CurrentObject)).EPSG > 0) gis.SetCSByEPSG(((MapLayer)(View.CurrentObject)).EPSG);
                                }
                            gis.FullExtent();
                        }
                           if (typeof(IUnits).IsAssignableFrom(View.ObjectTypeInfo.Type))
                        {
                            // GISHelperWin.MapUnits(mapview.ObjectSpace, gis, (IUnits)View.CurrentObject, map);
                        }
                        //  gis.SaveAll();
                        if (map != null)
                            if (map.EPSG > 0)
                                gis.SetCSByEPSG(map.EPSG);
                        gis.RecalcExtent();
                        gis.Refresh();
                        gis.Update();
                        if (gis.Items.Count > 0)
                        {
                            legend.GIS_Layer = gis.Get(((TGIS_LayerAbstract)gis.Items[0]).Name);
                        }
                        mapview.Refresh();
                        if (extent != null)
                            gis.VisibleExtent = extent.Extent;
                        gis.MouseClick += gis_MouseClick;
                        gis.MouseDoubleClick += gis_MouseDoubleClick;
                        gis.EditorChange += gis_EditorChange;
                        //legend.LayerSelect += new TGIS_LayerEvent(legend_LayerSelect);
                    }
                }
            }
        }*/

        void legend_LayerSelect(object _sender, TGIS_LayerEventArgs _e)
        {
            return;


            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
               DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
         TGIS_ControlLegend legend = GISHelperWin.GetGisLegend(mapview);
            if (contr != null)
            {
                if (legend.GIS_Layer == null)
                {
                    contr.singleChoiceAction_AddUnit.Items[0].Caption = "<Select layer in the legend>";
                }
                else
                {
                  //  contr.singleChoiceAction_AddUnit.Items[0].Caption = String.Format("Change shape for {0}", legend.GIS_Layer.Name);
                }
                //todo: doesnt update caption properly
            }
        }
        List<TGIS_Shape> shapes = new List<TGIS_Shape>();

        void gis_EditorChange(object sender, EventArgs e)
        {
            ((ObjectSpace)View.ObjectSpace).SetModified(View.CurrentObject);
            DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
            // if (popupWindowShowAction_RecordUnitByName.Tag == null) return;
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd(mapview);
            DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
            MapWindow_ViewController contr = editor.Frame.GetController<MapWindow_ViewController>();
            if (contr == null) return;

            /* if (editingshape != null)
             { }
             else*/
            {
                TGIS_Shape gisEditorCurrentShape = (TGIS_Shape)gis.Editor.CurrentShape;
                string oid = gisEditorCurrentShape.GetField("ECID").ToString();
                Guid guid = new Guid(oid);
                if (!String.IsNullOrEmpty(oid))
                {
                    BaseObject obj = ObjectSpace.FindObject<BaseObject>(new BinaryOperator("Oid", guid));
                    if (obj == null)
                    {
                        obj = ObjectSpace.FindObject<BaseObject>(new BinaryOperator("Oid", guid), true);
                    }
                        if (obj != null)
                        {
                            if (obj is IWKT)
                            {
                                IWKT iwkt = (IWKT)obj;
                                string wkt = gisEditorCurrentShape.ExportToWKT();
                                if (gisEditorCurrentShape is TGIS_ShapePolygon)
                                    iwkt.Area = ((TGIS_ShapePolygon)gisEditorCurrentShape).Area();
                                iwkt.WKT = wkt;
                            }
                        }
                   
                }
                //((TGIS_LayerVector)gis.Editor.Layer).SaveAll();
            }
            mapview.Refresh();
        }

        void ll_PaintShape(object _sender, TGIS_PaintShapeEventArgs _e)
        {
            if (_e.Shape.GetField("ECID") == null) return;
            TGIS_Shape shp = _e.Shape;
            shp.Params.Marker.Bitmap = null;
            BaseObject obj = ((ObjectSpace)View.ObjectSpace).FindObject<BaseObject>(new BinaryOperator("Oid", shp.GetField("ECID")));
            if (obj is Unit)
            {
                Unit unit = (Unit)obj;
                if (unit.TaxonomicName != null)
                    if (unit.TaxonomicName.MapIcon != null)
                    {
                        //  Bitmap _bmp = new Bitmap(unit.TaxonomicName.MapIcon.Width, unit.TaxonomicName.MapIcon.Height, PixelFormat.Format24bppRgb);
                        // Bitmap bmp = new Bitmap(unit.TaxonomicName.MapIcon);
                        //todo doesnt work
                        shp.Params.Marker.Bitmap = (Bitmap)unit.TaxonomicName.MapIcon;
                        shp.Params.Marker.Size = 250;
                    }
            }
        }

        void mapview_ControlsCreated(object sender, EventArgs e)
        {
           // UpdateControl();
     
                     
        }
    }
}
