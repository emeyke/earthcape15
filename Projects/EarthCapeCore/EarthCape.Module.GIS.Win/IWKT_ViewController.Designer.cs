namespace EarthCape.Module.GIS.Win
{
    partial class IWKT_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_MapUnits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_GE = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ToVectorMapFile = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_UpdateAreaAndLengthFromGeometry = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_MapUnits
            // 
            this.simpleAction_MapUnits.AcceptButtonCaption = null;
            this.simpleAction_MapUnits.CancelButtonCaption = null;
            this.simpleAction_MapUnits.Caption = "Map";
            this.simpleAction_MapUnits.Category = "Tools";
            this.simpleAction_MapUnits.ConfirmationMessage = null;
            this.simpleAction_MapUnits.Id = "simpleAction_MapUnits";
            this.simpleAction_MapUnits.ImageName = "BO_Localization";
            this.simpleAction_MapUnits.Shortcut = null;
            this.simpleAction_MapUnits.Tag = null;
            this.simpleAction_MapUnits.TargetObjectsCriteria = null;
            this.simpleAction_MapUnits.TargetViewId = null;
            this.simpleAction_MapUnits.ToolTip = null;
            this.simpleAction_MapUnits.TypeOfView = null;
            this.simpleAction_MapUnits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.simpleAction_MapUnits_CustomizePopupWindowParams);
            this.simpleAction_MapUnits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.simpleAction_MapUnits_Execute);
            // 
            // popupWindowShowAction_GE
            // 
            this.popupWindowShowAction_GE.AcceptButtonCaption = null;
            this.popupWindowShowAction_GE.CancelButtonCaption = null;
            this.popupWindowShowAction_GE.Caption = "Google Earth";
            this.popupWindowShowAction_GE.Category = "Export";
            this.popupWindowShowAction_GE.ConfirmationMessage = null;
            this.popupWindowShowAction_GE.Id = "popupWindowShowAction_GE";
            this.popupWindowShowAction_GE.ImageName = "google_earth_link";
            this.popupWindowShowAction_GE.Shortcut = null;
            this.popupWindowShowAction_GE.Tag = null;
            this.popupWindowShowAction_GE.TargetObjectsCriteria = null;
            this.popupWindowShowAction_GE.TargetViewId = null;
            this.popupWindowShowAction_GE.ToolTip = null;
            this.popupWindowShowAction_GE.TypeOfView = null;
            this.popupWindowShowAction_GE.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_GE_CustomizePopupWindowParams);
            this.popupWindowShowAction_GE.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_GE_Execute);
            // 
            // popupWindowShowAction_ToVectorMapFile
            // 
            this.popupWindowShowAction_ToVectorMapFile.AcceptButtonCaption = null;
            this.popupWindowShowAction_ToVectorMapFile.CancelButtonCaption = null;
            this.popupWindowShowAction_ToVectorMapFile.Caption = "Export to vector";
            this.popupWindowShowAction_ToVectorMapFile.Category = "Export";
            this.popupWindowShowAction_ToVectorMapFile.ConfirmationMessage = null;
            this.popupWindowShowAction_ToVectorMapFile.Id = "popupWindowShowAction_ToVectorMapFile";
            this.popupWindowShowAction_ToVectorMapFile.ImageName = "google_earth_link";
            this.popupWindowShowAction_ToVectorMapFile.Shortcut = null;
            this.popupWindowShowAction_ToVectorMapFile.Tag = null;
            this.popupWindowShowAction_ToVectorMapFile.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ToVectorMapFile.TargetViewId = null;
            this.popupWindowShowAction_ToVectorMapFile.ToolTip = null;
            this.popupWindowShowAction_ToVectorMapFile.TypeOfView = null;
            this.popupWindowShowAction_ToVectorMapFile.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ToVectorMapFile_CustomizePopupWindowParams);
            this.popupWindowShowAction_ToVectorMapFile.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ToVectorMapFile_Execute);
            // 
            // simpleAction_UpdateAreaAndLengthFromGeometry
            // 
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Caption = "Recalculate area";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Category = "Tools";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.ConfirmationMessage = "This will caculate are and perimeter for select objects based on Geometry (WKT) f" +
    "ield and overwrite existing values. Proceed?";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Id = "simpleAction_UpdateAreaAndLengthFromGeometry";
            this.simpleAction_UpdateAreaAndLengthFromGeometry.ImageName = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Shortcut = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Tag = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.TargetObjectsCriteria = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.TargetViewId = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.ToolTip = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.TypeOfView = null;
            this.simpleAction_UpdateAreaAndLengthFromGeometry.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateAreaAndLengthFromGeometry_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction simpleAction_MapUnits;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GE;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ToVectorMapFile;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateAreaAndLengthFromGeometry;
    }
}
