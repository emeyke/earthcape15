namespace EarthCape.Module.GIS.Win
{
    partial class EarthCapeGISWinModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // EarthCapeGISWinModule
            // 
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.Core.EarthCapeModule));
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.GIS.EarthCapeGISModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.PivotGrid.Win.PivotGridWindowsFormsModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.XAF.Modules.ModelMapper.ModelMapperModule));

        }

        #endregion
    }
}
