﻿using System;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using TatukGIS.NDK;
using System.Windows.Forms;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelMapItem : IModelViewItem { }

    [DetailViewItemAttribute(typeof(IModelMapItem))]
    public class MapDetailViewItem : ViewItem
    {
        public MapDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }   

        protected override object CreateControlCore()
        {
            TGIS_ViewerWnd map = new TGIS_ViewerWnd();
            map.Height = 800;
            map.Mode = TGIS_ViewerMode.gisZoom;
            map.BorderStyle = BorderStyle.FixedSingle;
            map.Width = 800;
            map.Name = "gis";
            return map;
        }
    }

}
  



