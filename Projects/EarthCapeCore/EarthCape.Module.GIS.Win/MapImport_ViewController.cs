using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK;
using System.IO;
using EarthCape.Module;
using DevExpress.Xpo;
using EarthCape.Module.GIS;
using EarthCape.Module.Win;

namespace EarthCape.Module.GIS.Win
{
    public partial class MapImport_ViewController : ViewController
    {
        public MapImport_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            Activated += new EventHandler(MapImport_ViewController_Activated);
            this.TargetViewNesting = Nesting.Root;
        }
        void MapImport_ViewController_Activated(object sender, EventArgs e)
        {

        }
        private void simpleAction1_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //todo runs out of memory on country.shp, slow
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Title = "Select file";
            dlgOpen.Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileTypes.gisFileTypeVector, false);
            dlgOpen.DefaultExt = "txt";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = false;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                UnitOfWork uow = new UnitOfWork(((ObjectSpace)View.ObjectSpace).Session.DataLayer);
                //ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
                User u = (uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), uow.GetKeyValue(SecuritySystem.CurrentUser)) as User);
                string name = Path.GetFileName(dlgOpen.FileName);
            /*    MapLayerVector llw = new MapLayerVector(uow);// os.CreateObject<MapLayerVector>();
                llw.Name = name;
                llw.Save();*/
                TGIS_LayerVector ll = null;
                if (Path.GetExtension(dlgOpen.FileName.ToLower()) == ".kml")
                    ll = new TGIS_LayerKML();
                if (Path.GetExtension(dlgOpen.FileName.ToLower()) == ".shp")
                    ll = new TGIS_LayerSHP();
                TGIS_Viewer gis = new TGIS_Viewer();
                ll.Name = "temp";
                ll.Path = dlgOpen.FileName;
                gis.Add(ll);
                TGIS_Shape shp = ll.FindFirst(ll.Extent);
                while (shp != null)
                {
                    shp = shp.MakeEditable();
                    if (shp != null)
                    {
                        string n = "";
                        if (shp.GetField("Name") != null) n = shp.GetField("Name").ToString();
                        if (shp.GetField("ID") != null) n = shp.GetField("ID").ToString();
                        string shpArea = Convert.ToString(shp.Area());
                        string shpWKT = shp.ExportToWKT();
                        Locality loc = Importer.CreateLocality(null, uow, n,"", shpArea, "", "", "", shpWKT, true, "",true,true,"");
                       /* for (int i = 0; i < ll.Fields.Count-1; i++)
                        {
                            if ((ll.FieldInfo(i).Name != "Name") && (ll.FieldInfo(i).Name != "ID"))
                            {
                                TagHelper.CreateTags(null,uow,
                            }
                        }*/
                      /*  if (loc != null)
                        {
                            llw.Localities.Add(loc);
                        }*/
                    }
                    shp.Free();
                    uow.CommitChanges();
                    shp = ll.FindNext();
                }
              //  llw.Save();
                uow.CommitChanges();
            }
        }

    }
}
