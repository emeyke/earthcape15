﻿using System;
using DevExpress.ExpressApp.Editors;
using TatukGIS.NDK;
using DevExpress.ExpressApp.Model;
using System.Windows.Forms;


namespace EarthCape.Module.GIS.Win
{
    public interface IModelGPSItem : IModelViewItem { }

    [DetailViewItemAttribute(typeof(IModelGPSItem))]
    public class MapGPSDetailViewItem : ViewItem
    {
        public MapGPSDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id) {
            CreateControl();
        }
        protected override object CreateControlCore()
        {

            TGIS_GpsNmea map = new TGIS_GpsNmea();
            map.BorderStyle = BorderStyle.None;
            map.Width = 100;
            map.Height = 180;
            map.Name = "gps";

            return map;
        }
    }

}
  



