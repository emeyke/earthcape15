using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Data.Filtering;
using TatukGIS.NDK;
using DevExpress.ExpressApp.Editors;
using System.Collections.ObjectModel;
using EarthCape.Interfaces;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace EarthCape.Module.GIS.Win
{
    public partial class IWKT_ViewController : ViewController
    {
        public IWKT_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType = typeof(IWKT);
            this.TargetViewType = ViewType.ListView;
        }
        private MapProject map = null;
        private void simpleAction_MapUnits_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ObjectSpace obs = (ObjectSpace)Application.CreateObjectSpace();
            MapWindow mapw = obs.CreateObject<MapWindow>();
            if ((e.PopupWindow.View).CurrentObject != null)
                map = obs.FindObject<MapProject>(new BinaryOperator("Oid", ((MapProject)(e.PopupWindow.View).CurrentObject).Oid));
            DetailView dv = Application.CreateDetailView(obs, mapw, true);
            dv.ControlsCreated += dv_ControlsCreated;
            //Frame.SetView(dv);
           e.ShowViewParameters.NewWindowTarget = NewWindowTarget.MdiChild;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
            e.ShowViewParameters.CreatedView = dv;

            


        }

        void dv_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)sender);
            Group group = Value_WindowController.Instance().GetGroup((ObjectSpace)((DetailView)sender).ObjectSpace);
           /* if map=null;
            if (group != null)
                if (group.Map != null)
                    map = group.Map;*/
            TGIS_LayerVector llSummary;
             Collection<String> coll;
             TGIS_LayerVector ll;
             int epsg = 0;
             GISHelperWin.PreparePlotUnit(((ObjectSpace)((DetailView)sender).ObjectSpace), gis, map, epsg, out llSummary, out coll, out ll);
          /*   if (group != null) if (group.EPSG > 0) epsg = group.EPSG;
             if (epsg > 0)
                 ll.SetCSByEPSG(epsg);*/
            // gis.Add(ll);
             foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotUnit(coll, ll, llSummary, item,group,false);
            }
       //      if (map != null) if (map.EPSG > 0) epsg = map.EPSG;
         //    if (epsg > 0)
           //      ll.SetCSByEPSG(epsg);
        
            ll.SaveData();
            ll.SaveAll();
            gis.VisibleExtent = ll.Extent;
            gis.Update();
      
          /*  TGIS_ControlCSSystem dlg;
            TGIS_HelpEvent he;

            TGIS_ControlCSSystem dlg = new TGIS_ControlCSSystem();

            he = null;
            if (dlg.Execute(gis.CS, he) == DialogResult.OK)
                gis.CS. = dlg.CS;
        */
        }


        /*  void dv_ControlsCreated(object sender, EventArgs e)
          {
              TGIS_ViewerWnd gis=GISHelperWin.MapObject(sender as DetailView);
              TGIS_LayerVector ll=new TGIS_LayerVector();
              gis.Add(ll);
              XPCollection coll=(XPCollection)(((ListView)View).CollectionSource.Collection);
              GISHelper.MapUnits2(coll,ll,(TGIS_LayerVector)gis.Get("MapSummaryLayer"),map.SummaryType);
          }*/

        private void simpleAction_MapUnits_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace obs = (ObjectSpace)Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            DevExpress.ExpressApp.ListView view = Application.CreateListView(obs,typeof(MapProject), false);
            e.View = view;

        }

     

        private void popupWindowShowAction_GE_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IWKTExport2GEOptions options = (IWKTExport2GEOptions)e.PopupWindow.View.CurrentObject;
            TGIS_ViewerWnd gis = new TGIS_ViewerWnd();
            Group group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
            MapProject map = null;
            if (group != null)
                if (group.Map != null)
                    map = group.Map;
            TGIS_LayerVector llSummary;
            Collection<String> coll;
            TGIS_LayerVector ll;
            int epsg = 0;
            if (group != null) if (group.EPSG > 0) epsg = group.EPSG;
            if (map != null) if (map.EPSG > 0) epsg = map.EPSG;
            GISHelperWin.PreparePlotUnit((ObjectSpace)View.ObjectSpace, gis, null, epsg, out llSummary, out coll, out ll);
            foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotUnit(coll, ll, llSummary, item, group, options.CentroidOnly);
               }

            /*  ll.SaveData();
              ll.SaveAll();
              gis.VisibleExtent = ll.Extent;
              gis.Update(); */
            TGIS_LayerVector layer = (TGIS_LayerVector)gis.Items[0];
            /* if (group != null)
                 if (group.Map != null)
                     if (group.Map.EPSG > 0)
                         layer.SetCSByEPSG(group.Map.EPSG);*/
            GISHelperWin.ExportToGE(layer);
        }

        private void popupWindowShowAction_GE_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {

            IObjectSpace os = Application.CreateObjectSpace();
            IWKTExport2GEOptions options = os.CreateObject<IWKTExport2GEOptions>();
            e.View = Application.CreateDetailView(os, options);
        }

        private void popupWindowShowAction_ToVectorMapFile_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IWKTExport2VectorOptions options = (IWKTExport2VectorOptions)e.PopupWindow.View.CurrentObject;
            TGIS_ViewerWnd gis = new TGIS_ViewerWnd();
            Group group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
            MapProject map = null;
            if (group != null)
                if (group.Map != null)
                    map = group.Map;
            TGIS_LayerVector llSummary;
            Collection<String> coll;
            TGIS_LayerVector ll;
            int epsg = 0;
            if (group != null) if (group.EPSG > 0) epsg = group.EPSG;
            if (map != null) if (map.EPSG > 0) epsg = map.EPSG;
            if (options.TargetEpsg>0)
            epsg = options.TargetEpsg;
          
            GISHelperWin.PreparePlotUnit((ObjectSpace)View.ObjectSpace, gis, null, epsg, out llSummary, out coll, out ll);
            
            foreach (IWKT item in View.SelectedObjects)
            {
                GISHelper.PlotUnit(coll, ll, llSummary, item, group, options.CentroidOnly);
            }

            /*  ll.SaveData();
              ll.SaveAll();
              gis.VisibleExtent = ll.Extent;
              gis.Update(); */
            TGIS_LayerVector layer = (TGIS_LayerVector)gis.Items[0];
            /* if (group != null)
                 if (group.Map != null)
                     if (group.Map.EPSG > 0)
                         layer.SetCSByEPSG(group.Map.EPSG);*/
            GISHelperWin.ExportToVector(layer);
        }

        private void popupWindowShowAction_ToVectorMapFile_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            IWKTExport2VectorOptions options = os.CreateObject<IWKTExport2VectorOptions>();
            e.View = Application.CreateDetailView(os, options);

        }

        private void simpleAction_UpdateAreaAndLengthFromGeometry_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IWKT iwkt in View.SelectedObjects)
            {
                GISHelper.UpdateAreaAndLengthFromWKT(iwkt);
                iwkt.Save();
            }
            View.ObjectSpace.CommitChanges();
        }
  
      }
}
