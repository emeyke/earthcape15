namespace EarthCape.Module.GIS.Win
{
    partial class MapWindow_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem46 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem47 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem48 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem49 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem50 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem51 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem52 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem53 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem54 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.simpleAction_ExportVectorLayer = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ExportToImage = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.singleChoiceAction_MapMode = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.simpleAction_MapFullExtent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_GPSLocateMe = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_AddLayer = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.parametrizedAction_WMS = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.simpleAction_LayerExtent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ExportToGE = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.singleChoiceAction_AddUnit = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.popupWindowShowAction_DowloadAndMapGbifForExtent = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_ExportVectorLayer
            // 
            this.simpleAction_ExportVectorLayer.Caption = "Export vector layer";
            this.simpleAction_ExportVectorLayer.Category = "Export";
            this.simpleAction_ExportVectorLayer.ConfirmationMessage = null;
            this.simpleAction_ExportVectorLayer.Id = "simpleAction_ExportSummary";
            this.simpleAction_ExportVectorLayer.ImageName = null;
            this.simpleAction_ExportVectorLayer.Shortcut = null;
            this.simpleAction_ExportVectorLayer.Tag = null;
            this.simpleAction_ExportVectorLayer.TargetObjectsCriteria = null;
            this.simpleAction_ExportVectorLayer.TargetViewId = null;
            this.simpleAction_ExportVectorLayer.ToolTip = null;
            this.simpleAction_ExportVectorLayer.TypeOfView = null;
            this.simpleAction_ExportVectorLayer.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ExportSummary_Execute);
            // 
            // popupWindowShowAction_ExportToImage
            // 
            this.popupWindowShowAction_ExportToImage.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportToImage.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportToImage.Caption = "Export to image";
            this.popupWindowShowAction_ExportToImage.Category = "Export";
            this.popupWindowShowAction_ExportToImage.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportToImage.Id = "popupWindowShowAction_ExportToImage";
            this.popupWindowShowAction_ExportToImage.ImageName = null;
            this.popupWindowShowAction_ExportToImage.Shortcut = null;
            this.popupWindowShowAction_ExportToImage.Tag = null;
            this.popupWindowShowAction_ExportToImage.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ExportToImage.TargetViewId = null;
            this.popupWindowShowAction_ExportToImage.ToolTip = null;
            this.popupWindowShowAction_ExportToImage.TypeOfView = null;
            this.popupWindowShowAction_ExportToImage.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ExportToImage_Execute);
            this.popupWindowShowAction_ExportToImage.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ExportToImage_CustomizePopupWindowParams);
            // 
            // singleChoiceAction_MapMode
            // 
            this.singleChoiceAction_MapMode.Caption = "Mode";
            this.singleChoiceAction_MapMode.Category = "View";
            this.singleChoiceAction_MapMode.ConfirmationMessage = null;
            this.singleChoiceAction_MapMode.DefaultItemMode = DevExpress.ExpressApp.Actions.DefaultItemMode.LastExecutedItem;
            this.singleChoiceAction_MapMode.Id = "singleChoiceAction_MapMode";
            this.singleChoiceAction_MapMode.ImageMode = DevExpress.ExpressApp.Actions.ImageMode.UseItemImage;
            this.singleChoiceAction_MapMode.ImageName = null;
            choiceActionItem46.Caption = "Select";
            choiceActionItem46.ImageName = null;
            choiceActionItem47.Caption = "Zoom";
            choiceActionItem47.ImageName = null;
            choiceActionItem48.Caption = "Pan";
            choiceActionItem48.ImageName = null;
            choiceActionItem49.Caption = "Edit";
            choiceActionItem49.ImageName = null;
            this.singleChoiceAction_MapMode.Items.Add(choiceActionItem46);
            this.singleChoiceAction_MapMode.Items.Add(choiceActionItem47);
            this.singleChoiceAction_MapMode.Items.Add(choiceActionItem48);
            this.singleChoiceAction_MapMode.Items.Add(choiceActionItem49);
            this.singleChoiceAction_MapMode.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.singleChoiceAction_MapMode.Shortcut = null;
            this.singleChoiceAction_MapMode.ShowItemsOnClick = true;
            this.singleChoiceAction_MapMode.Tag = null;
            this.singleChoiceAction_MapMode.TargetObjectsCriteria = null;
            this.singleChoiceAction_MapMode.TargetViewId = null;
            this.singleChoiceAction_MapMode.ToolTip = null;
            this.singleChoiceAction_MapMode.TypeOfView = null;
            this.singleChoiceAction_MapMode.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_MapMode_Execute);
            // 
            // simpleAction_MapFullExtent
            // 
            this.simpleAction_MapFullExtent.Caption = "Full extent";
            this.simpleAction_MapFullExtent.Category = "View";
            this.simpleAction_MapFullExtent.ConfirmationMessage = null;
            this.simpleAction_MapFullExtent.Id = "simpleAction_MapFullExtent";
            this.simpleAction_MapFullExtent.ImageName = null;
            this.simpleAction_MapFullExtent.Shortcut = null;
            this.simpleAction_MapFullExtent.Tag = null;
            this.simpleAction_MapFullExtent.TargetObjectsCriteria = null;
            this.simpleAction_MapFullExtent.TargetViewId = null;
            this.simpleAction_MapFullExtent.ToolTip = null;
            this.simpleAction_MapFullExtent.TypeOfView = null;
            this.simpleAction_MapFullExtent.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_MapFullExtent_Execute);
            // 
            // simpleAction_GPSLocateMe
            // 
            this.simpleAction_GPSLocateMe.Caption = "GPS";
            this.simpleAction_GPSLocateMe.Category = "View";
            this.simpleAction_GPSLocateMe.ConfirmationMessage = null;
            this.simpleAction_GPSLocateMe.Id = "simpleAction_GPSLocateMe";
            this.simpleAction_GPSLocateMe.ImageName = null;
            this.simpleAction_GPSLocateMe.Shortcut = null;
            this.simpleAction_GPSLocateMe.Tag = "0";
            this.simpleAction_GPSLocateMe.TargetObjectsCriteria = null;
            this.simpleAction_GPSLocateMe.TargetViewId = null;
            this.simpleAction_GPSLocateMe.ToolTip = null;
            this.simpleAction_GPSLocateMe.TypeOfView = null;
            this.simpleAction_GPSLocateMe.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_GPSLocateMe_Execute);
            // 
            // simpleAction_AddLayer
            // 
            this.simpleAction_AddLayer.Caption = "Add";
            this.simpleAction_AddLayer.Category = "OpenObject";
            this.simpleAction_AddLayer.ConfirmationMessage = null;
            this.simpleAction_AddLayer.Id = "simpleAction_AddLayer";
            this.simpleAction_AddLayer.ImageName = null;
            this.simpleAction_AddLayer.Shortcut = null;
            this.simpleAction_AddLayer.Tag = null;
            this.simpleAction_AddLayer.TargetObjectsCriteria = null;
            this.simpleAction_AddLayer.TargetViewId = null;
            this.simpleAction_AddLayer.ToolTip = null;
            this.simpleAction_AddLayer.TypeOfView = null;
            this.simpleAction_AddLayer.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_AddLayer_Execute);
            // 
            // parametrizedAction_WMS
            // 
            this.parametrizedAction_WMS.Caption = "Open Web Map Service (WMS)";
            this.parametrizedAction_WMS.Category = "OpenObject";
            this.parametrizedAction_WMS.ConfirmationMessage = null;
            this.parametrizedAction_WMS.Id = "parametrizedAction_WMS";
            this.parametrizedAction_WMS.ImageName = null;
            this.parametrizedAction_WMS.ShortCaption = "Add WMS";
            this.parametrizedAction_WMS.Shortcut = null;
            this.parametrizedAction_WMS.Tag = null;
            this.parametrizedAction_WMS.TargetObjectsCriteria = null;
            this.parametrizedAction_WMS.TargetViewId = null;
            this.parametrizedAction_WMS.ToolTip = null;
            this.parametrizedAction_WMS.TypeOfView = null;
            this.parametrizedAction_WMS.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.parametrizedAction_WMS_Execute);
            // 
            // simpleAction_LayerExtent
            // 
            this.simpleAction_LayerExtent.Caption = "Layer extent";
            this.simpleAction_LayerExtent.ConfirmationMessage = null;
            this.simpleAction_LayerExtent.Id = "simpleAction_LayerExtent";
            this.simpleAction_LayerExtent.ImageName = null;
            this.simpleAction_LayerExtent.Shortcut = null;
            this.simpleAction_LayerExtent.Tag = null;
            this.simpleAction_LayerExtent.TargetObjectsCriteria = null;
            this.simpleAction_LayerExtent.TargetViewId = null;
            this.simpleAction_LayerExtent.ToolTip = null;
            this.simpleAction_LayerExtent.TypeOfView = null;
            this.simpleAction_LayerExtent.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_LayerExtent_Execute);
            // 
            // simpleAction_ExportToGE
            // 
            this.simpleAction_ExportToGE.Caption = "Google Earth";
            this.simpleAction_ExportToGE.Category = "Export";
            this.simpleAction_ExportToGE.ConfirmationMessage = null;
            this.simpleAction_ExportToGE.Id = "simpleAction_ExportToGE";
            this.simpleAction_ExportToGE.ImageName = "google_earth_link";
            this.simpleAction_ExportToGE.Shortcut = null;
            this.simpleAction_ExportToGE.Tag = null;
            this.simpleAction_ExportToGE.TargetObjectsCriteria = null;
            this.simpleAction_ExportToGE.TargetViewId = null;
            this.simpleAction_ExportToGE.ToolTip = null;
            this.simpleAction_ExportToGE.TypeOfView = null;
            this.simpleAction_ExportToGE.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ExportToGE_Execute);
            // 
            // singleChoiceAction_AddUnit
            // 
            this.singleChoiceAction_AddUnit.Caption = "Add unit";
            this.singleChoiceAction_AddUnit.Category = "OpenObject";
            this.singleChoiceAction_AddUnit.ConfirmationMessage = null;
            this.singleChoiceAction_AddUnit.Id = "singleChoiceAction_AddUnit";
            this.singleChoiceAction_AddUnit.ImageName = null;
            choiceActionItem50.Caption = "<change shape>";
            choiceActionItem50.ImageName = null;
            choiceActionItem51.Caption = "Create new unit by double clicking the map (point)";
            choiceActionItem51.ImageName = null;
            choiceActionItem52.Caption = "Create new unit in current GPS position (point)";
            choiceActionItem52.ImageName = null;
            choiceActionItem53.Caption = "Create new unit by double clicking the map (area)";
            choiceActionItem53.ImageName = null;
            choiceActionItem54.Caption = "Create new area for current locality (OVERWRITES THE CURRENT AREA)";
            choiceActionItem54.ImageName = null;
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem50);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem51);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem52);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem53);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem54);
            this.singleChoiceAction_AddUnit.Shortcut = null;
            this.singleChoiceAction_AddUnit.Tag = null;
            this.singleChoiceAction_AddUnit.TargetObjectsCriteria = null;
            this.singleChoiceAction_AddUnit.TargetViewId = null;
            this.singleChoiceAction_AddUnit.ToolTip = null;
            this.singleChoiceAction_AddUnit.TypeOfView = null;
            this.singleChoiceAction_AddUnit.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_AddUnit_Execute);
            // 
            // popupWindowShowAction_DowloadAndMapGbifForExtent
            // 
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.AcceptButtonCaption = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.CancelButtonCaption = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Caption = "GBIF data";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Category = "OpenObject";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ConfirmationMessage = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Id = "popupWindowShowAction_DowloadAndMapGbifForExtent";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ImageName = "gbif_icon";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Shortcut = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Tag = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.TargetObjectsCriteria = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.TargetViewId = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.ToolTip = "Downloads GBIF data for current map extent";
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.TypeOfView = null;
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_DowloadAndMapGbifForExtent_Execute);
            this.popupWindowShowAction_DowloadAndMapGbifForExtent.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_DowloadAndMapGbifForExtent_CustomizePopupWindowParams);
            // 
            // MapWindow_ViewController
            // 
            this.Deactivating += new System.EventHandler(this.MapWindow_ViewController_Deactivating);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ExportVectorLayer;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportToImage;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_MapFullExtent;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GPSLocateMe;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_AddLayer;
        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_WMS;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_LayerExtent;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ExportToGE;
        public DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_MapMode;
        public DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_AddUnit;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DowloadAndMapGbifForExtent;
    }
}
