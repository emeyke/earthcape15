﻿using System;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using AForge.Controls;


namespace EarthCape.Module.Field.Win
{
    public interface IModelVideoItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelVideoItem))]
    public class VideoDetailViewItem : ViewItem
    {
        public VideoDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id)
        {
            CreateControl();
        }
        protected override object CreateControlCore()
        {
            VideoSourcePlayer video = new VideoSourcePlayer();
            video.AutoSizeControl = false;
           video.Name = "video";

            return video;
        }
    }


}




