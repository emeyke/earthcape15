﻿using System;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using System.Windows.Forms;
using System.Drawing;
using Emgu.CV.UI;


namespace EarthCape.Module.Field.Win
{
    public interface IModelPictureBoxItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelPictureBoxItem))]
    public class PictureBoxDetailViewItem : ViewItem
    {
        public PictureBoxDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id)
        {
            CreateControl();
        }
        protected override object CreateControlCore()
        {
            ImageBox video = new ImageBox();
            video.Name = "pictureBox";
            video.SizeMode = PictureBoxSizeMode.StretchImage;
            video.BackColor = Color.Black;
            video.Width = 1280;
            video.Height = 720;
             video.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
            return video;
        }
    }


}




