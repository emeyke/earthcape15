using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using EarthCape.Module.Core;

namespace EarthCape.Module.Field.Win
{
    [NonPersistent]
    public class AttachImagesWindow : XPCustomObject
    {
        public AttachImagesWindow(Session session) : base(session) { }
        [NonPersistent]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public VideoCaptureWindow VideoCaptureWindow
        {
            get 
            {  
                return new VideoCaptureWindow(Session);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Value_WindowController.Instance() == null) return;
            Project Project = Value_WindowController.Instance().GetGroup(Session);
           // if (Project != null)
             //   if (Project.FileStore != null)
              //      FileStore = Session.FindObject<FileStore>(new BinaryOperator("Oid", Project.FileStore.Oid));
        }
        private string _ImageComment;
        public string ImageComment
        {
            get
            {
                return _ImageComment;
            }
            set
            {
                SetPropertyValue("ImageComment", ref _ImageComment, value);
            }
        }
        private string _NoteForObject;
        public string NoteForObject
        {
            get
            {
                return _NoteForObject;
            }
            set
            {
                SetPropertyValue("NoteForObject", ref _NoteForObject, value);
            }
        }
        
     /*   private string _ImageComment;
        public string ImageComment
        {
            get
            {
                return _ImageComment;
            }
            set
            {
                SetPropertyValue("ImageComment", ref _ImageComment, value);
            }
        }
        private string _NoteForObject;
        public string NoteForObject
        {
            get
            {
                return _NoteForObject;
            }
            set
            {
                SetPropertyValue("NoteForObject", ref _NoteForObject, value);
            }
        }*/
    /*    private XPCollection<FileItem> _Images=null;
        public XPCollection<FileItem> Images
        {
            get
            {
                LoadImages();
                  return _Images;
            }
        }
        private void LoadImages()
        {
            _Images = new XPCollection<FileItem>(PersistentCriteriaEvaluationBehavior.InTransaction, Session, new BinaryOperator("CreatedBy.Oid", ((User)SecuritySystem.CurrentUser).Oid));
        }*/
    }

}
