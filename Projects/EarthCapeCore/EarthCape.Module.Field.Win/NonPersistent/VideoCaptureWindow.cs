using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;

namespace EarthCape.Module.Field.Win
{
    [NonPersistent]
    public class VideoCaptureWindow : XPCustomObject
    {
        public VideoCaptureWindow(Session session) : base(session) { }       
    }

}
