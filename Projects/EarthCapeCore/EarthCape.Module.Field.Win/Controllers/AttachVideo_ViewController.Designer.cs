namespace EarthCape.Module.Field.Win
{
    partial class AttachVideo_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AttachImages = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AttachImages
            // 
            this.popupWindowShowAction_AttachImages.AcceptButtonCaption = null;
            this.popupWindowShowAction_AttachImages.CancelButtonCaption = null;
            this.popupWindowShowAction_AttachImages.Caption = "Image capture";
            this.popupWindowShowAction_AttachImages.Category = "Tools";
            this.popupWindowShowAction_AttachImages.ConfirmationMessage = null;
            this.popupWindowShowAction_AttachImages.Id = "popupWindowShowAction_AttachImages";
            this.popupWindowShowAction_AttachImages.ImageName = null;
            this.popupWindowShowAction_AttachImages.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AttachImages.Shortcut = null;
            this.popupWindowShowAction_AttachImages.Tag = null;
            this.popupWindowShowAction_AttachImages.TargetObjectsCriteria = null;
            this.popupWindowShowAction_AttachImages.TargetViewId = null;
            this.popupWindowShowAction_AttachImages.ToolTip = null;
            this.popupWindowShowAction_AttachImages.TypeOfView = null;
            this.popupWindowShowAction_AttachImages.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AttachImages_CustomizePopupWindowParams);
            this.popupWindowShowAction_AttachImages.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AttachImages_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AttachImages;
    }
}
