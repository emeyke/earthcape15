using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Editors;
using AForge.Controls;
using AForge.Video.DirectShow;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.Data.Filtering;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using System.Configuration;
using DevExpress.ExpressApp.SystemModule;

namespace EarthCape.Module.Field.Win
{
    public partial class Video_ViewController : ViewController
    {
        public Video_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.Activated += Video_ViewController_Activated;
            this.Deactivated+= new EventHandler(Video_ViewController_Deactivating);
          // TargetViewId = "xx";
            TargetObjectType = typeof(VideoCaptureWindow);
        }

        void Video_ViewController_Deactivating(object sender, EventArgs e)
        {
            if (player != null)
            {
                try
                {
                    if (player.VideoSource != null)
                    {
                        player.VideoSource.Stop();
                    }
                   /*player.VideoSource = null;
                    player.SignalToStop();
                    player.WaitForStop();
                    player.Dispose();*/
                }
                catch (ApplicationException)
                {
                }

            }
        }
        void EnumeratedSupportedFrameSizes(VideoCaptureDevice videoDevice)
        {
            videoDevice.ProvideSnapshots = true;
           // singleChoiceAction_SnapShotResolution.SelectedIndex = 0;
           // singleChoiceAction_SnapShotResolution.Active.SetItemValue("", false);
            singleChoiceAction_SnapShotResolution.Items.Clear();
            if (singleChoiceAction_SnapShotResolution.Items.Count == 0)
            {
                 foreach (VideoCapabilities capabilty in videoDevice.SnapshotCapabilities)
                {
                    ChoiceActionItem choiceitem = new ChoiceActionItem();
                    choiceitem.Caption = capabilty.FrameSize.ToString();
                    choiceitem.Data = capabilty.FrameSize;
                    singleChoiceAction_SnapShotResolution.Items.Add(choiceitem);
                }
            }
            if (singleChoiceAction_SnapShotResolution.Items.Count > 0)
            {
                singleChoiceAction_SnapShotResolution.SelectedIndex = 0;
                singleChoiceAction_SnapShotResolution.Active.SetItemValue("", true);
            }
        
        }

        void Video_ViewController_Activated(object sender, EventArgs e)
        {
            View.ControlsCreated += new EventHandler(View_ControlsCreated);
            singleChoiceAction_VideoStore.SelectedIndex = 0;
            singleChoiceAction_VideoStore.Active.SetItemValue("", false);
            singleChoiceAction_SelectDevice.Items.Clear();
            if (singleChoiceAction_SelectDevice.Items.Count == 0)
            {
                FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo item in videoDevices)
                {
                    ChoiceActionItem choiceitem = new ChoiceActionItem();
                    choiceitem.Caption = item.Name;
                    choiceitem.Data = item.MonikerString;
                    singleChoiceAction_SelectDevice.Items.Add(choiceitem);
                }
                if (singleChoiceAction_SelectDevice.Items.Count > 0)
                {
                    singleChoiceAction_SelectDevice.SelectedIndex = 0;
                 }
            }
        }
        VideoSourcePlayer player = null;
        //VideoCaptureDevice videoSource = null;
        void View_ControlsCreated(object sender, EventArgs e)
        {
            foreach (ViewItem item in ((DetailView)View).Items)
            {
                if (item is VideoDetailViewItem)
                {
                    player = (item.Control as VideoSourcePlayer);
                    if (singleChoiceAction_SelectDevice.Items.Count > 0)
                    {
                        VideoCaptureDevice device = new VideoCaptureDevice(singleChoiceAction_SelectDevice.SelectedItem.Data.ToString());;
                        ActivateDevice(device);
                    }
                    /*try
                              {
                                  // enumerate video devices
                                 // FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

                                  //if (videoDevices.Count == 0)
                                    //  throw new ApplicationException();
                                  //videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
                                  player.SignalToStop();
                                  player.WaitForStop();

                                  // start new video source
                                  player.VideoSource = videoSource;
                                  player.Start();


                              }
                              catch (ApplicationException)
                              {
                              }*/
                }
            }
        }

        private FileItem SaveCapture(XPObjectSpace os, string store, string imageComment)
        {
            Bitmap frame = player.GetCurrentVideoFrame();
            // string path = ConfigurationManager.AppSettings["DefaultFileStore"];
            FileItem item = null;
            if (String.IsNullOrEmpty(store))
            {
                //       if (!Directory.Exists(Path.GetTempPath()))
                //         Directory.CreateDirectory(Path.GetTempPath());
                //   path = Path.GetTempPath();

                item = os.CreateObject<FileItemStored>();
                //        path = String.Format("{0}\\{1}.jpg", store, item.Oid);
            }
            // fileData.(store, "", item.Oid.ToString() + ".jpg", new FileStream(tempFileName, FileMode.Open));
            else
            {
                item = os.CreateObject<FileItemLinked>();
                string path = Path.Combine(store, item.Oid.ToString() + ".jpg");
            }
            if (item is FileItemStored)
            {
                FileDataStored fileData = os.CreateObject<FileDataStored>();
                if (!Directory.Exists(Path.GetTempPath()))
                           Directory.CreateDirectory(Path.GetTempPath());
    string path = Path.GetTempPath();
                path = Path.Combine(path, Guid.NewGuid().ToString()+".jpg");
                using (FileStream fs = new FileStream(path, FileMode.CreateNew))
                {

                    frame.Save(fs, ImageFormat.Jpeg);
                }
                FileDataHelper.LoadFromStream(fileData, Path.GetFileName(path), new FileStream(path, FileMode.Open,FileAccess.Read,FileShare.Read), path);
                //fileData.FileName = path;
                fileData.Save();
                ((FileItemStored)item).File = fileData;
            }
            if (item is FileItemEmbedded)
            {
            }
            if (item is FileItemEmbedded)
            {
                /*FileDataLinked fileData = os.CreateObject<FileDataLinked>();
                 fileData.FileName = path;
                 FileDataHelper.LoadFromStream(fileData, item.Oid.ToString() + ".jpg", new FileStream(path, FileMode.Open), path);
                 fileData.Save();
                ((FileItemLinked)item).File = fileData;*/
            }
            item.Name = String.Format("Camera ({1}) capture {0}", DateTime.Now, singleChoiceAction_SelectDevice.SelectedItem.Caption);
            item.Comment = imageComment;
            item.Save();
            return item;

        }
         private void simpleAction_ImageCapture_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
             //for when embedded in detailview
            if (typeof(BaseObject).IsAssignableFrom((((NestedFrame)Frame).ViewItem.ObjectTypeInfo.Type)))
            {
                XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                string store;
                BaseObject objreferred = os.FindObject<BaseObject>(new BinaryOperator("Oid", ((BaseObject)(((NestedFrame)Frame).ViewItem.CurrentObject)).Oid));
                  //  if (!String.IsNullOrEmpty(objreferred.Project.DefaultFolder))
                    //    store = objreferred.Project.DefaultFolder;
                   // else
                   //     store = null;
                 FileItem fileItem = SaveCapture(os,EarthCapeModule.FileSystemStoreLocation,"");
                fileItem.Save();
                ((IDescribed)objreferred).FileSet.Files.Add(fileItem);
                ((BaseObject)objreferred).Save();              
                os.CommitChanges();
               RefreshController contr=((NestedFrame)Frame).GetController<RefreshController>();
                if (contr!=null) contr.RefreshAction.DoExecute();
            }
            else // when several objects selected in listview
            {
                DevExpress.ExpressApp.View parentView = (((NestedFrame)Frame).ViewItem.View.Tag as DevExpress.ExpressApp.View);
                AttachImagesWindow attach = (AttachImagesWindow)(((NestedFrame)Frame).ViewItem.View.CurrentObject);
                XPObjectSpace os = (XPObjectSpace)parentView.ObjectSpace;
                string store;
                BaseObject objreferred = os.FindObject<BaseObject>(new BinaryOperator("Oid", ((BaseObject)parentView.SelectedObjects[0]).Oid));
               // if ((objreferred.Project!=null) && (!String.IsNullOrEmpty(objreferred.Project.DefaultFolder)))
                //    store = objreferred.Project.DefaultFolder;
               // else
                //    store = null;
                FileItem fileItem = SaveCapture(os, EarthCapeModule.FileSystemStoreLocation, attach.ImageComment);
                foreach (BaseObject item in parentView.SelectedObjects)
                {
                    ((IDescribed)item).FileSet.Files.Add(fileItem);
                    item.Save();
                    if (!String.IsNullOrEmpty(attach.NoteForObject)){
                        Note note = os.CreateObject<Note>();
                        note.Text = attach.NoteForObject;
                        note.Save();
                        ((IDescribed)item).NoteSet.Notes.Add(note);
                        item.Save();
                    }
                }
             os.CommitChanges();
             RefreshController contr = ((NestedFrame)Frame).GetController<RefreshController>();
             if (contr != null) contr.RefreshAction.DoExecute();
            }
            
        }

        private void singleChoiceAction_VideoStore_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {

        }

        private void singleChoiceAction_SelectDevice_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            VideoCaptureDevice device = new VideoCaptureDevice(e.SelectedChoiceActionItem.Data.ToString());

            ActivateDevice(device);
        }

        private void ActivateDevice(VideoCaptureDevice device)
        {
            player.SignalToStop();
            player.WaitForStop();
            if (player.VideoSource!=null)
            player.VideoSource.Stop();
           VideoCapabilities cap = null;
            foreach (VideoCapabilities capability in device.VideoCapabilities)
            {
                if (cap == null) cap = capability;
                else
                    if (cap.FrameSize.Width < capability.FrameSize.Width)
                        cap = capability;

            }
            if (cap != null)
                device.DesiredFrameSize = cap.FrameSize;
            player.VideoSource = device;
            EnumeratedSupportedFrameSizes(device);
            player.Start();
        }

        private void singleChoiceAction_SnapShotResolution_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {

        }
    }
}
