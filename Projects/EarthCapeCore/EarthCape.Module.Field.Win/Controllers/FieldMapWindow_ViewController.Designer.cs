namespace EarthCape.Module.Field.Win
{
    partial class FieldMapWindow_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_GPSLocateMe = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.singleChoiceAction_AddUnit = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // simpleAction_GPSLocateMe
            // 
            this.simpleAction_GPSLocateMe.Caption = "GPS";
            this.simpleAction_GPSLocateMe.Category = "View";
            this.simpleAction_GPSLocateMe.ConfirmationMessage = null;
            this.simpleAction_GPSLocateMe.Id = "simpleAction_GPSLocateMe";
            this.simpleAction_GPSLocateMe.ImageName = "Action_NavigationBar";
            this.simpleAction_GPSLocateMe.Tag = "0";
            this.simpleAction_GPSLocateMe.ToolTip = null;
            this.simpleAction_GPSLocateMe.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_GPSLocateMe_Execute);
            // 
            // singleChoiceAction_AddUnit
            // 
            this.singleChoiceAction_AddUnit.Caption = "a64a4bad-3beb-44b2-9d72-052fafe34e47";
            this.singleChoiceAction_AddUnit.ConfirmationMessage = null;
            this.singleChoiceAction_AddUnit.Id = "a64a4bad-3beb-44b2-9d72-052fafe34e47";
            this.singleChoiceAction_AddUnit.ToolTip = null;
            this.singleChoiceAction_AddUnit.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_AddUnit_Execute);
            // 
            // FieldMapWindow_ViewController
            // 
            this.Actions.Add(this.simpleAction_GPSLocateMe);
            this.Actions.Add(this.singleChoiceAction_AddUnit);
            this.TargetObjectType = typeof(EarthCape.Module.GIS.MapWindow);
            this.TypeOfView = typeof(DevExpress.ExpressApp.View);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GPSLocateMe;
        public DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_AddUnit;
    }
}
