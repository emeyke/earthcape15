using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Editors;
using System.Windows.Forms;

using System.Drawing;
using System.Drawing.Imaging;
using DevExpress.ExpressApp.Xpo;
using EarthCape.Module.Core;
using System.IO;
using DirectShowLib;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Field.Win.Controllers
{
    public partial class DirectShow_ViewController : ViewController
    {

      
        private Capture capture;        //takes images from camera as image frames
        private bool captureInProgress;
        private bool saveToFile;

        private void ProcessFrame(object sender, EventArgs arg)
        {
            Image<Bgr, Byte> ImageFrame = capture.QueryFrame();
            //Image<Bgr, Byte> ImageFrame = capture.RetrieveBgrFrame();
            // ImageBox imageBox = GetImageBox();
            pictureBox2.Image = ImageFrame;
           /* if (saveToFile)
            {
                ImageFrame.Save(@"c:\MyPic.jpg");
                saveToFile = !saveToFile;
            }*/
        }

      

        ImageBox pictureBox2;
        
        public DirectShow_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            Activated += DirectShow_ViewController_Activated;
            TargetObjectType = typeof(VideoCaptureWindow);
            Deactivated += DirectShow_ViewController_Deactivated;
        }

        void DirectShow_ViewController_Deactivated(object sender, EventArgs e)
        {
         //  disposecam();
        }

   /*     private void disposecam()
        {
            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            } 
            if (cam == null) return;
            cam.Dispose();

   
        }
*/
        void DirectShow_ViewController_Activated(object sender, EventArgs e)
        {
            View.ControlsCreated += new EventHandler(View_ControlsCreated);
            simpleAction_Capture.Active[""] = false;
            singleChoiceAction_SelectCam.Items.Clear();
            DsDevice[] capDevices;

            // Get the collection of video devices
            capDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            
            if (singleChoiceAction_SelectCam.Items.Count == 0)
            {
                foreach (DsDevice item in capDevices)
                {
                    ChoiceActionItem choiceitem = new ChoiceActionItem();
                    choiceitem.Caption = item.Name;
                    choiceitem.Data = item.Name;
                    singleChoiceAction_SelectCam.Items.Add(choiceitem);
                }
               /* if (singleChoiceAction_SelectCam.Items.Count > 0)
                {
                    singleChoiceAction_SelectCam.SelectedIndex = 1;
                    ActivateDevice(singleChoiceAction_SelectCam.SelectedIndex);
                    simpleAction_Capture.Active[""] = true;
                    simpleAction_ActivateCam.Caption = "Turn off";
                }*/
            }
            
            View.ControlsCreated += View_ControlsCreated;
      
        }
        void View_ControlsCreated(object sender, EventArgs e)
        {
          
            foreach (ViewItem item in ((DetailView)View).Items)
            {
                if (item is PictureBoxDetailViewItem)
                {
                    pictureBox2 = (item.Control as ImageBox);
                }
            }
         //   ActivateDevice(singleChoiceAction_SelectCam.SelectedIndex);
         //   singleChoiceAction_SelectCam.DoExecute(singleChoiceAction_SelectCam.SelectedItem);
        }
    
     /*   private Bitmap GetIt()
        {
            Cursor.Current = Cursors.WaitCursor;

            // Release any previous buffer
            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            }

            // capture image
            m_ip = cam.Click();
            
            Bitmap b = new Bitmap(cam.Width, cam.Height, cam.Stride, PixelFormat.Format24bppRgb, m_ip);

            // If the image is upsidedown
            b.RotateFlip(RotateFlipType.RotateNoneFlipY);
            Cursor.Current = Cursors.Default;
            return b;

        
        }*/
        private void saveJpeg(string path, Bitmap img, long quality)
        {
            // Encoder parameter for image quality

            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = this.getEncoderInfo("image/jpeg");

            if (jpegCodec == null)
                return;

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            img.Save(path, jpegCodec, encoderParams);
        }

        private ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
        
     /*   private FileItem SaveCapture(XPObjectSpace os, string store, string imageComment, Project project)
        {
           // capture.Grab();
            Image<Bgr, Byte> ImageFrame = capture. QueryFrame();
            FileItem item = null;
            if (String.IsNullOrEmpty(store))
            {
                throw new Exception("File store not defined");
            }
            else
            {
                item = os.CreateObject<FileItemStored>();
                 FileDataStored fileData = os.CreateObject<FileDataStored>();
                if (!Directory.Exists(Path.GetTempPath()))
                    Directory.CreateDirectory(Path.GetTempPath());
                string path = Path.GetTempPath();
                path = Path.Combine(path, Guid.NewGuid().ToString() + ".jpg");
                using (FileStream fs = new FileStream(path, FileMode.CreateNew))
                {

                    ImageFrame.ToBitmap().Save(fs, ImageFormat.Jpeg);
                }
                FileDataHelper.LoadFromStream(fileData, Path.GetFileName(path), new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read), path);
                fileData.Save();
                ((FileItemStored)item).File = fileData;
            }
            if (item is FileItemEmbedded)
            {
            }
            if (item is FileItemEmbedded)
            {
                /*FileDataLinked fileData = os.CreateObject<FileDataLinked>();
                 fileData.FileName = path;
                 FileDataHelper.LoadFromStream(fileData, item.Oid.ToString() + ".jpg", new FileStream(path, FileMode.Open), path);
                 fileData.Save();
                ((FileItemLinked)item).File = fileData;*/
         /*    }
            item.Name = String.Format("Camera ({1}) capture {0}", DateTime.Now, singleChoiceAction_SelectCam.SelectedItem.Caption);
            item.Comment = imageComment;
            item.Save();
            return item;
        
        }*/

        private void simpleAction_Capture_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (typeof(BaseObject).IsAssignableFrom((((NestedFrame)Frame).ViewItem.ObjectTypeInfo.Type)))
            {
                XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                string store = "";
                string exc = "";
                IAttachments objreferred = (IAttachments)((((NestedFrame)Frame).ViewItem.CurrentObject));
                Project project = null;
                project=GeneralHelper.GetCurrentProject(os);
                if (project == null) { 
                if (objreferred is IDatasetObject)
                {
                    exc = "Dataset is not specified for the object "+((BaseObject)objreferred).ToString();
                    if ((objreferred as IDatasetObject).Dataset != null)
                        project = (objreferred as IDatasetObject).Dataset.Project;
                }
                if (objreferred is IProjectObject)
                {
                    exc = "Project is not specified for the object " + ((BaseObject)objreferred).ToString();
                    project = (objreferred as IProjectObject).Project;
                }}
                if (!String.IsNullOrEmpty(project.DefaultFolder))
                    store = project.DefaultFolder;
                if (String.IsNullOrEmpty(store))
                    store = EarthCapeModule.FileSystemStoreLocation;
                if (project == null)
                    throw new Exception("exc");
                FileItem fileItem = SaveCapture(os, store, "", project);
                fileItem.Save();
                if (objreferred is Locality)
                { // workaround for missing fileset in locality bug
                    LocalityFile f = os.CreateObject<LocalityFile>();
                    f.Locality = (Locality)objreferred;
                    f.File=fileItem;
                    f.Save();
                }
                ((BaseObject)objreferred).Save();
                os.CommitChanges();
                /*  RefreshController contr = ((NestedFrame)Frame).GetController<RefreshController>();
                  if (contr != null)
                      if (contr.RefreshAction.Active.ResultValue == true)
                          contr.RefreshAction.DoExecute();*/
            }
            else // when several objects selected in listview
            {
                DevExpress.ExpressApp.View parentView = (((NestedFrame)Frame).ViewItem.View.Tag as DevExpress.ExpressApp.View);
                AttachImagesWindow attach = (AttachImagesWindow)(((NestedFrame)Frame).ViewItem.View.CurrentObject);
                XPObjectSpace os = (XPObjectSpace)parentView.ObjectSpace;
                string store = "";
                Project project = null;
                project = GeneralHelper.GetCurrentProject(os);
                IAttachments objreferred = (IAttachments)parentView.SelectedObjects[0];
                // IAttachments objreferred = (IAttachments)os.FindObject<BaseObject>(new BinaryOperator("Oid", ((BaseObject)parentView.SelectedObjects[0]).Oid));
                if (objreferred is IProjectObject)
                    project = (objreferred as IProjectObject).Project;
                if (objreferred is IDatasetObject)
                    project = (objreferred as IDatasetObject).Dataset.Project;
                if ((project != null) && (!String.IsNullOrEmpty(project.DefaultFolder)))
                    store = project.DefaultFolder;
                if (String.IsNullOrEmpty(store))
                    store = EarthCapeModule.FileSystemStoreLocation;
                Attachment fileItem = SaveCapture(os, store, attach.ImageComment, project);
                foreach (IAttachments item in parentView.SelectedObjects)
                {
                    if (objreferred is Locality)
                    { // workaround for missing fileset in locality bug
                        LocalityFile f = os.CreateObject<LocalityFile>();
                        f.Locality = (Locality)objreferred;
                        f.File = fileItem;
                        f.Save();
                    }
                    if (!String.IsNullOrEmpty(attach.NoteForObject))
                    {
                        LocalityNote note = os.CreateObject<LocalityNote>();
                        note.Text = attach.NoteForObject;
                        // note.NoteSet.ObjectTagged =(BaseObject)item;
                        note.Save();
                        ((Locality)item).Notes.Add(note);
                    }
                    ((BaseObject)item).Save();
                }
                 os.CommitChanges();
                /*  ((DevExpress.ExpressApp.ListView)parentView).CollectionSource.Reload();
                  parentView.Refresh();
                  //parentView.co
               /*   RefreshController contr = ((NestedFrame)Frame).GetController<RefreshController>();
                  if (contr != null)
                      if (contr.RefreshAction.Active.ResultValue == true)
                          contr.RefreshAction.DoExecute();*/
            }


        }

        private void singleChoiceAction_SelectCam_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
           // disposecam();
            simpleAction_ActivateCam.Caption = "Turn on";
            simpleAction_Capture.Active[""] = false;
            if (capture != null)
            {
                System.Windows.Forms.Application.Idle -= ProcessFrame;
                capture.Stop();
                capture.Dispose();
                capture = null;
            }
          }

        private void ActivateDevice(int device_num)
        {
           // disposecam();
            if (capture == null)
            {
                try
                {
                    capture = new Capture(device_num);
                    capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 1024);
                    capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 1280);
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                }
            }
           
            if (capture != null)
            {
                if (captureInProgress)
                {  //if camera is getting frames then stop the capture and set button Text
                    // "Start" for resuming capture
                     System.Windows.Forms.Application.Idle -= ProcessFrame;
                }
                else
                {
                    //if camera is NOT getting frames then start the capture and set button
                    // Text to "Stop" for pausing capture
                     System.Windows.Forms.Application.Idle += ProcessFrame;
                }

                captureInProgress = !captureInProgress;
            }
        }

        private void simpleAction_ActivateCam_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (simpleAction_ActivateCam.Caption == "Turn off")
            {
              //  disposecam();
                simpleAction_ActivateCam.Caption = "Turn on";
                simpleAction_Capture.Active[""] = false;
                if (capture != null)
                {
                    System.Windows.Forms.Application.Idle -= ProcessFrame;
                    capture.Stop();
                    capture.Dispose();
                    capture = null;
                }
            }
            else
            {
                ActivateDevice(singleChoiceAction_SelectCam.SelectedIndex);
                simpleAction_ActivateCam.Caption = "Turn off";
                simpleAction_Capture.Active[""] = true;
            }
            
            //singleChoiceAction_SelectCam.DoExecute(singleChoiceAction_SelectCam.SelectedItem);
        }

       
    }
}
