using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK;
using System.Drawing;
using System.IO;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using EarthCape.Interfaces;
using EarthCape.Taxonomy;
using EarthCape.Module.GIS.Win;
using EarthCape.Module.GIS;

namespace EarthCape.Module.Field.Win
{
    public partial class MapWindow_ViewController : ViewController
    {
        private TGIS_Point lastPointMap;
        //   private TGIS_ShapeArc currShape;
        private TGIS_LayerVector lgps;
        public MapWindow_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(EarthCape.Module.MapWindow);
           // TargetViewType = ViewType.DetailView;
            Activated += MapWindow_ViewController_Activated;
            Deactivating += MapWindow_ViewController_Deactivating;
        }

        void MapWindow_ViewController_Deactivating(object sender, EventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            View.ControlsCreated -= View_ControlsCreated;
            if (GPS != null)
            {
                GPS.Position -= GPS_Position;
                if (GPS.Active) GPS.Active = false;
            }
            View.CurrentObjectChanged -= View_CurrentObjectChanged;

        }

        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            //            throw new NotImplementedException();
        }

        void MapWindow_ViewController_Activated(object sender, EventArgs e)
        {
            View.ControlsCreated += View_ControlsCreated;
            singleChoiceAction_AddUnit.SelectedIndex = 0;
            View.ObjectSpace.Committing += obs_Committing;
            View.ObjectSpace.Committed += new EventHandler(ObjectSpace_Committed);
            /*  TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
             if (gis != null)
             {
                 gis.MouseClick += gis_MouseClick;
                 gis.MouseDoubleClick += gis_MouseDoubleClick;
             }*/
            //simpleAction_GISstopediting.Active.SetItemValue("isediting", false);
            singleChoiceAction_MapMode.SelectedIndex = 1;
            //cap = popupWindowShowAction_RecordUnitByName.Caption;
            //popupWindowShowAction_RecordUnitByName.Active.SetItemValue("Active", (Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace)!=null));
           
        }

        void ObjectSpace_Committed(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void WriteConfig()
        {
            TGIS_ViewerWnd gis_wnd = GISHelperWin.GetGisWnd((DetailView)View);
            string tempDirectory = Path.GetTempPath();
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
            //// gis.ProjectFile.UseRelativePath = false;
            gis_wnd.SaveProjectAs(tempFileName, false);
            // gis.ProjectFile.UseRelativePath = false;
            // gis.SaveProject();
            // gis.
            using (StreamReader streamReader = new StreamReader(tempFileName))
            {
                ((MapWindow)View.CurrentObject).MapProject.ProjectConfig = streamReader.ReadToEnd();
                ((MapWindow)View.CurrentObject).MapProject.Save();
                streamReader.Close();
            }
        }
        void obs_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // e.Cancel = true;
            WriteConfig();
        }
        //string cap = "";
        void legend_LayerSelect(object _sender, TGIS_LayerEventArgs _e)
        {
            if (_e.Layer == null) return;
            simpleAction_ExportVectorLayer.Active[""] = (_e.Layer is TGIS_LayerVector);
            simpleAction_ExportToGE.Active[""] = (_e.Layer is TGIS_LayerVector);
        }
        void View_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
           // gis.MouseClick += gis_MouseClick;
           // gis.MouseDoubleClick += gis_MouseDoubleClick;
            gis.LayerAdd += gis_changed;
            gis.LayerDelete += gis_changed;
            gis.ZoomChange += gis_ZoomChange;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_ControlScale scale = GISHelperWin.GetGisScale((DetailView)View);
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            simpleAction_ExportVectorLayer.Active[""] = false; ;
            simpleAction_ExportToGE.Active[""] = false; ;
            simpleAction_GPSLocateMe.Active[""] = false; ;

            if (scale != null)
                scale.GIS_Viewer = gis;
            if (legend != null)
                legend.GIS_Viewer = gis;
            if (legend != null)
            {
                legend.LayerSelect += legend_LayerSelect;
                legend.LayerParamsChange +=gis_changed;
                if (legend.GIS_Layer != null)
                {
                    simpleAction_ExportVectorLayer.Active[""] = (gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector);
                    simpleAction_ExportToGE.Active[""] = (gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector);
                }
            }
            simpleAction_GPSLocateMe.Active[""] = (GPS != null);
        }

        void gis_ZoomChange(object sender, EventArgs e)
        {
            if (View.CurrentObject != null)
                View.ObjectSpace.SetModified(View.CurrentObject);
        }

        void gis_changed(object _sender, TGIS_LayerEventArgs _e)
        {
            if (View!=null)
            if (View.CurrentObject!=null)
            View.ObjectSpace.SetModified(View.CurrentObject);
        }




        private void simpleAction_ExportSummary_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            GISHelperWin.ExportToVector((TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name));
        }


        private void popupWindowShowAction_ExportToImage_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            using (SaveFileDialog dlgSave = new SaveFileDialog { Title = "Save map image to file", Filter = "JPEG File Interchange Format (*.jpg;*.jpeg)|*.jpg|Tag Image File Format (*.tif;*.tiff)|*.tif|Portable Network Graphic (*.png)|*.png|Window Bitmap (*.bmp)|*.bmp", DefaultExt = "jpg" })
            {
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
                    MapWindowToImage par = ((MapWindowToImage)e.PopupWindow.View.CurrentObject);
                    gis.Update();
                    gis.ExportToImage(dlgSave.FileName, gis.VisibleExtent, par.Width, par.Height, par.Compression, 0, par.Dpi);
                }
            }

        }

        private void popupWindowShowAction_ExportToImage_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace obs = (ObjectSpace)Application.CreateObjectSpace();
            MapWindowToImage param = obs.CreateObject<MapWindowToImage>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;

        }

        private void singleChoiceAction_MapMode_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {

            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            try
            {
                if (gis.Editor!=null)
                if (gis.Editor.InEdit) gis.Editor.EndEdit();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                
            }
            if (e.SelectedChoiceActionItem.Caption == "Select")
                gis.Mode = TGIS_ViewerMode.gisSelect;
            if (e.SelectedChoiceActionItem.Caption == "Zoom")
                gis.Mode = TGIS_ViewerMode.gisZoom;
            if (e.SelectedChoiceActionItem.Caption == "Pan")
                gis.Mode = TGIS_ViewerMode.gisDrag;
            if (e.SelectedChoiceActionItem.Caption == "Edit")
                gis.Mode = TGIS_ViewerMode.gisEdit;
             }

        private void simpleAction_MapFullExtent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            gis.FullExtent();
        }
        private void simpleAction_GPSLocateMe_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            /*   
                    GPS.Com = 5;
                    GPS.BaudRate = 4800;
                    GPS.Active = true;
                    GPS.Position += GPS_Position;
                    return;*/
            TGIS_ViewerWnd GIS = GISHelperWin.GetGisWnd((DetailView)View);
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            if (simpleAction_GPSLocateMe.Tag == "0")
            {
                simpleAction_GPSLocateMe.Caption = "Searching...";
                Group group = Value_WindowController.Instance().GetGroup((ObjectSpace)View.ObjectSpace);
                simpleAction_GPSLocateMe.Tag = "1";
                if ((TGIS_LayerVector)GIS.Get("gps") != null)
                {
                    lgps = (TGIS_LayerVector)GIS.Get("gps");
                    lgps.Items.Clear();
                    //lgps.SaveAll();
                }
                else
                {
                    lgps = new TGIS_LayerVector { Name = "gps" };
                    lgps.Params.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleCross;
                    lgps.Params.Marker.Size = 120;
                    //lgps.SaveAll();
                    lgps.SetCSByEPSG(GIS.CS.EPSG);
                    GIS.Add(lgps);
                }
                //lgps.RecalcExtent();
                //GIS.RecalcExtent();
                try
                {
                    lgps.AddField("Date", TGIS_FieldType.gisFieldTypeString, 10, 0);
                }
                catch
                { }


                try
                {
                    lgps.AddField("Date", TGIS_FieldType.gisFieldTypeString, 10, 0);
                }
                catch
                { }

                   if (group != null)
                {
                         GPS.Com = group.GpsCom;
                        GPS.BaudRate = group.GpsBaudRate;
                 }
                else
                {
                    GPS.Com = 5;
                    GPS.BaudRate = 4800;
                }
                GPS.Position += GPS_Position;

                GPS.Active = true;
                 if (!(GPS.Active))
                 {
                     GPS.BaudRate = 4800;
                     for (int i = 0; i < 50; i++)
                     {
                         GPS.Com = i;
                         GPS.Active = true;
                         if (GPS.Active) break;
                     }
                     }
                 if (!GPS.Active)
                 {
                     MessageBox.Show("Unable to connect to GPS device!");
                     return;
                 }
                 else
                 {
                     MessageBox.Show(String.Format("Com {0} and Baud rate {1} are used.", GPS.Com, GPS.BaudRate));
                 }
                // GetLastPoint(GIS);
                //if (currShape != null) currShape.Free();
                //currShape = (TGIS_ShapeArc)lgps.CreateShape(TGIS_ShapeType.gisShapeTypeArc);
                // currShape.AddPart();
                //currShape.AddPoint(lastPointMap);
                //  GIS.Center = lastPointMap;
                // lgps.AddShape(currShape);
            }
            else
            {
                simpleAction_GPSLocateMe.Tag = "0";
                simpleAction_GPSLocateMe.Caption = "GPS";
                GPS.Active = false;
                lgps = (TGIS_LayerVector)GIS.Get("gps");
                lgps.Items.Clear();
                lgps.SaveAll();
                GIS.Update();
            }
        }


        private void GPS_Position(object sender, EventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            if (GPS.Satellites < 1)
            {
                simpleAction_GPSLocateMe.Caption = String.Format("Searching...", GPS.Satellites);

                return;
            }
            simpleAction_GPSLocateMe.Caption = String.Format("GPS ON (sat: {0})", GPS.Satellites);
            TGIS_ViewerWnd GIS = GISHelperWin.GetGisWnd((DetailView)View);
            TGIS_Point ptg = TGIS_Utils.GisPoint(GPS.Longitude, GPS.Latitude);
            //dist = cs.Datum.Ellipsoid.Distance(ptg, lastPointGps);
            //lastPointGps = ptg;
            lastPointMap = GIS.CS.FromWGS(ptg);

            // if (TGIS_Utils.GisIsPointInsideExtent(lastPointMap,GIS.Extent))

            // GIS.Center = lastPointMap;
            //currShape.AddPoint(lastPointMap);

            TGIS_ShapePoint shp = (TGIS_ShapePoint)lgps.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
            shp.AddPart();
            shp.AddPoint(lastPointMap);
           lgps.SaveData();
            lgps.SaveAll();
            lgps.RecalcExtent();
            GIS.RecalcExtent();
            GIS.Update();

            //    GIS.FullExtent();
        }



        private void simpleAction_AddLayer_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            using (OpenFileDialog dlgOpen = new OpenFileDialog
            {
                Title = "Open layer from file",
                Filter = TGIS_Utils.GisSupportedFiles(TGIS_FileTypes.gisFileTypeAll, false),
              //  CheckFileExists=false,
                Multiselect = true
            })
            {
                if (dlgOpen.ShowDialog() == DialogResult.OK)
                {
                    TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
                    foreach (string item in dlgOpen.FileNames)
                    {
                        string llname = Path.GetFileNameWithoutExtension(item);
                        TGIS_LayerAbstract ll = TGIS_Utils.GisCreateLayer(llname, item);
                        gis.Add(ll);
                    }
                    gis.Update();
                }
            }
        }

        private void parametrizedAction_WMS_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            string llname = (String)e.ParameterCurrentValue;
            TGIS_LayerWMS ll = (TGIS_LayerWMS)TGIS_Utils.GisCreateLayer(llname, llname);
            gis.Add(ll);
            gis.Update();
        }

        private void simpleAction_LayerExtent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (legend.GIS_Layer != null)
                gis.VisibleExtent = gis.Get(legend.GIS_Layer.Name).Extent;

        }

        private void simpleAction_ExportToGE_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (gis == null) return;
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            if (legend == null) return;
            if (!(gis.Get(legend.GIS_Layer.Name) is TGIS_LayerVector)) return;
            GISHelperWin.ExportToGE((TGIS_LayerVector)gis.Get(legend.GIS_Layer.Name));

        }

        private void simpleAction_GISstopediting_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (gis.Editor.InEdit)
            {
                 MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).DetailViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    contr.SaveEdits(); 
                    //simpleAction_GISstopediting.Active.SetItemValue("isediting", false);
            }
            //TODO NULL EXCETION HERE gis.Editor.EndEdit();

            //       Object frame = ((DetailView)View).Control;
            // ((DetailView)View).
        }

        private void singleChoiceAction_AddUnit_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (((SingleChoiceAction)e.Action).SelectedIndex == 0) //none
            {
                gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.CursorForUserDefined = gis.CursorForZoom;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 1) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 2) //Point by GPS point
            {
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll != null)
                {
                    DetailView view = (DetailView)((DetailPropertyEditor)(((NestedFrame)Frame).DetailViewItem)).Frame.View;
                    MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).DetailViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    TGIS_GpsNmea gps = GISHelperWin.GetGPS(view);
                    if (!gps.Active)
                    {
                        MessageBox.Show("GPS not active!");
                        ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                        return;
                    }
                    ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                    DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
                   // MapWindow_ViewController MapWindow_contr = editor.Frame.GetController<MapWindow_ViewController>();
                    DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                    if (contr == null) return;
                    //TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                    TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
 
                    IWKT iwkt =  (IWKT)(((NestedFrame)Frame).DetailViewItem.CurrentObject);
                    //MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
                  //  Session se = mapWindow.Session;

                    Unit newunit = ((ObjectSpace)View.ObjectSpace).CreateObject<Unit>();
                    string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                    newunit.WKT = wkt;
                    newunit.Save();
                    GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                    ((IUnits)iwkt).Units.Add(newunit);

                    GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.AccessionNumber, newunit.EPSG,false);
                    /* TGIS_ShapePoint shp = (TGIS_ShapePoint)ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                     shp.AddPart();
                     shp.AddPoint(ptg);*/
                    foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                        {
                            object listEditor = units_editor.ListView.Editor;
                            if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                units_editor.Refresh();
                        }
                    }
                    gis.Update();
                }
                else
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                }
               // singleChoiceAction_AddUnit.SelectedIndex = 0;
                singleChoiceAction_MapMode.SelectedIndex = 1;
                gis.Mode = TGIS_ViewerMode.gisZoom;

                //MessageBox.Show("Unit added");
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 3) //Point by doubleclicking
            {
                     gis.Mode = TGIS_ViewerMode.gisUserDefined;
                     gis.CursorForUserDefined = gis.CursorForEdit;
              }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 4) //locality outline by doubleclicking
            {
                singleChoiceAction_MapMode.SelectedIndex = 3;
                gis.Mode = TGIS_ViewerMode.gisEdit;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
       
        }

        private void singleChoiceAction_AddLocality_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (((SingleChoiceAction)e.Action).SelectedIndex == 0) //none
            {
                gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.CursorForUserDefined = gis.CursorForZoom;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 1) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 2) //Point by GPS point
            {
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll != null)
                {
                    DetailView view = (DetailView)((DetailPropertyEditor)(((NestedFrame)Frame).DetailViewItem)).Frame.View;
                    MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).DetailViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    TGIS_GpsNmea gps = GISHelperWin.GetGPS(view);
                    if (!gps.Active)
                    {
                        MessageBox.Show("GPS not active!");
                        ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                        return;
                    }
                    ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                    DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
                    MapWindow_ViewController MapWindow_contr = editor.Frame.GetController<MapWindow_ViewController>();
                    DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                    if (contr == null) return;
                    TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));

                    IWKT iwkt = (IWKT)View.CurrentObject;// (IWKT)(((NestedFrame)Frame).DetailViewItem.CurrentObject);
                    MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
                    Session se = mapWindow.Session;

                    Unit newunit = ((ObjectSpace)View.ObjectSpace).CreateObject<Unit>();
                    string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                    newunit.WKT = wkt;
                    newunit.Save();
                    GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                    ((IUnits)iwkt).Units.Add(newunit);

                    GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.AccessionNumber, newunit.EPSG,false);
                    /* TGIS_ShapePoint shp = (TGIS_ShapePoint)ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                     shp.AddPart();
                     shp.AddPoint(ptg);*/
                    foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                        {
                            object listEditor = units_editor.ListView.Editor;
                            if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                units_editor.Refresh();
                        }
                    }
                    gis.Update();
                }
                else
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                }
                singleChoiceAction_AddUnit.SelectedIndex = 0;
                singleChoiceAction_MapMode.SelectedIndex = 1;
                gis.Mode = TGIS_ViewerMode.gisZoom;

                MessageBox.Show("Unit added");
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 3) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
        }

        private void popupWindowShowAction_DowloadAndMapGbifForExtent_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            new Exception("not implemented");
            TaxonomicName name=(e.PopupWindow.View.CurrentObject as DowloadAndMapGbifForExtentParameters).Taxon;
               TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
               TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("GBIF");
               if (ll == null)
               {
                   ll = new TGIS_LayerVector();
                   gis.Add(ll);
               }
               TaxonomyHelper.GBIF(name, gis.VisibleExtent,ll);
        }

        private void popupWindowShowAction_DowloadAndMapGbifForExtent_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace obs = (ObjectSpace)Application.CreateObjectSpace();
            DowloadAndMapGbifForExtentParameters param = obs.CreateObject<DowloadAndMapGbifForExtentParameters>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;

        }

    /*    private void popupWindowShowAction_RecordUnitByName_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            if (popupWindowShowAction_RecordUnitByName.Tag == null)
            {
                string name = ((TaxonomicName)(e.PopupWindow.View.CurrentObject)).Name;
                if (name.Length > 9) name = name.Substring(0, 10) + "...";
                popupWindowShowAction_RecordUnitByName.Caption = name;
                popupWindowShowAction_RecordUnitByName.Tag = ((TaxonomicName)e.PopupWindow.View.CurrentObject).Oid;
            }
            else
            {
                popupWindowShowAction_RecordUnitByName.Caption = cap;
                popupWindowShowAction_RecordUnitByName.Tag = null;
    
            }
            
        }

        private void popupWindowShowAction_RecordUnitByName_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
                ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
                DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
                e.View = view;
        }

        private void popupWindowShowAction_RecordUnitByName_Cancel(object sender, EventArgs e)
        {
            popupWindowShowAction_RecordUnitByName.Caption = cap;
            popupWindowShowAction_RecordUnitByName.Tag = null;
      
        }
        */
      

    }
}
