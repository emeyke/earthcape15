namespace EarthCape.Module.Field.Win
{
    partial class MapWindow_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem4 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem5 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.simpleAction_GPSLocateMe = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.singleChoiceAction_AddUnit = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // simpleAction_GPSLocateMe
            // 
            this.simpleAction_GPSLocateMe.Caption = "GPS";
            this.simpleAction_GPSLocateMe.Category = "View";
            this.simpleAction_GPSLocateMe.ConfirmationMessage = null;
            this.simpleAction_GPSLocateMe.Id = "simpleAction_GPSLocateMe";
            this.simpleAction_GPSLocateMe.ImageName = null;
            this.simpleAction_GPSLocateMe.Shortcut = null;
            this.simpleAction_GPSLocateMe.Tag = "0";
            this.simpleAction_GPSLocateMe.TargetObjectsCriteria = null;
            this.simpleAction_GPSLocateMe.TargetViewId = null;
            this.simpleAction_GPSLocateMe.ToolTip = null;
            this.simpleAction_GPSLocateMe.TypeOfView = null;
            this.simpleAction_GPSLocateMe.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_GPSLocateMe_Execute);
            // 
            // singleChoiceAction_AddUnit
            // 
            this.singleChoiceAction_AddUnit.Caption = "Add unit";
            this.singleChoiceAction_AddUnit.Category = "OpenObject";
            this.singleChoiceAction_AddUnit.ConfirmationMessage = null;
            this.singleChoiceAction_AddUnit.Id = "singleChoiceAction_AddUnit";
            this.singleChoiceAction_AddUnit.ImageName = null;
            choiceActionItem1.Caption = "<change shape>";
            choiceActionItem1.ImageName = null;
            choiceActionItem1.Shortcut = null;
            choiceActionItem2.Caption = "Create new unit by double clicking the map (point)";
            choiceActionItem2.ImageName = null;
            choiceActionItem2.Shortcut = null;
            choiceActionItem3.Caption = "Create new unit in current GPS position (point)";
            choiceActionItem3.ImageName = null;
            choiceActionItem3.Shortcut = null;
            choiceActionItem4.Caption = "Create new unit by double clicking the map (area)";
            choiceActionItem4.ImageName = null;
            choiceActionItem4.Shortcut = null;
            choiceActionItem5.Caption = "Create new area for current locality (OVERWRITES THE CURRENT AREA)";
            choiceActionItem5.ImageName = null;
            choiceActionItem5.Shortcut = null;
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem1);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem2);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem3);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem4);
            this.singleChoiceAction_AddUnit.Items.Add(choiceActionItem5);
            this.singleChoiceAction_AddUnit.Shortcut = null;
            this.singleChoiceAction_AddUnit.Tag = null;
            this.singleChoiceAction_AddUnit.TargetObjectsCriteria = null;
            this.singleChoiceAction_AddUnit.TargetViewId = null;
            this.singleChoiceAction_AddUnit.ToolTip = null;
            this.singleChoiceAction_AddUnit.TypeOfView = null;
            this.singleChoiceAction_AddUnit.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_AddUnit_Execute);
            // 
            // MapWindow_ViewController
            // 
      
        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_GPSLocateMe;
        public DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_AddUnit;
    }
}
