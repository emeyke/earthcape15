using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using TatukGIS.NDK;
using System.IO;
using DevExpress.ExpressApp.Editors;

using EarthCape.Module.GIS.Win;
using EarthCape.Module.GIS;
using System.Drawing;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using System.Configuration;
using System.Device.Location;

namespace EarthCape.Module.Field.Win
{
    public partial class FieldMapWindow_ViewController : ViewController
    {
        private TGIS_Point lastPointMap;
        private TGIS_LayerVector lgps;
        TGIS_GpsNmea GPS;
        TGIS_ControlLegend legend;
        GeoCoordinateWatcher _geoWatcher;
        
        public FieldMapWindow_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(EarthCape.Module.GIS.MapWindow);
            TargetViewType = ViewType.DetailView;
         //   TargetViewNesting = Nesting.Nested;
            Activated += MapWindow_ViewController_Activated;
       //     Deactivating += MapWindow_ViewController_Deactivating;
            _geoWatcher = new GeoCoordinateWatcher();
        
        }

        void MapWindow_ViewController_Deactivating(object sender, EventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            View.ControlsCreated -= View_ControlsCreated;
            if (GPS != null)
            {
                GPS.Position -= GPS_Position;
                if (GPS.Active) GPS.Active = false;
            }
            View.CurrentObjectChanged -= View_CurrentObjectChanged;

        }

        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            //            throw new NotImplementedException();
        }

        void MapWindow_ViewController_Activated(object sender, EventArgs e)
        {
            View.ControlsCreated += View_ControlsCreated;
     //       singleChoiceAction_AddUnit.SelectedIndex = 0;
          GPS = GISHelperWin.GetGPS((DetailView)View);
           legend = GISHelperWin.GetGisLegend((DetailView)View);
 
        }

     
        private void WriteConfig()
        {
            TGIS_ViewerWnd gis_wnd = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            string tempDirectory = Path.GetTempPath();
            Directory.CreateDirectory(tempDirectory);
            string tempFileName = Path.Combine(tempDirectory, Guid.NewGuid().ToString() + ".ttkgp");
            //// gis.ProjectFile.UseRelativePath = false;
            gis_wnd.SaveProjectAs(tempFileName, false);
            // gis.ProjectFile.UseRelativePath = false;
            // gis.SaveProject();
            // gis.
            using (StreamReader streamReader = new StreamReader(tempFileName))
            {
                ((MapWindow)View.CurrentObject).MapProject.ProjectConfig = streamReader.ReadToEnd();
                ((MapWindow)View.CurrentObject).MapProject.Save();
                streamReader.Close();
            }
        }
        void obs_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // e.Cancel = true;
            WriteConfig();
        }
        //string cap = "";
        void View_ControlsCreated(object sender, EventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            simpleAction_GPSLocateMe.Active[""] = false; ;
            simpleAction_GPSLocateMe.Active[""] = (GPS != null);

            simpleAction_GPSLocateMe.Active[""] = true; ;
    
        }



        private void simpleAction_GPSLocateMe_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            /*   
                    GPS.Com = 5;
                    GPS.BaudRate = 4800;
                    GPS.Active = true;
                    GPS.Position += GPS_Position;
                    return;*/
            TGIS_ViewerWnd GIS = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_ControlLegend legend = GISHelperWin.GetGisLegend((DetailView)View);
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            if (GPS == null)
            {
                _geoWatcher.StatusChanged += GeoWatcherOnStatusChanged;

                _geoWatcher.Start();
                GeoPosition<GeoCoordinate> coordinate = _geoWatcher.Position;

                GeoCoordinate deviceLocation = coordinate.Location;
                
            }
            if ((string) simpleAction_GPSLocateMe.Tag == "0")
            {
                simpleAction_GPSLocateMe.Caption = "Searching...";
                Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)View.ObjectSpace);
                simpleAction_GPSLocateMe.Tag = "1";
                if ((TGIS_LayerVector)GIS.Get("gps") != null)
                {
                    lgps = (TGIS_LayerVector)GIS.Get("gps");
                    GIS.Delete("gps");
                    //lgps.SaveAll();
                }
                for (int i = 0; i < GIS.Items.Count - 1; i++)
                {
                    if (GIS.Items[i] is TGIS_LayerAbstract)
                        ((TGIS_LayerAbstract)GIS.Items[i]).CachedPaint = true;
                }
                lgps = new TGIS_LayerVector { Name = "gps" };
                lgps.Params.Marker.Color = Color.Red;
                lgps.Params.Marker.OutlineColor = Color.Blue;
                lgps.Params.Marker.OutlineWidth = 10;
                lgps.Params.Marker.Style = TGIS_MarkerStyle.gisMarkerStyleCircle;
                lgps.Params.Marker.Size = 100;
                //lgps.SaveAll();
                if (GIS.CS.EPSG > 0)
                    lgps.SetCSByEPSG(GIS.CS.EPSG);
                else
                {
                    Project gr = (Value_WindowController.Instance()).GetGroup((XPObjectSpace)View.ObjectSpace);
                    if (gr.EPSG > 0)
                    {
                        lgps.SetCSByEPSG(gr.EPSG);
                    }
                }

                GIS.Add(lgps);
                try
                {
                    lgps.AddField("Date", TGIS_FieldType.gisFieldTypeString, 10, 0);
                }
                catch
                { }


                try
                {
                    lgps.AddField("Date", TGIS_FieldType.gisFieldTypeString, 10, 0);
                }
                catch
                { }
                GIS.CachedPaint = true;
                lgps.IncrementalPaint = true;
                lgps.CachedPaint = false;
                if (GPS != null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["GPSCOM"]))
                        GPS.Com = Convert.ToInt32(ConfigurationManager.AppSettings["GPSCOM"]);
                     if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["GPSBAUD"]))
                        GPS.BaudRate = Convert.ToInt32(ConfigurationManager.AppSettings["GPSBAUD"]);
                     GPS.Position += GPS_Position;
                     GPS.Active = true;
                }
                /*  int gpsindex = -1;
                  for (int i = 0; i < GIS.Items.Count - 1; i++)
                  {
                      ((TGIS_LayerAbstract)GIS.Items[i]).CachedPaint = true;
                      if (((TGIS_LayerAbstract)GIS.Items[i]).Name == "GPS")
                          gpsindex = i;
                  }
                  lgps.IncrementalPaint = true;
                  lgps.CachedPaint = false;
                  lgps.Move((gpsindex));
                  legend.Update();*/
                /*      if (!(GPS.Active))
                      {
                          GPS.BaudRate = 4800;
                          for (int i = 0; i < 50; i++)
                          {
                              try
                              {
                                  GPS.Com = i;
                                  GPS.Active = true;
                              }
                              catch (Exception ex)
                              {

                              }
                              finally
                              {

                              }
                              if (GPS.Active) break;
                          }
                      }*/
              /*  if (!GPS.Active)
                {


                 
                    _geoWatcher.StatusChanged += GeoWatcherOnStatusChanged;

    _geoWatcher.Start();
    GeoPosition<GeoCoordinate> coordinate = _geoWatcher.Position;

    GeoCoordinate deviceLocation = coordinate.Location;
 
 
 
         //           MessageBox.Show("Unable to connect to GPS device!");
        //            return;
                }
                else
                {
                    // MessageBox.Show(String.Format("Com {0} and Baud rate {1} are used.", GPS.Com, GPS.BaudRate));
                }*/
                // GetLastPoint(GIS);
                //if (currShape != null) currShape.Free();
                //currShape = (TGIS_ShapeArc)lgps.CreateShape(TGIS_ShapeType.gisShapeTypeArc);
                // currShape.AddPart();
                //currShape.AddPoint(lastPointMap);
                //  GIS.Center = lastPointMap;
                // lgps.AddShape(currShape);
            }
            else
            {
                simpleAction_GPSLocateMe.Tag = "0";
                simpleAction_GPSLocateMe.Caption = "GPS";
                if (GPS!=null)
                GPS.Active = false;
                lgps = (TGIS_LayerVector)GIS.Get("gps");
                lgps.Items.Clear();
                //lgps.SaveAll();
                GIS.Invalidate();
            }
        }

        private void GeoWatcherOnStatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status != GeoPositionStatus.Ready) return;

            GeoPositionPermission allowed = _geoWatcher.Permission;


            GeoPosition<GeoCoordinate> coordinate = _geoWatcher.Position;

            GeoCoordinate deviceLocation = coordinate.Location;
            if (lgps == null) return;
            simpleAction_GPSLocateMe.Caption = "LOC ON";
            simpleAction_GPSLocateMe.ToolTip = "Accuracy: "+deviceLocation.HorizontalAccuracy;// String.Format("GPS ON (sat: {0})", GPS.Satellites);
            TGIS_ViewerWnd GIS = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_Point ptg = TGIS_Utils.GisPoint(deviceLocation.Longitude, deviceLocation.Latitude);
            //dist = cs.Datum.Ellipsoid.Distance(ptg, lastPointGps);
            //lastPointGps = ptg;
            Project gr = (Value_WindowController.Instance()).GetGroup((XPObjectSpace)View.ObjectSpace);
            if (GIS.CS.EPSG > 0)
                lastPointMap = GIS.CS.FromWGS(ptg);
            else
                if (gr.EPSG > 0)
                {
                    TGIS_CSCoordinateSystem cs = new TGIS_CSCoordinateSystem(gr.EPSG, "");
                    lastPointMap = cs.FromWGS(ptg);
                }
            // if (TGIS_Utils.GisIsPointInsideExtent(lastPointMap,GIS.Extent))

            // GIS.Center = lastPointMap;
            //currShape.AddPoint(lastPointMap);

            TGIS_ShapePoint shp = (TGIS_ShapePoint)lgps.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
            shp.AddPart();
            shp.AddPoint(lastPointMap);
            //   lgps.SaveData();
            // lgps.SaveAll();
            //  lgps.RecalcExtent();
            // GIS.RecalcExtent();
            //  if (lgps.Active)
            GIS.Invalidate();
          /*  DateTimeOffset fetchedAt = coordinate.Timestamp;
            reply.Text = string.Format("Lat: {0}, Long: {1}, fetched at: {2}",
                deviceLocation.Latitude,
                deviceLocation.Longitude,
                fetchedAt.DateTime.ToShortTimeString());*/
        }

        private void GPS_Position(object sender, EventArgs e)
        {
            if (GPS.Satellites < 1)
            {
                simpleAction_GPSLocateMe.Caption = String.Format("Searching...", GPS.Satellites);

                return;
            }
            if (lgps == null) return;
            simpleAction_GPSLocateMe.Caption = "GPS ON";// String.Format("GPS ON (sat: {0})", GPS.Satellites);
             TGIS_ViewerWnd GIS = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
              TGIS_Point ptg = TGIS_Utils.GisPoint(GPS.Longitude, GPS.Latitude);
            //dist = cs.Datum.Ellipsoid.Distance(ptg, lastPointGps);
            //lastPointGps = ptg;
            Project gr = (Value_WindowController.Instance()).GetGroup((XPObjectSpace)View.ObjectSpace);
            if (GIS.CS.EPSG>0)
            lastPointMap = GIS.CS.FromWGS(ptg);
            else
                if (gr.EPSG > 0)
                {
                    TGIS_CSCoordinateSystem cs = new TGIS_CSCoordinateSystem(gr.EPSG,"");
                    lastPointMap = cs.FromWGS(ptg);
                }
           // if (TGIS_Utils.GisIsPointInsideExtent(lastPointMap,GIS.Extent))

            // GIS.Center = lastPointMap;
            //currShape.AddPoint(lastPointMap);

            TGIS_ShapePoint shp = (TGIS_ShapePoint)lgps.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
            shp.AddPart();
            shp.AddPoint(lastPointMap);
        //   lgps.SaveData();
         // lgps.SaveAll();
          //  lgps.RecalcExtent();
           // GIS.RecalcExtent();
          //  if (lgps.Active)
            GIS.Invalidate();
         //todo PaintPrec(GPS.PositionPrec, GIS.MapToScreen(lastPointMap));
            //    GIS.FullExtent();
        }

        private void PaintPrec(double oldRadius,Point oldPos)
        {
            TGIS_ViewerWnd GIS = GISHelperWin.GetGisWndInMapWindow((DetailView)View);
            TGIS_PenMode old_mode;
            // Random rnd;
            // double dd;

            if (GIS.IsEmpty) return;

            if (GIS.Mode != TGIS_ViewerMode.gisSelect) return;
          
            old_mode = GIS.Canvas.Pen.Mode;
            GIS.Canvas.Brush.Style = TGIS_BrushStyle.gisBsClear;
            GIS.Canvas.Pen.Width = 1;
            GIS.Canvas.Pen.Color = Color.Red;
            GIS.Canvas.Pen.Mode = TGIS_PenMode.gisPmXor;
            GIS.Canvas.Canvas = GIS.CreateGraphics();
            try
            {
                if (oldRadius != 0)
                {
                    GIS.Canvas.Ellipse(Convert.ToInt32(oldPos.X - oldRadius),
                                                            Convert.ToInt32(oldPos.Y - oldRadius),
                                                            Convert.ToInt32(oldPos.X + oldRadius),
                                                           Convert.ToInt32(oldPos.Y + oldRadius)
                                                        );
                }
                /*  dd = Math.Round(Math.Sqrt(Math.Sqrt(Math.Abs(oldPos.X - e.X)) +
                                                                          Math.Sqrt(Math.Abs(oldPos.Y - e.Y))
                                                                       ), 0
                                                  );
                  try
                  {
                      oldRadius = Convert.ToInt32(dd);
                  }
                  catch
                  {
                      oldRadius = 0;
                  }
                
                  oldRadius = Convert.ToInt32(
                                              Math.Round(Math.Sqrt(Math.Pow(oldPos.X - e.X, 2) +
                                                                                           Math.Pow(oldPos.Y - e.Y, 2)
                                                                      ),
                                                                      0
                                              )
                                          );

                  //the var below is for specifying the readius depending on the zoom.
                  oldZoom = Convert.ToInt32(
                                              Math.Round(Math.Sqrt(Math.Pow(GIS.ClientRectangle.Width, 2) +
                                                                                           Math.Pow(GIS.ClientRectangle.Height, 2)
                                                                                       ),
                                                                                       0
                                                                  )
                                          );
                  rnd = new Random();
                  //oldColor = ColorTranslator.FromWin32(rnd.Next(0xFFFFFF));
                 // GIS.Canvas.Pen.Color = oldColor;
                  GIS.Canvas.Ellipse(oldPos.X - oldRadius,
                                                          oldPos.Y - oldRadius,
                                                          oldPos.X + oldRadius,
                                                          oldPos.Y + oldRadius
                                                      );*/
            }
            finally
            {
                GIS.Canvas.Canvas = null;
                GIS.Canvas.Pen.Mode = old_mode;
            }
        }

        private void singleChoiceAction_AddUnit_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {

        }



     /*   private void singleChoiceAction_AddUnit_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            TGIS_ViewerWnd gis = GISHelperWin.GetGisWnd((DetailView)View);
            if (((SingleChoiceAction)e.Action).SelectedIndex == 0) //none
            {
                gis.Mode = TGIS_ViewerMode.gisZoom;
                gis.CursorForUserDefined = gis.CursorForZoom;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 1) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 2) //Point by GPS point
            {
                TGIS_LayerVector ll = (TGIS_LayerVector)gis.Get("Units");
                if (ll != null)
                {
                    DetailView view = (DetailView)((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.View;
                    MapProject_ViewController contr = ((DetailPropertyEditor)(((NestedFrame)Frame).ViewItem)).Frame.GetController<MapProject_ViewController>(); //todo <- wrong Frame?
                    TGIS_GpsNmea gps = GISHelperWin.GetGPS(view);
                    if (!gps.Active)
                    {
                        MessageBox.Show("GPS not active!");
                        ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                        return;
                    }
                    ((SingleChoiceAction)e.Action).SelectedIndex = 0;
                    DetailPropertyEditor editor = ((DetailView)View).FindItem("MapWindow") as DetailPropertyEditor;
                    // MapWindow_ViewController MapWindow_contr = editor.Frame.GetController<MapWindow_ViewController>();
                    DetailView mapview = GISHelperWin.MapHostView((DetailView)View);
                    if (contr == null) return;
                    //TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                    TGIS_Point ptg = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));

                    IWKT iwkt = (IWKT)(((NestedFrame)Frame).ViewItem.CurrentObject);
                    //MapWindow mapWindow = (MapWindow)(editor.Frame.View.CurrentObject);
                    //  Session se = mapWindow.Session;

                    Unit newunit = ((XPObjectSpace)View.ObjectSpace).CreateObject<Unit>();
                    string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
                    newunit.WKT = wkt;
                    TGIS_Point ptg_ = gis.CS.FromWGS(TGIS_Utils.GisPoint(gps.Longitude, gps.Latitude));
                    newunit.GpsX = ptg_.X;
                    newunit.GpsY = ptg_.Y;
                    newunit.GpsAltitude = gps.Altitude;
                    newunit.GpsPrecision = gps.PositionPrec;
                    newunit.GpsSatellites = gps.Satellites;

                    newunit.EPSG = gis.CS.EPSG;
                    newunit.Save();
                    GISHelper.CreateCentroidXY(iwkt); // todo: doesnt work for points apparently
                    ((IUnits)iwkt).Units.Add(newunit);

                    GISHelper.AddShapeFromWKT(wkt, ll, newunit.Oid.ToString(), newunit.Oid, newunit.UnitID, newunit.EPSG, false,true);
                    /* TGIS_ShapePoint shp = (TGIS_ShapePoint)ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint);
                     shp.AddPart();
                     shp.AddPoint(ptg);*/
           /*         foreach (ViewItem item in view.Items)
                    {
                        ListPropertyEditor units_editor = item as ListPropertyEditor;
                        if (units_editor != null)
                        {
                            object listEditor = units_editor.ListView.Editor;
                            if (units_editor.ListView.ObjectTypeInfo.Type == typeof(Unit))
                                units_editor.Refresh();
                        }
                    }
                    gis.Invalidate();
                }
                else
                {
                    MessageBox.Show("Select \"Units\" layer in map legend");
                }
                // singleChoiceAction_AddUnit.SelectedIndex = 0;
                MapWindow_ViewController mapwindowcontr = Frame.GetController<MapWindow_ViewController>();
                if (mapwindowcontr != null)
                {
                    mapwindowcontr.singleChoiceAction_MapMode.SelectedIndex = 1;
                   gis.Mode = TGIS_ViewerMode.gisZoom;
                }

                //MessageBox.Show("Unit added");
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 3) //Point by doubleclicking
            {
                gis.Mode = TGIS_ViewerMode.gisUserDefined;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }
            if (((SingleChoiceAction)e.Action).SelectedIndex == 4) //locality outline by doubleclicking
            {
                MapWindow_ViewController mapwindowcontr = Frame.GetController<MapWindow_ViewController>();
                if (mapwindowcontr != null)
                {
                    mapwindowcontr.singleChoiceAction_MapMode.SelectedIndex = 3;
                }
                gis.Mode = TGIS_ViewerMode.gisEdit;
                gis.CursorForUserDefined = gis.CursorForEdit;
            }

        }*/

    
   
    /*    private void popupWindowShowAction_RecordUnitByName_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            if (popupWindowShowAction_RecordUnitByName.Tag == null)
            {
                string name = ((TaxonomicName)(e.PopupWindow.View.CurrentObject)).Name;
                if (name.Length > 9) name = name.Substring(0, 10) + "...";
                popupWindowShowAction_RecordUnitByName.Caption = name;
                popupWindowShowAction_RecordUnitByName.Tag = ((TaxonomicName)e.PopupWindow.View.CurrentObject).Oid;
            }
            else
            {
                popupWindowShowAction_RecordUnitByName.Caption = cap;
                popupWindowShowAction_RecordUnitByName.Tag = null;
    
            }
            
        }

        private void popupWindowShowAction_RecordUnitByName_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
                XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
                DevExpress.ExpressApp.ListView view = Application.CreateListView(os, typeof(TaxonomicName), false);
                e.View = view;
        }

        private void popupWindowShowAction_RecordUnitByName_Cancel(object sender, EventArgs e)
        {
            popupWindowShowAction_RecordUnitByName.Caption = cap;
            popupWindowShowAction_RecordUnitByName.Tag = null;
      
        }
        */
      

    }
}
