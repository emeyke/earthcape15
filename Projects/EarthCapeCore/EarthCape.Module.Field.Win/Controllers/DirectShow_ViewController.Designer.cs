namespace EarthCape.Module.Field.Win.Controllers
{
    partial class DirectShow_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_Capture = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.singleChoiceAction_SelectCam = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.simpleAction_ActivateCam = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_Capture
            // 
            this.simpleAction_Capture.Caption = "Capture";
            this.simpleAction_Capture.Category = "View";
            this.simpleAction_Capture.ConfirmationMessage = null;
            this.simpleAction_Capture.Id = "simpleAction_Capture";
            this.simpleAction_Capture.ToolTip = null;
            this.simpleAction_Capture.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Capture_Execute);
            // 
            // singleChoiceAction_SelectCam
            // 
            this.singleChoiceAction_SelectCam.Caption = "Device";
            this.singleChoiceAction_SelectCam.ConfirmationMessage = null;
            this.singleChoiceAction_SelectCam.Id = "singleChoiceAction_SelectCam";
            this.singleChoiceAction_SelectCam.ToolTip = null;
            this.singleChoiceAction_SelectCam.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_SelectCam_Execute);
            // 
            // simpleAction_ActivateCam
            // 
            this.simpleAction_ActivateCam.Caption = "Turn on";
            this.simpleAction_ActivateCam.ConfirmationMessage = null;
            this.simpleAction_ActivateCam.Id = "simpleAction_ActivateCam";
            this.simpleAction_ActivateCam.ToolTip = null;
            this.simpleAction_ActivateCam.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ActivateCam_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Capture;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_SelectCam;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ActivateCam;
    }
}
