using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Field.Win
{
    public partial class AttachVideo_ViewController : ViewController
    {
        public AttachVideo_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(IFiles);
        }

   
        private void popupWindowShowAction_AttachImages_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

        }
    
        private void popupWindowShowAction_AttachImages_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            AttachImagesWindow obj = os.CreateObject<AttachImagesWindow>();
            DetailView view = Application.CreateDetailView(os, obj);
            view.Tag = View;
            e.DialogController.CancelAction.Active.SetItemValue("", false);
            e.DialogController.AcceptAction.Caption = "Close";
            e.View = view;
            
        }

        private void AttachVideo_ViewController_Deactivating(object sender, EventArgs e)
        {

        }
    
    
    }
}
