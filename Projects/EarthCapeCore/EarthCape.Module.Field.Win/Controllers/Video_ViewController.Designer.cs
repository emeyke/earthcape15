
namespace EarthCape.Module.Field.Win
{
    partial class Video_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.simpleAction_ImageCapture = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.singleChoiceAction_VideoStore = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.singleChoiceAction_SelectDevice = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.singleChoiceAction_SnapShotResolution = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // simpleAction_ImageCapture
            // 
            this.simpleAction_ImageCapture.Caption = "Capture";
            this.simpleAction_ImageCapture.Category = "View";
            this.simpleAction_ImageCapture.ConfirmationMessage = null;
            this.simpleAction_ImageCapture.Id = "simpleAction_ImageCapture";
            this.simpleAction_ImageCapture.ToolTip = null;
            this.simpleAction_ImageCapture.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ImageCapture_Execute);
            // 
            // singleChoiceAction_VideoStore
            // 
            this.singleChoiceAction_VideoStore.Caption = "Store";
            this.singleChoiceAction_VideoStore.Category = "Options";
            this.singleChoiceAction_VideoStore.ConfirmationMessage = null;
            this.singleChoiceAction_VideoStore.Id = "singleChoiceAction_VideoStore";
            choiceActionItem1.Caption = "In database";
            choiceActionItem1.ImageName = null;
            choiceActionItem1.Shortcut = null;
            choiceActionItem1.ToolTip = null;
            this.singleChoiceAction_VideoStore.Items.Add(choiceActionItem1);
            this.singleChoiceAction_VideoStore.ToolTip = null;
            this.singleChoiceAction_VideoStore.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_VideoStore_Execute);
            // 
            // singleChoiceAction_SelectDevice
            // 
            this.singleChoiceAction_SelectDevice.Caption = "Device";
            this.singleChoiceAction_SelectDevice.ConfirmationMessage = null;
            this.singleChoiceAction_SelectDevice.Id = "singleChoiceAction_SelectDevice";
            this.singleChoiceAction_SelectDevice.ToolTip = null;
            this.singleChoiceAction_SelectDevice.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_SelectDevice_Execute);
            // 
            // singleChoiceAction_SnapShotResolution
            // 
            this.singleChoiceAction_SnapShotResolution.Caption = "Snapshot res";
            this.singleChoiceAction_SnapShotResolution.ConfirmationMessage = null;
            this.singleChoiceAction_SnapShotResolution.Id = "singleChoiceAction_SnapShotResolution";
            choiceActionItem2.Caption = "Not supported";
            choiceActionItem2.ImageName = null;
            choiceActionItem2.Shortcut = null;
            choiceActionItem2.ToolTip = null;
            this.singleChoiceAction_SnapShotResolution.Items.Add(choiceActionItem2);
            this.singleChoiceAction_SnapShotResolution.ToolTip = null;
            this.singleChoiceAction_SnapShotResolution.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.singleChoiceAction_SnapShotResolution_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImageCapture;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_VideoStore;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_SelectDevice;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction singleChoiceAction_SnapShotResolution;
    }
}
