﻿namespace EarthCape.Module.Lab.Win {
	partial class EarthCapeLabModuleWin
    {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            // 
            // WinModule
            // 
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.EarthCapeBaseModule));
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.Core.EarthCapeModule));
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.Lab.EarthCapeLabModule));
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.Win.EarthCapeWinModule));

            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.SystemModule.SystemModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.PivotGrid.PivotGridModule));
			this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.ReportsV2.ReportsModuleV2));
			this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule));

			this.RequiredModuleTypes.Add(typeof(Xpand.XAF.Modules.ModelMapper.ModelMapperModule));
		}

		#endregion
	}
}
