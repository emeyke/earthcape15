﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Xpand.ExpressApp.Win.ListEditors.GridListEditors.ColumnView;
using EditorAliases = Xpand.Persistent.Base.General.EditorAliases;
using ListView = DevExpress.ExpressApp.ListView;

namespace EarthCapePro.Module.Win.Editors
{
    public class HyperLinkGridListViewController : ViewController
    {
        WinColumnsListEditor _gridListEditor;

        public HyperLinkGridListViewController()
        {
            TargetViewType = ViewType.ListView;
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            _gridListEditor = ((DevExpress.ExpressApp.ListView)View).Editor as WinColumnsListEditor;
            if (_gridListEditor != null)
            {
                GridView gridView = _gridListEditor.GridView();
                if (gridView != null) gridView.MouseDown += GridView_MouseDown;
            }
        }

        protected override void OnDeactivated()
        {
            if (_gridListEditor != null && _gridListEditor.GridView() != null)
                _gridListEditor.GridView().MouseDown -= GridView_MouseDown;
            base.OnDeactivated();
        }

        void GridView_MouseDown(object sender, MouseEventArgs e)
        {
            var gv = (GridView)sender;
            GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.InRowCell)
            {
                var repositoryItemHyperLinkEdit = hi.Column.ColumnEdit as RepositoryItemHyperLinkEdit;
                if (repositoryItemHyperLinkEdit != null)
                {
                    var editor = (HyperLinkEdit)repositoryItemHyperLinkEdit.CreateEditor();
                    editor.ShowBrowser(
                        EnaPropertyEditor.GetResolvedUrl(gv.GetRowCellValue(hi.RowHandle, hi.Column)));
                }
            }
        }
    }

    [PropertyEditor(typeof(String), "EnaPropertyEditor", false)]
    public class EnaPropertyEditor : StringPropertyEditor, IComplexViewItem
    {
    
        HyperLinkEdit _hyperlinkEdit;

        public EnaPropertyEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
        }

        public new HyperLinkEdit Control
        {
            get { return _hyperlinkEdit; }
        }

        protected override RepositoryItem CreateRepositoryItem()
        {
            return new RepositoryItemHyperLinkEdit();
        }

        protected override object CreateControlCore()
        {
            _hyperlinkEdit = new HyperLinkEdit();
            return _hyperlinkEdit;
        }

        protected override void SetupRepositoryItem(RepositoryItem item)
        {
            base.SetupRepositoryItem(item);
            var hyperLinkProperties = (RepositoryItemHyperLinkEdit)item;
            hyperLinkProperties.SingleClick = View is ListView;
            hyperLinkProperties.TextEditStyle = TextEditStyles.Standard;
            hyperLinkProperties.OpenLink += hyperLinkProperties_OpenLink;
        }

        void hyperLinkProperties_OpenLink(object sender, OpenLinkEventArgs e)
        {
            e.EditValue = GetResolvedUrl(e.EditValue);
        }

        public static string GetResolvedUrl(object value)
        {
            string url = Convert.ToString(value);
            if (!string.IsNullOrEmpty(url))
            {
                url = string.Format("http://www.ebi.ac.uk/ena/data/view/{0}", url);
                    return url;
            }
            return string.Empty;
        }

        public void Setup(IObjectSpace objectSpace, XafApplication application)
        {
           // objectSpace.Committing += ObjectSpaceOnCommitting;
        }
      
    }
}
