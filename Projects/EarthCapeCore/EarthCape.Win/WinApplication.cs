﻿using System;
using System.ComponentModel;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Win;
using System.Collections.Generic;
using System.IO;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using Xpand.ExpressApp.Win;
using Xpand.Persistent.Base.General;
using DevExpress.ExpressApp.Win.Utils;

namespace EarthCape.Win {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/DevExpressExpressAppWinWinApplicationMembersTopicAll.aspx
    public partial class EarthCapeWinApplication : XpandWinApplication {
        static EarthCapeWinApplication()
        {
            DevExpress.Persistent.Base.PasswordCryptographer.EnableRfc2898 = true;
            DevExpress.Persistent.Base.PasswordCryptographer.SupportLegacySha512 = true;
            DevExpress.ExpressApp.Utils.ImageLoader.Instance.UseSvgImages = true;
        }

        public EarthCapeWinApplication() {
            InitializeComponent();
            InitializeDefaults();
        }

        protected override void CreateDefaultObjectSpaceProvider(CreateCustomObjectSpaceProviderEventArgs args)
        {
            args.ObjectSpaceProvider = new XpandObjectSpaceProvider(new DataStoreProvider(args.ConnectionString), Security, true);
            //  args.ObjectSpaceProvider=new SecuredObjectSpaceProvider((SecurityStrategyComplex)Security, new DataStoreProvider(args.ConnectionString), true);
            args.ObjectSpaceProviders.Add(new NonPersistentObjectSpaceProvider());
        }

        private void InitializeDefaults()
        {
            LinkNewObjectToParentImmediately = true;
            OptimizedControllersCreation = true;
            UseLightStyle = true;
            SplashScreen = new DXSplashScreen(typeof(XafSplashScreen), new DefaultOverlayFormOptions());
            ExecuteStartupLogicBeforeClosingLogonWindow = true;
            DevExpress.Xpo.DB.ConnectionProviderSql.GlobalDefaultCommandTimeout = 10000;
         //   DevExpress.ExpressApp.Utils.ImageLoader.Instance.UseSvgImages = true;
            //ServerPermissionRequestProcessor.UseAutoAssociationPermission = false;
            ServerPermissionPolicyRequestProcessor.UseAutoAssociationPermission = true;
        }
        protected override string GetModelAssemblyFilePath(){
            return Path.Combine(Path.GetTempPath(),$"{GetType().Name}{ModelAssemblyFileName}");
        }
        private IXpoDataStoreProvider GetDataStoreProvider(string connectionString, System.Data.IDbConnection connection)
        {
            System.Web.HttpApplicationState application = (System.Web.HttpContext.Current != null) ? System.Web.HttpContext.Current.Application : null;
            IXpoDataStoreProvider dataStoreProvider = null;
            if (application != null && application["DataStoreProvider"] != null)
            {
                dataStoreProvider = application["DataStoreProvider"] as IXpoDataStoreProvider;
            }
            else
            {
                dataStoreProvider = XPObjectSpaceProvider.GetDataStoreProvider(connectionString, connection, true);
                if (application != null)
                {
                    application["DataStoreProvider"] = dataStoreProvider;
                }
            }
            return dataStoreProvider;
        }


        private void EarthCapeWinApplication_CustomizeLanguagesList(object sender, CustomizeLanguagesListEventArgs e) {
            string userLanguageName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
            if(userLanguageName != "en-US" && e.Languages.IndexOf(userLanguageName) == -1) {
                e.Languages.Add(userLanguageName);
            }
        }
        private void EarthCapeWinApplication_DatabaseVersionMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e)
        {
            {
                e.Updater.Update();
                e.Handled = true;

            }
        }
        private void EarthCapeWinApplication_LastLogonParametersRead(object sender, LastLogonParametersReadEventArgs e)
        {
            // Just to read demo user name for logon.
            AuthenticationStandardLogonParameters logonParameters = e.LogonObject as AuthenticationStandardLogonParameters;
            if (logonParameters != null)
            {
                if (String.IsNullOrEmpty(logonParameters.UserName))
                {
                    logonParameters.UserName = "Admin";
                }
            }
        }
    }
}
