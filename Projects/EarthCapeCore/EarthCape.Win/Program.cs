﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Windows.Forms;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Security.ClientServer.Remoting;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.AuditTrail;
using DevExpress.Persistent.Base;
using DevExpress.XtraEditors;
using EarthCape.Module.Core;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;
using Xpand.XAF.Modules.CloneModelView;

namespace EarthCape.Win {
    public static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(){
#if EASYTEST
            DevExpress.ExpressApp.Win.EasyTest.EasyTestRemotingRegistration.Register();
#endif
            WindowsFormsSettings.LoadApplicationSettings();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DevExpress.ExpressApp.BaseObjectSpace.ThrowExceptionForNotRegisteredEntityType = true;
            EditModelPermission.AlwaysGranted = System.Diagnostics.Debugger.IsAttached;
            /*     if (Tracing.GetFileLocationFromSettings() == DevExpress.Persistent.Base.FileLocation.CurrentUserApplicationDataFolder)
                 {
                     Tracing.LocalUserAppDataPath = Application.LocalUserAppDataPath;
                 }*/
            Tracing.Initialize();
            int i = 0;
            AppDomain.CurrentDomain.AssemblyLoad += (sender, args) => {
                if (args.LoadedAssembly.GetName().Name.StartsWith("ModelMapperAssembly")){
                    Tracing.Tracer.LogSeparator($"{Path.GetFileName(args.LoadedAssembly.Location)}{i}");
                    Tracing.Tracer.LogText(args.LoadedAssembly.Location);
                    Tracing.Tracer.LogText(Environment.StackTrace);
                    i++;
                }
            };

            ValueManager.ValueManagerType = typeof(SimpleValueManager<>).GetGenericTypeDefinition();
            EarthCapeWinApplication winApplication = new EarthCapeWinApplication();
            Value_WindowController.GetApplicationInstance = delegate(){ return winApplication; };
            CaptionHelper.CustomizeConvertCompoundName += CaptionHelper_CustomizeConvertCompoundName;
            //   winApplication.UseOldTemplates = false;
            // winApplication.UseLightStyle = true;
            // winApplication.LinkNewObjectToParentImmediately = true;
            /*   if (Tracing.GetFileLocationFromSettings() == DevExpress.Persistent.Base.FileLocation.CurrentUserApplicationDataFolder)
              {
                  Tracing.LocalUserAppDataPath = Application.LocalUserAppDataPath;
              }*/
            Tracing.Initialize();

#if (EASYTEST != true)
            //    winApplication.SplashScreen = new DevExpress.ExpressApp.Win.Utils.DXSplashScreen("splash.png");
#endif
            //SecurityAdapterHelper.Enable();
            // IsGrantedAdapter.Enable(XPOSecurityAdapterHelper.GetXpoCachedRequestSecurityAdapters());
            //WinReportServiceController.UseNewWizard = true;
            //WindowsFormsSettings.AllowHoverAnimation = DefaultBoolean.False;
            AuditTrailService.Instance.ObjectAuditingMode = ObjectAuditingMode.Full;
            AuditTrailService.Instance.CustomizeAuditTrailSettings += Instance_CustomizeAuditTrailSettings;

            if (ConfigurationManager.ConnectionStrings["AppServerConnectionString"] != null){
                string connectionString = ConfigurationManager.ConnectionStrings["AppServerConnectionString"]
                    .ConnectionString;
                winApplication.ConnectionString = connectionString;
                winApplication.DatabaseUpdateMode = DatabaseUpdateMode.Never;


                try{
                    var t = new Hashtable{{"secure", true},{"tokenImpersonationLevel", "impersonation"}};
                    var channel = new TcpChannel(t, null, null);
                    ChannelServices.RegisterChannel(channel, true);
                    var clientDataServer = (IDataServer) Activator.GetObject(
                        typeof(RemoteSecuredDataServer), connectionString);
                    var securityClient =
                        new ServerSecurityClient(clientDataServer, new ClientInfoFactory())
                            {IsSupportChangePassword = true};
                    winApplication.ApplicationName = "SecuritySystemExample";
                    winApplication.Security = securityClient;
                    winApplication.CreateCustomObjectSpaceProvider +=
                        delegate(object sender, CreateCustomObjectSpaceProviderEventArgs e){
                            e.ObjectSpaceProvider =
                                new DataServerObjectSpaceProvider(clientDataServer, securityClient);
                        };
                    winApplication.UseOldTemplates = false;
                    winApplication.Setup();
                    winApplication.Start();
                }
                catch (Exception e){
                    winApplication.StopSplash();
                    winApplication.HandleException(e);
                }
            }
            else
                // if(System.Diagnostics.Debugger.IsAttached && winApplication.CheckCompatibilityType == CheckCompatibilityType.DatabaseSchema) {
            {
#if EASYTEST
                if (ConfigurationManager.ConnectionStrings["EasyTestConnectionString"] != null)
                {
                    winApplication.ConnectionString =
 ConfigurationManager.ConnectionStrings["EasyTestConnectionString"].ConnectionString;
                }

#else


                if (ConfigurationManager.ConnectionStrings["ConnectionString"] != null){
                    winApplication.ConnectionString =
                        ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                }
#endif
                winApplication.DatabaseUpdateMode = DatabaseUpdateMode.UpdateOldDatabase;
                try{
                    winApplication.Setup("EarthCape", winApplication.ConnectionString,
                        ConfigurationManager.AppSettings["Modules"].Split(';'));
                    //winApplication.Setup();
                    winApplication.Start();
                }
                catch (Exception e){
                    winApplication.StopSplash();
                    winApplication.HandleException(e);
                }
            }
        }

        private static void CaptionHelper_CustomizeConvertCompoundName(object sender,
            CustomizeConvertCompoundNameEventArgs e){
            e.Handled = true;
        }


        private static void Instance_CustomizeAuditTrailSettings(object sender, CustomizeAuditTrailSettingsEventArgs e){
            // e.AuditTrailSettings.Clear();
            e.AuditTrailSettings.RemoveType(typeof(ModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(RoleModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(UserModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(AspectObject));
            e.AuditTrailSettings.RemoveType(typeof(Settings));
        }
    }
}