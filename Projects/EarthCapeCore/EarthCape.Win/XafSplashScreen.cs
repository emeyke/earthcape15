﻿using DevExpress.XtraSplashScreen;
using EarthCape.Module.Core;
using System;
using System.Diagnostics;
using System.Reflection;

namespace EarthCape.Win
{
    public partial class XafSplashScreen : SplashScreen
    {
        public XafSplashScreen()
        {
            InitializeComponent();
            this.labelCopyright.Text = "© EarthCape Oy 2006 - " + DateTime.Now.Year.ToString();
            Assembly asm = Assembly.GetExecutingAssembly();
            Version version = asm.GetName().Version;
            string[] tokens = version.ToString().Split('.');
            this.lVersion.Text = String.Format("v {0}.{1}.{2}.{3}", tokens[0], tokens[1], tokens[2], tokens[3]);
            // this.ver
        }

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        private void pictureEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void lVersion_Click(object sender, EventArgs e)
        {

        }

        private void hyperlinkLabelControl1_HyperlinkClick(object sender, DevExpress.Utils.HyperlinkClickEventArgs e)
        {
            hyperlinkLabelControl1.LinkVisited = true;
            System.Diagnostics.Process.Start(e.Text);
        }

        private void hyperlinkLabelControl2_HyperlinkClick(object sender, DevExpress.Utils.HyperlinkClickEventArgs e)
        {
            hyperlinkLabelControl2.LinkVisited = true;
            System.Diagnostics.Process.Start(e.Text);
        }

        private void hyperlinkLabelControl3_HyperlinkClick(object sender, DevExpress.Utils.HyperlinkClickEventArgs e)
        {
            hyperlinkLabelControl3.LinkVisited = true;
            System.Diagnostics.Process.Start(e.Text);
        }
    }
}