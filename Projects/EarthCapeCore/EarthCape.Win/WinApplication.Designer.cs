﻿
using Xpand.Persistent.Base.General;
using System.Linq;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Utils;

namespace EarthCape.Win {
    partial class EarthCapeWinApplication {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.module1 = new DevExpress.ExpressApp.SystemModule.SystemModule();
            this.module2 = new DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule();
            this.module3 = new EarthCape.Module.EarthCapeBaseModule();
            this.module4 = new EarthCape.Module.Win.EarthCapeWinModule();
            this.securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
            this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
            this.authenticationStandard1 = new DevExpress.ExpressApp.Security.AuthenticationStandard();
            this.validationModule = new DevExpress.ExpressApp.Validation.ValidationModule();
            this.validationWindowsFormsModule = new DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule();
            this.cloneObjectModule1 = new DevExpress.ExpressApp.CloneObject.CloneObjectModule();
            this.chartModule1 = new DevExpress.ExpressApp.Chart.ChartModule();
            this.pivotGridModule1 = new DevExpress.ExpressApp.PivotGrid.PivotGridModule();
            this.earthCapeModule1 = new EarthCape.Module.Core.EarthCapeModule();
            this.conditionalAppearanceModule1 = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
            this.xpandSystemModule1 = new Xpand.ExpressApp.SystemModule.XpandSystemModule();
            this.earthCapeGISModule1 = new EarthCape.Module.GIS.EarthCapeGISModule();
            this.xpandSecurityModule1 = new Xpand.ExpressApp.Security.XpandSecurityModule();
            this.modelDifferenceModule1 = new Xpand.ExpressApp.ModelDifference.ModelDifferenceModule();
            this.treeListEditorsModuleBase1 = new DevExpress.ExpressApp.TreeListEditors.TreeListEditorsModuleBase();
            this.logicModule1 = new Xpand.ExpressApp.Logic.LogicModule();
            this.modelArtifactStateModule1 = new Xpand.ExpressApp.ModelArtifactState.ModelArtifactStateModule();
            this.ioModule1 = new Xpand.ExpressApp.IO.IOModule();
            this.viewVariantsModule1 = new DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule();
            this.modelDifferenceWindowsFormsModule1 = new Xpand.ExpressApp.ModelDifference.Win.ModelDifferenceWindowsFormsModule();
            this.chartWindowsFormsModule1 = new DevExpress.ExpressApp.Chart.Win.ChartWindowsFormsModule();
            this.fileAttachmentsWindowsFormsModule1 = new DevExpress.ExpressApp.FileAttachments.Win.FileAttachmentsWindowsFormsModule();
            this.pivotChartModuleBase1 = new DevExpress.ExpressApp.PivotChart.PivotChartModuleBase();
            this.pivotChartWindowsFormsModule1 = new DevExpress.ExpressApp.PivotChart.Win.PivotChartWindowsFormsModule();
            this.pivotGridWindowsFormsModule1 = new DevExpress.ExpressApp.PivotGrid.Win.PivotGridWindowsFormsModule();
            this.reportsModuleV21 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
            this.reportsWindowsFormsModuleV21 = new DevExpress.ExpressApp.ReportsV2.Win.ReportsWindowsFormsModuleV2();
            this.treeListEditorsWindowsFormsModule1 = new DevExpress.ExpressApp.TreeListEditors.Win.TreeListEditorsWindowsFormsModule();
            this.emailModule1 = new Xpand.ExpressApp.Email.EmailModule();
            this.additionalViewControlsModule1 = new Xpand.ExpressApp.AdditionalViewControlsProvider.AdditionalViewControlsModule();
            this.additionalViewControlsProviderWindowsFormsModule1 = new Xpand.ExpressApp.AdditionalViewControlsProvider.Win.AdditionalViewControlsProviderWindowsFormsModule();
            this.xpandViewVariantsModule1 = new Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule();
            this.xpandSecurityWinModule1 = new Xpand.ExpressApp.Security.Win.XpandSecurityWinModule();
            this.xpandSystemWindowsFormsModule1 = new Xpand.ExpressApp.Win.SystemModule.XpandSystemWindowsFormsModule();
            this.earthCapeGISWinModule1 = new EarthCape.Module.GIS.Win.EarthCapeGISWinModule();
           // this.earthCapeImporterWinModule1 = new EarthCape.Module.Importer.Win.EarthCapeImporterWinModule();
            this.xpandTreeListEditorsModule1 = new Xpand.ExpressApp.TreeListEditors.XpandTreeListEditorsModule();
            this.xpandTreeListEditorsWinModule1 = new Xpand.ExpressApp.TreeListEditors.Win.XpandTreeListEditorsWinModule();
            this.ioWinModule1 = new Xpand.ExpressApp.IO.Win.IOWinModule();
            this.auditTrailModule1 = new DevExpress.ExpressApp.AuditTrail.AuditTrailModule();
            this.htmlPropertyEditorWindowsFormsModule1 = new DevExpress.ExpressApp.HtmlPropertyEditor.Win.HtmlPropertyEditorWindowsFormsModule();
            this.earthCapeGBIFModule1 = new EarthCape.Module.GBIF.EarthCapeGBIFModule();
            this.businessClassLibraryCustomizationModule1 = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
            this.llamachantFrameworkModule1 = new LlamachantFramework.Module.LlamachantFrameworkModule();
            this.llamachantFrameworkWindowsFormsModule1 = new LlamachantFramework.Module.Win.LlamachantFrameworkWindowsFormsModule();
            this.officeWindowsFormsModule1 = new DevExpress.ExpressApp.Office.Win.OfficeWindowsFormsModule();
            this.notificationsModule1 = new DevExpress.ExpressApp.Notifications.NotificationsModule();
            this.excelImporterModule1 = new Xpand.ExpressApp.ExcelImporter.ExcelImporterModule();
            this.notificationsWindowsFormsModule1 = new DevExpress.ExpressApp.Notifications.Win.NotificationsWindowsFormsModule();
            this.excelImporterWinModule1 = new Xpand.ExpressApp.ExcelImporter.Win.ExcelImporterWinModule();
            this.schedulerModuleBase1 = new DevExpress.ExpressApp.Scheduler.SchedulerModuleBase();
            this.schedulerWindowsFormsModule1 = new DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule();
            this.xpandTreeListEditorsModule2 = new Xpand.ExpressApp.TreeListEditors.XpandTreeListEditorsModule();
            this.xpandTreeListEditorsWinModule2 = new Xpand.ExpressApp.TreeListEditors.Win.XpandTreeListEditorsWinModule();
            this.dashboardModule1 = new Xpand.ExpressApp.Dashboard.DashboardModule();
            this.modelViewInheritanceModule1 = new Xpand.XAF.Modules.ModelViewInheritance.ModelViewInheritanceModule();
            this.earthCapeGeolocateModule1 = new EarthCape.Module.Geolocate.EarthCapeGeolocateModule();
            this.earthCapeLabModule1 = new EarthCape.Module.Lab.EarthCapeLabModule();
            this.earthCapeLabModuleWin1 = new EarthCape.Module.Lab.Win.EarthCapeLabModuleWin();
            this.dashboardWindowsFormsModule1 = new Xpand.ExpressApp.XtraDashboard.Win.DashboardWindowsFormsModule();
            this.earthCapeBHLModule1 = new EarthCape.Module.BHL.EarthCapeBHLModule();
            this.earthCapeBHLWinModule1 = new EarthCape.Module.BHL.Win.EarthCapeBHLWinModule();
            this.stateMachineModule1 = new DevExpress.ExpressApp.StateMachine.StateMachineModule();
            this.xpandStateMachineModule1 = new Xpand.ExpressApp.StateMachine.XpandStateMachineModule();
            this.cloneModelViewModule1 = new Xpand.XAF.Modules.CloneModelView.CloneModelViewModule();
            this.cloneMemberValueModule1 = new Xpand.XAF.Modules.CloneMemberValue.CloneMemberValueModule();
            this.hideToolBarModule1 = new Xpand.XAF.Modules.HideToolBar.HideToolBarModule();
            this.progressBarViewItemModule1 = new Xpand.XAF.Modules.ProgressBarViewItem.ProgressBarViewItemModule();
            this.refreshViewModule1 = new Xpand.XAF.Modules.RefreshView.RefreshViewModule();
            this.viewEditModeModule1 = new Xpand.XAF.Modules.ViewEditMode.ViewEditModeModule();
            this.masterDetailModule1 = new Xpand.XAF.Modules.MasterDetail.MasterDetailModule();
            this.autoCommitModule1 = new Xpand.XAF.Modules.AutoCommit.AutoCommitModule();
            this.suppressConfirmationModule1 = new Xpand.XAF.Modules.SuppressConfirmation.SuppressConfirmationModule();
         //   this.modelMapperModule1 = new Xpand.XAF.Modules.ModelMapper.ModelMapperModule();
            this.gridListEditorModule1 = new Xpand.XAF.Modules.GridListEditor.GridListEditorModule();
            this.officeModule1 = new DevExpress.ExpressApp.Office.OfficeModule();
            this.wizardUIWindowsFormsModule1 = new Xpand.ExpressApp.WizardUI.Win.WizardUIWindowsFormsModule();
            this.reactiveModule2 = new Xpand.XAF.Modules.Reactive.ReactiveModule();
            this.scriptRecorderModuleBase1 = new DevExpress.ExpressApp.ScriptRecorder.ScriptRecorderModuleBase();
            this.scriptRecorderWindowsFormsModule1 = new DevExpress.ExpressApp.ScriptRecorder.Win.ScriptRecorderWindowsFormsModule();
            this.reactiveLoggerModule1 = new Xpand.XAF.Modules.Reactive.Logger.ReactiveLoggerModule();
            this.xpandReportsModule1 = new Xpand.ExpressApp.Reports.XpandReportsModule();
            this.reportsV2WinModule1 = new Xpand.ExpressApp.ReportsV2.Win.ReportsV2WinModule();
            this.xpandValidationModule1 = new Xpand.ExpressApp.Validation.XpandValidationModule();
            this.xpandValidationWinModule1 = new Xpand.ExpressApp.Validation.Win.XpandValidationWinModule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // securityStrategyComplex1
            // 
            this.securityStrategyComplex1.AllowAnonymousAccess = false;
            this.securityStrategyComplex1.Authentication = this.authenticationStandard1;
            this.securityStrategyComplex1.PermissionsReloadMode = DevExpress.ExpressApp.Security.PermissionsReloadMode.NoCache;
            this.securityStrategyComplex1.RoleType = typeof(Xpand.Persistent.BaseImpl.Security.XpandPermissionPolicyRole);
            this.securityStrategyComplex1.UserType = typeof(Xpand.Persistent.BaseImpl.Security.XpandPermissionPolicyUser);
            // 
            // authenticationStandard1
            // 
            this.authenticationStandard1.LogonParametersType = typeof(Xpand.ExpressApp.Security.AuthenticationProviders.XpandLogonParameters);
            // 
            // validationModule
            // 
            this.validationModule.AllowValidationDetailsAccess = true;
            this.validationModule.IgnoreWarningAndInformationRules = false;
            // 
            // cloneObjectModule1
            // 
            this.cloneObjectModule1.ClonerType = null;
            // 
            // pivotChartModuleBase1
            // 
            this.pivotChartModuleBase1.DataAccessMode = DevExpress.ExpressApp.CollectionSourceDataAccessMode.Client;
            this.pivotChartModuleBase1.ShowAdditionalNavigation = false;
            // 
            // reportsModuleV21
            // 
            this.reportsModuleV21.EnableInplaceReports = true;
            this.reportsModuleV21.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
            this.reportsModuleV21.ReportStoreMode = DevExpress.ExpressApp.ReportsV2.ReportStoreModes.XML;
            // 
            // auditTrailModule1
            // 
            this.auditTrailModule1.AuditDataItemPersistentType = typeof(DevExpress.Persistent.BaseImpl.AuditDataItemPersistent);
            // 
            // officeWindowsFormsModule1
            // 
            this.officeWindowsFormsModule1.RichTextMailMergeDataType = typeof(DevExpress.Persistent.BaseImpl.RichTextMailMergeData);
            // 
            // notificationsModule1
            // 
            this.notificationsModule1.CanAccessPostponedItems = false;
            this.notificationsModule1.NotificationsRefreshInterval = System.TimeSpan.Parse("00:05:00");
            this.notificationsModule1.NotificationsStartDelay = System.TimeSpan.Parse("00:00:05");
            this.notificationsModule1.ShowDismissAllAction = false;
            this.notificationsModule1.ShowNotificationsWindow = true;
            this.notificationsModule1.ShowRefreshAction = false;
            // 
            // stateMachineModule1
            // 
            this.stateMachineModule1.StateMachineStorageType = typeof(DevExpress.ExpressApp.StateMachine.Xpo.XpoStateMachine);
            // 
            // officeModule1
            // 
            this.officeModule1.RichTextMailMergeDataType = null;
            // 
            // EarthCapeWinApplication
            // 
            this.ApplicationName = "EarthCape";
            this.CheckCompatibilityType = DevExpress.ExpressApp.CheckCompatibilityType.DatabaseSchema;
            this.LinkNewObjectToParentImmediately = false;
            this.Modules.Add(this.module1);
            this.Modules.Add(this.module2);
            this.Modules.Add(this.validationModule);
            this.Modules.Add(this.cloneObjectModule1);
            this.Modules.Add(this.chartModule1);
            this.Modules.Add(this.pivotGridModule1);
            this.Modules.Add(this.reactiveModule2);
            this.Modules.Add(this.earthCapeModule1);
            this.Modules.Add(this.securityModule1);
            this.Modules.Add(this.conditionalAppearanceModule1);
            this.Modules.Add(this.modelViewInheritanceModule1);
            this.Modules.Add(this.cloneModelViewModule1);
            this.Modules.Add(this.cloneMemberValueModule1);
            this.Modules.Add(this.hideToolBarModule1);
            this.Modules.Add(this.progressBarViewItemModule1);
            this.Modules.Add(this.refreshViewModule1);
            this.Modules.Add(this.reactiveLoggerModule1);
            this.Modules.Add(this.xpandSystemModule1);
            this.Modules.Add(this.earthCapeGISModule1);
            this.Modules.Add(this.xpandSecurityModule1);
            this.Modules.Add(this.viewVariantsModule1);
            this.Modules.Add(this.xpandViewVariantsModule1);
            this.Modules.Add(this.modelDifferenceModule1);
            this.Modules.Add(this.treeListEditorsModuleBase1);
            this.Modules.Add(this.logicModule1);
            this.Modules.Add(this.xpandValidationModule1);
            this.Modules.Add(this.modelArtifactStateModule1);
            this.Modules.Add(this.ioModule1);
            this.Modules.Add(this.auditTrailModule1);
            this.Modules.Add(this.emailModule1);
            this.Modules.Add(this.reportsModuleV21);
            this.Modules.Add(this.notificationsModule1);
            this.Modules.Add(this.viewEditModeModule1);
            this.Modules.Add(this.dashboardModule1);
            this.Modules.Add(this.masterDetailModule1);
            this.Modules.Add(this.autoCommitModule1);
            this.Modules.Add(this.suppressConfirmationModule1);
            this.Modules.Add(this.excelImporterModule1);
            this.Modules.Add(this.businessClassLibraryCustomizationModule1);
            this.Modules.Add(this.llamachantFrameworkModule1);
            this.Modules.Add(this.earthCapeGBIFModule1);
            this.Modules.Add(this.module3);
            this.Modules.Add(this.validationWindowsFormsModule);
            this.Modules.Add(this.chartWindowsFormsModule1);
            this.Modules.Add(this.fileAttachmentsWindowsFormsModule1);
            this.Modules.Add(this.pivotChartModuleBase1);
            this.Modules.Add(this.pivotChartWindowsFormsModule1);
            this.Modules.Add(this.pivotGridWindowsFormsModule1);
            this.Modules.Add(this.reportsWindowsFormsModuleV21);
            this.Modules.Add(this.treeListEditorsWindowsFormsModule1);
            this.Modules.Add(this.additionalViewControlsModule1);
            this.Modules.Add(this.additionalViewControlsProviderWindowsFormsModule1);
            this.Modules.Add(this.xpandSecurityWinModule1);
        //    this.Modules.Add(this.modelMapperModule1);
            this.Modules.Add(this.modelDifferenceWindowsFormsModule1);
            this.Modules.Add(this.gridListEditorModule1);
            this.Modules.Add(this.xpandSystemWindowsFormsModule1);
            this.Modules.Add(this.earthCapeGISWinModule1);
         //   this.Modules.Add(this.earthCapeImporterWinModule1);
            this.Modules.Add(this.xpandTreeListEditorsModule2);
            this.Modules.Add(this.xpandTreeListEditorsWinModule2);
            this.Modules.Add(this.ioWinModule1);
            this.Modules.Add(this.htmlPropertyEditorWindowsFormsModule1);
            this.Modules.Add(this.llamachantFrameworkWindowsFormsModule1);
            this.Modules.Add(this.officeModule1);
            this.Modules.Add(this.officeWindowsFormsModule1);
            this.Modules.Add(this.notificationsWindowsFormsModule1);
            this.Modules.Add(this.xpandValidationWinModule1);
            this.Modules.Add(this.excelImporterWinModule1);
            this.Modules.Add(this.schedulerModuleBase1);
            this.Modules.Add(this.schedulerWindowsFormsModule1);
            this.Modules.Add(this.earthCapeGeolocateModule1);
            this.Modules.Add(this.dashboardWindowsFormsModule1);
            this.Modules.Add(this.earthCapeBHLModule1);
            this.Modules.Add(this.earthCapeBHLWinModule1);
            this.Modules.Add(this.stateMachineModule1);
            this.Modules.Add(this.xpandStateMachineModule1);
            this.Modules.Add(this.wizardUIWindowsFormsModule1);
            this.Modules.Add(this.scriptRecorderModuleBase1);
            this.Modules.Add(this.scriptRecorderWindowsFormsModule1);
            this.Modules.Add(this.xpandReportsModule1);
            this.Modules.Add(this.reportsV2WinModule1);
            this.Modules.Add(this.module4);
            this.Modules.Add(this.earthCapeLabModule1);
            this.Modules.Add(this.earthCapeLabModuleWin1);
            this.Security = this.securityStrategyComplex1;
            this.UseOldTemplates = false;
             this.DatabaseVersionMismatch += new System.EventHandler<DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs>(this.EarthCapeWinApplication_DatabaseVersionMismatch);
            this.LastLogonParametersRead += new System.EventHandler<DevExpress.ExpressApp.LastLogonParametersReadEventArgs>(this.EarthCapeWinApplication_LastLogonParametersRead);
            this.CustomizeLanguagesList += new System.EventHandler<DevExpress.ExpressApp.CustomizeLanguagesListEventArgs>(this.EarthCapeWinApplication_CustomizeLanguagesList);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

       #endregion

        private EarthCape.Module.EarthCapeBaseModule module3;
        private EarthCape.Module.Win.EarthCapeWinModule module4;

        
        private DevExpress.ExpressApp.SystemModule.SystemModule module1;
        private DevExpress.ExpressApp.Win.SystemModule.SystemWindowsFormsModule module2;
        private DevExpress.ExpressApp.Security.SecurityModule securityModule1;
        private DevExpress.ExpressApp.Security.SecurityStrategyComplex securityStrategyComplex1;
        private DevExpress.ExpressApp.Security.AuthenticationStandard authenticationStandard1;
        private DevExpress.ExpressApp.Validation.ValidationModule validationModule;
        private DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule validationWindowsFormsModule;
        private DevExpress.ExpressApp.CloneObject.CloneObjectModule cloneObjectModule1;
        private DevExpress.ExpressApp.Chart.ChartModule chartModule1;
        private DevExpress.ExpressApp.PivotGrid.PivotGridModule pivotGridModule1;
        private DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule conditionalAppearanceModule1;
        private DevExpress.ExpressApp.TreeListEditors.TreeListEditorsModuleBase treeListEditorsModuleBase1;
        private DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule viewVariantsModule1;
        private DevExpress.ExpressApp.Chart.Win.ChartWindowsFormsModule chartWindowsFormsModule1;
        private DevExpress.ExpressApp.FileAttachments.Win.FileAttachmentsWindowsFormsModule fileAttachmentsWindowsFormsModule1;
        private DevExpress.ExpressApp.PivotChart.PivotChartModuleBase pivotChartModuleBase1;
        private DevExpress.ExpressApp.PivotChart.Win.PivotChartWindowsFormsModule pivotChartWindowsFormsModule1;
        private DevExpress.ExpressApp.PivotGrid.Win.PivotGridWindowsFormsModule pivotGridWindowsFormsModule1;
        private DevExpress.ExpressApp.ReportsV2.ReportsModuleV2 reportsModuleV21;
        private DevExpress.ExpressApp.ReportsV2.Win.ReportsWindowsFormsModuleV2 reportsWindowsFormsModuleV21;
        private DevExpress.ExpressApp.TreeListEditors.Win.TreeListEditorsWindowsFormsModule treeListEditorsWindowsFormsModule1;
        
        private Xpand.ExpressApp.SystemModule.XpandSystemModule xpandSystemModule1;
        private Xpand.ExpressApp.Security.XpandSecurityModule xpandSecurityModule1;
        private Xpand.ExpressApp.ModelDifference.ModelDifferenceModule modelDifferenceModule1;
        private Xpand.ExpressApp.Logic.LogicModule logicModule1;
        private Xpand.ExpressApp.ModelArtifactState.ModelArtifactStateModule modelArtifactStateModule1;
        private Xpand.ExpressApp.IO.IOModule ioModule1;
        private Xpand.ExpressApp.ModelDifference.Win.ModelDifferenceWindowsFormsModule modelDifferenceWindowsFormsModule1;
        private Xpand.ExpressApp.Email.EmailModule emailModule1;
        private Xpand.ExpressApp.AdditionalViewControlsProvider.AdditionalViewControlsModule additionalViewControlsModule1;
        private Xpand.ExpressApp.AdditionalViewControlsProvider.Win.AdditionalViewControlsProviderWindowsFormsModule additionalViewControlsProviderWindowsFormsModule1;
        private Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule xpandViewVariantsModule1;
        private Xpand.ExpressApp.Security.Win.XpandSecurityWinModule xpandSecurityWinModule1;
        private Xpand.ExpressApp.Win.SystemModule.XpandSystemWindowsFormsModule xpandSystemWindowsFormsModule1;
        private Xpand.ExpressApp.TreeListEditors.XpandTreeListEditorsModule xpandTreeListEditorsModule1;
        private Xpand.ExpressApp.TreeListEditors.Win.XpandTreeListEditorsWinModule xpandTreeListEditorsWinModule1;
        private Xpand.ExpressApp.IO.Win.IOWinModule ioWinModule1;
    
        private EarthCape.Module.Core.EarthCapeModule earthCapeModule1;
       // private EarthCape.Module.Lab.EarthCapeLabModule earthCapeLabModule1;
         private EarthCape.Module.GIS.EarthCapeGISModule earthCapeGISModule1;
        private EarthCape.Module.GIS.Win.EarthCapeGISWinModule earthCapeGISWinModule1;
      //  private EarthCape.Module.Importer.Win.EarthCapeImporterWinModule earthCapeImporterWinModule1;
    
        private DevExpress.ExpressApp.AuditTrail.AuditTrailModule auditTrailModule1;
   //     private DevExpress.ExpressApp.Dashboards.DashboardsModule dashboardsModule1;
    //    private DevExpress.ExpressApp.Dashboards.Win.DashboardsWindowsFormsModule dashboardsWindowsFormsModule1;
        private DevExpress.ExpressApp.HtmlPropertyEditor.Win.HtmlPropertyEditorWindowsFormsModule htmlPropertyEditorWindowsFormsModule1;
        private EarthCape.Module.GBIF.EarthCapeGBIFModule earthCapeGBIFModule1;
        private DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule businessClassLibraryCustomizationModule1;
        private LlamachantFramework.Module.LlamachantFrameworkModule llamachantFrameworkModule1;
        private LlamachantFramework.Module.Win.LlamachantFrameworkWindowsFormsModule llamachantFrameworkWindowsFormsModule1;
        private DevExpress.ExpressApp.Office.Win.OfficeWindowsFormsModule officeWindowsFormsModule1;
        private DevExpress.ExpressApp.Notifications.NotificationsModule notificationsModule1;
        private Xpand.ExpressApp.ExcelImporter.ExcelImporterModule excelImporterModule1;
        private DevExpress.ExpressApp.Notifications.Win.NotificationsWindowsFormsModule notificationsWindowsFormsModule1;
        private Xpand.ExpressApp.ExcelImporter.Win.ExcelImporterWinModule excelImporterWinModule1;
        private DevExpress.ExpressApp.Scheduler.SchedulerModuleBase schedulerModuleBase1;
        private DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule schedulerWindowsFormsModule1;
        private Xpand.ExpressApp.TreeListEditors.XpandTreeListEditorsModule xpandTreeListEditorsModule2;
        private Xpand.ExpressApp.TreeListEditors.Win.XpandTreeListEditorsWinModule xpandTreeListEditorsWinModule2;
       private Xpand.ExpressApp.Dashboard.DashboardModule dashboardModule1;
    //   private Xpand.ExpressApp.XtraDashboard.Win.DashboardWindowsFormsModule dashboardWindowsFormsModule1;
        private Xpand.XAF.Modules.ModelViewInheritance.ModelViewInheritanceModule modelViewInheritanceModule1;
        private Module.Geolocate.EarthCapeGeolocateModule earthCapeGeolocateModule1;
        private Module.Lab.EarthCapeLabModule earthCapeLabModule1;
        private Module.Lab.Win.EarthCapeLabModuleWin earthCapeLabModuleWin1;
        private Xpand.ExpressApp.XtraDashboard.Win.DashboardWindowsFormsModule dashboardWindowsFormsModule1;
        private Module.BHL.EarthCapeBHLModule earthCapeBHLModule1;
        private Module.BHL.Win.EarthCapeBHLWinModule earthCapeBHLWinModule1;
        private DevExpress.ExpressApp.StateMachine.StateMachineModule stateMachineModule1;
        private Xpand.ExpressApp.StateMachine.XpandStateMachineModule xpandStateMachineModule1;
        private Xpand.XAF.Modules.CloneModelView.CloneModelViewModule cloneModelViewModule1;
        private Xpand.XAF.Modules.CloneMemberValue.CloneMemberValueModule cloneMemberValueModule1;
        private Xpand.XAF.Modules.HideToolBar.HideToolBarModule hideToolBarModule1;
        private Xpand.XAF.Modules.ProgressBarViewItem.ProgressBarViewItemModule progressBarViewItemModule1;
        private Xpand.XAF.Modules.RefreshView.RefreshViewModule refreshViewModule1;
        private Xpand.XAF.Modules.ViewEditMode.ViewEditModeModule viewEditModeModule1;
        private Xpand.XAF.Modules.MasterDetail.MasterDetailModule masterDetailModule1;
        private Xpand.XAF.Modules.AutoCommit.AutoCommitModule autoCommitModule1;
        private Xpand.XAF.Modules.SuppressConfirmation.SuppressConfirmationModule suppressConfirmationModule1;
     //   private Xpand.XAF.Modules.ModelMapper.ModelMapperModule modelMapperModule1;
        private Xpand.XAF.Modules.GridListEditor.GridListEditorModule gridListEditorModule1;
        private DevExpress.ExpressApp.Office.OfficeModule officeModule1;
        private Xpand.ExpressApp.WizardUI.Win.WizardUIWindowsFormsModule wizardUIWindowsFormsModule1;
        private Xpand.XAF.Modules.Reactive.ReactiveModule reactiveModule2;
        private DevExpress.ExpressApp.ScriptRecorder.ScriptRecorderModuleBase scriptRecorderModuleBase1;
        private DevExpress.ExpressApp.ScriptRecorder.Win.ScriptRecorderWindowsFormsModule scriptRecorderWindowsFormsModule1;
         private Xpand.XAF.Modules.Reactive.Logger.ReactiveLoggerModule reactiveLoggerModule1;
        private Xpand.ExpressApp.Reports.XpandReportsModule xpandReportsModule1;
        private Xpand.ExpressApp.ReportsV2.Win.ReportsV2WinModule reportsV2WinModule1;
        private Xpand.ExpressApp.Validation.XpandValidationModule xpandValidationModule1;
        private Xpand.ExpressApp.Validation.Win.XpandValidationWinModule xpandValidationWinModule1;
    }
}
