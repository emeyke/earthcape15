﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Maps.Web;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class MapNewMarkerController : ViewController, IXafCallbackHandler
    {
        const string HandlerId = "MapNewMarkerController";
        public MapNewMarkerController()
        {
            TargetObjectType = typeof(IMapsMarker);
            TargetViewType = ViewType.ListView;
        }
        public void ProcessAction(string parameter)
        {
           /* var parameters = parameter.Split(',');
            var objectSpace = Application.CreateObjectSpace(View.ObjectTypeInfo.Type);
            var marker = (IMapsMarker)objectSpace.CreateObject(View.ObjectTypeInfo.Type);
            View.ObjectTypeInfo.FindMember("Latitude").SetValue(marker, double.Parse(parameters[0]));
            View.ObjectTypeInfo.FindMember("Longitude").SetValue(marker, double.Parse(parameters[1]));
            var detailView = Application.CreateDetailView(objectSpace, marker);
            Application.ShowViewStrategy.ShowViewInPopupWindow(detailView, () => { View.ObjectSpace.Refresh(); });
            */
            
            var parameters = parameter.Split(',');
            var objectSpace = Application.CreateObjectSpace(View.ObjectTypeInfo.Type);
            var marker = (IMapsMarker)objectSpace.CreateObject(View.ObjectTypeInfo.Type);
            View.ObjectTypeInfo.FindMember("Latitude").SetValue(marker, double.Parse(parameters[0]));
            View.ObjectTypeInfo.FindMember("Longitude").SetValue(marker, double.Parse(parameters[1]));
            if (typeof(Unit).IsAssignableFrom(View.ObjectTypeInfo.Type))
                ((Unit)marker).Date = DateTime.Now;
            var detailView = Application.CreateDetailView(objectSpace,((ListView)View).Model.DetailView.Id,true, marker);
            detailView.Caption = "Create new "+detailView.Caption;
            Application.ShowViewStrategy.ShowViewInPopupWindow(detailView, () => 
            { 
                View.ObjectSpace.Refresh(); 
            });
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            bool granted = SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Create));
            if (granted)
            {
                WebMapsListEditor mapsListEditor = ((ListView)View).Editor as WebMapsListEditor;
                if (mapsListEditor != null)
                {
                    XafCallbackManager callbackManager = ((ICallbackManagerHolder)WebWindow.CurrentRequestPage).CallbackManager;
                    mapsListEditor.MapViewer.ClientSideEvents.MapClicked =
                    "function(sender, e) { "
                    + "window.delayedMapClick = window.setTimeout(function(){ "
                    + callbackManager.GetScript(HandlerId, "('' + e.lat + ',' + e.lng)")
                    + " }, 2000);"
                    + " }";
                    callbackManager.RegisterHandler(HandlerId, this);
                    mapsListEditor.MapViewer.ClientSideEvents.Customize = @"function(s,map){ map.on('ready', function(e){  
    var map = e.originalMap;  
    map.set('disableDoubleClickZoom', true);  
    map.addListener('dblclick', function (event) {  
        window.clearTimeout(window.delayedMapClick);  
        map.setZoom(map.getZoom()+1);  
    });  
}); }";
                }
            }
            
        }
    }
}
