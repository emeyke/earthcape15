﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Maps.Web;
using DevExpress.ExpressApp.Maps.Web.Helpers;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.ExpressApp.Web;

namespace EarthCape.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Map_ViewController : ViewController,IXafCallbackHandler
    {
        const string HandlerId = "Map_ViewController";
        public Map_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetObjectType = typeof(IMapsMarker);
        }
        public void ProcessAction(string parameter)
        {
            var parameters = parameter.Split(',');
            var objectSpace = Application.CreateObjectSpace(View.ObjectTypeInfo.Type);
            var marker = (IMapsMarker)objectSpace.CreateObject(View.ObjectTypeInfo.Type);
            View.ObjectTypeInfo.FindMember("Latitude").SetValue(marker, double.Parse(parameters[0]));
            View.ObjectTypeInfo.FindMember("Longitude").SetValue(marker, double.Parse(parameters[1]));
            var detailView = Application.CreateDetailView(objectSpace, marker);
            Application.ShowViewStrategy.ShowViewInPopupWindow(detailView, () => { View.ObjectSpace.Refresh(); });
        }

        // private NewObjectViewController controller;
        // private bool ismap = false;
        protected override void OnActivated()
        {
            base.OnActivated();
         }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Read)))
            {
                WebMapsListEditor mapsListEditor = ((ListView)View).Editor as WebMapsListEditor;
                if (mapsListEditor == null)
                {
                    parametrizedAction_SearchMap.Active.SetItemValue("Mapeditor", false);
                }
              /*  if (mapsListEditor != null)
                {
                    parametrizedAction_SearchMap.Active.SetItemValue("Mapeditor", true);
                    XafCallbackManager callbackManager = ((ICallbackManagerHolder)WebWindow.CurrentRequestPage).CallbackManager;
                    mapsListEditor.MapViewer.ClientSideEvents.MapClicked =
                    "function(sender, e) { "
                    + "window.delayedMapClick = window.setTimeout(function(){ "
                    + callbackManager.GetScript(HandlerId, "('' + e.lat + ',' + e.lng)")
                    + " }, 2000);"
                    + " }";
                    callbackManager.RegisterHandler(HandlerId, this);
                    mapsListEditor.MapViewer.ClientSideEvents.Customize = GetCustomizeScript();// ;
                }*/
            }
        }
        private string GetCustomizeScript()
        {
            return @"function(s,map){ 
                        map.on('ready', function(e){
                            var map = e.originalMap;
                            map.set('disableDoubleClickZoom', true);
                            map.addListener('dblclick', function (event) {
                                window.clearTimeout(window.delayedMapClick);
                                map.setZoom(map.getZoom()+1);
                            });
               
               
                        }); 
                    }";
  }

        /*
                    var mapInstance =e;
                            var googleMarkers = mapInstance.component._provider._markers;

                            var mcOptions = {gridSize: 50, maxZoom: 15, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'};
                            var mc = new MarkerClusterer(map, googleMarkers, mcOptions);
               
             */

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
             base.OnDeactivated();
        }

        private void parametrizedAction_SearchMap_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            WebMapsListEditor mapsListEditor = ((ListView)View).Editor as WebMapsListEditor;
            if (mapsListEditor != null)
            {
                mapsListEditor.MapViewer.ClientSideEvents.Customize = @"
                    function(sender, map) {
                            map.option('center', '" + e.ParameterCurrentValue.ToString() + @"');
                            map.option('autoAdjust', false);
                    }";
                mapsListEditor.MapViewer.MapSettings.ZoomLevel = 10;

            }
        }
    }
}
