﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Maps.Web;
using DevExpress.Persistent.Base;

// ...
namespace EarthCape.Module.Core
{
    public class MarkerIconController : ObjectViewController<ListView, Unit>
    {
        public MarkerIconController()
        {
            TargetViewId = "Unit_ListView_WebMap";
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            WebMapsListEditor webMapsListEditor = View.Editor as WebMapsListEditor;
            if (webMapsListEditor != null)
            {
                if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(Unit)))
                {
                    webMapsListEditor.MapViewer.MapSettings.IconSrcPropertyName = "MapLetter";
                }
            }
        }
       }
}