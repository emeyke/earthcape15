﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.PivotGrid.Web;
using DevExpress.ExpressApp.Scheduler.Web;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Web;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;

namespace EarthCape.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GriViewOptions_ViewController : ViewController
    {
        public GriViewOptions_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            this.TargetViewType = ViewType.ListView;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
            if (((ListView)View).Editor != null)
                if (((ListView)View).Editor.Control != null)
                {
                    if (((ListView)View).Editor is ASPxGridListEditor)
                    {
                        ASPxGridView gridView = (ASPxGridView)((ListView)View).Editor.Control;
                        gridView.EnablePagingGestures = AutoBoolean.False;
                        gridView.SettingsAdaptivity.AdaptivityMode = GridViewAdaptivityMode.HideDataCellsWindowLimit;
                        gridView.SettingsAdaptivity.HideDataCellsAtWindowInnerWidth = 500;
                    }
                    if (((ListView)View).Editor is ASPxPivotGridListEditor)
                    {
                        ASPxPivotGridListEditor gridView = (ASPxPivotGridListEditor)((ListView)View).Editor;
                        ASPxPivotGrid grid = gridView.PivotGridControl;
                        grid.OptionsView.HorizontalScrollBarMode = ScrollBarMode.Visible;
                        grid.OptionsView.HorizontalScrollingMode = PivotScrollingMode.Standard;
                        grid.OptionsView.VerticalScrollBarMode = ScrollBarMode.Visible;
                        grid.OptionsView.VerticalScrollingMode = PivotScrollingMode.Standard;
                    }
                    if (((ListView)View).Editor is ASPxSchedulerListEditor)
                    {
                        ASPxScheduler grid = ((ASPxSchedulerListEditor)((ListView)View).Editor).SchedulerControl;
                        grid.ResourceNavigator.Mode = ResourceNavigatorMode.Tokens;
                        grid.ResourceNavigator.Visibility = ResourceNavigatorVisibility.Always;
                        grid.OptionsView.ResourceHeaders.ShowCaption = true;
                        grid.OptionsView.ResourceHeaders.RotateCaption = false;
                    }
                }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
