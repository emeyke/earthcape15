﻿namespace EarthCape.Module.Web.Controllers
{
    partial class Map_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.parametrizedAction_SearchMap = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // parametrizedAction_SearchMap
            // 
            this.parametrizedAction_SearchMap.Caption = "Search map";
            this.parametrizedAction_SearchMap.Category = "Search";
            this.parametrizedAction_SearchMap.ConfirmationMessage = null;
            this.parametrizedAction_SearchMap.Id = "parametrizedAction_SearchMap";
            this.parametrizedAction_SearchMap.NullValuePrompt = null;
            this.parametrizedAction_SearchMap.ShortCaption = "Search map";
            this.parametrizedAction_SearchMap.ToolTip = null;
            this.parametrizedAction_SearchMap.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.parametrizedAction_SearchMap_Execute);
            // 
            // Map_ViewController
            // 
            this.Actions.Add(this.parametrizedAction_SearchMap);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction parametrizedAction_SearchMap;
    }
}
