﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using DevExpress.Compression;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Spreadsheet;
using DevExpress.SpreadsheetSource.Implementation;
using DevExpress.Web;
using DevExpress.Xpo;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;
using EarthCape.Module.Core;
using EarthCape.Module.GBIF;
using Xpand.ExpressApp;
using Xpand.ExpressApp.Web.ListEditors;
using static DevExpress.DataAccess.Sql.SortingInfo;

namespace EarthCape.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Dwc_ViewController : ViewController
    {
        public Dwc_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PopupWindowShowAction_ExportDwC_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

             DwcOptions options = (DwcOptions)e.PopupWindowView.CurrentObject;
               View.ControlsCreated += View_ControlsCreated;
           

        }
        DwcOptions options = null;
        private void View_ControlsCreated(object sender, EventArgs e)
        {
            Dataset ds = (Dataset)(((NestedFrame)Frame).ViewItem.View.CurrentObject);
            using (FileStream file = new FileStream(String.Format(@"{0}\dwc\occurrence.csv", PathHelper.GetApplicationFolder()), FileMode.Create, FileAccess.ReadWrite))
            {
                using (Workbook wb = new Workbook())
                {
                    DevExpress.Spreadsheet.Worksheet sheet = wb.Worksheets[0];
                    ListView view = (ListView)View;
                    ExternalDataSourceOptions dsOptions = new ExternalDataSourceOptions();
                    dsOptions.SkipHiddenColumns = true;
                    dsOptions.ImportHeaders = true;
                    ASPxGridView gridView = (ASPxGridView)((ListView)View).Editor.Control;
                    CsvExportOptionsEx options = new CsvExportOptionsEx();
                    options.Encoding = Encoding.UTF8;
                    // gridView.ExportToCsv(file, options);

                    XPView xpv = new XPView(new Session(GeneralHelper.GetDataLayer(Application.ConnectionString)), typeof(Unit));
                    IModelColumns cols = view.Model.Columns;
                    cols.OrderBy(f => f.Caption);
                    int i = 0;
                    foreach (IModelColumn col in cols)
                        xpv.AddProperty(col.Caption, col.PropertyName);
                    view.SaveModel(); 
                    xpv.Criteria = CriteriaOperator.Parse(view.Model.Filter);
                    var items = xpv.Cast<ViewRecord>();
                    //        var items = Application.CreateObjectSpace().GetObjects(typeof(Unit), CriteriaOperator.Parse(view.Model.Filter)).;

                    // Specify the binding options.

                    //Create data binding
                    WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindToDataSource(items, 0, 0, dsOptions);
                    sheetDataBinding.Range.AutoFitColumns();


                    //((XPBaseCollection)((ProxyCollection)((ListView)View).CollectionSource.Collection).OriginalCollection).Load();
                    // var list = ((IListSource)view.CollectionSource.Collection).GetList();
                    // ASPxGridViewExporter exporter = new ASPxGridViewExporter();
                    // ((Control)View.Control).Controls.Add(exporter);
                    //exporter.GridViewID = gridView.ID;
                    //exporter.WriteCsv(file);
                    // WorksheetDataBinding sheetDataBinding = sheet.DataBindings.BindTableToDataSource(gridView.DataSource);
                    // wb.Options.Export.Csv.QuotationMode = DevExpress.XtraSpreadsheet.Export.TextQuotationMode.Auto;
                    // wb.Options.Export.Csv.ValueSeparator = ';';


                    wb.SaveDocument(file, DocumentFormat.Csv);

                    /*                ASPxGridView gridView = (ASPxGridView)((ListView)View).Editor.Control;
                                        gridView.ExportToCsv(file);*/

                    file.Flush();
                    file.Close();

                    GbifHelper.CreateMeta((IModelListView)Application.Model.Views["Unit_ListView_DwC"]);
                    GbifHelper.CreateEML(ds);
                    using (ZipArchive archive = new ZipArchive())
                    {
                        archive.AddFile(Path.GetTempPath() + @"\meta.xml", @"\");
                        archive.AddFile(Path.GetTempPath() + @"\eml.xml", @"\");
                        archive.AddFile(String.Format(@"{0}\dwc\occurrence.csv", PathHelper.GetApplicationFolder()), @"\");
                        archive.Save(String.Format(@"{0}\dwc\{1}.zip", PathHelper.GetApplicationFolder(), ds.Oid));
                    }
                }
            }
            View.ControlsCreated -= View_ControlsCreated;
        }

        private void PopupWindowShowAction_ExportDwC_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            DwcOptions param = obs.CreateObject<DwcOptions>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;
        }

     
    }

      
    }
