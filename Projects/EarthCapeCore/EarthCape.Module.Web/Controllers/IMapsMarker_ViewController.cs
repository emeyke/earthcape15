﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IMapsMarker_ViewController : ViewController<DetailView>
    {
        private SimpleAction getPositionAction;
        public IMapsMarker_ViewController()
        {
            InitializeComponent();

            TargetObjectType = typeof(IMapsMarker);
            getPositionAction = new SimpleAction(
               this, "GetCurrentPosition",
               PredefinedCategory.Edit);
            getPositionAction.ConfirmationMessage = "This will overwrite current Latitude and Longitude values for current! Proceed?";
            string getPositionClientScript = @"
function setLocalizedValueToInput(input, value) {
    var curValue = input.GetValue().toString(); //get editors value
    var newValue = value.toString();    
    var isCommaAsFloatPoint = curValue.indexOf(',') > -1;
    
    if (isCommaAsFloatPoint)
        newValue = newValue.replace('.', ',');
     input.SetValue(newValue); //set editor's value
}
 function showMap(position) {
  //var latInputs = idContains('input', 'Latitude_Edit(?!.*Info$)');  
  let latEdit = ASPxClientControl.GetControlCollection().GetByName('Latitude'); //get editor
  let lonEdit = ASPxClientControl.GetControlCollection().GetByName('Longitude'); //get editor
  let accEdit = ASPxClientControl.GetControlCollection().GetByName('CoordAccuracy'); //get editor
//  let altEdit = ASPxClientControl.GetControlCollection().GetByName('Altitude'); //get editor
   var latitude = position.coords.latitude; //get coordinate
   var longitude = position.coords.longitude;
   var accuracy = position.coords.accuracy;
   var altitude = position.coords.altitude;
  //var lonInputs = idContains('input', 'Longitude_Edit(?!.*Info$)'); 
  setLocalizedValueToInput(lonEdit, longitude);
  setLocalizedValueToInput(latEdit, latitude); 
  setLocalizedValueToInput(accEdit, accuracy); 
  //if (lonInputs.length == 1) { setLocalizedValueToInput(lonInputs[0], longitude); }
}
 navigator.geolocation.getCurrentPosition(showMap);";
            getPositionAction.Caption = "Get current position";
            getPositionAction.ImageName = "BO_Localization";
            getPositionAction.SetClientScript(getPositionClientScript, false);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            View.ViewEditModeChanged += DetailView_ViewEditModeChanged;
            UpdateActionState();
            ASPxDoublePropertyEditor latPropertyEditor = ((DetailView)View).FindItem("Latitude") as ASPxDoublePropertyEditor;
            if (latPropertyEditor != null)
            {
                if (latPropertyEditor.Control != null)
                {
                    SetClientName(latPropertyEditor);
                }
                else
                {
                    latPropertyEditor.ControlCreated += new EventHandler<EventArgs>(propertyEditor_ControlCreated);
                }
            }



            //////longitude

            ASPxDoublePropertyEditor longPropertyEditor = ((DetailView)View).FindItem("Longitude") as ASPxDoublePropertyEditor;
            if (longPropertyEditor != null)
            {
                if (longPropertyEditor.Control != null)
                {
                    SetClientNameLongitude(longPropertyEditor);
                }
                else
                {
                    longPropertyEditor.ControlCreated += new EventHandler<EventArgs>(propertyEditor_ControlCreatedLongitude);
                }
            }
        }

  
        void SetClientName(ASPxDoublePropertyEditor editor)
        {
            if (editor != null)
                if (editor.Editor != null)
                    editor.Editor.ClientInstanceName = "Latitude"; //set editor's clientinstancename
        }

        void SetClientNameLongitude(ASPxDoublePropertyEditor editor)
        {
            if (editor != null)
                if (editor.Editor != null)
                    editor.Editor.ClientInstanceName = "Longitude"; //set editor's clientinstancename
        }



        private void propertyEditor_ControlCreated(object sender, EventArgs e)
        {
            SetClientName((ASPxDoublePropertyEditor)sender);
        }

        private void propertyEditor_ControlCreatedLongitude(object sender, EventArgs e)
        {
            SetClientNameLongitude((ASPxDoublePropertyEditor)sender);
        }
        protected override void OnDeactivated()
        {
            View.ViewEditModeChanged -= DetailView_ViewEditModeChanged;
            base.OnDeactivated();
        }
        private void DetailView_ViewEditModeChanged(object sender, System.EventArgs e)
        {
            UpdateActionState();
        }
        private void UpdateActionState()
        {
            getPositionAction.Enabled["EditMode"] = View.ViewEditMode == DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            bool granted = SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write));
            getPositionAction.Active.SetItemValue("SecurityAllowance", granted);
        }

    }
}
