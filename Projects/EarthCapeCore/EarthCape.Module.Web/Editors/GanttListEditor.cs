﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Web;
using DevExpress.Web.ASPxGantt;
using DevExpress.Web.ASPxGantt.Internal;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Web
{
    [ListEditor(typeof(UnitTask))]
    public class GanttListEditor : ListEditor, IComplexListEditor
    {
        ASPxGantt gantt;

        private CollectionSourceBase _CollectionSource;
        private XafApplication _Application;
        private IObjectSpace _ObjectSpace;

        /// <summary>
        /// 1.
        /// </summary>
        /// <param name="info"></param>
        public GanttListEditor(IModelListView info) : base(info)
        {

        }

        /// <summary>
        /// 2.
        /// </summary>
        /// <param name="collectionSource"></param>
        /// <param name="application"></param>
        public void Setup(CollectionSourceBase collectionSource, XafApplication application)
        {
            _CollectionSource = collectionSource;
            _Application = application;
            _ObjectSpace = application.CreateObjectSpace();
        }

        /// <summary>
        /// 3.
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void AssignDataSourceToControl(Object dataSource)
        {
            if (gantt != null)
            {
                IList<UnitTask> taskCollection = _ObjectSpace.GetObjects<UnitTask>();
               // IList<Dependency> dependenciesCollection = _ObjectSpace.GetObjects<Dependency>();
               /// IList<TaskResource> taskResourceCollection = _ObjectSpace.GetObjects<TaskResource>();
               // IList<TaskResourceAssignment> taskResourceAssignmentCollection = _ObjectSpace.GetObjects<TaskResourceAssignment>();
                gantt.TasksDataSource = taskCollection;
               //gantt.DependenciesDataSource = dependenciesCollection;
              // gantt.ResourceAssignmentsDataSource = taskResourceAssignmentCollection;
                gantt.DataBind();
                _ObjectSpace.Refresh();
            }
        }

        public override void Refresh()
        {
            if (gantt != null)
            {
                IList<UnitTask> taskCollection = _ObjectSpace.GetObjects<UnitTask>();
              //  IList<Dependency> dependenciesCollection = _ObjectSpace.GetObjects<Dependency>();
               // IList<TaskResource> taskResourceCollection = _ObjectSpace.GetObjects<TaskResource>();
             //   IList<TaskResourceAssignment> taskResourceAssignmentCollection = _ObjectSpace.GetObjects<TaskResourceAssignment>();
                gantt.TasksDataSource = taskCollection;
              //  gantt.DependenciesDataSource = dependenciesCollection;
             //   gantt.ResourcesDataSource = taskResourceCollection;
               // gantt.ResourceAssignmentsDataSource = taskResourceAssignmentCollection;
                gantt.DataBind();
                _ObjectSpace.Refresh();
            }
        }

        /// <summary>
        /// Kayıtları seçme şeklini belirtir.
        /// 5.
        /// </summary>
        public override SelectionType SelectionType
        {
            get
            {
                return SelectionType.TemporarySelection;
            }
        }

        /// <summary>
        /// Seçili objelerin listesini döndürür.
        /// 4.
        /// </summary>
        /// <returns></returns>
        public override IList GetSelectedObjects()
        {
            List<object> selectedObjects = new List<object>();
            if (gantt != null && gantt.TasksDataSource != null)
            {
                if (ListHelper.GetList(gantt.TasksDataSource).Count > 0)
                {
                    selectedObjects.Add(ListHelper.GetList(gantt.TasksDataSource)[0]);
                }
            }
            return selectedObjects;
        }

        protected override object CreateControlsCore()
        {
            gantt = new ASPxGantt();
            gantt.ID = "ASPxGantt1";
            gantt.Height = 700;


            gantt.SettingsGanttView.ViewType = GanttViewType.Days;
              gantt.SettingsTaskList.Columns.Add(new TaskDataColumn(GanttTaskField.Title));
            gantt.SettingsTaskList.Columns.Add(new TaskDataColumn(GanttTaskField.Start));
            gantt.SettingsTaskList.Columns.Add(new TaskDataColumn(GanttTaskField.End));
            gantt.SettingsTaskList.Columns.Add(new TaskDataColumn(GanttTaskField.Progress));

            gantt.SettingsEditing.AllowResourceUpdate = true;

            gantt.SettingsEditing.AllowTaskUpdate = true;
            gantt.SettingsEditing.AllowTaskInsert = true;
      //      gantt.Mappings.Task
            gantt.Mappings.Task.Key = "Oid";
            gantt.Mappings.Task.Title = "Subject";
            gantt.Mappings.Task.ParentKey = "Parent";
            gantt.Mappings.Task.Start = "StartDate";
            gantt.Mappings.Task.End = "DueDate";
            gantt.Mappings.Task.Progress = "PercentCompleted";
         //    gantt.Mappings.Dependency.Key = "Name";
        //    gantt.Mappings.Dependency.PredecessorKey = "Parent";
          ////  gantt.Mappings.Dependency.SuccessorKey = "DependentID";
           // gantt.Mappings.Dependency.DependencyType = "Type";

         /*   gantt.Mappings.Resource.Key = "Unit";
            gantt.Mappings.Resource.Name = "Unit";

            gantt.Mappings.ResourceAssignment.Key = "AssignedTo";
            gantt.Mappings.ResourceAssignment.TaskKey = "Name";
            gantt.Mappings.ResourceAssignment.ResourceKey = "ResourceID";*/

            gantt.DataBind();

            return gantt;
        }
    }
}
