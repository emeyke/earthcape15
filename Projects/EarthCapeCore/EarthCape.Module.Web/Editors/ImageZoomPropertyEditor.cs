using System;
using System.Drawing;
using System.Net;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web.Editors;
using DevExpress.Web;
using EarthCape.Module.Core;
// https://www.devexpress.com/support/center/Question/Details/T323893#comment-ecd03dd0-5a28-4594-8a46-a9ae137a0880
//using Image = System.Web.UI.WebControls.Image;

namespace EarthCape.Module.Web.Editors
{
    [PropertyEditor(typeof(System.Drawing.Image), false)]
    public class ImageZoomPropertyEditor : WebPropertyEditor
    {
        public ImageZoomPropertyEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
        }

        ASPxImageZoom control;
        protected override WebControl CreateViewModeControlCore()
        {
            control = new ASPxImageZoom { ID = "editor"+(new Guid()).ToString() };
            control.EnableExpandMode = true;
            control.EnableZoomMode = true;
            control.Width = 200;
            control.Height = 200;
            control.LargeImageLoadMode = LargeImageLoadMode.OnFirstShow;
            control.SettingsZoomMode.ZoomWindowHeight = 500;
            control.SettingsZoomMode.ZoomWindowWidth = 500;
             return control;
        }
        protected override WebControl CreateEditModeControlCore()
        {
            return new ASPxImageZoom { ID = "editor" + (new Guid()).ToString() };
        }

        protected override void ReadEditModeValueCore()
        {
            ReadViewModeValueCore();
        }

        protected override void ReadViewModeValueCore()
        {
            if (control != null)
            {
                if (View != null)
                {
                    if (View.CurrentObject != null)
                    {
                        var ed = control;
                        if (View.CurrentObject is UnitAttachment)
                            ed.ImageUrl = ((UnitAttachment)View.CurrentObject).JpegUrl;
                    }
                }
            }
        }

        protected override object GetControlValueCore()
        {
            var ed = (ASPxImageZoom)Editor;
            var webClient = new WebClient();
            var imageBytes = webClient.DownloadData(ed.ImageUrl);
            return imageBytes;
        }
    }
}