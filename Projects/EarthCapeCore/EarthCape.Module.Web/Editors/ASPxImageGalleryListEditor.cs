﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.Web;
using EarthCape.Module.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace EarthCape.Module.Web.Editors
{
    [ListEditor(typeof(UnitAttachment))]
    public class ASPxImageGalleryListEditor : ListEditor
    {
        bool hasSelected;
        List<object> selectedObjects = new List<object>();
        private ASPxImageGalleryListEditorControl control;
        private object focusedObject;
        public ASPxImageGalleryListEditor(IModelListView info) : base(info) { }

        protected override object CreateControlsCore()
        {
            control = new ASPxImageGalleryListEditorControl();
            control.ID = "ASPxImageGallery1";
            control.ClientInstanceName = "imageGallery";
           // control.FullscreenViewerThumbnailUrlField = "JpegUrl";
          //  control.ThumbnailUrlField = "JpegUrl";
            control.image = "Thumbnail";
            control.ImageUrlField = "JpegUrl";
            control.NameField = "Name";
            control.OnClick += new EventHandler<CustomListEditorClickEventArgs>(control_OnClick);
            control.OnSelect += new EventHandler<CustomListEditorSelectEventArgs>(Control_OnSelect);

            //control = new ASPxImageSlider();
            //control.ID = "ASPxImageSlider1";//!!!
            //control.ClientInstanceName = "imageSlider";
            //control.ThumbnailUrlField = "Url";
            //control.ImageUrlField = "Url";
            //control.NavigateUrlField = "Url";

            //control = new ASPxImageGallery();
            //control.ID = "ASPxImageGallery";//!!!
            //control.ClientInstanceName = "ASPxImageGallery";
            //control.FullscreenViewerThumbnailUrlField = "Url";
            //control.ThumbnailUrlField = "Url";
            //control.EnableViewState = true;
            //control.SettingsFullscreenViewer.Visible = true;
            //control.CustomCallback += Control_CustomCallback;
            //control.ClientSideEvents.FullscreenViewerShowing = "OnFullscreenViewerShowing";
            //control.ClientSideEvents.Init = @"
            //    var imageIndex = 0;
            //    function OnFullscreenViewerShowing(s, e) {
            //        imageIndex = e.index;
            //        alert('alert');
            //        //ASPxCallbackPanel.PerformCallback(imageIndex);
            //        popup.Show();
            //    }
            //";

            //panel = new ASPxCallbackPanel();
            //panel.ID = "ASPxCallbackPanel";//!!!
            //panel.ClientInstanceName = "ASPxCallbackPanel";//!!!
            //panel.Callback += Panel_Callback;
            //panel.Controls.Add(control);

            return control;
        }

        private void Control_CustomCallback(object sender, CallbackEventArgsBase e)
        {
            
        }

        private void Panel_Callback(object sender, CallbackEventArgsBase e)
        {

        }

        private void control_OnClick(object sender, CustomListEditorClickEventArgs e)
        {
            this.hasSelected = false;
            this.FocusedObject = e.ItemClicked;
            OnSelectionChanged();
            OnProcessSelectedItem();
        }
        private void Control_OnSelect(object sender, CustomListEditorSelectEventArgs e)
        {
            this.hasSelected = true;
            this.FocusedObject = e.ItemClicked;
            OnSelectionChanged();
        }

        protected override void AssignDataSourceToControl(Object dataSource)
        {
            if (control != null)
            {
                control.DataSource = ListHelper.GetList(dataSource);
                control.DataBind();
            }
        }
        public override IList GetSelectedObjects()
        {
            List<object> selectedObjects2 = new List<object>();
            if (FocusedObject != null)
            {
                if (hasSelected)
                {
                    selectedObjects2.AddRange(selectedObjects);
                }
                else
                {
                    selectedObjects2.Add(FocusedObject);
                }
            }
            return selectedObjects2;
        }
        public override void Refresh()
        {
            if (DataSource != null)
                AssignDataSourceToControl(this.DataSource);
        }
        public override object FocusedObject
        {
            get
            {
                return focusedObject;
            }
            set
            {
                focusedObject = value;
            }
        }
        public override DevExpress.ExpressApp.Templates.IContextMenuTemplate ContextMenuTemplate
        {
            get { return null; }
        }
        public override bool AllowEdit
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
        public override SelectionType SelectionType
        {
            get { return SelectionType.TemporarySelection; }
        }
    }

    public class ASPxImageGalleryListEditorControl : ASPxImageGallery, INamingContainer, IXafCallbackHandler
    {
        private IList dataSource;
        private Dictionary<string, System.Drawing.Image> images = new Dictionary<string, System.Drawing.Image>();
        private void RaiseItemClick(UnitAttachment item)
        {
            if (OnClick != null)
            {
                CustomListEditorClickEventArgs args = new CustomListEditorClickEventArgs();
                args.ItemClicked = item;
                OnClick(this, args);
            }
        }
        private void RaiseItemSelection(UnitAttachment item)
        {
            if (OnSelect != null)
            {
                CustomListEditorSelectEventArgs args = new CustomListEditorSelectEventArgs();
                args.ItemClicked = item;
                OnSelect(this, args);
            }
        }
        private UnitAttachment FindItemByID(Guid ID)
        {
            if (dataSource == null)
                return null;

            foreach (UnitAttachment item in dataSource)
            {
                if (item.Oid == ID)
                    return item;
            }
            return null;
        }
        //private UnitAttachment FindNextItem(UnitAttachment item)
        //{
        //    if (dataSource == null)
        //        return null;

        //    foreach (UnitAttachment d in dataSource)
        //    {
        //        if (d.Index == item.Index + 1)
        //            return d;
        //    }
        //    return null;
        //}
        //private UnitAttachment FindPreviousItem(UnitAttachment item)
        //{
        //    if (dataSource == null)
        //        return null;

        //    foreach (UnitAttachment d in dataSource)
        //    {
        //        if (d.Index == item.Index - 1)
        //            return d;
        //    }
        //    return null;
        //}
        //private byte[] ImageToByteArray(System.Drawing.Image image)
        //{
        //    if (image == null)
        //    {
        //        throw new ArgumentNullException("image");
        //    }
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        image.Save(ms, image.RawFormat);
        //        return ms.ToArray();
        //    }
        //}
        private XafCallbackManager CallbackManager
        {
            get { return Page != null ? ((ICallbackManagerHolder)Page).CallbackManager : null; }
        }
        private void ImageResourceHttpHandler_QueryImageInfo(object sender, ImageInfoEventArgs e)
        {
            if (e.Url.StartsWith("CLE"))
            {
                lock (images)
                {
                    if (images.ContainsKey(e.Url))
                    {
                        System.Drawing.Image image = images[e.Url];
                        e.ImageInfo = new DevExpress.ExpressApp.Utils.ImageInfo("", image, "");
                        images.Remove(e.Url);
                    }
                }
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Refresh();
        }
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Refresh();
        }
        public ASPxImageGalleryListEditorControl()
        {
            ImageResourceHttpHandler.QueryImageInfo += new EventHandler<ImageInfoEventArgs>(ImageResourceHttpHandler_QueryImageInfo);
        }
        public void Refresh()
        {

        }

        private void Tick_CheckedChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        //public IList DataSource
        //{
        //    get { return dataSource; }
        //    set { dataSource = value; }
        //}
        public event EventHandler<CustomListEditorClickEventArgs> OnClick;
        public event EventHandler<CustomListEditorSelectEventArgs> OnSelect;
        #region IXafCallbackHandler Members
        public void ProcessAction(string parameter)
        {
            var args = parameter.Split(';');

            //UnitAttachment item = FindItemByID((Guid)args[0]);
            
        }
        #endregion
    }

    public class CustomListEditorClickEventArgs : EventArgs
    {
        public UnitAttachment ItemClicked;
    }
    public class CustomListEditorSelectEventArgs : EventArgs
    {
        public UnitAttachment ItemClicked;
    }

}
