﻿using DevExpress.ExpressApp.Win.SystemModule;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Win.Controllers
{


    /// This controller ensures that the check for competitive changes in the
    /// database can only be run at certain intervals. That if checked, the
    /// next test again after ten seconds.
    /// </summary>
    public class MyVersionsCompatibilityController : VersionsCompatibilityController
    {
        public MyVersionsCompatibilityController()
        {
            Active["UpdateAllowed"] = false;
        }
    }

    public class DisableLockCheckController : LockController{
        /// <summary>

        /// These are the objects that are checked at the suspended intervals.
        /// </summary>
        private static IList emptyList = new ArrayList();

        /// <summary>
        /// The period to pass between the exams.
        /// </summary>
        private static TimeSpan duration = new TimeSpan(0, 0, 10000);

        /// <summary>
        ///When should the next test take place?
        /// </summary>
        private DateTime nextCheck = DateTime.MinValue;

        public override IList GetModifiedObjects(){
            return emptyList;

            // if (DateTime.Now <= nextCheck)
            //     {
            //         nextCheck = DateTime.Now + duration;
            //         return base.GetModifiedObjects();
            //     }
            //     else
            //     {
            //         return emptyList;
            //     }
            // }
        }
    }


}
