﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Win.Controllers
{
    public class MdiOptionsController : WindowController, IModelExtender
    {
        public MdiOptionsController()
        {
            this.TargetWindowType = WindowType.Main;
        }

   
       void IModelExtender.ExtendModelInterfaces(DevExpress.ExpressApp.Model.ModelInterfaceExtenders extenders)
        {
            extenders.Add(typeof(IModelOptions), typeof(IVDModelOptionsWin));
        }

        /// <summary>
        protected override void OnActivated()
        {
            base.OnActivated();

            var options = this.Application.Model.Options as IVDModelOptionsWin;
            if (options.TouchUIMode.HasValue)
                WindowsFormsSettings.TouchUIMode = options.TouchUIMode.Value;


        }
    }

    public interface IVDModelOptionsWin : IModelNode
    {
        /// <summary>
        /// Gets or sets the touch UI mode.
        /// </summary>
        /// <value>
        /// The touch UI mode.
        /// </value>
        [Category("Appearance")]
        TouchUIMode? TouchUIMode { get; set; }
    }
}