﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model;
using EarthCape.Module.Core;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using Xpand.ExpressApp.TreeListEditors.Win.ListEditors;
using System.IO;
using System.Runtime.InteropServices;
using DevExpress.ExpressApp.DC;
using Xpand.Persistent.Base.RuntimeMembers.Model;
using DevExpress.ExpressApp.Editors;
using DevExpress.XtraPrinting;
using DevExpress.XtraLayout;
using DevExpress.ExpressApp.Scheduler.Win;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Documentation_ViewController : ViewController
    {
        int navItemCount = 0;
        int viewCount = 0;
        List<IModelNavigationItem> myList;
        List<IModelListView> myViewList;
        int currentNavIndex;
        int currentViewIndex;
        bool listCreated;
        string path = "";
        string imagespath = "";
        string db = "";
        string link = "";
        string version;
           string prodversion;
        string date;
        public Documentation_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();

           
        }



        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        TextWriter toctw = null;
        TextWriter indextw = null;

        private void simpleAction_DocGeneration_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            
        }

        private void GenerateNavToc()
        {
            IModelNavigationItem node = null;
            var xafApplication = Application;
            var model = (xafApplication.Model);
            IModelList<IModelView> modelViews = model.Application.Views;
            IObjectSpace os = Application.CreateObjectSpace();
            IModelApplicationNavigationItems navigationItems = model as IModelApplicationNavigationItems;
            myList = new List<IModelNavigationItem>();
            foreach (IModelNode nd_ts in navigationItems.NavigationItems.AllItems)
            {
                if (nd_ts != null)
                {
                    node = (IModelNavigationItem)(nd_ts);
                    CreateToc(node);
                    if ((node.View != null) && ((typeof(IModelListView).IsAssignableFrom(node.View.GetType()))))
                    // if (((IModelListView)node.View).EditorType.Name != "XpandTreeListEditor")
                    {
                        if (node.Caption.IndexOf("model") < 0)
                        {
                            myList.Add(node);
                            CreateNavMdDoc(node.View);
                        }
                    }
                }
            }
        }
        private void GenerateBOModel()
        {
            IModelListView node = null;
            var xafApplication = Application;
            var model = (xafApplication.Model);
            IModelList<IModelView> modelViews = model.Application.Views;
            IObjectSpace os = Application.CreateObjectSpace();
            myViewList = new List<IModelListView>();

            foreach (var bo in model.BOModel)
            {
                if (bo.DefaultListView != null) {
                    IModelListView nd_ts = bo.DefaultListView;
                    if (nd_ts is IModelListView)
                    {
                        node = (IModelListView)(nd_ts);
                        //CreateToc(node);
                        /* if ((node.ModelClass.Name.Contains("EarthCape")))
                         {*/
                        myViewList.Add((IModelListView)nd_ts);
                            CreateNavMdDoc((IModelListView)nd_ts);
                     //   }
                    }
                }
            }

        }
        private void ProcessNextNavItemFromList()
        {
            if (currentNavIndex < myList.Count)
            {
                if (!listCreated)
                {
                    var node = myList[currentNavIndex];

                    var listview = Application.CreateListView(((IModelListView)((IModelView)node.View)).ModelClass.TypeInfo.Type, true);
                    if ((listview.Editor.GetType() != typeof(XpandTreeListEditor))
                        && (listview.Editor.GetType() != typeof(XpandTreeListEditor))
                         )
                    {
                        if (
                           (((IModelListView)((IModelView)node.View)).Caption.IndexOf("run") == -1)
                        &&
                           (((IModelListView)((IModelView)node.View)).Caption.IndexOf("Run") == -1)
                        &&
                        (((IModelListView)((IModelView)node.View)).Caption.IndexOf("rotocol") == -1)
                       &&
                        (((IModelListView)((IModelView)node.View)).Caption.IndexOf("vent") == -1)
                     &&
                        (((IModelListView)((IModelView)node.View)).Caption.IndexOf("alendar") == -1)
                         &&
                        (((IModelListView)((IModelView)node.View)).ModelClass.TypeInfo.Type.Name.IndexOf("model") == -1)
                        )
                        {
                            listview.ControlsCreated += Listview_ControlsCreated;
                            Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                            Frame.SetView(listview);
                            Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                            //    CreateDoc(node, tw);
                            listCreated = true;
                        }
                        else
                        {
                           

                            listview.ControlsCreated -= Listview_ControlsCreated;
                            currentNavIndex++;
                            ProcessNextNavItemFromList();
                        }
                    }
                    else
                    {
                        listview.ControlsCreated -= Listview_ControlsCreated;
                        currentNavIndex++;
                        ProcessNextNavItemFromList();
                    }
                }
                else
                {
                    listCreated = false;
                    var node = myList[currentNavIndex++];
                    var type = ((IModelListView)((IModelView)node.View)).ModelClass.TypeInfo.Type;
                    if (!type.IsAbstract)
                    {
                        IObjectSpace os1 = Application.CreateObjectSpace();
                        //  Object obj = os1.FindObject(type, new BinaryOperator("Oid", Guid.NewGuid(), BinaryOperatorType.NotEqual));
                        // if (obj == null)
                        Object obj = os1.CreateObject(type);

                        DetailView dv = Application.CreateDetailView(os1, obj);
                        if (dv != null)
                        {
                            dv.ControlsCreated += Listview_ControlsCreated;
                            try
                            {
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                Frame.SetView(dv);
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                            }

                            catch (Exception)
                            {

                            }
                        }
                    }
                    else ProcessNextNavItemFromList();
                }
            }
        }

        private void ProcessNextViewFromList()
        {
            if (currentViewIndex < myViewList.Count)
            {
                if (!listCreated)
                {
                    var node = myViewList[currentViewIndex];

                    if (
                     /* ((node.Caption.IndexOf("alendar") > -1)&&    (node.Caption.IndexOf("run") == -1)
                   &&
                      (node.Caption.IndexOf("Run") == -1)
                   &&
                   (node.Caption.IndexOf("rotocol") == -1)
                  &&
                   (node.Caption.IndexOf("vent") == -1)
                  &&
                   (node.Caption.IndexOf("dash") == -1)
                  &&
                   (node.Caption.IndexOf("alendar") == -1)
                       &&*/
                     (node.ModelClass.TypeInfo.Type.Name.IndexOf("model") == -1)
               &&
                     (node.ModelClass.TypeInfo.IsPersistent == true)
                     )
                    {
                        var listview = Application.CreateListView(node.ModelClass.TypeInfo.Type, true);
                        listview.ControlsCreated += Listview_ControlsCreated;
                        if (listview.Editor.GetType() != typeof(XpandTreeListEditor))
                        {
                            try
                            {
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                Frame.SetView(listview);
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                //    CreateDoc(node, tw);
                                listCreated = true;
                            }
                            catch (Exception)
                            {
                            }
                        }
                        else
                        {
                            listview.ControlsCreated -= Listview_ControlsCreated;
                            currentViewIndex++;
                            ProcessNextViewFromList();
                        }

                    }
                    else
                    {
                        currentViewIndex++;
                        ProcessNextViewFromList();
                    }
                }
                else
                {
                    listCreated = false;
                    var node = myViewList[currentViewIndex++];
                    var type = node.ModelClass.TypeInfo.Type;
                    if (!type.IsAbstract)
                    {

                        if (
                         /*  (node.Caption.IndexOf("run") == -1)
                        &&
                           (node.Caption.IndexOf("Run") == -1)
                        &&
                        (node.Caption.IndexOf("rotocol") == -1)
                       &&
                        (node.Caption.IndexOf("vent") == -1)
                       &&
                        (node.Caption.IndexOf("dash") == -1)
                       &&
                        (node.Caption.IndexOf("alendar") == -1)
                            &&
                         (node.ModelClass.TypeInfo.Type.Name.IndexOf("model") == -1)
                   &&*/
                         (node.ModelClass.TypeInfo.IsPersistent == true)
                         )
                        {
                            IObjectSpace os1 = Application.CreateObjectSpace();
                            //  Object obj = os1.FindObject(type, new BinaryOperator("Oid", Guid.NewGuid(), BinaryOperatorType.NotEqual));
                            // if (obj == null)
                            Object obj = os1.CreateObject(type);

                            DetailView dv = Application.CreateDetailView(os1, obj);
                            if (dv != null)
                            {
                                dv.ControlsCreated += Listview_ControlsCreated;
                                try
                                {
                                    Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                    Frame.SetView(dv);
                                    Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                }

                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                    else ProcessNextViewFromList();
                }
            }
        }

        private void printView(Control control, string id)
        {
            PrintableComponentLink l = new PrintableComponentLink(new PrintingSystem());
            l.Landscape = true;
            l.Component = (IPrintable)control;
            l.CreateDocument();
            l.PrintingSystem.Document.AutoFitToPagesWidth = 1;
             l.ExportToImage(imagespath + id + ".png");
        }

        private void CreateDoc(IModelNavigationItem node, TextWriter tw)
        {
            if (node.View is IModelListView)
            {
                tw.WriteLine(string.Format("- name: {0}", node.Caption));
                tw.WriteLine(string.Format("  href: {0}.md", node.Id));

                /*
                            IObjectSpace os = ObjectSpace;
                                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                                if (doc == null)
                                    doc = os.CreateObject<Doc>();
                                doc.NodeId = "view_" + node.Id;
                                doc.Caption = node.Caption;
                                doc.Order = i;
                                doc.ToolTip = node.ToolTip;
                                doc.Type = DocType.ListView;
                                doc.View = node.View.Id;
                                doc.Save();
                                os.CommitChanges();

                                IModelDetailView myDv = null;
                                if (node.View is IModelListView)
                                    myDv = ((IModelListView)(node.View)).DetailView;
                                if (node.View is IModelDetailView)
                                    myDv = ((IModelDetailView)(node.View));
                                i++;
                                if (myDv != null)
                                {
                                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                                    if (doc1 == null)
                                        doc1 = os.CreateObject<Doc>();
                                    doc1.NodeId = "view_" + myDv.Id;
                                    doc1.Caption = myDv.Caption;

                                    doc1.Order = i;
                                    doc1.Type = DocType.DetailView;
                                    doc1.View = myDv.Id;
                                    doc1.Parent = doc;
                                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                                    doc1.Save();
                                    os.CommitChanges();
                                    SaveViewItems(myDv.Layout[0], doc1, os);
                                }
                                SaveColumns(((IModelListView)(node.View)), doc, os);
                                */
            }
        }
        private void CreateNavMdDoc(IModelView view)
        {
            if (view is IModelListView)
            {
                CreateViewMdDoc(view);
            }
        }

        private void CreateViewMdDoc(IModelView imodelview)
        {
            IModelClass modelClass = ((IModelListView)(imodelview)).ModelClass;

            TextWriter listViewiewDoc = new StreamWriter(String.Format(@"{1}\{0}.md", modelClass.Name.Split('.').Last(),path), false);
            if (imodelview is IModelListView)
            {
                listViewiewDoc.WriteLine(String.Format("### {0}", ((IModelListView)(imodelview)).ModelClass.Caption));
                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine(string.Format("*This page was generated by EarthCape v {2} based on [{0}]({1}) database on {3}.*",db,link,version,date));
                listViewiewDoc.WriteLine("");

                string doc = ((IModelListView)(imodelview)).ModelClass.GetNode("Documentation").GetValue<String>("Description");
                if (!string.IsNullOrEmpty(doc))
                    listViewiewDoc.WriteLine(doc);
                listViewiewDoc.WriteLine("");

                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine(String.Format("[<img src=\"images/{0}.png\" width=\"200px\" height=\"200px\">](images/{0}.png)", ((IModelListView)(imodelview)).Id.Replace("@", ""), ((IModelListView)(imodelview)).Caption));
                listViewiewDoc.WriteLine(String.Format("[<img src=\"images/{0}.png\" width=\"200px\" height=\"200px\">](images/{0}.png)", ((IModelListView)(imodelview)).DetailView.Id.Replace("@", ""), ((IModelListView)(imodelview)).DetailView.Caption));
                listViewiewDoc.WriteLine("");

             
                listViewiewDoc.WriteLine(String.Format("#### All fields", ((IModelListView)(imodelview)).DetailView.Id.Replace("@", ""), ((IModelListView)(imodelview)).DetailView.Caption));
                listViewiewDoc.WriteLine("");

                SaveFields(((IModelListView)(imodelview)).ModelClass, listViewiewDoc);

                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine(String.Format("#### [ListView: {1}](images/{0}.png)", ((IModelListView)(imodelview)).Id.Replace("@", ""), ((IModelListView)(imodelview)).Caption));        
                listViewiewDoc.WriteLine("");

                SaveColumns((IModelListView)(imodelview), listViewiewDoc);

                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine(String.Format("#### [DetailView: {1}](images/{0}.png)", ((IModelListView)(imodelview)).DetailView.Id.Replace("@", ""), ((IModelListView)(imodelview)).DetailView.Caption));
                listViewiewDoc.WriteLine("");
                listViewiewDoc.WriteLine(String.Format("[<img src=\"images/{0}.png\" width=\"100px\" height=\"100px\">](images/{0}.png)", ((IModelListView)(imodelview)).DetailView.Id.Replace("@", ""), ((IModelListView)(imodelview)).DetailView.Caption));
                listViewiewDoc.WriteLine("");
                SaveViewItems(((IModelListView)(imodelview)).DetailView, listViewiewDoc);

                listViewiewDoc.Flush();
                listViewiewDoc.Close();
            }
        }

        private void CreateToc(IModelNavigationItem node)
        {
            if (node.View is IModelListView)
            {
                IModelClass modelClass = ((IModelListView)(node.View)).ModelClass;
                toctw.WriteLine(string.Format("- name: {0}", node.Caption));
                toctw.WriteLine(string.Format("  href: {0}.md", modelClass.Name.Split('.').Last()));
                indextw.WriteLine(string.Format("- [{0}]({1}.md)", node.Caption, modelClass.Name.Split('.').Last()));

                /*
                            IObjectSpace os = ObjectSpace;
                                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                                if (doc == null)
                                    doc = os.CreateObject<Doc>();
                                doc.NodeId = "view_" + node.Id;
                                doc.Caption = node.Caption;
                                doc.Order = i;
                                doc.ToolTip = node.ToolTip;
                                doc.Type = DocType.ListView;
                                doc.View = node.View.Id;
                                doc.Save();
                                os.CommitChanges();

                                IModelDetailView myDv = null;
                                if (node.View is IModelListView)
                                    myDv = ((IModelListView)(node.View)).DetailView;
                                if (node.View is IModelDetailView)
                                    myDv = ((IModelDetailView)(node.View));
                                i++;
                                if (myDv != null)
                                {
                                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                                    if (doc1 == null)
                                        doc1 = os.CreateObject<Doc>();
                                    doc1.NodeId = "view_" + myDv.Id;
                                    doc1.Caption = myDv.Caption;

                                    doc1.Order = i;
                                    doc1.Type = DocType.DetailView;
                                    doc1.View = myDv.Id;
                                    doc1.Parent = doc;
                                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                                    doc1.Save();
                                    os.CommitChanges();
                                    SaveViewItems(myDv.Layout[0], doc1, os);
                                }
                                SaveColumns(((IModelListView)(node.View)), doc, os);
                                */
            }
        }

        private static string getassembly(string s)
        {
            int idx = s.LastIndexOf('.');
            if (idx != -1)
                return s.Substring(0, idx);
            return "";
        }


        private void Listview_ControlsCreated(object sender, EventArgs e)
        {
            DevExpress.ExpressApp.View view = (sender as DevExpress.ExpressApp.View);
            Control viewControl = (Control)view.Control;
            if ((viewControl is SchedulerListEditorPanelControl)||(View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(ModelDifferenceObject))))
            {
                currentViewIndex++;
                simpleAction_DocCurrent.DoExecute();
             }
            else
            {
                viewControl.Tag = view.Id;
                if (View is DevExpress.ExpressApp.ListView)
                {
                    viewControl.Width = 1550;
                    viewControl.Height = 100;
                }
                if (View is DevExpress.ExpressApp.DetailView)
                {
                    viewControl.Width = 1550;
                    viewControl.Height = 1000;
                }
                viewControl.Paint += viewControl_Paint;
            }
        }

        void viewControl_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                ((Control)sender).Paint -= viewControl_Paint;
                Snip1((Control)sender, ((Control)sender).Tag.ToString());

                if (myViewList.Count > 0)
                    ProcessNextViewFromList();
                else
                    ProcessNextNavItemFromList();
            }
            catch (Exception)
            {
           //     throw new UserFriendlyException(ex.Message);
            }
        }
        // P/Invoke declarations
        [DllImport("gdi32.dll")]
        static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int
        wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, CopyPixelOperation rop);
        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr DeleteDC(IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr DeleteObject(IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [DllImport("gdi32.dll")]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
        [DllImport("user32.dll")]
        public static extern IntPtr GetDesktopWindow();
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowDC(IntPtr ptr);

        private void SaveColumns(IModelListView myIModelListView, TextWriter listviewtw)
        {
            IModelColumns list = myIModelListView.Columns;
            list.OrderBy(f => f.Caption);
      
                listviewtw.WriteLine("Visible");
            listviewtw.WriteLine("");
            foreach (IModelColumn col in list)
                if ((col != null) && (col.Index > -1))
                {
                    AddModelMemberMdDoc(listviewtw, col.ModelMember);
                }
            listviewtw.WriteLine("");
            listviewtw.WriteLine("Hidden");
            listviewtw.WriteLine("");
            foreach (IModelColumn col in myIModelListView.Columns)
                if ((col != null) && (col.Index == -1))
                {
                    AddModelMemberMdDoc(listviewtw, col.ModelMember);
                }
        }
        private void SaveFields(IModelClass modelClass, TextWriter listviewtw)
        {
            IModelBOModelClassMembers list = modelClass.OwnMembers;
            list.OrderBy(f => f.Name);
            foreach (IModelMember modelMember in list)
            {
                if (!modelMember.MemberInfo.IsManyToMany)
                    AddModelMemberMdDoc(listviewtw, modelMember);
            }
            listviewtw.WriteLine("");
            listviewtw.WriteLine("Collections");
            listviewtw.WriteLine("");
            foreach (IModelMember modelMember in modelClass.OwnMembers)
            {
                if (modelMember.MemberInfo.IsManyToMany)
                    AddModelMemberMdDoc(listviewtw, modelMember);
            }
        }

        private static void AddModelMemberMdDoc(TextWriter listviewtw, IModelMember modelMember)
        {
         
            string persistentMark = "";
            string tp = "";
            string fieldcoll = "field";

            tp = modelMember.Type.Name.Split('.').Last();


            if (modelMember is IModelMemberPersistent)
                persistentMark = "*USER FIELD*";
            if (modelMember.Type.IsGenericType && modelMember.Type.GetGenericTypeDefinition() == typeof(Nullable<>))
                tp = modelMember.MemberInfo.Owner.Type.GetProperty(modelMember.Name).PropertyType.ToString().Split('.').Last();
            if (modelMember.MemberInfo.IsAssociation)
            {
                tp = modelMember.MemberInfo.AssociatedMemberInfo.Owner.Name.Split('.').Last();
                if (modelMember.MemberInfo.IsManyToMany)
                    fieldcoll = "collection";
            }
            if (modelMember.Type.FullName.IndexOf("EarthCape") > -1)
                listviewtw.WriteLine(String.Format("- *{0}* ({5}: {1}, type: [{2}]({2}.md){4} *{3}*)", modelMember.Caption, modelMember.Name, tp, modelMember.ToolTip, persistentMark, fieldcoll));
            else
                listviewtw.WriteLine(String.Format("- *{0}* ({5}: {1}, type: {2}{4} *{3}*)", modelMember.Caption, modelMember.Name, tp, modelMember.ToolTip, persistentMark, fieldcoll));
            string doc = modelMember.GetNode("Documentation").GetValue<String>("Description");
            if (!string.IsNullOrEmpty(doc))
                listviewtw.WriteLine(String.Format("*{0}*", doc));
        }
    
        private void SaveViewItems(IModelDetailView myIModelDetailView, TextWriter listviewtw)
        {
            foreach (IModelViewItem col in myIModelDetailView.Items)
            {
                if ((col is IModelPropertyEditor))
                {
                    AddViewItem(listviewtw, col);
                }
            }
        }

        private static void AddViewItem(TextWriter listviewtw, IModelViewItem col)
        {
            IModelMember modelMember = ((IModelPropertyEditor)col).ModelMember;
            if (modelMember.Type.FullName.IndexOf("EarthCape") > -1)
                listviewtw.WriteLine(String.Format("- {0}\t(datafield: {1}, type: [{2}]({2}.md)\t{3})", modelMember.Caption, modelMember.Name, modelMember.Type.Name.Split('.').Last(), modelMember.ToolTip));
            else
                listviewtw.WriteLine(String.Format("- {0}\t(datafield: {1}, type: {2}\t{3})", modelMember.Caption, modelMember.Name, modelMember.Type.Name.Split('.').Last(), modelMember.ToolTip));
            string doc = modelMember.GetNode("Documentation").GetValue<String>("Description");
            if (!string.IsNullOrEmpty(doc))
                listviewtw.WriteLine(doc);
        }

        private void Snip1(Control myControl, string filename)
        {

                //Graphics g = myControl.CreateGraphics();
            Bitmap bmp = new Bitmap(myControl.Width, myControl.Height);
            myControl.DrawToBitmap(bmp, new Rectangle(0, 0, myControl.Width, myControl.Height));
            /*     IObjectSpace os = Application.CreateObjectSpace();
                 Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_"+myControl.Tag));
                 if (doc != null)
                 {
                     doc.Screenshot = bmp;
                     doc.Save();
                     os.CommitChanges();
                 }*/
            bmp.Save(imagespath + filename + ".png", ImageFormat.Png);
            bmp.Dispose();
        }

        private void simpleAction_DocCurrent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            path = Path.GetTempPath();
            if (String.IsNullOrEmpty(imagespath))
            imagespath = Path.GetTempPath() + @"\images\";
            if (!Directory.Exists(imagespath))
                Directory.CreateDirectory(imagespath);
            CreateViewMdDoc(View.Model);
            Snip1((Control)View.Control, View.Id);
            IObjectSpace os1 = Application.CreateObjectSpace();
            //  Object obj = os1.FindObject(type, new BinaryOperator("Oid", Guid.NewGuid(), BinaryOperatorType.NotEqual));
            // if (obj == null)

            Object obj = View.CurrentObject;
            if (obj == null) obj=os1.CreateObject(View.ObjectTypeInfo.Type);
            DetailView dv = null;
            try
            {
                dv = Application.CreateDetailView(os1, obj);
            }
            catch (Exception)
            {
          }
             if (dv != null)
            {
                dv.ControlsCreated += Listview_ControlsCreated;
                try
                {
                    Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                    Frame.SetView(dv);
                    Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                }

                catch (Exception)
                {

                }
            }
        }

        private void popupWindowShowAction_DocGeneration_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            DocExportOptions obj = os.CreateObject<DocExportOptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }
    

        private void popupWindowShowAction_DocGeneration_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            DocExportOptions options = ((DocExportOptions)e.PopupWindow.View.CurrentObject);
            path = options.Folder;
            db = options.Database;
            link = options.Link;
            version= System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            prodversion = System.Windows.Forms.Application.ProductVersion;
            date = string.Format("{0}.{1}.{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            Value_WindowController.Instance().Importing = true;
            try
            {

                navItemCount = 0;
                navItemCount++;
                viewCount = 0;
                viewCount++;
                /*     currentIndex = 0;
                     if (myList!=null)
                     myList.Clear();*/
                string toc = String.Format("{0}\\toc.yml", path);
                string index = String.Format("{0}\\index.md", path);
                imagespath = String.Format("{0}\\images\\", path);
                if (!Directory.Exists(imagespath))
                    Directory.CreateDirectory(imagespath);
                this.toctw = new StreamWriter(toc, false);
                this.indextw = new StreamWriter(index, false);
                indextw.WriteLine("#### Application");
                indextw.WriteLine("");

                GenerateNavToc();
                GenerateBOModel();
                ProcessNextViewFromList();
                 //ProcessNextNavItemFromList();


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                this.toctw.Flush();
                this.indextw.Flush();
                toctw.Close();
                indextw.Close();
                Value_WindowController.Instance().Importing = false;
            }
        }
    }
}
