﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using Thought.vCards;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Person_ViewController : ViewController
    {
        public Person_ViewController()
        {
            InitializeComponent();
            TargetObjectType = typeof(PersonCore);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_ImportVCF_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select file";
            dlgOpen.Filter = "VCF|*.vcf";
            dlgOpen.DefaultExt = "vcf";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    vCard vcard = new vCard(file);
                    PersonCore st = View.ObjectSpace.CreateObject<PersonCore>();
                    ((PersonCore)st).FirstName = vcard.GivenName;
                    ((PersonCore)st).LastName = vcard.FamilyName;
                    for (int i = 0; i < vcard.Phones.Count; i++)
                    {
                        if (i == 0)
                            if (!String.IsNullOrEmpty(vcard.Phones[0].FullNumber))
                                ((PersonCore)st).Phone = vcard.Phones[0].FullNumber;
                        if (i == 1)
                            if (!String.IsNullOrEmpty(vcard.Phones[1].FullNumber))
                                ((PersonCore)st).Phone1 = vcard.Phones[1].FullNumber;
                    }
                    for (int i = 0; i < vcard.EmailAddresses.Count; i++)
                    {
                        if (i == 0)
                            if (!String.IsNullOrEmpty(vcard.EmailAddresses[0].Address))
                                ((PersonCore)st).Email = vcard.EmailAddresses[0].Address;
                    }
                    for (int i = 0; i < vcard.DeliveryAddresses.Count; i++)
                    {
                        if (i == 0)
                        {
                            if (!String.IsNullOrEmpty(vcard.DeliveryAddresses[0].Street))
                                ((PersonCore)st).Address = vcard.DeliveryAddresses[0].Street;
                            if (!String.IsNullOrEmpty(vcard.DeliveryAddresses[0].City))
                                ((PersonCore)st).City = vcard.DeliveryAddresses[0].City;
                            if (!String.IsNullOrEmpty(vcard.DeliveryAddresses[0].PostalCode))
                                ((PersonCore)st).ZipCode = vcard.DeliveryAddresses[0].PostalCode;
                            if (!String.IsNullOrEmpty(vcard.DeliveryAddresses[0].Country))
                                ((PersonCore)st).Country = vcard.DeliveryAddresses[0].Country;
                            if (!String.IsNullOrEmpty(vcard.DeliveryAddresses[0].Region))
                                ((PersonCore)st).Province = vcard.DeliveryAddresses[0].Region;
                        }
                    }
                    for (int i = 0; i < vcard.Notes.Count; i++)
                    {
                        if (!String.IsNullOrEmpty(vcard.Notes[0].Text))
                            ((PersonCore)st).Comment = vcard.Notes[0].Text;
                    }
                    if (!String.IsNullOrEmpty(vcard.Organization))
                    {
                        ((PersonCore)st).Affiliation = vcard.Organization;
                    }
             ((PersonCore)st).Save();
                }
                View.ObjectSpace.CommitChanges();
            }

        }

    }
}

