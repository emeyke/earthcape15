﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.Windows.Forms;
using Xpand.Persistent.BaseImpl.ImportExport;
using Xpand.ExpressApp.IO.Core;
using System.IO;
using Xpand.ExpressApp.IO.Controllers;
using System.Xml.Linq;
using DevExpress.Xpo;
using System.Collections;
using DevExpress.ExpressApp.Xpo;
using System.Configuration;
using System.Xml;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ECIOViewController : ViewController
    {
        public ECIOViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_IO_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
           
        }

        private void simpleAction_Import_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog()
            {
                Title = "Select file",
                Filter = "EarthCape XML|*.xml*",
                DefaultExt = "xml",
                CheckFileExists = true,
                ShowReadOnly = true,
                Multiselect = false
            };
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                var importEngine = new ImportEngine();
                Stream stream = new FileStream(dlgOpen.FileName, FileMode.Open, FileAccess.Read);
                Value_WindowController.Instance().Importing = true;
                try
                {
                    importEngine.ImportObjects(stream, info => ObjectSpace);
                    ObjectSpace.CommitChanges();
                }
                finally
                {
                    Value_WindowController.Instance().Importing = false;
                }
            }
        }      
    }
}
