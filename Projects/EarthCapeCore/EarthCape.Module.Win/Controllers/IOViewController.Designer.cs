﻿namespace EarthCape.Module.Win.Controllers
{
    partial class ECIOViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_Import = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_Import
            // 
            this.simpleAction_Import.Caption = "Import from XML";
            this.simpleAction_Import.Category = "Export";
            this.simpleAction_Import.ConfirmationMessage = null;
            this.simpleAction_Import.Id = "simpleAction_Import";
            this.simpleAction_Import.ToolTip = null;
            this.simpleAction_Import.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_Import_Execute);
            // 
            // IOViewController
            // 
            this.Actions.Add(this.simpleAction_Import);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_Import;

    }
}
