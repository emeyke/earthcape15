﻿namespace EarthCape.Module.Win.Controllers
{
    partial class DwCImport_Controller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ImportDwC = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_ImportDwC
            // 
            this.popupWindowShowAction_ImportDwC.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportDwC.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportDwC.Caption = "Import Darwin Core Archive";
            this.popupWindowShowAction_ImportDwC.Category = "Export";
            this.popupWindowShowAction_ImportDwC.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportDwC.Id = "popupWindowShowAction_ImportDwC";
            this.popupWindowShowAction_ImportDwC.ToolTip = null;
            this.popupWindowShowAction_ImportDwC.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportDwC_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportDwC.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportDwC_Execute);
            // 
            // DwCImport_Controller
            // 
            this.Actions.Add(this.popupWindowShowAction_ImportDwC);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportDwC;
    }
}
