﻿namespace EarthCape.Module.Win.Controllers
{
    partial class Documentation_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            this.simpleAction_DocCurrent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_DocGeneration =
                new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_DocCurrent.Caption = "Generate current doc";
            this.simpleAction_DocCurrent.Category = "Tools";
            this.simpleAction_DocCurrent.ConfirmationMessage = null;
            this.simpleAction_DocCurrent.Id = "simpleAction_DocCurrent";
            this.simpleAction_DocCurrent.TargetViewId = "";
            this.simpleAction_DocCurrent.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_DocCurrent.ToolTip = null;
            this.simpleAction_DocCurrent.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_DocCurrent.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_DocCurrent_Execute);
            this.popupWindowShowAction_DocGeneration.AcceptButtonCaption = null;
            this.popupWindowShowAction_DocGeneration.CancelButtonCaption = null;
            this.popupWindowShowAction_DocGeneration.Caption = "Generate docs";
            this.popupWindowShowAction_DocGeneration.Category = "Tools";
            this.popupWindowShowAction_DocGeneration.ConfirmationMessage = null;
            this.popupWindowShowAction_DocGeneration.Id = "popupWindowShowAction_DocGeneration";
            this.popupWindowShowAction_DocGeneration.ToolTip =
                ("Recreates application reference section of EarthCape documentation based on curre" + "" +
                 "nt application model");
            this.popupWindowShowAction_DocGeneration.CustomizePopupWindowParams +=
                new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(
                    this.popupWindowShowAction_DocGeneration_CustomizePopupWindowParams);
            this.popupWindowShowAction_DocGeneration.Execute +=
                new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(
                    this.popupWindowShowAction_DocGeneration_Execute);
            this.Actions.Add(this.simpleAction_DocCurrent);
            this.Actions.Add(this.popupWindowShowAction_DocGeneration);
            this.TypeOfView = typeof(DevExpress.ExpressApp.View);
        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DocCurrent;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_DocGeneration;
    }
}
