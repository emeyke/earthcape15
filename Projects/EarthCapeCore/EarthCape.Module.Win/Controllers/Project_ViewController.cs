using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Collections;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using System.IO;
using System.Windows.Forms;
using Xpand.ExpressApp.IO.Core;
using Xpand.Persistent.BaseImpl.ImportExport;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;

namespace EarthCapeEBFB.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Project_ViewController : ViewController
    {
        public Project_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_CreateAuthorList_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text File|*.txt";
            sfd.FileName = "authors.txt";
            sfd.Title = "Save Text File";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sfd.FileName;
                string path1 = sfd.FileName.Replace(".txt", "_1.txt");
                string path2 = sfd.FileName.Replace(".txt", "_2.txt");

                ArrayList objs = new ArrayList();
                foreach (object item in View.SelectedObjects)
                {
                    objs.Add(os.GetKeyValue(item));

                }
                ArrayList orgs = new ArrayList();
                XPCollection<ProjectContact> coll = new XPCollection<ProjectContact>(((XPObjectSpace)ObjectSpace).Session, new InOperator("Oid", objs));
                coll.Sorting = new SortingCollection(coll, new SortProperty("Order", SortingDirection.Ascending));
                coll.Load();
                //  string path = String.Format("{0}\\Output\\authors.txt", PathHelper.GetApplicationFolder());
                // if (!Directory.Exists(String.Format("{0}\\Output\\", PathHelper.GetApplicationFolder())))
                //    Directory.CreateDirectory(String.Format("{0}\\Output\\", PathHelper.GetApplicationFolder()));
                TextWriter tw = new StreamWriter(path);
                TextWriter tw1 = new StreamWriter(path1);
                TextWriter tw2 = new StreamWriter(path2);
                string deceasedstr = "";
                int indcoll = 0;
                string org1 = "";
                string org2 = "";
                //create org list
                foreach (ProjectContact pc in coll)
                {
                    org1 = pc.Person.Affiliation + ", " + pc.Person.Address + ", " + pc.Person.Country;
                    org2 = pc.Person.Affiliation1;
                    if (!String.IsNullOrEmpty(org1))
                        if (orgs.IndexOf(org1) == -1)
                            orgs.Add(org1);
                    if (!String.IsNullOrEmpty(org2))
                        if (orgs.IndexOf(org2) == -1)
                            orgs.Add(org2);
                    }

                // create names list
                foreach (ProjectContact pc in coll)
                {
                    org1 = pc.Person.Affiliation + ", " + pc.Person.Address + ", " + pc.Person.Country;
                    org2 = pc.Person.Affiliation1;
                    indcoll++;
                    string deceased = "";
                    string comma = ", ";
                    if (pc.Person.Deceased)
                    {
                        deceased = ","+(orgs.Count + 1).ToString(); 
                        if (String.IsNullOrEmpty(deceasedstr))
                            deceasedstr = string.Format("{0} {1} is deceased.", pc.Person.FirstName, pc.Person.LastName);
                        else
                            deceasedstr = string.Format("{0}, and {1} {2} are deceased.", deceasedstr.Replace("and ", "").Replace(" is deceased.", "").Replace(" are deceased", ""), pc.Person.FirstName, pc.Person.LastName);
                    }
                    tw.WriteLine(string.Format("{0}, {1}<sup>{2},{3}{4}</sup>", pc.Person.LastName, pc.Person.FirstName, orgs.IndexOf(org1) + 1, (orgs.IndexOf(org2) > -1) ? Convert.ToString(orgs.IndexOf(org2) + 1) : "", deceased).Replace(",</sup>", "</sup>"));
                    tw1.WriteLine(string.Format("{0} {1}<sup>{2},{3}{4}</sup>", pc.Person.FirstName, pc.Person.LastName, orgs.IndexOf(org1) + 1, (orgs.IndexOf(org2) > -1) ? Convert.ToString(orgs.IndexOf(org2) + 1) : "", deceased).Replace(",</sup>", "</sup>"));
                    if (indcoll == coll.Count() - 1)
                        comma = " & ";
                    tw2.Write(string.Format("{0} {1}<sup>{2},{3}{4}</sup>{5}", pc.Person.FirstName, pc.Person.LastName, orgs.IndexOf(org1) + 1, (orgs.IndexOf(org2) > -1) ? Convert.ToString(orgs.IndexOf(org2) + 1) : "", deceased, comma).Replace(",</sup>", "</sup>").Replace(",,", ","));
                }
                int i = 1; ;
                tw2.WriteLine("");
                tw2.WriteLine("");
                 int indorg = 0;

                foreach (string item in orgs)
                {
                    indorg++;
                    string ind = Convert.ToString(i++);
                    tw.WriteLine(string.Format("{0} {1}", ind, item));
                    tw1.WriteLine(string.Format("{0} {1}", ind, item));
                    if (indorg == orgs.Count)               
                    tw2.Write(string.Format("<sup>{0}</sup>{1}.", ind, item));
                    else
                        tw2.Write(string.Format("<sup>{0}</sup>{1}, ", ind, item));
                }
                tw2.WriteLine("");
                if (!String.IsNullOrEmpty(deceasedstr))
                {
                    tw2.WriteLine(string.Format("<sup>{0}</sup>{1}", orgs.Count + 1, deceasedstr));
                }

                tw.Flush();
                tw.Close();
                tw1.Flush();
                tw1.Close();
                tw2.Flush();
                tw2.Close();
            }
        }

        private void AdvancedExport(PopupWindowShowActionExecuteEventArgs e, FolderBrowserDialog dlgOpen)
        {

        // unfinished, not working, use simple export in the end
             /*   var targetPath = dlgOpen.SelectedPath + "\\InstallerPackage";
            const string installerSource = "C:\\Users\\meykepro5\\OneDrive\\EarthCape\\Users\\EBFB\\EarthCape\\Deploy\\ECN_earthcape.iss";
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);
            var installerBat = targetPath + "//installer.bat";
            List<string> installerBatText = new List<string>();
            List<string> prjNames = new List<string>();
            installerBatText.Add("CD C:\\Program Files (x86)\\Inno Setup 5");
            string[] installerText = File.ReadAllLines(installerSource);


            foreach (Project prj in View.SelectedObjects)
            {
                IList allSelectedObjects = View.SelectedObjects;
                XPCollection<Dataset> coll = new XPCollection<Dataset>(((XPObjectSpace)View.ObjectSpace).Session, new BinaryOperator("Project.Oid", item.Oid));


                var PrjName = GeneralHelper.Clean(prj.Name);//.Substring(0, 5);
                var tempPrjName = PrjName;
                int i = 1;
                while (prjNames.IndexOf(tempPrjName) > -1)
                {
                    tempPrjName = PrjName + i;
                }
                prjNames.Add(tempPrjName);
                PrjName = tempPrjName;



                var path = targetPath + String.Format("\\{0}\\", PrjName);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                foreach (Dataset ds in prj.Datasets)
                {
                    if (((SelectIOConfiguration)e.PopupWindow.View.CurrentObject).IOConfiguration == null) return;
                    SerializationConfigurationGroup group = ObjectSpace.FindObject<SerializationConfigurationGroup>(new BinaryOperator("Oid", ((SelectIOConfiguration)e.PopupWindow.View.CurrentObject).IOConfiguration.Oid));
                    ExportEngine engine = new ExportEngine(ObjectSpace);

                    MemoryStream ms = new MemoryStream();
                    XmlWriterSettings xws = new XmlWriterSettings();
                    xws.OmitXmlDeclaration = true;
                    xws.Indent = true;

         

                    using (XmlWriter xw = XmlWriter.Create(ms, xws))
                    {
                        XDocument doc = engine.Export(coll, group);
                        var dsName = GeneralHelper.Clean(ds.Name).Replace(" ", "");//.Substring(0, 5);
                        var tempdsName = PrjName;
                        int i = 1;
                        while (prjNames.IndexOf(tempPrjName) > -1)
                        {
                            tempPrjName = PrjName + i;
                        }
                        prjNames.Add(tempPrjName);
                        PrjName = tempPrjName;
                        var xmlPath = string.Format("{0}\\{1}.xml", targetPath, PrjName);
                        FileStream file = File.Create(xmlPath);
                        doc.Save(file);
                        file.Close();
                    }

                    var installerPath = path + "EarthCape.iss";

                    installerBatText.Add(String.Format("ISCC {0} /Q[p]", installerPath));

                    List<string> installerText1 = new List<string>();
                  foreach (string line in installerText)
                    {
                        string newLine = line.Replace("{EarthcapeInitialDataFile}", xmlPath);
                        newLine = newLine.Replace("{EarthcapeOutputDir}", targetPath);
                        newLine = newLine.Replace("{EarthcapeFileEnding}", PrjName);
                        installerText1.Add(newLine);
                    }

                    File.WriteAllLines(installerPath, installerText1.ToArray()); 

}

            }
            //write to installer bat
            File.WriteAllLines(installerBat, installerBatText.ToArray());
            //run installer bat
            Process.Start(installerBat);*/
        }
        private void SimpleExport(PopupWindowShowActionExecuteEventArgs e, FolderBrowserDialog dlgOpen)
        {
            var targetPath = dlgOpen.SelectedPath;
            //  const string installerSource = "C:\\Repos\\EarthCape15\\Projects\\EarthCapeEBFB\\EarthCapeCN_users.iss";
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);
            List<string> dsNames = new List<string>();
            foreach (Dataset item in View.SelectedObjects)
            {
                if (((SelectIOConfiguration)e.PopupWindow.View.CurrentObject).IOConfiguration == null) return;
                SerializationConfigurationGroup group = ObjectSpace.FindObject<SerializationConfigurationGroup>(new BinaryOperator("Oid", ((SelectIOConfiguration)e.PopupWindow.View.CurrentObject).IOConfiguration.Oid));
                ExportEngine engine = new ExportEngine(ObjectSpace);

                MemoryStream ms = new MemoryStream();
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                xws.Indent = true;
                using (XmlWriter xw = XmlWriter.Create(ms, xws))
                {
                    XPCollection<Dataset> coll = new XPCollection<Dataset>(((XPObjectSpace)View.ObjectSpace).Session, new BinaryOperator("Oid", item.Oid));
                    XDocument doc = engine.Export(coll, group);
                    var DsName = GeneralHelper.Clean(item.Name).Replace(" ", "");//.Substring(0, 5);
                    var tempDsName = DsName;
                    int i = 1;
                    while (dsNames.IndexOf(tempDsName) > -1)
                    {
                        tempDsName = DsName + i;
                    }
                    dsNames.Add(tempDsName);
                    DsName = tempDsName;
                    string prjCode = "";
                    if (item.Project != null)
                        if ((!String.IsNullOrEmpty(item.Project.Code)))
                        {
                            prjCode = item.Project.Code + "\\";
                            if (!Directory.Exists(string.Format("{0}\\{1}", targetPath, prjCode)))
                                Directory.CreateDirectory(string.Format("{0}\\{1}", targetPath, prjCode));
                        }
                    var xmlPath = string.Format("{0}\\{1}{2}.xml", targetPath,prjCode, DsName);
                    FileStream file = File.Create(xmlPath);
                    doc.Save(file);
                    file.Close();
                }
            }
        }
        private void popupWindowShowAction_ExportProjectToXML_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            FolderBrowserDialog dlgOpen = new FolderBrowserDialog();
            if (dlgOpen.ShowDialog() == DialogResult.OK)
               //AdvancedExport(e, dlgOpen);
               SimpleExport(e, dlgOpen);
         
        }

 
 
        private void popupWindowShowAction_ExportProjectToXML_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            SelectIOConfiguration options = os.CreateObject<SelectIOConfiguration>();
            e.View = Application.CreateDetailView(os, options);
          
        }
    }
}
