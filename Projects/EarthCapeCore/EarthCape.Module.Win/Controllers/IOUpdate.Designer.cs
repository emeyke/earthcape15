﻿using Xpand.Persistent.Base.ImportExport;
namespace EarthCapePro.Module.Win
{
    partial class IOUpdateController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_UpdateCalculations = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_UpdateCalculations
            // 
            this.simpleAction_UpdateCalculations.Caption = "Update Configuration";
            this.simpleAction_UpdateCalculations.ConfirmationMessage = null;
            this.simpleAction_UpdateCalculations.Id = "simpleAction_UpdateConfiguration";
            this.simpleAction_UpdateCalculations.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateCalculations.ToolTip = null;
            this.simpleAction_UpdateCalculations.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateProportions_Execute);
            // 
            // IOUpdateController
            // 
            this.Actions.Add(this.simpleAction_UpdateCalculations);
            this.TargetObjectType = typeof(Xpand.Persistent.Base.ImportExport.ISerializationConfiguration);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateCalculations;
    }
}
