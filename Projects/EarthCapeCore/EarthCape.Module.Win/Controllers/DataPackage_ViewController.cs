﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using EarthCape.Module.GBIF;
using EarthCape.Module.Lab;
using EarthCape.Zenodo;
using ListView = DevExpress.ExpressApp.ListView;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DataPackage_ViewController : ViewController
    {
        public DataPackage_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
          //  this.TargetViewType = ViewType.ListView;
            //this.TargetObjectType = typeof(Unit);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        private void popupWindowShowAction_ExportDataPackage_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            //((DataPackageOptions)e.PopupWindowViewCurrentObject).Save();
            if (!Directory.Exists("Output"))
                Directory.CreateDirectory("Output");
            DataPackageOptions options = (DataPackageOptions)e.PopupWindowViewCurrentObject;
           // e.PopupWindowView.ObjectSpace.CommitChanges();
           // DataPackage pc = View.ObjectSpace.FindObject<DataPackage>(new BinaryOperator("Oid", ((DataPackage)e.PopupWindowViewCurrentObject).Oid));
            int exported = 0;
            int empty = 0;
            int failed = 0;
            //CriteriaOperator cr = ((ListView)View).Model.Filter;
        /*    IModelListView unitsview = (IModelListView)Application.Model.Views["DataPackage_Unit_ListView"];
            IModelListView taxaview = (IModelListView)Application.Model.Views["DataPackage_TaxonomicName_ListView"];
            IModelListView locsview = (IModelListView)Application.Model.Views["DataPackage_Locality_ListView"];
            IModelListView visitsview = (IModelListView)Application.Model.Views["DataPackage_LocalityVisit_ListView"];
            IModelListView dsview = (IModelListView)Application.Model.Views["DataPackage_Dataset_ListView"];
            IModelListView prview = (IModelListView)Application.Model.Views["DataPackage_Project_ListView"];
            IModelListView prperson = (IModelListView)Application.Model.Views["DataPackage_PersonCore_ListView"];*/
            Settings settings = View.ObjectSpace.FindObject<Settings>(View.ObjectSpace.ParseCriteria("1=1"));
            PublishHelper.CreateDataPackage(Application,options,settings, crUnits, View.ObjectSpace, ref exported, ref empty, ref failed);
            View.ObjectSpace.CommitChanges();
        }
        CriteriaOperator crUnits = null;
        CriteriaOperator crTaxa = null;
        CriteriaOperator crLocs = null;
        CriteriaOperator crDatasets = null;
        CriteriaOperator crProjects = null;
        CriteriaOperator crPerson = null;

        private void popupWindowShowAction_ExportDataPackage_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            View.SaveModel();
            IObjectSpace os = Application.CreateObjectSpace();
            DataPackageOptions obj = os.CreateObject<DataPackageOptions>();
            obj.CoreViews = View.Id;
           // obj.Name = String.Format("units-{0}", DateTime.Now.ToString("yyyy'-'MM'-'dd'-'HH'-'mm"));
            //View.SaveModel();
            List<Guid> unitslist = new List<Guid>();
             var units=new List<Unit>();
            if ((((ListView)View).SelectedObjects.Count == ((ListView)View).CollectionSource.GetCount())
                ||
                (((ListView)View).SelectedObjects.Count == 1))
            {
                crUnits = os.ParseCriteria(((ListView)View).Model.Filter);
                
            }
            else
            {
                foreach (Unit item in ((ListView)View).SelectedObjects)
                {
                    unitslist.Add(item.Oid);
                }
                crUnits = new InOperator("Oid", unitslist);



               
            }
          //  obj.Save();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }

        private void popupWindowShowAction_ExportBOLDSpecimenSheet_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            View.SaveModel();
            IObjectSpace os = Application.CreateObjectSpace();
            DataPackage obj = os.CreateObject<DataPackage>();
            obj.Name = String.Format("units-{0}", DateTime.Now.ToString("yyyy'-'MM'-'dd'-'HH'-'mm"));
            View.SaveModel();
            List<Guid> unitslist = new List<Guid>();
            List<Guid> loclist = new List<Guid>();
            List<Guid> dslist = new List<Guid>();
            List<Guid> tlist = new List<Guid>();
            List<Guid> clist = new List<Guid>();
            List<Guid> prlist = new List<Guid>();
            if ((((ListView)View).SelectedObjects.Count == ((ListView)View).CollectionSource.GetCount()))
            {
                crUnits = os.ParseCriteria(((ListView)View).Model.Filter);
            }
            else
            {
                foreach (DnaExtract item in ((ListView)View).SelectedObjects)
                {
                    unitslist.Add(item.Oid);
                }
                crUnits = new InOperator("Oid", unitslist);
            }
            if (!ReferenceEquals(crUnits, null))
                obj.FilterUnits = crUnits.ToString();
            if (!ReferenceEquals(crTaxa, null))
                obj.FilterTaxa = crTaxa.ToString();
            if (!ReferenceEquals(crLocs, null))
                obj.FilterLocalities = crLocs.ToString();
            if (!ReferenceEquals(crDatasets, null))
                obj.FilterDatasets = crDatasets.ToString();
            if (!ReferenceEquals(crProjects, null))
                obj.FilterProjects = crProjects.ToString();
            if (!ReferenceEquals(crPerson, null))
                obj.FilterPeople = crPerson.ToString();

            foreach (DnaExtract item in View.SelectedObjects)
            {
                if (item.Tissue != null)
                {
                    if (item.Tissue.Locality != null)
                        if (loclist.IndexOf(item.Tissue.Locality.Oid) == -1)
                            loclist.Add(item.Tissue.Locality.Oid);
                    if (item.Tissue.Dataset != null)
                    {
                        if (dslist.IndexOf(item.Tissue.Dataset.Oid) == -1)
                        {
                            dslist.Add(item.Tissue.Dataset.Oid);
                            if (item.Tissue.Dataset.Project != null)
                                if (prlist.IndexOf(item.Tissue.Dataset.Project.Oid) == -1)
                                {
                                    prlist.Add(item.Tissue.Dataset.Project.Oid);
                                }
                        }
                    }
                    if (item.Tissue.TaxonomicName != null)
                        if (tlist.IndexOf(item.Tissue.TaxonomicName.Oid) == -1)
                            tlist.Add(item.Tissue.TaxonomicName.Oid);
                }
            }

            foreach (Guid g in dslist)
            {
                DatasetUsage du = os.CreateObject<DatasetUsage>();
                du.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", g));
                obj.DatasetsUsed.Add(du);
            }

            obj.Save();

            foreach (DatasetUsage du in obj.DatasetsUsed)
            {
                foreach (DatasetContribution dc in du.Dataset.Contributions)
                {
                    if (dc.Person != null)
                        if (clist.IndexOf(dc.Person.Oid) == -1)
                            clist.Add(dc.Person.Oid);
                }
            }
            foreach (Guid g in clist)
            {
                ProjectContact du = os.CreateObject<ProjectContact>();
                du.Person = os.FindObject<PersonCore>(new BinaryOperator("Oid", g));
                obj.Contacts.Add(du);
            }
            crTaxa = new InOperator("Oid", tlist);
            crLocs = new InOperator("Oid", loclist);
            crDatasets = new InOperator("Oid", dslist);
            crProjects = new InOperator("Oid", prlist);
            crPerson = new InOperator("Oid", clist);
            obj.Save();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }

        private void popupWindowShowAction_ExportBOLDSpecimenSheet_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ((DataPackage)e.PopupWindowViewCurrentObject).Save();
            if (!Directory.Exists("Output"))
                Directory.CreateDirectory("Output");
            //DataPackageOptions options = (DataPackageOptions)e.PopupWindowViewCurrentObject;
            e.PopupWindowView.ObjectSpace.CommitChanges();
            DataPackage pc = View.ObjectSpace.FindObject<DataPackage>(new BinaryOperator("Oid", ((DataPackage)e.PopupWindowViewCurrentObject).Oid));
            int exported = 0;
            int empty = 0;
            int failed = 0;
            //CriteriaOperator cr = ((ListView)View).Model.Filter;
            IModelListView unitsview_v = (IModelListView)Application.Model.Views["DataPackage_BOLD_Voucher_DNAExtract_ListView"];
            IModelListView unitsview_t = (IModelListView)Application.Model.Views["DataPackage_BOLD_Taxonomy_DNAExtract_ListView"];
            IModelListView unitsview_s = (IModelListView)Application.Model.Views["DataPackage_BOLD_Specimen_DNAExtract_ListView"];
            IModelListView unitsview_c = (IModelListView)Application.Model.Views["DataPackage_BOLD_Collection_DNAExtract_ListView"];
            IModelListView taxaview = (IModelListView)Application.Model.Views["DataPackage_TaxonomicName_ListView"];
            IModelListView locsview = (IModelListView)Application.Model.Views["DataPackage_Locality_ListView"];
            IModelListView dsview = (IModelListView)Application.Model.Views["DataPackage_Dataset_ListView"];
            IModelListView prview = (IModelListView)Application.Model.Views["DataPackage_Project_ListView"];
            IModelListView prperson = (IModelListView)Application.Model.Views["DataPackage_PersonCore_ListView"];
            Settings settings = View.ObjectSpace.FindObject<Settings>(View.ObjectSpace.ParseCriteria("1=1"));
            PublishHelper.CreateDataPackageBOLD(String.Format("Output/datapackage-{0}.zip", pc.Oid),settings, pc, unitsview_v, unitsview_t, unitsview_s, unitsview_c, taxaview, locsview, dsview, prview, prperson, crUnits, crTaxa, crLocs, crDatasets, crProjects, crPerson, View.ObjectSpace, ref exported, ref empty, ref failed, true, true);
            View.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_PublishSpecimen_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            PublishSpecimenOptions options = (PublishSpecimenOptions)e.PopupWindowViewCurrentObject;
            if (!Directory.Exists("Output"))
                Directory.CreateDirectory("Output");
            Settings settings = View.ObjectSpace.FindObject<Settings>(View.ObjectSpace.ParseCriteria("1=1"));
            IModelListView prattachments = (IModelListView)Application.Model.Views["Unit_Attachments_ListView"];
            IModelListView unitv = (IModelListView)Application.Model.Views["Unit_ListView_DigitalSpecimen"];

            HttpClient httpClient = new HttpClient();
            ZenodoClient cl = new ZenodoClient(httpClient);
            cl.BaseUrl = true ? PublishHelper.zenodoSandboxApi : PublishHelper.zenodoApi;
            string baseurl = true ? PublishHelper.zenodoSandboxUrl : PublishHelper.zenodoUrl;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", true ? settings.ZenodoSandboxToken : settings.ZenodoToken);
            foreach (Unit unit in View.SelectedObjects)
            {
                string packagepath = String.Format("Output/specimen-{0}.zip", unit.Oid);
               // PublishHelper.CreateSpecimenPackage(packagepath, ((Unit)View.CurrentObject), prattachments, View.ObjectSpace, ref exported, ref empty, ref failed, true, true); ;
                PublishHelper.CreateZenodoSpecimenJson(unit, unitv,View.ObjectSpace,settings);
                PublishHelper.ZenodoUploadUnitSpecimen(cl, packagepath, unit, settings, options);
                View.ObjectSpace.CommitChanges();
            }
        }

        private void popupWindowShowAction_PublishSpecimen_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            var obj = os.CreateObject<PublishSpecimenOptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }
    }
}
