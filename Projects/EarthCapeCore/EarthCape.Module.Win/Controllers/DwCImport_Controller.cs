﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.GBIF;
using EarthCape.Module.Integrations.GBIF.EML;
using EarthCape.Module.Integrations.GBIF.Meta;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DwCImport_Controller : ViewController
    {
        public DwCImport_Controller()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_ImportDwC_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ImportDwcOptions options = os.CreateObject<ImportDwcOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;

        }

        private void popupWindowShowAction_ImportDwC_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportDwcOptions options = (ImportDwcOptions)e.PopupWindowViewCurrentObject;
            string pathToZipArchive = options.DwcArchive.FullName;
            string pathToExtract = Path.GetTempPath() + "\\" + Path.GetFileNameWithoutExtension(pathToZipArchive);
            if (!Directory.Exists(pathToExtract))
                Directory.CreateDirectory(pathToExtract);
            ((XPObjectSpace)View.ObjectSpace).Session.TrackPropertiesModifications = false;
            using (var fileStream = System.IO.File.Open(pathToZipArchive, System.IO.FileMode.Open))
            {
                var parentArchive = new ZipArchive(fileStream);
                var meta = parentArchive.GetEntry("meta.xml");
                XmlSerializer metaSerialize = new XmlSerializer(typeof(Archive));
                Archive Meta = (Archive)metaSerialize.Deserialize(new StringReader(new StreamReader(meta.Open()).ReadToEnd()));

                var entry = parentArchive.GetEntry(Meta.Core.Files.Location);
                var ext = parentArchive.GetEntry(Meta.Extension[0].Files.Location);

                //var eml1 = parentArchive.GetEntry(Meta.Metadata);

                //   XmlSerializer xmlSerialize = new XmlSerializer(typeof(Eml));
                /*     XmlSerializer xmlSerialize = new XmlSerializer(typeof(Integrations.GBIF.EML.Dataset), new XmlRootAttribute("dataset"));
                     StreamReader streamReader = new StreamReader(eml1.Open());
                     StringReader stringReader = new StringReader(streamReader.ReadToEnd());
                     Integrations.GBIF.EML.Dataset EML = (Integrations.GBIF.EML.Dataset)xmlSerialize.Deserialize(stringReader);*/

                if (entry != null)
                    if (options.Occurrence)
                        ImportDwcOccurrence(entry.Open(), null, options.MatchUnitID, options.SkipFirst);
                foreach (Extension item in Meta.Extension)
                {


                    if (item.Files != null)
                    {
                        Stream stream = parentArchive.GetEntry(item.Files.Location).Open();
                        if (item.RowType == @"http://rs.gbif.org/terms/1.0/Multimedia")
                            if (options.Multimedia)
                                ImportDwcMultimedia(stream, null, options.MatchUnitID, options.SkipFirst);
                        if (item.RowType == @"http://rs.tdwg.org/dwc/terms/Identification")
                            if (options.Identification)
                                ImportDwcIdentifications(stream, null, options.MatchUnitID, options.SkipFirst);
                    }
                }
            }
            ((XPObjectSpace)View.ObjectSpace).Session.TrackPropertiesModifications = true;
        }

        private void ImportDwcMultimedia(Stream stream, Integrations.GBIF.EML.Dataset dataset, bool match, long skip)
        {

            UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

            StreamReader csvreader = new StreamReader(stream);
            string inputLine = "";
            inputLine = csvreader.ReadLine();
            string[] headers = inputLine.Split('\t');
            if (headers.Length == 1) headers = Regex.Split(inputLine, ",(?=(?:[^']*'[^']*')*[^']*$)"); //inputLine.Split(','); 
            int i = 0;
            for (long j = 0; j < skip; j++)
            {
                csvreader.ReadLine();
            }
            while ((inputLine = csvreader.ReadLine()) != null)
            {
                i++;
                inputLine = Regex.Replace(inputLine, "(\\{.*\\})", "");
                string[] csvArray = inputLine.Split('\t');
                Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                if (csvArray.Length == 1) csvArray = CSVParser.Split(inputLine);
                if (headers.Length == csvArray.Length)
                {

                    UnitAttachment att = null;
                    if (att == null)
                    {
                        att = new UnitAttachment(uow);
                    }
                    if (Array.IndexOf(headers, "id") > -1)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "id")]))
                        {
                            string idval = csvArray[Array.IndexOf(headers, "id")];
                            int pos = idval.LastIndexOf("/") + 1;
                            string id = idval.Substring(pos, idval.Length - pos);
                            //  Unit unit = uow.FindObject<Unit>(new BinaryOperator("UnitID", id));
                            // att.Unit = unit;
                            att.Comment = id;
                            if (Array.IndexOf(headers, "identifier") > -1)
                                if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "identifier")]))
                                {
                                    att.JpegUrl = csvArray[Array.IndexOf(headers, "identifier")];
                                }
                        }
                    att.Save();
                    if (i == 1000) {
                        uow.CommitChanges();
                        uow.Dispose();
                        uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

                        i = 0;
                    }
                }
            }



        }

        private void ImportDwcIdentifications(Stream stream,  Integrations.GBIF.EML.Dataset dataset, bool match, long skip)
        {
            UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

            StreamReader csvreader = new StreamReader(stream);
            string inputLine = "";
            inputLine = csvreader.ReadLine();
            string[] headers = inputLine.Split('\t');
            if (headers.Length == 1) headers = Regex.Split(inputLine, ",(?=(?:[^']*'[^']*')*[^']*$)"); //inputLine.Split(','); 
            int i = 0;
            for (long j = 0; j < skip; j++)
            {
                csvreader.ReadLine();
            }
            while ((inputLine = csvreader.ReadLine()) != null)
            {
                i++;
                inputLine = Regex.Replace(inputLine, "(\\{.*\\})", "");
                string[] csvArray = inputLine.Split('\t');
                Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                if (csvArray.Length == 1) csvArray = CSVParser.Split(inputLine);
                if (headers.Length == csvArray.Length)
                {

                    UnitIdentification att = null;
                    if (att == null)
                    {
                        att = new UnitIdentification(uow);
                    }
                    if (Array.IndexOf(headers, "id") > -1)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "id")]))
                        {
                            string idval = csvArray[Array.IndexOf(headers, "id")];
                            int pos = idval.LastIndexOf("/") + 1;
                            string id = idval.Substring(pos, idval.Length - pos);
                            // Unit unit = uow.FindObject<Unit>(new BinaryOperator("UnitID", id));
                            //att.Unit = unit;
                            att.IdentificationRemarks = id;
                        }
                    if (Array.IndexOf(headers, "identifiedBy") > -1)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "identifiedBy")]))
                            att.IdentifiedBy = csvArray[Array.IndexOf(headers, "identifiedBy")];
                    if (Array.IndexOf(headers, "scientificName") > -1)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "scientificName")]))
                            att.NameText = csvArray[Array.IndexOf(headers, "scientificName")];
                    if (Array.IndexOf(headers, "dateIdentified") > -1)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "dateIdentified")]))
                            att.DateVerbatim = csvArray[Array.IndexOf(headers, "dateIdentified")];
                    att.Save();
                    if (i == 1000)
                    {
                        uow.CommitChanges();
                        uow.Dispose();
                        uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);
                        i = 0;
                    }
                }
            }



        }

        private void ImportDwcOccurrence(Stream stream, Integrations.GBIF.EML.Dataset dataset,bool match, long skip)
        {
            UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);
            StreamReader csvreader = new StreamReader(stream);
            string inputLine = "";
            inputLine = csvreader.ReadLine();
            string[] headers = inputLine.Split('\t');
            inputLine = inputLine.Replace("\"", "");
            if (headers.Length == 1) headers = Regex.Split(inputLine, ",(?=(?:[^']*'[^']*')*[^']*$)"); //inputLine.Split(','); 
            Core.Dataset ds = null;
            int i = 0;
            for (long j = 0; j < skip; j++)
            {
                csvreader.ReadLine();
            }
            while ((inputLine = csvreader.ReadLine()) != null)
            {
                i++;
                inputLine = Regex.Replace(inputLine, "(\\{.*\\})", "");
                inputLine = inputLine.Replace("\"","");
                string[] csvArray = inputLine.Split('\t');
                Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                if (csvArray.Length==1) csvArray = CSVParser.Split(inputLine);
                if (headers.Length == csvArray.Length)
                {
                    if (dataset != null)

                    {
                        if (ds == null)
                            if (Array.IndexOf(headers, "datasetName") > 0)
                            {
                                string dsname = csvArray[Array.IndexOf(headers, "datasetName")];
                                if (!String.IsNullOrEmpty(dsname))
                                {
                                    if (dsname.Length > 100) dsname = dsname.Substring(0, 95);
                                    ds = GeneralHelper.GetDataset(null, uow, dsname, "");
                                }
                            }
                        if (ds == null)
                        {
                            //  string dsname = EML.Dataset.Title;
                            string dsname = dataset.Title;
                            if (!String.IsNullOrEmpty(dsname))
                            {
                                if (dsname.Length > 100) dsname = dsname.Substring(0, 95);
                            }
                            ds = GeneralHelper.GetDataset(null, uow, dsname, "");
                        }
                        //  ds.Description = EML.Dataset.Abstract.Para;
                        if (ds != null)
                            ds.Description = dataset.Abstract.Para.ToString();
                    }
                    Unit unit = null;
                    //       if (csvArray[Array.IndexOf(headers, "catalogNumber")]== "P00039249")
                    if (match)
                        if (Array.IndexOf(headers, "catalogNumber") > 0)
                            if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "catalogNumber")]))
                                unit = uow.FindObject<Unit>(new BinaryOperator("UnitID", csvArray[Array.IndexOf(headers, "catalogNumber")]));
                    if (unit == null)
                    {
                        unit = new Unit(uow);
                        if (Array.IndexOf(headers, "catalogNumber") > 0)
                            if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "catalogNumber")]))
                                unit.UnitID = csvArray[Array.IndexOf(headers, "catalogNumber")];
                        // unit.GbifKey = Convert.ToInt64(csvArray[Array.IndexOf(headers, "occurrenceID")]);
                        if (Array.IndexOf(headers, "occurrenceID") > 0)
                            if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "occurrenceID")]))
                                unit.GbifOccurrenceID = csvArray[Array.IndexOf(headers, "occurrenceID")];
                    }
                    if (Array.IndexOf(headers, "taxonKey") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "taxonKey")]))
                        {
                            string taxonkey = csvArray[Array.IndexOf(headers, "taxonKey")];
                            if (!String.IsNullOrEmpty(taxonkey))
                            {
                                //   unit.TaxonomicName = PublishHelper.GBIFTaxonomyDownloadByKey(taxonkey, null, objectSpace);
                            }
                        }
                    unit.TemporaryName = csvArray[Array.IndexOf(headers, "scientificName")].Length > 100 ? csvArray[Array.IndexOf(headers, "scientificName")].Substring(0, 99) : csvArray[Array.IndexOf(headers, "scientificName")];
                    if (ds != null)
                        unit.Dataset = ds;
                    if (Array.IndexOf(headers, "gbifID") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "gbifID")]))
                            unit.GbifKey = Convert.ToInt64(csvArray[Array.IndexOf(headers, "gbifID")]);
                    if (Array.IndexOf(headers, "specificEpithet") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "specificEpithet")]))
                            unit.SpecificEpithet = csvArray[Array.IndexOf(headers, "specificEpithet")];
                    if (Array.IndexOf(headers, "infraspecificEpithet") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "infraspecificEpithet")]))
                            unit.InfraspecificEpithet = csvArray[Array.IndexOf(headers, "infraspecificEpithet")];
                    if (Array.IndexOf(headers, "genus") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "genus")]))
                            unit.Genus = csvArray[Array.IndexOf(headers, "genus")];
                    if (Array.IndexOf(headers, "family") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "family")]))
                            unit.Family = csvArray[Array.IndexOf(headers, "family")];
                    if (Array.IndexOf(headers, "order") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "order")]))
                            unit.Order = csvArray[Array.IndexOf(headers, "order")];
                    if (Array.IndexOf(headers, "class") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "class")]))
                            unit.Class = csvArray[Array.IndexOf(headers, "class")];
                    if (Array.IndexOf(headers, "phylum") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "phylum")]))
                            unit.Phylum = csvArray[Array.IndexOf(headers, "phylum")];
                    if (Array.IndexOf(headers, "kingdom") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "kingdom")]))
                            unit.Kingdom = csvArray[Array.IndexOf(headers, "kingdom")];
                    if (Array.IndexOf(headers, "verbatimLocality") > 0)
                        unit.PlaceDetails = csvArray[Array.IndexOf(headers, "verbatimLocality")];
                    if (Array.IndexOf(headers, "eventDate") > 0)
                    {
                        string date = csvArray[Array.IndexOf(headers, "eventDate")];
                        if (!String.IsNullOrEmpty(date))
                        {
                            unit.DateVerbatim = date;
                            try
                            {
                                unit.Date = Convert.ToDateTime(date);
                                if (unit.Date.Value.Year <= 1753) { unit.Date = null; unit.Day = null; unit.Month = null; unit.Year = null; }
                            }
                            catch (Exception)
                            {
                                //       unit.DateVerbatim = date;
                            }
                        }
                    }
                    if (Array.IndexOf(headers, "country") > 0)
                        unit.Country = csvArray[Array.IndexOf(headers, "country")];
                    //todo  unit.BasisOfRecord =(BasisOfRecord)System.Enum.Parse(typeof(BasisOfRecord), item.basisOfRecord);
                    if (Array.IndexOf(headers, "collectionCode") > 0)
                        unit.CollectionCode = csvArray[Array.IndexOf(headers, "collectionCode")];
                    if (Array.IndexOf(headers, "decimalLatitude") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "decimalLatitude")]))
                            unit.Latitude = Convert.ToDouble(csvArray[Array.IndexOf(headers, "decimalLatitude")]);
                    if (Array.IndexOf(headers, "decimalLongitude") > 0)
                        if (!String.IsNullOrEmpty(csvArray[Array.IndexOf(headers, "decimalLongitude")]))
                            unit.Longitude = Convert.ToDouble(csvArray[Array.IndexOf(headers, "decimalLongitude")]);
                    //     unit.EPSG = item.geodeticDatum;
                    unit.Save();
                    if (i == 1000)
                    {
                        uow.CommitChanges();
                        uow.Dispose();
                        uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer);

                        i = 0;
                    }
                }
            }



        }
    }
}
