using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.XtraMap;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.Drawing;
using System.Collections;
using EarthCape.Module.Core;

//https://www.devexpress.com/Support/Center/Question/Details/T248845/when-list-is-refreshed-via-separate-objectspace-map-layer-bound-to-same-list-does-not

namespace EarthCape.Module.Win.Controllers
{
    public partial class MapControl_ViewController : ViewController
    {
        public MapControl_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        //    this.TargetObjectType = typeof(IMapsMarker);
            this.TargetViewType = ViewType.DetailView;
       }
        protected override void OnActivated()
        {
            base.OnActivated();
            MapControlDetailViewItem item = ((DetailView)View).FindItem("MapControl") as MapControlDetailViewItem;
            if (item != null)
            {
                if (item.Control != null)
                {
                    Item_ControlCreated(item, EventArgs.Empty);
                }
                item.ControlCreated += Item_ControlCreated;
            }
        }


        private void Item_ControlCreated(object sender, EventArgs e)
        {
            if (((MapControlDetailViewItem)sender).Control != null)
            {
                map = ((MapControlDetailViewItem)sender).Control as MapControl;
                if (map != null)
                    InitialiseMap(map);
            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        MapControl map = null;
        private void InitialiseMap(MapControl map)
        {

            // bind to the Stores list as in https://www.devexpress.com/Support/Center/Question/Details/T174024 

            ListPropertyEditor editor = ((ListPropertyEditor)(((DetailView)View).FindItem("Units")));
            if (editor != null)
            {
                editor.ControlCreated += (s, e) =>
                {
                    CollectionSourceBase cs = ((ListPropertyEditor)s).ListView.CollectionSource;
                   // cs.SetCriteria("map", ((ListPropertyEditor)s).ListView.Model.Filter);
                    if (View.ObjectTypeInfo.Type.IsAssignableFrom(typeof(Dataset)))
                    {
                        //cs =new CollectionSource(Application.CreateObjectSpace(), typeof(Unit));
                        // cs.Criteria["Map"] = CriteriaOperator.And("[Latitude] <> 0.0 And [Latitude] Is Not Null And [EPSG] = 4329");
                    }
                    map.Layers.Add(CreateVectorDotsLayer<IMapsMarker>(cs, "units"));
                };
                editor.CreateControl();
            }
        }

        

        private VectorItemsLayer CreateVectorDotsLayer<T>(CollectionSourceBase collectionSource,string name)
        {
            // Create a vector items layer.
            VectorItemsLayer itemsLayer = new VectorItemsLayer();
            itemsLayer.Name = Name;
            itemsLayer.ItemStyle.StrokeWidth = 5;
            itemsLayer.ItemStyle.Stroke = Color.Red;

            ListSourceDataAdapter adapter = new ListSourceDataAdapter();
            adapter.DataSource = collectionSource.Collection;   // locations;
            adapter.DefaultMapItemType = MapItemType.Dot;
            adapter.Mappings.Latitude = "Latitude";
            adapter.Mappings.Longitude = "Longitude";
            //  adapter.Mappings.Text = "Name";

            itemsLayer.Data = adapter;

            collectionSource.CollectionChanged += (s, e) =>
            {
                PropertyCollectionSource source = s as PropertyCollectionSource;
                if (source != null)
                {
                    adapter.DataSource = source.Collection;
                   /* MapControlDetailViewItem item = ((DetailView)View).FindItem("MapControl") as MapControlDetailViewItem;
                    MapControl map = ((MapControlDetailViewItem)item).Control as MapControl;
                    map.ZoomToFitLayerItems(new LayerBase[] { itemsLayer });*/
                }
            };
            return itemsLayer;
        }

    }
}
