﻿namespace EarthCapePro.Module.Win.Controllers
{
    partial class GetGPS_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_RecordCoordinates = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_RecordCoordinates
            // 
            this.simpleAction_RecordCoordinates.Caption = "Get GPS";
            this.simpleAction_RecordCoordinates.ConfirmationMessage = null;
            this.simpleAction_RecordCoordinates.Id = "simpleAction_RecordCoordinates";
            this.simpleAction_RecordCoordinates.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_RecordCoordinates.TargetObjectType = typeof(EarthCape.Module.Core.IWKT);
            this.simpleAction_RecordCoordinates.TargetViewId = "";
            this.simpleAction_RecordCoordinates.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.simpleAction_RecordCoordinates.ToolTip = null;
            this.simpleAction_RecordCoordinates.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.simpleAction_RecordCoordinates.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_RecordCoordinates_Execute);
            // 
            // GetGPS_ViewController
            // 
            this.Actions.Add(this.simpleAction_RecordCoordinates);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_RecordCoordinates;
    }
}
