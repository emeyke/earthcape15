﻿namespace EarthCape.Module.Win.Controllers
{
    partial class Dwc_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ExportDwC = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_ExportDwC
            // 
            this.popupWindowShowAction_ExportDwC.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportDwC.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportDwC.Caption = "Export DwC Archive";
            this.popupWindowShowAction_ExportDwC.Category = "Export";
            this.popupWindowShowAction_ExportDwC.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportDwC.Id = "popupWindowShowAction_ExportDwC";
            this.popupWindowShowAction_ExportDwC.TargetViewId = "Unit_ListView_DwC";
            this.popupWindowShowAction_ExportDwC.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.popupWindowShowAction_ExportDwC.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_ExportDwC.ToolTip = null;
            this.popupWindowShowAction_ExportDwC.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_ExportDwC.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.PopupWindowShowAction_ExportDwC_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportDwC.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.PopupWindowShowAction_ExportDwC_Execute);
            // 
            // Dwc_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_ExportDwC);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportDwC;
    }
}
