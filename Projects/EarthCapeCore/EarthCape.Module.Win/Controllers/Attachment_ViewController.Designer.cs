namespace EarthCapePro.Module.Win.Controllers
{
    partial class Attachment_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_LoadFiles = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_CameraCapture = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_UpdateThumbnailsFromUrl = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_OCR = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_LoadFiles
            // 
            this.simpleAction_LoadFiles.Caption = "Load files";
            this.simpleAction_LoadFiles.Category = "ObjectsCreation";
            this.simpleAction_LoadFiles.ConfirmationMessage = null;
            this.simpleAction_LoadFiles.Id = "simpleAction_LoadFiles";
            this.simpleAction_LoadFiles.ImageName = "Open2";
            this.simpleAction_LoadFiles.TargetViewId = "";
            this.simpleAction_LoadFiles.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_LoadFiles.ToolTip = null;
            this.simpleAction_LoadFiles.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_LoadFiles.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_AddFile_Execute);
            // 
            // simpleAction_CameraCapture
            // 
            this.simpleAction_CameraCapture.Caption = "Camera capture";
            this.simpleAction_CameraCapture.Category = "Tools";
            this.simpleAction_CameraCapture.ConfirmationMessage = "This will replace the attachment info in this record with a camera capture if it " +
    "already exists. Continue?";
            this.simpleAction_CameraCapture.Id = "simpleAction_CameraCapture";
            this.simpleAction_CameraCapture.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_CameraCapture.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.simpleAction_CameraCapture.ToolTip = null;
            this.simpleAction_CameraCapture.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.simpleAction_CameraCapture.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_CameraCapture_Execute);
            // 
            // simpleAction_UpdateThumbnailsFromUrl
            // 
            this.simpleAction_UpdateThumbnailsFromUrl.Caption = "Update thumbnails from URL";
            this.simpleAction_UpdateThumbnailsFromUrl.Category = "Tools";
            this.simpleAction_UpdateThumbnailsFromUrl.ConfirmationMessage = null;
            this.simpleAction_UpdateThumbnailsFromUrl.Id = "simpleAction_UpdateThumbnailsFromUrl";
            this.simpleAction_UpdateThumbnailsFromUrl.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateThumbnailsFromUrl.ToolTip = null;
            this.simpleAction_UpdateThumbnailsFromUrl.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_UpdateThumbnailsFromUrl_Execute);
            // 
            // popupWindowShowAction_OCR
            // 
            this.popupWindowShowAction_OCR.AcceptButtonCaption = null;
            this.popupWindowShowAction_OCR.CancelButtonCaption = null;
            this.popupWindowShowAction_OCR.Caption = "OCR (beta)";
            this.popupWindowShowAction_OCR.Category = "Tools";
            this.popupWindowShowAction_OCR.ConfirmationMessage = null;
            this.popupWindowShowAction_OCR.Id = "popupWindowShowAction_OCR";
            this.popupWindowShowAction_OCR.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_OCR.TargetViewId = "x";
            this.popupWindowShowAction_OCR.ToolTip = "Tesseract engine OCR";
            this.popupWindowShowAction_OCR.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_OCR_CustomizePopupWindowParams);
            this.popupWindowShowAction_OCR.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_OCR_Execute);
            // 
            // Attachment_ViewController
            // 
            this.Actions.Add(this.simpleAction_LoadFiles);
            this.Actions.Add(this.simpleAction_CameraCapture);
            this.Actions.Add(this.simpleAction_UpdateThumbnailsFromUrl);
            this.Actions.Add(this.popupWindowShowAction_OCR);
            this.TargetObjectType = typeof(EarthCape.Module.Core.UnitAttachment);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_LoadFiles;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_CameraCapture;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateThumbnailsFromUrl;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_OCR;
    }
}
