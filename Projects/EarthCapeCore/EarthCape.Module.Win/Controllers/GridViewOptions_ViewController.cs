﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraScheduler;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GridViewOptions_ViewController : ViewController
    {
        public GridViewOptions_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (((ListView)View).Editor is GridListEditor)
            {
                GridView gridView = ((GridListEditor)((ListView)View).Editor)
                    .GridView;
                gridView.OptionsView.ColumnAutoWidth = false;
            }
            if (((ListView)View).Editor is SchedulerListEditor)
            {
                SchedulerControl grid = ((SchedulerListEditor)((ListView)View).Editor).SchedulerControl;
                //grid.GroupType = SchedulerGroupType.Resource;
              //  grid.TimelineView.ShowResourceHeaders = true;
              //  grid.ResourceNavigator.Visibility = ResourceNavigatorVisibility.Always;
                 grid.OptionsView.ResourceHeaders.RotateCaption = false;
            }
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
