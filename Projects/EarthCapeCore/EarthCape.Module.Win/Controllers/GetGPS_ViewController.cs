﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TatukGIS.NDK.WinForms;
using EarthCape.Module.GIS.Win;
using EarthCape.Module.Core;
using EarthCape.Module.GIS;
using System.Configuration;
using TatukGIS.NDK;

namespace EarthCapePro.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GetGPS_ViewController : ViewController
    {
        public GetGPS_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_RecordCoordinates_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TGIS_GpsNmea GPS = GISHelperWin.GetGPS((DetailView)View);
            if (GPS != null)
            {
                if (!GPS.Active)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["GPSCOM"]))
                       GPS.Com = Convert.ToInt32(ConfigurationManager.AppSettings["GPSCOM"]);
                  if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["GPSBAUD"]))
                      GPS.BaudRate = Convert.ToInt32(ConfigurationManager.AppSettings["GPSBAUD"]);
                    GPS.Active = true;
                    GPS.Position += GPS_Position;
                }
               IWKT obj= ((IWKT)View.CurrentObject);
               obj.EPSG = 4326;
               TGIS_Point ptg = TGIS_Utils.GisPoint(GPS.Longitude, GPS.Latitude);
               string wkt = String.Format("POINT({0} {1})", ptg.X, ptg.Y);
               obj.WKT = wkt;// string.Format("POINT({0} {1})", shp.Centroid().X.ToString().Replace(",", "."), shp.Centroid().Y.ToString().Replace(",", "."));
               GISHelper.UpdateAreaAndLengthFromWKT(obj);
            }
         }

        void GPS_Position(object sender, EventArgs e)
        {
            
        }
    }
}
