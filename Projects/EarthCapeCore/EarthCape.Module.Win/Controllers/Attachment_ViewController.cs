﻿using System;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using EarthCape.Module.Core;
using EarthCape.Module.Win;
using DevExpress.XtraEditors.Camera;
using System.Net.Http;
using EarthCape.Zenodo;
using System.Net.Http.Headers;
using EarthCape.Module.GBIF;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;

namespace EarthCapePro.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Attachment_ViewController : ViewController
    {
        public Attachment_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            /*    if ((popupWindowShowAction_AddFiles.Active) && (View.ObjectTypeInfo != null))
                {
                    popupWindowShowAction_AddFiles.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Write)));
                }*/
            if ((popupWindowShowAction_OCR.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_OCR.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
            if ((simpleAction_CameraCapture.Active) && (View.ObjectTypeInfo != null))
            {
                simpleAction_CameraCapture.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
            if ((simpleAction_UpdateThumbnailsFromUrl.Active) && (View.ObjectTypeInfo != null))
            {
                simpleAction_UpdateThumbnailsFromUrl.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        private void simpleAction_AddFile_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Title = "Select files";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in dlgOpen.FileNames)
                {
                    string name = Path.GetFileName(file);
                    UnitAttachment at = View.ObjectSpace.FindObject<UnitAttachment>(new BinaryOperator("Name", name));

                    if (at == null)
                    {
                        at = View.ObjectSpace.CreateObject<UnitAttachment>();
                        at.Name = name;
                        at.LocalPath = file;
                        var im = new Bitmap(file);
                        at.Thumbnail = AttachmentHelper.CreateThumbnailFromBitmap(im, 200, 200);
                        Unit u = null;
                        if ((Frame is NestedFrame) && (((NestedFrame)Frame).ViewItem.View.CurrentObject != null))
                        {
                            if (((NestedFrame)Frame).ViewItem.View.ObjectTypeInfo.Type == typeof(Unit))
                                u = (Unit)((NestedFrame)Frame).ViewItem.View.CurrentObject;                        
                        } else
                            u = View.ObjectSpace.FindObject<Unit>(new BinaryOperator("UnitID", Path.GetFileNameWithoutExtension(file)));
                        if (u != null)
                            at.Unit = u;
                        else
                        { 
                            at.Unit = View.ObjectSpace.CreateObject<Unit>();
                            at.Unit.UnitID = Path.GetFileNameWithoutExtension(file);
                        }
                        at.Save();
                    }
                }
                View.ObjectSpace.CommitChanges();
            }
        }



        private void simpleAction_CameraCapture_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            TakePictureDialog dlg = new TakePictureDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Image image = dlg.Image;
                UnitAttachment file = (UnitAttachment)View.CurrentObject;
                string filename = string.Format("Camera-capture-{0}-{1}-{2}-{3}-{4}-{5}-{6}.jpg", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);
                /*         image.Save(filename, ImageFormat.Jpeg);
                         attach.OriginalFileName = Path.GetFileName(filename);
                         attach.OriginalFilePath = Path.GetDirectoryName(filename);*/
                //    AttachmentFileData fdata = ObjectSpace.CreateObject<AttachmentFileData>();
                using (var stream = new MemoryStream())
                {
                    image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    file.Thumbnail = AttachmentHelper.CreateThumbnail(stream, 300, 300);// ResizeImage(Image.FromFile(filename), 100);
                    file.LocalPath = filename;
                    // if (file.Storage == null)
                    //{
                    file.LocalPath = EarthCapeModule.FileSystemStoreLocation;
                    if (!Directory.Exists(EarthCapeModule.FileSystemStoreLocation))
                        Directory.CreateDirectory(EarthCapeModule.FileSystemStoreLocation);
                    var i2 = new Bitmap(image);
                    i2.Save(EarthCapeModule.FileSystemStoreLocation + "\\" + filename, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //}
                    file.Save();
                    ObjectSpace.CommitChanges();
                    string Key = string.Format("{0:N}{1}", file.Oid, Path.GetExtension(file.LocalPath)).ToUpper();
                    /*     if ((file.Storage != null) && (file.Storage is AttachmentStorageAmazonS3))
                             AttachmentHelperAWS.SaveToS3(file, (AttachmentStorageAmazonS3)(file.Storage), stream, Key);
                             */
                    /*
                    AttachmentFileData fdata = ObjectSpace.CreateObject<AttachmentFileData>();
                    FileDataHelper.LoadFromStream(fdata, filename, stream, filename);
                    file.FileUpload = fdata;
                    fdata.FileName = filename;*/
                    //  file.Save();
                }
                file.Save();
                ObjectSpace.CommitChanges();
            }
        }

        private void popupWindowShowAction_AddFiles_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            using (OpenFileDialog dlgOpen = new OpenFileDialog())
            {
                dlgOpen.CheckFileExists = true;
                dlgOpen.Multiselect = true;
                if (dlgOpen.ShowDialog() == DialogResult.OK)
                {
                    //AttachmentStorage store = ObjectSpace.FindObject<AttachmentStorage>(new BinaryOperator("Oid", ((AttachmentStorage)e.PopupWindowViewCurrentObject).Oid));
                    foreach (string filename in dlgOpen.FileNames)
                    {
                        Attachment file = ObjectSpace.CreateObject<Attachment>();
                        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            if (GeneralHelper.IsRecognisedImageFile(filename))
                                file.Thumbnail = AttachmentHelper.CreateThumbnail(fs, 100, 100);// ResizeImage(Image.FromFile(filename), 100);
                            file.OriginalFileName = Path.GetFileName(filename);
                            file.OriginalFilePath = Path.GetDirectoryName(filename);
                            //file.Storage = store;
                            file.Save();
                            ObjectSpace.CommitChanges();
                            string Key = string.Format("{0:N}{1}", file.Oid, Path.GetExtension(file.OriginalFileName)).ToUpper();
                            /*   if ((file.Storage != null) && (file.Storage is AttachmentStorageAmazonS3))
                                   AttachmentHelperAWS.SaveToS3(file, (AttachmentStorageAmazonS3)(file.Storage), fs, Key);
                          */
                        }
                        file.Save();
                    }
                    ObjectSpace.CommitChanges();
                }
            }
        }

        private void popupWindowShowAction_AddFiles_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            //   e.View = Application.CreateListView(os,typeof(AttachmentStorage),true);
        }

        private void simpleAction_UpdateThumbnailsFromUrl_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (UnitAttachment item in View.SelectedObjects)
            {
                if (GeneralHelper.IsRecognisedImageFile(item.LocalPath))
                {
                    var im = new Bitmap(item.LocalPath);
                    item.Thumbnail = AttachmentHelper.CreateThumbnailFromBitmap(im, 200, 200);
                    item.Save();
                }
                else
                if (!String.IsNullOrEmpty(item.JpegUrl))
                {
                    var im = AttachmentHelper.GetBitmapFromUrl(item.JpegUrl);
                    if (im != null)
                    {
                        item.Thumbnail = AttachmentHelper.CreateThumbnailFromBitmap(im, 200, 200);
                        item.Save();
                    }
                }
            }
            View.ObjectSpace.CommitChanges();
        }

        private void simpleAction_OCR_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //string output = "";
        }

        private void popupWindowShowAction_OCR_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            OcrOptions options = ((OcrOptions)(e.PopupWindowViewCurrentObject));

            foreach (UnitAttachment item in View.SelectedObjects)
            {
                var text = "";
                try
                {
                    text = OCRHelper.OCRAttachment(item, options.CopyToUnit);
                    ObjectSpace.CommitChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                //   MessageBox.Show(output);


            }

        }

        private void popupWindowShowAction_OCR_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            OcrOptions options = os.CreateObject<OcrOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }

        private void popupWindowShowAction_UnitAttachmentUploadToZenodo_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ZenodoUploadOptions options = os.CreateObject<ZenodoUploadOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }

        private void popupWindowShowAction_UnitAttachmentUploadToZenodo_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            /*Settings settings = View.ObjectSpace.FindObject<Settings>(CriteriaOperator.Parse("1=1"));

            ZenodoUploadOptions options = (ZenodoUploadOptions)e.PopupWindowViewCurrentObject;
            HttpClient httpClient = new HttpClient();

            ZenodoClient cl = new ZenodoClient(httpClient);
            cl.BaseUrl = options.Test ? PublishHelper.zenodoSandboxApi : PublishHelper.zenodoApi;
            string baseurl = options.Test ? PublishHelper.zenodoSandboxUrl : PublishHelper.zenodoUrl;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", options.Test ? settings.ZenodoSandboxToken : settings.ZenodoToken);
            foreach (UnitAttachment at in View.SelectedObjects)
            {
                PublishHelper.ZenodoUploadUnitAttachment(cl, at, options,settings);
                View.ObjectSpace.CommitChanges();
            }*/
        }
    }
}

