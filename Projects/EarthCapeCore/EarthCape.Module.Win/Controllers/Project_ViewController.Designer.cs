namespace EarthCapeEBFB.Module.Controllers
{
    partial class Project_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_CreateAuthorList = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.popupWindowShowAction_ExportProjectToXML = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // simpleAction_CreateAuthorList
            // 
            this.simpleAction_CreateAuthorList.Caption = "Create author list";
            this.simpleAction_CreateAuthorList.ConfirmationMessage = null;
            this.simpleAction_CreateAuthorList.Id = "simpleAction_CreateAuthorList";
            this.simpleAction_CreateAuthorList.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_CreateAuthorList.TargetObjectType = typeof(EarthCape.Module.Core.ProjectContact);
            this.simpleAction_CreateAuthorList.ToolTip = null;
            this.simpleAction_CreateAuthorList.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_CreateAuthorList_Execute);
            // 
            // popupWindowShowAction_ExportProjectToXML
            // 
            this.popupWindowShowAction_ExportProjectToXML.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportProjectToXML.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportProjectToXML.Caption = "Export datasets to XML";
            this.popupWindowShowAction_ExportProjectToXML.Category = "Export";
            this.popupWindowShowAction_ExportProjectToXML.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportProjectToXML.Id = "popupWindowShowAction_ExportProjectToXML";
            this.popupWindowShowAction_ExportProjectToXML.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_ExportProjectToXML.TargetObjectType = typeof(EarthCape.Module.Core.Dataset);
            this.popupWindowShowAction_ExportProjectToXML.TargetViewId = "";
            this.popupWindowShowAction_ExportProjectToXML.ToolTip = null;
            this.popupWindowShowAction_ExportProjectToXML.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ExportProjectToXML_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportProjectToXML.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ExportProjectToXML_Execute);
            // 
            // Project_ViewController
            // 
            this.Actions.Add(this.simpleAction_CreateAuthorList);
            this.Actions.Add(this.popupWindowShowAction_ExportProjectToXML);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_CreateAuthorList;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportProjectToXML;
    }
}
