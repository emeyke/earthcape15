﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using EarthCape.Module.GBIF;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Dataset_ViewController : ViewController
    {
        public Dataset_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SimpleAction_ExportDwcA_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            FolderBrowserDialog dlgOpen = new FolderBrowserDialog();
            int exported = 0;
            int empty = 0;
            int failed = 0;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (Dataset item in View.SelectedObjects)
                {
                    string dwc = PublishHelper.GetDwcA(String.Format("{0}/dataset-{1}-dwca.zip",dlgOpen.SelectedPath,item.Oid), item, View.ObjectSpace, (IModelListView)Application.Model.Views["Unit_ListView_DwC"], (IModelListView)Application.Model.Views["UnitAttachment_ListView_Dwc"], (IModelListView)Application.Model.Views["LocalityVisit_ListView_DwC"], ref exported, ref empty, ref failed,item.GbifPreferredIdentifier, item.ZenodoConceptDoi);

                }
            }

        }
    }
}
