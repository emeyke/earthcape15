﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.PivotGrid.Win;
using DevExpress.XtraPivotGrid;
using DevExpress.Data.PivotGrid;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PivotGridCustom_ViewController : ViewController
    {
        public PivotGridCustom_ViewController()
        {
            InitializeComponent();
            TargetViewType = ViewType.ListView;
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            PivotGridControl pgrid = FindGrid((ListView)View);
            if (pgrid != null)
            {
                pgrid.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(pgrid_CustomCellDisplayText);
                if (View.Id == "SequencingRun_Plate_ListView") { 
                pgrid.OptionsView.HideAllTotals();
                pgrid.OptionsView.ShowColumnHeaders = false;
                pgrid.OptionsView.ShowDataHeaders = false;
                pgrid.OptionsView.ShowFilterHeaders = false;
                pgrid.OptionsView.ShowRowHeaders = false;
            }
                //pgrid.CellDoubleClick += new PivotCellEventHandler(pgrid_CellDoubleClick);
                // Access and customize the target View control.
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        private static PivotGridControl FindGrid(ListView listView)
        {
                if (listView.Editor is PivotGridListEditor)
                {
                    PivotGridListEditor editor = (PivotGridListEditor)(listView.Editor);
                    return editor.PivotGridControl;
                }
            return null;
        }
        void BindDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            PivotGridControl pgrid = FindGrid((ListView)View);
            pgrid.CustomCellDisplayText += new PivotCellDisplayTextEventHandler(pgrid_CustomCellDisplayText);
         //   pgrid.CellDoubleClick += new PivotCellEventHandler(pgrid_CellDoubleClick);
         }

    
      /*  void pgrid_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            drillDownDataSource = e.CreateDrillDownDataSource();
            simpleAction1.DoExecute();
        }*/

        void pgrid_CustomCellDisplayText(object sender, PivotCellDisplayTextEventArgs e)
        {
            PivotDrillDownDataSource dataSource = e.CreateDrillDownDataSource();
            PivotGridFieldBase fd = e.DataField;
            if (fd != null)
            {
                if (fd.SummaryType == PivotSummaryType.Custom)
                {
                    if (dataSource != null)
                    {
                        if (dataSource.RowCount == 1)
                        {
                            if (!fd.FieldName.Contains("Oid"))
                                e.DisplayText = Convert.ToString(dataSource.GetValue(0, fd));
                        }
                        else
                        {
                            if (dataSource.RowCount > 1)
                                e.DisplayText = "<...>";

                        }

                    }
                }
            }
        }
    /*    PivotDrillDownDataSource drillDownDataSource;
        private void simpleAction1_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (drillDownDataSource == null) return;
            PivotGridControl pgrid = FindGrid((DetailView)View);
            Type type = (View.CurrentObject).GetType();
            CollectionSource newCollectionSource = new CollectionSource(Application.CreateObjectSpace(), type);
            List<Guid> list = new List<Guid>();
            for (int i = 0; i < drillDownDataSource.RowCount; i++)
            {
                list.Add((Guid)drillDownDataSource.GetValue(i, "Oid"));
            }
            InOperator inOperator = new InOperator("Oid", list);
            newCollectionSource.Criteria["CreateDrillDownDataSource"] = inOperator;
            e.ShowViewParameters.CreatedView = Application.CreateListView(Application.FindListViewId(type), newCollectionSource, true); ;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            /*  DialogController dialogController = Application.CreateController<DialogController>();
              dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(PopupNotesController_Accepting);
              e.ShowViewParameters.Controllers.Add(dialogController);*/
       // }
    }
}
