﻿using DevExpress.ExpressApp.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Xpand.Persistent.Base.General;
using Xpand.Persistent.Base.ImportExport;
using Xpand.Xpo;
using Xpand.ExpressApp.IO.Core;
using Xpand.Persistent.BaseImpl.ImportExport;

namespace EarthCapePro.Module.Win
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IOUpdateController : ViewController
    {
        public IOUpdateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_UpdateProportions_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = View.ObjectSpace;
            foreach (ISerializationConfiguration item in View.SelectedObjects)
            {
                Generate(item);
            }



        }
        string[] _excludedMembers;
        ISerializationConfigurationGroup _serializationConfigurationGroup;

        // public string[] ExcludedMembers => _excludedMembers;

        public void Generate(ISerializationConfiguration serializationConfiguration)
        {
            var typeToSerialize = serializationConfiguration.TypeToSerialize;
            var castTypeToTypeInfo = XafTypesInfo.CastTypeToTypeInfo(typeToSerialize);
            var objectSpace = XPObjectSpace.FindObjectSpaceByObject(serializationConfiguration);
            _serializationConfigurationGroup = serializationConfiguration.SerializationConfigurationGroup;
            if (_serializationConfigurationGroup == null)
                throw new NullReferenceException("_serializationConfigurationGroup");
            foreach (var descendant in ReflectionHelper.FindTypeDescendants(castTypeToTypeInfo))
            {
                Generate(objectSpace, descendant.Type);
            }
            foreach (IClassInfoGraphNode classInfoGraphNode in CreateGraph(objectSpace, castTypeToTypeInfo))
            {
                var clnode = serializationConfiguration.SerializationGraph.FirstOrDefault(c => c.Name == classInfoGraphNode.Name);
                if (clnode == null)
                    serializationConfiguration.SerializationGraph.Add(classInfoGraphNode);
            }

        }

        IEnumerable<IClassInfoGraphNode> CreateGraph(IObjectSpace objectSpace, ITypeInfo typeToSerialize)
        {
            var memberInfos = GetMemberInfos(typeToSerialize);
            var classInfoGraphNodes = CreateGraphCore(memberInfos, objectSpace).ToArray();
            ResetDefaultKeyWhenMultiple(classInfoGraphNodes);
            return classInfoGraphNodes;
        }

        void ResetDefaultKeyWhenMultiple(IEnumerable<IClassInfoGraphNode> classInfoGraphNodes)
        {
            var infoGraphNodes = classInfoGraphNodes as IClassInfoGraphNode[] ?? classInfoGraphNodes.ToArray();
            var nonDefaultKey = infoGraphNodes.Skip(1).FirstOrDefault(node => node.Key);
            if (nonDefaultKey != null)
            {
                infoGraphNodes.First(graphNode => graphNode.Key).Key = false;
            }
        }


        IEnumerable<IClassInfoGraphNode> CreateGraphCore(IEnumerable<IMemberInfo> memberInfos, IObjectSpace objectSpace)
        {
            return memberInfos.Select(memberInfo => (!memberInfo.MemberTypeInfo.IsPersistent && !memberInfo.IsList) || memberInfo.MemberType == typeof(byte[])
                                                                                    ? CreateSimpleNode(memberInfo, objectSpace)
                                                                                    : CreateComplexNode(memberInfo, objectSpace)).ToList();
        }

        Type GetSerializedType(IMemberInfo memberInfo)
        {
            return memberInfo.IsList ? memberInfo.ListElementType : memberInfo.MemberType;
        }

        IClassInfoGraphNode CreateComplexNode(IMemberInfo memberInfo, IObjectSpace objectSpace)
        {
            NodeType nodeType = memberInfo.MemberTypeInfo.IsPersistent ? NodeType.Object : NodeType.Collection;
            IClassInfoGraphNode classInfoGraphNode = CreateClassInfoGraphNode(objectSpace, memberInfo, nodeType);
            //classInfoGraphNode.SerializationStrategy = GetSerializationStrategy(memberInfo, SerializationStrategy.SerializeAsObject);
            Generate(objectSpace, ReflectionHelper.GetType(classInfoGraphNode.TypeName));
            return classInfoGraphNode;

        }

        void Generate(IObjectSpace objectSpace, Type typeToSerialize)
        {
            var configuration = objectSpace.QueryObject<ISerializationConfiguration>(serializationConfiguration => serializationConfiguration.SerializationConfigurationGroup == _serializationConfigurationGroup &&
                                                                                                    serializationConfiguration.TypeToSerialize == typeToSerialize);
            if (configuration == null)
            {
                var serializationConfiguration = objectSpace.Create<ISerializationConfiguration>();
                serializationConfiguration.SerializationConfigurationGroup = _serializationConfigurationGroup;
                serializationConfiguration.TypeToSerialize = typeToSerialize;
                Generate(serializationConfiguration);
            }
        }



        IClassInfoGraphNode CreateSimpleNode(IMemberInfo memberInfo, IObjectSpace objectSpace)
        {
            return CreateClassInfoGraphNode(objectSpace, memberInfo, NodeType.Simple);
        }

        bool IsKey(IMemberInfo memberInfo)
        {
            return memberInfo.IsKey || memberInfo.FindAttribute<SerializationKeyAttribute>() != null;
        }


        IClassInfoGraphNode CreateClassInfoGraphNode(IObjectSpace objectSpace, IMemberInfo memberInfo, NodeType nodeType)
        {
            var classInfoGraphNode = objectSpace.Create<IClassInfoGraphNode>();
            classInfoGraphNode.Name = memberInfo.Name;
            //classInfoGraphNode.SerializationStrategy = GetSerializationStrategy(memberInfo, classInfoGraphNode.SerializationStrategy);
            classInfoGraphNode.TypeName = GetSerializedType(memberInfo).Name;
            classInfoGraphNode.NodeType = nodeType;
            classInfoGraphNode.Key = IsKey(memberInfo);
            return classInfoGraphNode;
        }


        IEnumerable<IMemberInfo> GetMemberInfos(ITypeInfo typeInfo)
        {
            _excludedMembers = new[] {
                                         XPObject.Fields.OptimisticLockField.PropertyName,
                                         XPObject.Fields.ObjectType.PropertyName,
                                         XpandCustomObject.ChangedPropertiesName
                                     };
            return typeInfo.Members.Where(info => IsPersistent(info) && !(_excludedMembers.Contains(info.Name))).OrderByDescending(memberInfo => memberInfo.IsKey);
        }

        bool IsPersistent(IMemberInfo info)
        {
            return (info.IsPersistent || (info.IsList && info.ListElementTypeInfo != null && info.ListElementTypeInfo.IsPersistent));
        }

        public void ApplyStrategy(SerializationStrategy serializationStrategy, ISerializationConfiguration serializationConfiguration)
        {
            var serializationConfigurationGroup = serializationConfiguration.SerializationConfigurationGroup;
            var infoGraphNodes = serializationConfigurationGroup.Configurations.SelectMany(configuration => configuration.SerializationGraph);
            var classInfoGraphNodes = infoGraphNodes.Where(node => node.TypeName == serializationConfiguration.TypeToSerialize.Name);
            foreach (var classInfoGraphNode in classInfoGraphNodes)
            {
                classInfoGraphNode.SerializationStrategy = serializationStrategy;
            }
        }

        private void simpleAction_RemoveUsers_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

            IObjectSpace os = View.ObjectSpace;
            foreach (ISerializationConfiguration item in View.SelectedObjects)
            {
                RemoveUser(item);
            }


        }

        private void RemoveUser(ISerializationConfiguration serializationConfiguration)
        {
            var typeToSerialize = serializationConfiguration.TypeToSerialize;
            var castTypeToTypeInfo = XafTypesInfo.CastTypeToTypeInfo(typeToSerialize);
            var objectSpace = XPObjectSpace.FindObjectSpaceByObject(serializationConfiguration);
            _serializationConfigurationGroup = serializationConfiguration.SerializationConfigurationGroup;
            if (_serializationConfigurationGroup == null)
                throw new NullReferenceException("_serializationConfigurationGroup");
            foreach (IClassInfoGraphNode classInfoGraphNode in CreateGraph(objectSpace, castTypeToTypeInfo))
            {
                ClassInfoGraphNode clnode = (ClassInfoGraphNode)serializationConfiguration.SerializationGraph.FirstOrDefault(c => c.Name == classInfoGraphNode.Name);
                if (clnode != null)
                {
                    if (clnode.TypeName == "SecuritySystemUser") clnode.SerializationStrategy = SerializationStrategy.DoNotSerialize;
                    clnode.Save();
                }
            }

        }

    }
}
