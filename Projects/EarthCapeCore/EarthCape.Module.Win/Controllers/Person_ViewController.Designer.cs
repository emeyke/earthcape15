﻿namespace EarthCape.Module.Win.Controllers
{
    partial class Person_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ImportVCF = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ImportVCF
            // 
            this.simpleAction_ImportVCF.Caption = "Import VCF";
            this.simpleAction_ImportVCF.Category = "Export";
            this.simpleAction_ImportVCF.ConfirmationMessage = null;
            this.simpleAction_ImportVCF.Id = "simpleAction_ImportVCF";
            this.simpleAction_ImportVCF.ToolTip = null;
            this.simpleAction_ImportVCF.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ImportVCF_Execute);
            // 
            // Person_ViewController
            // 
            this.Actions.Add(this.simpleAction_ImportVCF);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImportVCF;
    }
}
