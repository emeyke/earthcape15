﻿namespace EarthCape.Module.Win.Controllers
{
    partial class Dataset_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ExportDwcA = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ExportDwcA
            // 
            this.simpleAction_ExportDwcA.Caption = "Export DwC-A";
            this.simpleAction_ExportDwcA.Category = "Tools";
            this.simpleAction_ExportDwcA.ConfirmationMessage = null;
            this.simpleAction_ExportDwcA.Id = "simpleAction_ExportDwcA";
            this.simpleAction_ExportDwcA.ToolTip = null;
            this.simpleAction_ExportDwcA.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SimpleAction_ExportDwcA_Execute);
            // 
            // Dataset_ViewController
            // 
            this.Actions.Add(this.simpleAction_ExportDwcA);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Dataset);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ExportDwcA;
    }
}
