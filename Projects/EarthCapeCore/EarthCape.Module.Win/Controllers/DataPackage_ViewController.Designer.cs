﻿namespace EarthCape.Module.Win.Controllers
{
    partial class DataPackage_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ExportDataPackage = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ExportBOLDSpecimenSheet = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_PublishSpecimen = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_ExportDataPackage
            // 
            this.popupWindowShowAction_ExportDataPackage.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportDataPackage.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportDataPackage.Caption = "Export Data Package";
            this.popupWindowShowAction_ExportDataPackage.Category = "Export";
            this.popupWindowShowAction_ExportDataPackage.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportDataPackage.Id = "popupWindowShowAction_ExportDataPackage";
            this.popupWindowShowAction_ExportDataPackage.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.popupWindowShowAction_ExportDataPackage.ToolTip = null;
            this.popupWindowShowAction_ExportDataPackage.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ExportDataPackage_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportDataPackage.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ExportDataPackage_Execute);
            // 
            // popupWindowShowAction_ExportBOLDSpecimenSheet
            // 
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.Caption = "Export BOLD Specimen Sheet";
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.Category = "Export";
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.TargetObjectType = typeof(EarthCape.Module.Lab.DnaExtract);
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.Id = "popupWindowShowAction_ExportBOLDSpecimenSheet";
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.ToolTip = null;
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ExportBOLDSpecimenSheet_CustomizePopupWindowParams);
            this.popupWindowShowAction_ExportBOLDSpecimenSheet.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ExportBOLDSpecimenSheet_Execute);
            // 
            // popupWindowShowAction_PublishSpecimen
            // 
            this.popupWindowShowAction_PublishSpecimen.AcceptButtonCaption = null;
            this.popupWindowShowAction_PublishSpecimen.CancelButtonCaption = null;
            this.popupWindowShowAction_PublishSpecimen.Caption = "Publish Specimen";
            this.popupWindowShowAction_PublishSpecimen.Category = "Export";
            this.popupWindowShowAction_PublishSpecimen.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.popupWindowShowAction_PublishSpecimen.ConfirmationMessage = "This is a test restricted to Zenodo Sandbox";
            this.popupWindowShowAction_PublishSpecimen.Id = "popupWindowShowAction_PublishSpecimen";
            this.popupWindowShowAction_PublishSpecimen.ToolTip = null;
            this.popupWindowShowAction_PublishSpecimen.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_PublishSpecimen_CustomizePopupWindowParams);
            this.popupWindowShowAction_PublishSpecimen.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_PublishSpecimen_Execute);
            // 
            // DataPackage_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_ExportDataPackage);
            this.Actions.Add(this.popupWindowShowAction_ExportBOLDSpecimenSheet);
            this.Actions.Add(this.popupWindowShowAction_PublishSpecimen);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportDataPackage;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportBOLDSpecimenSheet;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_PublishSpecimen;
    }
}
