﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DevExpress.Compression;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using EarthCape.Module.Core;
using EarthCape.Module.GBIF;

namespace EarthCape.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Dwc_ViewController : ViewController
    {
        public Dwc_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PopupWindowShowAction_ExportDwC_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            using (FileStream file = new FileStream(Path.GetTempPath()+@"\occurrences.csv", FileMode.Create, FileAccess.ReadWrite))
            {
                if (!Directory.Exists(PathHelper.GetApplicationFolder() + @"\Output")) Directory.CreateDirectory(PathHelper.GetApplicationFolder() + @"\Output");
                DwcOptions options = (DwcOptions)e.PopupWindowView.CurrentObject;
                ((GridView)((GridControl)((ListView)View).Control).MainView).OptionsPrint.PrintSelectedRowsOnly = false;
                CsvExportOptionsEx exoptions = new CsvExportOptionsEx();
                exoptions.Encoding = Encoding.UTF8;
                ((GridControl)((ListView)View).Control).ExportToCsv(file,exoptions);
                file.Flush();
                file.Close();
                Dataset ds = (Dataset)(((NestedFrame)Frame).ViewItem.View.CurrentObject);
                GbifHelper.CreateMeta((IModelListView)Application.Model.Views["Unit_ListView_DwC"]);
                GbifHelper.CreateEML(ds);
                using (ZipArchive archive = new ZipArchive())
                {
                    archive.AddFile(Path.GetTempPath()+@"\meta.xml", @"\");
                    archive.AddFile(Path.GetTempPath()+@"\eml.xml", @"\");
                    archive.AddFile(Path.GetTempPath()+@"\occurrences.csv", @"\");
                    archive.Save(PathHelper.GetApplicationFolder()+@"\Output\dwc.zip");
                }
            }

        }
        private void PopupWindowShowAction_ExportDwC_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace obs = (XPObjectSpace)Application.CreateObjectSpace();
            DwcOptions param = obs.CreateObject<DwcOptions>();
            DetailView dv = Application.CreateDetailView(obs, param);
            e.View = dv;
        }

     
    }

      
    }
