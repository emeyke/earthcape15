using DevExpress.Persistent.Validation;
using DevExpress.Xpo;


namespace EarthCape.Module.Win
{
   
    [NonPersistent]
    public class DocExportOptions : XPCustomObject
    {
        public DocExportOptions(Session session) : base(session) { }

        string folder;

        [Size(1000)]
        [RuleRequiredField(DefaultContexts.Save)]
            public string Folder
        {
            get => folder;
            set => SetPropertyValue(nameof(Folder), ref folder, value);
        }

        private string _Database;
        public string Database
        {
            get { return _Database; }
            set { SetPropertyValue<string>(nameof(Database), ref _Database, value); }
        }


        private string _Link;
        public string Link
        {
            get { return _Link; }
            set { SetPropertyValue<string>(nameof(Link), ref _Link, value); }
        }



    }

}
