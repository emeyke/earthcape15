using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Win
{
   
    [NonPersistent]
    public class ImportDwcOptions : XPCustomObject
    {
        public ImportDwcOptions(Session session) : base(session) { }



        private FileDataEx _DwcArchive;
        public FileDataEx DwcArchive
        {
            get { return _DwcArchive; }
            set { SetPropertyValue<FileDataEx>(nameof(DwcArchive), ref _DwcArchive, value); }
        }

        private bool _MatchUnitID=false;
        public bool MatchUnitID
        {
            get { return _MatchUnitID; }
            set { SetPropertyValue<bool>(nameof(MatchUnitID), ref _MatchUnitID, value); }
        }


        private long _SkipFirst=0;
        public long SkipFirst
        {
            get { return _SkipFirst; }
            set { SetPropertyValue<long>(nameof(SkipFirst), ref _SkipFirst, value); }
        }


        private bool _Occurrence=true;
        public bool Occurrence
        {
            get { return _Occurrence; }
            set { SetPropertyValue<bool>(nameof(Occurrence), ref _Occurrence, value); }
        }

        private bool _Multimedia=true;
        public bool Multimedia
        {
            get { return _Multimedia; }
            set { SetPropertyValue<bool>(nameof(Multimedia), ref _Multimedia, value); }
        }


        private bool _Identification=true;
        public bool Identification
        {
            get { return _Identification; }
            set { SetPropertyValue<bool>(nameof(Identification), ref _Identification, value); }
        }





    }

}
