using DevExpress.Persistent.Validation;
using DevExpress.Xpo;


namespace EarthCape.Module.Win
{
   
    [NonPersistent]
    public class OcrOptions : XPCustomObject
    {
        public OcrOptions(Session session) : base(session) { }


        private bool _CopyToUnit=true;
        public bool CopyToUnit
        {
            get { return _CopyToUnit; }
            set { SetPropertyValue<bool>(nameof(CopyToUnit), ref _CopyToUnit, value); }
        }



    }

}
