﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.SystemModule;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using EarthCape.Module.Core;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

//https://supportcenter.devexpress.com/ticket/details/t832660/win-xaf-gantt-chart

namespace EarthCape.Module.Win
{
    [ListEditor(typeof(UnitTask), false)]
    public class GanttListEditor : ListEditor, IRequireContextMenu, IRequireDXMenuManager
    {
        private DevExpress.XtraGantt.GanttControl ganttControl;
        private Object controlDataSource;
        private DevExpress.XtraTreeList.Columns.TreeListColumn unitIDColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn datasetColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn projectColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn subjectColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn startDateColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn assignedColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn dueColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn locColumn;
        private DevExpress.XtraTreeList.Columns.TreeListColumn percentCompleted;

        public GanttListEditor(IModelListView info) : base(info) { }

        public override SelectionType SelectionType
        {
            get { return SelectionType.Full; }
        }

        public override IList GetSelectedObjects()
        {
            if (ganttControl == null) return new object[0] { };

            object[] result = new object[ganttControl.Selection.Count];
            for (int i = 0; i < ganttControl.Selection.Count; i++)
            {
                result[i] = ganttControl.Selection[0].Tag;
            }
            return result;
        }

        public override void Refresh()
        {
            if (ganttControl == null) return;

            ganttControl.ClearSelection();

            try
            {
                ganttControl.BeginUpdate();
                ganttControl.DataSource = controlDataSource;
            }
            finally
            {
                ganttControl.EndUpdate();
            }
        }

        protected override void AssignDataSourceToControl(object dataSource)
        {
            if (controlDataSource != dataSource)
            {
                if (controlDataSource is IBindingList oldBindingList)
                {
                    oldBindingList.ListChanged -= new ListChangedEventHandler(dataSource_ListChanged);
                }

                controlDataSource = dataSource;
                if (controlDataSource is IBindingList bindingList)
                {
                    bindingList.ListChanged += new ListChangedEventHandler(dataSource_ListChanged);
                }
                Refresh();
            }
        }

        private void dataSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            Refresh();
        }

        protected override object CreateControlsCore()
        {
            ganttControl = new DevExpress.XtraGantt.GanttControl();
            this.datasetColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.unitIDColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.startDateColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.projectColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.subjectColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.assignedColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dueColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.locColumn = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.percentCompleted = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            //  
            // taskNameColumn  
            //  
            this.unitIDColumn.Caption = "UnitID";
            this.unitIDColumn.SortIndex = 0;
            this.unitIDColumn.FieldName = "Unit.UnitID";
            this.unitIDColumn.MinWidth = 100;
            this.unitIDColumn.Name = "unitIDColumn";
            this.unitIDColumn.Visible = true;
            this.unitIDColumn.VisibleIndex = 0;
            this.unitIDColumn.Width = 260;
            //  
            // startDateColumn  
            //  
            this.startDateColumn.Caption = "StartDate";
            this.startDateColumn.SortIndex = 1;
            this.startDateColumn.FieldName = "StartDate";
            this.startDateColumn.MinWidth = 100;
             this.startDateColumn.Name = "startDateColumn";
            this.startDateColumn.Visible = true;
            this.startDateColumn.VisibleIndex = 5;
            this.startDateColumn.Width = 150;

            this.dueColumn.Caption = "DueDate";
            this.dueColumn.FieldName = "DueDate";
            this.dueColumn.MinWidth = 100;
            this.dueColumn.Name = "dueColumn";
            this.dueColumn.Visible = true;
            this.dueColumn.VisibleIndex = 6;
            this.dueColumn.Width = 150;
            //  
            this.locColumn.Caption = "Location";
            this.locColumn.FieldName = "Unit.Locality";
            this.locColumn.MinWidth = 100;
            this.locColumn.Name = "locColumn";
            this.locColumn.Visible = true;
            this.locColumn.VisibleIndex = 3;
            this.locColumn.Width = 150;
            //  
            //  
            // resourcesColumn  
            //  
            this.datasetColumn.Caption = "Dataset";
            this.datasetColumn.FieldName = "Dataset.Name";
            this.datasetColumn.MinWidth = 13;
            this.datasetColumn.Name = "datasetColumn";
            this.datasetColumn.Visible = true;
            this.datasetColumn.VisibleIndex = 10;
            this.datasetColumn.Width = 150;

            this.subjectColumn.Caption = "Subject";
            this.subjectColumn.FieldName = "Subject";
            this.subjectColumn.MinWidth = 13;
            this.subjectColumn.Name = "subjectColumn";
            this.subjectColumn.Visible = true;
            this.subjectColumn.VisibleIndex = 1;
            this.subjectColumn.Width = 150;


            this.assignedColumn.Caption = "AssignedTo";
            this.assignedColumn.FieldName = "AssignedTo.FullName";
            this.assignedColumn.MinWidth = 13;
            this.assignedColumn.Name = "assignedColumn";
            this.assignedColumn.Visible = true;
            this.assignedColumn.VisibleIndex = 7;
            this.assignedColumn.Width = 150;

         //   RepositoryItemSpinEdit se = new RepositoryItemSpinEdit();
          //  se.Increment = 20;
            RepositoryItemProgressBar pb = new RepositoryItemProgressBar();
            this.percentCompleted.Caption = "PercentCompleted";
            this.percentCompleted.FieldName = "PercentCompleted";
            this.percentCompleted.MinWidth = 13;
            this.percentCompleted.Name = "percentCompleted";
            this.percentCompleted.Visible = true;
            this.percentCompleted.VisibleIndex = 2;
            this.percentCompleted.Width = 150;
            this.percentCompleted.ColumnEdit = pb;
        //    this.percentCompleted.for

            this.ganttControl.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[]
            {
            this.unitIDColumn,
            this.startDateColumn,
            this.percentCompleted,
            this.subjectColumn,
            this.assignedColumn,
            this.locColumn
        });
            ganttControl.MouseDoubleClick += GanttControl_MouseDoubleClick;
            ganttControl.KeyDown += GanttControl_KeyDown;
            ganttControl.OptionsSplitter.PanelVisibility = DevExpress.XtraGantt.GanttPanelVisibility.Both;
            ganttControl.OptionsSplitter.AllowResize = true;
            ganttControl.OptionsEditForm.ShowOnDoubleClick = DefaultBoolean.True;
            ganttControl.OptionsFind.AllowFindPanel = true;
            ganttControl.OptionsView.AutoWidth = true;
            ganttControl.OptionsBehavior.Editable = true;
            ganttControl.OptionsBehavior.EditingMode = DevExpress.XtraTreeList.TreeListEditingMode.EditForm;
            ganttControl.OptionsBehavior.EditorShowMode = DevExpress.XtraTreeList.TreeListEditorShowMode.DoubleClick;
            ganttControl.OptionsBehavior.AutoScrollOnSorting = true;
         
          //  ganttControl.TreeListMappings.KeyFieldName = "This";
          //  ganttControl.TreeListMappings.ParentFieldName = "Parent";
            ganttControl.TreeListMappings.KeyFieldName = "Name";
            ganttControl.TreeListMappings.ParentFieldName = "Parent.Name";

            ganttControl.ChartMappings.StartDateFieldName = startDateColumn.FieldName;
            ganttControl.ChartMappings.FinishDateFieldName = dueColumn.FieldName;
           //     ganttControl.ChartMappings.PredecessorsFieldName = "Parent.Subject";
            ganttControl.ChartMappings.ProgressFieldName = percentCompleted.FieldName;
            ganttControl.ChartMappings.TextFieldName = subjectColumn.FieldName;
         
            Refresh();
            return ganttControl;
        }

        private void GanttControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OnProcessSelectedItem();
            }
        }

        private void GanttControl_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                OnProcessSelectedItem();
            }
        }

        public void SetMenu(PopupMenu popupMenu, BarManager barManager)
        {
            barManager.SetPopupContextMenu(ganttControl, popupMenu);
            barManager.QueryShowPopupMenu += BarManager_QueryShowPopupMenu;
        }

        private void BarManager_QueryShowPopupMenu(object sender, QueryShowPopupMenuEventArgs e)
        {
            if (e.Control != ganttControl)
            {
                e.Cancel = true;
            }
        }

        public void SetMenuManager(IDXMenuManager menuManager) { }
    }
}

