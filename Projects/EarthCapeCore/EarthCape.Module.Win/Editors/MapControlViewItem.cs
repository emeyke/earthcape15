﻿using System;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.XtraMap;

namespace EarthCape.Module.Win
{
    public interface IModelMapControlItem : IModelViewItem { }

    [ViewItemAttribute(typeof(IModelMapControlItem))]
    public class MapControlDetailViewItem : ViewItem
    {
        public MapControlDetailViewItem(IModelViewItem model, Type objectType)
            : base(objectType, model.Id)
        {
            CreateControl();
        }

        protected override object CreateControlCore()
        {

            MapControl map = new MapControl();
       
            
            /*    map.Layers.AddRange(new LayerBase[] {
            new ImageLayer {
                DataProvider = new OpenStreetMapDataProvider { }
            },
            new InformationLayer {
                DataProvider = new OsmSearchDataProvider { }
            }
        });*/



           map.Layers.AddRange(new LayerBase[] {
        new ImageLayer() {
    DataProvider = new BingMapDataProvider() {
        BingKey = "Z7xynZysdXRDCYBTmHb9~NDeGihtjlzRl9SuYOgHs3g~ApSnPhGCvbPMXrLtUyZXa4wX9AfB-Hc4-XkaqwhH-g758JpgBkwOrspkfnJ9eND7" }
        }
        
    });

         /*   //doesnt project properly

            OpenStreetMapDataProvider mapDataProvider = new OpenStreetMapDataProvider();
            string temp = @"https://api.mapbox.com/v4/mapbox.satellite/1/0/0@2x.png?access_token=pk.eyJ1IjoiZWFydGhjYXBlIiwiYSI6ImNqdmh4OTAzYjAwMXc0YW1uODU3bXd6ZjkifQ.JvRd-4It91qDF_nt0hBXpA";
        //   string temp = @"http://{0}.tiles.mapbox.com/v3/pk.eyJ1IjoiZWFydGhjYXBlIiwiYSI6ImNqdmh4OTAzYjAwMXc0YW1uODU3bXd6ZjkifQ.JvRd-4It91qDF_nt0hBXpA/{1}/{2}/{3}.png";
            mapDataProvider.TileUriTemplate = temp;
            ImageLayer layer = new ImageLayer();
            layer.DataProvider = mapDataProvider;
            map.Layers.Add(layer);
            */

            map.ShowSearchPanel = true;
            map.ShowToolTips = true;
            
            return map;
        }
    }


}




