﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Validation;
using Xpand.Persistent.Base.General;
using ECDB.IO.Module.Win;
using MySql.Data.MySqlClient;

namespace Ecdb.Security.Controllers
{


    public interface IDBServerParameter
    {
        string DBServer { get; set; }
    }

    public class ChooseEcdbAtLogonController : ObjectViewController<DetailView, IDBServerParameter>
    {
        string connectionString = "";
        string modules = "";

        static ChooseEcdbAtLogonController()
        {
        }

        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnFrameAssigned()
        {
            base.OnFrameAssigned();
            if (!Application.IsLoggedIn())
            {
                Application.LoggingOn += ApplicationOnLoggingOn;
                Frame.Disposing += FrameOnDisposing;
            }
        }

        private void FrameOnDisposing(object sender, EventArgs eventArgs)
        {
            Frame.Disposing -= FrameOnDisposing;
            Application.LoggingOn -= ApplicationOnLoggingOn;
        }

        private void GetConnectionString(string clientid, out string connectionString, out string modules)
        {
            string connStr = ConfigurationManager.ConnectionStrings["Connection"].ToString();
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                // Console.WriteLine("Connecting to MySQL...");
                conn.Open();

                string sql = String.Format("select ConnectionString, Modules from Account where Name='{0}'", clientid);
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();
                connectionString = rdr[0].ToString();
                modules = rdr[1].ToString();
                rdr.Close();
            }
            finally
            {

            }

            conn.Close();
        }


        private void ApplicationOnLoggingOn(object sender, LogonEventArgs logonEventArgs)
        {
            var parameter = logonEventArgs.LogonParameters as IDBServerParameter;
            GetConnectionString(((EcdbLogonParameters)logonEventArgs.LogonParameters).Database, out connectionString, out modules);
            Application.ConnectionString = connectionString;
            foreach (var provider in Application.ObjectSpaceProviders.OfType<XpandObjectSpaceProvider>().Select(provider => provider.DataStoreProvider).OfType<MultiDataStoreProvider>())
            {
                provider.ConnectionString = connectionString;
            }
            Application.TypesInfo.ModifySequenceObjectWhenMySqlDatalayer();
        }
    }
}