﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpand.ExpressApp.Security.AuthenticationProviders;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Validation;

namespace ECDB.IO.Module.Win
{
    [NonPersistent]
    [Serializable]
    public class EcdbLogonParameters : XpandLogonParameters
    {
        private string _Database;

        public string Database
        {
            get { return _Database; }
            set
            {
                _Database = value;
                RaisePropertyChanged("Database");
            }
        }
       
    }
}
