using EarthCape.Module.Core;
using Tesseract;

namespace EarthCape.Module.Win
{
    public class OCRHelper
    {

        public static string OCRAttachment(UnitAttachment item,bool copyToUnit)
        {
            string text;
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var img = Pix.LoadFromFile(item.LocalPath + "\\" + item.LocalPath))
                {
                    using (var page = engine.Process(img))
                    {
                        text = page.GetText();
                        // output=output+ System.Environment.NewLine+String.Format("Mean confidence: {0}", page.GetMeanConfidence());

                        //  output=output+ System.Environment.NewLine+String.Format("Text (GetText): \r\n{0}", text);
                        // output=output+ System.Environment.NewLine+String.Format("Text (iterator):");
                        /* using (var iter = page.GetIterator())
                         {
                             iter.Begin();

                             do
                             {
                                 do
                                 {
                                     do
                                     {
                                         do
                                         {
                                            /* if (iter.IsAtBeginningOf(PageIteratorLevel.Block))
                                             {
                                                 output=output+ System.Environment.NewLine+String.Format("<BLOCK>");
                                             }

                                             Console.Write(iter.GetText(PageIteratorLevel.Word));
                                            // Console.Write(" ");

                                             if (iter.IsAtFinalOf(PageIteratorLevel.TextLine, PageIteratorLevel.Word))
                                             {
                                                 output=output+ System.Environment.NewLine;
                                             }
                                         } while (iter.Next(PageIteratorLevel.TextLine, PageIteratorLevel.Word));

                                         if (iter.IsAtFinalOf(PageIteratorLevel.Para, PageIteratorLevel.TextLine))
                                         {
                                             output=output+ System.Environment.NewLine;
                                         }
                                     } while (iter.Next(PageIteratorLevel.Para, PageIteratorLevel.TextLine));
                                 } while (iter.Next(PageIteratorLevel.Block, PageIteratorLevel.Para));
                             } while (iter.Next(PageIteratorLevel.Block));
                         }*/
                    }
                }
            }
          /*  if (copyToUnit)
             foreach (UnitAttachment unitA in item.Units)
             {
                 unitA.Unit.PlaceDetails = text;
                 unitA.Unit.Save();
                 unitA.Save();
             }*/
            item.OCR = text;
            item.Save();
            return text;
        }


    }
}
