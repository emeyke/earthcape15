﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Web.Routing;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Web;
using DevExpress.Web;
using DevExpress.ExpressApp.Security.Adapters;
using DevExpress.ExpressApp.Security.Xpo.Adapters;
using System.Net;
using DevExpress.Persistent.AuditTrail;
using EarthCape.Module.Core;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Utils;

namespace EarthCape.Web {
    public class Global : System.Web.HttpApplication {
        public Global() {
            InitializeComponent();
        }
        protected void Application_Start(Object sender, EventArgs e) {
            RouteTable.Routes.RegisterXafRoutes();
            DevExpress.ExpressApp.BaseObjectSpace.ThrowExceptionForNotRegisteredEntityType = true;
#if EASYTEST
            DevExpress.ExpressApp.Web.TestScripts.TestScriptsManager.EasyTestEnabled = true;
#endif
            ASPxWebControl.CallbackError += new EventHandler(Application_Error);
           // IsGrantedAdapter.Enable(XPOSecurityAdapterHelper.GetXpoCachedRequestSecurityAdapters());
        }
        private void ErrorHandling_CustomSendMailMessage(object sender, CustomSendMailMessageEventArgs e)
        {
            e.Smtp.UseDefaultCredentials = false;
            e.Smtp.EnableSsl = false;
            e.Smtp.Port = 587;
            e.Smtp.Host = "smtp.sendgrid.net";
            e.Smtp.Credentials = new NetworkCredential("apikey", "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");
        }
        protected void Session_Start(Object sender, EventArgs e) {
             Tracing.Initialize();
            WebApplication.SetInstance(Session, new EarthCapeAspNetApplication());
            DevExpress.ExpressApp.Web.Templates.DefaultVerticalTemplateContentNew.ClearSizeLimit();
            WebApplication.Instance.SwitchToNewStyle();
            WebApplication.Instance.LinkNewObjectToParentImmediately = true;
            //   WebApplication.Instance.DefaultCollectionSourceMode = CollectionSourceMode.Normal;
            WebApplicationStyleManager.EnableUpperCase = false;
            WebApplicationStyleManager.EnableGridColumnsUpperCase = false;
            WebApplicationStyleManager.EnableGroupUpperCase = false;
            WebApplicationStyleManager.EnableNavigationGroupsUpperCase = false;
            // WebApplication.Instance.Settings.LogonTemplateContentPath = "LogonTemplateContent3.ascx";
            //    WebApplication.Instance.Settings.DefaultVerticalTemplateContentPath = "DefaultVerticalTemplateContent1.ascx";
            ServerPermissionRequestProcessor.UseAutoAssociationPermission = false;
            ServerPermissionPolicyRequestProcessor.UseAutoAssociationPermission = false;
            AuditTrailService.Instance.ObjectAuditingMode = ObjectAuditingMode.Full;
            AuditTrailService.Instance.CustomizeAuditTrailSettings += Instance_CustomizeAuditTrailSettings1; ;
            CaptionHelper.CustomizeConvertCompoundName += CaptionHelper_CustomizeConvertCompoundName;
            ImageLoader.Instance.UseSvgImages = true;
            Value_WindowController.GetApplicationInstance = delegate () { return WebApplication.Instance; };
            string connectionString = "";
            string modules = "";


#if EASYTEST
            if (ConfigurationManager.ConnectionStrings["EasyTestConnectionString"] != null)
            {
                connectionString = ConfigurationManager.ConnectionStrings["EasyTestConnectionString"].ConnectionString;
                modules = ConfigurationManager.AppSettings["Modules"];
            }

#else

           if (ConfigurationManager.ConnectionStrings["ConnectionString"] != null)
            {
                connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                modules = ConfigurationManager.AppSettings["Modules"];
            }
            else
            {
                if (Request.Url.Host.ToString().Contains("ecdb.io"))
                {
                    var clientid = Request.Url.Host.Replace("ecdb.io", "").TrimEnd('.');
                    if (String.IsNullOrEmpty(clientid))
                        Response.Redirect("https://earthcape.com");
                    //clientid = "demo";
                    GetConnectionString(clientid, out connectionString, out modules);
                }
                else
                {
                    Response.Redirect("https://earthcape.com");
                    //  GetConnectionString("demo", out connectionString, out modules);
                }
            }

#endif

            WebApplication.Instance.DatabaseUpdateMode = DatabaseUpdateMode.UpdateOldDatabase;
            WebApplication.Instance.Setup(WebApplication.Instance.ApplicationName, connectionString, modules.Split(';'));
            WebApplication.Instance.Start();
        }

        private void CaptionHelper_CustomizeConvertCompoundName(object sender, CustomizeConvertCompoundNameEventArgs e)
        {
            e.Handled = true;
        }
        private void Instance_CustomizeAuditTrailSettings1(object sender, CustomizeAuditTrailSettingsEventArgs e)
        {
            // e.AuditTrailSettings.Clear();
            e.AuditTrailSettings.RemoveType(typeof(ModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(RoleModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(UserModelDifferenceObject));
            e.AuditTrailSettings.RemoveType(typeof(AspectObject));
            e.AuditTrailSettings.RemoveType(typeof(Settings));
        }




        private void GetConnectionString(string clientid, out string connectionString, out string modules)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection("Data Source=WIN-RH2DEQT3EVP;Initial Catalog=ECDBAdmin1;User Id=admin;Password = Apollo%2018;"))
                {
                    conn.Open();
                    string sql = String.Format("select ConnectionString, Modules from PermissionPolicyUser where UserName='{0}'", clientid);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Read();
                    connectionString = rdr[0].ToString();
                    modules = rdr[1].ToString();
                    rdr.Close();
                }
            }
            finally
            {

            }
        }
        protected void Application_BeginRequest(Object sender, EventArgs e) {
        }
        protected void Application_EndRequest(Object sender, EventArgs e) {
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {
        }
        protected void Application_Error(Object sender, EventArgs e) {
            ErrorHandling.Instance.ProcessApplicationError();
        }
        protected void Session_End(Object sender, EventArgs e) {
            WebApplication.LogOff(Session);
            WebApplication.DisposeInstance(Session);
        }
        protected void Application_End(Object sender, EventArgs e) {
        }
#region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        }
#endregion
    }
}
