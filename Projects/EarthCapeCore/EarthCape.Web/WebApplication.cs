﻿using System;
using System.IO;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Security;
using Xpand.ExpressApp.Web;
using Xpand.Persistent.Base.General;
using Xpand.Persistent.BaseImpl.Security;
using Xpand.ExpressApp.Security.Core;
using Xpand.ExpressApp.Security.AuthenticationProviders;
using DevExpress.ExpressApp.DC;
using System.Linq;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Web;
using System.Reflection;

namespace EarthCape.Web {
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.Web.WebApplication
    public partial class EarthCapeAspNetApplication : XpandWebApplication
    {
        private DevExpress.ExpressApp.SystemModule.SystemModule module1;
        private DevExpress.ExpressApp.Web.SystemModule.SystemAspNetModule module2;
        private EarthCape.Module.Core.EarthCapeModule module3;
        private EarthCape.Module.Web.EarthCapeAspNetModule module4;
        private DevExpress.ExpressApp.Validation.ValidationModule validationModule1;
        private DevExpress.ExpressApp.Validation.Web.ValidationAspNetModule validationAspNetModule1;
        private DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule viewVariantsModule1;
        private DevExpress.ExpressApp.Maps.Web.MapsAspNetModule mapsAspNetModule1;
        private DevExpress.ExpressApp.TreeListEditors.TreeListEditorsModuleBase treeListEditorsModuleBase1;
        private DevExpress.ExpressApp.TreeListEditors.Web.TreeListEditorsAspNetModule treeListEditorsAspNetModule1;
        private DevExpress.ExpressApp.FileAttachments.Web.FileAttachmentsAspNetModule fileAttachmentsAspNetModule1;
        private DevExpress.ExpressApp.AuditTrail.AuditTrailModule auditTrailModule1;
        private DevExpress.ExpressApp.ReportsV2.ReportsModuleV2 reportsModuleV21;
        private DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2 reportsAspNetModuleV21;
        private DevExpress.ExpressApp.HtmlPropertyEditor.Web.HtmlPropertyEditorAspNetModule htmlPropertyEditorAspNetModule1;
        private DevExpress.ExpressApp.Chart.ChartModule chartModule1;
        private DevExpress.ExpressApp.Chart.Web.ChartAspNetModule chartAspNetModule1;
        private DevExpress.ExpressApp.PivotGrid.PivotGridModule pivotGridModule1;
        private DevExpress.ExpressApp.PivotGrid.Web.PivotGridAspNetModule pivotGridAspNetModule1;
        private DevExpress.ExpressApp.Security.SecurityModule securityModule1;
        private DevExpress.ExpressApp.CloneObject.CloneObjectModule cloneObjectModule1;
        private DevExpress.ExpressApp.Scheduler.SchedulerModuleBase schedulerModuleBase1;
        private DevExpress.ExpressApp.Scheduler.Web.SchedulerAspNetModule schedulerAspNetModule1;
        private DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule conditionalAppearanceModule1;
        private Xpand.XAF.Modules.CloneModelView.CloneModelViewModule cloneModelViewModule1;
        private Xpand.XAF.Modules.Reactive.ReactiveModule reactiveModule1;
        private Xpand.XAF.Modules.CloneMemberValue.CloneMemberValueModule cloneMemberValueModule1;
        private Xpand.XAF.Modules.HideToolBar.HideToolBarModule hideToolBarModule1;
        private Xpand.XAF.Modules.ProgressBarViewItem.ProgressBarViewItemModule progressBarViewItemModule1;
        private Xpand.XAF.Modules.RefreshView.RefreshViewModule refreshViewModule1;
        private Xpand.ExpressApp.SystemModule.XpandSystemModule xpandSystemModule1;
        private Xpand.XAF.Modules.ModelMapper.ModelMapperModule modelMapperModule1;
        private Xpand.ExpressApp.Web.SystemModule.XpandSystemAspNetModule xpandSystemAspNetModule1;
        private Xpand.XAF.Modules.ModelViewInheritance.ModelViewInheritanceModule modelViewInheritanceModule1;
        private Xpand.ExpressApp.Security.XpandSecurityModule xpandSecurityModule1;
        private Xpand.ExpressApp.Security.Web.XpandSecurityWebModule xpandSecurityWebModule1;
        private Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule xpandViewVariantsModule1;
        private Xpand.ExpressApp.ModelDifference.ModelDifferenceModule modelDifferenceModule1;
        private Xpand.ExpressApp.ModelDifference.Web.ModelDifferenceAspNetModule modelDifferenceAspNetModule1;
        private Xpand.ExpressApp.Logic.LogicModule logicModule1;
        private Xpand.ExpressApp.Email.EmailModule emailModule1;
        private DevExpress.ExpressApp.Notifications.NotificationsModule notificationsModule1;
        private Xpand.XAF.Modules.MasterDetail.MasterDetailModule masterDetailModule1;
        private Xpand.XAF.Modules.AutoCommit.AutoCommitModule autoCommitModule1;
        private Xpand.XAF.Modules.ViewEditMode.ViewEditModeModule viewEditModeModule1;
        private Xpand.XAF.Modules.SuppressConfirmation.SuppressConfirmationModule suppressConfirmationModule1;
        private Xpand.ExpressApp.ExcelImporter.ExcelImporterModule excelImporterModule1;
        private Xpand.ExpressApp.ExcelImporter.Web.ExcelImporterWebModule excelImporterWebModule1;
        private Xpand.ExpressApp.Dashboard.DashboardModule dashboardModule1;
        private Xpand.ExpressApp.XtraDashboard.Web.XtraDashboardWebModule xtraDashboardWebModule1;
        private Xpand.ExpressApp.ModelArtifactState.ModelArtifactStateModule modelArtifactStateModule1;
        private Xpand.ExpressApp.IO.IOModule ioModule1;
        private Xpand.ExpressApp.IO.Web.IOAspNetModule ioAspNetModule1;
        private Xpand.ExpressApp.NCarousel.Web.NCarouselWebModule nCarouselWebModule1;
        private LlamachantFramework.Module.LlamachantFrameworkModule llamachantFrameworkModule1;
        private LlamachantFramework.Module.Web.LlamachantFrameworkAspNetModule llamachantFrameworkAspNetModule1;
        private Module.GBIF.EarthCapeGBIFModule earthCapeGBIFModule1;
        private Module.EarthCapeBaseModule earthCapeBaseModule1;
        private Module.Lab.EarthCapeLabModule earthCapeLabModule1;
        private Module.Lab.Web.EarthCapeLabModuleWeb earthCapeLabModuleWeb1;
        private Xpand.XAF.Modules.Reactive.Logger.ReactiveLoggerModule reactiveLoggerModule1;
        private Xpand.ExpressApp.Validation.XpandValidationModule xpandValidationModule1;
        private DevExpress.ExpressApp.ScriptRecorder.ScriptRecorderModuleBase scriptRecorderModuleBase1;
        private DevExpress.ExpressApp.ScriptRecorder.Web.ScriptRecorderAspNetModule scriptRecorderAspNetModule1;
        private DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule objectsModule;

        #region Default XAF configuration options (https://www.devexpress.com/kb=T501418)
        static EarthCapeAspNetApplication() {
            EnableMultipleBrowserTabsSupport = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxGridListEditor.AllowFilterControlHierarchy = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxGridListEditor.MaxFilterControlHierarchyDepth = 2;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxCriteriaPropertyEditor.AllowFilterControlHierarchyDefault = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxCriteriaPropertyEditor.MaxHierarchyDepthDefault = 2;
            DevExpress.Persistent.Base.PasswordCryptographer.EnableRfc2898 = true;
            DevExpress.Persistent.Base.PasswordCryptographer.SupportLegacySha512 = true;
        }
        private void InitializeDefaults() {
            LinkNewObjectToParentImmediately = true;
            OptimizedControllersCreation = true;
            DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2.EnableValueManagerInHtml5DocumentViewer = true;
        }
        #endregion
        public EarthCapeAspNetApplication() {
            InitializeComponent();
			InitializeDefaults();
            this.NewSecurityStrategyComplexV2<XpandPermissionPolicyUser, XpandPermissionPolicyRole>(typeof(AuthenticationStandard), typeof(XpandLogonParameters));
        }
        protected override IViewUrlManager CreateViewUrlManager() {
            return new ViewUrlManager();
        }
        protected override void CreateDefaultObjectSpaceProvider(CreateCustomObjectSpaceProviderEventArgs args) {
            args.ObjectSpaceProvider = new XpandObjectSpaceProvider(new DataStoreProvider(args.ConnectionString), Security, true);
            // this breaks anon login and makes site sporadically redirect args.ObjectSpaceProvider=new SecuredObjectSpaceProvider((SecurityStrategyComplex)Security, new DataStoreProvider(args.ConnectionString), true);
            args.ObjectSpaceProviders.Add(new NonPersistentObjectSpaceProvider());
        }
        protected override string GetModelAssemblyFilePath()
        {
            return GetType().GetField("sharedModelManager", BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.Static)?.GetValue(this) == null
                ? Path.Combine(Path.GetTempPath(), $"{GetType().Name}{ModelAssemblyFileName}") : null;
        }
        private IXpoDataStoreProvider GetDataStoreProvider(string connectionString, System.Data.IDbConnection connection) {
            System.Web.HttpApplicationState application = (System.Web.HttpContext.Current != null) ? System.Web.HttpContext.Current.Application : null;
            IXpoDataStoreProvider dataStoreProvider = null;
            if(application != null && application["DataStoreProvider"] != null) {
                dataStoreProvider = application["DataStoreProvider"] as IXpoDataStoreProvider;
            }
            else {
                dataStoreProvider = XPObjectSpaceProvider.GetDataStoreProvider(connectionString, connection, true);
                if(application != null) {
                    application["DataStoreProvider"] = dataStoreProvider;
                }
            }
			return dataStoreProvider;
        }
        private void EarthCapeAspNetApplication_DatabaseVersionMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e) {
            e.Updater.Update();
            e.Handled = true;
        }
        private void InitializeComponent() {
            this.module1 = new DevExpress.ExpressApp.SystemModule.SystemModule();
            this.module2 = new DevExpress.ExpressApp.Web.SystemModule.SystemAspNetModule();
            this.module3 = new EarthCape.Module.Core.EarthCapeModule();
            this.module4 = new EarthCape.Module.Web.EarthCapeAspNetModule();
            this.objectsModule = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
            this.validationModule1 = new DevExpress.ExpressApp.Validation.ValidationModule();
            this.validationAspNetModule1 = new DevExpress.ExpressApp.Validation.Web.ValidationAspNetModule();
            this.viewVariantsModule1 = new DevExpress.ExpressApp.ViewVariantsModule.ViewVariantsModule();
            this.mapsAspNetModule1 = new DevExpress.ExpressApp.Maps.Web.MapsAspNetModule();
            this.treeListEditorsModuleBase1 = new DevExpress.ExpressApp.TreeListEditors.TreeListEditorsModuleBase();
            this.treeListEditorsAspNetModule1 = new DevExpress.ExpressApp.TreeListEditors.Web.TreeListEditorsAspNetModule();
            this.fileAttachmentsAspNetModule1 = new DevExpress.ExpressApp.FileAttachments.Web.FileAttachmentsAspNetModule();
            this.auditTrailModule1 = new DevExpress.ExpressApp.AuditTrail.AuditTrailModule();
            this.reportsModuleV21 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
            this.reportsAspNetModuleV21 = new DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2();
            this.htmlPropertyEditorAspNetModule1 = new DevExpress.ExpressApp.HtmlPropertyEditor.Web.HtmlPropertyEditorAspNetModule();
            this.chartModule1 = new DevExpress.ExpressApp.Chart.ChartModule();
            this.chartAspNetModule1 = new DevExpress.ExpressApp.Chart.Web.ChartAspNetModule();
            this.pivotGridModule1 = new DevExpress.ExpressApp.PivotGrid.PivotGridModule();
            this.pivotGridAspNetModule1 = new DevExpress.ExpressApp.PivotGrid.Web.PivotGridAspNetModule();
            this.securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
            this.cloneObjectModule1 = new DevExpress.ExpressApp.CloneObject.CloneObjectModule();
            this.schedulerModuleBase1 = new DevExpress.ExpressApp.Scheduler.SchedulerModuleBase();
            this.schedulerAspNetModule1 = new DevExpress.ExpressApp.Scheduler.Web.SchedulerAspNetModule();
            this.conditionalAppearanceModule1 = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
            this.cloneModelViewModule1 = new Xpand.XAF.Modules.CloneModelView.CloneModelViewModule();
            this.reactiveModule1 = new Xpand.XAF.Modules.Reactive.ReactiveModule();
            this.cloneMemberValueModule1 = new Xpand.XAF.Modules.CloneMemberValue.CloneMemberValueModule();
            this.hideToolBarModule1 = new Xpand.XAF.Modules.HideToolBar.HideToolBarModule();
            this.progressBarViewItemModule1 = new Xpand.XAF.Modules.ProgressBarViewItem.ProgressBarViewItemModule();
            this.refreshViewModule1 = new Xpand.XAF.Modules.RefreshView.RefreshViewModule();
            this.xpandSystemModule1 = new Xpand.ExpressApp.SystemModule.XpandSystemModule();
            this.modelMapperModule1 = new Xpand.XAF.Modules.ModelMapper.ModelMapperModule();
            this.xpandSystemAspNetModule1 = new Xpand.ExpressApp.Web.SystemModule.XpandSystemAspNetModule();
            this.modelViewInheritanceModule1 = new Xpand.XAF.Modules.ModelViewInheritance.ModelViewInheritanceModule();
            this.xpandSecurityModule1 = new Xpand.ExpressApp.Security.XpandSecurityModule();
            this.xpandSecurityWebModule1 = new Xpand.ExpressApp.Security.Web.XpandSecurityWebModule();
            this.xpandViewVariantsModule1 = new Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule();
            this.modelDifferenceModule1 = new Xpand.ExpressApp.ModelDifference.ModelDifferenceModule();
            this.modelDifferenceAspNetModule1 = new Xpand.ExpressApp.ModelDifference.Web.ModelDifferenceAspNetModule();
            this.logicModule1 = new Xpand.ExpressApp.Logic.LogicModule();
            this.emailModule1 = new Xpand.ExpressApp.Email.EmailModule();
            this.notificationsModule1 = new DevExpress.ExpressApp.Notifications.NotificationsModule();
            this.masterDetailModule1 = new Xpand.XAF.Modules.MasterDetail.MasterDetailModule();
            this.autoCommitModule1 = new Xpand.XAF.Modules.AutoCommit.AutoCommitModule();
            this.viewEditModeModule1 = new Xpand.XAF.Modules.ViewEditMode.ViewEditModeModule();
            this.suppressConfirmationModule1 = new Xpand.XAF.Modules.SuppressConfirmation.SuppressConfirmationModule();
            this.excelImporterModule1 = new Xpand.ExpressApp.ExcelImporter.ExcelImporterModule();
            this.excelImporterWebModule1 = new Xpand.ExpressApp.ExcelImporter.Web.ExcelImporterWebModule();
            this.dashboardModule1 = new Xpand.ExpressApp.Dashboard.DashboardModule();
            this.xtraDashboardWebModule1 = new Xpand.ExpressApp.XtraDashboard.Web.XtraDashboardWebModule();
            this.modelArtifactStateModule1 = new Xpand.ExpressApp.ModelArtifactState.ModelArtifactStateModule();
            this.ioModule1 = new Xpand.ExpressApp.IO.IOModule();
            this.ioAspNetModule1 = new Xpand.ExpressApp.IO.Web.IOAspNetModule();
            this.nCarouselWebModule1 = new Xpand.ExpressApp.NCarousel.Web.NCarouselWebModule();
            this.llamachantFrameworkModule1 = new LlamachantFramework.Module.LlamachantFrameworkModule();
            this.llamachantFrameworkAspNetModule1 = new LlamachantFramework.Module.Web.LlamachantFrameworkAspNetModule();
            this.earthCapeGBIFModule1 = new EarthCape.Module.GBIF.EarthCapeGBIFModule();
            this.earthCapeBaseModule1 = new EarthCape.Module.EarthCapeBaseModule();
            this.earthCapeLabModule1 = new EarthCape.Module.Lab.EarthCapeLabModule();
            this.earthCapeLabModuleWeb1 = new EarthCape.Module.Lab.Web.EarthCapeLabModuleWeb();
            this.reactiveLoggerModule1 = new Xpand.XAF.Modules.Reactive.Logger.ReactiveLoggerModule();
            this.xpandValidationModule1 = new Xpand.ExpressApp.Validation.XpandValidationModule();
            this.scriptRecorderModuleBase1 = new DevExpress.ExpressApp.ScriptRecorder.ScriptRecorderModuleBase();
            this.scriptRecorderAspNetModule1 = new DevExpress.ExpressApp.ScriptRecorder.Web.ScriptRecorderAspNetModule();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // validationModule1
            // 
            this.validationModule1.AllowValidationDetailsAccess = true;
            this.validationModule1.IgnoreWarningAndInformationRules = false;
            // 
            // mapsAspNetModule1
            // 
            this.mapsAspNetModule1.GoogleApiKey = "AIzaSyB_DL6lff3r_C-aJ4d4bpMl2qGckXs6TRw";
            // 
            // auditTrailModule1
            // 
            this.auditTrailModule1.AuditDataItemPersistentType = typeof(DevExpress.Persistent.BaseImpl.AuditDataItemPersistent);
            // 
            // reportsModuleV21
            // 
            this.reportsModuleV21.EnableInplaceReports = true;
            this.reportsModuleV21.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
            this.reportsModuleV21.ReportStoreMode = DevExpress.ExpressApp.ReportsV2.ReportStoreModes.XML;
            // 
            // cloneObjectModule1
            // 
            this.cloneObjectModule1.ClonerType = null;
            // 
            // notificationsModule1
            // 
            this.notificationsModule1.CanAccessPostponedItems = false;
            this.notificationsModule1.NotificationsRefreshInterval = System.TimeSpan.Parse("00:05:00");
            this.notificationsModule1.NotificationsStartDelay = System.TimeSpan.Parse("00:00:05");
            this.notificationsModule1.ShowDismissAllAction = false;
            this.notificationsModule1.ShowNotificationsWindow = true;
            this.notificationsModule1.ShowRefreshAction = false;
            // 
            // EarthCapeAspNetApplication
            // 
            this.ApplicationName = "EarthCape";
            this.CheckCompatibilityType = DevExpress.ExpressApp.CheckCompatibilityType.DatabaseSchema;
            this.Modules.Add(this.module1);
            this.Modules.Add(this.module2);
            this.Modules.Add(this.reactiveModule1);
            this.Modules.Add(this.module3);
            this.Modules.Add(this.validationModule1);
            this.Modules.Add(this.validationAspNetModule1);
            this.Modules.Add(this.viewVariantsModule1);
            this.Modules.Add(this.mapsAspNetModule1);
            this.Modules.Add(this.treeListEditorsModuleBase1);
            this.Modules.Add(this.treeListEditorsAspNetModule1);
            this.Modules.Add(this.fileAttachmentsAspNetModule1);
            this.Modules.Add(this.auditTrailModule1);
            this.Modules.Add(this.reportsModuleV21);
            this.Modules.Add(this.reportsAspNetModuleV21);
            this.Modules.Add(this.htmlPropertyEditorAspNetModule1);
            this.Modules.Add(this.chartModule1);
            this.Modules.Add(this.chartAspNetModule1);
            this.Modules.Add(this.pivotGridModule1);
            this.Modules.Add(this.pivotGridAspNetModule1);
            this.Modules.Add(this.securityModule1);
            this.Modules.Add(this.cloneObjectModule1);
            this.Modules.Add(this.schedulerModuleBase1);
            this.Modules.Add(this.schedulerAspNetModule1);
            this.Modules.Add(this.conditionalAppearanceModule1);
            this.Modules.Add(this.cloneModelViewModule1);
            this.Modules.Add(this.cloneMemberValueModule1);
            this.Modules.Add(this.hideToolBarModule1);
            this.Modules.Add(this.progressBarViewItemModule1);
            this.Modules.Add(this.refreshViewModule1);
            this.Modules.Add(this.reactiveLoggerModule1);
            this.Modules.Add(this.xpandSystemModule1);
            this.Modules.Add(this.modelMapperModule1);
            this.Modules.Add(this.xpandSystemAspNetModule1);
            this.Modules.Add(this.modelViewInheritanceModule1);
            this.Modules.Add(this.xpandSecurityModule1);
            this.Modules.Add(this.xpandSecurityWebModule1);
            this.Modules.Add(this.xpandViewVariantsModule1);
            this.Modules.Add(this.modelDifferenceModule1);
            this.Modules.Add(this.modelDifferenceAspNetModule1);
            this.Modules.Add(this.logicModule1);
            this.Modules.Add(this.xpandValidationModule1);
            this.Modules.Add(this.emailModule1);
            this.Modules.Add(this.notificationsModule1);
            this.Modules.Add(this.masterDetailModule1);
            this.Modules.Add(this.autoCommitModule1);
            this.Modules.Add(this.viewEditModeModule1);
            this.Modules.Add(this.suppressConfirmationModule1);
            this.Modules.Add(this.excelImporterModule1);
            this.Modules.Add(this.excelImporterWebModule1);
            this.Modules.Add(this.dashboardModule1);
            this.Modules.Add(this.xtraDashboardWebModule1);
            this.Modules.Add(this.modelArtifactStateModule1);
            this.Modules.Add(this.ioModule1);
            this.Modules.Add(this.ioAspNetModule1);
            this.Modules.Add(this.nCarouselWebModule1);
            this.Modules.Add(this.objectsModule);
            this.Modules.Add(this.llamachantFrameworkModule1);
            this.Modules.Add(this.llamachantFrameworkAspNetModule1);
            this.Modules.Add(this.earthCapeGBIFModule1);
            this.Modules.Add(this.earthCapeBaseModule1);
            this.Modules.Add(this.earthCapeLabModule1);
            this.Modules.Add(this.earthCapeLabModuleWeb1);
            this.Modules.Add(this.scriptRecorderModuleBase1);
            this.Modules.Add(this.scriptRecorderAspNetModule1);
            this.Modules.Add(this.module4);
            this.DatabaseVersionMismatch += new System.EventHandler<DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs>(this.EarthCapeAspNetApplication_DatabaseVersionMismatch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
    }
}
