using System;
using System.Collections.Generic;
using System.Text;
using EarthCape.Module.Core;
using DevExpress.Xpo;

namespace EarthCape.Module.Lab
{
    public interface ISequenceObject
    {
        string Sequence{ get; set; }
    
    }
    
}
