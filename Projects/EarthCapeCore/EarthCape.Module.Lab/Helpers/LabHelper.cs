﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarthCape.Module.Lab.Helpers
{
    public class LabHelper
    {

        public static void ArrangePlate(IObjectSpace os, SequencingRun plate, bool forward, bool reverse, bool remove, string toremove)
        {
            XPCollection<PcrDnaExtractSequenced> coll = new XPCollection<PcrDnaExtractSequenced>(((XPObjectSpace)os).Session, new BinaryOperator("SequencingRun.Oid", plate.Oid));
            coll.Sorting = new SortingCollection(coll,
                new SortProperty("PcrDnaExtract.DnaExtract.Code", SortingDirection.Ascending),
                new SortProperty("PcrDnaExtract.PrimerForward.Gene.Name", SortingDirection.Ascending),
                new SortProperty("PcrDnaExtract.PrimerForward.Name", SortingDirection.Ascending),
                new SortProperty("PcrDnaExtract.PrimerReverse.Name", SortingDirection.Ascending)
                );
            coll.Load();
            string rows = "ABCDEFGH";
            //   var _plate = new string[8, 12];
            int x = 0; int y = 0;
            int rowcheck = 0;
            // string line = "";
            foreach (PcrDnaExtractSequenced sample in coll)
            {
                sample.PlatePosF = string.Format("{0}:{1}", rows[x], (y + 1).ToString());
                sample.PlatePosR = string.Format("{0}:{1}", rows[x], (y + 2).ToString());
                x = x + 1;
                //   line =line + "\t" + sample.PcrDnaExtract.DnaExtract.Code + " " + rows[x] + " " + (y + 1).ToString();
                if (rowcheck == 7)
                {
                    y = y + 2;
                    rowcheck = 0;
                    x = 0;
                    //      plate.Comment = plate.Comment + Environment.NewLine + line;
                    //      line = "";
                }
                else
                    rowcheck = rowcheck + 1;
                sample.Save();
            }
            plate.Save();
            coll.Sorting = new SortingCollection(coll, new SortProperty("PlatePosF", SortingDirection.Ascending));
            coll.Load();
            string r = "A";
            string l = "";
            plate.Plate = "";
            foreach (PcrDnaExtractSequenced sample in coll)
            {
                if (sample.PcrDnaExtract == null) throw new UserFriendlyException("Sample " + sample.FullName + " missing pcr product link!");
                if (forward && (sample.PcrDnaExtract.PrimerForward == null)) throw new UserFriendlyException("Sample " + sample.FullName + " missing pcr product forward primer!");
                if (reverse && (sample.PcrDnaExtract.PrimerReverse == null)) throw new UserFriendlyException("Sample " + sample.FullName + " missing pcr product reverse primer!");
                if (r == sample.PlatePosF.Substring(0, 1))
                {
                    string label =sample.PcrDnaExtract.DnaExtract.Name + "_" + sample.PcrDnaExtract.PrimerForward.Name + "\t" + sample.PcrDnaExtract.DnaExtract.Name + "_" + sample.PcrDnaExtract.PrimerReverse.Name;
                    l = (!String.IsNullOrEmpty(l)) ? l + "\t" + label : label;
                }
                else
                {
                    plate.Plate = (!String.IsNullOrEmpty(plate.Plate)) ? (plate.Plate + Environment.NewLine + l) : l;
                    l = sample.PcrDnaExtract.DnaExtract.Name + "_" + sample.PcrDnaExtract.PrimerForward.Name + "\t" + sample.PcrDnaExtract.DnaExtract.Name + "_" + sample.PcrDnaExtract.PrimerReverse.Name;
                }
                r = sample.PlatePosF.Substring(0, 1);
            }
            plate.Plate = (!String.IsNullOrEmpty(plate.Plate)) ? (plate.Plate + Environment.NewLine + l) : l;
            if (remove) plate.Plate = new string(plate.Plate.Where(c => !toremove.Contains(c)).ToArray());
            plate.Save();
            // os.CommitChanges();
        }
    }
}
