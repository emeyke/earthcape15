using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class SequencingSampleBatchUpdate : XPCustomObject
    {
        public SequencingSampleBatchUpdate(Session session) : base(session) { }
        SequencingResult? result;
        public SequencingResult? Result
        {
            get => result;
            set => SetPropertyValue(nameof(Result), ref result, value);
        }
        private string _ResultQuality;
        public string ResultQuality
        {
            get
            {
                return _ResultQuality;
            }
            set
            {
                SetPropertyValue("ResultQuality", ref _ResultQuality, value);
            }
        }
        bool? sequenceChecked;
        public bool? SequenceChecked
        {
            get => sequenceChecked;
            set => SetPropertyValue(nameof(SequenceChecked), ref sequenceChecked, value);
        }
    }

}
