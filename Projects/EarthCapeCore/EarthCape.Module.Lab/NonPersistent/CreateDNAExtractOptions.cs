using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;
using Xpand.Xpo.Converters.ValueConverters;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class CreateDnaExtrOptions : XPCustomObject
    {
        public CreateDnaExtrOptions(Session session) : base(session) { }
        bool addToNewAmplification=false;
        ProtocolPCR protocol;
        string suffix = "_d";
        private Project _OwnerGroup;
        public Project OwnerGroup
        {
            get
            {
                return _OwnerGroup;
            }
            set
            {
                SetPropertyValue("OwnerGroup", ref _OwnerGroup, value);
            }
        }
        
        public bool AddToNewAmplification
        {
            get => addToNewAmplification;
            set => SetPropertyValue(nameof(AddToNewAmplification), ref addToNewAmplification, value);
        }
        public ProtocolPCR Protocol
        {
            get => protocol;
            set => SetPropertyValue(nameof(Protocol), ref protocol, value);
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Suffix
        {
            get => suffix;
            set => SetPropertyValue(nameof(Suffix), ref suffix, value);
        }
        private int _ExtractsPerSample = 1;
        public int ExtractsPerSample
        {
            get
            {
                return _ExtractsPerSample;
            }
            set
            {
                SetPropertyValue("ExtractsPerSample", ref _ExtractsPerSample, value);
            }
        }
     
        private Store _Store;
        public Store Store
        {
            get
            {
                return _Store;
            }
            set
            {
                SetPropertyValue("Store", ref _Store, value);
            }
        }
    
        private double _DnaAmountInitialMl;
        public double DnaAmountInitialMl
        {
            get
            {
                return _DnaAmountInitialMl;
            }
            set
            {
                SetPropertyValue("DnaAmountInitialMl", ref _DnaAmountInitialMl, value);
            }
        }
        private double _DnaAmountLeftMl;
        public double DnaAmountLeftMl
        {
            get
            {
                return _DnaAmountLeftMl;
            }
            set
            {
                SetPropertyValue("DnaAmountLeftMl", ref _DnaAmountLeftMl, value);
            }
        }
       
        private string _Preservation;
        public string Preservation
        {
            get
            {
                return _Preservation;
            }
            set
            {
                SetPropertyValue("Preservation", ref _Preservation, value);
            }
        }
        private string _DoneBy;
        public string DoneBy
        {
            get
            {
                return _DoneBy;
            }
            set
            {
                SetPropertyValue("DoneBy", ref _DoneBy, value);
            }
        }
        private string _ExtractionMethod;
        public string ExtractionMethod
        {
            get
            {
                return _ExtractionMethod;
            }
            set
            {
                SetPropertyValue("ExtractionMethod", ref _ExtractionMethod, value);
            }
        }
        private string _PurificationMethod;
        public string PurificationMethod
        {
            get
            {
                return _PurificationMethod;
            }
            set
            {
                SetPropertyValue("PurificationMethod", ref _PurificationMethod, value);
            }
        }
        private string _RatioOfAbsorbance260_280;
        public string RatioOfAbsorbance260_280
        {
            get
            {
                return _RatioOfAbsorbance260_280;
            }
            set
            {
                SetPropertyValue("RatioOfAbsorbance260_280", ref _RatioOfAbsorbance260_280, value);
            }
        }
        private string _RatioOfAbsorbance260_230;
        public string RatioOfAbsorbance260_230
        {
            get
            {
                return _RatioOfAbsorbance260_230;
            }
            set
            {
                SetPropertyValue("RatioOfAbsorbance260_230", ref _RatioOfAbsorbance260_230, value);
            }
        }
        private string _Concentration;
        public string Concentration
        {
            get
            {
                return _Concentration;
            }
            set
            {
                SetPropertyValue("Concentration", ref _Concentration, value);
            }
        }
        private string _Quality;
        public string Quality
        {
            get
            {
                return _Quality;
            }
            set
            {
                SetPropertyValue("Quality", ref _Quality, value);
            }
        }
        private DateTime _QualityCheckDate;
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        public DateTime QualityCheckDate
        {
            get
            {
                return _QualityCheckDate;
            }
            set
            {
                SetPropertyValue("QualityCheckDate", ref _QualityCheckDate, value);
            }
        }
        private string _ProvidedBy;
        public string ProvidedBy
        {
            get
            {
                return _ProvidedBy;
            }
            set
            {
                SetPropertyValue("ProvidedBy", ref _ProvidedBy, value);
            }
        }
        private DateTime? _ExtractionDate=DateTime.Now;
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        public DateTime? ExtractionDate
        {
            get
            {
                return _ExtractionDate;
            }
            set
            {
                SetPropertyValue("ExtractionDate", ref _ExtractionDate, value);
            }
        }
    }

}
