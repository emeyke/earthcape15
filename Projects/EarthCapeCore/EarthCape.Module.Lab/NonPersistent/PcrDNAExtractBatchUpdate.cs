using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class PcrDnaExtrBatchUpdate : XPCustomObject
    {
        public PcrDnaExtrBatchUpdate(Session session) : base(session) { }
        private AmplificationResult? _Result;
        public AmplificationResult? Result
        {
            get
            {
                return _Result;
            }
            set
            {
                SetPropertyValue("Result", ref _Result, value);
            }
        }
        private Primer _PrimerForward;
        public Primer PrimerForward
        {
            get
            {
                return _PrimerForward;
            }
            set
            {
                SetPropertyValue("PrimerForward", ref _PrimerForward, value);
            }
        }
        private Primer _PrimerReverse;
        public Primer PrimerReverse
        {
            get
            {
                return _PrimerReverse;
            }
            set
            {
                SetPropertyValue("PrimerReverse", ref _PrimerReverse, value);
            }
        }

        private double? _Concentration;
        public double? Concentration
        {
            get { return _Concentration; }
            set { SetPropertyValue<double?>(nameof(Concentration), ref _Concentration, value); }
        }


        private double? _Dilution;
        public double? Dilution
        {
            get { return _Dilution; }
            set { SetPropertyValue<double?>(nameof(Dilution), ref _Dilution, value); }
        }



        //private Gene _Gene;
        /*   public Gene Gene
           {
               get
               {
                   return _Gene;
               }
               set
               {
                   SetPropertyValue("Gene", ref _Gene, value);
               }
           }*/
    }

}
