using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;
using Xpand.Xpo.Converters.ValueConverters;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class ArrangePlateOptions : XPCustomObject
    {
        public ArrangePlateOptions(Session session) : base(session) { }

        bool forward = true;
        public bool Forward
        {
            get => forward;
            set => SetPropertyValue(nameof(Forward), ref forward, value);
        }
        bool reverse=true;
        public bool Reverse
        {
            get => reverse;
            set => SetPropertyValue(nameof(Reverse), ref reverse, value);
        }

        private bool _RemoveCharacters=false;
        public bool RemoveCharacters
        {
            get { return _RemoveCharacters; }
            set { SetPropertyValue<bool>(nameof(RemoveCharacters), ref _RemoveCharacters, value); }
        }

        private string _CharactersToRemove= "#!?/; ";
        public string CharactersToRemove
        {
            get { return _CharactersToRemove; }
            set { SetPropertyValue<string>(nameof(CharactersToRemove), ref _CharactersToRemove, value); }
        }






    }

}
