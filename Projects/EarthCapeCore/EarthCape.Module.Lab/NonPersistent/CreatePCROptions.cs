using System;

using DevExpress.Xpo;
using EarthCape.Module;
using EarthCape.Genetics;


namespace EarthCape.Genetics
{
   
    [NonPersistent]
    public class CreatePCROptions : XPCustomObject
    {
        public CreatePCROptions(Session session) : base(session) { }
        private ProtocolPCR _ProtocolPCR;
        public ProtocolPCR ProtocolPCR
        {
            get
            {
                return _ProtocolPCR;
            }
            set
            {
                SetPropertyValue("ProtocolPCR", ref _ProtocolPCR, value);
            }
        }
        private CreateDNAExtractOptions _CreateExtracts;
        public CreateDNAExtractOptions CreateExtracts
        {
            get
            {
                return _CreateExtracts;
            }
            set
            {
                SetPropertyValue("CreateExtracts", ref _CreateExtracts, value);
            }
        }
        private int _NumberOfSamples;
        public int NumberOfSamples
        {
            get
            {
                return _NumberOfSamples;
            }
            set
            {
                SetPropertyValue("NumberOfSamples", ref _NumberOfSamples, value);
            }
        }
        private XPCollection<Unit> _Samples;
        public XPCollection<Unit> Samples
        {
            get
            {
                return _Samples;
            }
            set
            {
                SetPropertyValue("Samples", ref _Samples, value);
            }
        }
       
        
    
    }

}
