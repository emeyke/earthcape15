using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;
using Xpand.Xpo.Converters.ValueConverters;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class AddToSequencingOptions : XPCustomObject
    {
        public AddToSequencingOptions(Session session) : base(session) { }
        SequencingRun sequencingRun;
        public SequencingRun SequencingRun
        {
            get => sequencingRun;
            set => SetPropertyValue(nameof(SequencingRun), ref sequencingRun, value);
        }
        bool forward=true;
        public bool Forward
        {
            get => forward;
            set => SetPropertyValue(nameof(Forward), ref forward, value);
        }
        bool reverse=true;
        public bool Reverse
        {
            get => reverse;
            set => SetPropertyValue(nameof(Reverse), ref reverse, value);
        }
    }

}
