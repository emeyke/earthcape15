using System;

using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Lab;
using Xpand.Xpo.Converters.ValueConverters;


namespace EarthCape.Module.Lab
{
   
    [NonPersistent]
    public class AddToPCROptions : XPCustomObject
    {
        public AddToPCROptions(Session session) : base(session) { }
        Amplification amplification;
        public Amplification Amplification
        {
            get => amplification;
            set => SetPropertyValue(nameof(Amplification), ref amplification, value);
        }
      
    }

}
