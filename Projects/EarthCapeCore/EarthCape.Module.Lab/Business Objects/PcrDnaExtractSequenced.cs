using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    public class PcrDnaExtractSequenced : BaseObject
    {
        public PcrDnaExtractSequenced(Session session)
             : base(session)
        {
        }
        ///    PlatePosition platePositionForward;
        ///    PlatePosition platePositionReverse;

        private string _PlatePosF;
        public string PlatePosF
        {
            get { return _PlatePosF; }
            set { SetPropertyValue<string>(nameof(PlatePosF), ref _PlatePosF, value); }
        }
        private string _PlatePosR;
        public string PlatePosR
        {
            get { return _PlatePosR; }
            set { SetPropertyValue<string>(nameof(PlatePosR), ref _PlatePosR, value); }
        }



        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(500)]
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref _FullName, value);
            }

        }

        int dnaLength;
        public int DnaLength
        {
            get => dnaLength;
            set => SetPropertyValue(nameof(DnaLength), ref dnaLength, value);
        }
        string customerNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerNumber
        {
            get => customerNumber;
            set => SetPropertyValue(nameof(CustomerNumber), ref customerNumber, value);
        }
        /*  PlatePosition _PlatePositionForward = null;
          [ImmediatePostData(true)]
          public PlatePosition PlatePositionForward
          {
              get { return _PlatePositionForward; }
              set
              {
                  if (_PlatePositionForward == value)
                      return;

                  // Store a reference to the former _PlatePositionForward. 
                  PlatePosition prevOwner = _PlatePositionForward;
                  _PlatePositionForward = value;

                  if (IsLoading) return;

                  // Remove an _PlatePositionForward's reference to this building, if exists. 
                  if (prevOwner != null && prevOwner.SampleForward == this)
                      prevOwner.SampleForward = null;

                  // Specify that the building is a new _PlatePositionForward's house. 
                  if (_PlatePositionForward != null)
                      _PlatePositionForward.SampleForward = this;
                  OnChanged("PlatePositionForward");
              }
          }*/
        
      /*  public PlatePosition PlatePositionForward
        {
            get => platePositionForward;
            set => SetPropertyValue(nameof(PlatePositionForward), ref platePositionForward, value);
        }
        public PlatePosition PlatePositionReverse
        {
            get => platePositionReverse;
            set => SetPropertyValue(nameof(PlatePositionReverse), ref platePositionReverse, value);
        }*/
        /*PlatePosition _PlatePositionReverse = null;
        [ImmediatePostData(true)]
        public PlatePosition PlatePositionReverse
        {
            get { return _PlatePositionReverse; }
            set
            {
                if (_PlatePositionReverse == value)
                    return;

                // Store a reference to the former _PlatePositionReverse. 
                PlatePosition prevOwner = _PlatePositionReverse;
                _PlatePositionReverse = value;

                if (IsLoading) return;

                // Remove an _PlatePositionReverse's reference to this building, if exists. 
                if (prevOwner != null && prevOwner.SampleReverse == this)
                    prevOwner.SampleReverse = null;

                // Specify that the building is a new _PlatePositionReverse's house. 
                if (_PlatePositionReverse != null)
                    _PlatePositionReverse.SampleReverse = this;
                OnChanged("PlatePositionReverse");
            }
        }*/
      
      
        private PcrDnaExtract _PcrDnaExtract;
           [Association("PcrDnaExtract-PcrDnaExtractsSequenced")]
        [RuleRequiredField("RuleRequiredField for PcrDnaExtractSequencing.PcrDnaExtract", DefaultContexts.Save, "DNA extract amplified (PCRDnaExtract) property must not be empty.")]
        public PcrDnaExtract PcrDnaExtract
        {
            get
            {
                return _PcrDnaExtract;
            }
            set
            {
                SetPropertyValue("Result", ref _PcrDnaExtract, value);
            }
        }
        bool sequenceChecked;
        public bool SequenceChecked
        {
            get => sequenceChecked;
            set => SetPropertyValue(nameof(SequenceChecked), ref sequenceChecked, value);
        }
        SequencingResult? resultForward;
        public SequencingResult? ResultForward
        {
            get => resultForward;
            set => SetPropertyValue(nameof(ResultForward), ref resultForward, value);
        }
        SequencingResult? resultReverse;
        public SequencingResult? ResultReverse
        {
            get => resultReverse;
            set => SetPropertyValue(nameof(ResultReverse), ref resultReverse, value);
        }
        SequencingResult? result;
        public SequencingResult? Result
        {
            get => result;
            set => SetPropertyValue(nameof(Result), ref result, value);
        }

        private SequencingRun _SequencingRun;
 //       [ImmediatePostData(true)]
        [Association("SequencingRun-SequencingRunDnaExtrs")]
     //   [RuleRequiredField("RuleRequiredField for SequencingRunDnaExtract.SequencingRun", DefaultContexts.Save, "SequencingRun property must not be empty.")]
        public SequencingRun SequencingRun
        {
            get
            {
                return _SequencingRun;
            }
            set
            {
                SetPropertyValue("Result", ref _SequencingRun, value);
            }
        }
        /*  private SequencingResult? _Result;
          public SequencingResult? Result
          {
              get
              {
                  return _Result;
              }
              set
              {
                  SetPropertyValue("Result", ref _Result, value);
              }
          }
       */
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
        private string _Genbank;
        public string Genbank
        {
            get
            {
                return _Genbank;
            }
            set
            {
                SetPropertyValue("Genbank", ref _Genbank, value);
            }
        }
        private string _ResultQuality;
        public string ResultQuality
        {
            get
            {
                return _ResultQuality;
            }
            set
            {
                SetPropertyValue("ResultQuality", ref _ResultQuality, value);
            }
        }
        protected override void OnDeleting()
        {
           // Session.Delete(this.PlatePositionForward);
           // Session.Delete(this.PlatePositionReverse);
            base.OnDeleting();
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
            if (IsDeleted) return;
         /*   if (PlatePositionForward == null)
            {
                PlatePositionForward = new PlatePosition(Session);
             //   PlatePositionForward.SequencingRun = _SequencingRun;
                PlatePositionForward.Save();
            }
            if (PlatePositionReverse == null)
            {
                PlatePositionReverse = new PlatePosition(Session);
              //  PlatePositionReverse.SequencingRun = _SequencingRun;
                PlatePositionReverse.Save();
            }*/
            /* if (Position < 1)
              {
                  //Pcr.DnaExtrs.Load();
                  Position = Pcr.DnaExtrs.Count+1;
              }*/
         /*   if ((PcrDnaExtract != null) && (PlatePositionForward != null) && (PlatePositionReverse != null))
                FullName = String.Format("{0}: {1},F:{2}{3}, R:{4}{5}", PcrDnaExtract.DnaExtract.Code, Result, PlatePositionForward.Row, PlatePositionForward.Col, PlatePositionReverse.Row, PlatePositionReverse.Col);
            else*/
                 if ((PcrDnaExtract != null))
                FullName = String.Format("{0}: {1}", PcrDnaExtract.DnaExtract.Code, Result);
            else
                FullName = "";
            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


    }


}
