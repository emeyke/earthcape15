using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;
using System.ComponentModel;


namespace EarthCape.Module.Lab
{
    //[DefaultClassOptions]
    [DefaultProperty("Code")]
    public class UnitGenotyping : BaseObject, ICode
    {
        private Genotyping _Genotyping;
        private string _Code;
        private string _Well;
        private string _Plate;
        private string _Genotyper;
        private string _Comment;
        private Unit _Unit;
 
            protected override void OnSaving()
            {
                base.OnSaving();
                if (Session.IsNewObject(this))
                {
                    if (String.IsNullOrEmpty(Code))
                    {
                        Code = string.Format("{0}{1}{2}", (Unit != null) ? Unit.UnitID : "", Plate, Well);
                     }
                }
            }
            [VisibleInListView(false), VisibleInDetailView(false)]
            [Size(SizeAttribute.Unlimited)]
            public string Comment
            {
                get
                {
                    return _Comment;
                }
                set
                {
                    SetPropertyValue("Comment", ref _Comment, value);
                }

            }
        public UnitGenotyping(Session session) : base(session) { }

        [Association("Genotyping-GenotypedUnits")]
        public Genotyping Genotyping
        {
            get
            {
                return _Genotyping;
            }
            set
            {
                SetPropertyValue("Genotyping", ref _Genotyping, value);
            }
        }
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                SetPropertyValue("Code", ref _Code, value);
            }
        }
        [ProvidedAssociation("UnitGenotyping-Unit", "Genotyping", RelationType.OneToMany, "Aggregated")]
        public Unit Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetPropertyValue("Unit", ref _Unit, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Genotyper
        {
            get
            {
                return _Genotyper;
            }
            set
            {
                SetPropertyValue("Genotyper", ref _Genotyper, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Plate
        {
            get
            {
                return _Plate;
            }
            set
            {
                SetPropertyValue("Plate", ref _Plate, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Well
        {
            get
            {
                return _Well;
            }
            set
            {
                SetPropertyValue("Well", ref _Well, value);
            }
        }
        [Association("UnitGenotyping-Values"), Aggregated]
        public XPCollection<UnitGenotypingValue> Values
        {
            get
            {
                return GetCollection<UnitGenotypingValue>("Values");
            }
        }
    }


}
