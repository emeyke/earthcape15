using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp.Xpo;


namespace EarthCape.Module.Lab
{
    //  [DefaultClassOptions]
    [VisibleInReports(true)]
    public class Sequencing : BaseObject, IDatasetObject, IUnitData, ICode, IName
    {
        
        [Association("DnaExtract-NGS")]
        public DnaExtract DnaExtract
        {
            get => dnaExtract;
            set => SetPropertyValue(nameof(DnaExtract), ref dnaExtract, value);
        }
        public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        }

        public Sequencing(Session session) : base(session) { }
        DnaExtract dnaExtract;
        private int _TotalReads;
        private DateTime _Date;
        private string _ReadType;
        private string _SequencingMachine;
        private string _SequencingCentre;
        private string _LibraryPreparedBy;
        private string _LibraryPreparationMethod;
        private string _Code;
        private Unit _Unit;
        [ProvidedAssociation("Sequence-Unit", "Sequences", RelationType.OneToMany, "Aggregated")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Unit Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetPropertyValue("Unit", ref _Unit, value);
            }
        }
        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;
[Persistent]
        public string Name
        { 
        get{ return Code; }
}
        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


        public void UpdateInfo()
        {
            if ((Unit != null) && (!String.IsNullOrEmpty(Unit.UnitID)))
                UnitID = Unit.UnitID;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    
                    CreatedByUser = SecuritySystem.CurrentUserName;;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (Session.IsNewObject(this))
            {
            }
            UpdateInfo();
            if (IsLoading) return;
       
                if (SecuritySystem.CurrentUser != null)
                {
                    
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                LastModifiedOn = DateTime.Now;
    }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LibraryPreparationMethod
        {
            get
            {
                return _LibraryPreparationMethod;
            }
            set
            {
                SetPropertyValue("LibraryPreparationMethod", ref _LibraryPreparationMethod, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LibraryPreparedBy
        {
            get
            {
                return _LibraryPreparedBy;
            }
            set
            {
                SetPropertyValue("LibraryPreparedBy", ref _LibraryPreparedBy, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SequencingCentre
        {
            get
            {
                return _SequencingCentre;
            }
            set
            {
                SetPropertyValue("SequencingCentre", ref _SequencingCentre, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SequencingMachine
        {
            get
            {
                return _SequencingMachine;
            }
            set
            {
                SetPropertyValue("SequencingMachine", ref _SequencingMachine, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ReadType
        {
            get
            {
                return _ReadType;
            }
            set
            {
                SetPropertyValue("ReadType", ref _ReadType, value);
            }
        }

        public DateTime Date
        {
            get
            {
                return _Date;
            }
            set
            {
                SetPropertyValue("Date", ref _Date, value);
            }
        }

        public int TotalReads
        {
            get
            {
                return _TotalReads;
            }
            set
            {
                SetPropertyValue("TotalReads", ref _TotalReads, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
       public string Code
       {
           get
           {
               return _Code;
           }
           set
           {
               SetPropertyValue("Code", ref _Code, value);
           }
       }
        private string _UnitID;
        public string UnitID
        {
            get
            {
                return _UnitID;
            }
            set
            {
                SetPropertyValue("UnitID", ref _UnitID, value);
            }
        }
        private Dataset _Dataset;
        [ProvidedAssociation("Sequence-Dataset", "Sequences", RelationType.OneToMany, "Aggregated")]
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }

        private string _Comment;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }

    }
 
}
