using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.ComponentModel;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    public class PcrDnaExtract : BaseObject, ISequenceObject
    {
        [Association("PcrDnaExtract-PcrDnaExtractsSequenced")]
        public XPCollection<PcrDnaExtractSequenced> Sequencing
        {
            get
            {
                return GetCollection<PcrDnaExtractSequenced>(nameof(Sequencing));
            }
        }


        private double? _Concentration;
        public double? Concentration
        {
            get { return _Concentration; }
            set { SetPropertyValue<double?>(nameof(Concentration), ref _Concentration, value); }
        }


        private double? _Dilution;
        public double? Dilution
        {
            get { return _Dilution; }
            set { SetPropertyValue<double?>(nameof(Dilution), ref _Dilution, value); }
        }

            
        PcrDnaExtract rePcrSource;
        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(500)]
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref _FullName, value);
            }

        }
        private string _Position;
        public string Position
        {
            get
            {
                return _Position;
            }
            set
            {
                SetPropertyValue("Position", ref _Position, value);
            }
        }
        /*    [Association("Unit-Consensus")]
            public XPCollection<Sequence> UsedInConsensusSequences
            {
                get
                {
                    return GetCollection<Sequence>("UsedInConsensusSequences");
                }
            }*/
        public PcrDnaExtract(Session session) : base(session) { }
        private DnaExtract _DnaExtr;
        [Association("PcrDnaExtrs")]
        [RuleRequiredField("RuleRequiredField for PcrDnaExtract.DnaExtract", DefaultContexts.Save, "DNA extract property must not be empty.")]
        public DnaExtract DnaExtract
        {
            get
            {
                return _DnaExtr;
            }
            set
            {
                _DnaExtr = value;
            }
        }
        private Amplification _Pcr;
        [Association("Amplification-DnaExtrs")]
        //   [RuleRequiredField("RuleRequiredField for PcrDnaExtract.Pcr", DefaultContexts.Save, "PCR property must not be empty.")]
        public Amplification Pcr
        {
            get
            {
                return _Pcr;
            }
            set
            {
                SetPropertyValue("Result", ref _Pcr, value);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();

            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (DnaExtract != null)
                FullName = String.Format("{0}: {1} - {2}", Position, Result, DnaExtract.FullName);
            else
                FullName = String.Format("{0}: {1}", Position, Result);

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }


        private AmplificationResult? _Result;
        public AmplificationResult? Result
        {
            get
            {
                return _Result;
            }
            set
            {
                SetPropertyValue("Result", ref _Result, value);
            }
        }
        private Primer _PrimerForward;
        [Association("PrimerForward-Extraction")]
        public Primer PrimerForward
        {
            get
            {
                return _PrimerForward;
            }
            set
            {
                SetPropertyValue("PrimerForward", ref _PrimerForward, value);
            }
        }
        private Primer _PrimerReverse;
        [Association("PrimerReverse-Extraction")]
        public Primer PrimerReverse
        {
            get
            {
                return _PrimerReverse;
            }
            set
            {
                SetPropertyValue("PrimerReverse", ref _PrimerReverse, value);
            }
        }
        //private Gene _Gene;
        /*   [Association("Gene-Extraction")]
           public Gene Gene
           {
               get
               {
                   return _Gene;
               }
               set
               {
                   SetPropertyValue("Gene", ref _Gene, value);
               }
           }*/
        
        public PcrDnaExtract RePcrSource
        {
            get => rePcrSource;
            set => SetPropertyValue(nameof(RePcrSource), ref rePcrSource, value);
        }
        private string _Sequence;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string Sequence
        {
            get
            {
                return _Sequence;
            }
            set
            {
                SetPropertyValue("Sequence", ref _Sequence, value);
            }
        }
        private string _SequenceForward;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string SequenceForward
        {
            get
            {
                return _SequenceForward;
            }
            set
            {
                SetPropertyValue("SequenceForward", ref _SequenceForward, value);
            }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
        private string _Genbank;
        public string Genbank
        {
            get
            {
                return _Genbank;
            }
            set
            {
                SetPropertyValue("Genbank", ref _Genbank, value);
            }
        }
        private string _ResultQuality;
        public string ResultQuality
        {
            get
            {
                return _ResultQuality;
            }
            set
            {
                SetPropertyValue("ResultQuality", ref _ResultQuality, value);
            }
        }
        private string _SequenceReverse;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public string SequenceReverse
        {
            get
            {
                return _SequenceReverse;
            }
            set
            {
                SetPropertyValue("SequenceReverse", ref _SequenceReverse, value);
            }
        }
    }
  

}
