using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;

namespace EarthCape.Module.Lab
{
  //  [DefaultClassOptions]
    [VisibleInReports(true)]
    public class SNPdata : BaseObject,IDatasetObject
    {

        public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        } 

        public SNPdata(Session session) : base(session) { }
        private Unit _Unit;
       [ProvidedAssociation("SNPdata-Unit", "SNPData", RelationType.OneToMany, "Aggregated")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Unit Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetPropertyValue("Unit", ref _Unit, value);
            }
        }
        private Snp _Snp;
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public Snp Snp
        {
            get
            {
                return _Snp;
            }
            set
            {
                SetPropertyValue("Snp", ref _Snp, value);
            }
        }

        private string _SnpName;
        public string SnpName
        {
            get
            {
                return _SnpName;
            }
            set
            {
                SetPropertyValue("SnpName", ref _SnpName, value);
            }
        }
        private string _UnitID;
        public string UnitID
        {
            get
            {
                return _UnitID;
            }
            set
            {
                SetPropertyValue("UnitID", ref _UnitID, value);
            }
        }
        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }
     
        private string valAllele1;
        public string ValAllele1
        {
            get { return valAllele1; }
            set { valAllele1 = value; }
        }
        private string valAllele2;
        public string ValAllele2
        {
            get { return valAllele2; }
            set { valAllele2 = value; }
        }
    
    }
 
}
