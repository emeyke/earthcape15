using System;

using DevExpress.Xpo;
using EarthCape.Module.Lab;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;


namespace EarthCape.Module.Lab
{
   [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class Snp : BaseObject, IName, IProjectObject
    {
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }

        }
        public Snp(Session session) : base(session) { }
        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
       private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        private Primer _ProbeF;
        public Primer ProbeF
        {
            get
            {
                return _ProbeF;
            }
            set
            {
                SetPropertyValue("ProbeF", ref _ProbeF, value);
            }
        }

        private Primer _ProbeR;
        public Primer ProbeR
        {
            get
            {
                return _ProbeR;
            }
            set
            {
                SetPropertyValue("ProbeR", ref _ProbeR, value);
            }
        }
        
    }
}
