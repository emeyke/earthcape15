using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Lab
{
    public class PlatePosition : BaseObject
    {
        public PlatePosition(Session session) : base(session) { }
        /*    SequencingRun sequencingRun;
            this causes a problem cant delete samples from run
        //    [Association("SequencingRun-Plate")]
            public SequencingRun SequencingRun
            {
                get => sequencingRun;
                set => SetPropertyValue(nameof(SequencingRun), ref sequencingRun, value);
            }*/
        
        public PcrDnaExtractSequenced SampleForward
        {
            get => sampleForward;
            set => SetPropertyValue(nameof(SampleForward), ref sampleForward, value);
        }
        PcrDnaExtractSequenced sampleReverse;
        PcrDnaExtractSequenced sampleForward;
        
        public PcrDnaExtractSequenced SampleReverse
        {
            get => sampleReverse;
            set => SetPropertyValue(nameof(SampleReverse), ref sampleReverse, value);
        }
       /* PcrDnaExtractSequenced sampleForward = null;
        public PcrDnaExtractSequenced SampleForward
        {
            get { return sampleForward; }
            set
            {
                if (sampleForward == value)
                    return;

                // Store a reference to the person's former sampleForward. 
                PcrDnaExtractSequenced prevSampleForward = sampleForward;
                sampleForward = value;

                if (IsLoading) return;

                // Remove a reference to the sampleForward's owner, if the person is its owner. 
                if (prevSampleForward != null && prevSampleForward.PlatePositionForward == this)
                    prevSampleForward.PlatePositionForward = null;

                // Specify the person as a new owner of the sampleForward. 
                if (sampleForward != null)
                    sampleForward.PlatePositionForward = this;

                OnChanged("SampleForward");
            }
        }
        PcrDnaExtractSequenced sampleReverse = null;
        public PcrDnaExtractSequenced SampleReverse
        {
            get { return sampleReverse; }
            set
            {
                if (sampleReverse == value)
                    return;

                // Store a reference to the person's former sampleReverse. 
                PcrDnaExtractSequenced prevSampleReverse = sampleReverse;
                sampleReverse = value;

                if (IsLoading) return;

                // Remove a reference to the sampleReverse's owner, if the person is its owner. 
                if (prevSampleReverse != null && prevSampleReverse.PlatePositionReverse == this)
                    prevSampleReverse.PlatePositionReverse = null;

                // Specify the person as a new owner of the sampleReverse. 
                if (sampleReverse != null)
                    sampleReverse.PlatePositionReverse = this;

                OnChanged("SampleReverse");
            }
        }*/
        string row;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [ImmediatePostData(true)]
        public string Row
        {
            get => row;
            set => SetPropertyValue(nameof(Row), ref row, value);
        }
        int col;
        [ImmediatePostData(true)]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public int Col
        {
            get => col;
            set => SetPropertyValue(nameof(Col), ref col, value);
        }
        [Persistent]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FullName
        {
            get => Row + Col;
        }
        [Persistent]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Label
        {
            get
            {
                if (SampleForward != null)
                    if (SampleForward.PcrDnaExtract != null)
                        if (SampleForward.PcrDnaExtract.DnaExtract != null)
                            if (SampleForward.PcrDnaExtract.PrimerForward != null)
                                return SampleForward.PcrDnaExtract.DnaExtract.Code + "_" + SampleForward.PcrDnaExtract.PrimerForward.Name;
                            else
                                return SampleForward.PcrDnaExtract.DnaExtract.Code;

                if (SampleReverse != null)
                    if (SampleReverse.PcrDnaExtract != null)
                        if (SampleReverse.PcrDnaExtract.DnaExtract != null)
                            if (SampleReverse.PcrDnaExtract.PrimerReverse != null)
                                return SampleReverse.PcrDnaExtract.DnaExtract.Code + "_" + SampleReverse.PcrDnaExtract.PrimerReverse.Name;
                            else
                                return SampleReverse.PcrDnaExtract.DnaExtract.Code;
                return "";
            }
        }
    }
  

}
