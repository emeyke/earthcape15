using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;


namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    public class Genotyping : BaseObject, IName, IProjectObject
    {
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }

        }
        public Genotyping(Session session) : base(session) { }
        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        [Association("Genotyping-GenotypedUnits"),Aggregated]
        public XPCollection<UnitGenotyping> GenotypedUnits
        {
            get
            {
                return GetCollection<UnitGenotyping>("GenotypedUnits");
            }
        }
        
    }
 

}
