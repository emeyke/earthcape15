using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

using DevExpress.Data.Filtering;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    public class SequencingRun : BaseObject,IName,ILabBookPage
    {
         private string _labbook;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("RTF")]
        [Delayed(true)]
        //[EditorAlias("RtfText")]
        //RtfText
        //[EditorAlias("HtmlPropertyEditor")]
        public string LabBook
        {
            get { return _labbook; }
            set { SetPropertyValue("LabBook", ref _labbook, value); }
        }

        private string _Plate;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Plate
        {
            get { return _Plate; }
            set { SetPropertyValue<string>(nameof(Plate), ref _Plate, value); }
        }


        private string _xls;
        [VisibleInListView(false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias("XLS")]
        //[EditorAlias("RtfText")]
        //RtfText
        //[EditorAlias("HtmlPropertyEditor")]
        public string LabBookXLS
        {
            get { return _xls; }
            set { SetPropertyValue("LabBookXLS", ref _xls, value); }
        }


      /*  private XPCollection<PlatePosition> _Plate;
        public XPCollection<PlatePosition> Plate
        {
            get
            {
                if (_Plate == null)
                {
                    _Plate = new XPCollection<PlatePosition>(Session, 
                        CriteriaOperator.Or(
                        new BinaryOperator("SampleForward.SequencingRun.Oid",this.Oid), 
                        new BinaryOperator("SampleReverse.SequencingRun.Oid", this.Oid)));
                     _Plate.BindingBehavior = CollectionBindingBehavior.AllowNone;
                }
                return _Plate;
            }
        }*/

     
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
               }
        }
        string labBook;
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }
            [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }

     
        protected override void OnSaving()
        {
            if ((Value_WindowController.Instance()!=null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;
                      if (SecuritySystem.CurrentUser != null)
                    {
                        
                        LastModifiedByUser = SecuritySystem.CurrentUserName;
                    }
                    LastModifiedOn = DateTime.Now;
         } 
        public SequencingRun(Session session)
            : base(session) 
        {
            Name = String.Format("Sequencing-{0}{1:yyMMdd}", SecuritySystem.CurrentUserName ?? "", DateTime.Now);
        }
      
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
      /*  AttachmentStorage fileStorage;
        public AttachmentStorage FileStorage
        {
            get => fileStorage;
            set => SetPropertyValue(nameof(FileStorage), ref fileStorage, value);
        }*/
        PersonCore owner;
        public PersonCore Owner
        {
            get => owner;
            set => SetPropertyValue(nameof(Owner), ref owner, value);
        }
        string customerNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string CustomerNumber
        {
            get => customerNumber;
            set => SetPropertyValue(nameof(CustomerNumber), ref customerNumber, value);
        }
        string serviceType;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string ServiceType
        {
            get => serviceType;
            set => SetPropertyValue(nameof(ServiceType), ref serviceType, value);
        }
        string sequencingChemistry;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string SequencingChemistry
        {
            get => sequencingChemistry;
            set => SetPropertyValue(nameof(SequencingChemistry), ref sequencingChemistry, value);
        }

        [Association("SequencingRun-SequencingRunDnaExtrs"),Aggregated]
        public XPCollection<PcrDnaExtractSequenced> PcrDnaExtractsSequenced
        {
            get
            {
                return GetCollection<PcrDnaExtractSequenced>("PcrDnaExtractsSequenced");
            }
        }
    }
}
