using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;

using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Lab
{
     [NavigationItem(false)]
    [CreatableItem(false)]
    [DefaultProperty("Name")]
    public abstract class DnaFragment : BaseObject, IName
    {
    
        public DnaFragment(Session session) : base(session) { }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
        private string _TaxonomicName;
        public string TaxonomicName
        {
            get
            {
                return _TaxonomicName;
            }
            set
            {
                SetPropertyValue("TaxonomicName", ref _TaxonomicName, value);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
            if (SecuritySystem.CurrentUser != null)
            {

                CreatedByUser = SecuritySystem.CurrentUserName; ;
                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            CreatedOn = DateTime.Now;
            LastModifiedOn = DateTime.Now;
        }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
          
            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }

        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }



    }

}
