using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;


namespace EarthCape.Module.Lab
{
    public class UnitGenotypingValue : BaseObject
    {
        private string _Value;
        private Marker _Marker;
        private UnitGenotyping _UnitGenotyping;
        private string _Comment;

        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }

        }
        public UnitGenotypingValue(Session session) : base(session) { }
        [Association("UnitGenotyping-Values"), Aggregated]
         public UnitGenotyping UnitGenotyping
        {
            get
            {
                return _UnitGenotyping;
            }
            set
            {
                SetPropertyValue("UnitGenotyping", ref _UnitGenotyping, value);
            }
        }

         [Association("Marker-UnitGenotyping")]
         public Marker Marker
         {
             get
             {
                 return _Marker;
             }
             set
             {
                 SetPropertyValue("Marker", ref _Marker, value);
             }
         }

         [Size(SizeAttribute.DefaultStringMappingFieldSize)]
         public string Value
         {
             get
             {
                 return _Value;
             }
             set
             {
                 SetPropertyValue("Value", ref _Value, value);
             }
         }
    }


}
