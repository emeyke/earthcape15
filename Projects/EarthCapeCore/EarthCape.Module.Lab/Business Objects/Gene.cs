using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    public class Gene : DnaFragment
    {
        public Gene(Session session) : base(session) { }
        /*
        private Primer primerPCR_F;
        public Primer PrimerPCR_F
        {
            get { return primerPCR_F; }
            set { primerPCR_F = value; }
        }
        private Primer primerPCR_R;
        public Primer PrimerPCR_R
        {
            get { return primerPCR_R; }
            set { primerPCR_R = value; }
        }
        private Int32 startBP;
        public Int32 StartBP
        {
            get { return startBP; }
            set { startBP = value; }
        }
        private Int32 stopBP;
        public Int32 StopBP
        {
            get { return stopBP; }
            set { stopBP = value; }
        }
      [Association("Sequence-Gene")]
        public XPCollection<Sequence> Sequences
        {
            get { return GetCollection<Sequence>("Sequences"); }
        }
        [Association("Gene-Extraction")]
        public XPCollection<PcrDnaExtract> Extractions
        {
            get
            {
                return GetCollection<PcrDnaExtract>("Extractions");
            }
        }*/
    }


}
