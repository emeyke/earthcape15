using System;

using DevExpress.Xpo;

using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using System.ComponentModel;
using Xpand.ExpressApp.Attributes;
using DevExpress.Persistent.BaseImpl;
using Xpand.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    public class DnaExtract : BaseObject, IUnitData,ICode, IName
    {
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Session.IsNewObject(this))
            {
                if (SecuritySystem.CurrentUser != null)
                {

                    CreatedByUser = SecuritySystem.CurrentUserName; ;
                    LastModifiedByUser = SecuritySystem.CurrentUserName;
                }
                CreatedOn = DateTime.Now;
                LastModifiedOn = DateTime.Now;
            }
        }
        [Association("DnaExtract-NGS")]
        public XPCollection<Sequencing> NGS
        {
            get
            {
                return GetCollection<Sequencing>(nameof(NGS));
            }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            FullName = Code;

            if ((Value_WindowController.Instance() != null) && (Value_WindowController.Instance().Importing)) return;
            base.OnSaving();
            if (IsLoading) return;

            if (SecuritySystem.CurrentUser != null)
            {

                LastModifiedByUser = SecuritySystem.CurrentUserName;
            }
            LastModifiedOn = DateTime.Now;
        }

        [Persistent("CreatedByUser")]
        [NoForeignKey]
        private String CreatedByUser;

        [PersistentAlias("CreatedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string CreatedByUser
        {
            get { return CreatedByUser; }
        }
   
        [Persistent("LastModifiedByUser")]
        [NoForeignKey]
        private String LastModifiedByUser;

        [PersistentAlias("LastModifiedByUser")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public string LastModifiedByUser
        {
            get { return LastModifiedByUser; }
        }

        [Persistent("CreatedOn")]
        [NoForeignKey]
        private DateTime CreatedOn;

        [PersistentAlias("CreatedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn
        {
            get { return CreatedOn; }
        }

        [Persistent("LastModifiedOn")]
        [NoForeignKey]
        private DateTime LastModifiedOn;

        [PersistentAlias("LastModifiedOn")]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        [VisibleInDetailView(false)]
        [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn
        {
            get { return LastModifiedOn; }
        }
      


       public static IEnumerable<Attribute> Aggregated
        {
            get { yield return new AggregatedAttribute(); }
        }
        [VisibleInLookupListView(false), VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        public string Name
        { get { return Code; } }

        private double _TotalQuantity;
        private double _Volume;
        private string _QuantificationEstimateMethod;
        private string _Code;
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                SetPropertyValue("Code", ref _Code, value);
            }
        }
        public DnaExtract(Session session) : base(session) { }
        private string _FullName;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Persistent]
        [Size(4000)]
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref _FullName, value);
            }
        }

        public double Volume
        {
            get
            {
                return _Volume;
            }
            set
            {
                SetPropertyValue("Volume", ref _Volume, value);
            }
        }

        public double TotalQuantity
        {
            get
            {
                return _TotalQuantity;
            }
            set
            {
                SetPropertyValue("TotalQuantity", ref _TotalQuantity, value);
            }
        }

      
        private string _Preservation;
        public string Preservation
        {
            get
            {
                return _Preservation;
            }
            set
            {
                SetPropertyValue("Preservation", ref _Preservation, value);
            }
        }
        private string _DoneBy;
        public string DoneBy
        {
            get
            {
                return _DoneBy;
            }
            set
            {
                SetPropertyValue("DoneBy", ref _DoneBy, value);
            }
        }
        private string _ExtractionMethod;
        public string ExtractionMethod
        {
            get
            {
                return _ExtractionMethod;
            }
            set
            {
                SetPropertyValue("ExtractionMethod", ref _ExtractionMethod, value);
            }
        }
        private string _ExtractionBuffer;
        public string ExtractionBuffer
        {
            get
            {
                return _ExtractionBuffer;
            }
            set
            {
                SetPropertyValue("ExtractionBuffer", ref _ExtractionBuffer, value);
            }
        }
        private string _PurificationMethod;
        public string PurificationMethod
        {
            get
            {
                return _PurificationMethod;
            }
            set
            {
                SetPropertyValue("PurificationMethod", ref _PurificationMethod, value);
            }
        }

        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string QuantificationEstimateMethod
        {
            get
            {
                return _QuantificationEstimateMethod;
            }
            set
            {
                SetPropertyValue("QuantificationEstimateMethod", ref _QuantificationEstimateMethod, value);
            }
        }
        private string _RatioOfAbsorbance260_280;
        public string RatioOfAbsorbance260_280
        {
            get
            {
                return _RatioOfAbsorbance260_280;
            }
            set
            {
                SetPropertyValue("RatioOfAbsorbance260_280", ref _RatioOfAbsorbance260_280, value);
            }
        }
        private string _RatioOfAbsorbance260_230;
        public string RatioOfAbsorbance260_230
        {
            get
            {
                return _RatioOfAbsorbance260_230;
            }
            set
            {
                SetPropertyValue("RatioOfAbsorbance260_230", ref _RatioOfAbsorbance260_230, value);
            }
        }
        private string _Concentration;
        public string Concentration
        {
            get
            {
                return _Concentration;
            }
            set
            {
                SetPropertyValue("Concentration", ref _Concentration, value);
            }
        }
        private string _Quality;
        public string Quality
        {
            get
            {
                return _Quality;
            }
            set
            {
                SetPropertyValue("Quality", ref _Quality, value);
            }
        }
        private DateTime _QualityCheckDate;
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        public DateTime QualityCheckDate
        {
            get
            {
                return _QualityCheckDate;
            }
            set
            {
                SetPropertyValue("QualityCheckDate", ref _QualityCheckDate, value);
            }
        }
        private string _ProvidedBy;
        public string ProvidedBy
        {
            get
            {
                return _ProvidedBy;
            }
            set
            {
                SetPropertyValue("ProvidedBy", ref _ProvidedBy, value);
            }
        }
        private DateTime? _ExtractionDate;
        //[ValueConverter(typeof(XpandUtcDateTimeConverter))]
        public DateTime? ExtractionDate
        {
            get
            {
                return _ExtractionDate;
            }
            set
            {
                SetPropertyValue("ExtractionDate", ref _ExtractionDate, value);
            }
        }
       
        private double _DnaAmountInitialMl;
        public double DnaAmountInitialMl
        {
            get
            {
                return _DnaAmountInitialMl;
            }
            set
            {
                SetPropertyValue("DnaAmountInitialMl", ref _DnaAmountInitialMl, value);
            }
        }
        private double _DnaAmountLeftMl;
        public double DnaAmountLeftMl
        {
            get
            {
                return _DnaAmountLeftMl;
            }
            set
            {
                SetPropertyValue("DnaAmountLeftMl", ref _DnaAmountLeftMl, value);
            }
        }
        [Persistent]
        public double Left
        {
            get
            {
                if ((!this.IsLoading) && (!this.IsSaving))
                {
                    double left = DnaAmountLeftMl;
                    foreach (PcrDnaExtract item in Amplifications)
                    {
                        if (item.Pcr != null)
                            left = left - item.Pcr.Dna;
                    }
                    return left;
                }
                else return 0;
            }
        }
        Unit IUnitData.Unit
        {
            get { return Tissue; }
            set { Tissue = (Unit)value; }
        }
        private Unit _Tissue;
         [Association("DNAExtractTissue")]
        public Unit Tissue
        {
            get
            {
                return _Tissue;
            }
            set
            {
                _Tissue = value;
            }
        }
        private Store _Store;
        [ProvidedAssociation("DNAExtractStore")]
         public Store Store
        {
            get
            {
                return _Store;
            }
            set
            {
                _Store = value;
            }
        }
        
        [Association("PcrDnaExtrs"), Aggregated]
        public XPCollection<PcrDnaExtract> Amplifications
        {
            get
            {
                return GetCollection<PcrDnaExtract>("Amplifications");
            }
        }
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
    }


}
