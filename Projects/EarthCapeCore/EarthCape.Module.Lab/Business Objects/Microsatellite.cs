using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using DevExpress.Persistent.BaseImpl;


namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    public class Microsatellite : BaseObject, IName, IProjectObject
    {
        private string _Comment;
        [VisibleInListView(false), VisibleInDetailView(false)]
        [Size(SizeAttribute.Unlimited)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }

        }
        public Microsatellite(Session session) : base(session) { }
        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        private string repeat;
        public string Repeat
        {
            get { return repeat; }
            set { repeat = value; }
        }
        
    }
 

}
