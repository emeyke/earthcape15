using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Lab
{
    [DefaultClassOptions]
    public class ProtocolPCR : Protocol
    {
        public ProtocolPCR(Session session) : base(session) { }

        [Association("ProtocolPCR-Amplifications")]
        public XPCollection<Amplification> Amplifications
        {
            get
            {
                return GetCollection<Amplification>(nameof(Amplifications));
            }
        }
    }
  

}
