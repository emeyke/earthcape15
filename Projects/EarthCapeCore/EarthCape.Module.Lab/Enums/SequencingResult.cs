using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.Lab
{
    public enum SequencingResult
    {
        [ImageName("State_Validation_Valid")]
        Positive,
        [ImageName("State_Validation_Invalid")]
        Negative
    }
}
