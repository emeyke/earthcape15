using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using DevExpress.Xpo.Metadata;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.Lab
{
    public sealed partial class EarthCapeLabModule : ModuleBase
    {
        public EarthCapeLabModule()
        {
            InitializeComponent();
        }
    }
}
