namespace EarthCape.Module.Genetics
{
    partial class Sequences_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ExportFastA = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_ExportFastA
            // 
            this.popupWindowShowAction_ExportFastA.AcceptButtonCaption = null;
            this.popupWindowShowAction_ExportFastA.CancelButtonCaption = null;
            this.popupWindowShowAction_ExportFastA.Caption = "Export FastA";
            this.popupWindowShowAction_ExportFastA.Category = "Tools";
            this.popupWindowShowAction_ExportFastA.ConfirmationMessage = null;
            this.popupWindowShowAction_ExportFastA.Id = "popupWindowShowAction_ExportFastA";
            this.popupWindowShowAction_ExportFastA.ImageName = null;
            this.popupWindowShowAction_ExportFastA.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_ExportFastA.Shortcut = null;
            this.popupWindowShowAction_ExportFastA.Tag = null;
            this.popupWindowShowAction_ExportFastA.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ExportFastA.TargetViewId = null;
            this.popupWindowShowAction_ExportFastA.ToolTip = null;
            this.popupWindowShowAction_ExportFastA.TypeOfView = null;
            this.popupWindowShowAction_ExportFastA.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ExportFastA_CustomizePopupWindowParams);
            // 
            // Sequences_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Module.Genetics.Sequence);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ExportFastA;
    }
}
