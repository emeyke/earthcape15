﻿namespace EarthCape.Module.Lab.Controllers
{
    partial class PcrDnaExtractSequenced_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddPcrDnaExtracts = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_BatchUpdateSequencingSamples = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddPcrDnaExtracts
            // 
            this.popupWindowShowAction_AddPcrDnaExtracts.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddPcrDnaExtracts.CancelButtonCaption = null;
            this.popupWindowShowAction_AddPcrDnaExtracts.Caption = "Add samples";
            this.popupWindowShowAction_AddPcrDnaExtracts.Category = "View";
            this.popupWindowShowAction_AddPcrDnaExtracts.ConfirmationMessage = null;
            this.popupWindowShowAction_AddPcrDnaExtracts.Id = "popupWindowShowAction_AddPcrDnaExtracts";
            this.popupWindowShowAction_AddPcrDnaExtracts.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.popupWindowShowAction_AddPcrDnaExtracts.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_AddPcrDnaExtracts.ToolTip = null;
            this.popupWindowShowAction_AddPcrDnaExtracts.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_AddPcrDnaExtracts.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddPcrDnaExtracts_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddPcrDnaExtracts.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddPcrDnaExtracts_Execute);
            // 
            // popupWindowShowAction_BatchUpdateSequencingSamples
            // 
            this.popupWindowShowAction_BatchUpdateSequencingSamples.AcceptButtonCaption = null;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.CancelButtonCaption = null;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.Caption = "Batch update";
            this.popupWindowShowAction_BatchUpdateSequencingSamples.ConfirmationMessage = null;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.Id = "popupWindowShowAction_BatchUpdateSequencingSamples";
            this.popupWindowShowAction_BatchUpdateSequencingSamples.ImageName = "format2";
            this.popupWindowShowAction_BatchUpdateSequencingSamples.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.ToolTip = null;
            this.popupWindowShowAction_BatchUpdateSequencingSamples.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_BatchUpdateSequencingSamples_CustomizePopupWindowParams);
            this.popupWindowShowAction_BatchUpdateSequencingSamples.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_BatchUpdateSequencingSamples_Execute);
            // 
            // PcrDnaExtractSequenced_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddPcrDnaExtracts);
            this.Actions.Add(this.popupWindowShowAction_BatchUpdateSequencingSamples);
            this.TargetObjectType = typeof(EarthCape.Module.Lab.PcrDnaExtractSequenced);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddPcrDnaExtracts;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_BatchUpdateSequencingSamples;
    }
}
