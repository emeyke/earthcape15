using DevExpress.ExpressApp.Actions;
namespace EarthCape.Module.Lab
{
    partial class PcrDnaExtract_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddExtracts = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_BatchUpdateExtracts = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_AddToSequencing = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_rePCR = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddExtracts
            // 
            this.popupWindowShowAction_AddExtracts.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddExtracts.CancelButtonCaption = null;
            this.popupWindowShowAction_AddExtracts.Caption = "Add extracts";
            this.popupWindowShowAction_AddExtracts.ConfirmationMessage = null;
            this.popupWindowShowAction_AddExtracts.Id = "popupWindowShowAction_AddExtracts";
            this.popupWindowShowAction_AddExtracts.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.popupWindowShowAction_AddExtracts.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_AddExtracts.ToolTip = null;
            this.popupWindowShowAction_AddExtracts.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_AddExtracts.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddExtracts_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddExtracts.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddExtracts_Execute);
            // 
            // popupWindowShowAction_BatchUpdateExtracts
            // 
            this.popupWindowShowAction_BatchUpdateExtracts.AcceptButtonCaption = null;
            this.popupWindowShowAction_BatchUpdateExtracts.CancelButtonCaption = null;
            this.popupWindowShowAction_BatchUpdateExtracts.Caption = "Batch update";
            this.popupWindowShowAction_BatchUpdateExtracts.ConfirmationMessage = null;
            this.popupWindowShowAction_BatchUpdateExtracts.Id = "popupWindowShowAction_BatchUpdateExtracts";
            this.popupWindowShowAction_BatchUpdateExtracts.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.popupWindowShowAction_BatchUpdateExtracts.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_BatchUpdateExtracts.ToolTip = null;
            this.popupWindowShowAction_BatchUpdateExtracts.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_BatchUpdateExtracts_CustomizePopupWindowParams);
            this.popupWindowShowAction_BatchUpdateExtracts.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_BatchUpdateExtracts_Execute);
            // 
            // popupWindowShowAction_AddToSequencing
            // 
            this.popupWindowShowAction_AddToSequencing.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddToSequencing.CancelButtonCaption = null;
            this.popupWindowShowAction_AddToSequencing.Caption = "To sequencing";
            this.popupWindowShowAction_AddToSequencing.Category = "View";
            this.popupWindowShowAction_AddToSequencing.ConfirmationMessage = null;
            this.popupWindowShowAction_AddToSequencing.Id = "popupWindowShowAction_AddToSequencing";
            this.popupWindowShowAction_AddToSequencing.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddToSequencing.ToolTip = "Add selected extracts to a sequencing run";
            this.popupWindowShowAction_AddToSequencing.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddToSequencing_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddToSequencing.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToSequencing_Execute);
            // 
            // popupWindowShowAction_rePCR
            // 
            this.popupWindowShowAction_rePCR.AcceptButtonCaption = null;
            this.popupWindowShowAction_rePCR.CancelButtonCaption = null;
            this.popupWindowShowAction_rePCR.Caption = "rePCR";
            this.popupWindowShowAction_rePCR.Category = "View";
            this.popupWindowShowAction_rePCR.ConfirmationMessage = null;
            this.popupWindowShowAction_rePCR.Id = "popupWindowShowAction_rePCR";
            this.popupWindowShowAction_rePCR.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_rePCR.ToolTip = null;
            this.popupWindowShowAction_rePCR.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_rePCR_CustomizePopupWindowParams);
            this.popupWindowShowAction_rePCR.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToPCR_Execute);
            // 
            // PcrDnaExtract_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddExtracts);
            this.Actions.Add(this.popupWindowShowAction_BatchUpdateExtracts);
            this.Actions.Add(this.popupWindowShowAction_AddToSequencing);
            this.Actions.Add(this.popupWindowShowAction_rePCR);
            this.TargetObjectType = typeof(EarthCape.Module.Lab.PcrDnaExtract);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddExtracts;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_BatchUpdateExtracts;
        private PopupWindowShowAction popupWindowShowAction_AddToSequencing;
        private PopupWindowShowAction popupWindowShowAction_rePCR;
    }
}
