﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Lab.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PcrDnaExtractSequenced_ViewController : ViewController
    {
        public PcrDnaExtractSequenced_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_AddPcrDnaExtracts_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os, typeof(PcrDnaExtract), false);
            e.DialogController.SaveOnAccept = true;
            e.View = view;
        }

        private void popupWindowShowAction_AddPcrDnaExtracts_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ListView view = (ListView)e.PopupWindow.View;
            View parentView = (((NestedFrame)Frame).ViewItem.View as View);
            foreach (PcrDnaExtract item in view.SelectedObjects)
            {
                PcrDnaExtractSequenced pcrdna = parentView.ObjectSpace.CreateObject<PcrDnaExtractSequenced>();
                pcrdna.PcrDnaExtract = parentView.ObjectSpace.FindObject<PcrDnaExtract>(new BinaryOperator("Oid", item.Oid));
                ((SequencingRun)parentView.CurrentObject).PcrDnaExtractsSequenced.Add(pcrdna);
                pcrdna.Save();
            }
            parentView.ObjectSpace.CommitChanges();
            ((SequencingRun)parentView.CurrentObject).Save();
            parentView.Refresh();
        }

        private void popupWindowShowAction_BatchUpdateSequencingSamples_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            SequencingSampleBatchUpdate obj = os.CreateObject<SequencingSampleBatchUpdate>();
            DetailView view = Application.CreateDetailView(os, obj);
            view.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = view;
        }

        private void popupWindowShowAction_BatchUpdateSequencingSamples_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            SequencingSampleBatchUpdate update = (SequencingSampleBatchUpdate)e.PopupWindow.View.CurrentObject;
            IObjectSpace os = View.ObjectSpace;
            foreach (PcrDnaExtractSequenced dna in View.SelectedObjects)
            {
                bool mod = false;
                if (update.SequenceChecked != null)
                {
                    mod = true;
                    dna.SequenceChecked = update.SequenceChecked.Value;
                }
                if (!String.IsNullOrEmpty(update.ResultQuality))
                {
                    mod = true;
                    dna.ResultQuality = update.ResultQuality;
                }
                if (update.Result != null)
                {
                    mod = true;
                    dna.Result = update.Result;
                }
                if (mod)
                    dna.Save();
            }
        }
    }
}
