namespace EarthCape.Module.Lab
{
    partial class LabUnit_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_NewPCR = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_NewPCR
            // 
            this.popupWindowShowAction_NewPCR.AcceptButtonCaption = null;
            this.popupWindowShowAction_NewPCR.CancelButtonCaption = null;
            this.popupWindowShowAction_NewPCR.Caption = "Create DNA Extracts";
            this.popupWindowShowAction_NewPCR.Category = "View";
            this.popupWindowShowAction_NewPCR.ConfirmationMessage = null;
            this.popupWindowShowAction_NewPCR.Id = "popupWindowShowAction_NewPCR";
            this.popupWindowShowAction_NewPCR.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_NewPCR.ToolTip = "Creates DNA extract records and adds them to new PCR";
            this.popupWindowShowAction_NewPCR.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.popupWindowShowAction_NewPCR.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_CreateDNA_CustomizePopupWindowParams);
            this.popupWindowShowAction_NewPCR.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_CreateDNA_Execute);
            // 
            // LabUnit_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_NewPCR);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_NewPCR;
    }
}
