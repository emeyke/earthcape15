using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;

namespace EarthCape.Module.Genetics
{
    public partial class Sequences_ViewController : ViewController
    {
        public Sequences_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_ExportFastA_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
        }
    }
}
