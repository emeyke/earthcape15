﻿namespace EarthCape.Module.Lab.Controllers
{
    partial class SequencingRun_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ArrangePlate = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_ArrangePlate
            // 
            this.popupWindowShowAction_ArrangePlate.AcceptButtonCaption = null;
            this.popupWindowShowAction_ArrangePlate.CancelButtonCaption = null;
            this.popupWindowShowAction_ArrangePlate.Caption = "Arrange plate";
            this.popupWindowShowAction_ArrangePlate.Category = "View";
            this.popupWindowShowAction_ArrangePlate.ConfirmationMessage = "This will clear all well assignments and recreate them. Proceed?";
            this.popupWindowShowAction_ArrangePlate.Id = "popupWindowShowAction_ArrangePlate";
            this.popupWindowShowAction_ArrangePlate.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_ArrangePlate.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.popupWindowShowAction_ArrangePlate.ToolTip = null;
            this.popupWindowShowAction_ArrangePlate.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.popupWindowShowAction_ArrangePlate.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ArrangePlate_CustomizePopupWindowParams);
            this.popupWindowShowAction_ArrangePlate.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ArrangePlate_Execute);
            // 
            // SequencingRun_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_ArrangePlate);
            this.TargetObjectType = typeof(EarthCape.Module.Lab.SequencingRun);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ArrangePlate;
    }
}
