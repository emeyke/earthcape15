using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Editors;

namespace EarthCape.Module.Lab
{
    public partial class AmplificationTemplate_ViewController : ViewController
    {
        public AmplificationTemplate_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void simpleAction_CreateAmplificationRun_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ProtocolPCR template = (ProtocolPCR)View.CurrentObject;
            Amplification pcr = os.CreateObject<Amplification>();
            pcr.LabBook = template.LabBook;
         
          //  pcr.Save();
            DetailView view=Application.CreateDetailView(os,pcr,true);
            view.ViewEditMode = ViewEditMode.Edit;
              e.ShowViewParameters.CreatedView = view;
        }
    }
}
