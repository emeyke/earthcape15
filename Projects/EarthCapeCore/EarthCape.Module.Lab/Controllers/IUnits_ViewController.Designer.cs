﻿namespace EarthCape.Module.Lab.Controllers
{
    partial class IUnits_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_IUnitsShowSequencing = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_IUnitsShowSequencing
            // 
            this.simpleAction_IUnitsShowSequencing.Caption = "Show sequencing";
            this.simpleAction_IUnitsShowSequencing.Category = "Tools";
            this.simpleAction_IUnitsShowSequencing.ConfirmationMessage = null;
            this.simpleAction_IUnitsShowSequencing.Id = "simpleAction_IUnitsShowSequencing";
            this.simpleAction_IUnitsShowSequencing.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_IUnitsShowSequencing.ToolTip = null;
            this.simpleAction_IUnitsShowSequencing.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowSequencing_Execute);
            // 
            // IUnits_ViewController
            // 
            this.Actions.Add(this.simpleAction_IUnitsShowSequencing);
            this.TargetObjectType = typeof(EarthCape.Module.Core.IUnits);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_IUnitsShowSequencing;
    }
}
