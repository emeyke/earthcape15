using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Editors;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using System.Collections;

namespace EarthCape.Module.Lab
{
    public partial class LabUnit_ViewController : ViewController
    {
        public LabUnit_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(Unit);
            TargetViewType = ViewType.ListView;
        }

        private void simpleAction_CreateDnaExtrs_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //  Group Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)ObjectSpace);
            foreach (Unit item in View.SelectedObjects)
            {
                DnaExtract dna = ObjectSpace.CreateObject<DnaExtract>();
                dna.Tissue = item;
                // dna.Project = item.Project;
                if (!String.IsNullOrEmpty(item.UnitID))
                {
                    dna.Name = item.UnitID;
                }
                else
                {
                    dna.Name = item.Oid.ToString();
                }
                dna.Save();
            }
            ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_CreateDNA_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

            //      need_to_add sorting colelction by sample id &&restart suffix

            Project Project = Value_WindowController.Instance().GetGroup((XPObjectSpace)ObjectSpace);
            XPObjectSpace os = (XPObjectSpace)View.ObjectSpace;
            // IObjectSpace os = Application.CreateObjectSpace();
            CreateDnaExtrOptions createDNAOptions = ((CreateDnaExtrOptions)e.PopupWindow.View.CurrentObject);
            // int pos = 0;

            ArrayList objs = new ArrayList();
            foreach (object item in View.SelectedObjects)
            {
                objs.Add(os.GetKeyValue(item));

            }
            ArrayList orgs = new ArrayList();
            XPCollection<Unit> coll = new XPCollection<Unit>(((XPObjectSpace)ObjectSpace).Session, new InOperator("Oid", objs));
            coll.Sorting = new SortingCollection(coll, new SortProperty("UnitID", SortingDirection.Ascending));
            coll.Load();

            int objects = View.SelectedObjects.Count + createDNAOptions.ExtractsPerSample;
            Amplification pcr = null;
            if (createDNAOptions.AddToNewAmplification)
            {
                pcr = os.CreateObject<Amplification>();
            }
            int totalcount = 0;
            foreach (Unit item in coll)
            {
                int c = 0;
                for (int i = 1; i <= createDNAOptions.ExtractsPerSample; i++)
                {
                    totalcount++;
                    c += 1;
                    string code = item.UnitID + createDNAOptions.Suffix + c;
                    DnaExtract dna = os.CreateObject<DnaExtract>();
                    while (os.FindObject<DnaExtract>(new BinaryOperator("Name", code)) != null)
                    {
                        code = item.UnitID + createDNAOptions.Suffix + c++;
                    }
                    dna.Tissue = item;
                    dna.Concentration = createDNAOptions.Concentration;
                    dna.DoneBy = createDNAOptions.DoneBy;
                    dna.ExtractionDate = createDNAOptions.ExtractionDate;
                    dna.ExtractionMethod = createDNAOptions.ExtractionMethod;
                    /*  if (createDNAOptions.OwnerGroup != null)
                          dna.Project = createDNAOptions.OwnerGroup;*/
                    dna.Preservation = createDNAOptions.Preservation;
                    dna.ProvidedBy = createDNAOptions.ProvidedBy;
                    dna.PurificationMethod = createDNAOptions.PurificationMethod;
                    dna.Quality = createDNAOptions.Quality;
                    dna.QualityCheckDate = createDNAOptions.QualityCheckDate;
                    dna.RatioOfAbsorbance260_230 = createDNAOptions.RatioOfAbsorbance260_230;
                    dna.RatioOfAbsorbance260_280 = createDNAOptions.RatioOfAbsorbance260_280;
                    dna.DnaAmountInitialMl = createDNAOptions.DnaAmountInitialMl;
                    dna.DnaAmountLeftMl = createDNAOptions.DnaAmountLeftMl;
                    if (!String.IsNullOrEmpty(item.UnitID))
                    {
                        dna.Name = code;
                    }
                    else
                    {
                        dna.Name = String.Format("{0:yyMMdd}{1}{2}", item.Oid, createDNAOptions.Suffix, i);
                    }
                    if (createDNAOptions.Store != null)
                        dna.Store = os.FindObject<Store>(new BinaryOperator("Oid", createDNAOptions.Store.Oid));
                    dna.Save();
                    if (createDNAOptions.AddToNewAmplification)
                    {
                        if (createDNAOptions.Protocol != null)
                        {
                            pcr.LabBook = createDNAOptions.Protocol.LabBook;
                            pcr.Protocol = os.FindObject<ProtocolPCR>(new BinaryOperator("Oid", createDNAOptions.Protocol.Oid));
                        }
                        if (createDNAOptions.ExtractionDate != null)
                            pcr.StartDate = createDNAOptions.ExtractionDate.Value;
                        pcr.Save();
                        if (pcr != null)
                        {
                            PcrDnaExtract pcrdna = os.CreateObject<PcrDnaExtract>();
                            //    pcrdna.Position = pos;
                            pcrdna.DnaExtract = dna;
                            pcrdna.Pcr = pcr;
                            pcrdna.Save();
                        }
                    }
                }
            }
            // CollectionSource ds = new CollectionSource(os, View.ObjectTypeInfo.Type);
            //   e.PopupWindow.SetView(Application.CreateListView(Application.FindListViewId(View.ObjectTypeInfo.Type), ds, false));
            os.CommitChanges();
            MessageOptions options1 = new MessageOptions();
            options1.Duration = 5000;
            options1.Message = string.Format("{0} extracts created", totalcount);
            options1.Type = InformationType.Info;
            options1.Web.Position = InformationPosition.Top;
            options1.Win.Caption = "Extracts created";
            options1.Win.Type = WinMessageType.Toast;
            Application.ShowViewStrategy.ShowMessage(options1);
        }

        private void popupWindowShowAction_CreateDNA_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            Project Project = Value_WindowController.Instance().GetGroup(os);
            CreateDnaExtrOptions obj = os.CreateObject<CreateDnaExtrOptions>();
            obj.OwnerGroup = Project;
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }

        private void simpleAction_CreateAmplificationRun_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }
    }
}
