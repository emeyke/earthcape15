using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Editors;

namespace EarthCape.Module.Lab
{
    public partial class PcrDnaExtract_ViewController : ViewController
    {
        public PcrDnaExtract_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_AddExtracts_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            ListView view = Application.CreateListView(os,typeof(DnaExtract),false);
            e.DialogController.SaveOnAccept = true;
            e.View = view;
  
        }

        private void popupWindowShowAction_AddExtracts_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ListView view = (ListView)e.PopupWindow.View;
            View parentView = (((NestedFrame)Frame).ViewItem.View as View);
            foreach (DnaExtract item in view.SelectedObjects)
            {
                PcrDnaExtract pcrdna = parentView.ObjectSpace.CreateObject<PcrDnaExtract>();
                pcrdna.DnaExtract = parentView.ObjectSpace.FindObject<DnaExtract>(new BinaryOperator("Oid", item.Oid));
                 ((Amplification)parentView.CurrentObject).DnaExtrs.Add(pcrdna);
                 pcrdna.Save();
            }
            parentView.ObjectSpace.CommitChanges();
            ((Amplification)parentView.CurrentObject).Save();
            parentView.Refresh();
           /* ((ListView)View).CollectionSource.Reload();
            View.Refresh();
            RefreshController refreshController = parentView.Frame.GetController<RefreshController>();
            if (refreshController != null)
            {
                if (refreshController.RefreshAction.Active.ResultValue == true)
                    refreshController.RefreshAction.DoExecute();
            }*/
           // parentView.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_BatchUpdateExtracts_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            PcrDnaExtrBatchUpdate update = (PcrDnaExtrBatchUpdate)e.PopupWindow.View.CurrentObject;
            IObjectSpace os = View.ObjectSpace;
            foreach (PcrDnaExtract dna in View.SelectedObjects)
            {
                bool mod = false;
              /*  if (update.Gene != null)
                {
                    mod = true;
                    dna.Gene = os.FindObject<Gene>(new BinaryOperator("Oid", update.Gene.Oid));
                }*/
                if (update.PrimerForward != null)
                {
                    mod = true;
                    dna.PrimerForward = os.FindObject<Primer>(new BinaryOperator("Oid", update.PrimerForward.Oid));
                }
                if (update.PrimerReverse != null)
                {
                    mod = true;
                    dna.PrimerReverse = os.FindObject<Primer>(new BinaryOperator("Oid", update.PrimerReverse.Oid));
                }
                if (update.Result != null)
                {
                    mod = true;
                    dna.Result = update.Result;
                }
                if (update.Concentration != null)
                {
                    mod = true;
                    dna.Concentration = update.Concentration;
                }
                if (update.Dilution != null)
                {
                    mod = true;
                    dna.Dilution = update.Dilution;
                }
                if (mod)
                    dna.Save();
            }
        }

        private void popupWindowShowAction_BatchUpdateExtracts_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            e.DialogController.SaveOnAccept = false;
            PcrDnaExtrBatchUpdate obj = os.CreateObject<PcrDnaExtrBatchUpdate>();
            DetailView view = Application.CreateDetailView(os,obj);
            view.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = view;
  
        }

        private void simpleAction_CreateAmplificationRun_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
          
        }

        private void popupWindowShowAction_AddToSequencing_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            AddToSequencingOptions options = ((AddToSequencingOptions)e.PopupWindow.View.CurrentObject);
            SequencingRun run = View.ObjectSpace.FindObject<SequencingRun>(new BinaryOperator("Oid", options.SequencingRun.Oid));
            foreach (PcrDnaExtract item in View.SelectedObjects)
            {

                PcrDnaExtractSequenced platedsample = View.ObjectSpace.CreateObject<PcrDnaExtractSequenced>();
                platedsample.PcrDnaExtract = item;
                platedsample.SequencingRun = run;
                platedsample.Save();
            }
            View.ObjectSpace.CommitChanges();
            MessageOptions options1 = new MessageOptions();
            options1.Duration = 5000;
            options1.Message = string.Format("{0} PCR products added to sequencing run {1}", View.SelectedObjects.Count, run.Name);
            options1.Type = InformationType.Info;
            options1.Web.Position = InformationPosition.Top;
            options1.Win.Caption = "PCR products added to sequencing";
            options1.Win.Type = WinMessageType.Toast;
            Application.ShowViewStrategy.ShowMessage(options1);
        }


        private void popupWindowShowAction_AddToSequencing_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            AddToSequencingOptions obj = os.CreateObject<AddToSequencingOptions>();
              DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }

        private void popupWindowShowAction_AddToPCR_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            AddToPCROptions options = ((AddToPCROptions)e.PopupWindow.View.CurrentObject);
            Amplification run = View.ObjectSpace.FindObject<Amplification>(new BinaryOperator("Oid", options.Amplification.Oid));
            foreach (PcrDnaExtract item in View.SelectedObjects)
            {

                PcrDnaExtract pcrdnaextract = View.ObjectSpace.CreateObject<PcrDnaExtract>();
                pcrdnaextract.DnaExtract = item.DnaExtract;
                pcrdnaextract.Pcr = run;
                pcrdnaextract.RePcrSource = item;
                pcrdnaextract.PrimerForward = item.PrimerForward;
                pcrdnaextract.PrimerReverse = item.PrimerReverse;
                pcrdnaextract.Save();
            }
            View.ObjectSpace.CommitChanges();
        }

        private void popupWindowShowAction_rePCR_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            AddToPCROptions obj = os.CreateObject<AddToPCROptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }
    }
}
