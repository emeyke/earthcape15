﻿namespace EarthCape.Module.Lab.Controllers
{
    partial class Unit_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ShowSequencing = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ShowSequencing
            // 
            this.simpleAction_ShowSequencing.Caption = "Show sequencing";
            this.simpleAction_ShowSequencing.Category = "Tools";
            this.simpleAction_ShowSequencing.ConfirmationMessage = null;
            this.simpleAction_ShowSequencing.Id = "simpleAction_ShowSequencing";
            this.simpleAction_ShowSequencing.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_ShowSequencing.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_ShowSequencing.ToolTip = null;
            this.simpleAction_ShowSequencing.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ShowSequencing_Execute);
            // 
            // Unit_ViewController
            // 
            this.Actions.Add(this.simpleAction_ShowSequencing);
            this.TargetObjectType = typeof(EarthCape.Module.Core.Unit);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ShowSequencing;
    }
}
