namespace EarthCape.Genetics
{
    partial class GeneticsAmplification_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_CreatePCR = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_CreatePCR
            // 
            this.popupWindowShowAction_CreatePCR.AcceptButtonCaption = null;
            this.popupWindowShowAction_CreatePCR.CancelButtonCaption = null;
            this.popupWindowShowAction_CreatePCR.Caption = "Create PCR";
            this.popupWindowShowAction_CreatePCR.ConfirmationMessage = null;
            this.popupWindowShowAction_CreatePCR.Id = "popupWindowShowAction_CreatePCR";
            this.popupWindowShowAction_CreatePCR.ImageName = null;
            this.popupWindowShowAction_CreatePCR.Shortcut = null;
            this.popupWindowShowAction_CreatePCR.Tag = null;
            this.popupWindowShowAction_CreatePCR.TargetObjectsCriteria = null;
            this.popupWindowShowAction_CreatePCR.TargetViewId = null;
            this.popupWindowShowAction_CreatePCR.ToolTip = null;
            this.popupWindowShowAction_CreatePCR.TypeOfView = null;
            this.popupWindowShowAction_CreatePCR.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_CreatePCR_CustomizePopupWindowParams);
            this.popupWindowShowAction_CreatePCR.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_CreatePCR_Execute);
            // 
            // GeneticsAmplification_ViewController
            // 
            this.TargetObjectType = typeof(EarthCape.Genetics.Amplification);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_CreatePCR;
    }
}
