﻿namespace EarthCape.Module.Lab.Controllers
{
    partial class DNAExtract_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddToPCR = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddToPCR
            // 
            this.popupWindowShowAction_AddToPCR.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddToPCR.CancelButtonCaption = null;
            this.popupWindowShowAction_AddToPCR.Caption = "To PCR";
            this.popupWindowShowAction_AddToPCR.Category = "View";
            this.popupWindowShowAction_AddToPCR.ConfirmationMessage = null;
            this.popupWindowShowAction_AddToPCR.Id = "popupWindowShowAction_AddToPCR";
            this.popupWindowShowAction_AddToPCR.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_AddToPCR.ToolTip = null;
            this.popupWindowShowAction_AddToPCR.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddToPCR_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddToPCR.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddToPCR_Execute);
            // 
            // DNAExtract_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddToPCR);
            this.TargetObjectType = typeof(EarthCape.Module.Lab.DnaExtract);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddToPCR;
    }
}
