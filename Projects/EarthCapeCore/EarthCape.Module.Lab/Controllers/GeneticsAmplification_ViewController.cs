using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;

namespace EarthCape.Genetics
{
    public partial class GeneticsAmplification_ViewController : ViewController
    {
        public GeneticsAmplification_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void popupWindowShowAction_CreatePCR_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            ObjectSpace os = (ObjectSpace)Application.CreateObjectSpace();
            CreatePCROptions obj = os.CreateObject<CreatePCROptions>();
            e.View = Application.CreateDetailView(os, obj);
  
        }

        private void popupWindowShowAction_CreatePCR_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {

        }
    }
}
