﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using System.Collections;

namespace EarthCape.Module.Lab.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IUnits_ViewController : ViewController
    {
        public IUnits_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_ShowSequencing_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ArrayList sequencing = new ArrayList();
            CollectionSource sequencing_source = new CollectionSource(os, typeof(Sequencing));
            foreach (Unit unit in ((IUnits)View.CurrentObject).Units)
            {
                sequencing.Add(os.GetKeyValue(unit));

            }
            //  sequencing_source.Criteria["Selected"] = new InOperator("Oid", sequencing);
            // Frame.SetView(Application.CreateListView(Application.FindListViewId(typeof(Sequencing)), sequencing_source, true));

            CollectionSource newCollectionSource = new CollectionSource(Application.CreateObjectSpace(), typeof(Sequencing));
            newCollectionSource.Criteria["SearchResults"] = new InOperator("Unit.Oid", sequencing);
            e.ShowViewParameters.CreatedView = Application.CreateListView("Sequencing_ListView", newCollectionSource, true); ;
            e.ShowViewParameters.NewWindowTarget = NewWindowTarget.Default;
            e.ShowViewParameters.TargetWindow = TargetWindow.Current;
        }
    }
}
