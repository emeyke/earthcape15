﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace EarthCape.Module.Lab.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DNAExtract_ViewController : ViewController
    {
        public DNAExtract_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_AddToPCR_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            AddToPCROptions obj = os.CreateObject<AddToPCROptions>();
            DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;
        }

        private void popupWindowShowAction_AddToPCR_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            AddToPCROptions options = ((AddToPCROptions)e.PopupWindow.View.CurrentObject);
            Amplification run = View.ObjectSpace.FindObject<Amplification>(new BinaryOperator("Oid", options.Amplification.Oid));
            foreach (DnaExtract item in View.SelectedObjects)
            {

                PcrDnaExtract pcrdnaextract = View.ObjectSpace.CreateObject<PcrDnaExtract>();
                pcrdnaextract.DnaExtract = item;
                pcrdnaextract.Pcr = run;
                pcrdnaextract.Save();
            }
            MessageOptions options1 = new MessageOptions();
            options1.Duration = 5000;
            options1.Message = string.Format("{0} extracts added to PCR {1}", View.SelectedObjects.Count,run.Name);
            options1.Type = InformationType.Info;
            options1.Web.Position = InformationPosition.Top;
            options1.Win.Caption = "Extracts added to PCR";
            options1.Win.Type = WinMessageType.Toast;
            Application.ShowViewStrategy.ShowMessage(options1);

            View.ObjectSpace.CommitChanges();
        }
    }
}
