namespace EarthCape.Module.Lab
{
    partial class AmplificationTemplate_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_CreateAmplificationRun = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_CreateAmplificationRun
            // 
            this.simpleAction_CreateAmplificationRun.Caption = "Use template";
            this.simpleAction_CreateAmplificationRun.Category = "Tools";
            this.simpleAction_CreateAmplificationRun.ConfirmationMessage = null;
            this.simpleAction_CreateAmplificationRun.Id = "simpleAction_CreateAmplificationRun";
            this.simpleAction_CreateAmplificationRun.ImageName = null;
            this.simpleAction_CreateAmplificationRun.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.simpleAction_CreateAmplificationRun.Shortcut = null;
            this.simpleAction_CreateAmplificationRun.Tag = null;
            this.simpleAction_CreateAmplificationRun.TargetObjectsCriteria = null;
            this.simpleAction_CreateAmplificationRun.TargetObjectType = typeof(EarthCape.Module.Lab.ProtocolPCR);
            this.simpleAction_CreateAmplificationRun.TargetViewId = null;
            this.simpleAction_CreateAmplificationRun.ToolTip = null;
            this.simpleAction_CreateAmplificationRun.TypeOfView = null;
            this.simpleAction_CreateAmplificationRun.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_CreateAmplificationRun_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_CreateAmplificationRun;
    }
}
