﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Lab.Helpers;

namespace EarthCape.Module.Lab.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SequencingRun_ViewController : ViewController
    {
        public SequencingRun_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_ArrangePlate_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ArrangePlateOptions arrangePlateOptions = ((ArrangePlateOptions)e.PopupWindow.View.CurrentObject);
            if (((SequencingRun)View.CurrentObject).PcrDnaExtractsSequenced.Count>96)
                throw new Exception("Number of samples is larger than 96! Currently not supported.");
            LabHelper.ArrangePlate(ObjectSpace, (SequencingRun)View.CurrentObject, arrangePlateOptions.Forward, arrangePlateOptions.Reverse, arrangePlateOptions.RemoveCharacters, arrangePlateOptions.CharactersToRemove);
            View.Refresh();
        }

        private void popupWindowShowAction_ArrangePlate_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
             ArrangePlateOptions obj = os.CreateObject<ArrangePlateOptions>();
             DetailView dv = Application.CreateDetailView(os, obj);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.DialogController.SaveOnAccept = true;
            e.View = dv;

        }
    }
}
