using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.SystemModule;
using TatukGIS.NDK;
using EarthCape.Interfaces;
using TatukGIS.NDK;
using EarthCape.Module.Core;

namespace EarthCape.Module.GIS
{
    public partial class UnitSaving_ViewController : ViewController
    {
        public UnitSaving_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            this.TargetObjectType=typeof(IWKT);
            Activated += new EventHandler(UnitSaving_ViewController_Activated);
        }

        void UnitSaving_ViewController_Activated(object sender, EventArgs e)
        {
            if (!(this is IWKT)) return;
            Frame.GetController<DetailViewController>().SaveAndNewAction.Executing += SaveAction_Executing;
            Frame.GetController<DetailViewController>().SaveAndCloseAction.Executing += SaveAction_Executing;
            Frame.GetController<DetailViewController>().SaveAction.Executing += SaveAction_Executing;
        }

        void SaveAction_Executing(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(((IWKT)View.CurrentObject).WKT))
                GISHelper.CreateCentroidXY((IWKT)View.CurrentObject);
            else
                if (typeof(Unit).IsAssignableFrom(View.ObjectTypeInfo.Type))
                    if (((Unit)View.CurrentObject).Locality != null)
                    {
                        if (!string.IsNullOrEmpty(((Unit)View.CurrentObject).Locality.WKT))
                        {
                            TGIS_Point ptg = GISHelper.GetWKTCentroid(((Unit)View.CurrentObject).Locality.WKT);
                            ((Unit)View.CurrentObject).Centroid_X = ptg.X;
                            ((Unit)View.CurrentObject).Centroid_Y = ptg.Y;
                        }
                    }
            GISHelper.UpdateAreaAndLengthFromWKT((IWKT)View.CurrentObject);
        }

    }
}
