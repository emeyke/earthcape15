﻿using System;
using System.Drawing;
using DevExpress.XtraReports.UI;
using TatukGIS.NDK;
using EarthCape.Module.Core;


namespace EarthCape.Module.Std
{
    [ToolboxBitmap(typeof(XRMapPictureBox))]
    public class XRMapPictureBox : XRPictureBox
    {
    
      public String SummaryLayer { get; set; }
        public String UnitCriteria { get; set; }
 
     public String SummaryType { get; set; }
    
      
    }
}
