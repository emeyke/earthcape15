using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using TatukGIS.NDK;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;

namespace EarthCape.Module.GIS
{
    public partial class UnitGIS_ViewController : ViewController
    {
        public UnitGIS_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void simpleAction_UpdateCentroids_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Unit item in View.SelectedObjects)
            {
                if (item.Locality != null)
                    if (item.Locality.WKT != null)
                    {
                        TGIS_Point ptg = GISHelper.GetWKTCentroid(item.Locality.WKT);
                        item.Centroid_X = Convert.ToDecimal(ptg.X);
                        item.Centroid_Y = Convert.ToDecimal(ptg.Y);
                        item.Save();
                    }
            }
            ((XPObjectSpace)View.ObjectSpace).CommitChanges();
        }

     /*   private void simpleAction_UpdateCentroidsFromGeometry_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IWKT item in View.SelectedObjects)
            {
                    if (item.WKT != null)
                    {
                        GISHelper.CreateCentroidXY(item);
                        item.Save();
                    }
            }
            ((XPObjectSpace)View.ObjectSpace).CommitChanges();

        }*/
    }
}
