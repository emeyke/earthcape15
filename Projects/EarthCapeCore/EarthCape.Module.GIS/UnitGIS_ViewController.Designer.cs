
namespace EarthCape.Module.GIS
{
    partial class UnitGIS_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(){
            this.components = new System.ComponentModel.Container();
            this.simpleAction_UpdateCentroidsFromLocality =
                new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_UpdateCentroidsFromGeometry =
                new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_UpdateCentroidsFromLocality.Caption = "Update centroid from locality";
            this.simpleAction_UpdateCentroidsFromLocality.Category = "Tools";
            this.simpleAction_UpdateCentroidsFromLocality.ConfirmationMessage = "Update centroids for selected units?";
            this.simpleAction_UpdateCentroidsFromLocality.Id = "simpleAction_UpdateCentroidsFromLocality";
            this.simpleAction_UpdateCentroidsFromLocality.SelectionDependencyType =
                DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateCentroidsFromLocality.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.simpleAction_UpdateCentroidsFromLocality.TargetViewId = "x";
            this.simpleAction_UpdateCentroidsFromLocality.ToolTip = null;
            this.simpleAction_UpdateCentroidsFromLocality.Execute +=
                new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this
                    .simpleAction_UpdateCentroids_Execute);
            this.simpleAction_UpdateCentroidsFromGeometry.Caption = "Update centroids from geometry";
            this.simpleAction_UpdateCentroidsFromGeometry.Category = "Tools";
            this.simpleAction_UpdateCentroidsFromGeometry.ConfirmationMessage = "Update centroids for selected units?";
            this.simpleAction_UpdateCentroidsFromGeometry.Id = "simpleAction_UpdateCentroidsFromGeometry";
            this.simpleAction_UpdateCentroidsFromGeometry.SelectionDependencyType =
                DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.simpleAction_UpdateCentroidsFromGeometry.TargetObjectType = typeof(EarthCape.Module.Core.Unit);
            this.simpleAction_UpdateCentroidsFromGeometry.TargetViewId = "x";
            this.simpleAction_UpdateCentroidsFromGeometry.ToolTip = null;
            this.Actions.Add(this.simpleAction_UpdateCentroidsFromLocality);
            this.Actions.Add(this.simpleAction_UpdateCentroidsFromGeometry);
        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateCentroidsFromLocality;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_UpdateCentroidsFromGeometry;
    }
}
