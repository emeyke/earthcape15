using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Editors;
using System.Windows.Forms;
using EarthCape.Module.Core;

namespace EarthCape.Module.EOL.Win
{
    public partial class EOL_WebBrowser_ViewController : ViewController
    {
        public EOL_WebBrowser_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewType = ViewType.DetailView;
            TargetObjectType=typeof(TaxonomicName);
            Activated += EOL_WebBrowser_ViewController_Activated;
        }

        void EOL_WebBrowser_ViewController_Activated(object sender, EventArgs e)
        {
            View.CurrentObjectChanged += View_CurrentObjectChanged;
        }

        void View_CurrentObjectChanged(object sender, EventArgs e)
        {
            WebBrowser browser = null;
            foreach (ViewItem item in ((DetailView)View).Items)
            {
                if (item is WebBrowserItem)
                {
                    if (item.Id == "eol")
                        browser = (((WebBrowserItem)item).Control as WebBrowser);
                }
            }
            if (browser == null) return;
            TaxonomicName name = ((TaxonomicName)View.CurrentObject);
            if (name == null)
            {
                browser.Navigate("about:blank");
                return;
            }
            if (name.GetMemberValue("EolId") != null)
            {
                if ((int)(name.GetMemberValue("EolId")) < 1)
                {
                    browser.Navigate("about:blank");
                    return;
                }
                browser.Navigate("http://www.eol.org/pages/" + name.GetMemberValue("EolId").ToString());
            }else
            {
                browser.Navigate("about:blank");
                return;
            }
         
        }

    
    }
}
