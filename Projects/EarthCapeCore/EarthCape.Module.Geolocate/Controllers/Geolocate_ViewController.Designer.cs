﻿namespace EarthCape.Module.Geolocate.Controllers
{
    partial class Geolocate_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_GeolocateUnit = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_GeolocateLocality = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_GeolocateUnit
            // 
            this.popupWindowShowAction_GeolocateUnit.AcceptButtonCaption = null;
            this.popupWindowShowAction_GeolocateUnit.CancelButtonCaption = null;
            this.popupWindowShowAction_GeolocateUnit.Caption = "Geolocate (beta)";
            this.popupWindowShowAction_GeolocateUnit.Category = "Tools";
            this.popupWindowShowAction_GeolocateUnit.ConfirmationMessage = null;
            this.popupWindowShowAction_GeolocateUnit.Id = "popupWindowShowAction_GeolocateUnit";
            this.popupWindowShowAction_GeolocateUnit.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_GeolocateUnit.TargetViewId = "";
            this.popupWindowShowAction_GeolocateUnit.ToolTip = null;
            this.popupWindowShowAction_GeolocateUnit.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_GeolocateUnit_CustomizePopupWindowParams);
            this.popupWindowShowAction_GeolocateUnit.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_GeolocateUnit_Execute);
            // 
            // popupWindowShowAction_GeolocateLocality
            // 
            this.popupWindowShowAction_GeolocateLocality.AcceptButtonCaption = null;
            this.popupWindowShowAction_GeolocateLocality.CancelButtonCaption = null;
            this.popupWindowShowAction_GeolocateLocality.Caption = "Geolocate (beta)";
            this.popupWindowShowAction_GeolocateLocality.Category = "Tools";
            this.popupWindowShowAction_GeolocateLocality.ConfirmationMessage = null;
            this.popupWindowShowAction_GeolocateLocality.Id = "popupWindowShowAction_GeolocateLocality";
            this.popupWindowShowAction_GeolocateLocality.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_GeolocateLocality.TargetObjectType = typeof(EarthCape.Module.Core.Locality);
            this.popupWindowShowAction_GeolocateLocality.TargetViewId = "x";
            this.popupWindowShowAction_GeolocateLocality.ToolTip = null;
            this.popupWindowShowAction_GeolocateLocality.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_GeolocateLocality_CustomizePopupWindowParams);
            this.popupWindowShowAction_GeolocateLocality.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_GeolocateLocality_Execute);
            // 
            // Geolocate_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_GeolocateUnit);
            this.Actions.Add(this.popupWindowShowAction_GeolocateLocality);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GeolocateLocality;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_GeolocateUnit;
    }
}
