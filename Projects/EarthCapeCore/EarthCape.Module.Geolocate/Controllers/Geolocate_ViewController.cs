﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;

namespace EarthCape.Module.Geolocate.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Geolocate_ViewController : ViewController
    {
        public Geolocate_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            if ((popupWindowShowAction_GeolocateLocality.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_GeolocateLocality.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, typeof(Locality), DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
            if ((popupWindowShowAction_GeolocateUnit.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_GeolocateUnit.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, typeof(Unit), DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void simpleAction_Geolocate_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
           }

        private void simpleAction_Geolocate_Unit_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
             }

        private void popupWindowShowAction_GeolocateUnit_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            GeolocateOptions options = ((GeolocateOptions)(e.PopupWindowViewCurrentObject));

            foreach (Unit item in View.SelectedObjects)
            {
                GeolocateHelper.GeolocateUnit(item, (XPObjectSpace)ObjectSpace,options);
            }
            View.Refresh();

        }

        private void popupWindowShowAction_GeolocateLocality_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            GeolocateOptions options = ((GeolocateOptions)(e.PopupWindowViewCurrentObject));

            foreach (Locality item in View.SelectedObjects)
            {
                GeolocateHelper.GeolocateLocality(item, (XPObjectSpace)ObjectSpace,options);
            }
            View.Refresh();

        }

        private void popupWindowShowAction_GeolocateLocality_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GeolocateOptions options = os.CreateObject<GeolocateOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }

        private void popupWindowShowAction_GeolocateUnit_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            GeolocateOptions options = os.CreateObject<GeolocateOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;

        }
    }
}
