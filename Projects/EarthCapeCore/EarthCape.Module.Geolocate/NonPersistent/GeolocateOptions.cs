using DevExpress.Persistent.Validation;
using DevExpress.Xpo;


namespace EarthCape.Module.Geolocate
{
   
    [NonPersistent]
    public class GeolocateOptions : XPCustomObject
    {
        public GeolocateOptions(Session session) : base(session) { }


        private string _Country="";
        public string Country
        {
            get { return _Country; }
            set { SetPropertyValue<string>(nameof(Country), ref _Country, value); }
        }

        private string _Province="";
        public string Province
        {
            get { return _Province; }
            set { SetPropertyValue<string>(nameof(Province), ref _Province, value); }
        }




    }

}
