using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Office.Utils;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using EarthCape.Module.Geolocate.GeolocateSvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace EarthCape.Module.Geolocate
{
    public class GeolocateHelper
    {

        public static void GeolocateLocality(Locality loc, XPObjectSpace os,GeolocateOptions options)
        {

            using (geolocatesvc Soap = new geolocatesvc())
            {
                // XPCollection<Locality> locs;
                try
                {
                    string province = "";
                    string district = "";
                    if (!String.IsNullOrEmpty(loc.Province)) province = loc.Province;
                    if (!String.IsNullOrEmpty(loc.District)) district = loc.District;
                    Georef_Result_Set result = Soap.Georef2(loc.Country, province, district, loc.Name, false, false);
                    foreach (Georef_Result item in result.ResultSet)
                    {
                        loc.Latitude = item.WGS84Coordinate.Latitude;
                        loc.Longitude = item.WGS84Coordinate.Longitude;
                        loc.EPSG = 4326;
                        loc.ParsePattern = item.ParsePattern;
                        loc.Save();
                    }
                    os.CommitChanges();
                }
                catch (Exception ex)
                {
                    if (ex is WebException)
                        // throw (new Exception("BHL web service did not respond. Please try again later. Tweet #fail @BioDivLibrary (OR check your internet connection)."));
                        throw (new Exception("Geolocate web service did not respond. Please try again later or check your internet connection."));
                }
            }
        }
        public static void GeolocateUnit(Unit unit, XPObjectSpace os, GeolocateOptions options)
        {

            using (geolocatesvc Soap = new geolocatesvc())
            {
                try
                {
                    string pl = unit.PlaceDetails;
                    string c = options.Country;
                    string p = options.Province;
                    if (!String.IsNullOrEmpty(unit.Country))
                        c = unit.Country;
                    if (!String.IsNullOrEmpty(unit.Province))
                       p = unit.Province;

                    Georef_Result_Set result = Soap.Georef2(c, p, "", pl, false, false);
                    foreach (Georef_Result item in result.ResultSet)
                    {
                        unit.Latitude = item.WGS84Coordinate.Latitude;
                        unit.Longitude = item.WGS84Coordinate.Longitude;
                        unit.EPSG = 4326;
                        //loc.ParsePattern = item.ParsePattern;
                        unit.Save();
                    }
                    os.CommitChanges();
                }
                catch (Exception ex)
                {
                    if (ex is WebException)
                        // throw (new Exception("BHL web service did not respond. Please try again later. Tweet #fail @BioDivLibrary (OR check your internet connection)."));
                        throw (new Exception("Geolocate web service did not respond. Please try again later or check your internet connection."));
                }
            }
        }

        /*
This file contains the snippets of a program that automate as much as possible the finding of the position of a shipwreck based on a descriptive coordinate system.

Andrea Siotto
https://sites.temple.edu/tudsc/2016/09/03/making-a-program-in-c-to-find-the-coordinates/
*/

        // This function takes a string (a description of the events with a possible bearing and distance of the sinking location from a given place),
        // searches for a combination of words (the bearing),
        // and returns a list of the bearings found.
        private List<String> searchAndTransfDirections(string description) // searches for the definition of bearing and distances
        {
            List<String> result = new List<String>(); // the list where to put the results
                                                      // this block of coding is the real magic: with the help of the regular expressions (regex), it searches for strings such as "(128 km) east by north" or similar
            string pattern = @"(\(\d + km\)|\(\d +,\d + km\)|\(\d +.\d + km\)) (east | south | north | west) by(east | south | north | west)";
            // string pattern1 = @"(\(\d + km\)|\(\d +,\d + km\)|\(\d +.\d + km\)) (east | south | north | west)(east | south | north | west)(east | south | north | west)";
            // string pattern2 = @"(\(\d + km\)|\(\d +,\d + km\)|\(\d +.\d + km\)) (east | south | north | west)(east | south | north | west) by(east | south | north | west)";
            // string pattern3 = @"(\(\d + km\)|\(\d +,\d + km\)|\(\d +.\d + km\)) (east | south | north | west)(east | south | north | west) of";
            // string pattern4 = @"(\(\d + km\)|\(\d +,\d + km\)|\(\d +.\d + km\)) (east | south | north | west) of";

            MatchCollection m = Regex.Matches(description, pattern, RegexOptions.IgnoreCase);//searches the "direction" by "direction" case
            if (m.Count > 0)
            {
                for (int ctr = 0; ctr < m.Count; ctr++)
                {
                    result.Add(m[ctr].Value);
                }
            }

            return result; // it returns the list of the bearings found.
        }
        // this function takes a string with a description of the bearings and transforms them in degrees.
        private string convertBearingstoDegree(string bearings)
        {

            // declaration of the matrix containing all the descriptions and the respective degrees.
            string[,] directions = new string[2, 32]
            {
//new string[32]
{

"North",
"North by East",
"North North East",
"North East by North",
"North East",
"North East by East",
"East North East",
"East by North",
"East",
"East by South",
"East South East",
"South East by East",
"South East",
"South East by South",
"South South East",
"South by East",
"South",
"South by West",
"South South West",
"South West by South",
"South West",
"South West by West",
"West South West",
"West by South",
"West",
"West by North",
"West North West",
"North West by West",
"North West",
"North West by North",
"North North West",
"North by West",
},
//new string[32]
{
"0",
"11.25",
"22.5",
"33.75",
"45",
"56.25",
"67.5",
"78.75",
"90",
"101.25",
"112.5",
"123.75",
"135",
"146.25",
"157.5",
"168.75",
"180",
"191.25",
"202.5",
"213.75",
"225",
"236.25",
"247.5",
"258.75",
"270",
"281.25",
"292.5",
"303.75",
"315",
"326.25",
"337.5",
"348.75"
}
            };
            string pattern = @"(east | south | north | west)(east | south | north | west) by(east | south | north | west)";
            string pattern1 = @"(east | south | north | west) by(east | south | north | west)";
            string pattern2 = @"(east|south|north|west) (east|south|north|west) (east|south|north|west)";
            string pattern3 = @"(east|south|north|west) (east|south|north|west)";
            string pattern4 = @"(east|south|north|west)";
            // in this block of code the function searches for the matching bearings to see if the string is a bearing
            string controlledBearings = "";
            string match = Regex.Match(bearings, pattern).Value;
            if (match != "") { controlledBearings = match; }
            else
            {
                match = Regex.Match(bearings, pattern1).Value;
                if (match != "") { controlledBearings = match; }
                else
                {
                    match = Regex.Match(bearings, pattern2).Value;
                    if (match != "") { controlledBearings = match; }
                    else
                    {
                        match = Regex.Match(bearings, pattern3).Value;
                        if (match != "") { controlledBearings = match; }
                        else
                        {
                            match = Regex.Match(bearings, pattern4).Value;
                            if (match != "") { controlledBearings = match; }
                        }
                    }

                }
            }

            string result = "-1";
            for (int x = 0; x < 32; x++)
            {
                if (string.Equals(directions[0, x], controlledBearings, StringComparison.OrdinalIgnoreCase))// if the bearing is found in the list then it returns the value

                {
                    return directions[1, x];
                }
            }
            return result; // if the value it is not found then it returns an empty string.
        }


    }
}
