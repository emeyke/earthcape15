using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Importer
{
    public enum ObjectType
    {
        Unit,
        Locality,
        TaxonomicName,
        LocalityVisit,
        Store,
        Publication,
        CustomValueDateTime,
        CustomValueString,
        CustomValueNumber,
        Dataset,
        Project
    }
}
