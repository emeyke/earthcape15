using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Importer
{
    public enum ErrorHandling
    {
        StepOver,
        ReportAndStepOver,
        ReportAndStop,
        ReportAndImportPartially
    }
}
