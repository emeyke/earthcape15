﻿using System;
using System.Data.OleDb;
using System.Collections.ObjectModel;

using System.IO;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System.Windows.Forms;
using EarthCape.Module.Core;

using EarthCape.Taxonomy;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using DevExpress.ExpressApp.Xpo;
//using SharpKml.Engine;
using TatukGIS.NDK;
using Xpand.ExpressApp.Security.Core;

namespace EarthCape.Module.Win
{
    public class Importer
    {
        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, string parent, string area, string alt1, string date, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT, string admin)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                if (admin == "")
                {
                    loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                    if (loc == null)
                        loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                }
                if (admin == "country")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<Country>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<Country>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<Country>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<Country>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm1")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm1>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm1>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm1>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm1>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm2")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm2>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm2>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm2>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm2>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm3")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm3>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm3>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm3>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm3>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm4")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm4>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm4>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm4>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm4>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
            }
            if (loc == null)
            {
                if (createnew)
                {
                    if (admin == "")
                    {
                        loc = new Locality(uow);
                    }
                    if (admin == "country")
                    {
                        loc = new Country(uow);
                    }
                    if (admin == "adm1")
                    {
                        loc = new LocalityAdm1(uow);
                    }
                    if (admin == "adm2")
                    {
                        loc = new LocalityAdm2(uow);
                    }
                    if (admin == "adm3")
                    {
                        loc = new LocalityAdm3(uow);
                    }
                    if (admin == "adm4")
                    {
                        loc = new LocalityAdm4(uow);
                    }
                    loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
               /* Note newnote = new Note(uow, note);
                if (!String.IsNullOrEmpty(date)) 
                newnote.RecordedOn = DateTime.Parse(date);
                newnote.Save();
                loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }

        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, LocalityAdm parent, string area, string alt1, string date, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                //if (parent ==null)
                //{
                    loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                    if (loc == null)
                        loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                /*}else{

                    loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Category.Oid", parent.Oid)));
                }*/
            }
            if (loc == null)
            {
                if (createnew)
                {
                        loc = new Locality(uow);
                        loc.Category = parent;
                       loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
                /* Note newnote = new Note(uow, note);
                 if (!String.IsNullOrEmpty(date)) 
                 newnote.RecordedOn = DateTime.Parse(date);
                 newnote.Save();
                 loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }

        private static void LatLongs(OleDbDataReader reader,out double? X, out double? Y)
        {
            X = null;
            Y = null;
            if (reader.FieldExists("latdeg"))
                if (reader.FieldExists("latmin"))
                    if (reader.FieldExists("latsec"))
                        if (reader.FieldExists("longdeg"))
                            if (reader.FieldExists("longmin"))
                                if (reader.FieldExists("longsec"))
                                {
                                   
                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (!String.IsNullOrEmpty(reader["latmin"].ToString())) && (!String.IsNullOrEmpty(reader["latsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["latdeg"].ToString());
                                        double min = Convert.ToDouble(reader["latmin"].ToString());
                                        double sec = Convert.ToDouble(reader["latsec"].ToString());
                                        Y = deg + (min + sec / 60) / 60;
                                    }


                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (!String.IsNullOrEmpty(reader["latmin"].ToString())) && (String.IsNullOrEmpty(reader["latsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["latdeg"].ToString());
                                        double min = Convert.ToDouble(reader["latmin"].ToString());
                                        Y = deg + min / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (String.IsNullOrEmpty(reader["latmin"].ToString())) && (String.IsNullOrEmpty(reader["latsec"].ToString())))
                                        Y = Convert.ToDouble(reader["latdeg"].ToString());
                                    if (reader.FieldExists("NS"))
                                    {
                                        if (reader["NS"].ToString() == "S")
                                        {
                                            Y = Y * (-1);
                                        }
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (!String.IsNullOrEmpty(reader["longmin"].ToString())) && (!String.IsNullOrEmpty(reader["longsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["longdeg"].ToString());
                                        double min = Convert.ToDouble(reader["longmin"].ToString());
                                        double sec = Convert.ToDouble(reader["longsec"].ToString());
                                        X = deg + (min + sec / 60) / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (!String.IsNullOrEmpty(reader["longmin"].ToString())) && (String.IsNullOrEmpty(reader["longsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["longdeg"].ToString());
                                        double min = Convert.ToDouble(reader["longmin"].ToString());
                                        X = deg + min / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (String.IsNullOrEmpty(reader["longmin"].ToString())) && (String.IsNullOrEmpty(reader["longsec"].ToString())))
                                        X = Convert.ToDouble(reader["longdeg"].ToString());
                                    if (reader.FieldExists("WE"))
                                    {
                                        if (reader["WE"].ToString() == "W")
                                        {
                                            X = X * (-1);
                                        }
                                    }
                                  
                                   /* if ((X != null) && (Y != null))
                                    {
                                        to.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                                        //to.Centroid_X = X;
                                        //to.Centroid_Y = Y;
                                    }*/
                                }
        }

 
         public static void Units(string strConn, string AltSConn, UnitOfWork uow, String fn, bool CreateLocalityVisitForUnits, IfFieldExists ifFieldExists, bool IgnoreDuplicateUnitId)
        {
            OleDbConnection cn = null;
           try
            {
                cn = new OleDbConnection(strConn);
                cn.Open();
            }
            catch
            {
               // cn = new OleDbConnection(AltSConn);
                //cn.Open();
   
            }
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            OleDbDataReader reader = cmd.ExecuteReader();
            string _Genus = "", _Taxon = "", _Sampling = "", _Place = "", _Date1 = "", _Date2 = "", _Type = "", _Medium = "", _Sex = "", _Parent = "", _Stage = "";
            //LocalityVisit pv = null;
            //Unit to = null;
            Collection<string> fields = new Collection<string>();
            //StringWriter stringWrite = new StringWriter();
            // fields.Add("UserID");
            fields.Add("UnitID");
           // fields.Add("AssociationRecID");
            fields.Add("Parent");
            fields.Add("SpecimenType");
            //fields.Add("IndividualID");
           // fields.Add("RecID");
            fields.Add("Storage");
         //   fields.Add("PersonCreated"); todo introduce recordedBy?
            fields.Add("Date1");
            fields.Add("Date2");
            fields.Add("Genus");
            fields.Add("Area");
            fields.Add("Species");
            fields.Add("Father");
            fields.Add("Notes");
            fields.Add("Mother");
            fields.Add("Collection");
            fields.Add("FieldID");
            fields.Add("UnitType");
            fields.Add("Type");
            fields.Add("TypeStatus");
            fields.Add("Medium");
            fields.Add("Sex");
            fields.Add("Stage");
            fields.Add("CollectedBy");
            fields.Add("Year");
            fields.Add("Month");
            fields.Add("Day");
            fields.Add("Locality");
            fields.Add("Locality details");
            //fields.Add("Owner");
            //fields.Add("SourceReference");
            fields.Add("Dataset");
            fields.Add("Project");
            fields.Add("Organization");
            //fields.Add("SourceComments");
            fields.Add("Comments");
            //fields.Add("AssociationType");
            // fields.Add("AssociationRole");
            // fields.Add("AssociationRole1");
            fields.Add("WKT");
            fields.Add("X");
            fields.Add("Y");
            fields.Add("x");
            fields.Add("y");
            fields.Add("Altitude1");
            fields.Add("Altitude2");
            fields.Add("Count");
            fields.Add("BirthDate");
            fields.Add("DeathDate");
            fields.Add("QuantityUnit");
            fields.Add("Locality1");
            fields.Add("Cf");
            fields.Add("IdentifiedBy");
            fields.Add("IdentificationConfirmedBy");
            fields.Add("IdentificationConfirmedYear");
            fields.Add("IdentifiedOn");
            fields.Add("IdentifiedOnYear");
            fields.Add("IdentificationConfirmedOn");
            fields.Add("IdentComments");
            fields.Add("Biology details");
            fields.Add("Habitat details");
            fields.Add("AuthorYear");
            fields.Add("AltDate");
            fields.Add("CollectionMethod");
            fields.Add("latdeg");
            fields.Add("latmin");
            fields.Add("latsec");
            fields.Add("longdeg");
            fields.Add("longmin");
            fields.Add("longsec");
            fields.Add("NS");
            fields.Add("WE");
            fields.Add("OtherId");
            fields.Add("OtherID");
            fields.Add("HostNotes");
            fields.Add("Host");
            fields.Add("AssociatedTaxa");
            fields.Add("EPSG");
            fields.Add("CoordRad");
            fields.Add("CoordSource");
            fields.Add("CoordDetails");
            fields.Add("CoordNotes");
            fields.Add("Country");
            fields.Add("CoordNotes");
            fields.Add("Admin1");
            fields.Add("Admin2");
            fields.Add("Admin3");
            fields.Add("Admin4");
            fields.Add("LabelText");
            fields.Add("IsDead");
            fields.Add("Density");
            fields.Add("Length");
            fields.Add("assembly");
            fields.Add("Assembly");
            fields.Add("ShortName");
            fields.Add("TimeSpent");
            fields.Add("TimeSpentUnit");
            fields.Add("GpsStatus");
            fields.Add("Time1");
            fields.Add("Time2");
  fields.Add("RecordType");
  fields.Add("TemporaryName");
  fields.Add("Preservation");
  fields.Add("WhereInStore");
  fields.Add("PreparationType");
  fields.Add("PreparedBy");
  fields.Add("PreparationDate");
  fields.Add("LocalityVisit");
 fields.Add("ReasonNotValid");
            fields.Add("NotValid");
            fields.Add("ValidLocality");
           fields.Add("dummy");

            //            TagDataset tagDataset = null;
            //Store Store = null;
            //Group project = null;
            //UnitDataset dataset = null;
            //WorkItem workItem = null;




            //  if (SecuritySystem.CurrentUser!=null)
            //User p = uow.FindObject<User>(new BinaryOperator("UserName", SecuritySystem.CurrentUserName));// os.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), os.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            // String setName = String.Format("Unit dataset from {0} by {1}", Path.GetFileName(fn), (SecuritySystem.CurrentUser!=null) ? p.FullName );
            StreamWriter log = null;
            string folder = Path.GetDirectoryName(cn.DataSource);
            string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
            string logpath = folder + "\\" + filename + "_log.txt";
            log = new StreamWriter(logpath, false);
            log.WriteLine("Import started " + DateTime.Now.ToString());

            //  setName = String.Format("locality dataset from {0} by {1}", Path.GetFileName(fn), p.FullName);
            int readerline = 0;
            string recordType = "";
            string TimeSpentUnit = "";
            string TimeSpent = "";
            string date1 = "";
            int year = 0;
            int identifiedOnYear = 0;
            int identificationConfirmedYear = 0;
            string date2 = "";
            string identifiedOn = "";
            string identificationConfirmedOn = "";
            string Time1 = "";
            string Time2 = "";
            string altitude = "";
            string altitude2 = "";
            string notes = "";
            string locname = "";
            string locvisit = "";
            string species = "";
            string genus = "";
            string shortname = "";
            string unitID = "";
            string WKT = "";
           
             
            while (reader.Read())
            {
                try
                {
                    
                     TimeSpentUnit = "";
                     TimeSpent = "";
                     date1 = "";
                     date2 = "";
                     altitude = "";
                     altitude2 = "";
                     notes = "";
                     locname = "";
                     species ="";
                     genus = "";
                     shortname = "";
                     unitID = "";
                     WKT = "";
                     if (reader.FieldExists("Date1"))
                        try
                        {
                            if (! string.IsNullOrEmpty(reader["Date1"].ToString()))
                            date1 = DateTime.Parse(reader["Date1"].ToString()).ToShortDateString();
                        }
                        catch
                        {
                            log.WriteLine("Date1 conversion failed " + reader["Date1"].ToString() + " line "+readerline);
                        }
                    if (reader.FieldExists("Date2"))
                        try
                        {
                            if (!string.IsNullOrEmpty(reader["Date2"].ToString()))
                                date2 = DateTime.Parse(reader["Date2"].ToString()).ToShortDateString();
                        }
                        catch
                        {
                            log.WriteLine("Date2 conversion failed " + reader["Date2"].ToString() + " line " + readerline);
                        }
                    if (reader.FieldExists("IdentifiedOn"))
                        try
                        {
                            if (!string.IsNullOrEmpty(reader["IdentifiedOn"].ToString()))
                                identifiedOn = DateTime.Parse(reader["IdentifiedOn"].ToString()).ToShortDateString();
                        }
                        catch
                        {
                            log.WriteLine("IdentifiedOn conversion failed " + reader["IdentifiedOn"].ToString() + " line " + readerline);
                        }
                    if (reader.FieldExists("IdentificationConfirmedOn"))
                        try
                        {
                            if (!string.IsNullOrEmpty(reader["IdentificationConfirmedOn"].ToString()))
                                identificationConfirmedOn = DateTime.Parse(reader["IdentificationConfirmedOn"].ToString()).ToShortDateString();
                        }
                        catch
                        {
                            log.WriteLine("IdentificationConfirmedOn conversion failed " + reader["IdentificationConfirmedOn"].ToString() + " line " + readerline);
                        } 
                    if (reader.FieldExists("Time1"))
                        try
                        {
                            if (!string.IsNullOrEmpty(reader["Time1"].ToString()))
                                Time1 = DateTime.Parse(reader["Time1"].ToString()).ToShortTimeString();
                        }
                        catch
                        {
                            log.WriteLine("Time1 conversion failed " + reader["Time1"].ToString() + " line " + readerline);
                        }
                    if (reader.FieldExists("Time2"))
                        try
                        {
                            if (!string.IsNullOrEmpty(reader["Time2"].ToString()))
                                Time2 = DateTime.Parse(reader["Time2"].ToString()).ToShortTimeString();
                        }
                        catch
                        {
                            log.WriteLine("Time2 conversion failed " + reader["Time2"].ToString() + " line " + readerline);
                        }
                   

                    if (reader.FieldExists("RecordType"))
                        recordType = reader["RecordType"].ToString();
                   if (reader.FieldExists("Notes"))
                        notes = reader["Notes"].ToString();
                    if (reader.FieldExists("Locality"))
                        locname = reader["Locality"].ToString();
                    if (reader.FieldExists("Species"))
                        species = reader["Species"].ToString();
                    if (reader.FieldExists("ShortName"))
                        shortname = reader["ShortName"].ToString();
                    if (reader.FieldExists("Genus"))
                        genus = reader["Genus"].ToString();
                    if (reader.FieldExists("UnitID"))
                        unitID = reader["UnitID"].ToString();
                    year = 0;
                    identifiedOnYear = 0;
                    identificationConfirmedYear = 0;
                    if (reader.FieldExists("Year"))
                        if (!String.IsNullOrEmpty(reader["Year"].ToString()))
                        {
                            if (String.IsNullOrEmpty(date1))
                            {
                                year = Convert.ToInt32(reader["Year"].ToString());
                            }
                        }
                    if (reader.FieldExists("IdentifiedOnYear"))
                        if (!String.IsNullOrEmpty(reader["IdentifiedOnYear"].ToString()))
                        {
                            if (String.IsNullOrEmpty(identifiedOn))
                            {
                                identifiedOnYear = Convert.ToInt32(reader["IdentifiedOnYear"].ToString());
                            }
                        }
                    if (reader.FieldExists("IdentificationConfirmedYear"))
                        if (!String.IsNullOrEmpty(reader["IdentificationConfirmedYear"].ToString()))
                        {
                            if (String.IsNullOrEmpty(identificationConfirmedOn))
                            {
                                identificationConfirmedYear = Convert.ToInt32(reader["IdentificationConfirmedYear"].ToString());
                            }
                        } 

                    if (reader.FieldExists("TimeSpentUnit"))
                    {
                        if (!String.IsNullOrEmpty(reader["TimeSpentUnit"].ToString()))
                        {
                            TimeSpentUnit = reader["TimeSpentUnit"].ToString();
                        }
                    }
                    if (reader.FieldExists("TimeSpent"))
                    {
                        if (!String.IsNullOrEmpty(reader["TimeSpent"].ToString()))
                        {
                            TimeSpent = reader["TimeSpent"].ToString();
                        }
                    }
                    bool IsUnit =
                         (
                             (!(String.IsNullOrEmpty(genus)) || !(String.IsNullOrEmpty(shortname)) || !(String.IsNullOrEmpty(species)) || !(String.IsNullOrEmpty(unitID))) || 
                             ( 
                          !(String.IsNullOrEmpty(unitID)) &&
                             (((String.IsNullOrEmpty(locvisit)) || (String.IsNullOrEmpty(locname))) &&
                             !(String.IsNullOrEmpty(date1))
                             )));
                    bool IsLocality =
                        (
                            (String.IsNullOrEmpty(genus)) &&
                             (String.IsNullOrEmpty(species)) &&
                           (String.IsNullOrEmpty(shortname)) &&
                            (String.IsNullOrEmpty(unitID)) &&
                            (String.IsNullOrEmpty(date1)) &&
                            !(String.IsNullOrEmpty(locname))
                            );
                    bool IsLocVisit =
                        (
                            !(String.IsNullOrEmpty(locname)) &&
                            !(String.IsNullOrEmpty(date1))
                            );
                    Locality loc = null;
                    Project project = null;
                    Dataset dataset = null;
                    TaxonomicName name = null;
                   /* if (reader.FieldExists("Organization"))
                        if (!String.IsNullOrEmpty(reader["Organization"].ToString()))
                    {
                        if ((Organization == null) || (Organization.Name != reader["Organization"].ToString()))
                        {
                            Organization = uow.FindObject<Organization>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Organization"].ToString()));
                            if (Organization == null)
                                Organization = uow.FindObject<Organization>(new BinaryOperator("Name", reader["Organization"].ToString()));
                            if (Organization == null)
                            {
                                Organization = new Organization(uow);
                                Organization.Name = reader["Organization"].ToString();
                                Organization.Save();
                            }
                        }
                    }*/

                    if (reader.FieldExists("Project"))
                        if (!String.IsNullOrEmpty(reader["Project"].ToString()))
                    {
                        if ((project == null) || (project.Name != reader["Project"].ToString()))
                        {
                            project = uow.FindObject<Project>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Project"].ToString()));
                            if (project == null)
                                project = uow.FindObject<Project>(new BinaryOperator("Name", reader["Project"].ToString()));
                            if (project == null)
                            {
                                project = new Project(uow);
                                project.Name = reader["Project"].ToString();
                                project.CreateRoles();
                           //     if (Organization != null)
                             //       project.Organization = Organization;
                                project.Save();
                            }
                        }
                    }


                    if (reader.FieldExists("Dataset"))
                        if (!String.IsNullOrEmpty(reader["Dataset"].ToString()))
                    {
                        if ((dataset == null) || (dataset.Name != reader["Dataset"].ToString()))
                        {
                            dataset = uow.FindObject<Dataset>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Dataset"].ToString()));
                            if (dataset == null)
                                dataset = uow.FindObject<Dataset>(new BinaryOperator("Name", reader["Dataset"].ToString()));
                            if (dataset == null)
                            {
                                dataset = new Dataset(uow);
                                dataset.Name = reader["Dataset"].ToString();
                                if (project != null)
                                    dataset.Project = project;
                                dataset.Save();
                            }
                        }
                    }
                   
                    

                    LocalityAdm country = null;
                    LocalityAdm locadm1 = null;
                    LocalityAdm locadm2 = null;
                    LocalityAdm locadm3 = null;
                    LocalityAdm locadm4 = null;
                    if (reader.FieldExists("Country"))
                    {
                        if (!String.IsNullOrEmpty(reader["Country"].ToString()))
                        {
                            country = (Country)CreateLocality(
                                project,
                                uow,
                                reader["Country"].ToString(),
                                "", "",
                                altitude,
                                date1,
                                altitude2,
                                WKT, true,
                                notes,
                                true, false, "country");

                        }
                    }
                    if (reader.FieldExists("Admin1"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin1"].ToString()))
                        {
                            string parent = "";
                            if (country != null) parent = country.Name;
                            locadm1 = (LocalityAdm1)CreateLocality(project, uow, reader["Admin1"].ToString(), parent, "", altitude, date1, altitude2, WKT, true, notes, true, false, "adm1");
                            if (country != null)
                            {
                                locadm1.Parent = country;
                                locadm1.Save();
                            }
                        }
                    }
                    if (reader.FieldExists("Admin2"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin2"].ToString()))
                        {
                            string parent = "";
                            if (locadm1 != null) parent = locadm1.Name;
                            else
                                if (country != null) parent = country.Name;
                            locadm2 = (LocalityAdm2)CreateLocality(project, uow, reader["Admin2"].ToString(), parent, "", altitude, date1, altitude2, WKT, true, notes, true, false, "adm2");
                            if (locadm1 != null)
                            {
                                locadm2.Parent = locadm1;
                                locadm2.Save();
                            }
                            else
                                if (country
                                    != null)
                                {
                                    locadm2.Parent = country;
                                    locadm2.Save();
                                }
                        }
                    }

                    if (reader.FieldExists("Admin3"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin3"].ToString()))
                        {
                            string parent = "";
                            if (locadm2 != null) parent = locadm2.Name;
                            else
                                if (locadm1 != null) parent = locadm1.Name;
                                else
                                    if (country != null) parent = country.Name;
                            locadm3 = (LocalityAdm3)CreateLocality(project, uow, reader["Admin3"].ToString(), parent, "", altitude, date1, altitude2, WKT, true, notes, true, false, "adm3");
                            if (locadm2 != null)
                            {
                                locadm3.Parent = locadm2;
                                locadm3.Save();
                            }
                            else
                                if (locadm1 != null)
                                {
                                    locadm3.Parent = locadm1;
                                    locadm3.Save();
                                }
                                else
                                    if (country
                                        != null)
                                    {
                                        locadm3.Parent = country;
                                        locadm3.Save();
                                    }
                        }
                    }
                    if (reader.FieldExists("Admin4"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin4"].ToString()))
                        {
                            string parent = "";
                            if (locadm3 != null) parent = locadm3.Name;
                            else
                                if (locadm2 != null) parent = locadm2.Name;
                                else
                                    if (locadm1 != null) parent = locadm1.Name;
                                    else
                                        if (country != null) parent = country.Name;
                            locadm4 = (LocalityAdm4)CreateLocality(project, uow, reader["Admin4"].ToString(), parent, "", altitude, date1, altitude2, WKT, true, notes, true, false, "adm4");
                            if (locadm3 != null)
                            {
                                locadm4.Parent = locadm3;
                                locadm4.Save();
                            }
                            else
                                if (locadm2 != null)
                                {
                                    locadm4.Parent = locadm2;
                                    locadm4.Save();
                                }
                                else
                                    if (locadm1 != null)
                                    {
                                        locadm4.Parent = locadm1;
                                        locadm4.Save();
                                    }
                                    else
                                        if (country
                                            != null)
                                        {
                                            locadm4.Parent = country;
                                            locadm4.Save();
                                        }


                        }
                    }
                    if (!String.IsNullOrEmpty(locname))
                    {
                        LocalityAdm lowerLoc = null;
                        if (locadm4 != null)
                            lowerLoc = locadm4;
                        else
                            if (locadm3 != null)
                                lowerLoc = locadm3;
                            else
                                if (locadm2 != null)
                                    lowerLoc = locadm2;
                                else
                                    if (locadm1 != null)
                                        lowerLoc = locadm1;
                                    else
                                        if (country != null)
                                            lowerLoc = country;
                        if (IsLocality)
                        {
                            CalcIWKT(reader, ref altitude, ref altitude2, ref WKT);
                        } 
                        if ((loc == null) || (loc.Name != locname))
                        {
                            loc = CreateLocality(project, uow, locname, lowerLoc, "", altitude, date1, altitude2, WKT, true, notes, true, true);
                        }
                        if (IsLocality)
                        {
                            double? X;
                            double? Y;
                            LatLongs(reader, out X, out Y);
                            if ((X != null) && (Y != null))
                            {
                                loc.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                            }
                            if (reader.FieldExists("EPSG"))
                                if (!String.IsNullOrEmpty(reader["EPSG"].ToString()))
                                    loc.EPSG = Convert.ToInt32(reader["EPSG"].ToString());
                            if (reader.FieldExists("Length"))
                            {
                                if (!String.IsNullOrEmpty(reader["Length"].ToString()))
                                {
                                    loc.Perimeter = Convert.ToDouble(reader["Length"].ToString());
                                }
                            }
                            if (reader.FieldExists("Area"))
                            {
                                if (!String.IsNullOrEmpty(reader["Area"].ToString()))
                                {
                                    loc.Area = Convert.ToDouble(reader["Area"].ToString());
                                }
                            }
                        }
                        if (reader.FieldExists("ReasonNotValid"))
                        {
                            if (!String.IsNullOrEmpty(reader["ReasonNotValid"].ToString()))

                                loc.ReasonNotValid = reader["ReasonNotValid"].ToString();
                        }
                        if (reader.FieldExists("NotValid"))
                        {
                            if (!String.IsNullOrEmpty(reader["NotValid"].ToString()))
                            {
                                if (Convert.ToBoolean(reader["NotValid"]) == true)
                                    loc.NotValid = true;
                                if (Convert.ToBoolean(reader["NotValid"]) == false)
                                    loc.NotValid = false;
                            }
                        }
                        if (reader.FieldExists("ValidLocality"))
                        {
                            if (!String.IsNullOrEmpty(reader["ValidLocality"].ToString()))
                            {
                                Locality locValid = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                if (locValid == null)
                                    locValid = uow.FindObject<Locality>(new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                if (locValid != null)
                                {
                                    loc.ValidLocality = locValid;
                                }
                            }


                        }
                        
                        if ((loc != null) && (lowerLoc!=null))
                        {
                            loc.Category = lowerLoc;
                            loc.Save();
                        }
                        else if (lowerLoc != null)
                        { loc = lowerLoc; }
                        /*if (reader.FieldExists("LocalityClassification"))
                        {
                            Locality loc_cat_root = null;
                            Locality loc_cat_sub = null;
                            if (reader.FieldExists("LocalityCategoryRoot"))
                            {
                                if (!(String.IsNullOrEmpty(reader["LocalityCategoryRoot"].ToString())))
                                    loc_cat_root = CreateLocality(Group, uow, reader["LocalityCategoryRoot"].ToString(), "", "", altitude, date1, altitude2, WKT, true, null, true, false, "");
                            }
                            if (loc_cat_root != null)
                            {
                                if (reader.FieldExists("LocalityCategorySub"))
                                {
                                    if (!(String.IsNullOrEmpty(reader["LocalityCategorySub"].ToString())))
                                        loc_cat_sub = CreateLocality(Group, uow, reader["LocalityCategorySub"].ToString(), loc_cat_root.Name, "", altitude, date1, altitude2, WKT, true, null, true, false, "");
                                }
                                ClassifyLocality(uow, loc, loc_cat_root, loc_cat_sub, reader["LocalityClassification"].ToString());
                            }
                        }*/
                    }
                    if ((String.IsNullOrEmpty(genus)) &&
                        (String.IsNullOrEmpty(unitID)) &&
                        ((!(String.IsNullOrEmpty(locname))) &&
                        (!(String.IsNullOrEmpty(date1)) || (year > 0)) &&
                        CreateLocalityVisitForUnits
                        ))
                    {

                        LocalityVisit pv = GetLocalityVisit(project, uow, locname, date1, date2, altitude, altitude2, true, TimeSpent, TimeSpentUnit, year);
                        TagHelper.CreateTags(dataset, uow, reader, fields, pv, date1, log, ifFieldExists);
                    }
                    else
                    {
                        if ((String.IsNullOrEmpty(genus)) && (String.IsNullOrEmpty(unitID)) && ((!(String.IsNullOrEmpty(locname)))))
                        {
                            //uow.CommitChanges();
                            TagHelper.CreateTags(dataset, uow, reader, fields, loc, date1, log, ifFieldExists);
                        }
                    }
                        if (IsUnit)
                        {                       
                               Unit to = null;
                                if (!String.IsNullOrEmpty(unitID))
                                {

                                    if (!IgnoreDuplicateUnitId)
                                    {
                                        to = uow.FindObject<Unit>(CriteriaOperator.And(new BinaryOperator("UnitID", unitID), new BinaryOperator("Dataset.Project.Oid", project.Oid)));
                                        if (to == null)
                                            to = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.BeforeTransaction, CriteriaOperator.And(new BinaryOperator("UnitID", unitID), new BinaryOperator("Dataset.Project.Oid", project.Oid)));
                                    }
                                }
                                if (to == null)
                                    to = new Unit(uow);
                                /* if (reader["SpecimenType"].ToString() == "Zoological")
                                     to = uow.CreateObject<UnitZoological>();
                                 if (reader["SpecimenType"].ToString() == "Botanical")
                                     to = uow.CreateObject<UnitBotanical>();
                                 if (reader["SpecimenType"].ToString() == "Mycological")
                                     to = uow.CreateObject<UnitMycological>();
                                 if (to == null)
                                 {
                                     MessageBox.Show("SpecimenType is not defined!");
                                     return;
                                 }*/
                                to.UpdateInfo();
                                to.Save();
                                if (project != null)
                                    to.Dataset = dataset;
                                //  RecID1.Add(reader["RecID"].ToString());
                                //  RecID2.Add(Convert.ToString(to.Oid));
                                if (reader.FieldExists("Locality details"))
                                    if (!String.IsNullOrEmpty(reader["Locality details"].ToString()))
                                    {
                                        to.PlaceDetails = reader["Locality details"].ToString();
                                    }
                                if (CreateLocalityVisitForUnits == true)
                                {
                                    if (!String.IsNullOrEmpty(locname))
                                    {
                                       

                                        LocalityVisit pv = GetLocalityVisit(project, uow,
                                             locname,
                                            date1,
                                             date2,
                                             altitude, altitude2, false, TimeSpent, TimeSpentUnit,year);
                                        if (pv != null)
                                            to.LocalityVisit = pv;
                                    }
                                }

                                #region tags

                                TagHelper.CreateTags(dataset, uow, reader, fields, to, date1, log, ifFieldExists);
                                if (loc != null)
                                {
                                    to.Locality = loc;
                                    //  if (loc != null)
                                    //  if (Group != null)
                                    //  loc.Groups.Add(Group);
                                }
                                DateTime _date1;
                                if (!string.IsNullOrEmpty(date1))
                                {
                                    _date1 = DateTime.Parse(date1);
                                    if (reader.FieldExists("Time1"))
                                    {
                                        if (!string.IsNullOrEmpty(reader["Time1"].ToString()))
                                            to.Date = DateTime.Parse(date1+" "+Time1);
                                        to.Date = _date1;
                                    }
                                }
                                DateTime _date2;
                                if (!string.IsNullOrEmpty(date2))
                                {
                                    _date2 = DateTime.Parse(date2);
                                    to.EndOn = _date2;
                                }
                                DateTime _IdentifiedOn;
                                if (!string.IsNullOrEmpty(identifiedOn))
                                {
                                    _IdentifiedOn = DateTime.Parse(identifiedOn);
                                    to.IdentifiedOn = _IdentifiedOn;
                                }
                               DateTime _IdentificationConfirmedOn;
                                if (!string.IsNullOrEmpty(identificationConfirmedOn))
                                {
                                    _IdentificationConfirmedOn = DateTime.Parse(identificationConfirmedOn);
                                    //todo to.IdentificationConfirmedOn = _IdentificationConfirmedOn;
                                }
                                #endregion

                                // FileAttachment f = uow.FindObject<FileAttachment>(new BinaryOperator("Oid", ((FileAttachment)(View.CurrentObject)).Oid));
                                if (reader.FieldExists("FieldID"))
                                    if (!String.IsNullOrEmpty(reader["FieldID"].ToString()))
                                    {
                                        to.FieldID = reader["FieldID"].ToString();

                                    }
                                //    to.Dataset = dataset;

                                /* if ((!String.IsNullOrEmpty(reader["AssociationRecID"].ToString())) && (RecID1.IndexOf(reader["AssociationRecID"].ToString()) > -1))
                                 {
                                     Unit oc = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Oid", RecID2[RecID1.IndexOf(reader["AssociationRecID"].ToString())]));
                                     if (oc == null)
                                         oc = uow.FindObject<Unit>(new BinaryOperator("Oid", RecID2[RecID1.IndexOf(reader["AssociationRecID"].ToString())]));
                                     if (oc != null)
                                     {
                                         DoAssociations(uow, reader, to, oc);
                                     }
                                 }*/

                                if ((!String.IsNullOrEmpty(genus)) || (!String.IsNullOrEmpty(species)))
                                {
                                    TaxonomicName tc = TaxonomyHelper.GetTaxon(project, uow, genus, species, (reader.FieldExists("AuthorYear")) ? reader["AuthorYear"].ToString() : "");
                                    if (tc != null)
                                    {
                                        to.TaxonomicName = tc;
                                        if (reader.FieldExists("ShortName"))
                                        {
                                            if (!String.IsNullOrEmpty(reader["ShortName"].ToString()))
                                            {
                                                tc.ShortName = reader["ShortName"].ToString();
                                                tc.Save();
                                            }
                                        }

                                        // if (tc != null)
                                        // if (Group != null)
                                        //  if (Group != tc.Project)
                                        //   tc.Groups.Add(Group);
                                    }
                                    tc.UpdateInfo();
                                    tc.Save();
                                }
                                if ((String.IsNullOrEmpty(genus)) && (String.IsNullOrEmpty(species)) && (!String.IsNullOrEmpty(shortname)))
                                {
                                    Species _species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("ShortName",shortname));
                                    if (_species == null)
                                    {
                                        _species = new Species(uow);
                                        _species.ShortName = reader["ShortName"].ToString();                                        
                                        _species.UpdateInfo();
                                        _species.Save();
                                    }
                                    _species.UpdateInfo();
                                    _species.Save();
                                    to.TaxonomicName = _species;
                                }
                                if (reader.FieldExists("WKT"))
                                {
                                    if (!String.IsNullOrEmpty(reader["WKT"].ToString()))
                                    {
                                        to.WKT = reader["WKT"].ToString();
                                    }
                                }
                                if (String.IsNullOrEmpty(to.WKT))
                                    if (reader.FieldExists("X"))
                                        if (!String.IsNullOrEmpty(reader["X"].ToString()))
                                        {
                                            if (reader.FieldExists("Y"))
                                                if (!String.IsNullOrEmpty(reader["Y"].ToString()))
                                                {
                                                    double y = Convert.ToDouble(reader["Y"].ToString());
                                                    double x = Convert.ToDouble(reader["X"].ToString());
                                                    to.WKT = String.Format("POINT ({0} {1})", x.ToString().Replace(",", "."), y.ToString().Replace(",", "."));
                                                }
                                        }
                               
                                /* if (to.WKT == null)
                                     if ((to.Place != null) && (to.Place.WKT != null))
                                         to.WKT = to.Place.WKT;*/
                                if (!String.IsNullOrEmpty(altitude))
                                {
                                    to.Altitude1 = Convert.ToDouble(altitude);
                                }
                                if (!String.IsNullOrEmpty(altitude2))
                                {
                                    to.Altitude2 = Convert.ToDouble(altitude2);
                                }
                                if (year > 0)
                                    to.Year = year;
                                if (identifiedOnYear > 0)
                                    to.IdentifiedOnYear = identifiedOnYear;
                                if (reader.FieldExists("Month"))
                                    if (!String.IsNullOrEmpty(reader["Month"].ToString()))
                                    {
                                        if (String.IsNullOrEmpty(date1))
                                        {
                                            to.Month = Convert.ToInt32(reader["Month"].ToString());
                                        }
                                    }
                                if (reader.FieldExists("Day"))
                                    if (!String.IsNullOrEmpty(reader["Day"].ToString()))
                                    {
                                        if (String.IsNullOrEmpty(date1))
                                        {
                                            to.Day = Convert.ToInt32(reader["Day"].ToString());
                                        }
                                    }
                                if ((to.Day > 0) && (to.Month > 0) && (to.Year > 0))
                                    to.Date = new DateTime(to.Year.Value, to.Month.Value, to.Day.Value);
                                if (reader.FieldExists("Locality1"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Locality1"].ToString()))
                                    {
                                        Locality loc1 = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Locality1"].ToString()));
                                        if (loc1 == null)
                                            loc1 = uow.FindObject<Locality>(new BinaryOperator("Name", reader["Locality1"].ToString()));
                                        if (loc1 == null)
                                        {
                                            loc1 = new Locality(uow);
                                            if (project != null) loc1.Project = project;
                                            loc1.Name = reader["Locality1"].ToString();
                                            loc1.Save();
                                        }
                                        to.Locality1 = loc1;
                                    }


                                }
                                if (!String.IsNullOrEmpty(date1))
                                {
                                    to.Date = DateTime.Parse(date1);
                                }
                                if (reader.FieldExists("DeathDate"))
                                    if (!String.IsNullOrEmpty(reader["DeathDate"].ToString()))
                                    {
                                        to.DeathDate = DateTime.Parse(reader["DeathDate"].ToString());
                                    }
                                if (reader.FieldExists("BirthDate"))
                                    if (!String.IsNullOrEmpty(reader["BirthDate"].ToString()))
                                    {
                                        to.BirthDate = DateTime.Parse(reader["BirthDate"].ToString());
                                    }
                                if (!String.IsNullOrEmpty(date2))
                                {
                                    to.EndOn = DateTime.Parse(date2);
                                }




                                if (!String.IsNullOrEmpty(unitID))
                                {
                                    to.UnitID = unitID;
                                }
                                if (reader.FieldExists("Comments"))
                                    if (!String.IsNullOrEmpty(reader["Comments"].ToString()))
                                    {
                                        to.Comment = reader["Comments"].ToString();
                                    }
                                if (reader.FieldExists("CollectionMethod"))
                                {
                                    if (!String.IsNullOrEmpty(reader["CollectionMethod"].ToString()))
                                    {
                                        to.CollectionMethod = reader["CollectionMethod"].ToString();
                                    }
                                }

                                if (reader.FieldExists("IdentifiedBy"))
                                {
                                    if (!String.IsNullOrEmpty(reader["IdentifiedBy"].ToString()))
                                    {
                                        to.IdentifiedBy = reader["IdentifiedBy"].ToString();
                                    }
                                }
                                if (reader.FieldExists("IdentificationConfirmedBy"))
                                {
                                    if (!String.IsNullOrEmpty(reader["IdentificationConfirmedBy"].ToString()))
                                    {
                                        to.IdentificationConfirmedBy = reader["IdentificationConfirmedBy"].ToString();
                                    }
                                }
                               
                                
                                if (reader.FieldExists("LabelText"))
                                {
                                    if (!String.IsNullOrEmpty(reader["LabelText"].ToString()))
                                    {
                                        to.LabelText = reader["LabelText"].ToString();
                                    }
                                }
                                if (reader.FieldExists("TemporaryName"))
                                {
                                    if (!String.IsNullOrEmpty(reader["TemporaryName"].ToString()))
                                    {
                                        to.TemporaryName = reader["TemporaryName"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Collection"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Collection"].ToString()))
                                    {
                                        to.CollectionCode = reader["Collection"].ToString();
                                    }
                                }
                                if (reader.FieldExists("IsDead"))
                                {
                                    if (reader["IsDead"].ToString() == "TRUE")
                                    {
                                        to.IsDead = true;
                                    }
                                    if (reader["IsDead"].ToString() == "FALSE")
                                    {
                                        to.IsDead = false;
                                    }
                                } 
                                if (reader.FieldExists("Cf"))
                                {
                                    if (reader["Cf"].ToString() == "TRUE")
                                    {
                                        to.Cf = true;
                                    }
                                    if (reader["Cf"].ToString() == "FALSE")
                                    {
                                        to.Cf = false;
                                    }
                                }
                                if (reader.FieldExists("OtherId"))
                                    if (!String.IsNullOrEmpty(reader["OtherId"].ToString()))
                                        to.OtherId = reader["OtherId"].ToString();
                                if (reader.FieldExists("LocalityVisit"))
                                    if (!String.IsNullOrEmpty(reader["LocalityVisit"].ToString()))
                                        to.LocalityVisit = GetLocVisitByID(project, uow,reader["LocalityVisit"].ToString());
                                if (reader.FieldExists("OtherID"))
                                    if (!String.IsNullOrEmpty(reader["OtherID"].ToString()))
                                        to.OtherId = reader["OtherID"].ToString();
                                if (reader.FieldExists("HostNotes"))
                                    if (!String.IsNullOrEmpty(reader["HostNotes"].ToString()))
                                        to.HostDetails = reader["HostNotes"].ToString();


                                if (reader.FieldExists("CoordRad"))
                                    if (!String.IsNullOrEmpty(reader["CoordRad"].ToString()))
                                        to.CoordRadius = Convert.ToDouble(reader["CoordRad"].ToString());
                                if (reader.FieldExists("CoordSource"))
                                    if (!String.IsNullOrEmpty(reader["CoordSource"].ToString()))
                                        to.CoordSource = reader["CoordSource"].ToString();
                                if (reader.FieldExists("CoordDetails"))
                                    if (!String.IsNullOrEmpty(reader["CoordDetails"].ToString()))
                                        to.CoordDetails = reader["CoordDetails"].ToString();
                                if (reader.FieldExists("CoordNotes"))
                                    if (!String.IsNullOrEmpty(reader["CoordNotes"].ToString()))
                                        to.CoordDetails = reader["CoordNotes"].ToString();

                                double? X;
                                double? Y;
                                LatLongs(reader, out X, out Y);
                                if ((X != null) && (Y != null))
                                {
                                    to.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                                }
                                if (reader.FieldExists("EPSG"))
                                    if (!String.IsNullOrEmpty(reader["EPSG"].ToString()))
                                        to.EPSG = Convert.ToInt32(reader["EPSG"].ToString());
                                if (reader.FieldExists("IdentComments"))
                                {
                                    if (!String.IsNullOrEmpty(reader["IdentComments"].ToString()))
                                    {
                                        to.IdentificationComments = reader["IdentComments"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Habitat details"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Habitat details"].ToString()))
                                    {
                                        to.HabitatDetails = reader["Habitat details"].ToString();
                                    }
                                }
                                if (reader.FieldExists("CollectionMethod"))
                                {
                                    if (!String.IsNullOrEmpty(reader["CollectionMethod"].ToString()))
                                    {
                                        to.CollectionMethod = reader["CollectionMethod"].ToString();
                                    }
                                }
                                if (reader.FieldExists("AssociatedTaxa"))
                                {
                                    if (!String.IsNullOrEmpty(reader["AssociatedTaxa"].ToString()))
                                    {
                                        to.AssociatedTaxa = reader["AssociatedTaxa"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Biology details"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Biology details"].ToString()))
                                    {
                                        to.BiologyDetails = reader["Biology details"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Biology details"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Biology details"].ToString()))
                                    {
                                        to.BiologyDetails = reader["Biology details"].ToString();
                                    }
                                }
                                if (reader.FieldExists("AltDate"))
                                {
                                    if (!String.IsNullOrEmpty(reader["AltDate"].ToString()))
                                    {
                                        to.AlternativeDate = reader["AltDate"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Preservation"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Preservation"].ToString()))
                                    {
                                        to.Preservation = reader["Preservation"].ToString();
                                    }
                                }
                                if (reader.FieldExists("PreparationDate"))
                                {
                                    if (!String.IsNullOrEmpty(reader["PreparationDate"].ToString()))
                                    {
                                        to.PreparationDate = DateTime.Parse(reader["PreparationDate"].ToString());
                                    }
                                }
                               
                                if (reader.FieldExists("PreparedBy"))
                                {
                                    if (!String.IsNullOrEmpty(reader["PreparedBy"].ToString()))
                                    {
                                        to.PreparedBy = reader["PreparedBy"].ToString();
                                    }
                                }
                                if (reader.FieldExists("PreparationType"))
                                {
                                    if (!String.IsNullOrEmpty(reader["PreparationType"].ToString()))
                                    {
                                        to.PreparationType = reader["PreparationType"].ToString();
                                    }
                                }
                                if (reader.FieldExists("WhereInStore"))
                                {
                                    if (!String.IsNullOrEmpty(reader["WhereInStore"].ToString()))
                                    {
                                        to.WhereInStore = reader["WhereInStore"].ToString();
                                    }
                                }
                                if (reader.FieldExists("Stage"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Stage"].ToString()))
                                    {
                                        try
                                        {
                                            DevelopmentStage stage = (DevelopmentStage)Enum.Parse(typeof(DevelopmentStage), reader["Stage"].ToString(), true);
                                            to.Stage = stage;
                                        }
                                        catch (Exception ex)
                                        {
                                            log.WriteLine(String.Format("{0}\tline: {1}\t{2}\t{3} (failed \"Stage\" value conversion:{4})", DateTime.Now, readerline, unitID, ex.Message, reader["Stage"].ToString()));

                                        }
                                        finally
                                        {

                                        }
                                    }
                                }
                                if (reader.FieldExists("Sex"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Sex"].ToString()))
                                    {
                                        if ((reader["Sex"].ToString() == "Male") || (reader["Sex"].ToString() == "male"))
                                            to.Sex = Sex.Male;
                                        if ((reader["Sex"].ToString() == "Female") || (reader["Sex"].ToString() == "female"))
                                            to.Sex = Sex.Female;
                                        if ((reader["Sex"].ToString() == "NotSpecified") || (reader["Sex"].ToString() == "not specified"))
                                            to.Sex = Sex.NotSpecified;
                                        if ((reader["Sex"].ToString() == "Unknown") || (reader["Sex"].ToString() == "unknown"))
                                            to.Sex = Sex.Unknown;
                                    }
                                }
                                if (reader.FieldExists("Type"))
                                {
                                    if (!String.IsNullOrEmpty(reader["Type"].ToString()))
                                    {
                                        if ((reader["Type"].ToString() == "Hapantotype") || (reader["Type"].ToString() == "hapantotype"))
                                            to.Type = SpecimenType.Hapantotype;
                                        if ((reader["Type"].ToString() == "Holotype") || (reader["Type"].ToString() == "holotype"))
                                            to.Type = SpecimenType.Holotype;
                                        if ((reader["Type"].ToString() == "Paratype") || (reader["Type"].ToString() == "paratype"))
                                            to.Type = SpecimenType.Paratype;
                                        if ((reader["Type"].ToString() == "Neotype") || (reader["Type"].ToString() == "neotype"))
                                            to.Type = SpecimenType.Neotype;
                                        if ((reader["Type"].ToString() == "Syntype") || (reader["Type"].ToString() == "syntype"))
                                            to.Type = SpecimenType.Syntype;
                                        if ((reader["Type"].ToString() == "Lectotype") || (reader["Type"].ToString() == "lectotype"))
                                            to.Type = SpecimenType.Lectotype;
                                        if ((reader["Type"].ToString() == "Paralectotype") || (reader["Type"].ToString() == "paralectotype"))
                                            to.Type = SpecimenType.Paralectotype;


                                      if ((reader["Type"].ToString() == "Type") || (reader["Type"].ToString() == "type"))
                                            to.Type = SpecimenType.Type;
                                      if ((reader["Type"].ToString() == "Isoepitype") || (reader["Type"].ToString() == "isoepitype"))
                                            to.Type = SpecimenType.Isoepitype;
                                      if ((reader["Type"].ToString() == "Isolectotype") || (reader["Type"].ToString() == "isolectotype"))
                                            to.Type = SpecimenType.Isolectotype;
                                      if ((reader["Type"].ToString() == "Isoneotype") || (reader["Type"].ToString() == "isoneotype"))
                                            to.Type = SpecimenType.Isoneotype;
                                      if ((reader["Type"].ToString() == "Isotype") || (reader["Type"].ToString() == "isotype"))
                                            to.Type = SpecimenType.Isotype;
                                    }
                                }

                                if (reader.FieldExists("TypeStatus"))
                                {
                                    if (!String.IsNullOrEmpty(reader["TypeStatus"].ToString()))
                                    {
                                        if ((reader["TypeStatus"].ToString() == "NotVerified") || (reader["TypeStatus"].ToString() == "not verified"))
                                            to.TypeStatus = TypeStatus.NotVerified;
                                        if ((reader["TypeStatus"].ToString() == "Verified") || (reader["TypeStatus"].ToString() == "verified"))
                                            to.TypeStatus = TypeStatus.Verified;
                                    }
                                }

                                Unit _Parent_ = null;
                                if (reader.FieldExists("Parent"))
                                    if (!String.IsNullOrEmpty(reader["Parent"].ToString()))
                                    {
                                        _Parent_ = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("UnitID", reader["Parent"].ToString()));
                                        if (_Parent_ == null)
                                            _Parent_ = uow.FindObject<Unit>(new BinaryOperator("UnitID", reader["Parent"].ToString()));
                                        if (_Parent_ == null)
                                        {
                                            //stringWrite.WriteLine(reader["UnitID"].ToString() + "/" + reader["Parent"].ToString());
                                        }
                                    }
                                if (reader.FieldExists("assembly"))
                                {
                                    if (!String.IsNullOrEmpty(reader["assembly"].ToString()))
                                    {
                                        Unit assembly = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("UnitID", reader["assembly"].ToString()));
                                        if (assembly == null)
                                            assembly = uow.FindObject<Unit>(new BinaryOperator("UnitID", reader["assembly"].ToString()));
                                        if (assembly == null)
                                        {
                                            //stringWrite.WriteLine(reader["UnitID"].ToString() + "/" + reader["Parent"].ToString());
                                        }
                                        else
                                        {
                                            to.UnitAssembly = assembly;
                                        }
                                    }
                                }
                                Unit _Father_ = null;
                                if (reader.FieldExists("Father"))
                                    if (!String.IsNullOrEmpty(reader["Father"].ToString()))
                                    {
                                        _Father_ = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("UnitID", reader["Father"].ToString()));
                                        if (_Father_ == null)
                                            _Father_ = uow.FindObject<Unit>(new BinaryOperator("UnitID", reader["Father"].ToString()));
                                        if (_Father_ == null)
                                        {
                                            //stringWrite.WriteLine(reader["UnitID"].ToString() + "/" + reader["Father"].ToString());
                                        }

                                    }
                                Unit _Mother_ = null;
                                if (reader.FieldExists("Mother"))
                                    if (!String.IsNullOrEmpty(reader["Mother"].ToString()))
                                    {
                                        _Mother_ = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("UnitID", reader["Mother"].ToString()));
                                        if (_Mother_ == null)
                                            _Mother_ = uow.FindObject<Unit>(new BinaryOperator("UnitID", reader["Mother"].ToString()));
                                        if (_Mother_ == null)
                                        {
                                            //stringWrite.WriteLine(reader["UnitID"].ToString() + "/" + reader["Mother"].ToString());
                                        }

                                    }


                                // String unitSetName = String.Format("Unit collection from {0} by {1}", Path.GetFileName(fn), p.FullName);
                                if (reader.FieldExists("Storage"))
                                    if (!String.IsNullOrEmpty(reader["Storage"].ToString()))
                                    {
                                        Store Store = uow.FindObject<Store>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", reader["Storage"].ToString())));
                                        if (Store == null)
                                            Store = uow.FindObject<Store>(CriteriaOperator.And(new BinaryOperator("Name", reader["Storage"].ToString())));
                                        if (Store == null)
                                        {
                                            Store = new Store(uow);
                                            Store.Project = project;
                                            Store.Name = reader["Storage"].ToString();
                                            //  Note newnote = new Note(uow, String.Format("Imported from file {0} by {1}", Path.GetFileName(fn), p.FullName));
                                            //   newnote.Save();
                                            //  Store.Notes.Add(newnote);
                                            Store.Save();
                                        }

                                        if (Store != null)
                                        {
                                            to.Store = Store;
                                        }
                                    }
                                if (reader.FieldExists("UnitType"))
                                {
                                    if (!String.IsNullOrEmpty(reader["UnitType"].ToString()))
                                    {
                                        to.UnitType = reader["UnitType"].ToString();
                                    }
                                }
                                if (reader.FieldExists("UnitID"))
                                    to.UnitID = unitID;

                                if (_Parent_ != null)
                                {
                                    to.DerivedFrom = _Parent_;
                                }
                                if (reader.FieldExists("Notes"))
                                    if (!string.IsNullOrEmpty(notes))
                                    {
                                        Note newnote = new Note(uow, notes);
                                        newnote.Save();
                                        to.NoteSet.Notes.Add(newnote);
                                    }

                                if (_Father_ != null)
                                {
                                    _Father_.Offspring.Add(to);
                                    ((Unit)_Father_).Sex = Sex.Male;
                                    _Father_.Save();
                                    to.Parents.Add(_Father_);

                                }
                                if (_Mother_ != null)
                                {
                                    _Mother_.Offspring.Add(to);
                                    ((Unit)_Mother_).Sex = Sex.Female;
                                    _Mother_.Save();
                                    to.Parents.Add(_Mother_);

                                }

                                if (reader.FieldExists("CollectedBy"))
                                    if (!String.IsNullOrEmpty(reader["CollectedBy"].ToString()))
                                        to.CollectedOrObservedByText = reader["CollectedBy"].ToString();
                                if (reader.FieldExists("Count"))
                                    if (!String.IsNullOrEmpty(reader["Count"].ToString()))
                                        to.Quantity = Convert.ToInt32(reader["Count"]);
                                if (reader.FieldExists("Altitude1"))
                                    if (!String.IsNullOrEmpty(reader["Altitude1"].ToString()))
                                        to.Altitude1 = Convert.ToDouble(reader["Altitude1"]);
                                if (reader.FieldExists("Altitude2"))
                                    if (!String.IsNullOrEmpty(reader["Altitude2"].ToString()))
                                        to.Altitude2 = Convert.ToDouble(reader["Altitude2"]);
                                if (reader.FieldExists("QuantityUnit"))
                                    if (!String.IsNullOrEmpty(reader["QuantityUnit"].ToString()))
                                        to.QuantityUnit = reader["QuantityUnit"].ToString();
                                to.Save();
                                /* if (tagDataset!=null)
                                     Routines.ShareSet(tagDataset, Group, uow, false);*/

                            }
                            else
                            {
                                if ((loc != null))
                                {
                                    if (reader.FieldExists("Comments"))
                                        if (!String.IsNullOrEmpty(reader["Comments"].ToString()))
                                        {
                                            loc.Comment = reader["Comments"].ToString();
                                         }
                                    if (reader.FieldExists("CoordSource"))
                                        if (!String.IsNullOrEmpty(reader["CoordSource"].ToString()))
                                        {
                                            loc.CoordSource = reader["CoordSource"].ToString();
                                         }
                                    if (reader.FieldExists("Notes"))
                                        if (!String.IsNullOrEmpty(reader["Notes"].ToString()))
                                        {
                                            Note newnote = new Note(uow, notes);
                                            newnote.Save();
                                            loc.NoteSet.Notes.Add(newnote);
                                        }
                                    loc.Save();
                                }
                                // log.WriteLine("record skipped:" + "\t" + readerline.ToString());
                            }
                        
                    
                    //uow.CommitChanges();
                    // _Genus = reader["Genus"].ToString();
                    //  _Taxon = reader["Species"].ToString();
                    // _Place = reader["Locality"].ToString();
                    // _Date1 = reader["Date1"].ToString();
                    //_Date2 = reader["Date2"].ToString();
                    // _Type = reader["Type"].ToString();
                    //  _Medium = reader["Medium"].ToString();
                    // _Sex = reader["Sex"].ToString();
                    // _Parent = reader["Parent"].ToString();
                    // _Stage = reader["Stage"].ToString(); ;
                    //_Sampling = String.Format("{0} {1} {2}", reader["Locality"], reader["Date1"], reader["Date2"]);
                }
                catch (Exception ex)
                {
                    log.WriteLine(String.Format("{0}\tline: {1}\t{2}\t{3}", DateTime.Now, readerline, unitID, ex.Message));

                }
                finally
                {

                }
                readerline += 1;
                uow.CommitChanges();
            }
            // workItem.Save();
            try
            {
                log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                uow.CommitChanges();
            }
            catch (Exception ex)
            {
                log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

            }
            finally
            {
                reader.Close();
                cn.Close();
                log.WriteLine("import finished " + DateTime.Now.ToString());
                log.Close();
                Process.Start(logpath);

            }
        }

        private static LocalityVisit GetLocVisitByID(Project project, UnitOfWork uow, string p)
        {
            LocalityVisit locvisit = null;
            locvisit = uow.FindObject<LocalityVisit>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", p), new BinaryOperator("Project.Oid", project.Oid)));
               if (locvisit == null)
            
            {
                locvisit = new LocalityVisit(uow);
                locvisit.Name = p;
                locvisit.Project = project;
                locvisit.Save();
            }
            return locvisit;
        }

      

        private static void CalcIWKT(OleDbDataReader reader, ref string altitude, ref string altitude2, ref string WKT)
        {
            if (reader.FieldExists("Altitude1"))
                altitude = reader["Altitude1"].ToString();
            if (reader.FieldExists("Altitude2"))
                altitude2 = reader["Altitude2"].ToString();
            if (reader.FieldExists("WKT"))
                if (!String.IsNullOrEmpty(reader["WKT"].ToString()))
                {
                    WKT = reader["WKT"].ToString();
                }
            if (String.IsNullOrEmpty(WKT))
            {
                if (reader.FieldExists("X"))
                    if (!String.IsNullOrEmpty(reader["X"].ToString()))
                    {
                        if (reader.FieldExists("Y"))
                            if (!String.IsNullOrEmpty(reader["Y"].ToString()))
                            {
                                double y = Convert.ToDouble(reader["Y"].ToString());
                                double x = Convert.ToDouble(reader["X"].ToString());
                                WKT = String.Format("POINT ({0} {1})", x.ToString().Replace(",", "."), y.ToString().Replace(",", "."));
                            }
                    }
            }
        }

        /* private static void ClassifyLocality(UnitOfWork uow, Locality loc, Locality loc_cat_root, Locality loc_cat_sub, string classification)
         {
             if (string.IsNullOrEmpty(classification)) return;
             if (classification == "") return;
             LocalityClassification localityClassification = uow.FindObject<LocalityClassification>(new BinaryOperator("Name", classification), false);
             if (localityClassification == null)
                 localityClassification = uow.FindObject<LocalityClassification>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", classification));
             if (localityClassification == null)
                 localityClassification = new LocalityClassification(uow);
             localityClassification.Name = classification;


       
             LocalityClassificationItem locclassitem_root = null;
             if (loc_cat_root != null)
             {
                 CriteriaOperator cr = CriteriaOperator.And(new BinaryOperator("Locality.Oid", loc_cat_root.Oid), new BinaryOperator("Classification.Oid", localityClassification.Oid));
                 locclassitem_root = uow.FindObject<LocalityClassificationItem>(
                        cr, false);
                 if (locclassitem_root == null)
                     locclassitem_root = uow.FindObject<LocalityClassificationItem>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                 if (locclassitem_root == null)
                     locclassitem_root = new LocalityClassificationItem(uow);
                 locclassitem_root.Locality = loc_cat_root;
                 locclassitem_root.Classification = localityClassification;
             }

             LocalityClassificationItem locclassitem_sub = null;
             if (loc_cat_sub != null)
             {
                 CriteriaOperator cr = CriteriaOperator.And(new BinaryOperator("Locality.Oid", loc_cat_sub.Oid), new BinaryOperator("Classification.Oid", localityClassification.Oid));
                 locclassitem_sub = uow.FindObject<LocalityClassificationItem>(
                        cr, false);
                 if (locclassitem_sub == null)
                     locclassitem_sub = uow.FindObject<LocalityClassificationItem>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                 if (locclassitem_sub == null)
                     locclassitem_sub = new LocalityClassificationItem(uow);
                 locclassitem_sub.Locality = loc_cat_sub;
                 locclassitem_sub.Classification = localityClassification;
             }

             LocalityClassificationItem targetItem = null;
             if (locclassitem_root != null)
             {
                 targetItem = locclassitem_root;
             }
             if (locclassitem_sub != null)
             {
                 targetItem = locclassitem_sub;
             }



             CriteriaOperator cr1 = CriteriaOperator.And(new BinaryOperator("Locality.Oid", loc.Oid), new BinaryOperator("Category.Oid", targetItem.Oid));
             LocalityClassified locclassitem = uow.FindObject<LocalityClassified>(cr1, false);
             if (locclassitem == null)
                 locclassitem = uow.FindObject<LocalityClassified>(PersistentCriteriaEvaluationBehavior.InTransaction, cr1);
             if (locclassitem == null)
                 locclassitem = new LocalityClassified(uow);
             locclassitem.Locality = loc;
         
             if ((loc_cat_root != null) && (loc_cat_sub != null))
             {
                 if (locclassitem_sub != locclassitem_root)
                 {
                     locclassitem.Category = locclassitem_sub;
                     locclassitem_sub.Parent = locclassitem_root;
                 }
             }
             if ((loc_cat_root == null) && (loc_cat_sub != null))
             {
                 locclassitem.Category = locclassitem_sub;
             }
             if ((loc_cat_root != null) && (loc_cat_sub == null))
             {
                 locclassitem.Category = locclassitem_root;
             }
             if (locclassitem != null)
                 locclassitem.Save();
             if (locclassitem_root != null)
                 locclassitem_root.Save();
             if (locclassitem_sub != null)
                 locclassitem_sub.Save();
         }

       */



        /*   private static void DoAssociations(UnitOfWork uow, OleDbDataReader reader, Unit to, Unit oc)
           {
               Interaction inter = new Interaction(uow);
               try
               {
                   if (!String.IsNullOrEmpty(reader["AssociationType"].ToString()))
                       inter.Type = (InteractionType)(Enum.Parse(typeof(InteractionType), reader["AssociationType"].ToString()));
               }
               finally
               {

               }
               inter.Unit1 = to;
               inter.Unit2 = oc;

               try
               {
                   if (!String.IsNullOrEmpty(reader["AssociationRole"].ToString()))
                       inter.Role1 = (InteractionRole)(Enum.Parse(typeof(InteractionRole), reader["AssociationRole"].ToString()));
               }
                 finally
               {

               }
               try
               {
                   if (!String.IsNullOrEmpty(reader["AssociationRole1"].ToString()))
                       inter.Role2 = (InteractionRole)(Enum.Parse(typeof(InteractionRole), reader["AssociationRole1"].ToString()));
               }
                 finally
               {

               }
               inter.Save();
           }
           */
        public static LocalityVisit GetLocalityVisit(Project project, UnitOfWork uow, String _Geoname, String date1, String date2, String alt1, String alt2, bool createnew, String TimeSpent, String TimeSpentUnit, int year)
        {
            CriteriaOperator cr = null;
            DateTime _date1;
            if (!string.IsNullOrEmpty(date1))
            {
                _date1 = DateTime.Parse(date1);
                if (!(_date1 > DateTime.MinValue))
                    cr = new BinaryOperator("Locality.Name", _Geoname);
                else
                    cr = CriteriaOperator.And(new BinaryOperator("Locality.Name", _Geoname), new BinaryOperator("DateTimeStart", _date1));

                LocalityVisit pv = uow.FindObject<LocalityVisit>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                if (pv == null)
                    pv = uow.FindObject<LocalityVisit>(cr);
                if (pv == null)
                {
                    pv = new LocalityVisit(uow);
                    pv.Locality = CreateLocality(project, uow, _Geoname, "", "", alt1, date1, alt2, "", true, "", true, false, "");
                    if (year > 0)
                        pv.Year = year;
                    if (!String.IsNullOrEmpty(TimeSpent))
                        pv.TimeSpent = Convert.ToDouble(TimeSpent);
                    pv.TimeSpentUnit = TimeSpentUnit;
                    if (_date1 != null)
                    {
                        pv.DateTimeStart = _date1;
                        if (!String.IsNullOrEmpty(date2))
                            pv.DateTimeEnd = DateTime.Parse(date2);
                    }

                    /* if (!String.IsNullOrEmpty(alt1))
                     {
                         pv.Altitude1 = Convert.ToDouble(alt1);
                         if (!String.IsNullOrEmpty(alt2))
                             pv.Altitude1 = Convert.ToDouble(alt2);
                     }
                     if (!String.IsNullOrEmpty(X))
                     {
                         pv.X = Convert.ToDouble(X);
                         if (!String.IsNullOrEmpty(Y))
                         {
                             pv.Y = Convert.ToDouble(Y);
                         }
                     }*/
                    pv.Save();
                }
                return pv;
            }
            return null;
        }
        /*    public static LocalityVisit MakeLocalityVisit(XPObjectSpace objectSpace, Session se, OleDbDataReader reader, Unit to, StringWriter stringWrite)
             {
                 if (!String.IsNullOrEmpty(reader["Locality"].ToString()))
                     try
                     {
                         Place pl = null;
                         LocalityVisit pv = GetLocalityVisit(objectSpace, se, reader["Locality"].ToString(), reader["Date1"].ToString(), reader["Date2"].ToString(), altitude, altitude2, reader["X"].ToString(), reader["Y"].ToString());
                         if (pv == null)
                         {
                             pl =PlaceRoutines.GetPlace(objectSpace, se, reader["Locality"].ToString(), altitude, altitude2, reader["X"].ToString(), reader["Y"].ToString());
                         }
                         else
                         {
                             pv.Place=pl;
                         }
                         to.Date = pv.Date;
                         to.EndOn = pv.EndOn;
                         pv.Save();
                         return pv;
                     }
                     catch
                     {
                         stringWrite.WriteLine(reader["UnitID"].ToString() + "\t" + "error in LocalityVisit");
                         return null;
                     }
                 return null;
             }*/
        /*   private void CreateMembership(XPObjectSpace objectSpace, User user, Group project)
           {
               if (project == null) return;

               GroupMembership grm = objectSpace.Session.FindObject<GroupMembership>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Member", user), new BinaryOperator("Group", project)));
               if (grm == null)
                   grm = objectSpace.Session.FindObject<GroupMembership>(CriteriaOperator.And(new BinaryOperator("Member", user), new BinaryOperator("Group", project)));
               if (grm == null)
               {
                   grm = objectSpace.CreateObject<GroupMembership>();
                   grm.Member = user;
                   grm.Project = project;
                   grm.Save();
               }
           }*/




    //    private static Collection<string> RecID1 = new Collection<string>();
     //   private static Collection<string> RecID2 = new Collection<string>();

        private void Locs(string strConn, XPObjectSpace objectSpace, Session se, String fn)
        {
            string strSQL = "SELECT * FROM [Sheet1$]";
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand(strSQL, cn);
            OleDbDataReader reader = cmd.ExecuteReader();
            //    User p = objectSpace.GetObjectByKey(SecuritySystem.CurrentUser.GetType(), objectSpace.GetKeyValue(SecuritySystem.CurrentUser)) as User;

            while (reader.Read())
            {

                if (reader["Project"].ToString() != "")
                {
                    Project project = objectSpace.FindObject<Project>(new BinaryOperator("Name", reader["Project"].ToString()), true);
                    if (project == null)
                        project = objectSpace.FindObject<Project>(new BinaryOperator("Name", reader["Project"].ToString()));
                    if (project == null)
                    {
                        project = objectSpace.CreateObject<Project>();
                        project.Name = reader["Project"].ToString();
                        project.Save();
                    }
                }
                Locality loc = objectSpace.FindObject<Locality>(new BinaryOperator("Name", reader["Name"].ToString()), true);
                if (loc == null)
                    loc = objectSpace.FindObject<Locality>(new BinaryOperator("Name", reader["Name"].ToString()));
                if (loc == null)
                {
                    loc = objectSpace.CreateObject<Locality>();
                }
                string s = reader["Name"].ToString().Trim();
                if (s.Length > 254) s = s.Substring(0, 254);
                loc.Name = s;
                if (!Convert.IsDBNull(reader["Area"]))
                {
                    loc.Area = Convert.ToDouble(reader["Area"]);
                }


                loc.Save();
            }
            objectSpace.CommitChanges();


        }

        public static void ImportDwC(UnitOfWork uow)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Title = "Select file";
            dlgOpen.Filter = "DwC text|*.txt";
            dlgOpen.DefaultExt = "txt";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = false;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                //os = SyncHelper.MyObjectSpace(SyncHelper.GetDataLayer(SyncHelper.GetAppConnStr()));
                string strConn = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"", dlgOpen.FileName);
                //   Units(strConn, uow, dlgOpen.FileName, false);

                //   uow.CommitChanges();
            }

        }

        internal static void SiteSp(string strConn, UnitOfWork uow, string item, Dataset dataset)
        {
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                Collection<string> fields = new Collection<string>();
                fields.Add("Locality");
                fields.Add("Date");

                StreamWriter log = null;
                string folder = Path.GetDirectoryName(cn.DataSource);
                string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
                string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
                log = new StreamWriter(logpath, false);
                log.WriteLine("Import started " + DateTime.Now);
                int readerline = 0;
                Locality locality = null;
                List<Species> splist = new List<Species>();
                Project project = uow.FindObject<Project>(new BinaryOperator("Oid", dataset.Project.Oid));
                if (!reader.FieldExists("Locality"))
                {
                    log.WriteLine("Locality field is missing!");
                    throw new Exception("Locality field is missing!");
                }

                while (reader.Read())
                {
                    if (String.IsNullOrEmpty(reader["Locality"].ToString()))
                    {
                        log.WriteLine("Locality value is missing");
                        throw new Exception("Locality value is missing");
                    }

                    try
                    {



                        if ((locality == null) || (reader["Locality"].ToString() != locality.Name))
                        {
                            locality = uow.FindObject<Locality>(new BinaryOperator("Name", reader["Locality"].ToString()));
                            if (locality == null)
                                locality = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Locality"].ToString()));
                            if (locality == null)
                            {
                                locality = new Locality(uow);
                                locality.Name = reader["Locality"].ToString();
                                if (project != null)
                                    locality.Project = project;
                                locality.Save();
                            }
                        }
                        if (splist.Count == 0)
                            for (int i = 0; i < reader.FieldCount - 1; i++)
                            {
                                if (fields.IndexOf(reader.GetName(i)) == -1)
                                {
                                    string readerGetName = reader.GetName(i);
                                    Species species = uow.FindObject<Species>(new BinaryOperator("Name", readerGetName));
                                    if (species == null)
                                        species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", readerGetName));
                                    if (species == null)
                                    {
                                        species = new Species(uow);
                                        species.Name = readerGetName;
                                        if (project != null)
                                            species.Project = project;
                                        species.Save();
                                        splist.Add(species);
                                    }
                                }
                            }
                        for (int i = 0; i < reader.FieldCount - 1; i++)
                        {
                            if (i > 1)
                            {
                                Unit unit = new Unit(uow);
                                unit.Locality = locality;
                                if (reader.FieldExists("Date"))
                                    if (!String.IsNullOrEmpty(reader["Date"].ToString()))
                                        unit.Date = Convert.ToDateTime(reader["Date"].ToString());
                                if (!String.IsNullOrEmpty(reader[reader.GetName(i)].ToString()))
                                    unit.Quantity = Convert.ToDouble(reader[reader.GetName(i)].ToString());
                                unit.TaxonomicName = splist[i - 2];
                                if (dataset != null)
                                    unit.Dataset = dataset;
                                unit.Save();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));
                        if (ex.Message.Contains("value is missing")) break;
                    }
                    finally
                    {

                    }
                    readerline += 1;
                }

                try
                {
                    log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

                }
                finally
                {
                    reader.Close();
                    cn.Close();
                    log.WriteLine("import finished " + DateTime.Now);
                    log.Close();
                    Process.Start(logpath);

                }
            }
        }

        internal static void Taxonomy(string strConn, UnitOfWork uow, string item, Dataset dataset)
        {
            Project Project = null;
            if (dataset!=null)
            Project = dataset.Project;
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                Collection<string> fields = new Collection<string>();
               fields.Add("Class");
               fields.Add("Order");
               fields.Add("Family");
               fields.Add("Subfamily");
               fields.Add("Genus");
               fields.Add("Species");
               fields.Add("Author");
               fields.Add("Year");
               fields.Add("ShortName");
               fields.Add("ValidSpecies");
            
                StreamWriter log = null;
                string folder = Path.GetDirectoryName(cn.DataSource);
                string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
                string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
                log = new StreamWriter(logpath, false);
                log.WriteLine("Import started " + DateTime.Now);
                int readerline = 0;
                if (!reader.FieldExists("Species"))
                {
                    log.WriteLine("Species field is missing!");
                    throw new Exception("Species field is missing!");
                }

                while (reader.Read())
                {
                    Species species = null;
                    Genus genus = null;
                    Species validSpecies = null;
                    HigherTaxon subfamily = null;
                    Family family = null;
                    HigherTaxon Order = null;
                    string shortName = "";
                    if (String.IsNullOrEmpty(reader["Species"].ToString()))
                    {
                        log.WriteLine("Species value is missing");
                        //throw new Exception("Species value is missing");
                    }

                    try
                    {
                        string auth = "";
                        if (reader.FieldExists("Author"))
                            auth = reader["Author"].ToString();
                        string year = "";
                        if (reader.FieldExists("Year"))
                            year = reader["Year"].ToString();

                        if (!String.IsNullOrEmpty(reader["Species"].ToString()))
                        {
                            if ((reader.FieldExists("ShortName")) && (!String.IsNullOrEmpty(reader["ShortName"].ToString())))
                                species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("ShortName", reader["ShortName"].ToString()));
                            else
                              /*  if ((!String.IsNullOrEmpty(reader["Species"].ToString())))
                                    species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Species"].ToString()));                            
                            else*/
                                    species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", reader["Species"].ToString()));
                            if (species == null)
                            {
                                species = null;// new Species(uow);
                                if ((reader.FieldExists("Genus") && (reader.FieldExists("Species"))))
                                    TaxonomyHelper.ParseBinomial(uow, ref species, ref genus, auth, year, reader["Genus"].ToString().Trim() + " "+reader["Species"].ToString().Trim(), " ", Project);
                                if (species==null)
                                    TaxonomyHelper.ParseBinomial(uow, ref species, ref genus, auth, year, reader["Species"].ToString().Trim(), ".", Project);
                                //species.Genus=TaxonomyHelper.GetGenus(
                                if (species==null)
                                {
                                    species = new Species(uow);
                                    species.Name = "";
                                    if (!string.IsNullOrEmpty(auth))
                                        species.Authorship = auth;
                                    if (!string.IsNullOrEmpty(year))
                                        try
                                        {
                                            species.AuthorshipYear = Convert.ToInt32(year);
                                        }
                                        catch
                                        {
                                            log.WriteLine(String.Format("Year conversion failed - "+ year));
                                        }
                                            genus = TaxonomyHelper.GetGenus(uow, Project, reader["Species"].ToString().Trim(),"",0);
                                    species.Genus = genus;

                                }
                                if (reader.FieldExists("ShortName"))
                                    species.ShortName = reader["ShortName"].ToString().Trim();
                                if (Project != null)
                                    species.Project = Project;
                                species.UpdateInfo();
                                species.Save();
                            }
                            else
                            {
                                if (Project != null)
                                    if (species.Project != Project)
                                    {
                                        species.Project = Project;
                                        species.UpdateInfo();
                                        species.Save();
                                    }
                            }
                            if (reader.FieldExists("Comment"))
                                if (!String.IsNullOrEmpty(reader["Comment"].ToString()))
                                {
                                    species.Comment = reader["Comment"].ToString();
                                    species.UpdateInfo();
                                    species.Save();
                                }
                        }
                        if ((reader.FieldExists("Genus")) && (genus == null))
                            if (!String.IsNullOrEmpty(reader["Genus"].ToString()))
                            {
                                string genus_string = reader["Genus"].ToString().Trim();
                                genus = TaxonomyHelper.GetGenus(uow, Project, genus_string,"",0);
                            } 
                        if (reader.FieldExists("ValidSpecies"))
                            if (!String.IsNullOrEmpty(reader["ValidSpecies"].ToString()))
                            {
                                validSpecies = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", reader["ValidSpecies"].ToString()));
                                if (validSpecies == null)
                                {

                                    Genus validgen = null;
                                    TaxonomyHelper.ParseBinomial(uow, ref validSpecies, ref validgen, "", "", reader["ValidSpecies"].ToString().Trim(), " ", Project);
                                    if (validSpecies == null)
                                        TaxonomyHelper.ParseBinomial(uow, ref validSpecies, ref validgen, "", "", reader["ValidSpecies"].ToString().Trim(), ".", Project);

                                    validSpecies.UpdateInfo();
                                    validSpecies.Save();
                                }
                                else
                                {
                                    if (Project != null)
                                        if (validSpecies.Project != Project)
                                        {
                                            validSpecies.Project = Project;
                                            validSpecies.UpdateInfo();
                                            validSpecies.Save();
                                        }
                                }
                                if (species != null)
                                {
                                    Synonymy synonymy = new Synonymy(uow) { TaxonomicName = validSpecies, Synonym = species };
                                    synonymy.Save();
                                }
                            }
                        if ((reader.FieldExists("ShortName")) && (species == null))
                        {
                            species = uow.FindObject<Species>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("ShortName", reader["ShortName"].ToString()));
                            if (species == null)
                            {
                                species = new Species(uow);
                                species.ShortName = reader["ShortName"].ToString();
                                if (genus != null)
                                {
                                    species.Genus = genus;
                                }
                                species.UpdateInfo();
                                species.Save();
                            }
                           
                        }
                      
                        if ((validSpecies != null) && (validSpecies.Genus != null))
                        {
                            validSpecies.Parent = validSpecies.Genus;
                            validSpecies.Category = validSpecies.Genus;
                            validSpecies.UpdateInfo();
                            validSpecies.Save();
                        }
                        if ((species != null) && (genus != null))
                        {
                            species.Genus = genus;
                                       species.Category = genus;
                                species.Parent = genus;
                            species.UpdateInfo();
                            species.Save();
                        }
                        
                        if (reader.FieldExists("Subfamily"))
                            if (!String.IsNullOrEmpty(reader["Subfamily"].ToString()))
                            {
                                subfamily = uow.FindObject<HigherTaxon>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Subfamily"].ToString().Trim()));
                                if (subfamily == null)
                                {
                                    subfamily = new HigherTaxon(uow);
                                    subfamily.Name = reader["Subfamily"].ToString().Trim();
                                    subfamily.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow, "Subfamily");
                                    if (Project != null)
                                        subfamily.Project = Project;
                                    subfamily.Save();
                                }
                                else
                                {
                                    if (Project != null)
                                    {
                                        subfamily.Project = Project;
                                        subfamily.Save();
                                    }
                                }
                                if (genus != null)
                                {
                                    genus.Parent = subfamily;
                                    genus.Save();
                                    if (species != null)
                                    {
                                        species.Category = genus;
                                        species.Parent = genus;
                                        species.Save();
                                    }
                                }
                                else
                                    if (species != null)
                                    {
                                        species.Category = subfamily;
                                        species.Parent = subfamily;
                                        species.Save();
                                    }
                            }
                        if (reader.FieldExists("Family"))
                            if (!String.IsNullOrEmpty(reader["Family"].ToString()))
                            {
                                family = uow.FindObject<Family>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Family"].ToString().Trim()));
                                if (family == null)
                                {
                                    family = new Family(uow);
                                    family.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow, "Family");
                                    family.Name = reader["Family"].ToString().Trim();
                                    if (Project != null)
                                        family.Project = Project;
                                    family.Save();

                                }
                                if (subfamily != null)
                                {
                                    subfamily.Parent = family;
                                    subfamily.Save();
                                }
                                else
                                    if (genus != null)
                                    {
                                        genus.Parent = family;
                                        genus.Save();
                                        if (species != null)
                                        {
                                            species.Category = genus;
                                            species.Parent = genus;
                                            species.UpdateInfo();
                                            species.Save();
                                        }
                                    }
                                    else
                                        if (species != null)
                                    {
                                        species.Category = family;
                                        species.Parent = family;
                                        species.UpdateInfo();
                                        species.Save();
                                    }
                            }

                        if (reader.FieldExists("Order"))
                            if (!String.IsNullOrEmpty(reader["Order"].ToString()))
                            {
                                Order = uow.FindObject<HigherTaxon>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["Order"].ToString().Trim()));
                                if (Order == null)
                                {
                                    Order = new HigherTaxon(uow);
                                    Order.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(uow, "Order");
                                    Order.Name = reader["Order"].ToString().Trim();
                                    if (Project != null)
                                        Order.Project = Project;
                                    Order.Save();
                                }
                                else
                                {
                                    if (Project != null)
                                    {
                                        Order.Project = Project;
                                        Order.Save();
                                    }
                                }
                                if (family != null)
                                {
                                    family.Parent = Order;
                                    family.Save();
                                }
                                else
                                {
                                    if (subfamily != null)
                                    {
                                        subfamily.Parent = Order;
                                        subfamily.Save();
                                    }
                                    else
                                        if (genus != null)
                                        {
                                            genus.Parent = Order;
                                            genus.Save();
                                            if (species != null)
                                            {
                                                species.Category = genus;
                                                species.Parent = genus;
                                                species.UpdateInfo();
                                                species.Save();
                                            }
                                        }
                                        else
                                        {
                                        if (species != null)
                                        {
                                            species.Category = Order;
                                            species.Parent = Order;
                                            species.UpdateInfo();
                                            species.Save();
                                        }

                                    }
                                }
                            }
                         if (validSpecies != null)
                        {
                            if (validSpecies.Genus != null)
                            {
                                if (species != null)
                                {
                                    if (species.Genus != null)
                                    {
                                        validSpecies.Genus.Parent = species.Genus.Parent;
                                        validSpecies.Genus.Save();
                                    }
                                }
                            }
                            else
                            { 
                            }
                        }
                         TagHelper.CreateTags(dataset, uow, reader, fields, species, "", null, IfFieldExists.Overwrite);
          
                    }
                   
                      
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));
                        if (ex.Message.Contains("value is missing")) break;
                    }
                    finally
                    {

                    }
                    readerline += 1;
                }

                try
                {
                    log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

                }
                finally
                {
                    reader.Close();
                    cn.Close();
                    log.WriteLine("import finished " + DateTime.Now);
                    log.Close();
                    Process.Start(logpath);

                }
            }
        }

       

       
       
        internal static void ImportUsers(string strConn, UnitOfWork uow, string item)
        {
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                Collection<string> fields = new Collection<string>();
                fields.Add("UserName");
                fields.Add("FirstName");
                fields.Add("LastName");
              
                StreamWriter log = null;
                string folder = Path.GetDirectoryName(cn.DataSource);
                string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
                string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
                log = new StreamWriter(logpath, false);
                log.WriteLine("Import started " + DateTime.Now);
                int readerline = 0;
                 if (!reader.FieldExists("UserName"))
                {
                    log.WriteLine("UserName field is missing!");
                    throw new Exception("UserName field is missing!");
                }

                while (reader.Read())
                {
                    try{
                        if (reader.FieldExists("UserName"))
                        {
                            User user = uow.FindObject<User>(new BinaryOperator("UserName", reader["UserName"].ToString()),true);
                            if (user == null)
                            {
                                user = new User(uow);
                                user.UserName = reader["UserName"].ToString();
                                  user.Save();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));
                        if (ex.Message.Contains("value is missing")) break;
                    }
                    finally
                    {

                    }
                    readerline += 1;
                }

                try
                {
                    log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

                }
                finally
                {
                    reader.Close();
                    cn.Close();
                    log.WriteLine("import finished " + DateTime.Now);
                    log.Close();
                    Process.Start(logpath);

                }
            }
        }
        internal static void ImportTags(string strConn, UnitOfWork uow, string item)
        {
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                Collection<string> fields = new Collection<string>();
                fields.Add("Project");
                fields.Add("Name");
                fields.Add("Type");
                fields.Add("Description");

                StreamWriter log = null;
                string folder = Path.GetDirectoryName(cn.DataSource);
                string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
                string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
                log = new StreamWriter(logpath, false);
                log.WriteLine("Import started " + DateTime.Now);
                int readerline = 0;
                if (!reader.FieldExists("Project"))
                {
                    log.WriteLine("Project field is missing!");
                    throw new Exception("Project field is missing!");
                }
                if (!reader.FieldExists("Name"))
                {
                    log.WriteLine("Name field is missing!");
                    throw new Exception("Name field is missing!");
                }
                if (!reader.FieldExists("Type"))
                {
                    log.WriteLine("Type field is missing!");
                    throw new Exception("Type field is missing!");
                }
                if (!reader.FieldExists("Description"))
                {
                    log.WriteLine("Description field is missing!");
                    throw new Exception("Description field is missing!");
                }

                while (reader.Read())
                {
                    try
                    {
                        if (reader.FieldExists("Name"))
                        {
                            if (reader.FieldExists("Description"))
                            {
                                if (reader.FieldExists("Project"))
                                {
                                    //              if ((!reader.FieldExists("Type")) || (String.IsNullOrEmpty(reader["Type"].ToString())))
                                    //            {
                                    CustomField tag = uow.FindObject<CustomField>(CriteriaOperator.And(new BinaryOperator("Project.Name", reader["Project"].ToString()), new BinaryOperator("Name", reader["Name"].ToString())), true);
                                    if (tag != null)
                                    {
                                        tag.Comment = reader["Description"].ToString();
                                        tag.Save();
                                        //              }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));
                        if (ex.Message.Contains("value is missing")) break;
                    }
                    finally
                    {

                    }
                    readerline += 1;
                }

                try
                {
                    log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

                }
                finally
                {
                    reader.Close();
                    cn.Close();
                    log.WriteLine("import finished " + DateTime.Now);
                    log.Close();
                    Process.Start(logpath);

                }
            }
        }
        internal static void ImportProjects(string strConn, UnitOfWork uow, string item)
        {
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", cn);
            using (OleDbDataReader reader = cmd.ExecuteReader())
            {
                Collection<string> fields = new Collection<string>();
                fields.Add("Name");
                fields.Add("FullName");
                fields.Add("Description");
          fields.Add("FullDescription");
          fields.Add("StartDate");
    fields.Add("EndDate");
    fields.Add("Lead");
    fields.Add("Participants");
    fields.Add("Status");
    fields.Add("ContactEmail");
    fields.Add("Details");
    fields.Add("Code");
  

                StreamWriter log = null;
                string folder = Path.GetDirectoryName(cn.DataSource);
                string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
                string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
                log = new StreamWriter(logpath, false);
                log.WriteLine("Import started " + DateTime.Now);
                int readerline = 0;
                if (!reader.FieldExists("Name"))
                {
                    log.WriteLine("Name field is missing!");
                    throw new Exception("Name field is missing!");
                }
                
                if (!reader.FieldExists("Description"))
                {
                    log.WriteLine("Description field is missing!");
                    throw new Exception("Description field is missing!");
                }

                while (reader.Read())
                {
                    try
                    {
                        if (reader.FieldExists("Name"))
                        {
                            if (reader.FieldExists("Description"))
                            {
                                if ((!reader.FieldExists("Type")) || (String.IsNullOrEmpty(reader["Type"].ToString())))
                                {
                                    Project Project = uow.FindObject<Project>(new BinaryOperator("Name", reader["Name"].ToString()), true);
                                    if (Project != null)
                                    {
                                        Project.Description = reader["Description"].ToString();
                                        Project.LongTitle = reader["FullName"].ToString();
                                        if ((reader.FieldExists("StartDate") && (!string.IsNullOrEmpty(reader["StartDate"].ToString()))))
                                        Project.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
                                        if ((reader.FieldExists("EndDate") && (!string.IsNullOrEmpty(reader["EndDate"].ToString()))))
                                        Project.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
                                        Project.Lead = reader["Lead"].ToString();
                                        Project.Participants = reader["Participants"].ToString();
                                        Project.Status = reader["Status"].ToString();
                                        Project.Details = reader["Details"].ToString();
                                        Project.ContactEmail = reader["ContactEmail"].ToString();
                                        Project.Save();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));
                        if (ex.Message.Contains("value is missing")) break;
                    }
                    finally
                    {

                    }
                    readerline += 1;
                }

                try
                {
                    log.WriteLine(String.Format("{0}\t commit started", DateTime.Now));
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    log.WriteLine("Something went wrong on commit" + "\t" + "\t" + ex.Message);

                }
                finally
                {
                    reader.Close();
                    cn.Close();
                    log.WriteLine("import finished " + DateTime.Now);
                    log.Close();
                    Process.Start(logpath);

                }
            }
        }
    }
}
