﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using EarthCape.Module.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.ObjectModel;
using EarthCape.Module.Win;
using System.Diagnostics;
using DevExpress.Persistent.Base;
using DevExpress.Spreadsheet;
using DevExpress.XtraSpreadsheet;
using DevExpress.XtraWaitForm;
using DevExpress.XtraSplashScreen;
using EarthCape.Module.Importer.Win;
using System.ComponentModel;

namespace EarthCape.Module.Importer
{
    public static class DataReaderExtensions
    {
        public static bool FieldExists(this IDataReader reader, string fieldName)
        {
            reader.GetSchemaTable().DefaultView.RowFilter = string.Format("ColumnName= '{0}'", fieldName);
            return (reader.GetSchemaTable().DefaultView.Count > 0);
        }
    }
    public class ImportHelper
    {
        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, string parent, string area, string alt1, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT, string admin)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                if (admin == "")
                {
                    loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                    if (loc == null)
                        loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                }
                if (admin == "country")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<Country>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<Country>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<Country>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<Country>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm1")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm1>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm1>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm1>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm1>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm2")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm2>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm2>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm2>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm2>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm3")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm3>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm3>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm3>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm3>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
                if (admin == "adm4")
                {
                    if (string.IsNullOrEmpty(parent))
                        loc = uow.FindObject<LocalityAdm4>(new BinaryOperator("Name", locname));
                    else
                        loc = uow.FindObject<LocalityAdm4>(CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                    if (loc == null)
                        if (string.IsNullOrEmpty(parent))
                            loc = uow.FindObject<LocalityAdm4>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                        else
                            loc = uow.FindObject<LocalityAdm4>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Parent.Name", parent)));
                }
            }
            if (loc == null)
            {
                if (createnew)
                {
                    if (admin == "")
                    {
                        loc = new Locality(uow);
                    }
                    if (admin == "country")
                    {
                        loc = new Country(uow);
                    }
                    if (admin == "adm1")
                    {
                        loc = new LocalityAdm1(uow);
                    }
                    if (admin == "adm2")
                    {
                        loc = new LocalityAdm2(uow);
                    }
                    if (admin == "adm3")
                    {
                        loc = new LocalityAdm3(uow);
                    }
                    if (admin == "adm4")
                    {
                        loc = new LocalityAdm4(uow);
                    }
                    loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
                /* Note newnote = new Note(uow, note);
                 if (!String.IsNullOrEmpty(date)) 
                 newnote.RecordedOn = DateTime.Parse(date);
                 newnote.Save();
                 loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }
        public static Locality CreateLocality(Project project, UnitOfWork uow, string locname, LocalityAdm parent, string area, string alt1, string alt2, string WKT, bool checkdups, string note, bool createnew, bool updLocWKT)
        {
            Locality loc = null;
            locname = locname.Trim();
            if (locname.Length > 254) locname = locname.Substring(0, 254);
            if ((string.IsNullOrEmpty(locname)) && (string.IsNullOrEmpty(WKT))) { return null; }
            if (checkdups)
            {
                //if (parent ==null)
                //{
                loc = (Locality)uow.FindObject<Locality>(new BinaryOperator("Name", locname));
                if (loc == null)
                    loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", locname));
                /*}else{

                    loc = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Name", locname), new BinaryOperator("Category.Oid", parent.Oid)));
                }*/
            }
            if (loc == null)
            {
                if (createnew)
                {
                    loc = new Locality(uow);
                    loc.Category = parent;
                    loc.Name = locname;
                }
                else
                {
                    return null;
                }
            }
            //loc.WorkItems.Add((WorkItem)wi);
            if (!string.IsNullOrEmpty(note))
            {
                /* Note newnote = new Note(uow, note);
                 if (!String.IsNullOrEmpty(date)) 
                 newnote.RecordedOn = DateTime.Parse(date);
                 newnote.Save();
                 loc.Notes.Add(newnote);*/
            }
            if (!string.IsNullOrEmpty(alt1))
                loc.Altitude1 = Convert.ToDouble(alt1);
            if (!string.IsNullOrEmpty(alt2))
                loc.Altitude2 = Convert.ToDouble(alt2);
            if (!string.IsNullOrEmpty(area))
                loc.Area = Convert.ToDouble(area);
            if (updLocWKT || (String.IsNullOrEmpty(loc.WKT)))
                if (!string.IsNullOrEmpty(WKT))
                    loc.WKT = WKT;
            if (project != null)
                loc.Project = project;
            loc.Save();
            if (loc.Name == "")
            {
                //objectSpace.CommitChanges();
                loc.Name = loc.Oid.ToString();// Guid.NewGuid().ToString();
                loc.Save();
            }
            //objectSpace.CommitChanges();
            return loc;
        }
        public static void LatLongs(OleDbDataReader reader, out double? X, out double? Y)
        {
            X = null;
            Y = null;
            if (reader.FieldExists("latdeg"))
                if (reader.FieldExists("latmin"))
                    if (reader.FieldExists("latsec"))
                        if (reader.FieldExists("longdeg"))
                            if (reader.FieldExists("longmin"))
                                if (reader.FieldExists("longsec"))
                                {

                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (!String.IsNullOrEmpty(reader["latmin"].ToString())) && (!String.IsNullOrEmpty(reader["latsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["latdeg"].ToString());
                                        double min = Convert.ToDouble(reader["latmin"].ToString());
                                        double sec = Convert.ToDouble(reader["latsec"].ToString());
                                        Y = deg + (min + sec / 60) / 60;
                                    }


                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (!String.IsNullOrEmpty(reader["latmin"].ToString())) && (String.IsNullOrEmpty(reader["latsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["latdeg"].ToString());
                                        double min = Convert.ToDouble(reader["latmin"].ToString());
                                        Y = deg + min / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["latdeg"].ToString())) && (String.IsNullOrEmpty(reader["latmin"].ToString())) && (String.IsNullOrEmpty(reader["latsec"].ToString())))
                                        Y = Convert.ToDouble(reader["latdeg"].ToString());
                                    if (reader.FieldExists("NS"))
                                    {
                                        if (reader["NS"].ToString() == "S")
                                        {
                                            Y = Y * (-1);
                                        }
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (!String.IsNullOrEmpty(reader["longmin"].ToString())) && (!String.IsNullOrEmpty(reader["longsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["longdeg"].ToString());
                                        double min = Convert.ToDouble(reader["longmin"].ToString());
                                        double sec = Convert.ToDouble(reader["longsec"].ToString());
                                        X = deg + (min + sec / 60) / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (!String.IsNullOrEmpty(reader["longmin"].ToString())) && (String.IsNullOrEmpty(reader["longsec"].ToString())))
                                    {
                                        double deg = Convert.ToDouble(reader["longdeg"].ToString());
                                        double min = Convert.ToDouble(reader["longmin"].ToString());
                                        X = deg + min / 60;
                                    }
                                    if ((!String.IsNullOrEmpty(reader["longdeg"].ToString())) && (String.IsNullOrEmpty(reader["longmin"].ToString())) && (String.IsNullOrEmpty(reader["longsec"].ToString())))
                                        X = Convert.ToDouble(reader["longdeg"].ToString());
                                    if (reader.FieldExists("WE"))
                                    {
                                        if (reader["WE"].ToString() == "W")
                                        {
                                            X = X * (-1);
                                        }
                                    }

                                    /* if ((X != null) && (Y != null))
                                     {
                                         to.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                                         //to.Centroid_X = X;
                                         //to.Centroid_Y = Y;
                                     }*/
                                }
        }
        public static void CalcIWKT(OleDbDataReader reader, ref string altitude, ref string altitude2, ref string WKT)
        {
            if (reader.FieldExists("Altitude1"))
                altitude = reader["Altitude1"].ToString();
            if (reader.FieldExists("Altitude2"))
                altitude2 = reader["Altitude2"].ToString();
            if (reader.FieldExists("WKT"))
                if (!String.IsNullOrEmpty(reader["WKT"].ToString()))
                {
                    WKT = reader["WKT"].ToString();
                }
            if (String.IsNullOrEmpty(WKT))
            {
                if (reader.FieldExists("X"))
                    if (!String.IsNullOrEmpty(reader["X"].ToString()))
                    {
                        if (reader.FieldExists("Y"))
                            if (!String.IsNullOrEmpty(reader["Y"].ToString()))
                            {
                                double y = Convert.ToDouble(reader["Y"].ToString());
                                double x = Convert.ToDouble(reader["X"].ToString());
                                WKT = String.Format("POINT ({0} {1})", x.ToString().Replace(",", "."), y.ToString().Replace(",", "."));
                            }
                    }
            }
        }
        public static void ImportLocs(string strConn, UnitOfWork uow, string fn, Dataset dataset, bool importcustomdata, StreamWriter errors)
        {
            Project project = null;
            if (dataset != null)
                project = dataset.Project;
            string strSQL = "SELECT * FROM [Sheet1$]";
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand(strSQL, cn);
            string folder = Path.GetDirectoryName(cn.DataSource);
            string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
            string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
            errors = new StreamWriter(logpath, false);
            errors.AutoFlush = true;
            errors.WriteLine("Import started " + DateTime.Now.ToString());
            OleDbDataReader reader = cmd.ExecuteReader();

            Collection<string> fields = new Collection<string>();
            fields.Add("Name");
            fields.Add("Altitude1");
            fields.Add("Altitude2");
            fields.Add("WKT");
            fields.Add("EPSG");
            fields.Add("Notes");
            fields.Add("Comments");
            fields.Add("X");
            fields.Add("Y");
            fields.Add("latdeg");
            fields.Add("latmin");
            fields.Add("latsec");
            fields.Add("longdeg");
            fields.Add("longmin");
            fields.Add("longsec");
            fields.Add("NS");
            fields.Add("WE");
            fields.Add("CoordRad");
            fields.Add("CoordSource");
            fields.Add("CoordDetails");
            fields.Add("CoordNotes");
            fields.Add("Country");
            fields.Add("CoordNotes");
            fields.Add("Admin1");
            fields.Add("Admin2");
            fields.Add("Admin3");
            fields.Add("Admin4");
            fields.Add("dummy");

            int readerline = 0;
            // User p = uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(); uow.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            while (reader.Read())
            {
                String WKT = "";
                String name = "";
                String notes = "";
                String locality = "";
                String epsg = "";
                String coordsource = "";
                String alt1 = "";
                String alt2 = "";
                String Method = "";
                try
                {
                    if (reader.FieldExists("WKT"))
                        WKT = reader["WKT"].ToString();
                    if (reader.FieldExists("EPSG"))
                        epsg = reader["EPSG"].ToString();
                    if (reader.FieldExists("CoordSource"))
                        coordsource = reader["CoordSource"].ToString();
                    if (reader.FieldExists("Name"))
                        name = reader["Name"].ToString();
                    if (reader.FieldExists("Notes"))
                        notes = reader["Notes"].ToString();
                    if (reader.FieldExists("Locality"))
                        locality = reader["Locality"].ToString();
                    if (reader.FieldExists("Method"))
                        Method = reader["Method"].ToString();
                    if (reader.FieldExists("Altitude1"))
                        alt1 = reader["Altitude1"].ToString();
                    if (reader.FieldExists("Altitude2"))
                        alt2 = reader["Altitude2"].ToString();


                    LocalityAdm country = null;
                    LocalityAdm locadm1 = null;
                    LocalityAdm locadm2 = null;
                    LocalityAdm locadm3 = null;
                    LocalityAdm locadm4 = null;
                    if (reader.FieldExists("Country"))
                    {
                        if (!String.IsNullOrEmpty(reader["Country"].ToString()))
                        {
                            country = (Country)ImportHelper.CreateLocality(
                                project,
                                uow,
                                reader["Country"].ToString(),
                                "", "",
                                alt1,
                                alt2,
                                WKT, true,
                                notes,
                                true, false, "country");

                        }
                    }
                    if (reader.FieldExists("Admin1"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin1"].ToString()))
                        {
                            string parent = "";
                            if (country != null) parent = country.Name;
                            locadm1 = (LocalityAdm1)ImportHelper.CreateLocality(project, uow, reader["Admin1"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm1");
                            if (country != null)
                            {
                                locadm1.Parent = country;
                                locadm1.Save();
                            }
                        }
                    }
                    if (reader.FieldExists("Admin2"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin2"].ToString()))
                        {
                            string parent = "";
                            if (locadm1 != null) parent = locadm1.Name;
                            else
                                if (country != null) parent = country.Name;
                            locadm2 = (LocalityAdm2)ImportHelper.CreateLocality(project, uow, reader["Admin2"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm2");
                            if (locadm1 != null)
                            {
                                locadm2.Parent = locadm1;
                                locadm2.Save();
                            }
                            else
                                if (country
                                    != null)
                                {
                                    locadm2.Parent = country;
                                    locadm2.Save();
                                }
                        }
                    }

                    if (reader.FieldExists("Admin3"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin3"].ToString()))
                        {
                            string parent = "";
                            if (locadm2 != null) parent = locadm2.Name;
                            else
                                if (locadm1 != null) parent = locadm1.Name;
                                else
                                    if (country != null) parent = country.Name;
                            locadm3 = (LocalityAdm3)ImportHelper.CreateLocality(project, uow, reader["Admin3"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm3");
                            if (locadm2 != null)
                            {
                                locadm3.Parent = locadm2;
                                locadm3.Save();
                            }
                            else
                                if (locadm1 != null)
                                {
                                    locadm3.Parent = locadm1;
                                    locadm3.Save();
                                }
                                else
                                    if (country
                                        != null)
                                    {
                                        locadm3.Parent = country;
                                        locadm3.Save();
                                    }
                        }
                    }
                    if (reader.FieldExists("Admin4"))
                    {
                        if (!String.IsNullOrEmpty(reader["Admin4"].ToString()))
                        {
                            string parent = "";
                            if (locadm3 != null) parent = locadm3.Name;
                            else
                                if (locadm2 != null) parent = locadm2.Name;
                                else
                                    if (locadm1 != null) parent = locadm1.Name;
                                    else
                                        if (country != null) parent = country.Name;
                            locadm4 = (LocalityAdm4)ImportHelper.CreateLocality(project, uow, reader["Admin4"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm4");
                            if (locadm3 != null)
                            {
                                locadm4.Parent = locadm3;
                                locadm4.Save();
                            }
                            else
                                if (locadm2 != null)
                                {
                                    locadm4.Parent = locadm2;
                                    locadm4.Save();
                                }
                                else
                                    if (locadm1 != null)
                                    {
                                        locadm4.Parent = locadm1;
                                        locadm4.Save();
                                    }
                                    else
                                        if (country
                                            != null)
                                        {
                                            locadm4.Parent = country;
                                            locadm4.Save();
                                        }


                        }
                    }
                    if (!String.IsNullOrEmpty(name))
                    {
                        LocalityAdm lowerLoc = null;
                        if (locadm4 != null)
                            lowerLoc = locadm4;
                        else
                            if (locadm3 != null)
                                lowerLoc = locadm3;
                            else
                                if (locadm2 != null)
                                    lowerLoc = locadm2;
                                else
                                    if (locadm1 != null)
                                        lowerLoc = locadm1;
                                    else
                                        if (country != null)
                                            lowerLoc = country;
                        ImportHelper.CalcIWKT(reader, ref alt1, ref alt2, ref WKT);
                        Locality loc = ImportHelper.CreateLocality(project, uow, name, lowerLoc, "", alt1, alt2, WKT, true, notes, true, true);
                        double? X;
                        double? Y;
                        ImportHelper.LatLongs(reader, out X, out Y);
                        if ((X != null) && (Y != null))
                        {
                            loc.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                        }
                        if (reader.FieldExists("EPSG"))
                            if (!String.IsNullOrEmpty(reader["EPSG"].ToString()))
                                loc.EPSG = Convert.ToInt32(reader["EPSG"].ToString());
                        if (reader.FieldExists("Length"))
                        {
                            if (!String.IsNullOrEmpty(reader["Length"].ToString()))
                            {
                                loc.Length = Convert.ToDouble(reader["Length"].ToString());
                            }
                        }
                        if (reader.FieldExists("Area"))
                        {
                            if (!String.IsNullOrEmpty(reader["Area"].ToString()))
                            {
                                loc.Area = Convert.ToDouble(reader["Area"].ToString());
                            }
                        }
                        if (reader.FieldExists("ReasonNotValid"))
                        {
                            if (!String.IsNullOrEmpty(reader["ReasonNotValid"].ToString()))

                                loc.ReasonNotValid = reader["ReasonNotValid"].ToString();
                        }
                        if (reader.FieldExists("NotValid"))
                        {
                            if (!String.IsNullOrEmpty(reader["NotValid"].ToString()))
                            {
                                if (Convert.ToBoolean(reader["NotValid"]) == true)
                                    loc.NotValid = true;
                                if (Convert.ToBoolean(reader["NotValid"]) == false)
                                    loc.NotValid = false;
                            }
                        }
                        if (reader.FieldExists("ValidLocality"))
                        {
                            if (!String.IsNullOrEmpty(reader["ValidLocality"].ToString()))
                            {
                                Locality locValid = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                if (locValid == null)
                                    locValid = uow.FindObject<Locality>(new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                if (locValid != null)
                                {
                                    loc.ValidLocality = locValid;
                                }
                            }


                        }

                        if ((loc != null) && (lowerLoc != null))
                        {
                            loc.Category = lowerLoc;
                            loc.Save();
                        }
                        else if (lowerLoc != null)
                        { loc = lowerLoc; }
                        if (loc != null)
                        {
                            if (importcustomdata)
                                TagHelper.CreateTags(dataset, uow, reader, fields, loc, "", errors, IfFieldExists.Overwrite);
                        }
                    }

                }
                catch (Exception ex)
                {
                    errors.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));

                }
                finally
                {

                }
                readerline += 1;
                if (readerline % 1000 == 999)
                {
                    errors.WriteLine(DateTime.Now.ToString() + readerline + "records done");
                    errors.WriteLine("committing...");
                    errors.WriteLine("committed " + DateTime.Now.ToString());
                    uow.CommitChanges();
                }

            }
            reader.Close();
            cn.Close();
            uow.CommitChanges();
            errors.WriteLine("import finished (" + readerline + " records) " + DateTime.Now.ToString());
            errors.Close();
            Process.Start(logpath);
        }
        public static void ImportImageList(string strConn, UnitOfWork uow, Project project, StreamWriter errors)
        {
            string strSQL = "SELECT * FROM [Sheet1$]";
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand(strSQL, cn);
            string folder = Path.GetDirectoryName(cn.DataSource);
            string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
            string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
            errors = new StreamWriter(logpath, false);
            errors.AutoFlush = true;
            errors.WriteLine("Import started " + DateTime.Now.ToString());
            OleDbDataReader reader = cmd.ExecuteReader();
            project = uow.FindObject<Project>(new BinaryOperator("Oid", project.Oid));
            Collection<string> fields = new Collection<string>();
            fields.Add("FileName");
            fields.Add("Caption");
            fields.Add("FullPath");
            fields.Add("Year");
            fields.Add("Date");
            fields.Add("Comment");
            fields.Add("ImageID");

            fields.Add("Time");

            fields.Add("X");

            fields.Add("Y");
            fields.Add("Locality");
            fields.Add("UnitID");



            int readerline = 0;
            // User p = uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(); uow.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            while (reader.Read())
            {
                Unit Unit = null;
                if (reader.FieldExists("UnitID"))
                    if (!string.IsNullOrEmpty(reader["UnitID"].ToString()))
                    {

                        Unit = uow.FindObject<Unit>(CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", project.Oid), new BinaryOperator("UnitID", reader["UnitID"].ToString())));
                        if (Unit == null)
                            Unit = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", project.Oid), new BinaryOperator("UnitID", reader["UnitID"].ToString())));
                    }

                FileItemLinked fileItem = null;
                try
                {
                    if (fileItem == null)
                        fileItem = uow.FindObject<FileItemLinked>(CriteriaOperator.And(new BinaryOperator("Project.Oid", project.Oid), new BinaryOperator("File.FileName", reader["FileName"].ToString())));
                    if (fileItem == null)
                        fileItem = uow.FindObject<FileItemLinked>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Project.Oid", project.Oid), new BinaryOperator("File.FileName", reader["FileName"].ToString())));
                    if (fileItem == null)
                    {

                        string path = reader["FullPath"].ToString();
                        if (File.Exists(path))
                        {
                            fileItem = new FileItemLinked(uow);
                            FileDataLinked file = new FileDataLinked(uow);
                            using (FileStream filestream = new FileStream(path, FileMode.Open))
                            {
                                FileDataHelper.LoadFromStream(file, Path.GetFileName(path), filestream, path);

                                //file.LoadFromStream(path, filestream, (messageBoxShow == DialogResult.Yes));
                            }

                            file.Project = project;
                            file.FileName = reader["FileName"].ToString();
                            file.Folder = Path.GetDirectoryName(reader["FullPath"].ToString());
                            file.Save();
                            ((FileItemLinked)fileItem).File = file;
                            fileItem.Project = project;
                            fileItem.Name = Path.GetFileNameWithoutExtension(file.FileName);
                            fileItem.Save();
                        }
                    }
                }
                catch (Exception ex)
                {
                    errors.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));

                }
                finally
                {

                }
                if (fileItem != null)
                {
                    if (reader.FieldExists("Comment"))
                        if (!string.IsNullOrEmpty(reader["Comment"].ToString()))
                        {
                            fileItem.Comment = reader["Comment"].ToString();
                        }

                    if (reader.FieldExists("ImageID"))
                        if (!string.IsNullOrEmpty(reader["ImageID"].ToString()))
                        {
                            fileItem.SourceId = reader["ImageID"].ToString();
                        }
                    if (Unit != null)
                    {
                        if (Unit.FileSet != null)
                            Unit.FileSet.Files.Add(fileItem);
                    }
                    fileItem.Save();
                }

                readerline += 1;
                if (readerline % 1000 == 999)
                {
                    errors.WriteLine(DateTime.Now.ToString() + readerline + "records done");
                    errors.WriteLine("committing...");
                    errors.WriteLine("committed " + DateTime.Now.ToString());
                    uow.CommitChanges();
                }

            }
            reader.Close();
            cn.Close();
            uow.CommitChanges();
            errors.WriteLine("import finished (" + readerline + " records) " + DateTime.Now);
            errors.Close();
            Process.Start(logpath);
        }
        public static void ImportLocVisit(string strConn, UnitOfWork uow, string fn, Dataset dataset, bool importcustomdata, StreamWriter errors)
        {
            string strSQL = "SELECT * FROM [Sheet1$]";
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand(strSQL, cn);
            string folder = Path.GetDirectoryName(cn.DataSource);
            string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
            string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
            errors = new StreamWriter(logpath, false);
            errors.AutoFlush = true;
            errors.WriteLine("Import started " + DateTime.Now.ToString());
            OleDbDataReader reader = cmd.ExecuteReader();

            Collection<string> fields = new Collection<string>();
            fields.Add("Name");
            fields.Add("Parent");
            fields.Add("Locality");
            fields.Add("Year");
            fields.Add("Date1");
            fields.Add("Date2");
            fields.Add("Altitude1");
            fields.Add("Altitude2");
            fields.Add("TimeSpent");
            fields.Add("TimeSpentUnit");
            fields.Add("WKT");
            fields.Add("EPSG");
            fields.Add("CoordSource");
            fields.Add("Method");

            int readerline = 0;

            // User p = uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(); uow.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            while (reader.Read())
            {
                String WKT = "";
                String name = "";
                String parent = "";
                String locality = "";
                String date1 = "";
                String date2 = "";
                String year = "";
                String epsg = "";
                String coordsource = "";
                String alt1 = "";
                String alt2 = "";
                String TimeSpent = "";
                String TimeSpentUnit = "";
                String Method = "";
                try
                {
                    if (reader.FieldExists("WKT"))
                        WKT = reader["WKT"].ToString();
                    if (reader.FieldExists("EPSG"))
                        epsg = reader["EPSG"].ToString();
                    if (reader.FieldExists("CoordSource"))
                        coordsource = reader["CoordSource"].ToString();
                    if (reader.FieldExists("Name"))
                        name = reader["Name"].ToString();
                    if (reader.FieldExists("Parent"))
                        parent = reader["Parent"].ToString();
                    if (reader.FieldExists("Year"))
                        year = reader["Year"].ToString();
                    if (reader.FieldExists("Locality"))
                        locality = reader["Locality"].ToString();
                    if (reader.FieldExists("Method"))
                        Method = reader["Method"].ToString();
                    if (reader.FieldExists("Date1"))
                        if (!String.IsNullOrEmpty(reader["Date1"].ToString()))
                            date1 = reader["Date1"].ToString();
                    if (reader.FieldExists("Date2"))
                        if (!String.IsNullOrEmpty(reader["Date2"].ToString()))
                            date2 = reader["Date2"].ToString();
                    if (reader.FieldExists("Altitude1"))
                        alt1 = reader["Altitude1"].ToString();
                    if (reader.FieldExists("Altitude2"))
                        alt2 = reader["Altitude2"].ToString();
                    if (reader.FieldExists("TimeSpent"))
                        TimeSpent = reader["TimeSpent"].ToString();
                    if (reader.FieldExists("TimeSpentUnit"))
                        TimeSpentUnit = reader["TimeSpentUnit"].ToString();
                    LocalityVisit pv = null;
                    if (!string.IsNullOrEmpty(name))
                    {
                        pv = uow.FindObject<LocalityVisit>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", name));
                        if (pv == null)
                            pv = new LocalityVisit(uow);
                    }
                    else
                    { pv = new LocalityVisit(uow); }
                    if (!string.IsNullOrEmpty(locality))
                        pv.Locality = ImportHelper.CreateLocality(dataset.Project, uow, locality, "", "", alt1, alt2, "", true, "", true, false, "");
                    if (!String.IsNullOrEmpty(TimeSpent))
                        pv.TimeSpent = Convert.ToDouble(TimeSpent);
                    if (!String.IsNullOrEmpty(coordsource))
                        pv.CoordSource = coordsource;
                    if (!String.IsNullOrEmpty(epsg))
                        pv.EPSG = Convert.ToInt32(epsg);
                    pv.TimeSpentUnit = TimeSpentUnit;
                    pv.Method = Method;
                    pv.Project = uow.FindObject<Project>(new BinaryOperator("Oid", dataset.Project.Oid));
                    pv.Parent = uow.FindObject<LocalityVisit>(new BinaryOperator("Name", parent));
                    pv.Name = name;
                    if (!string.IsNullOrEmpty(WKT))
                        pv.WKT = WKT;
                    if (!String.IsNullOrEmpty(date1))
                    {
                        pv.DateTimeStart = DateTime.Parse(date1);
                        if (!String.IsNullOrEmpty(date2))
                            pv.DateTimeEnd = DateTime.Parse(date2);
                    }
                    if (!string.IsNullOrEmpty(year))
                        pv.Year = Convert.ToInt32(year);
                    pv.Save();
                    if (importcustomdata)
                        TagHelper.CreateTags(dataset, uow, reader, fields, pv, date1, errors, IfFieldExists.Overwrite);
                }
                catch (Exception ex)
                {
                    errors.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));

                }
                finally
                {

                }
                readerline += 1;
                if (readerline % 1000 == 999)
                {
                    errors.WriteLine(DateTime.Now.ToString() + readerline + "records done");
                    errors.WriteLine("committing...");
                    errors.WriteLine("committed " + DateTime.Now.ToString());
                    uow.CommitChanges();
                }

            }
            reader.Close();
            cn.Close();
            uow.CommitChanges();
            errors.WriteLine("import finished (" + readerline + " records) " + DateTime.Now.ToString());
            errors.Close();
            Process.Start(logpath);
        }   
     }
}


/*
       fields.Add("Dataset");
            fields.Add("Comment");
            fields.Add("CreatedByUser");
            fields.Add("LastModifiedByUser");
            fields.Add("CreatedOn");
            fields.Add("LastModifiedOn");
            fields.Add("Country");
            fields.Add("LocalityVisit");
            fields.Add("Preservation");
            fields.Add("OtherId");
            fields.Add("CoordRadius");
            fields.Add("CoordSource");
            fields.Add("CoordDetails");
            fields.Add("Host");
            fields.Add("HostDetails");
            fields.Add("LabelText");
            fields.Add("Area");
            fields.Add("EPSG");
            fields.Add("DoesNotExist");
            fields.Add("CentroidX");
            fields.Add("CentroidY");
            fields.Add("InstitutionCode");
            fields.Add("CollectionCode");
            fields.Add("Stage");
            fields.Add("Type");
            fields.Add("AssociatedTaxa");
            fields.Add("TypeStatus");
            fields.Add("Sex");
            fields.Add("BirthDate");
            fields.Add("DeathDate");
            fields.Add("TaxonomicName");
            fields.Add("X");
            fields.Add("Y");
            fields.Add("IdentifiedBy");
            fields.Add("IdentificationConfirmedBy");
            fields.Add("IdentifiedOn");
            fields.Add("IdentificationComments");
            fields.Add("CollectedOrObservedByText");
            fields.Add("CollectionMethod");
            fields.Add("IsDead");
            fields.Add("Cf");
            fields.Add("CheckDate");
            fields.Add("DerivedFrom");
            fields.Add("Altitude1");
            fields.Add("Altitude2");
            fields.Add("Year");
            fields.Add("IdentifiedOnYear");
            fields.Add("IdentificationConfirmedYear");
            fields.Add("Month");
            fields.Add("Day");
            fields.Add("WKT");
            fields.Add("UnitID");
            fields.Add("FieldID");
            fields.Add("UnitType");
            fields.Add("TemporaryName");
            fields.Add("Date");
            fields.Add("EndOn");
            fields.Add("PreparationDate");
            fields.Add("PreparedBy");
            fields.Add("PreparationType");
            fields.Add("Locality");
            fields.Add("Locality1");
            fields.Add("Store");
            fields.Add("WhereInStore");
            fields.Add("PlaceDetails");
            fields.Add("HabitatDetails");
            fields.Add("AlternativeDate");
            fields.Add("BiologyDetails");
            fields.Add("QuantityUnit");
            fields.Add("Quantity");

            fields.Add("latdeg");
            fields.Add("latmin");
            fields.Add("latsec");
            fields.Add("longdeg");
            fields.Add("longmin");
            fields.Add("longsec");
            fields.Add("NS");
            fields.Add("WE");

            Dataset Dataset;
            User CreatedBy;
            User LastModifiedByUser;
            DateTime CreatedOn;
            DateTime LastModifiedOn;
            Country Country;
            LocalityVisit LocalityVisit;
            DevelopmentStage? Stage;
            SpecimenType? Type;
            TypeStatus? TypeStatus;
            Sex? Sex;
            TaxonomicName TaxonomicName;
            Unit DerivedFrom;
            Locality Locality;
            Locality Locality1;
            Store Store;


            string Comment;
            string Preservation;
            string OtherId;
            string CoordSource;
            string CoordDetails;
            string Host;
            string HostDetails;
            string LabelText;
            string InstitutionCode;
            string CollectionCode;
            string AssociatedTaxa;
            string IdentifiedBy;
            string IdentificationConfirmedBy;
            string IdentificationComments;
            string CollectedOrObservedByText;
            string CollectionMethod;
            string WKT;
            string UnitID;
            string FieldID;
            string UnitType;
            string TemporaryName;
            string WhereInStore;
            string PlaceDetails;
            string HabitatDetails;
            string AlternativeDate;
            string BiologyDetails;
            string QuantityUnit;
            string PreparedBy;
            string PreparationType;
           
     
            double Area;
            double CoordRadius;
            int EPSG;
            Decimal Centroid_X;
            Decimal Centroid_Y;
            double? X;
            double? Y;
            double? Altitude1;
            double? Altitude2;
            int? Year;
            int? IdentifiedOnYear;
            int? IdentificationConfirmedYear;
            int? Month;
            int? Day;
            Double Quantity;
   

            bool DoesNotExist;
            bool IsDead;
            bool Cf;
              
            
               DateTime BirthDate;
            DateTime DeathDate;
               DateTime IdentifiedOn;
            DateTime CheckDate;
            DateTime Date;
            DateTime EndOn;
            DateTime PreparationDate;
        
     
            // Access the first row by its index in the collection of rows.

            /*
                       private Dataset _Dataset;
            public string Comment
            public User CreatedBy;
            public User LastModifiedByUser;
            [NonCloneableAttribute] [ModelDefault("AllowEdit", "False")] public DateTime CreatedOn;
            [NonCloneableAttribute]   [ModelDefault("AllowEdit", "False")] public DateTime LastModifiedOn;
    public Country Country;
            public LocalityVisit LocalityVisit
            public string Preservation
             public string OtherId
               public double CoordRadius
               public string CoordSource
             private string _CoordDetails;
             public string Host
              public string HostDetails
             public string LabelText
           public double Area
             public int EPSG
              public bool DoesNotExist
            public Decimal Centroid_X
           public Decimal Centroid_Y
           public string InstitutionCode
          public string CollectionCode
              public DevelopmentStage? Stage
         public SpecimenType? Type
            public string AssociatedTaxa
           public TypeStatus? TypeStatus
           public Sex? Sex
           public DateTime BirthDate
           public DateTime DeathDate
             public TaxonomicName TaxonomicName
            public double? X
              public double? Y
            public string IdentifiedBy
              public string IdentificationConfirmedBy
             public DateTime IdentifiedOn
            public string IdentificationComments
           public string CollectedOrObservedByText
            public string CollectionMethod
           public bool IsDead
             public bool Cf
            public DateTime CheckDate
            public Unit DerivedFrom
            public double? Altitude1
           public double? Altitude2
           public int? Year
           public int? IdentifiedOnYear
           public int? IdentificationConfirmedYear
             public int? Month
            public int? Day
            public string WKT
             public string UnitID
             public string FieldID
            public string UnitType
              public string TemporaryName
             public DateTime Date { get; set; }
            public DateTime EndOn { get; set; }
            public DateTime PreparationDate { get; set; }
            public String PreparedBy { get; set; }
            public String PreparationType { get; set; }
   public Locality Locality
           public Locality Locality1
             public Store Store
            public string WhereInStore
             public string PlaceDetails
              public string HabitatDetails
             public string AlternativeDate
            public string BiologyDetails
          public string QuantityUnit
            public Double Quantity
        

          //  Collection<string> fields = new Collection<string>();
            fields.Add("Name");
            fields.Add("Altitude1");
            fields.Add("Altitude2");
            fields.Add("WKT");
            fields.Add("EPSG");
            fields.Add("Notes");
            fields.Add("Comments");
            fields.Add("X");
            fields.Add("Y");

            fields.Add("CoordRad");
            fields.Add("CoordSource");
            fields.Add("CoordDetails");
            fields.Add("CoordNotes");
            fields.Add("Country");
            fields.Add("CoordNotes");
   fields.Add("Admin1");
            fields.Add("Admin2");
            fields.Add("Admin3");
            fields.Add("Admin4");
            fields.Add("dummy");

            int readerline = 0;
            // User p = uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(); uow.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            /* while (reader.Read())
             {
                 String WKT = "";
                 String name = "";
                 String notes = "";
                 String locality = "";
                 String epsg = "";
                 String coordsource = "";
                 String alt1 = "";
                 String alt2 = "";
                 String Method = "";
                 try
                 {
                     if (reader.FieldExists("WKT"))
                         WKT = reader["WKT"].ToString();
                     if (reader.FieldExists("EPSG"))
                         epsg = reader["EPSG"].ToString();
                     if (reader.FieldExists("CoordSource"))
                         coordsource = reader["CoordSource"].ToString();
                     if (reader.FieldExists("Name"))
                         name = reader["Name"].ToString();
                     if (reader.FieldExists("Notes"))
                         notes = reader["Notes"].ToString();
                     if (reader.FieldExists("Locality"))
                         locality = reader["Locality"].ToString();
                     if (reader.FieldExists("Method"))
                         Method = reader["Method"].ToString();
                     if (reader.FieldExists("Altitude1"))
                         alt1 = reader["Altitude1"].ToString();
                     if (reader.FieldExists("Altitude2"))
                         alt2 = reader["Altitude2"].ToString();


                     LocalityAdm country = null;
                     LocalityAdm locadm1 = null;
                     LocalityAdm locadm2 = null;
                     LocalityAdm locadm3 = null;
                     LocalityAdm locadm4 = null;
                     if (reader.FieldExists("Country"))
                     {
                         if (!String.IsNullOrEmpty(reader["Country"].ToString()))
                         {
                             country = (Country)ImportHelper.CreateLocality(
                                 project,
                                 uow,
                                 reader["Country"].ToString(),
                                 "", "",
                                 alt1,
                                 alt2,
                                 WKT, true,
                                 notes,
                                 true, false, "country");

                         }
                     }
                     if (reader.FieldExists("Admin1"))
                     {
                         if (!String.IsNullOrEmpty(reader["Admin1"].ToString()))
                         {
                             string parent = "";
                             if (country != null) parent = country.Name;
                             locadm1 = (LocalityAdm1)ImportHelper.CreateLocality(project, uow, reader["Admin1"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm1");
                             if (country != null)
                             {
                                 locadm1.Parent = country;
                                 locadm1.Save();
                             }
                         }
                     }
                     if (reader.FieldExists("Admin2"))
                     {
                         if (!String.IsNullOrEmpty(reader["Admin2"].ToString()))
                         {
                             string parent = "";
                             if (locadm1 != null) parent = locadm1.Name;
                             else
                                 if (country != null) parent = country.Name;
                             locadm2 = (LocalityAdm2)ImportHelper.CreateLocality(project, uow, reader["Admin2"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm2");
                             if (locadm1 != null)
                             {
                                 locadm2.Parent = locadm1;
                                 locadm2.Save();
                             }
                             else
                                 if (country
                                     != null)
                                 {
                                     locadm2.Parent = country;
                                     locadm2.Save();
                                 }
                         }
                     }

                     if (reader.FieldExists("Admin3"))
                     {
                         if (!String.IsNullOrEmpty(reader["Admin3"].ToString()))
                         {
                             string parent = "";
                             if (locadm2 != null) parent = locadm2.Name;
                             else
                                 if (locadm1 != null) parent = locadm1.Name;
                                 else
                                     if (country != null) parent = country.Name;
                             locadm3 = (LocalityAdm3)ImportHelper.CreateLocality(project, uow, reader["Admin3"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm3");
                             if (locadm2 != null)
                             {
                                 locadm3.Parent = locadm2;
                                 locadm3.Save();
                             }
                             else
                                 if (locadm1 != null)
                                 {
                                     locadm3.Parent = locadm1;
                                     locadm3.Save();
                                 }
                                 else
                                     if (country
                                         != null)
                                     {
                                         locadm3.Parent = country;
                                         locadm3.Save();
                                     }
                         }
                     }
                     if (reader.FieldExists("Admin4"))
                     {
                         if (!String.IsNullOrEmpty(reader["Admin4"].ToString()))
                         {
                             string parent = "";
                             if (locadm3 != null) parent = locadm3.Name;
                             else
                                 if (locadm2 != null) parent = locadm2.Name;
                                 else
                                     if (locadm1 != null) parent = locadm1.Name;
                                     else
                                         if (country != null) parent = country.Name;
                             locadm4 = (LocalityAdm4)ImportHelper.CreateLocality(project, uow, reader["Admin4"].ToString(), parent, "", alt1, alt2, WKT, true, notes, true, false, "adm4");
                             if (locadm3 != null)
                             {
                                 locadm4.Parent = locadm3;
                                 locadm4.Save();
                             }
                             else
                                 if (locadm2 != null)
                                 {
                                     locadm4.Parent = locadm2;
                                     locadm4.Save();
                                 }
                                 else
                                     if (locadm1 != null)
                                     {
                                         locadm4.Parent = locadm1;
                                         locadm4.Save();
                                     }
                                     else
                                         if (country
                                             != null)
                                         {
                                             locadm4.Parent = country;
                                             locadm4.Save();
                                         }


                         }
                     }
                     if (!String.IsNullOrEmpty(name))
                     {
                         LocalityAdm lowerLoc = null;
                         if (locadm4 != null)
                             lowerLoc = locadm4;
                         else
                             if (locadm3 != null)
                                 lowerLoc = locadm3;
                             else
                                 if (locadm2 != null)
                                     lowerLoc = locadm2;
                                 else
                                     if (locadm1 != null)
                                         lowerLoc = locadm1;
                                     else
                                         if (country != null)
                                             lowerLoc = country;
                         ImportHelper.CalcIWKT(reader, ref alt1, ref alt2, ref WKT);
                         Locality loc = ImportHelper.CreateLocality(project, uow, name, lowerLoc, "", alt1, alt2, WKT, true, notes, true, true);
                         double? X;
                         double? Y;
                         ImportHelper.LatLongs(reader, out X, out Y);
                         if ((X != null) && (Y != null))
                         {
                             loc.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                         }
                         if (reader.FieldExists("EPSG"))
                             if (!String.IsNullOrEmpty(reader["EPSG"].ToString()))
                                 loc.EPSG = Convert.ToInt32(reader["EPSG"].ToString());
                         if (reader.FieldExists("Length"))
                         {
                             if (!String.IsNullOrEmpty(reader["Length"].ToString()))
                             {
                                 loc.Length = Convert.ToDouble(reader["Length"].ToString());
                             }
                         }
                         if (reader.FieldExists("Area"))
                         {
                             if (!String.IsNullOrEmpty(reader["Area"].ToString()))
                             {
                                 loc.Area = Convert.ToDouble(reader["Area"].ToString());
                             }
                         }
                         if (reader.FieldExists("ReasonNotValid"))
                         {
                             if (!String.IsNullOrEmpty(reader["ReasonNotValid"].ToString()))

                                 loc.ReasonNotValid = reader["ReasonNotValid"].ToString();
                         }
                         if (reader.FieldExists("NotValid"))
                         {
                             if (!String.IsNullOrEmpty(reader["NotValid"].ToString()))
                             {
                                 if (Convert.ToBoolean(reader["NotValid"]) == true)
                                     loc.NotValid = true;
                                 if (Convert.ToBoolean(reader["NotValid"]) == false)
                                     loc.NotValid = false;
                             }
                         }
                         if (reader.FieldExists("ValidLocality"))
                         {
                             if (!String.IsNullOrEmpty(reader["ValidLocality"].ToString()))
                             {
                                 Locality locValid = uow.FindObject<Locality>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                 if (locValid == null)
                                     locValid = uow.FindObject<Locality>(new BinaryOperator("Name", reader["ValidLocality"].ToString()));
                                 if (locValid != null)
                                 {
                                     loc.ValidLocality = locValid;
                                 }
                             }


                         }

                         if ((loc != null) && (lowerLoc != null))
                         {
                             loc.Category = lowerLoc;
                             loc.Save();
                         }
                         else if (lowerLoc != null)
                         { loc = lowerLoc; }
                         if (loc != null)
                         {
                             if (importcustomdata)
                                 TagHelper.CreateTags(dataset, uow, reader, fields, loc, "", errors, IfFieldExists.Overwrite);
                         }
                     }

                 }
                 catch (Exception ex)
                 {
                     errors.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));

                 }
                 finally
                 {

                 }
                 readerline += 1;
                 if (readerline % 1000 == 999)
                 {
                     errors.WriteLine(DateTime.Now.ToString() + readerline + "records done");
                     errors.WriteLine("committing...");
                     errors.WriteLine("committed " + DateTime.Now.ToString());
                     uow.CommitChanges();
                 }

             }
             reader.Close();
             cn.Close();*/
             //errors.WriteLine("import finished (" + readerline + " records) " + DateTime.Now.ToString());
           // errors.Close();
           // Process.Start(logpath)