using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using System.Windows.Forms;
using System.ComponentModel;
using EarthCape.Module.Core;
using DevExpress.Spreadsheet;
using System.Collections.ObjectModel;
using DevExpress.XtraSplashScreen;
using System.IO;
using EarthCape.Taxonomy;
using DevExpress.XtraSpreadsheet;
using System.Drawing;
using System.Diagnostics;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.ExpressApp.SystemModule;

namespace EarthCape.Module.Importer.Win.Controllers
{
    public partial class SpreadsheetImporter_ViewController : ViewController
    {
        static Locker locker1;
        static BackgroundWorker backgroundWorker1;
        static ImportOptions options;
        static IObjectSpace os;
        // static Worksheet results = null; 

        public SpreadsheetImporter_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_ImportSpreadsheet_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace obs = Application.CreateObjectSpace();
            DetailView dv = Application.CreateDetailView(obs, obs.CreateObject<ImportUnitsOptions>());
            e.View = dv;
        }

        private void popupWindowShowAction_ImportSpreadsheet_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            RunImport(e);
        }

        private void RunImport(PopupWindowShowActionExecuteEventArgs e)
        {
            os = Application.CreateObjectSpace();
            options = (ImportOptions)e.PopupWindow.View.CurrentObject;
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            locker1 = new Locker();
            backgroundWorker1.RunWorkerAsync(locker1);
        }

        private void simpleAction_ChooseFiles_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            ImportOptions options = (ImportOptions)View.CurrentObject;
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select files";
            dlgOpen.Filter = "Spreadsheets (*.xls,*.xlsx,*.csv,*.txt)|*.xls;*.xlsx;*.csv;*.txt|Microsoft Excel 97-2003 Workbooks (*.xls)|*.xls|Microsoft Excel 2007-2013 Workbooks (*.xlsx)|*.xlsx|CSV (Comma delimited) (*.csv)|*.csv|Text (Tab delimited) (*.txt)|*.txt";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    options.Files = (String.IsNullOrEmpty(options.Files)) ? file : options.Files + System.Environment.NewLine + file;
                }
            }
        }
        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SplashScreenManager.CloseForm();
            locker1.IsCanceled = false;
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message); return;
            }
            else
            {
                os.CommitChanges();
                /* SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
                 if (contr != null)
                     contr.popupWindowShowAction_ImportUnits.Active.SetItemValue("Import", true);*/
            }
        }

        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            /*   SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
         if (contr != null)
            {
                DisableActions(contr);
            }*/
            BackgroundWorker worker = sender as BackgroundWorker;
            Worksheet sheet = null;
            Range range = null;
            Collection<string> filefields;
            if (worker != null)
            {
                try
                {
                    SplashScreenManager.ShowForm(typeof(SplashScreen_process));
                    string[] lines = options.Files.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetTotalFileCount, lines.Length);
                    int current = 0;
                    foreach (string fn in lines)
                    {
                        if (((ILocked)e.Argument).IsCanceled)
                        {
                            break;
                        }
                        try
                        {
                            current++;
                            //SpreadsheetControl spreadsheet = new SpreadsheetControl();
                            //results = spreadsheet.Document.Worksheets[0];
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCurrentFileCount, current);
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetFileLabel, Path.GetFileName(fn));
                            LoadDocument(out sheet, out range, out filefields, fn);
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCount, range.RowCount - 1);
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SendObject, locker1);
                            if (options is ImportUnitsOptions)
                                ImportUnits(e, sheet, range, filefields, (ImportUnitsOptions)options);
                            if (options is ImportOptionsProjectDataLocalities)
                                ImportLocalities(e, sheet, range, filefields, (ImportOptionsProjectDataLocalities)options);
                            if (options is ImportOptionsProjectDataTaxa)
                                ImportTaxa(e, sheet, range, filefields, (ImportOptionsProjectDataTaxa)options);
                        }
                        finally
                        {
                            if (sheet != null)
                            {
                                string output = String.Format("{0}\\{1}_{2}{3}", Path.GetTempPath(), Path.GetFileNameWithoutExtension(fn), DateTime.Now.ToString("yyyyMMddHHmmssffff"), Path.GetExtension(fn));
                                sheet.Workbook.SaveDocument(output, DocumentFormat.OpenXml);
                                Process.Start(output);
                            }
                            /* if (contr != null)
                             {
                                 EnableActions(contr);
                             }*/
                        }
                    }
                }
                finally
                {
                    /* if (contr != null)
                    {
                        EnableActions(contr);
                    }*/
                }
            }
        }

        private void ImportUnits(DoWorkEventArgs e, Worksheet sheet, Range range, Collection<string> filefields, ImportUnitsOptions options)
        {
            for (int i = 1; i < range.RowCount - 1; i++)
            {
                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                Unit unit = null;
                String UnitID;
                if (filefields.IndexOf("UnitID") > -1)
                {
                    UnitID = sheet.Cells[i, filefields.IndexOf("UnitID")].Value.ToString();
                    if (!String.IsNullOrEmpty(UnitID))
                    {
                        CriteriaOperator cr = null;
                        if (options.UnitIdMatching == UnitIdMatching.MatchWithinDataset)
                        {
                            if (options.Dataset != null)
                                cr = CriteriaOperator.And(new BinaryOperator("Dataset.Oid", options.Dataset.Oid), new BinaryOperator("UnitID", UnitID));
                        }
                        if (options.UnitIdMatching == UnitIdMatching.MatchWithinProject)
                        {
                            if (options.Dataset != null)
                                if (options.Dataset.Project != null)
                                    cr = CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", options.Dataset.Project.Oid), new BinaryOperator("UnitID", UnitID));
                        }
                        if (!ReferenceEquals(cr, null))
                        {
                            unit = os.FindObject<Unit>(cr);
                            //if (unit == null)
                            //   unit = os.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                        }
                    }
                }
                if (unit == null)
                    unit = os.CreateObject<Unit>();

                //ImportSimpleUnitFields(sheet, filefields, i, unit);
                ImportSimplePropertiesGeneric(unit, sheet, range, filefields, i);
                ImportComplexUnitFields(sheet, filefields, i, unit, ((options.Dataset != null) && (options.Dataset.Project != null)) ? (options.Dataset.Project) : null);

                unit.Save();
                os.CommitChanges();
                if (((ILocked)e.Argument).IsCanceled)
                {
                    break;
                }
            }
        }

        private void ImportLocalities(DoWorkEventArgs e, Worksheet sheet, Range range, Collection<string> filefields, ImportOptionsProjectDataLocalities options)
        {
            /*
            filefields.Add("X");
            filefields.Add("Y");
            filefields.Add("latdeg");
            filefields.Add("latmin");
            filefields.Add("latsec");
            filefields.Add("longdeg");
            filefields.Add("longmin");
            filefields.Add("longsec");
            filefields.Add("NS");
            filefields.Add("WE");
            filefields.Add("Admin1");
            filefields.Add("Admin2");
            filefields.Add("Admin3");
            filefields.Add("Admin4");
            */


            for (int i = 1; i < range.RowCount; i++)
            {
                try
                {
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                    Locality Locality = null;
                    String LocalityName;
                    CriteriaOperator cr = null;
                    if (filefields.IndexOf("Name") > -1)
                    {
                        LocalityName = sheet.Cells[i, filefields.IndexOf("Name")].Value.ToString();
                        if (!String.IsNullOrEmpty(LocalityName))
                        {
                            if (options.DuplicatesLookupOption == DuplicateLocalitiesLookupOption.MatchNamesWithinProject)
                            {
                                if (options.Project != null)
                                    cr = CriteriaOperator.And(new BinaryOperator("Project.Oid", options.Project.Oid), new BinaryOperator("Name", LocalityName));
                            }
                            if (options.DuplicatesLookupOption == DuplicateLocalitiesLookupOption.MatchAllNamesInDb)
                            {
                                if (options.Project != null)
                                    cr = new BinaryOperator("Name", LocalityName);
                            }
                        }
                    }
                    if (filefields.IndexOf("Code") > -1)
                    {
                        string code = sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString();
                        if (options.DuplicatesLookupOption == DuplicateLocalitiesLookupOption.MatchCodesWithinProject)
                        {
                            if (options.Project != null)
                                cr = CriteriaOperator.And(new BinaryOperator("Project.Oid", options.Project.Oid), new BinaryOperator("Code", code));
                        }
                        if (options.DuplicatesLookupOption == DuplicateLocalitiesLookupOption.MatchAllCodesInDb)
                        {
                            if (options.Project != null)
                                cr = new BinaryOperator("Code", code);
                        }
                    }
                    if (!ReferenceEquals(cr, null))
                    {
                        Locality = os.FindObject<Locality>(cr);
                    }
                    //  if (Locality == null)
                    //     Locality = os.CreateObject<Locality>();
                    Locality = ImportAdmin(sheet, filefields, i, Locality, cr); // can result in Admin, country, continent or locality
                    ImportSimpleLocalityFields(sheet, filefields, i, Locality);
                    Locality.Save();
                    /*  CalcIWKT(reader, ref alt1, ref alt2, ref WKT);
                          double? X;
                          double? Y;
                          ImportHelper.LatLongs(reader, out X, out Y);
                          if ((X != null) && (Y != null))
                          {
                              loc.WKT = String.Format("POINT ({0} {1})", X.ToString().Replace(",", "."), Y.ToString().Replace(",", "."));
                          }
                         */
                    if (Locality != null)
                    {
                        try
                        {
                            os.CommitChanges();
                            sheet.Cells[i, range.ColumnCount].SetValue("ok");
                            sheet.Cells[i, range.ColumnCount].Fill.BackgroundColor = Color.Green;
                        }
                        catch (Exception ex)
                        {
                            sheet.Cells[i, range.ColumnCount + 1].SetValue(ex.Message);
                            sheet.Cells[i, range.ColumnCount].SetValue("failed");
                            sheet.Cells[i, range.ColumnCount].Fill.BackgroundColor = Color.Red;
                        }
                        finally
                        {

                        }
                    }
                    if (((ILocked)e.Argument).IsCanceled)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    sheet.Cells[i, range.ColumnCount + 1].SetValue(ex.Message);
                    sheet.Cells[i, range.ColumnCount].SetValue("failed");
                    sheet.Cells[i, range.ColumnCount].Style.Fill.BackgroundColor = Color.Red;
                }

            }
        }

        TaxonomicName taxon = null;
        TaxonomicName parent = null;

        private void ImportTaxa(DoWorkEventArgs e, Worksheet sheet, Range range, Collection<string> filefields, ImportOptionsProjectDataTaxa options)
        {
            List<string> supportedCategories = new List<string>(new string[]
	{
	    "Kingdom",
	    "Phylum",    
	    "Class",    
	    "Order",
	    "Family",
	    "Subfamily",
	    "Genus",
	    "Species",
	    "Subspecies",
	    "Variety",
	    "Form"
	});
            string parentColumn = "";
            sheet.Cells[0, range.ColumnCount].SetValue("ImportedName");
            sheet.Cells[0, range.ColumnCount + 1].SetValue("Imported");
            foreach (string category in supportedCategories)
            {
                if (filefields.IndexOf(category) > -1)
                {
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, String.Format("Importing {0}:", category));
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, 0);
                    GetTaxonomicColumn(e, sheet, category, options, range, filefields, supportedCategories);
                    os.CommitChanges();
                }
            }
            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, 0);
            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Importing rest of data: ");
            for (int i = 1; i < range.RowCount; i++)
            {
                try
                {
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                    TaxonomicName taxon = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", sheet.Cells[i, range.ColumnCount + 1].Value.ToString()));
                    if ((filefields.IndexOf("Authorship") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Authorship")].Value.ToString()))) taxon.Authorship = sheet.Cells[i, filefields.IndexOf("Authorship")].Value.ToString();
                    if ((filefields.IndexOf("AuthorshipYear") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("AuthorshipYear")].Value.ToString()))) taxon.AuthorshipYear = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("AuthorshipYear")].Value.NumericValue);
                    if ((filefields.IndexOf("ShortName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString()))) taxon.ShortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                    if ((filefields.IndexOf("CreatedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.ToString()))) taxon.CreatedOn = sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.DateTimeValue;
                    if ((filefields.IndexOf("LastModifiedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.ToString()))) taxon.LastModifiedOn = sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.DateTimeValue;
                    if (((ILocked)e.Argument).IsCanceled)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    sheet.Cells[i, range.ColumnCount + 3].SetValue(ex.Message);
                    sheet.Cells[i, range.ColumnCount + 2].SetValue("failed");
                    sheet.Cells[i, range.ColumnCount + 2].Style.Fill.BackgroundColor = Color.Red;
                }

            }
        }

        private void GetTaxonomicColumn(DoWorkEventArgs e, Worksheet sheet, string category, ImportOptionsProjectDataTaxa options, Range range, Collection<string> filefields, List<string> supportedCategories)
        {
            if (filefields.IndexOf(category) > -1)
            {
                string current = "";
                Guid parentOid = Guid.Empty;
                Guid currentOid = Guid.Empty;
                TaxonomicName taxon = null;
                for (int i = 1; i < range.RowCount; i++)
                {
                    try
                    {
                        SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                        if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(category)].Value.ToString()))
                        {
                            if (sheet.Cells[i, filefields.IndexOf(category)].Value.ToString() != current)
                            {
                                string shortName = "";
                                if ((filefields.IndexOf("ShortName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString())))
                                    shortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                                parentOid = Guid.Empty;
                                current = sheet.Cells[i, filefields.IndexOf(category)].Value.ToString();
                                string parentColumn = GetParentColumn(sheet, category, i, supportedCategories, filefields);
                                if (!string.IsNullOrEmpty(parentColumn))
                                    parentOid = Guid.Parse(sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString());
                                taxon = GetTaxon(e, sheet, current, category, shortName, parentOid, options);
                                if (taxon != null)
                                {
                                    parent = taxon;
                                    currentOid = taxon.Oid;
                                }
                            }
                            sheet.Cells[i, filefields.IndexOf(category)].SetValue(taxon.Oid.ToString());
                            if ((String.IsNullOrEmpty(sheet.Cells[i, range.ColumnCount].Value.ToString())) || IsLower(supportedCategories, category, sheet.Cells[i, range.ColumnCount].Value.ToString()))
                            {
                                sheet.Cells[i, range.ColumnCount].SetValue(category);
                                sheet.Cells[i, range.ColumnCount + 1].SetValue(taxon.Oid.ToString());
                            }


                        }
                        if (((ILocked)e.Argument).IsCanceled)
                        {
                            break;
                        }
                    }
                    catch
                    {
                        if (((ILocked)e.Argument).IsCanceled)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private bool IsLower(List<string> supportedCategories, string category, string p)
        {
            return supportedCategories.IndexOf(category) > supportedCategories.IndexOf(p);
        }

        private string GetParentColumn(Worksheet sheet, string p, int row, List<string> supportedCategories, Collection<string> filefields)
        {
            int colIndex = -1;
            int i = supportedCategories.IndexOf(p) - 1;
            while ((colIndex == -1) && (i > -1))
            {
                if (filefields.IndexOf(supportedCategories[i]) > -1)
                    if ((!string.IsNullOrEmpty(sheet.Cells[row, filefields.IndexOf(supportedCategories[i])].Value.ToString())))
                        colIndex = filefields.IndexOf(supportedCategories[i]);
                i = i - 1;
            }
            if (colIndex == -1)
                return "";
            return filefields[colIndex];
        }

        private TaxonomicName GetTaxon(DoWorkEventArgs e, Worksheet sheet, string taxonName, string shortName, string categoryName, Guid parentOid, ImportOptionsProjectDataTaxa options)
        {
            TaxonomicName taxon = null;
            TaxonomicName parentTaxon = null;
            CriteriaOperator cr = null;
            Guid projectOid = Guid.Empty;
            if (options.Project != null) projectOid = options.Project.Oid;
            if (!parentOid.Equals(Guid.Empty))
            {
                cr = new BinaryOperator("Parent", parentOid);
                parentTaxon = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", parentOid));
            }
            cr = GetOptionsCriteria(options, taxonName, shortName, parentOid, projectOid);
            taxon = os.FindObject<TaxonomicName>(cr);
            if (taxon == null)
            {
                if (categoryName == "Genus")
                    taxon = os.CreateObject<Genus>();
                else
                    if (categoryName == "Species")
                    {
                        taxon = os.CreateObject<Species>();
                        if (parentTaxon is HigherTaxon)
                            ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                        if (parentTaxon is Genus)
                            ((Species)taxon).Genus = (Genus)parentTaxon;
                    }
                    else
                        if (categoryName == "Family")
                            taxon = os.CreateObject<Family>();
                        else
                            if ((categoryName == "Subspecies") || (categoryName == "Variety") || (categoryName == "Form"))
                                taxon = os.CreateObject<SubspecificTaxon>();
                            else
                                taxon = os.CreateObject<HigherTaxon>();
                taxon.Name = taxonName;
                taxon.Parent = parentTaxon;
                taxon.Save();
                os.CommitChanges();
            }
            return taxon;
        }

        private CriteriaOperator GetOptionsCriteria(ImportOptionsProjectDataTaxa options, string name, string shortName, Guid parentOid, Guid projectOid)
        {
            CriteriaOperator parentCr = null;
            CriteriaOperator projectCr = null;
            if (!parentOid.Equals(Guid.Empty))
                parentCr = new BinaryOperator("Parent", parentOid);
            if (!projectOid.Equals(Guid.Empty))
                projectCr = new BinaryOperator("Project", projectOid);
            if (options.DuplicatesLookupOption == DuplicateTaxaLookupOption.NoMatching)
                return parentCr;
            if (options.DuplicatesLookupOption == DuplicateTaxaLookupOption.MatchAllNamesInDb)
                return CriteriaOperator.And(new BinaryOperator("Name", name), parentCr);
            if (options.DuplicatesLookupOption == DuplicateTaxaLookupOption.MatchAllShortNamesInDb)
                return CriteriaOperator.And(new BinaryOperator("ShortName", shortName), parentCr);
            if (options.DuplicatesLookupOption == DuplicateTaxaLookupOption.MatchShortNameWithinProject)
                return CriteriaOperator.And(new BinaryOperator("ShortName", shortName), parentCr, projectCr);
            if (options.DuplicatesLookupOption == DuplicateTaxaLookupOption.MatchNamesWithinProject)
                return CriteriaOperator.And(new BinaryOperator("Name", name), parentCr, projectCr);
            return null;
        }



        /*
        private void ImportTaxa(DoWorkEventArgs e, Worksheet sheet, Range range, Collection<string> filefields, ImportOptionsProjectDataTaxa options)
        {
            

            Project Project = options.Project;
            for (int i = 1; i < range.RowCount; i++)
            {
                Species species = null;
                Genus genus = null;
                Species validSpecies = null;
                HigherTaxon subfamily = null;
                Family family = null;
                HigherTaxon Order = null;
                TaxonomicName name = null;
                string shortName = "";
                try
                {
                    string auth = "";
                    if (filefields.IndexOf("Author") > -1)
                        auth = sheet.Cells[i, filefields.IndexOf("Authorship")].Value.ToString();
                    string year = "";
                    if (filefields.IndexOf("AuthorshipYear") > -1)
                        year = sheet.Cells[i, filefields.IndexOf("AuthorshipYear")].Value.ToString();

                    if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString()))
                    {
                        if ((filefields.IndexOf("ShortName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString())))
                            species = os.FindObject<Species>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString()));
                        else
                             species = os.FindObject<Species>(new BinaryOperator("FullName", sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString()));
                        if (species == null)
                        {
                            species = null;// new Species(os);
                            if ((filefields.IndexOf("Genus") > -1) && (filefields.IndexOf("Species") > -1))
                                TaxonomyHelper.ParseBinomial(os, ref species, ref genus, auth, year, sheet.Cells[i, filefields.IndexOf("Genus")].Value.ToString().Trim() + " " + sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString().Trim(), " ", Project);
                            if (species == null)
                                TaxonomyHelper.ParseBinomial(os, ref species, ref genus, auth, year, sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString().Trim(), ".", Project);
                            //species.Genus=TaxonomyHelper.GetGenus(
                            if (species == null)
                            {
                                species = os.CreateObject<Species>();
                                species.Name = "";
                                genus = TaxonomyHelper.GetGenus(os, Project, sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString().Trim(), "", 0);
                                species.Genus = genus;

                            }
                            if (filefields.IndexOf("ShortName") > -1)
                                species.ShortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString().Trim();
                            if (Project != null)
                                species.Project = Project;
                            species.UpdateInfo();
                            species.Save();
                        }
                        else
                        {
                            if (Project != null)
                                if (species.Project != Project)
                                {
                                    species.Project = Project;
                                    species.UpdateInfo();
                                    species.Save();
                                }
                        }
                        if (filefields.IndexOf("Comment") > -1)
                            if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString()))
                            {
                                species.Comment = sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString();
                                species.UpdateInfo();
                                species.Save();
                            }
                    }
                    if ((filefields.IndexOf("Genus") > -1) && (genus == null))
                        if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Genus")].Value.ToString()))
                        {
                            string genus_string = sheet.Cells[i, filefields.IndexOf("Genus")].Value.ToString().Trim();
                            genus = TaxonomyHelper.GetGenus(os, Project, genus_string, "", 0);
                        }
                    if (filefields.IndexOf("ValidSpecies") > -1)
                        if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ValidSpecies")].Value.ToString()))
                        {
                            validSpecies = os.FindObject<Species>(new BinaryOperator("FullName", sheet.Cells[i, filefields.IndexOf("ValidSpecies")].Value.ToString()));
                            if (validSpecies == null)
                            {

                                Genus validgen = null;
                                TaxonomyHelper.ParseBinomial(os, ref validSpecies, ref validgen, "", "", sheet.Cells[i, filefields.IndexOf("ValidSpecies")].Value.ToString().Trim(), " ", Project);
                                if (validSpecies == null)
                                    TaxonomyHelper.ParseBinomial(os, ref validSpecies, ref validgen, "", "", sheet.Cells[i, filefields.IndexOf("ValidSpecies")].Value.ToString().Trim(), ".", Project);

                                validSpecies.UpdateInfo();
                                validSpecies.Save();
                            }
                            else
                            {
                                if (Project != null)
                                    if (validSpecies.Project != Project)
                                    {
                                        validSpecies.Project = Project;
                                        validSpecies.UpdateInfo();
                                        validSpecies.Save();
                                    }
                            }
                            if (species != null)
                            {
                                Synonymy synonymy = os.CreateObject<Synonymy>();
                                synonymy.TaxonomicName = validSpecies;
                                synonymy.Synonym = species;
                                synonymy.Save();
                            }
                        }
                    if ((filefields.IndexOf("ShortName") > -1) && (species == null))
                    {
                        species = os.FindObject<Species>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString()));
                        if (species == null)
                        {
                            species = os.CreateObject<Species>();
                            species.ShortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                            if (genus != null)
                            {
                                species.Genus = genus;
                            }
                            species.UpdateInfo();
                            species.Save();
                        }

                    }

                    if ((validSpecies != null) && (validSpecies.Genus != null))
                    {
                        validSpecies.Parent = validSpecies.Genus;
                        validSpecies.Category = validSpecies.Genus;
                        validSpecies.UpdateInfo();
                        validSpecies.Save();
                    }
                    if ((species != null) && (genus != null))
                    {
                        species.Genus = genus;
                        species.Category = genus;
                        species.Parent = genus;
                        species.UpdateInfo();
                        species.Save();
                    }

                    if (filefields.IndexOf("Subfamily") > -1)
                        if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Subfamily")].Value.ToString()))
                        {
                            subfamily = os.FindObject<HigherTaxon>(new BinaryOperator("Name", sheet.Cells[i, filefields.IndexOf("Subfamily")].Value.ToString().Trim()));
                            if (subfamily == null)
                            {
                                subfamily = os.CreateObject<HigherTaxon>();
                                subfamily.Name = sheet.Cells[i, filefields.IndexOf("Subfamily")].Value.ToString().Trim();
                                subfamily.Rank = GeneralHelper.GetRank(os, "Subfamily");
                                if (Project != null)
                                    subfamily.Project = Project;
                                subfamily.Save();
                            }
                            else
                            {
                                if (Project != null)
                                {
                                    subfamily.Project = Project;
                                    subfamily.Save();
                                }
                            }
                            if (genus != null)
                            {
                                genus.Parent = subfamily;
                                genus.Save();
                                if (species != null)
                                {
                                    species.Category = genus;
                                    species.Parent = genus;
                                    species.Save();
                                }
                            }
                            else
                                if (species != null)
                                {
                                    species.Category = subfamily;
                                    species.Parent = subfamily;
                                    species.Save();
                                }
                        }
                    if (filefields.IndexOf("Family") > -1)
                        if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Family")].Value.ToString()))
                        {
                            family = os.FindObject<Family>(new BinaryOperator("Name", sheet.Cells[i, filefields.IndexOf("Family")].Value.ToString().Trim()));
                            if (family == null)
                            {
                                family = os.CreateObject<Family>();
                                family.Rank = GeneralHelper.GetRank(os, "Family");
                                family.Name = sheet.Cells[i, filefields.IndexOf("Family")].Value.ToString().Trim();
                                if (Project != null)
                                    family.Project = Project;
                                family.Save();

                            }
                            if (subfamily != null)
                            {
                                subfamily.Parent = family;
                                subfamily.Save();
                            }
                            else
                                if (genus != null)
                                {
                                    genus.Parent = family;
                                    genus.Save();
                                    if (species != null)
                                    {
                                        species.Category = genus;
                                        species.Parent = genus;
                                        species.UpdateInfo();
                                        species.Save();
                                    }
                                }
                                else
                                    if (species != null)
                                    {
                                        species.Category = family;
                                        species.Parent = family;
                                        species.UpdateInfo();
                                        species.Save();
                                    }
                        }

                    if (filefields.IndexOf("Order") > -1)
                        if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Order")].Value.ToString()))
                        {
                            Order = os.FindObject<HigherTaxon>(new BinaryOperator("Name", sheet.Cells[i, filefields.IndexOf("Order")].Value.ToString().Trim()));
                            if (Order == null)
                            {
                                Order = os.CreateObject<HigherTaxon>();
                                Order.Rank = GeneralHelper.GetRank(os, "Order");
                                Order.Name = sheet.Cells[i, filefields.IndexOf("Order")].Value.ToString().Trim();
                                if (Project != null)
                                    Order.Project = Project;
                                Order.Save();
                            }
                            else
                            {
                                if (Project != null)
                                {
                                    Order.Project = Project;
                                    Order.Save();
                                }
                            }
                            if (family != null)
                            {
                                family.Parent = Order;
                                family.Save();
                            }
                            else
                            {
                                if (subfamily != null)
                                {
                                    subfamily.Parent = Order;
                                    subfamily.Save();
                                }
                                else
                                    if (genus != null)
                                    {
                                        genus.Parent = Order;
                                        genus.Save();
                                        if (species != null)
                                        {
                                            species.Category = genus;
                                            species.Parent = genus;
                                            species.UpdateInfo();
                                            species.Save();
                                        }
                                    }
                                    else
                                    {
                                        if (species != null)
                                        {
                                            species.Category = Order;
                                            species.Parent = Order;
                                            species.UpdateInfo();
                                            species.Save();
                                        }

                                    }
                            }
                        }
                    if (validSpecies != null)
                    {
                        if (validSpecies.Genus != null)
                        {
                            if (species != null)
                            {
                                if (species.Genus != null)
                                {
                                    validSpecies.Genus.Parent = species.Genus.Parent;
                                    validSpecies.Genus.Save();
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    if (kingdom != null)
                        name = kingdom;
                    if (phylum != null)
                        name = phylum;
                    if (clss != null)
                        name = clss;
                    if (order != null)
                        name = order;
                    if (family != null)
                        name = family;
                    if (subfamily != null)
                        name = subfamily;
                    if (genus != null)
                        name = genus;
                    if (species != null)
                        name = species;
                    if (!string.IsNullOrEmpty(auth))
                        name.Authorship = auth;
                    if (!string.IsNullOrEmpty(year))
                        try
                        {
                            name.AuthorshipYear = Convert.ToInt32(year);
                        }
                        catch
                        {
                            sheet.Cells[i, filefields.IndexOf("Genus")].FillColor = Color.Red;
                        }
                }

   

                catch (Exception ex)
                {
                    sheet.Cells[i, range.ColumnCount + 1].SetValue(ex.Message);
                    sheet.Cells[i, range.ColumnCount].SetValue("failed");
                    sheet.Cells[i, range.ColumnCount].Style.Fill.BackgroundColor = Color.Red;
                }

            }
        }
   */
        /* 
         * 
         *        public static void LatLongs(OleDbDataReader reader, out double? X, out double? Y)
              {
                  X = null;
                  Y = null;
                  if (reader.FieldExists("latdeg")>-1)
                      if (reader.FieldExists("latmin"))
                          if (reader.FieldExists("latsec"))
                              if (reader.FieldExists("longdeg"))
                                  if (reader.FieldExists("longmin"))
                                      if (reader.FieldExists("longsec"))
                                      {

                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latmin")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latsec")].Value.ToString())))
                                          {
                                              double deg = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString());
                                              double min = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latmin")].Value.ToString());
                                              double sec = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latsec")].Value.ToString());
                                              Y = deg + (min + sec / 60) / 60;
                                          }


                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latmin")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latsec")].Value.ToString())))
                                          {
                                              double deg = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString());
                                              double min = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latmin")].Value.ToString());
                                              Y = deg + min / 60;
                                          }
                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latmin")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("latsec")].Value.ToString())))
                                              Y = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("latdeg")].Value.ToString());
                                          if (reader.FieldExists("NS"))
                                          {
                                              if (sheet.Cells[i, filefields.IndexOf("NS")].Value.ToString() == "S")
                                              {
                                                  Y = Y * (-1);
                                              }
                                          }
                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longmin")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longsec")].Value.ToString())))
                                          {
                                              double deg = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString());
                                              double min = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longmin")].Value.ToString());
                                              double sec = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longsec")].Value.ToString());
                                              X = deg + (min + sec / 60) / 60;
                                          }
                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString())) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longmin")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longsec")].Value.ToString())))
                                          {
                                              double deg = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString());
                                              double min = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longmin")].Value.ToString());
                                              X = deg + min / 60;
                                          }
                                          if ((!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longmin")].Value.ToString())) && (String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("longsec")].Value.ToString())))
                                              X = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("longdeg")].Value.ToString());
                                          if (reader.FieldExists("WE"))
                                          {
                                              if (sheet.Cells[i, filefields.IndexOf("WE")].Value.ToString() == "W")
                                              {
                                                  X = X * (-1);
                                              }
                                          }

                                   
              }
       public static void CalcIWKT(OleDbDataReader reader, ref string altitude, ref string altitude2, ref string WKT)
              {
                  if (reader.FieldExists("Altitude1"))
                      altitude = sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.ToString();
                  if (reader.FieldExists("Altitude2"))
                      altitude2 = sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.ToString();
                  if (reader.FieldExists("WKT"))
                      if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString()))
                      {
                          WKT = sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString();
                      }
                  if (String.IsNullOrEmpty(WKT))
                  {
                      if (reader.FieldExists("X"))
                          if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("X")].Value.ToString()))
                          {
                              if (reader.FieldExists("Y"))
                                  if (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Y")].Value.ToString()))
                                  {
                                      double y = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("Y")].Value.ToString());
                                      double x = Convert.ToDouble(sheet.Cells[i, filefields.IndexOf("X")].Value.ToString());
                                      WKT = String.Format("POINT ({0} {1})", x.ToString().Replace(",", "."), y.ToString().Replace(",", "."));
                                  }
                          }
                  }
              }*/

        private Locality ImportAdmin(Worksheet sheet, Collection<string> filefields, int i, Locality Locality, CriteriaOperator cr)
        {
            Continent continent = null;
            Country country = null;
            LocalityAdm Admin1 = null;
            LocalityAdm Admin2 = null;
            LocalityAdm Admin3 = null;
            LocalityAdm Admin4 = null;
            LocalityAdm parent = null;
            continent = (Continent)CreateAdm(sheet, filefields, i, cr, parent, "Continent");
            if (continent != null) parent = continent;
            country = (Country)CreateAdm(sheet, filefields, i, cr, parent, "Country");
            if (country != null) parent = country;
            Admin1 = (LocalityAdm)CreateAdm(sheet, filefields, i, cr, parent, "Admin1");
            if (Admin1 != null) parent = Admin1;
            Admin2 = (LocalityAdm)CreateAdm(sheet, filefields, i, cr, parent, "Admin2");
            if (Admin2 != null) parent = Admin2;
            Admin3 = (LocalityAdm)CreateAdm(sheet, filefields, i, cr, parent, "Admin3");
            if (Admin3 != null) parent = Admin3;
            Admin4 = (LocalityAdm)CreateAdm(sheet, filefields, i, cr, parent, "Admin3");
            if (Admin4 != null) parent = Admin4;

            if (Admin4 != null)
            {
                if (Locality != null)
                {
                    Locality.Category = Admin4;
                    return Locality;
                }
                return Admin4;
            }
            else
                if (Admin3 != null)
                {
                    if (Locality != null)
                    {
                        Locality.Category = Admin3;
                        return Locality;
                    }
                    return Admin3;
                }
                else
                    if (Admin2 != null)
                    {
                        if (Locality != null)
                        {
                            Locality.Category = Admin2;
                            return Locality;
                        }
                        return Admin2;
                    }
                    else
                        if (Admin1 != null)
                        {
                            if (Locality != null)
                            {
                                Locality.Category = Admin1;
                                return Locality;
                            }
                            return Admin1;
                        }
                        else
                            if (country != null)
                            {
                                if (Locality != null)
                                {
                                    Locality.Category = country;
                                    return Locality;
                                }
                                return country;
                            }
            if (continent != null) return continent;
            if (Locality != null) return Locality;
            return null;
        }

        private static Locality CreateAdm(Worksheet sheet, Collection<string> filefields, int i, CriteriaOperator cr, LocalityAdm parent, string fieldName)
        {
            Locality Admin = null;
            if ((filefields.IndexOf(fieldName) > -1) && (!sheet.Cells[i, filefields.IndexOf(fieldName)].Value.Equals(null)))
            {
                string AdminVal = sheet.Cells[i, filefields.IndexOf(fieldName)].Value.ToString();
                Admin = os.FindObject<Locality>(CriteriaOperator.And(new BinaryOperator("Name", AdminVal), cr));
                if ((Admin != null) && (fieldName == "Country") && (Admin.GetType() != typeof(Country))) throw new Exception("Cannot create Country as object already exists with the same name but different type");
                if ((Admin != null) && (fieldName == "Continent") && (Admin.GetType() != typeof(Continent))) throw new Exception("Cannot create Continent as object already exists with the same name but different type");
                if ((Admin != null) && (parent != null))
                {
                    if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                        if (((LocalityAdm)Admin).Parent.Oid != parent.Oid)
                            Admin = null;
                }
                if (Admin == null)
                {
                    if (fieldName == "Continent")
                        Admin = os.CreateObject<Continent>();
                    else
                        if (fieldName == "Country")
                            Admin = os.CreateObject<Country>();
                        else
                            Admin = os.CreateObject<LocalityAdm>();
                    Admin.Name = AdminVal;
                    if (parent != null)
                        if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                            ((LocalityAdm)Admin).Parent = parent;
                    Admin.Save();
                }
                if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                    parent = (LocalityAdm)Admin;
            }
            return Admin;
        }

        private static void ImportSimpleUnitFields(Worksheet sheet, Collection<string> filefields, int i, Unit unit)
        {
            if ((filefields.IndexOf("Comment") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString()))) unit.Comment = sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString();
            if ((filefields.IndexOf("UnitID") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("UnitID")].Value.ToString()))) unit.UnitID = sheet.Cells[i, filefields.IndexOf("UnitID")].Value.ToString();
            if ((filefields.IndexOf("Preservation") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Preservation")].Value.ToString()))) unit.Preservation = sheet.Cells[i, filefields.IndexOf("Preservation")].Value.ToString();
            if ((filefields.IndexOf("OtherId") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("OtherId")].Value.ToString()))) unit.OtherId = sheet.Cells[i, filefields.IndexOf("OtherId")].Value.ToString();
            if ((filefields.IndexOf("CoordSource") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordSource")].Value.ToString()))) unit.CoordSource = sheet.Cells[i, filefields.IndexOf("CoordSource")].Value.ToString();
            if ((filefields.IndexOf("CoordDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value.ToString()))) unit.CoordDetails = sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value.ToString();
            if ((filefields.IndexOf("Host") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Host")].Value.ToString()))) unit.Host = sheet.Cells[i, filefields.IndexOf("Host")].Value.ToString();
            if ((filefields.IndexOf("HostDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("HostDetails")].Value.ToString()))) unit.HostDetails = sheet.Cells[i, filefields.IndexOf("HostDetails")].Value.ToString();
            if ((filefields.IndexOf("LabelText") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("LabelText")].Value.ToString()))) unit.LabelText = sheet.Cells[i, filefields.IndexOf("LabelText")].Value.ToString();
            if ((filefields.IndexOf("InstitutionCode") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("InstitutionCode")].Value.ToString()))) unit.InstitutionCode = sheet.Cells[i, filefields.IndexOf("InstitutionCode")].Value.ToString();
            if ((filefields.IndexOf("CollectionCode") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CollectionCode")].Value.ToString()))) unit.CollectionCode = sheet.Cells[i, filefields.IndexOf("CollectionCode")].Value.ToString();
            if ((filefields.IndexOf("AssociatedTaxa") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("AssociatedTaxa")].Value.ToString()))) unit.AssociatedTaxa = sheet.Cells[i, filefields.IndexOf("AssociatedTaxa")].Value.ToString();
            if ((filefields.IndexOf("IdentifiedBy") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentifiedBy")].Value.ToString()))) unit.IdentifiedBy = sheet.Cells[i, filefields.IndexOf("IdentifiedBy")].Value.ToString();
            if ((filefields.IndexOf("IdentificationConfirmedBy") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedBy")].Value.ToString()))) unit.IdentificationConfirmedBy = sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedBy")].Value.ToString();
            if ((filefields.IndexOf("IdentificationComments") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentificationComments")].Value.ToString()))) unit.IdentificationComments = sheet.Cells[i, filefields.IndexOf("IdentificationComments")].Value.ToString();
            if ((filefields.IndexOf("CollectedOrObservedByText") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CollectedOrObservedByText")].Value.ToString()))) unit.CollectedOrObservedByText = sheet.Cells[i, filefields.IndexOf("CollectedOrObservedByText")].Value.ToString();
            if ((filefields.IndexOf("CollectionMethod") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CollectionMethod")].Value.ToString()))) unit.CollectionMethod = sheet.Cells[i, filefields.IndexOf("CollectionMethod")].Value.ToString();
            if ((filefields.IndexOf("WKT") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString()))) unit.WKT = sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString();
            if ((filefields.IndexOf("FieldID") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("FieldID")].Value.ToString()))) unit.FieldID = sheet.Cells[i, filefields.IndexOf("FieldID")].Value.ToString();
            if ((filefields.IndexOf("UnitType") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("UnitType")].Value.ToString()))) unit.UnitType = sheet.Cells[i, filefields.IndexOf("UnitType")].Value.ToString();
            if ((filefields.IndexOf("TemporaryName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("TemporaryName")].Value.ToString()))) unit.TemporaryName = sheet.Cells[i, filefields.IndexOf("TemporaryName")].Value.ToString();
            if ((filefields.IndexOf("WhereInStore") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("WhereInStore")].Value.ToString()))) unit.WhereInStore = sheet.Cells[i, filefields.IndexOf("WhereInStore")].Value.ToString();
            if ((filefields.IndexOf("PlaceDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("PlaceDetails")].Value.ToString()))) unit.PlaceDetails = sheet.Cells[i, filefields.IndexOf("PlaceDetails")].Value.ToString();
            if ((filefields.IndexOf("HabitatDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("HabitatDetails")].Value.ToString()))) unit.HabitatDetails = sheet.Cells[i, filefields.IndexOf("HabitatDetails")].Value.ToString();
            if ((filefields.IndexOf("AlternativeDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("AlternativeDate")].Value.ToString()))) unit.AlternativeDate = sheet.Cells[i, filefields.IndexOf("AlternativeDate")].Value.ToString();
            if ((filefields.IndexOf("BiologyDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("BiologyDetails")].Value.ToString()))) unit.BiologyDetails = sheet.Cells[i, filefields.IndexOf("BiologyDetails")].Value.ToString();
            if ((filefields.IndexOf("QuantityUnit") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("QuantityUnit")].Value.ToString()))) unit.QuantityUnit = sheet.Cells[i, filefields.IndexOf("QuantityUnit")].Value.ToString();
            if ((filefields.IndexOf("PreparedBy") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("PreparedBy")].Value.ToString()))) unit.PreparedBy = sheet.Cells[i, filefields.IndexOf("PreparedBy")].Value.ToString();
            if ((filefields.IndexOf("PreparationType") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("PreparationType")].Value.ToString()))) unit.PreparationType = sheet.Cells[i, filefields.IndexOf("PreparationType")].Value.ToString();

            if ((filefields.IndexOf("Area") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Area")].Value.ToString()))) unit.Area = sheet.Cells[i, filefields.IndexOf("Area")].Value.NumericValue;
            if ((filefields.IndexOf("CoordRadius") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.ToString()))) unit.CoordRadius = sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.NumericValue;
            if ((filefields.IndexOf("EPSG") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.ToString()))) unit.EPSG = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.NumericValue);
            if ((filefields.IndexOf("Centroid_X") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.ToString()))) unit.Centroid_X = sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.NumericValue;
            if ((filefields.IndexOf("Centroid_Y") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.ToString()))) unit.Centroid_Y = sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.NumericValue;
            if ((filefields.IndexOf("X") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("X")].Value.ToString()))) unit.X = sheet.Cells[i, filefields.IndexOf("X")].Value.NumericValue;
            if ((filefields.IndexOf("Y") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Y")].Value.ToString()))) unit.Y = sheet.Cells[i, filefields.IndexOf("Y")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude1") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.ToString()))) unit.Altitude1 = sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude2") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.ToString()))) unit.Altitude2 = sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.NumericValue;
            if ((filefields.IndexOf("Year") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Year")].Value.ToString()))) unit.Year = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Year")].Value.NumericValue);
            if ((filefields.IndexOf("IdentifiedOnYear") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentifiedOnYear")].Value.ToString()))) unit.IdentifiedOnYear = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("IdentifiedOnYear")].Value.NumericValue);
            if ((filefields.IndexOf("IdentificationConfirmedYear") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedYear")].Value.ToString()))) unit.IdentificationConfirmedYear = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedYear")].Value.NumericValue);
            if ((filefields.IndexOf("Month") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Month")].Value.ToString()))) unit.Month = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Month")].Value.NumericValue);
            if ((filefields.IndexOf("Day") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Day")].Value.ToString()))) unit.Day = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Day")].Value.NumericValue);
            if ((filefields.IndexOf("Quantity") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Quantity")].Value.ToString()))) unit.Quantity = sheet.Cells[i, filefields.IndexOf("Quantity")].Value.NumericValue;
            if ((filefields.IndexOf("DoesNotExist") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("DoesNotExist")].Value.ToString()))) unit.DoesNotExist = sheet.Cells[i, filefields.IndexOf("DoesNotExist")].Value.BooleanValue;
            if ((filefields.IndexOf("IsDead") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IsDead")].Value.ToString()))) unit.IsDead = sheet.Cells[i, filefields.IndexOf("IsDead")].Value.BooleanValue;
            if ((filefields.IndexOf("Cf") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Cf")].Value.ToString()))) unit.Cf = sheet.Cells[i, filefields.IndexOf("Cf")].Value.BooleanValue;
            if ((filefields.IndexOf("BirthDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("BirthDate")].Value.ToString()))) unit.BirthDate = sheet.Cells[i, filefields.IndexOf("BirthDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("DeathDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("DeathDate")].Value.ToString()))) unit.DeathDate = sheet.Cells[i, filefields.IndexOf("DeathDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("IdentifiedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentifiedOn")].Value.ToString()))) unit.IdentifiedOn = sheet.Cells[i, filefields.IndexOf("IdentifiedOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("CheckDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CheckDate")].Value.ToString()))) unit.CheckDate = sheet.Cells[i, filefields.IndexOf("CheckDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("Date") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Date")].Value.ToString()))) unit.Date = sheet.Cells[i, filefields.IndexOf("Date")].Value.DateTimeValue;
            if ((filefields.IndexOf("EndOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("EndOn")].Value.ToString()))) unit.EndOn = sheet.Cells[i, filefields.IndexOf("EndOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("PreparationDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("PreparationDate")].Value.ToString()))) unit.PreparationDate = sheet.Cells[i, filefields.IndexOf("PreparationDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("CreatedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.ToString()))) unit.CreatedOn = sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("LastModifiedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.ToString()))) unit.LastModifiedOn = sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.DateTimeValue;
        }

        private static void ImportSimpleLocalityFields(Worksheet sheet, Collection<string> filefields, int i, Locality Locality)
        {
            if ((filefields.IndexOf("Name") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Name")].Value.ToString()))) Locality.Name = sheet.Cells[i, filefields.IndexOf("Name")].Value.ToString();
            if ((filefields.IndexOf("Code") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString()))) Locality.Code = sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString();
            if ((filefields.IndexOf("Comment") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString()))) Locality.Comment = sheet.Cells[i, filefields.IndexOf("Comment")].Value.ToString();
            if ((filefields.IndexOf("CoordSource") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordSource")].Value.ToString()))) Locality.CoordSource = sheet.Cells[i, filefields.IndexOf("CoordSource")].Value.ToString();
            if ((filefields.IndexOf("CoordDetails") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value.ToString()))) Locality.CoordDetails = sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value.ToString();
            if ((filefields.IndexOf("WKT") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString()))) Locality.WKT = sheet.Cells[i, filefields.IndexOf("WKT")].Value.ToString();
            if ((filefields.IndexOf("Area") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Area")].Value.ToString()))) Locality.Area = sheet.Cells[i, filefields.IndexOf("Area")].Value.NumericValue;
            if ((filefields.IndexOf("CoordRadius") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.ToString()))) Locality.CoordRadius = sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.NumericValue;
            if ((filefields.IndexOf("EPSG") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.ToString()))) Locality.EPSG = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.NumericValue);
            if ((filefields.IndexOf("Centroid_X") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.ToString()))) Locality.Centroid_X = sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.NumericValue;
            if ((filefields.IndexOf("Centroid_Y") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.ToString()))) Locality.Centroid_Y = sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude1") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.ToString()))) Locality.Altitude1 = sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude2") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.ToString()))) Locality.Altitude2 = sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.NumericValue;
            if ((filefields.IndexOf("Label") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Label")].Value.ToString()))) Locality.Label = sheet.Cells[i, filefields.IndexOf("Label")].Value.ToString();
            if ((filefields.IndexOf("ReasonNotValid") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ReasonNotValid")].Value.ToString()))) Locality.ReasonNotValid = sheet.Cells[i, filefields.IndexOf("ReasonNotValid")].Value.ToString();
            if ((filefields.IndexOf("NotValid") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("NotValid")].Value.ToString()))) Locality.NotValid = sheet.Cells[i, filefields.IndexOf("NotValid")].Value.BooleanValue;
            if ((filefields.IndexOf("CreatedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.ToString()))) Locality.CreatedOn = sheet.Cells[i, filefields.IndexOf("CreatedOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("LastModifiedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.ToString()))) Locality.LastModifiedOn = sheet.Cells[i, filefields.IndexOf("LastModifiedOn")].Value.DateTimeValue;
        }

        private static void ImportComplexUnitFields(Worksheet sheet, Collection<string> filefields, int i, Unit unit, Project project)
        {
            TaxonomicName taxonomicName = null;
            if ((filefields.IndexOf("Genus") > -1) && (filefields.IndexOf("Species") > -1) && (sheet.Cells[i, filefields.IndexOf("Genus")].Value != null) && (sheet.Cells[i, filefields.IndexOf("Species")].Value != null))
                taxonomicName = TaxonomyHelper.GetTaxon(project, os, sheet.Cells[i, filefields.IndexOf("Genus")].Value.ToString(), sheet.Cells[i, filefields.IndexOf("Species")].Value.ToString(), "");
            if ((taxonomicName == null) && (filefields.IndexOf("ShortName") > -1) && (sheet.Cells[i, filefields.IndexOf("ShortName")].Value != null))
            {
                taxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString()));
                if (taxonomicName == null)
                    taxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString()), true);
                if (taxonomicName == null)
                {
                    taxonomicName = os.CreateObject<Species>();
                    taxonomicName.ShortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                    taxonomicName.Save();
                }
            }

            if (((unit.TaxonomicName != null) && (unit.TaxonomicName.Oid != taxonomicName.Oid)) || (unit.TaxonomicName == null))
                unit.TaxonomicName = taxonomicName;

            Locality loc = null;
            if ((filefields.IndexOf("Locality") > -1) && (sheet.Cells[i, filefields.IndexOf("Locality")].Value != null))
                loc = os.FindObject<Locality>(new BinaryOperator("Name", (sheet.Cells[i, filefields.IndexOf("Locality")].Value.ToString())));
            if (loc == null)
                if ((filefields.IndexOf("LocalityCode") > -1) && (sheet.Cells[i, filefields.IndexOf("LocalityCode")].Value != null))
                    loc = os.FindObject<Locality>(new BinaryOperator("Code", (sheet.Cells[i, filefields.IndexOf("LocalityCode")].Value.ToString())));
            if (loc != null)
                unit.Locality = loc;

            Unit parent = null;
            if ((filefields.IndexOf("DerivedFrom") > -1) && (sheet.Cells[i, filefields.IndexOf("DerivedFrom")].Value != null))
                parent = os.FindObject<Unit>(new BinaryOperator("UnitID", (sheet.Cells[i, filefields.IndexOf("DerivedFrom")].Value.ToString())));
            if (parent != null)
                unit.DerivedFrom = parent;

            Unit assembly = null;
            if ((filefields.IndexOf("UnitAssembly") > -1) && (sheet.Cells[i, filefields.IndexOf("UnitAssembly")].Value != null))
                assembly = os.FindObject<Unit>(new BinaryOperator("UnitID", (sheet.Cells[i, filefields.IndexOf("UnitAssembly")].Value.ToString())));
            if (parent != null)
                unit.UnitAssembly = assembly;
        }

        private void LoadDocument(out Worksheet sheet, out Range range, out Collection<string> filefields, string fn)
        {
            //string fn = String.Format("{0}\\{1}", options.File.Folder, options.File.FileName);
            SpreadsheetControl doc = new SpreadsheetControl();
            IWorkbook workbook = doc.Document;
            try
            {
                using (FileStream stream = new FileStream(fn, FileMode.Open))
                {
                    if (Path.GetExtension(fn) == ".xlsx")
                        workbook.LoadDocument(stream, DocumentFormat.OpenXml);
                    if (Path.GetExtension(fn) == ".xls")
                        workbook.LoadDocument(stream, DocumentFormat.Xls);
                    if (Path.GetExtension(fn) == ".csv")
                        workbook.LoadDocument(stream, DocumentFormat.Csv);
                    if (Path.GetExtension(fn) == ".txt")
                        workbook.LoadDocument(stream, DocumentFormat.Text);
                }
            }
            finally
            {
                /*SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
                if (contr != null)
                {
                    EnableActions(contr);
                }*/
            }
            sheet = workbook.Worksheets[0];
            range = sheet.GetUsedRange();
            RowCollection rows = sheet.Rows;
            filefields = new Collection<string>();
            for (int i = 0; i <= range.ColumnCount - 1; i++)
            {
                Cell cell = sheet.Cells[0, i];
                filefields.Add(cell.Value.ToString());
            }
        }

        private void popupWindowShowAction_ImportLocalities_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace obs = Application.CreateObjectSpace();
            DetailView dv = Application.CreateDetailView(obs, obs.CreateObject<ImportOptionsProjectDataLocalities>());
            e.View = dv;
        }

        private void popupWindowShowAction_ImportLocalities_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            RunImport(e);
        }

        private void popupWindowShowAction_ImportTaxa_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            RunImport(e);

        }

        private void popupWindowShowAction_ImportTaxa_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace obs = Application.CreateObjectSpace();
            DetailView dv = Application.CreateDetailView(obs, obs.CreateObject<ImportOptionsProjectDataTaxa>());
            e.View = dv;
        }


        private void ImportSimplePropertiesGeneric(Object obj, Worksheet sheet, Range range, Collection<string> filefields, int i)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if (cell.Value != null)
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            if (member.ReferenceType == null)
                            {
                                try
                                {
                                    if (typeof(String).IsAssignableFrom(memberType))
                                        if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                            if ((member.MappingFieldSize > -1) && (cell.Value.ToString().Length > member.MappingFieldSize))
                                                throw new Exception("Maximum size: " + member.MappingFieldSize);
                                            else
                                                member.SetValue(obj, cell.Value.ToString());
                                    if (typeof(Boolean).IsAssignableFrom(memberType))
                                        member.SetValue(obj, cell.Value.BooleanValue);
                                    if (typeof(DateTime).IsAssignableFrom(memberType))
                                        member.SetValue(obj, cell.Value.DateTimeValue);
                                    if ((typeof(Double).IsAssignableFrom(memberType)) || (typeof(int).IsAssignableFrom(member.MemberType)))
                                        member.SetValue(obj, cell.Value.NumericValue);
                                    if (memberType.IsEnum)
                                    {
                                        if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            object enumValue = Enum.Parse(memberType, cell.Value.ToString());
                                            if (Enum.IsDefined(memberType, enumValue))
                                                member.SetValue(obj, enumValue);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                                    sheet.Comments.Add(cell, SecuritySystem.CurrentUserName, ex.Message);
                                    cell.Fill.BackgroundColor = Color.Red;
                                }
                                finally
                                { }
                            }

                        }
                    }
            }
        }
        private void ImportReferencePropertiesGeneric(Object obj, Worksheet sheet, Range range, Collection<string> filefields, int i)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if (cell.Value != null)
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            try
                            {
                                if (typeof(Unit).IsAssignableFrom(member.ReferenceType.ClassType))
                                {
                                    ImportComplexMemberUnit(cell, member, obj);
                                }
                                if (typeof(Locality).IsAssignableFrom(member.ReferenceType.ClassType))
                                {
                                    ImportComplexMemberLocality(cell, member, obj);
                                }
                                if (typeof(TaxonomicName).IsAssignableFrom(member.ReferenceType.ClassType))
                                {
                                    ImportComplexMemberTaxonomicName(cell, member, obj);
                                }
                                if (typeof(User).IsAssignableFrom(member.ReferenceType.ClassType))
                                {
                                    ImportComplexMemberUser(cell, member, obj);
                                }
                            }
                            catch (Exception ex)
                            {
                                cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                                sheet.Comments.Add(cell, SecuritySystem.CurrentUserName, ex.Message);
                                cell.Fill.BackgroundColor = Color.Red;
                            }
                            finally
                            { }
                        }
                    }
            }
        }

        private void ImportComplexMemberUser(Cell cell, XPMemberInfo member, object obj)
        {
            throw new NotImplementedException();
        }

        private void ImportComplexMemberTaxonomicName(Cell cell, XPMemberInfo member, object obj)
        {
            throw new NotImplementedException();
        }

        private void ImportComplexMemberLocality(Cell cell, XPMemberInfo member, object obj)
        {
            throw new NotImplementedException();
        }

        private void ImportComplexMemberUnit(Cell cell, XPMemberInfo member, object obj)
        {
            throw new NotImplementedException();
        }

        private void simpleAction_ImportData_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            CollectionSource collectionSource =
                new CollectionSource(objectSpace, typeof(ImportDataObject));
            if ((collectionSource.Collection as XPBaseCollection) != null)
            {
                ((XPBaseCollection)collectionSource.Collection).LoadingEnabled = false;
            }
            DevExpress.ExpressApp.ListView view = Application.CreateListView(Application.GetListViewId(typeof(ImportDataObject)), collectionSource, false);
            view.Editor.AllowEdit = true;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            e.ShowViewParameters.CreatedView = view;
            e.ShowViewParameters.Context = TemplateContext.PopupWindow;
            DialogController dc = Application.CreateController<DialogController>();
            e.ShowViewParameters.Controllers.Add(dc);
   
        }
    }
}
