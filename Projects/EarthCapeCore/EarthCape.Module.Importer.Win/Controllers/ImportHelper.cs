﻿using DevExpress.Spreadsheet;
using DevExpress.XtraSpreadsheet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Importer.Win.Controllers
{
    class ImportHelper
    {
        public static void LoadDocument(out Worksheet sheet, out CellRange range, out Collection<string> filefields, string fn)
        {
            Workbook workbook = new Workbook();
            try
            {
                using (FileStream stream = new FileStream(fn, FileMode.Open))
                {
                    if (Path.GetExtension(fn) == ".xlsx")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.OpenXml);
                    if (Path.GetExtension(fn) == ".xls")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Xls);
                    if (Path.GetExtension(fn) == ".csv")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Csv);
                    if (Path.GetExtension(fn) == ".txt")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Text);
                }
            }
            finally
            {
                /*SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
                if (contr != null)
                {
                    EnableActions(contr);
                }*/
            }
            sheet = workbook.Worksheets[0];
            range = sheet.GetUsedRange();
            RowCollection rows = sheet.Rows;
            filefields = new Collection<string>();
            for (int i = 0; i <= range.ColumnCount - 1; i++)
            {
                Cell cell = sheet.Cells[0, i];
                filefields.Add(cell.Value.ToString());
            }
            /*filefields.Add("LastModifiedByUser");
            filefields.Add("LastModifiedOn");
            filefields.Add("CreatedByUser");
            filefields.Add("CreatedOn");*/
        }

    }
}
