using System;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;

using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Actions;
using DevExpress.Spreadsheet;
using DevExpress.XtraSplashScreen;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.AuditTrail;

using EarthCape.Module.Core;


namespace EarthCape.Module.Importer.Win.Controllers
{
    public class ImportDataObjectController : ViewController<DevExpress.ExpressApp.ListView>
    {
        // static Locker locker1;
        // static BackgroundWorker backgroundWorker1;
        static IObjectSpace os;
        CollectionSourceBase collection = null;

        public ImportDataObjectController()
        {
            TargetObjectType = typeof(ImportDataObject);
            SimpleAction actionNew = new SimpleAction(this, "Add files", DevExpress.Persistent.Base.PredefinedCategory.PopupActions);
            actionNew.Execute += new SimpleActionExecuteEventHandler(simpleAction_AddFiles);
            SimpleAction actionNew1 = new SimpleAction(this, "Import", DevExpress.Persistent.Base.PredefinedCategory.PopupActions);
            actionNew1.Execute += new SimpleActionExecuteEventHandler(simpleAction_ImportExecute);
            this.Activated += ImportDataObjectController_Activated;
        }

        void ImportDataObjectController_Activated(object sender, EventArgs e)
        {
            if (Frame == null) return;
            ListViewProcessCurrentObjectController controller = Frame.GetController<ListViewProcessCurrentObjectController>();
            if (controller != null)
            {
                controller.ProcessCurrentObjectAction.Enabled[""] = false;
            }

        }
        void simpleAction_ImportExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            os = Application.CreateObjectSpace();
            // options = (ImportOptions)e.PopupWindow.View.CurrentObject;
            foreach (ImportDataObject dataObject in (collection.Collection as XPBaseCollection))
            {
                if (dataObject.ObjectType == null)
                    throw new UserFriendlyException("Object Type missing!");          
            }

#if RELEASE
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            locker1 = new Locker();
            backgroundWorker1.RunWorkerAsync(locker1);
            os.CommitChanges();

#else
                Worksheet sheet = null;
            CellRange range = null;
            Collection<string> filefields;
            int current = 0;
            if ((collection.Collection as XPBaseCollection) != null)
                foreach (ImportDataObject dataObject in (collection.Collection as XPBaseCollection))
                {
                    current++;
                    ImportHelper.LoadDocument(out sheet, out range, out filefields, dataObject.File);

                    DoImport(null, sheet, range, filefields, dataObject);
                    os.CommitChanges();
                }
#endif

        }
        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SplashScreenManager.CloseForm();
            // locker1.IsCanceled = false;
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message); return;
            }
            else
            {
                os.CommitChanges();
                /* SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
                 if (contr != null)
                     contr.popupWindowShowAction_ImportUnits.Active.SetItemValue("Import", true);*/
            }
        }
        void Instance_SaveAuditTrailData(object sender, SaveAuditTrailDataEventArgs e)
        {
            e.Handled = true;
        }
        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            Worksheet sheet = null;
            CellRange range = null;
            Collection<string> filefields;
            if (worker != null)
            {
                try
                {
                    AuditTrailService.Instance.SaveAuditTrailData += Instance_SaveAuditTrailData;
                    SplashScreenManager.ShowForm(typeof(SplashScreen_process));
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetTotalFileCount, collection.List.Count);
                    int current = 0;
                    if ((collection.Collection as XPBaseCollection) != null)
                        foreach (ImportDataObject dataObject in (collection.Collection as XPBaseCollection))
                        {
                            if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                            try
                            {
                                current++;
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCurrentFileCount, current);
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetFileLabel, Path.GetFileName(dataObject.File));
                                ImportHelper.LoadDocument(out sheet, out range, out filefields, dataObject.File);
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCount, range.RowCount - 1);
                                // SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SendObject, locker1);

                                DoImport(e, sheet, range, filefields, dataObject);
                            }
                            finally
                            {
                                if (sheet != null)
                                {
                                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Saving...");
                                    os.CommitChanges();
                                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Opening output file...");
                                    string output = String.Format("{0}\\{1}_{2}{3}", Path.GetTempPath(), Path.GetFileNameWithoutExtension(dataObject.File), DateTime.Now.ToString("yyyyMMddHHmmssffff"), Path.GetExtension(dataObject.File));
                                    sheet.Workbook.SaveDocument(output, DevExpress.Spreadsheet.DocumentFormat.OpenXml);
                                    Process.Start(output);
                                }
                            }
                        }
                }
                finally
                {
                    AuditTrailService.Instance.SaveAuditTrailData -= Instance_SaveAuditTrailData;
                }
            }
        }

        private void DoImport(DoWorkEventArgs e, Worksheet sheet, CellRange range, Collection<string> filefields, ImportDataObject dataObject)
        {
            int col = range.ColumnCount;
            bool hasExtraFields = false;
            if (filefields.IndexOf("Oid") == -1)
            {
                filefields.Add("Oid");
                sheet.Cells[0, col].SetValue("Oid");
                col = col + 1;
            }
            if (filefields.IndexOf("ImportedName") == -1)
            {
                filefields.Add("ImportedName");
                sheet.Cells[0, col].SetValue("ImportedName");
            }
            if (typeof(TaxonomicName).IsAssignableFrom(dataObject.GetObjectType()))
            {
                hasExtraFields = ImportTaxonomicFields(e, sheet, range, filefields, dataObject);
                hasExtraFields = false;
            }
            // if (typeof(Locality).IsAssignableFrom(dataObject.GetObjectType()))
            if (dataObject.GetObjectType() == typeof(Locality))
            {
                hasExtraFields = ImportLocalityFields(e, sheet, range, filefields, dataObject);
                hasExtraFields = true;
            }
            if (typeof(Unit).IsAssignableFrom(dataObject.GetObjectType()))
            {
                hasExtraFields = true;
            }
            object keyValue = "";
            String keyField = "";
            if (e != null)
                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Importing record data...");
            //os.CommitChanges();
            //if (hasExtraFields)
          
            for (int i = 1; i <= range.RowCount - 1; i++)
            {
                //break;
                if (e != null)
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "UnitID", "UserName", "Name", "Oid", "Code" });
                XPCustomObject obj = ImportRecord(keyValue, keyField, null, null, dataObject);
                if (obj != null)
                {
                    ImportSimplePropertiesGeneric(obj, sheet, range, filefields, i);
                    ImportReferencePropertiesGeneric(obj, sheet, range, filefields, i, dataObject);
                    ImportPropertiesSpecialized(obj, sheet, range, filefields, i);
                    obj.Save();
                    os.CommitChanges();
                }
                if (e != null)
                    if (((ILocked)e.Argument).IsCanceled)
                    {
                        break;
                    }
            }
        }


        private static void FindKey(Worksheet sheet, Collection<string> filefields, ref object keyValue, ref String keyField, int i, string[] keys)
        {
            foreach (string key in keys)
                if (filefields.IndexOf(key) > -1)
                    if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(key)].Value.ToString()))
                    {
                        keyValue = sheet.Cells[i, filefields.IndexOf(key)].Value.ToObject().ToString();
                        keyField = key;
                        return;
                    }

            if (filefields.IndexOf("Oid") > -1)
                if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Oid")].Value.ToString()))
                {
                    keyValue = sheet.Cells[i, filefields.IndexOf("Oid")].Value.ToObject();
                    keyField = "Oid";
                    return;
                }

        }

        private void ImportSimplePropertiesGeneric(Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if (cell.Value != null)
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            if (member.ReferenceType == null)
                            {
                                try
                                {
                                    if (!cell.Value.IsEmpty)
                                    {
                                        if (typeof(String).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                if ((member.MappingFieldSize > -1) && (cell.Value.ToString().Length > member.MappingFieldSize))
                                                    throw new Exception("Maximum size: " + member.MappingFieldSize);
                                                else
                                                    member.SetValue(obj, cell.Value.ToObject().ToString());
                                        if (typeof(Boolean).IsAssignableFrom(memberType))
                                            member.SetValue(obj, cell.Value.BooleanValue);
                                        if (typeof(DateTime).IsAssignableFrom(memberType))
                                        {
                                            if (cell.Value.DateTimeValue > DateTime.MinValue)
                                            member.SetValue(obj, cell.Value.DateTimeValue);
                                        }
                                        if (typeof(Double).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, cell.Value.NumericValue);
                                        if ((typeof(int).IsAssignableFrom(memberType)) || 
                                            (typeof(System.Int16).IsAssignableFrom(memberType)) ||
                                            (typeof(System.Int32).IsAssignableFrom(memberType)))
                                           if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, Convert.ToInt32(cell.Value.NumericValue));
                                          if  (typeof(System.Int64).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, Convert.ToInt64(cell.Value.NumericValue));
                                       if (memberType.IsEnum)
                                        {
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                object enumValue = Enum.Parse(memberType, cell.Value.ToString());
                                                if (Enum.IsDefined(memberType, enumValue))
                                                    member.SetValue(obj, enumValue);
                                            }
                                        }
                                    }
                                    //else 
                                    //  member.SetValue(obj, null);                                 
                                }
                                catch (Exception ex)
                                {
                                    cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                                    sheet.Comments.Add(cell, ex.Message);
                                    cell.Fill.BackgroundColor = Color.Red;
                                }
                                finally
                                { }
                            }

                        }
                    }
            }
        }
        private void ImportPropertiesSpecialized(Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i)
        {
            //  MessageBox.Show("1");
            Cell cell = null;
            try
            {
                if (obj is Attachment)
                {
                    cell = ImportAttachment((Attachment)obj, sheet, filefields, i, cell);
                }
            }
            catch (Exception ex)
            {
                cell = sheet.Cells[i, 0];
                sheet.Comments.Add(cell, ex.Message);
                cell.Fill.BackgroundColor = Color.Red;
            }
            finally
            { }   /*       if (obj is FileItemLinked)
            {
                cell = ImportFileItemLinked((FileItemLinked)obj, sheet, filefields, i, cell);
            }
        /*    if (obj is FileItemAzureBlob)
            {
                cell = ImportFileAzureBlob((FileItemAzureBlob)obj, sheet, filefields, i, cell);
            }*/
        }

        private static Cell ImportAttachment(Attachment file, Worksheet sheet, Collection<string> filefields, int i, Cell cell)
        {
            // MessageBox.Show("2");
            string filePath = "";
            string filename = "";
            string unitID = "";
            string localityName = "";
            //AttachmentStorage store = null;
            cell = sheet.Cells[i, filefields.IndexOf("FilePath")];
            if (cell.Value != null)
                filePath = cell.Value.TextValue;
            cell = sheet.Cells[i, filefields.IndexOf("FileName")];
            if (cell.Value != null)
                filename = cell.Value.TextValue;
            cell = sheet.Cells[i, filefields.IndexOf("Unit")];
            if (cell.Value != null)
                unitID = cell.Value.TextValue;
            cell = sheet.Cells[i, filefields.IndexOf("Storage")];
        /*    if (cell.Value != null)
            {
                store = os.FindObject<AttachmentStorage>(new BinaryOperator("Name", cell.Value.TextValue));
            }
            if (store != null)
                using (FileStream fs = new FileStream(Path.Combine(filePath, filename), FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    if (GeneralHelper.IsRecognisedImageFile(filename))
                        file.Thumbnail = AttachmentHelper.CreateThumbnail(fs, 100, 100);// ResizeImage(Image.FromFile(filename), 100);
                    file.OriginalFileName = filename;
                    file.OriginalFilePath = filePath;
                    file.Storage = store;
                    file.Save();
                    os.CommitChanges();
                    string Key = string.Format("{0:N}{1}", file.Oid, Path.GetExtension(file.OriginalFileName)).ToUpper();
                    file.Key = Key;
                    file.Save();
                    os.CommitChanges();
                    store.Upload(file, fs, Key);
                }*/
            file.Save();


            Unit unit = null;
            if (!String.IsNullOrEmpty(unitID))
                unit = os.FindObject<Unit>(new BinaryOperator("UnitID", unitID));
            Locality loc = null;
            if (!String.IsNullOrEmpty(localityName))
                loc = os.FindObject<Locality>(new BinaryOperator("Name", localityName));
            // MessageBox.Show(unitID + "/" + localityName);

            /*  if (loc != null)
              {
               //   MessageBox.Show("3");
                  LocalityFile f = os.CreateObject<LocalityFile>();
                  f.Locality = loc;
                  f.File = obj;
                  f.Save();
              }*/
            if (unit != null)
            {
                //  MessageBox.Show("4");
                UnitAttachment f = os.CreateObject<UnitAttachment>();
                f.Unit = unit;
               // f.File = file;
                f.Save();
            }
            return cell;
        }


        /*       private static Cell ImportFileAzureBlob(FileItemAzureBlob obj, Worksheet sheet, Collection<string> filefields, int i, Cell cell)
               {
                   // MessageBox.Show("2");
                   string filePath = "";
                   string fileName = "";
                   string unitID = "";
                   string localityName = "";
                   cell = sheet.Cells[i, filefields.IndexOf("FilePath")];
                   if (cell.Value != null)
                       filePath = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("FileName")];
                   if (cell.Value != null)
                       fileName = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("LinkedUnitID")];
                   if (cell.Value != null)
                       unitID = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("LinkedLocalityName")];
                   if (cell.Value != null)
                       localityName = cell.Value.TextValue;
                   string path = Path.Combine(filePath, fileName);
                   if (File.Exists(path))
                   {

                       using (var fileStream = File.OpenRead(path))
                       {
                           var data = os.CreateObject<FileDataAzureBlob>();

                           data.LoadFromStream(Path.GetFileName(path), fileStream);
                           data.FullName = path;
                           obj.File = data;
                       }
                       obj.Save();
                       Unit unit = null;
                       if (!String.IsNullOrEmpty(unitID))
                           unit = os.FindObject<Unit>(new BinaryOperator("UnitID", unitID));
                       Locality loc = null;
                       if (!String.IsNullOrEmpty(localityName))
                           loc = os.FindObject<Locality>(new BinaryOperator("Name", localityName));
                       // MessageBox.Show(unitID + "/" + localityName);

                       if (loc != null)
                       {
                           //   MessageBox.Show("3");
                           LocalityFile f = os.CreateObject<LocalityFile>();
                           f.Locality = loc;
                           f.File = obj;
                           f.Save();
                       }
                       if (unit != null)
                       {
                           //  MessageBox.Show("4");
                           UnitFile f = os.CreateObject<UnitFile>();
                           f.Unit = unit;
                           f.File = obj;
                           f.Save();
                       }
                   }

                   return cell;
               }
               */


        private void ImportCustomData(Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i, ImportDataObject options)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);

            foreach (string column in filefields)
            {
                if ((column != "Order1") && (column != "Genus1") && (column != "Species1"))
                {
                    bool stop = false;
                    foreach (XPMemberInfo member in classInfo.Members)
                    {
                        if (member.Name == column)
                        {
                            stop = true;
                            break;
                        }
                    }
                    if (!stop)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(column)];
                        if ((cell.Value != null) && (!string.IsNullOrEmpty(cell.Value.ToString())))
                            try
                            {
                                //CreateStringCustomField(obj, column, cell.Value, options);
                            }
                            catch (Exception ex)
                            {
                                cell = sheet.Cells[i, filefields.IndexOf(column)];
                                sheet.Comments.Add(cell, ex.Message);
                                cell.Fill.BackgroundColor = Color.Red;
                            }
                            finally
                            { }

                    }
                }
            }
        }



  /*      private void CreateStringCustomField(object obj, string column, CellValue cellValue, ImportDataObject options)
        {
            CustomField field = GetCustomField(column, options);
            CustomValue value = null;
            if (obj is Unit)
                value = os.CreateObject<CustomValueUnit>();
            if (obj is Locality)
                value = os.CreateObject<CustomValueLocality>();
            if (cellValue.Type == CellValueType.Text)
            {
                value.StringValue = cellValue.TextValue;
            }
            if (cellValue.Type == CellValueType.Numeric)
            {
                value.NumberValue = cellValue.NumericValue;
            }
            if (cellValue.Type == CellValueType.Boolean)
            {
                value.BoolValue = cellValue.BooleanValue;
            }
            if (cellValue.Type == CellValueType.DateTime)
            {
                value.DateTimeValue = cellValue.DateTimeValue;
            }
            if ((cellValue.Type == CellValueType.None) || (cellValue.Type == CellValueType.Unknown))
            {
                value.StringValue = cellValue.ToString(); ;
            }
            value.CustomField = field;
            if (obj is Unit)
                ((CustomValueUnit)value).Unit = ((Unit)obj);
            if (obj is Locality)
                ((CustomValueLocality)value).Locality = ((Locality)obj);

            if (options.DatasetImport != null)
                value.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", options.DatasetImport.Oid));
            else
            {
                if (obj is IDatasetObject)
                {
                    if (((IDatasetObject)obj).Dataset != null)
                        value.Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", ((IDatasetObject)obj).Dataset.Oid));
                }
            }
            value.Save();

            return;
        }
*/
  /*      private CustomField GetCustomField(string column, ImportDataObject options)
        {
            CriteriaOperator cr = null;
            cr = CriteriaOperator.And(new BinaryOperator("Name", column), GetGeneralImportCriteria(options));
            CustomField field = (CustomField)os.FindObject(typeof(CustomField), cr);
            if (field == null)
            {
                field = os.CreateObject<CustomField>();
                field.Name = column;
                if (options.ProjectImport != null)
                    field.Project = os.FindObject<Project>(new BinaryOperator("Oid", options.ProjectImport.Oid));
                field.Save();
                os.CommitChanges();
            }
            return field;
        }

*/
        private void ImportReferencePropertiesGeneric(Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i, ImportDataObject options)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if ((cell.Value != null) && (cell.Value.ToObject() != null) && (!string.IsNullOrEmpty(cell.Value.ToObject().ToString())))
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            if (member.ReferenceType != null)
                            {
                                try
                                {
                                    XPCustomObject imported = ImportRecord(cell.Value.ToObject().ToString(), "", member, obj, options);
                                    member.SetValue(obj, imported);
                                }
                                catch (Exception ex)
                                {
                                    sheet.Comments.Add(cell, ex.Message);
                                    cell.Fill.BackgroundColor = Color.Red;
                                }
                                finally
                                { }
                            }
                        }
                    }
            }
        }

    


        private XPCustomObject ImportRecord(object keyValue, string keyField, XPMemberInfo referencingMember, object ReferencingObject, ImportDataObject options)
        {
            XPCustomObject newObject = null;
            Type type;
            //   string keyField="";
            if (referencingMember != null)
                type = referencingMember.MemberType;
            else
                type = options.GetObjectType();
            CriteriaOperator cr = null;
            if (!String.IsNullOrEmpty((string)keyValue))
            {
                if (keyField == "Oid")
                    cr = new BinaryOperator("Oid", keyValue);
                else
                    if (!String.IsNullOrEmpty(keyField))
                        cr = CriteriaOperator.And(new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                    else
                        cr = CriteriaOperator.And(GetKeyCriteria(type, keyValue, out keyField), GetGeneralImportCriteria(options));
                newObject = (XPCustomObject)os.FindObject(type, cr);
            }
            if ((ReferencingObject != null) && (referencingMember != null) && (newObject != null))
                referencingMember.SetValue(ReferencingObject, newObject);
            else
                if (((options.CreateMissingReferences) || (ReferencingObject == null)) && (newObject == null))
                {
                    if ((typeof(TaxonomicName).IsAssignableFrom(type)) && (!typeof(HigherTaxon).IsAssignableFrom(type)) && (!typeof(SubspecificTaxon).IsAssignableFrom(type)))
                        newObject = os.CreateObject<Species>();
                    else
                        if ((typeof(TaxonomicName).IsAssignableFrom(type)))
                            newObject = (XPCustomObject)os.CreateObject(SecuritySystem.UserType);
                        else
                            newObject = (XPCustomObject)os.CreateObject(type);
                    if (newObject is IDatasetObject)
                        if (options.DatasetImport != null)
                            ((IDatasetObject)newObject).Dataset = os.FindObject<Dataset>(new BinaryOperator("Oid", options.DatasetImport.Oid));
                    if (newObject is IProjectObject)
                        if (options.DatasetImport != null)
                            ((IProjectObject)newObject).Project = os.FindObject<Project>(new BinaryOperator("Oid", options.ProjectImport.Oid));
                    if ((!string.IsNullOrEmpty(keyField)) && (keyValue != null))
                    {
                        if (typeof(PersonCore).IsAssignableFrom(type))
                        {
                            if (((string)keyValue).Contains(" "))
                            {
                                string fname = ((string)keyValue).Split(new string[] { " " }, 2, StringSplitOptions.None)[0].Trim();
                                string lname = ((string)keyValue).Split(new string[] { " " }, 2, StringSplitOptions.None)[1].Trim();
                                ((PersonCore)newObject).FirstName = fname;
                                ((PersonCore)newObject).LastName = lname;
                            }
                            else
                                ((PersonCore)newObject).FirstName = ((string)keyValue);
                        }
                        else
                            newObject.SetMemberValue(keyField, keyValue);
                    }
                    newObject.Save();
 //                   os.CommitChanges();
                }
            return newObject;
        }

        private static CriteriaOperator GetKeyCriteria(Type type, object keyValue, out string keyField)
        {
            CriteriaOperator cr = null;
            keyField = "";

            if (typeof(TaxonomicName).IsAssignableFrom(type))
            {
                cr = CriteriaOperator.Or(new BinaryOperator("ShortName", keyValue), new BinaryOperator("FullName", keyValue));
                keyField = "Name";
                return cr;
            }
                /*if (typeof(PersonCore).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("FullName", keyValue);
                keyField = "FullName";
                return cr;
            }*/

            if (typeof(Unit).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("UnitID", keyValue);
                keyField = "UnitID";
                return cr;
            }
            if ((typeof(ICode).IsAssignableFrom(type)) && (typeof(IName).IsAssignableFrom(type)))
            {
                cr = CriteriaOperator.Or(new BinaryOperator("Name", keyValue), new BinaryOperator("Code", keyValue));
                keyField = "Name";
                return cr;
            }
            if (typeof(ICode).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("Code", keyValue);
                keyField = "Code";
                return cr;
            }
            if (typeof(IName).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("Name", keyValue);
                keyField = "Name";
                return cr;
            }
           


            return null;
        }

        private static CriteriaOperator GetGeneralImportCriteria(ImportDataObject options)
        {
            CriteriaOperator cr = null;
            if (options.DatasetDuplicates != null)
                cr = CriteriaOperator.And(cr, new BinaryOperator("Dataset.Oid", options.DatasetDuplicates.Oid));
            else
                if (options.ProjectDuplicates != null)
                    cr = CriteriaOperator.And(cr, new BinaryOperator("Dataset.Project.Oid", options.ProjectDuplicates.Oid));
            return cr;
        }




        void simpleAction_AddFiles(object sender, SimpleActionExecuteEventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select files";
            dlgOpen.Filter = "Spreadsheets (*.xls,*.xlsx,*.csv,*.txt)|*.xls;*.xlsx;*.csv;*.txt|Microsoft Excel 97-2003 Workbooks (*.xls)|*.xls|Microsoft Excel 2007-2013 Workbooks (*.xlsx)|*.xlsx|CSV (Comma delimited) (*.csv)|*.csv|Text (Tab delimited) (*.txt)|*.txt";
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;

            IObjectSpace os = View.ObjectSpace;
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    ImportDataObject obj = os.CreateObject<ImportDataObject>();
                    obj.File = file;
                    obj.CreateMissingReferences = true;
                    foreach (var typeInfo in XafTypesInfo.Instance.PersistentTypes)
                    {
                        if ((/*typeInfo.AssemblyInfo.FullName.Contains("EarthCape") && */(typeInfo.IsPersistent) && (GeneralHelper.IsCreatable(typeInfo))))
                            if (file.Contains(typeInfo.Name))
                            {
                                obj.ObjectType = typeInfo.Name;
                                break;
                            }
                    }
                    View.CollectionSource.Add(obj);
                }
            }
            //View.ObjectSpace.CommitChanges();
            View.Refresh();
            collection = View.CollectionSource;
        }


        private bool ImportTaxonomicFields(DoWorkEventArgs e, Worksheet sheet, CellRange range, Collection<string> filefields, ImportDataObject options)
        {
            List<string> supportedCategories = new List<string>(new string[]
	{
	    "Kingdom",
	    "Phylum",    
	    "Class",    
	    "Order",
	    "Family",
	    "Subfamily",
	    "Genus",
	    "Species",
	    "Subspecies",
	    "Variety",
	    "Form"
	});
            // string parentColumn = "";
            // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
            // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
            foreach (string category in supportedCategories)
            {
                if (filefields.IndexOf(category) > -1)
                {
                    if (e != null)
                    {
                        SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, String.Format("Importing {0}:", category));
                        SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, 0);
                    }
                    GetTaxonomicColumn(e, sheet, category, options, range, filefields, supportedCategories);
                    os.CommitChanges();
                }
            }
            if (filefields.IndexOf("Order") > -1)
                filefields[filefields.IndexOf("Order")] = "Order1";
            if (filefields.IndexOf("Genus") > -1)
                filefields[filefields.IndexOf("Genus")] = "Genus1";
            if (filefields.IndexOf("Species") > -1)
                filefields[filefields.IndexOf("Species")] = "Species1";


            //checking for extra fields apart from hierarcgical ones
            return CheckExtraFields(filefields, supportedCategories);

        }

        /*     private bool ImportTaxonomicFields( Worksheet sheet, CellRange range, Collection<string> filefields, ImportDataObject options)
             {
                 List<string> supportedCategories = new List<string>(new string[]
         {
             "Kingdom",
             "Phylum",    
             "Class",    
             "Order",
             "Family",
             "Subfamily",
             "Genus",
             "Species",
             "Subspecies",
             "Variety",
             "Form"
         });
                 string parentColumn = "";
                 // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
                 // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
                 foreach (string category in supportedCategories)
                 {
                     if (filefields.IndexOf(category) > -1)
                     {
                         GetTaxonomicColumn(sheet, category, options, range, filefields, supportedCategories);
                         os.CommitChanges();
                     }
                 }
                 if (filefields.IndexOf("Order") > -1)
                     filefields[filefields.IndexOf("Order")] = "Order1";
                 if (filefields.IndexOf("Genus") > -1)
                     filefields[filefields.IndexOf("Genus")] = "Genus1";
                 if (filefields.IndexOf("Species") > -1)
                     filefields[filefields.IndexOf("Species")] = "Species1";


                 //checking for extra fields apart from hierarcgical ones
                 return CheckExtraFields(filefields, supportedCategories);

             }
        */
        private bool ImportLocalityFields(DoWorkEventArgs e, Worksheet sheet1, CellRange range, Collection<string> filefields, ImportDataObject options)
        {
            List<string> supportedCategories = new List<string>(new string[]
 	{
	    "Continent",
	    "Country",    
	    "Admin1",    
	    "Admin2",
	    "Admin3",
	    "Admin4",
	    "Name",
	});
            Worksheet sheet = sheet1.Workbook.Worksheets.Add();
            sheet.CopyFrom(sheet1);
            sheet.Name = "Hierarchy import";
            int col = range.ColumnCount;
            if (filefields.IndexOf("Oid") == -1)
            {
                filefields.Add("Oid");
                sheet.Cells[0, col].SetValue("Oid");
                col = col + 1;
            }


            // string parentColumn = "";
            // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
            // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
            foreach (string category in supportedCategories)
            {
                if (filefields.IndexOf(category) > -1)
                {
                    if (e != null)
                    {
                        SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, String.Format("Importing {0}:", category));
                        SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, 0);
                    }
                    GetLocalityColumn(e, sheet, sheet1, category, options, range, filefields, supportedCategories);
                    //  os.CommitChanges();
                }
            }
            os.CommitChanges();

            //checking for extra fields apart from hierarcgical ones
            return CheckExtraFields(filefields, supportedCategories);
        }

        /*      private bool ImportLocalityFields(Worksheet sheet1, CellRange range, Collection<string> filefields, ImportDataObject options)
              {
                  List<string> supportedCategories = new List<string>(new string[]
          {
              "Continent",
              "Country",    
              "Admin1",    
              "Admin2",
              "Admin3",
              "Admin4",
              "Name",
          });
                  Worksheet sheet = sheet1.Workbook.Worksheets.Add();
                  sheet.CopyFrom(sheet1);
                  sheet.Name = "Hierarchy import";
                  int col = range.ColumnCount;
                  if (filefields.IndexOf("Oid") == -1)
                  {
                      filefields.Add("Oid");
                      sheet.Cells[0, col].SetValue("Oid");
                      col = col + 1;
                  }


                  string parentColumn = "";
                  // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
                  // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
                  foreach (string category in supportedCategories)
                  {
                      if (filefields.IndexOf(category) > -1)
                      {
                          GetLocalityColumn(sheet, sheet1, category, options, range, filefields, supportedCategories);
                          //  os.CommitChanges();
                      }
                  }
                  os.CommitChanges();

                  //checking for extra fields apart from hierarcgical ones
                  return CheckExtraFields(filefields, supportedCategories);
              }
         */
        private static bool CheckExtraFields(Collection<string> filefields, List<string> supportedCategories)
        {
            foreach (string filefield in filefields)
            {
                if ((filefield != "Code") && (filefield != "ShortName") && (filefield != "Oid") && (filefield != "ImportedName") && (filefield != "Order1") && (filefield != "Genus1") && (filefield != "Species1"))
                    if (supportedCategories.IndexOf(filefield) == -1)
                        return true;
            }
            return false;
        }

        private void GetTaxonomicColumn(DoWorkEventArgs e, Worksheet sheet, string category, ImportDataObject options, CellRange range, Collection<string> filefields, List<string> supportedCategories)
        {
            bool keyFieldIsCode = false;

            if (filefields.IndexOf(category) > -1)
            {
                string current = "";
                Guid parentOid = Guid.Empty;
                Guid currentOid = Guid.Empty;
                TaxonomicName taxon = null;
                for (int i = 1; i < range.RowCount; i++)
                {
                    try
                    {
                        if (e != null)
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                        if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(category)].Value.ToString()))
                        {
                            string parentColumn = GetParentColumn(sheet, category, i, supportedCategories, filefields);
                            string childColumn = GetChildColumn(sheet, category, i, supportedCategories, filefields);
                            if ((sheet.Cells[i, filefields.IndexOf(category)].Value.ToString() != current) || ((!string.IsNullOrEmpty(parentColumn)) && (sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString() != parentOid.ToString())))
                            {
                                string shortName = "";
                                if ((filefields.IndexOf("ShortName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString())))
                                    shortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                                parentOid = Guid.Empty;
                                current = sheet.Cells[i, filefields.IndexOf(category)].Value.ToString();
                                string keyField = "";
                                object keyValue = null;
                                if (!string.IsNullOrEmpty(parentColumn))
                                    parentOid = Guid.Parse(sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString());
                                if (!string.IsNullOrEmpty(childColumn))
                                {
                                    keyField = "Name";
                                    keyValue = current;
                                    taxon = GetTaxon(keyField, keyValue, category, "", current, parentOid, options, false);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(shortName))
                                    {
                                        keyField = "ShortName";
                                        keyValue = shortName;
                                        keyFieldIsCode = true;
                                    }
                                    else
                                    {
                                        keyField = "Name";
                                        keyValue = current;
                                        keyFieldIsCode = false;
                                    }
                                    //  FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "Oid", "Name", "ShortName" });
                                    taxon = GetTaxon(keyField, keyValue, category, shortName, current, parentOid, options, keyFieldIsCode);
                                }
                                if (taxon != null)
                                {
                                    parent = taxon;
                                    currentOid = taxon.Oid;
                                }
                            }
                            sheet.Cells[i, filefields.IndexOf(category)].SetValue(taxon.Oid.ToString());
                            if (string.IsNullOrEmpty(childColumn))
                            {
                                if ((String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString())) || IsLower(supportedCategories, category, sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString()))
                                {
                                    sheet.Cells[i, filefields.IndexOf("ImportedName")].SetValue(category);
                                    sheet.Cells[i, filefields.IndexOf("Oid")].SetValue(taxon.Oid.ToString());
                                }
                            }


                        }
                        else
                        {
                        }
                        if (e != null)
                            if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                    }
                    catch (Exception ex)
                    {
                        Cell cell = sheet.Cells[i, filefields.IndexOf(category)];
                        sheet.Comments.Add(cell, SecuritySystem.CurrentUserName, ex.Message);
                        cell.Fill.BackgroundColor = Color.Red;
                        if (e != null)
                            if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                    }
                }
            }
        }
    
        Locality parentLoc = null;
        private void GetLocalityColumn(DoWorkEventArgs e, Worksheet sheet, Worksheet sheetOriginal, string category, ImportDataObject options, CellRange range, Collection<string> filefields, List<string> supportedCategories)
        {
            if (filefields.IndexOf(category) > -1)
            {
                string current = "";
                Guid parentOid = Guid.Empty;
                Guid currentOid = Guid.Empty;
                Locality loc = null;
                for (int i = 1; i < range.RowCount; i++)
                {
                    try
                    {
                        if (e != null)
                            SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                        if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(category)].Value.ToString()))
                        {
                            string parentColumn = GetParentColumn(sheet, category, i, supportedCategories, filefields);
                            string childColumn = GetChildColumn(sheet, category, i, supportedCategories, filefields);
                            if ((sheet.Cells[i, filefields.IndexOf(category)].Value.ToString() != current) || ((!string.IsNullOrEmpty(parentColumn)) && (sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString() != parentOid.ToString())))
                            {
                                string Code = "";
                                parentOid = Guid.Empty;
                                current = sheet.Cells[i, filefields.IndexOf(category)].Value.ToString();
                                if (String.IsNullOrEmpty(childColumn))
                                    if ((filefields.IndexOf("Code") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString())))
                                        Code = sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString();
                                //string keyField = "";
                                //object keyValue = null;
                                if (!string.IsNullOrEmpty(parentColumn))
                                    parentOid = Guid.Parse(sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString());
                                //FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "Oid", "Name", "Code" });
                                loc = GetLocality("Name", current, category, Code, current, parentOid, options);
                                if (loc != null)
                                {
                                    parentLoc = loc;
                                    currentOid = loc.Oid;
                                }
                            }
                            sheet.Cells[i, filefields.IndexOf(category)].SetValue(loc.Oid.ToString());
                            if ((String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString())) || IsLower(supportedCategories, category, sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString()))
                            {
                                sheet.Cells[i, filefields.IndexOf("ImportedName")].SetValue(category);
                                sheet.Cells[i, filefields.IndexOf("Oid")].SetValue(loc.Oid.ToString());
                                sheetOriginal.Cells[i, filefields.IndexOf("Oid")].SetValue(loc.Oid.ToString());
                            }


                        }
                        if (e != null)
                            if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                    }
                    catch (Exception ex)
                    {
                        Cell cell = sheet.Cells[i, filefields.IndexOf(category)];
                        string auth=SecuritySystem.CurrentUserName;
                        if (auth == "") auth = "no user";
                        sheet.Comments.Add(cell, auth, ex.Message);
                        cell.Fill.BackgroundColor = Color.Red;

                        if (e != null)
                            if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                    }
                }
            }
        }
        private bool IsLower(List<string> supportedCategories, string category, string p)
        {
            return supportedCategories.IndexOf(category) > supportedCategories.IndexOf(p);
        }

        private string GetParentColumn(Worksheet sheet, string p, int row, List<string> supportedCategories, Collection<string> filefields)
        {
            int colIndex = -1;
            int i = supportedCategories.IndexOf(p) - 1;
            while ((colIndex == -1) && (i > -1))
            {
                if (filefields.IndexOf(supportedCategories[i]) > -1)
                    if ((!string.IsNullOrEmpty(sheet.Cells[row, filefields.IndexOf(supportedCategories[i])].Value.ToString())))
                        colIndex = filefields.IndexOf(supportedCategories[i]);
                i = i - 1;
            }
            if (colIndex == -1)
                return "";
            return filefields[colIndex];
        }
        private string GetChildColumn(Worksheet sheet, string p, int row, List<string> supportedCategories, Collection<string> filefields)
        {
            int colIndex = -1;
            int i = supportedCategories.IndexOf(p) + 1;
            while ((colIndex == -1) && (i < supportedCategories.Count))
            {
                if (filefields.IndexOf(supportedCategories[i]) > -1)
                    if ((!string.IsNullOrEmpty(sheet.Cells[row, filefields.IndexOf(supportedCategories[i])].Value.ToString())))
                        colIndex = filefields.IndexOf(supportedCategories[i]);
                i = i + 1;
            }
            if (colIndex == -1)
                return "";
            return filefields[colIndex];
        }


        ITreeNode parent = null;

        private TaxonomicName GetTaxon(string keyField, object keyValue, string categoryName, string shortName, string name, Guid parentOid, ImportDataObject options, bool keyFieldIsCode)
        {
            TaxonomicName taxon = null;
            TaxonomicName parentTaxon = null;
            CriteriaOperator cr = null;
            CriteriaOperator crparent = null;
            Guid projectOid = Guid.Empty;
            if (options.DatasetDuplicates != null) projectOid = options.DatasetDuplicates.Oid;
            if (!parentOid.Equals(Guid.Empty))
            {
                crparent = new BinaryOperator("Parent", parentOid);
                parentTaxon = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", parentOid));
            }
            if (keyFieldIsCode)
                cr = CriteriaOperator.And(new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
            else
                cr = CriteriaOperator.Or(
                   CriteriaOperator.And(crparent, new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options)),
                   CriteriaOperator.And(new NullOperator("Parent"), new BinaryOperator(keyField, keyValue)) //note NullOperator removes "authorName" error when importing
                   );
            taxon = os.FindObject<TaxonomicName>(cr);
            if ((taxon == null) && (keyField == "ShortName") && (!string.IsNullOrEmpty(name)))
            {
                cr = CriteriaOperator.And(crparent, new BinaryOperator("ShortName", ""), new BinaryOperator("Name", name), GetGeneralImportCriteria(options));
                taxon = os.FindObject<TaxonomicName>(cr);
                if (taxon != null)
                {
                    taxon.ShortName = shortName;
                    taxon.Save();
                    os.CommitChanges();
                }
            }
            if (taxon != null)
            {
                taxon.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,os,null, categoryName);
                if (!string.IsNullOrEmpty(name))
                    taxon.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    taxon.ShortName = shortName;
                if (parentTaxon != null)
                    taxon.Parent = parentTaxon;
                if ((taxon is Species) && (parentTaxon != null) && (parentTaxon is HigherTaxon))
                    ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                if ((taxon is SubspecificTaxon) && (parentTaxon != null) && (parentTaxon is Species))
                    ((SubspecificTaxon)taxon).Species = (Species)parentTaxon;
                taxon.Save();
                os.CommitChanges();
            }
            if (taxon == null)
            {
                if (categoryName == "Genus")
                    taxon = os.CreateObject<Genus>();
                else
                    if (categoryName == "Species")
                    {
                        taxon = os.CreateObject<Species>();
                        if (parentTaxon is HigherTaxon)
                            ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                        if (parentTaxon is Genus)
                            ((Species)taxon).Genus = (Genus)parentTaxon;
                    }
                    else
                        if (categoryName == "Family")
                        {
                            taxon = os.CreateObject<Family>();
                        }
                        else
                            if ((categoryName == "Subspecies") || (categoryName == "Variety") || (categoryName == "Form"))
                            {
                                taxon = os.CreateObject<SubspecificTaxon>();
                            }
                            else
                            {
                                taxon = os.CreateObject<HigherTaxon>();
                            }
                taxon.SetMemberValue(keyField, keyValue);
                taxon.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,os,null, categoryName);
                if (!string.IsNullOrEmpty(name))
                    taxon.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    taxon.ShortName = shortName;
                if (parentTaxon != null)
                    taxon.Parent = parentTaxon;
                if ((taxon is Species) && (parentTaxon != null) && (parentTaxon is HigherTaxon))
                    ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                if ((taxon is SubspecificTaxon) && (parentTaxon != null) && (parentTaxon is Species))
                    ((SubspecificTaxon)taxon).Species = (Species)parentTaxon;
                taxon.Save();
                os.CommitChanges();
            }
            return taxon;
        }

        private Locality GetLocality(string keyField, object keyValue, string categoryName, string shortName, string name, Guid parentOid, ImportDataObject options)
        {
            Locality loc = null;
            Locality parentTaxon = null;
            CriteriaOperator cr = null;
            Guid projectOid = Guid.Empty;
            if (options.DatasetDuplicates != null) projectOid = options.DatasetDuplicates.Oid;
            if (!parentOid.Equals(Guid.Empty))
            {
                //cr =CriteriaOperator.Or(new BinaryOperator("Parent", parentOid),new BinaryOperator("Category", parentOid));
                cr = new BinaryOperator("Category", parentOid);
                parentTaxon = os.FindObject<Locality>(CriteriaOperator.And(new BinaryOperator("Oid", parentOid)));
                cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                loc = os.FindObject<Locality>(cr);
                if (loc == null) //covering for existing loc but with no category
                {
                    cr = CriteriaOperator.And(
                        CriteriaOperator.Parse("Category is null"),
                        !CriteriaOperator.Or(
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm).FullName)/*,
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm1).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm2).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm3).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm4).FullName)*/
                        ),
                        new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                    loc = os.FindObject<Locality>(cr);
                }
                if (loc == null)
                {
                    cr = new BinaryOperator("Parent", parentOid);
                    cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                    loc = os.FindObject<LocalityAdm>(cr);
                }
                //this was causing bug reassigning root items
                /* if (loc == null) //covering for existing loc but with no category
                 {
                     cr = CriteriaOperator.Parse("Parent is null");
                     cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                     loc = os.FindObject<LocalityAdm>(cr);
                 }*/
                if ((loc != null) && (parentTaxon != null))
                {
                    if (typeof(LocalityAdm).IsAssignableFrom(loc.GetType()))
                    {
                        if ((((LocalityAdm)loc).Parent == null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                        {
                            ((LocalityAdm)loc).Parent = (LocalityAdm)parentTaxon;
                            loc.Save();
                            os.CommitChanges();
                        }
                    }
                    if (!typeof(LocalityAdm).IsAssignableFrom(loc.GetType()))
                    {
                        if (((loc).Category == null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                        {
                            loc.Category = (LocalityAdm)parentTaxon;
                            loc.Save();
                            os.CommitChanges();
                        }
                    }
                }
            }
            else
            {
                cr = CriteriaOperator.And(new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(options));
                loc = os.FindObject<Locality>(cr);
            }
            if (loc == null)
            {
                if (
                      (categoryName == "Continent")
                    || (categoryName == "Country")
                    || (categoryName == "Admin1")
                    || (categoryName == "Admin2")
                    || (categoryName == "Admin3")
                    || (categoryName == "Admin4"))
                    loc = os.CreateObject<LocalityAdm>();
                else
                    loc = os.CreateObject<Locality>();
                loc.SetMemberValue(keyField, keyValue);
                if (!string.IsNullOrEmpty(name))
                    loc.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    loc.Code = shortName;
                if ((loc is Locality) && (parentTaxon != null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())) && (!typeof(LocalityAdm).IsAssignableFrom(loc.GetType())))
                    ((Locality)loc).Category = (LocalityAdm)parentTaxon;
                if ((loc is LocalityAdm) && (parentTaxon != null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                    ((LocalityAdm)loc).Parent = (LocalityAdm)parentTaxon;
                loc.Save();
                os.CommitChanges();
            }
            return loc;
        }


        private static Locality CreateAdm(Worksheet sheet, Collection<string> filefields, int i, CriteriaOperator cr, LocalityAdm parent, string fieldName)
        {
            Locality Admin = null;
            if ((filefields.IndexOf(fieldName) > -1) && (!sheet.Cells[i, filefields.IndexOf(fieldName)].Value.Equals(null)))
            {
                string AdminVal = sheet.Cells[i, filefields.IndexOf(fieldName)].Value.ToString();
                Admin = os.FindObject<Locality>(CriteriaOperator.And(new BinaryOperator("Name", AdminVal), cr));
                //  if ((Admin != null) && (fieldName == "Country") && (Admin.GetType() != typeof(Country))) throw new Exception("Cannot create Country as object already exists with the same name but different type");
                // if ((Admin != null) && (fieldName == "Continent") && (Admin.GetType() != typeof(Continent))) throw new Exception("Cannot create Continent as object already exists with the same name but different type");
                if ((Admin != null) && (parent != null))
                {
                    if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                        if (((LocalityAdm)Admin).Parent.Oid != parent.Oid)
                            Admin = null;
                }
                if (Admin == null)
                {
                    /* if (fieldName == "Continent")
                         Admin = os.CreateObject<Continent>();
                     else
                         if (fieldName == "Country")
                             Admin = os.CreateObject<Country>();
                         else
                             Admin = os.CreateObject<LocalityAdm>();*/
                    Admin.Name = AdminVal;
                    if (parent != null)
                        if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                            ((LocalityAdm)Admin).Parent = parent;
                    Admin.Save();
                }
                if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                    parent = (LocalityAdm)Admin;
            }
            return Admin;
        }
    }
}