using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Actions;
using System.ComponentModel;
using DevExpress.Spreadsheet;
using System.Collections.ObjectModel;
using DevExpress.XtraSplashScreen;
using System.Windows.Forms;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class SpecialImporters_ViewController : ViewController
    {
        public SpecialImporters_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

           static Locker locker1;
        static BackgroundWorker backgroundWorker1;
        static IObjectSpace os;
        string filename="";
    
        private void simpleAction1_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            os = Application.CreateObjectSpace();
            // options = (ImportOptions)e.PopupWindow.View.CurrentObject;
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            locker1 = new Locker();
            backgroundWorker1.RunWorkerAsync(locker1);


        }
              void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           SplashScreenManager.CloseForm();
            locker1.IsCanceled = false;
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message); return;
            }
            else
            {
                os.CommitChanges();
                /* SpreadsheetImporter_ViewController contr = Frame.GetController<SpreadsheetImporter_ViewController>();
                 if (contr != null)
                     contr.popupWindowShowAction_ImportUnits.Active.SetItemValue("Import", true);*/
            }
        }

         void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            Worksheet sheet = null;
            Range range = null;
            Collection<string> filefields;
            if (worker != null)
            {
                try
                {
                    SplashScreenManager.ShowForm(typeof(SplashScreen_process));
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetTotalFileCount, collection.List.Count);
                    int current = 0;
                             if (((ILocked)e.Argument).IsCanceled)
                            {
                                break;
                            }
                            try
                            {
                                current++;
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCurrentFileCount, current);
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetFileLabel, Path.GetFileName(dataObject.File));
                                ImportHelper.LoadDocument(out sheet, out range, out filefields, filename);
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCount, range.RowCount - 1);
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SendObject, locker1);
                                int col = range.ColumnCount;
                                bool hasExtraFields = true;
                                if (filefields.IndexOf("Oid") == -1)
                                {
                                    filefields.Add("Oid");
                                    sheet.Cells[0, col].SetValue("Oid");
                                    col = col + 1;
                                }
                                if (filefields.IndexOf("ImportedName") == -1)
                                {
                                    filefields.Add("ImportedName");
                                    sheet.Cells[0, col].SetValue("ImportedName");
                                }
                                if (typeof(TaxonomicName).IsAssignableFrom(dataObject.GetObjectType()))
                                {
                                    hasExtraFields=ImportTaxonomicFields(e, sheet, range, filefields, dataObject);
                                }
                                if (typeof(Locality).IsAssignableFrom(dataObject.GetObjectType()))
                                {
                                    hasExtraFields = ImportLocalityFields(e, sheet, range, filefields, dataObject);
                                }
                                object keyValue = "";
                                String keyField = "";
                                if (!hasExtraFields)
                                     break;
                                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Importing record data...");
                                for (int i = 1; i <= range.RowCount - 1; i++)
                                {
                                    //break;
                                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                                    FileItemLinked obj = ImportRecord();
                                    if (obj != null)
                                    {
                                        ImportSimplePropertiesGeneric(obj, sheet, range, filefields, i);
                                        ImportReferencePropertiesGeneric(obj, sheet, range, filefields, i, dataObject);
                                        ImportCustomData(obj, sheet, range, filefields, i, dataObject);
                                        obj.Save();
                                       // os.CommitChanges();
                                    }
                                    if (((ILocked)e.Argument).IsCanceled)
                                    {
                                        break;
                                    }
                                }
                            }
                            finally
                            {
                                if (sheet != null)
                                {
                                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Saving...");
                                    os.CommitChanges();
                                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgressLabel, "Opening output file...");
                                    string output = String.Format("{0}\\{1}_{2}{3}", Path.GetTempPath(), Path.GetFileNameWithoutExtension(dataObject.File), DateTime.Now.ToString("yyyyMMddHHmmssffff"), Path.GetExtension(dataObject.File));
                                    sheet.Workbook.SaveDocument(output, DocumentFormat.OpenXml);
                                 }
                            }
                        
                }
                finally
                {
                }
            }
        }

        private void ImportList()
        {
     string strSQL = "SELECT * FROM [Sheet1$]";
            OleDbConnection cn = new OleDbConnection(strConn);
            cn.Open();
            OleDbCommand cmd = new OleDbCommand(strSQL, cn);
            string folder = Path.GetDirectoryName(cn.DataSource);
            string filename = Path.GetFileNameWithoutExtension(cn.DataSource);
            string logpath = String.Format("{0}\\{1}_log.txt", folder, filename);
            errors = new StreamWriter(logpath, false);
            errors.AutoFlush = true;
            errors.WriteLine("Import started " + DateTime.Now.ToString());
            OleDbDataReader reader = cmd.ExecuteReader();
            project = uow.FindObject<Project>(new BinaryOperator("Oid", project.Oid));
            Collection<string> fields = new Collection<string>();
            fields.Add("FileName");
            fields.Add("Caption");
            fields.Add("FullPath");
            fields.Add("Year");
            fields.Add("Date");
            fields.Add("Comment");
            fields.Add("ImageID");

            fields.Add("Time");

            fields.Add("X");

            fields.Add("Y");
            fields.Add("Locality");
            fields.Add("UnitID");



            int readerline = 0;
            // User p = uow.GetObjectByKey(SecuritySystem.CurrentUser.GetType(); uow.GetKeyValue(SecuritySystem.CurrentUser)) as User;
            while (reader.Read())
            {
                Unit Unit = null;
                if (reader.FieldExists("UnitID"))
                    if (!string.IsNullOrEmpty(reader["UnitID"].ToString()))
                    {

                        Unit = uow.FindObject<Unit>(CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", project.Oid), new BinaryOperator("UnitID", reader["UnitID"].ToString())));
                        if (Unit == null)
                            Unit = uow.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", project.Oid), new BinaryOperator("UnitID", reader["UnitID"].ToString())));
                    }

                FileItemLinked fileItem = null;
                try
                {
                    if (fileItem == null)
                        fileItem = uow.FindObject<FileItemLinked>(CriteriaOperator.And(new BinaryOperator("Project.Oid", project.Oid), new BinaryOperator("File.FileName", reader["FileName"].ToString())));
                    if (fileItem == null)
                        fileItem = uow.FindObject<FileItemLinked>(PersistentCriteriaEvaluationBehavior.InTransaction, CriteriaOperator.And(new BinaryOperator("Project.Oid", project.Oid), new BinaryOperator("File.FileName", reader["FileName"].ToString())));
                    if (fileItem == null)
                    {

                        string path = reader["FullPath"].ToString();
                        if (File.Exists(path))
                        {
                            fileItem = new FileItemLinked(uow);
                            FileDataLinked file = new FileDataLinked(uow);
                            using (FileStream filestream = new FileStream(path, FileMode.Open))
                            {
                                FileDataHelper.LoadFromStream(file, Path.GetFileName(path), filestream, path);

                                //file.LoadFromStream(path, filestream, (messageBoxShow == DialogResult.Yes));
                            }

                            file.Project = project;
                            file.FileName = reader["FileName"].ToString();
                            file.Folder = Path.GetDirectoryName(reader["FullPath"].ToString());
                            file.Save();
                            ((FileItemLinked)fileItem).File = file;
                            fileItem.Project = project;
                            fileItem.Name = Path.GetFileNameWithoutExtension(file.FileName);
                            fileItem.Save();
                        }
                    }
                }
                catch (Exception ex)
                {
                    errors.WriteLine(String.Format("{0}\tline: {1}\t{2}", DateTime.Now, readerline, ex.Message));

                }
                finally
                {

                }
                if (fileItem != null)
                {
                    if (reader.FieldExists("Comment"))
                        if (!string.IsNullOrEmpty(reader["Comment"].ToString()))
                        {
                            fileItem.Comment = reader["Comment"].ToString();
                        }

                    if (reader.FieldExists("ImageID"))
                        if (!string.IsNullOrEmpty(reader["ImageID"].ToString()))
                        {
                            fileItem.SourceId = reader["ImageID"].ToString();
                        }
                    if (Unit != null)
                    {
                        if (Unit.FileSet != null)
                            Unit.FileSet.Files.Add(fileItem);
                    }
                    fileItem.Save();
                }

                readerline += 1;
                if (readerline % 1000 == 999)
                {
                    errors.WriteLine(DateTime.Now.ToString() + readerline + "records done");
                    errors.WriteLine("committing...");
                    errors.WriteLine("committed " + DateTime.Now.ToString());
                    uow.CommitChanges();
                }

            }
            reader.Close();
            cn.Close();
            uow.CommitChanges();
            errors.WriteLine("import finished (" + readerline + " records) " + DateTime.Now);
            errors.Close();
            Process.Start(logpath);
   
        }}
    }
}
