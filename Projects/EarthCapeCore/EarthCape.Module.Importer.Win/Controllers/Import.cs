using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Module.Core;
using EarthCape.Module.Win;
using EarthCape.Module.Importer;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Xpo;
using System.Data.OleDb;
using System.IO;
using System.Collections.ObjectModel;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraSpreadsheet;
using DevExpress.Spreadsheet;
using EarthCape.Taxonomy;

namespace EarthCape.Module.Importer.Win.Controllers
{
    public partial class Import : ViewController
    {
       
        
        private StreamWriter errors = null;
     
        public Import()
        {
            InitializeComponent();
            RegisterActions(components);
          }
     
         private void popupWindowShowAction_ImportLocalityVisits_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ImportLocVisitsOptions options = os.CreateObject<ImportLocVisitsOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
       
        }
      
        private void popupWindowShowAction_ImportLocalityVisits_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportLocVisitsOptions options = (ImportLocVisitsOptions)e.PopupWindow.View.CurrentObject;
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select file";
            if (options.DataProvider == DataProvider.AceOleDb12)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls*";
                dlgOpen.DefaultExt = "xlsx";
            }
            if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls";
                dlgOpen.DefaultExt = "xls";
            }
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
                    {
                        string strConn = "";
                        if (options.DataProvider == DataProvider.AceOleDb12)
                            strConn = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", file);
                        if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
                            strConn = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"", file);
                        Dataset dataset = uow.FindObject<Dataset>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Oid", options.Dataset.Oid));
                        ImportHelper.ImportLocVisit(strConn, uow, file, dataset,options.ImportCustomData,errors);
                        uow.CommitChanges();
                    }
                }
            }
        }

        private void popupWindowShowAction_ImportLocalities_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ImportLocsOptions options = os.CreateObject<ImportLocsOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }

        private void popupWindowShowAction_ImportLocalities_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportLocsOptions options = (ImportLocsOptions)e.PopupWindow.View.CurrentObject;
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select file";
            if (options.DataProvider == DataProvider.AceOleDb12)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls*";
                dlgOpen.DefaultExt = "xlsx";
            }
            if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls";
                dlgOpen.DefaultExt = "xls";
            }
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
                    {
                        string strConn = "";
                        if (options.DataProvider == DataProvider.AceOleDb12)
                            strConn = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", file);
                        if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
                            strConn = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"", file);

                        Dataset dataset = null;
                        if (options.Dataset != null)
                            dataset=uow.FindObject<Dataset>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("Oid", options.Dataset.Oid));
                        ImportHelper.ImportLocs(strConn, uow, file, dataset, options.ImportCustomData,errors);
                        uow.CommitChanges();
                    }
                }
            }
        }

        private void popupWindowShowAction_ImportImageList_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportImageListOptions options = (ImportImageListOptions)e.PopupWindow.View.CurrentObject;
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select file";
            if (options.DataProvider == DataProvider.AceOleDb12)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls*";
                dlgOpen.DefaultExt = "xlsx";
            }
            if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
            {
                dlgOpen.Filter = "Microsoft Excel|*.xls";
                dlgOpen.DefaultExt = "xls";
            }
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.DataLayer))
                    {
                        string strConn = "";
                        if (options.DataProvider == DataProvider.AceOleDb12)
                            strConn = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", file);
                        if (options.DataProvider == DataProvider.MicrosoftJetOleDBb4)
                            strConn = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"", file);
                        ImportHelper.ImportImageList(strConn, uow, options.Project,errors);
                        uow.CommitChanges();
                    }
                }
            }
       
        }

        private void popupWindowShowAction_ImportImageList_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {

            IObjectSpace os = Application.CreateObjectSpace();
            ImportImageListOptions options = os.CreateObject<ImportImageListOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }
        static Locker locker1;
        static BackgroundWorker backgroundWorker1;
        static ImportUnitsOptions options;
        static IObjectSpace os;
       private void popupWindowShowAction_ImportUnits_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            os = Application.CreateObjectSpace();
            options = (ImportUnitsOptions)e.PopupWindow.View.CurrentObject;
                backgroundWorker1 = new BackgroundWorker();
                backgroundWorker1.WorkerSupportsCancellation = true;
                backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
                backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
                locker1 = new Locker();
                backgroundWorker1.RunWorkerAsync(locker1);

       
        }
        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SplashScreenManager.CloseForm();
           locker1.IsCanceled = false;
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message); return;
            }
            else
            {
                os.CommitChanges();
            }
        }

        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker != null)
            {

                SplashScreenManager.ShowForm(typeof(SplashScreen_process));
                string[] lines = options.Files.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetTotalFileCount, lines.Length);
                int current = 0;
                foreach (string fn in lines)
                {
                    current++;
                    Worksheet sheet;
                    Range range;
                    Collection<string> filefields;
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCurrentFileCount, current);
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetFileLabel, Path.GetFileName(fn));
                    LoadDocument(out sheet, out range, out filefields, fn);            
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetCount, range.RowCount);
                    SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SendObject, locker1);
                    ImportUnits(e, sheet, range, filefields);
                }
            }
        }

        private void ImportUnits(DoWorkEventArgs e, Worksheet sheet, Range range, Collection<string> filefields)
        {
            for (int i = 1; i < range.RowCount - 1; i++)
            {
                SplashScreenManager.Default.SendCommand(SplashScreen_process.SplashScreenCommand.SetProgress, i);
                Unit unit = null;
                String UnitID;
                if (filefields.IndexOf("UnitID") > -1)
                {
                    UnitID = sheet.Cells[i, filefields.IndexOf("UnitID")].Value.ToString();
                    if (!String.IsNullOrEmpty(UnitID))
                    {
                        CriteriaOperator cr = null;
                        if (options.UnitIdMatching == UnitIdMatching.MatchWithinDataset)
                        {
                            if (options.Dataset != null)
                                cr = CriteriaOperator.And(new BinaryOperator("Dataset.Oid", options.Dataset.Oid), new BinaryOperator("UnitID", UnitID));
                        }
                        if (options.UnitIdMatching == UnitIdMatching.MatchWithinProject)
                        {
                            if (options.Dataset != null)
                                if (options.Dataset.Project != null)
                                    cr = CriteriaOperator.And(new BinaryOperator("Dataset.Project.Oid", options.Dataset.Project.Oid), new BinaryOperator("UnitID", UnitID));
                        }
                        if (!ReferenceEquals(cr, null))
                        {
                            unit = os.FindObject<Unit>(cr);
                            //if (unit == null)
                            //   unit = os.FindObject<Unit>(PersistentCriteriaEvaluationBehavior.InTransaction, cr);
                        }
                    }
                }
                if (unit == null)
                    unit = os.CreateObject<Unit>();

                ExtractSimpleFields(sheet, filefields, i, unit);
                ExtractComplexFields(sheet, filefields, i, unit, ((options.Dataset != null) && (options.Dataset.Project != null)) ? (options.Dataset.Project):null);
         
                unit.Save();
                os.CommitChanges();
                if (((ILocked)e.Argument).IsCanceled)
                {
                    break;
                }
            }
        }

        private static void ExtractSimpleFields(Worksheet sheet, Collection<string> filefields, int i, Unit unit)
        {
            if ((filefields.IndexOf("Comment") > -1) && (sheet.Cells[i, filefields.IndexOf("Comment")].Value!=null)) unit.Comment = sheet.Cells[i, filefields.IndexOf("Comment")].Value.TextValue;
            if ((filefields.IndexOf("UnitID") > -1) && (sheet.Cells[i, filefields.IndexOf("UnitID")].Value!=null)) unit.UnitID = sheet.Cells[i, filefields.IndexOf("UnitID")].Value.TextValue;
            if ((filefields.IndexOf("Preservation") > -1) && (sheet.Cells[i, filefields.IndexOf("Preservation")].Value!=null)) unit.Preservation = sheet.Cells[i, filefields.IndexOf("Preservation")].Value.TextValue;
            if ((filefields.IndexOf("OtherId") > -1) && (sheet.Cells[i, filefields.IndexOf("OtherId")].Value!=null)) unit.OtherId = sheet.Cells[i, filefields.IndexOf("OtherId")].Value.TextValue;
            if ((filefields.IndexOf("CoordSource") > -1) && (sheet.Cells[i, filefields.IndexOf("CoordSource")].Value!=null)) unit.CoordSource = sheet.Cells[i, filefields.IndexOf("CoordSource")].Value.TextValue;
            if ((filefields.IndexOf("CoordDetails") > -1) && (sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value!=null)) unit.CoordDetails = sheet.Cells[i, filefields.IndexOf("CoordDetails")].Value.TextValue;
            if ((filefields.IndexOf("Host") > -1) && (sheet.Cells[i, filefields.IndexOf("Host")].Value!=null)) unit.Host = sheet.Cells[i, filefields.IndexOf("Host")].Value.TextValue;
            if ((filefields.IndexOf("HostDetails") > -1) && (sheet.Cells[i, filefields.IndexOf("HostDetails")].Value!=null)) unit.HostDetails = sheet.Cells[i, filefields.IndexOf("HostDetails")].Value.TextValue;
            if ((filefields.IndexOf("LabelText") > -1) && (sheet.Cells[i, filefields.IndexOf("LabelText")].Value!=null)) unit.LabelText = sheet.Cells[i, filefields.IndexOf("LabelText")].Value.TextValue;
            if ((filefields.IndexOf("InstitutionCode") > -1) && (sheet.Cells[i, filefields.IndexOf("InstitutionCode")].Value!=null)) unit.InstitutionCode = sheet.Cells[i, filefields.IndexOf("InstitutionCode")].Value.TextValue;
            if ((filefields.IndexOf("CollectionCode") > -1) && (sheet.Cells[i, filefields.IndexOf("CollectionCode")].Value!=null)) unit.CollectionCode = sheet.Cells[i, filefields.IndexOf("CollectionCode")].Value.TextValue;
            if ((filefields.IndexOf("AssociatedTaxa") > -1) && (sheet.Cells[i, filefields.IndexOf("AssociatedTaxa")].Value!=null)) unit.AssociatedTaxa = sheet.Cells[i, filefields.IndexOf("AssociatedTaxa")].Value.TextValue;
            if ((filefields.IndexOf("IdentifiedBy") > -1) && (sheet.Cells[i, filefields.IndexOf("IdentifiedBy")].Value!=null)) unit.IdentifiedBy = sheet.Cells[i, filefields.IndexOf("IdentifiedBy")].Value.TextValue;
            if ((filefields.IndexOf("IdentificationConfirmedBy") > -1) && (sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedBy")].Value!=null)) unit.IdentificationConfirmedBy = sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedBy")].Value.TextValue;
            if ((filefields.IndexOf("IdentificationComments") > -1) && (sheet.Cells[i, filefields.IndexOf("IdentificationComments")].Value!=null)) unit.IdentificationComments = sheet.Cells[i, filefields.IndexOf("IdentificationComments")].Value.TextValue;
            if ((filefields.IndexOf("CollectedOrObservedByText") > -1) && (sheet.Cells[i, filefields.IndexOf("CollectedOrObservedByText")].Value!=null)) unit.CollectedOrObservedByText = sheet.Cells[i, filefields.IndexOf("CollectedOrObservedByText")].Value.TextValue;
            if ((filefields.IndexOf("CollectionMethod") > -1) && (sheet.Cells[i, filefields.IndexOf("CollectionMethod")].Value!=null)) unit.CollectionMethod = sheet.Cells[i, filefields.IndexOf("CollectionMethod")].Value.TextValue;
            if ((filefields.IndexOf("WKT") > -1) && (sheet.Cells[i, filefields.IndexOf("WKT")].Value!=null)) unit.WKT = sheet.Cells[i, filefields.IndexOf("WKT")].Value.TextValue;
            if ((filefields.IndexOf("FieldID") > -1) && (sheet.Cells[i, filefields.IndexOf("FieldID")].Value!=null)) unit.FieldID = sheet.Cells[i, filefields.IndexOf("FieldID")].Value.TextValue;
            if ((filefields.IndexOf("UnitType") > -1) && (sheet.Cells[i, filefields.IndexOf("UnitType")].Value!=null)) unit.UnitType = sheet.Cells[i, filefields.IndexOf("UnitType")].Value.TextValue;
            if ((filefields.IndexOf("TemporaryName") > -1) && (sheet.Cells[i, filefields.IndexOf("TemporaryName")].Value!=null)) unit.TemporaryName = sheet.Cells[i, filefields.IndexOf("TemporaryName")].Value.TextValue;
            if ((filefields.IndexOf("WhereInStore") > -1) && (sheet.Cells[i, filefields.IndexOf("WhereInStore")].Value!=null)) unit.WhereInStore = sheet.Cells[i, filefields.IndexOf("WhereInStore")].Value.TextValue;
            if ((filefields.IndexOf("PlaceDetails") > -1) && (sheet.Cells[i, filefields.IndexOf("PlaceDetails")].Value!=null)) unit.PlaceDetails = sheet.Cells[i, filefields.IndexOf("PlaceDetails")].Value.TextValue;
            if ((filefields.IndexOf("HabitatDetails") > -1) && (sheet.Cells[i, filefields.IndexOf("HabitatDetails")].Value!=null)) unit.HabitatDetails = sheet.Cells[i, filefields.IndexOf("HabitatDetails")].Value.TextValue;
            if ((filefields.IndexOf("AlternativeDate") > -1) && (sheet.Cells[i, filefields.IndexOf("AlternativeDate")].Value!=null)) unit.AlternativeDate = sheet.Cells[i, filefields.IndexOf("AlternativeDate")].Value.TextValue;
            if ((filefields.IndexOf("BiologyDetails") > -1) && (sheet.Cells[i, filefields.IndexOf("BiologyDetails")].Value!=null)) unit.BiologyDetails = sheet.Cells[i, filefields.IndexOf("BiologyDetails")].Value.TextValue;
            if ((filefields.IndexOf("QuantityUnit") > -1) && (sheet.Cells[i, filefields.IndexOf("QuantityUnit")].Value!=null)) unit.QuantityUnit = sheet.Cells[i, filefields.IndexOf("QuantityUnit")].Value.TextValue;
            if ((filefields.IndexOf("PreparedBy") > -1) && (sheet.Cells[i, filefields.IndexOf("PreparedBy")].Value!=null)) unit.PreparedBy = sheet.Cells[i, filefields.IndexOf("PreparedBy")].Value.TextValue;
            if ((filefields.IndexOf("PreparationType") > -1) && (sheet.Cells[i, filefields.IndexOf("PreparationType")].Value!=null)) unit.PreparationType = sheet.Cells[i, filefields.IndexOf("PreparationType")].Value.TextValue;

            if ((filefields.IndexOf("Area") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Area")].Value.ToString()))) unit.Area = sheet.Cells[i, filefields.IndexOf("Area")].Value.NumericValue;
            if ((filefields.IndexOf("CoordRadius") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.ToString()))) unit.CoordRadius = sheet.Cells[i, filefields.IndexOf("CoordRadius")].Value.NumericValue;
            if ((filefields.IndexOf("EPSG") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.ToString()))) unit.EPSG = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("EPSG")].Value.NumericValue);
            if ((filefields.IndexOf("Centroid_X") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.ToString()))) unit.Centroid_X = sheet.Cells[i, filefields.IndexOf("Centroid_X")].Value.NumericValue;
            if ((filefields.IndexOf("Centroid_Y") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.ToString()))) unit.Centroid_Y = sheet.Cells[i, filefields.IndexOf("Centroid_Y")].Value.NumericValue;
            if ((filefields.IndexOf("X") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("X")].Value.ToString()))) unit.X = sheet.Cells[i, filefields.IndexOf("X")].Value.NumericValue;
            if ((filefields.IndexOf("Y") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Y")].Value.ToString()))) unit.Y = sheet.Cells[i, filefields.IndexOf("Y")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude1") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.ToString()))) unit.Altitude1 = sheet.Cells[i, filefields.IndexOf("Altitude1")].Value.NumericValue;
            if ((filefields.IndexOf("Altitude2") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.ToString()))) unit.Altitude2 = sheet.Cells[i, filefields.IndexOf("Altitude2")].Value.NumericValue;
            if ((filefields.IndexOf("Year") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Year")].Value.ToString()))) unit.Year = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Year")].Value.NumericValue);
            if ((filefields.IndexOf("IdentifiedOnYear") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentifiedOnYear")].Value.ToString()))) unit.IdentifiedOnYear = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("IdentifiedOnYear")].Value.NumericValue);
            if ((filefields.IndexOf("IdentificationConfirmedYear") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedYear")].Value.ToString()))) unit.IdentificationConfirmedYear = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("IdentificationConfirmedYear")].Value.NumericValue);
            if ((filefields.IndexOf("Month") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Month")].Value.ToString()))) unit.Month = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Month")].Value.NumericValue);
            if ((filefields.IndexOf("Day") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Day")].Value.ToString()))) unit.Day = Convert.ToInt32(sheet.Cells[i, filefields.IndexOf("Day")].Value.NumericValue);
            if ((filefields.IndexOf("Quantity") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Quantity")].Value.ToString()))) unit.Quantity = sheet.Cells[i, filefields.IndexOf("Quantity")].Value.NumericValue;
            if ((filefields.IndexOf("DoesNotExist") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("DoesNotExist")].Value.ToString()))) unit.DoesNotExist = sheet.Cells[i, filefields.IndexOf("DoesNotExist")].Value.BooleanValue;
            if ((filefields.IndexOf("IsDead") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IsDead")].Value.ToString()))) unit.IsDead = sheet.Cells[i, filefields.IndexOf("IsDead")].Value.BooleanValue;
            if ((filefields.IndexOf("Cf") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Cf")].Value.ToString()))) unit.Cf = sheet.Cells[i, filefields.IndexOf("Cf")].Value.BooleanValue;
            if ((filefields.IndexOf("BirthDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("BirthDate")].Value.ToString()))) unit.BirthDate = sheet.Cells[i, filefields.IndexOf("BirthDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("DeathDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("DeathDate")].Value.ToString()))) unit.DeathDate = sheet.Cells[i, filefields.IndexOf("DeathDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("IdentifiedOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("IdentifiedOn")].Value.ToString()))) unit.IdentifiedOn = sheet.Cells[i, filefields.IndexOf("IdentifiedOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("CheckDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("CheckDate")].Value.ToString()))) unit.CheckDate = sheet.Cells[i, filefields.IndexOf("CheckDate")].Value.DateTimeValue;
            if ((filefields.IndexOf("Date") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Date")].Value.ToString()))) unit.Date = sheet.Cells[i, filefields.IndexOf("Date")].Value.DateTimeValue;
            if ((filefields.IndexOf("EndOn") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("EndOn")].Value.ToString()))) unit.EndOn = sheet.Cells[i, filefields.IndexOf("EndOn")].Value.DateTimeValue;
            if ((filefields.IndexOf("PreparationDate") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("PreparationDate")].Value.ToString()))) unit.PreparationDate = sheet.Cells[i, filefields.IndexOf("PreparationDate")].Value.DateTimeValue;
        }

        private static void ExtractComplexFields(Worksheet sheet, Collection<string> filefields, int i, Unit unit, Project project)
        {
            TaxonomicName taxonomicName = null;
            if ((filefields.IndexOf("Genus") > -1) && (filefields.IndexOf("Species") > -1) && (sheet.Cells[i, filefields.IndexOf("Genus")].Value != null) && (sheet.Cells[i, filefields.IndexOf("Species")].Value != null)) taxonomicName = TaxonomyHelper.GetTaxon(project, os, sheet.Cells[i, filefields.IndexOf("Genus")].Value.TextValue, sheet.Cells[i, filefields.IndexOf("Species")].Value.TextValue, "");
            if ((taxonomicName == null) && (filefields.IndexOf("ShortName") > -1) && (sheet.Cells[i, filefields.IndexOf("ShortName")].Value != null))
            {
                taxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.TextValue));
                if (taxonomicName == null)
                    taxonomicName = os.FindObject<TaxonomicName>(new BinaryOperator("ShortName", sheet.Cells[i, filefields.IndexOf("ShortName")].Value.TextValue), true);
                if (taxonomicName == null)
                {
                    taxonomicName = os.CreateObject<Species>();
                    taxonomicName.ShortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.TextValue;
                    taxonomicName.Save();
                }
            }
           
            if (((unit.TaxonomicName != null) && (unit.TaxonomicName.Oid != taxonomicName.Oid)) || (unit.TaxonomicName == null))
                unit.TaxonomicName = taxonomicName;
        }
        private static void LoadDocument(out Worksheet sheet, out Range range, out Collection<string> filefields, string fn)
        {
            //string fn = String.Format("{0}\\{1}", options.File.Folder, options.File.FileName);
            SpreadsheetControl doc = new SpreadsheetControl();
            IWorkbook workbook = doc.Document;
            using (FileStream stream = new FileStream(fn, FileMode.Open))
            {
                workbook.LoadDocument(stream, DocumentFormat.OpenXml);
            }
            sheet = workbook.Worksheets[0];
            range = sheet.GetUsedRange();
            RowCollection rows = sheet.Rows;
            filefields = new Collection<string>();
            for (int i = 0; i <= range.ColumnCount - 1; i++)
            {
                Cell cell = sheet.Cells[0, i];
                filefields.Add(cell.Value.ToString());
            }
            //WaitForm form = new WaitForm();
        }
  
        private void popupWindowShowAction_ImportUnits_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ImportUnitsOptions options = os.CreateObject<ImportUnitsOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            e.View = dv;
        }

        private void simpleAction_ChooseFiles_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            ImportUnitsOptions options = (ImportUnitsOptions)View.CurrentObject;
            OpenFileDialog dlgOpen = new OpenFileDialog();

            dlgOpen.Title = "Select files";
           
            dlgOpen.CheckFileExists = true;
            dlgOpen.ShowReadOnly = false;
            dlgOpen.Multiselect = true;


            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in dlgOpen.FileNames)
                {
                    options.Files=(String.IsNullOrEmpty(options.Files)) ? file : options.Files+System.Environment.NewLine+file;
                }
            }
        }
    }
}
