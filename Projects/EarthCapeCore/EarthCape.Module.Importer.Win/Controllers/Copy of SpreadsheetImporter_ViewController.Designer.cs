using EarthCape.Module.Core;
namespace EarthCape.Module.Importer.Win.Controllers
{
    partial class SpreadsheetImporter_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ImportUnits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ImportLocalities = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ImportTaxa = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ImportData = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.simpleAction_ChooseFiles = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_ImportUnits
            // 
            this.popupWindowShowAction_ImportUnits.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportUnits.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportUnits.Caption = "Import units";
            this.popupWindowShowAction_ImportUnits.Category = "Export";
            this.popupWindowShowAction_ImportUnits.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportUnits.Id = "popupWindowShowAction_ImportUnits";
            this.popupWindowShowAction_ImportUnits.TargetViewId = "x";
            this.popupWindowShowAction_ImportUnits.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_ImportUnits.ToolTip = null;
            this.popupWindowShowAction_ImportUnits.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.popupWindowShowAction_ImportUnits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportSpreadsheet_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportUnits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportSpreadsheet_Execute);
            // 
            // popupWindowShowAction_ImportLocalities
            // 
            this.popupWindowShowAction_ImportLocalities.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportLocalities.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportLocalities.Caption = "Import localities";
            this.popupWindowShowAction_ImportLocalities.Category = "Export";
            this.popupWindowShowAction_ImportLocalities.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportLocalities.Id = "popupWindowShowAction_ImportLocalities";
            this.popupWindowShowAction_ImportLocalities.TargetViewId = "x";
            this.popupWindowShowAction_ImportLocalities.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_ImportLocalities.ToolTip = null;
            this.popupWindowShowAction_ImportLocalities.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.popupWindowShowAction_ImportLocalities.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportLocalities_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportLocalities.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportLocalities_Execute);
            // 
            // popupWindowShowAction_ImportTaxa
            // 
            this.popupWindowShowAction_ImportTaxa.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportTaxa.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportTaxa.Caption = "Import taxa";
            this.popupWindowShowAction_ImportTaxa.Category = "Export";
            this.popupWindowShowAction_ImportTaxa.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportTaxa.Id = "popupWindowShowAction_ImportTaxa";
            this.popupWindowShowAction_ImportTaxa.TargetViewId = "x";
            this.popupWindowShowAction_ImportTaxa.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_ImportTaxa.ToolTip = null;
            this.popupWindowShowAction_ImportTaxa.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.popupWindowShowAction_ImportTaxa.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportTaxa_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportTaxa.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportTaxa_Execute);
            // 
            // simpleAction_ImportData
            // 
            this.simpleAction_ImportData.Caption = "Import data";
            this.simpleAction_ImportData.Category = "Export";
            this.simpleAction_ImportData.ConfirmationMessage = null;
            this.simpleAction_ImportData.Id = "simpleAction_ImportData";
            this.simpleAction_ImportData.ImageName = "InsertTable";
            this.simpleAction_ImportData.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_ImportData.ToolTip = null;
            this.simpleAction_ImportData.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ImportData_Execute);
            // 
            // simpleAction_ChooseFiles
            // 
            this.simpleAction_ChooseFiles.Caption = "Choose files";
            this.simpleAction_ChooseFiles.Category = "PopupActions";
            this.simpleAction_ChooseFiles.ConfirmationMessage = null;
            this.simpleAction_ChooseFiles.Id = "simpleAction_ChooseFiles";
            this.simpleAction_ChooseFiles.TargetObjectType = typeof(EarthCape.Module.Importer.ImportOptions);
            this.simpleAction_ChooseFiles.ToolTip = null;
            this.simpleAction_ChooseFiles.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ChooseFiles_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportUnits;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ChooseFiles;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportLocalities;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportTaxa;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImportData;
    }
}
