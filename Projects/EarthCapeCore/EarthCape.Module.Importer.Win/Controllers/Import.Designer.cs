using EarthCape.Module.Core;
namespace EarthCape.Module.Importer.Win.Controllers
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_ImportLocalityVisits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ImportLocalities = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ImportImageList = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.popupWindowShowAction_ImportUnits = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.simpleAction_ChooseFiles = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // popupWindowShowAction_ImportLocalityVisits
            // 
            this.popupWindowShowAction_ImportLocalityVisits.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportLocalityVisits.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportLocalityVisits.Caption = "Import loc. visits";
            this.popupWindowShowAction_ImportLocalityVisits.Category = "Tools";
            this.popupWindowShowAction_ImportLocalityVisits.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportLocalityVisits.Id = "popupWindowShowAction_ImportLocalityVisits";
            this.popupWindowShowAction_ImportLocalityVisits.ImageName = null;
            this.popupWindowShowAction_ImportLocalityVisits.Shortcut = null;
            this.popupWindowShowAction_ImportLocalityVisits.Tag = null;
            this.popupWindowShowAction_ImportLocalityVisits.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ImportLocalityVisits.TargetViewId = null;
            this.popupWindowShowAction_ImportLocalityVisits.ToolTip = null;
            this.popupWindowShowAction_ImportLocalityVisits.TypeOfView = null;
            this.popupWindowShowAction_ImportLocalityVisits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportLocalityVisits_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportLocalityVisits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportLocalityVisits_Execute);
            // 
            // popupWindowShowAction_ImportLocalities
            // 
            this.popupWindowShowAction_ImportLocalities.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportLocalities.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportLocalities.Caption = "Import localities";
            this.popupWindowShowAction_ImportLocalities.Category = "Tools";
            this.popupWindowShowAction_ImportLocalities.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportLocalities.Id = "popupWindowShowAction_ImportLocalities";
            this.popupWindowShowAction_ImportLocalities.ImageName = null;
            this.popupWindowShowAction_ImportLocalities.Shortcut = null;
            this.popupWindowShowAction_ImportLocalities.Tag = null;
            this.popupWindowShowAction_ImportLocalities.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ImportLocalities.TargetViewId = "";
            this.popupWindowShowAction_ImportLocalities.ToolTip = null;
            this.popupWindowShowAction_ImportLocalities.TypeOfView = null;
            this.popupWindowShowAction_ImportLocalities.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportLocalities_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportLocalities.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportLocalities_Execute);
            // 
            // popupWindowShowAction_ImportImageList
            // 
            this.popupWindowShowAction_ImportImageList.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportImageList.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportImageList.Caption = "Import image list";
            this.popupWindowShowAction_ImportImageList.Category = "ObjectsCreation";
            this.popupWindowShowAction_ImportImageList.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportImageList.Id = "popupWindowShowAction_ImportImageList";
            this.popupWindowShowAction_ImportImageList.ImageName = null;
            this.popupWindowShowAction_ImportImageList.Shortcut = null;
            this.popupWindowShowAction_ImportImageList.Tag = null;
            this.popupWindowShowAction_ImportImageList.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ImportImageList.TargetObjectType = typeof(EarthCape.Module.Core.FileItem);
            this.popupWindowShowAction_ImportImageList.TargetViewId = null;
            this.popupWindowShowAction_ImportImageList.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_ImportImageList.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_ImportImageList.ToolTip = null;
            this.popupWindowShowAction_ImportImageList.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_ImportImageList.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportImageList_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportImageList.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportImageList_Execute);
            // 
            // popupWindowShowAction_ImportUnits
            // 
            this.popupWindowShowAction_ImportUnits.AcceptButtonCaption = null;
            this.popupWindowShowAction_ImportUnits.CancelButtonCaption = null;
            this.popupWindowShowAction_ImportUnits.Caption = "Import units";
            this.popupWindowShowAction_ImportUnits.Category = "Tools";
            this.popupWindowShowAction_ImportUnits.ConfirmationMessage = null;
            this.popupWindowShowAction_ImportUnits.Id = "popupWindowShowAction_ImportUnits";
            this.popupWindowShowAction_ImportUnits.ImageName = null;
            this.popupWindowShowAction_ImportUnits.Shortcut = null;
            this.popupWindowShowAction_ImportUnits.Tag = null;
            this.popupWindowShowAction_ImportUnits.TargetObjectsCriteria = null;
            this.popupWindowShowAction_ImportUnits.TargetViewId = null;
            this.popupWindowShowAction_ImportUnits.ToolTip = null;
            this.popupWindowShowAction_ImportUnits.TypeOfView = null;
            this.popupWindowShowAction_ImportUnits.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_ImportUnits_CustomizePopupWindowParams);
            this.popupWindowShowAction_ImportUnits.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_ImportUnits_Execute);
            // 
            // simpleAction_ChooseFiles
            // 
            this.simpleAction_ChooseFiles.Caption = "Choose files";
            this.simpleAction_ChooseFiles.ConfirmationMessage = null;
            this.simpleAction_ChooseFiles.Id = "simpleAction_ChooseFiles";
            this.simpleAction_ChooseFiles.ImageName = null;
            this.simpleAction_ChooseFiles.Shortcut = null;
            this.simpleAction_ChooseFiles.Tag = null;
            this.simpleAction_ChooseFiles.TargetObjectsCriteria = null;
            this.simpleAction_ChooseFiles.TargetObjectType = typeof(EarthCape.Module.Core.ImportUnitsOptions);
            this.simpleAction_ChooseFiles.TargetViewId = null;
            this.simpleAction_ChooseFiles.ToolTip = null;
            this.simpleAction_ChooseFiles.TypeOfView = null;
            this.simpleAction_ChooseFiles.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ChooseFiles_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportLocalityVisits;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportLocalities;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportImageList;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_ImportUnits;
        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ChooseFiles;
    }
}
