using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;


namespace EarthCape.Module.Importer.Win.Controllers
{
    public partial class SpreadsheetImporter_ViewController : ViewController
    {
        public SpreadsheetImporter_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }     
        private void simpleAction_ImportData_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
             IObjectSpace objectSpace = Application.CreateObjectSpace();
            CollectionSource collectionSource =
                new CollectionSource(objectSpace, typeof(ImportDataObject));
            if ((collectionSource.Collection as XPBaseCollection) != null)
            {
                ((XPBaseCollection)collectionSource.Collection).LoadingEnabled = false;
            }
            DevExpress.ExpressApp.ListView view = Application.CreateListView(Application.GetListViewId(typeof(ImportDataObject)), collectionSource, false);
            view.Editor.AllowEdit = true;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            e.ShowViewParameters.CreatedView = view;
            e.ShowViewParameters.Context = TemplateContext.PopupWindow;
       //     DialogController dc = Application.CreateController<DialogController>();
         //   e.ShowViewParameters.Controllers.Add(dc);
        }

        

      
       
    }
}
