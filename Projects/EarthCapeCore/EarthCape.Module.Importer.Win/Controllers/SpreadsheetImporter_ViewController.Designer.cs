using EarthCape.Module.Core;
namespace EarthCape.Module.Importer.Win.Controllers
{
    partial class SpreadsheetImporter_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

         private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_ImportData = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_ImportData
            // 
            this.simpleAction_ImportData.Caption = "Import data";
            this.simpleAction_ImportData.Category = "Print";
            this.simpleAction_ImportData.ConfirmationMessage = null;
            this.simpleAction_ImportData.Id = "simpleAction_ImportData";
            this.simpleAction_ImportData.ImageName = "InsertTable";
            this.simpleAction_ImportData.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_ImportData.ToolTip = null;
            this.simpleAction_ImportData.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_ImportData_Execute);

         } 

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_ImportData;
    }
}
