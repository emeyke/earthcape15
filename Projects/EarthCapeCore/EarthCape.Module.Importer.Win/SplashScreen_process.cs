﻿using System;
using System.Collections.Generic;
using DevExpress.XtraSplashScreen;

namespace EarthCape.Module.Importer.Win
{
    public partial class SplashScreen_process : SplashScreen
    {
        public SplashScreen_process()
        {
            InitializeComponent();
        }

        #region Overrides
        int progress = 0;
        string progressLabel = "Records";
        int total = 0;
        int currentFileCount = 0;
        int totalFileCount = 0;
        object locker;

    
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
            SplashScreenCommand command = (SplashScreenCommand)cmd;
            if (command == SplashScreenCommand.SetProgress)
            {
                progress = (int)arg;
                progressBar1.Position = progress;
                labelControl2.Text = string.Format("{0} {1} of {2}", progressLabel, progress, total);
            }
            if (command == SplashScreenCommand.SetProgressLabel)
            {
                progressLabel = arg.ToString();
            }
            if (command == SplashScreenCommand.SetCount)
            {
                int pos = (int)arg;
                progressBar1.Properties.Maximum = pos;
                total = pos;
            }
            if (command == SplashScreenCommand.SendObject)
            {
                locker = arg;
            }
            if (command == SplashScreenCommand.SetFileLabel)
            {
                labelControl1.Text = string.Format("Processing file {0} ({1} out of {2})", arg, currentFileCount, totalFileCount);
            }
            if (command == SplashScreenCommand.SetCurrentFileCount)
            {
                currentFileCount = (int)arg;
            }
            if (command == SplashScreenCommand.SetTotalFileCount)
            {
                totalFileCount = (int)arg;
            }
        }
        #endregion

        public enum SplashScreenCommand
        {
            SetProgress,
            SetCount,
            SendObject,
            SetTotalFileCount,
            SetCurrentFileCount,
            SetFileLabel,
            SetProgressLabel
        }

     
        private void button1_Click(object sender, EventArgs e)
        {
           // if (MessageBox.Show("Save imported so far?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{
                if (locker != null)
                    ((ILocked)locker).IsCanceled = true;
           // }
        }
    }
}