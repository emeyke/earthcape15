using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportOptionsDatasetData : ImportOptions
    {
        public ImportOptionsDatasetData(Session session) : base(session) { }

        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }
      

    }
}
