using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
   [NonPersistent]
    public class ImportDataObject : XPBaseObject
    {
        public ImportDataObject(Session session)      : base(session)    {    }
  
 //       [ImmediatePostData]
        public string File { get; set; }
        //  [ImmediatePostData]
        [ModelDefault("PropertyEditorType", "EarthCape.Module.Importer.DataTypeEditor")]
        public String ObjectType { get; set; }
        //   [ImmediatePostData]
        public ErrorHandling ErrorHandling { get; set; }
        //   [ImmediatePostData]
        public int Progress { get; set; }
        //   [ImmediatePostData]
        public Project ProjectDuplicates { get; set; }
        //  [ImmediatePostData]
         public Dataset DatasetDuplicates { get; set; }
        //   [ImmediatePostData]
        public Project ProjectImport { get; set; }
        //  [ImmediatePostData]
        public Dataset DatasetImport { get; set; }
      
        
       
       // [ImmediatePostData]
       // public Dataset DatasetImport { get; set; }
        //  [ImmediatePostData]
        public bool CreateMissingReferences { get; set; }

       internal Type GetObjectType()
       {
           foreach (var typeInfo in XafTypesInfo.Instance.PersistentTypes)
           {
               if (typeInfo.Name == ObjectType) return typeInfo.Type;
           }
           return null;
       }
    }
}