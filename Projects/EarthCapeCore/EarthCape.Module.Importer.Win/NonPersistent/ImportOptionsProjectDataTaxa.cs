using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportOptionsProjectDataTaxa : ImportOptionsProjectData
    {
        public ImportOptionsProjectDataTaxa(Session session) : base(session) { }

        private DuplicateTaxaLookupOption _DuplicatesLookupOption = DuplicateTaxaLookupOption.MatchNamesWithinProject;
        public DuplicateTaxaLookupOption DuplicatesLookupOption
        {
            get
            {
                return _DuplicatesLookupOption;
            }
            set
            {
                SetPropertyValue("DuplicatesLookupOption", ref _DuplicatesLookupOption, value);
            }
        }
    }
}
