using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module.Core;

namespace EarthCape.Module.Core
{
    
    [NonPersistent]
    public class ImportImageListOptions : XPCustomObject
    {
        public ImportImageListOptions(Session session) : base(session) { }

        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
        private DataProvider _DataProvider;
        public DataProvider DataProvider
        {
            get
            {
                return _DataProvider;
            }
            set
            {
                SetPropertyValue("DataProvider", ref _DataProvider, value);
            }
        }
        private bool _ImportCustomData=false;
        public bool ImportCustomData
        {
            get
            {
                return _ImportCustomData;
            }
            set
            {
                SetPropertyValue("ImportCustomData", ref _ImportCustomData, value);
            }
        }
    }

}
