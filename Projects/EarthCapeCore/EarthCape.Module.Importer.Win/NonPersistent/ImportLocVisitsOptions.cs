using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module.Core;

namespace EarthCape.Module.Core
{
    
    [NonPersistent]
    public class ImportLocVisitsOptions : XPCustomObject
    {
        public ImportLocVisitsOptions(Session session) : base(session) { }

        private Dataset _Dataset;
        public Dataset Dataset
        {
            get
            {
                return _Dataset;
            }
            set
            {
                SetPropertyValue("Dataset", ref _Dataset, value);
            }
        }
        private DataProvider _DataProvider;
        public DataProvider DataProvider
        {
            get
            {
                return _DataProvider;
            }
            set
            {
                SetPropertyValue("DataProvider", ref _DataProvider, value);
            }
        }
        private bool _ImportCustomData=false;
        public bool ImportCustomData
        {
            get
            {
                return _ImportCustomData;
            }
            set
            {
                SetPropertyValue("ImportCustomData", ref _ImportCustomData, value);
            }
        }
    }

}
