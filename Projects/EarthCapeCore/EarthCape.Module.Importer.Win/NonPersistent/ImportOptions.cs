using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportOptions : XPCustomObject
    {
        public ImportOptions(Session session) : base(session) { }

      private string _Files;
        [Size(SizeAttribute.Unlimited)]
        public string Files
        {
            get
            {
                return _Files;
            }
            set
            {
                SetPropertyValue("Files", ref _Files, value);
            }
        }

      private ErrorHandling _ErrorHandling = ErrorHandling.ReportAndStepOver;
        public ErrorHandling ErrorHandling
        {
            get
            {
                return _ErrorHandling;
            }
            set
            {
                SetPropertyValue("ErrorHandling", ref _ErrorHandling, value);
            }
        }
    }

}