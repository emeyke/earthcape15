using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportUnitsOptions : ImportOptionsDatasetData
    {
        public ImportUnitsOptions(Session session) : base(session) { }
       
        private UnitIdMatching _UnitIdMatching = UnitIdMatching.MatchWithinDataset;
        public UnitIdMatching UnitIdMatching
        {
            get
            {
                return _UnitIdMatching;
            }
            set
            {
                SetPropertyValue("UnitIdMatching", ref _UnitIdMatching, value);
            }
        }
        private bool _AutoCreateUsers = false;
        public bool AutoCreateUsers
        {
            get
            {
                return _AutoCreateUsers;
            }
            set
            {
                SetPropertyValue("AutoCreateUsers", ref _AutoCreateUsers, value);
            }
        }
    }
}
