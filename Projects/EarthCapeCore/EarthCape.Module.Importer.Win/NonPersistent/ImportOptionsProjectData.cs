using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportOptionsProjectData : ImportOptions
    {
        public ImportOptionsProjectData(Session session) : base(session) { }

        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }

       

    }
}
