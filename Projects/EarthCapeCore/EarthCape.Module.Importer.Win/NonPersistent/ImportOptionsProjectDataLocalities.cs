using System;
using DevExpress.Xpo;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [NonPersistent]
    public class ImportOptionsProjectDataLocalities : ImportOptionsProjectData
    {
        public ImportOptionsProjectDataLocalities(Session session) : base(session) { }

        private DuplicateLocalitiesLookupOption _DuplicatesLookupOption = DuplicateLocalitiesLookupOption.MatchNamesWithinProject;
        public DuplicateLocalitiesLookupOption DuplicatesLookupOption
        {
            get
            {
                return _DuplicatesLookupOption;
            }
            set
            {
                SetPropertyValue("DuplicatesLookupOption", ref _DuplicatesLookupOption, value);
            }
        }
    }
}
