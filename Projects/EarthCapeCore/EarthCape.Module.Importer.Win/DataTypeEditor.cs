using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using EarthCape.Module.Core;

namespace EarthCape.Module.Importer
{
    [PropertyEditor(typeof(String), false)]
    public class DataTypeEditor : StringPropertyEditor
    {
        public DataTypeEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
        }
        protected override object CreateControlCore()
        {
            return new ComboBoxEdit();
        }
        protected override RepositoryItem CreateRepositoryItem()
        {
            return new RepositoryItemComboBox();
        }
        protected override void SetupRepositoryItem(RepositoryItem item)
        {
  //          ((RepositoryItemComboBox)item).Items.Add("TaxonomicName");
            foreach (var typeInfo in XafTypesInfo.Instance.PersistentTypes)
            {
                if ((/*typeInfo.AssemblyInfo.FullName.Contains("EarthCape") && */(typeInfo.IsPersistent) && (GeneralHelper.IsCreatable(typeInfo))))
                ((RepositoryItemComboBox)item).Items.Add(typeInfo.Name);
            }
            ((RepositoryItemComboBox)item).Sorted = true;
            ((RepositoryItemComboBox)item).TextEditStyle = TextEditStyles.DisableTextEditor;
            ((RepositoryItemComboBox)item).Items.Insert(0, "System.String");
            ((RepositoryItemComboBox)item).Items.Insert(0, "System.Int32");
            ((RepositoryItemComboBox)item).Items.Insert(0, "System.Boolean");
            ((RepositoryItemComboBox)item).Items.Insert(0, "System.DateTime");
        }
    }
}
