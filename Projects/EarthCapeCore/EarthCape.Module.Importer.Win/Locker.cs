﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Importer.Win
{
    public class Locker : ILocked
    {
        //// Fields...

        private bool _IsCanceled;

        public bool IsCanceled
        {
            get { return _IsCanceled; }
            set { _IsCanceled = value; }
        }
        public Locker()
        {
            _IsCanceled = false;
        }
    }
    public interface ILocked
    {
        bool IsCanceled { get; set; }
    }
}
