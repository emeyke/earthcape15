using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Taxonomy;
using DevExpress.Data.Filtering;
using EarthCape.Module.Core;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Data.Filtering;

namespace EarthCape.Module.EOL
{
    public partial class EOL_ViewController : ViewController
    {
        public EOL_ViewController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private void singleChoiceAction_EOL_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
           
        }

        private void popupWindowShowAction_EOL_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            EolSearchParameters search=((EolSearchParameters)e.PopupWindow.View.CurrentObject);
            if (search.DownloadEolIdIfMissing)
            {
                   foreach (TaxonomicName item in View.SelectedObjects)
                    {
                        Helper.EOL_GetID(item);
                    }
            }
          /*  if (search.DownloadHierarchiesTo!=null)
            {
                foreach (TaxonomicName item in View.SelectedObjects)
                {
                 //   Helper.EOL_GetHierarchy(item,((XPObjectSpace)View.ObjectSpace).FindObject<TaxonomicClassification>(new BinaryOperator("Oid",search.DownloadHierarchiesTo.Oid)));
                }
        
            }*/
            if (search.DownloadHierarchy)
            {
                foreach (TaxonomicName item in View.SelectedObjects)
                {
                   // Helper.EOL_GetHierarchy(item);
                }

            }
        }

        private void popupWindowShowAction_EOL_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
           XPObjectSpace os = (XPObjectSpace)Application.CreateObjectSpace();
            EolSearchParameters search = os.CreateObject<EolSearchParameters>();
            e.DialogController.SaveOnAccept = false;
            DetailView view = Application.CreateDetailView(os,search);
            e.View = view;
            
        }
    }
}
