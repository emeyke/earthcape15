using System;
using System.Collections.Generic;

using DevExpress.ExpressApp;
using EarthCape.Module.Core;
using DevExpress.Xpo.Metadata;

namespace EarthCape.Module.EOL
{
    public sealed partial class EarthCapeEOLModule : ModuleBase
    {
        public EarthCapeEOLModule()
        {
            InitializeComponent();
        }
        public override void CustomizeTypesInfo(DevExpress.ExpressApp.DC.ITypesInfo typesInfo)
        {
            base.CustomizeTypesInfo(typesInfo);
           /* if (typesInfo.FindTypeInfo(typeof(TaxonomicName)).FindMember("EolId") == null)
            {
                typesInfo.FindTypeInfo(typeof(TaxonomicName)).CreateMember("EolId", typeof(int));
            }
            XafTypesInfo.Instance.RefreshInfo(typeof(TaxonomicName));*/
            
            
            
            //   typesInfo.FindTypeInfo(typeof(TaxonomicName)).AddAttribute(new DevExpress.Persistent.Base.DefaultClassOptionsAttribute());

            //Dennis: simple creation of a new member.
          /*   //Dennis: establishing a One-To-Many relationship between two classes.
                XPDictionary xpDictionary = XafTypesInfo.XpoTypeInfoSource.XPDictionary;
                if (xpDictionary.GetClassInfo(typeof(PersistentObject1)).FindMember("PersistentObject2s") == null)
                {
                    xpDictionary.GetClassInfo(typeof(PersistentObject1)).CreateMember("PersistentObject2s", typeof(XPCollection<PersistentObject2>), true,
                        new AggregatedAttribute(), new AssociationAttribute("PersistentObject1-PersistentObject2s", typeof(PersistentObject2)));
                }
                if (xpDictionary.GetClassInfo(typeof(PersistentObject2)).FindMember("PersistentObject1") == null)
                {
                    xpDictionary.GetClassInfo(typeof(PersistentObject2)).CreateMember("PersistentObject1", typeof(PersistentObject1), new AssociationAttribute("PersistentObject1-PersistentObject2s"));
                }*/
            //Dennis: take special note of the fact that you have to refresh information about type, only if you made the changes in the XPDictionary. If you made the changes directly in the TypeInfo, refreshing is not necessary.
         //   XafTypesInfo.Instance.RefreshInfo(typeof(PersistentObject2));
        }
    }
}
