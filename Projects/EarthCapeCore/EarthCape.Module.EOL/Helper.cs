﻿using System;
using EarthCape.Module.Core;
using System.Data;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EarthCape.Taxonomy;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;



namespace EarthCape.Module.EOL
{
    class Helper
    {
        /* internal static void EOL_GetHierarchy(TaxonomicName item, TaxonomicClassification taxonomicClassification)
          {
              int conceptID = 0;
              using (UnitOfWork uow = new UnitOfWork(item.Session.DataLayer))
              {
                  using (DataSet dataSet = new DataSet())
                  {

                      try
                      {
                        
                          dataSet.ReadXml(String.Format("http://www.eol.org/api/pages/1.0/{0}?common_names=1&details=1&images=2&subjects=all&text=2&videos=1", item.GetMemberValue("EolId")), XmlReadMode.InferSchema);
                      }
                      catch (Exception ex)
                      {
                          throw new Exception(ex.Message);
                      }
                      finally
                      { }
                      DataTable table = dataSet.Tables["taxonConcept"];
                      if (table == null) return;
                      //  if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;

                      foreach (DataRow row in table.Rows)
                      {
                          try
                          {
                              conceptID = Convert.ToInt32(row[0]);
                          }
                          catch (Exception ex)
                          {

                          }
                          finally
                          {

                          }
                          break; //todo just forst result
                      }
                      if (conceptID > 0)
                          using (DataSet dataSet1 = new DataSet())
                          {

                              try
                              {
                                  dataSet1.ReadXml(String.Format("http://www.eol.org/api/hierarchy_entries/1.0/{0}", conceptID), XmlReadMode.InferSchema);
                              }
                              catch (Exception ex)
                              {
                                  throw new Exception(ex.Message);
                              }
                              finally
                              { }
                              DataTable taxa = dataSet.Tables["taxon"];
                              if (table == null) return;
                              //  if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;
                              foreach (DataRow row in taxa.Rows)
                              {
                                  try
                                  {

                                      if (item is Species)
                                      {
                                          SpeciesClassified spcl = new SpeciesClassified(uow);
                                          spcl.Species = uow.FindObject<Species>(new BinaryOperator("Oid", item.Oid));
                                          //  spcl.Category=EOL_GetHierarchy(
                                      }
                                  }
                                  catch (Exception ex)
                                  {

                                  }
                                  finally
                                  {

                                  }
                                  break; //todo just forst result
                              }

                              // objectSpace.CommitChanges();
                          }
                  }
              }
          }*/
    /*     internal static void EOL_GetHierarchy(TaxonomicName item)
          {
              int conceptID = 0;
              using (UnitOfWork uow = new UnitOfWork(item.Session.DataLayer))
              {
                  using (DataSet dataSet = new DataSet())
                  {

                      try
                      {
                        
                          dataSet.ReadXml(String.Format("http://www.eol.org/api/pages/1.0/{0}?common_names=1&details=1&images=2&subjects=all&text=2&videos=1", item.GetMemberValue("EolId")), XmlReadMode.InferSchema);
                      }
                      catch (Exception ex)
                      {
                          throw new Exception(ex.Message);
                      }
                      finally
                      { }
                      DataTable table = dataSet.Tables["taxon"];
                      if (table == null) return;
                      //  if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;

                      foreach (DataRow row in table.Rows)
                      {
                          try
                          {
                              conceptID = Convert.ToInt32(row[1]);
                          }
                          catch (Exception ex)
                          {

                          }
                          finally
                          {

                          }
                          break; //todo just forst result
                      }
                      if (conceptID > 0)
                          using (DataSet dataSet1 = new DataSet())
                          {

                              try
                              {
                                  dataSet1.ReadXml(String.Format("http://www.eol.org/api/hierarchy_entries/1.0/{0}", conceptID), XmlReadMode.InferSchema);
                              }
                              catch (Exception ex)
                              {
                                  throw new Exception(ex.Message);
                              }
                              finally
                              { }
                              DataTable taxa = dataSet1.Tables["taxon"];
                              if (table == null) return;
                              //  if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;
                              TaxonomicName parent = null;
                              foreach (DataRow row in taxa.Rows)
                              {
                                  try
                                  {

                                      if (item is Species)
                                      {
                                          HigherTaxon higherTaxon = null;
                                          HigherRank higherRank = null;
                                          int eolTaxonId = Convert.ToInt32(row["taxonID"]);
                                          higherTaxon = uow.FindObject<HigherTaxon>(new BinaryOperator("EolTaxonId", eolTaxonId));
                                          if (higherTaxon == null)
                                              higherTaxon = uow.FindObject<HigherTaxon>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("EolTaxonId", eolTaxonId));
                                          if (higherTaxon == null)
                                          {
                                              Int32 parentNameUsageID = Convert.ToInt32(row["parentNameUsageID"]);
                                              string rank = Convert.ToString(row["taxonRank"]);
                                              string scientificName = Convert.ToString(row["scientificName"]);
                                              if (rank == "family")
                                              {
                                                  higherTaxon = new Family(uow);
                                                  higherRank = uow.FindObject<TaxonomicCategory>(new BinaryOperator("Name", rank));
                                              }
                                              else
                                              {
                                                  higherTaxon = new HigherTaxon(uow);
                                                  higherRank = uow.FindObject<TaxonomicCategory>(new BinaryOperator("Name", rank));
                                              }
                                              if (higherRank == null)
                                              {
                                                  higherRank = new TaxonomicCategory(uow);
                                                  higherRank.Name = rank;
                                              }
                                              higherTaxon.TaxonomicCategory = higherRank;
                                              higherTaxon.Name = scientificName;
                                              higherTaxon.EolTaxonId = eolTaxonId;
                                          }
                                          if (parent != null)
                                              higherTaxon.Parent = parent;
                                          higherTaxon.Save();

                                          parent = higherTaxon;
                                          // SpeciesClassified spcl = new SpeciesClassified(uow);
                                          //spcl.Species = uow.FindObject<Species>(new BinaryOperator("Oid", item.Oid));
                                          //  spcl.Category=EOL_GetHierarchy(
                                      }
                                  }
                                  catch (Exception ex)
                                  {

                                  }
                                  finally
                                  {

                                  }
                                  //break; //todo just first result
                              }
                              uow.CommitChanges();
                              // objectSpace.CommitChanges();
                          }
                  }
              }
          }*/
        internal static void EOL_GetID(TaxonomicName item)
        {

            using (DataSet dataSet = new DataSet())
            {

                try
                {
                    dataSet.ReadXml(String.Format("http://www.eol.org/api/search/{0}", item.FullName), XmlReadMode.InferSchema);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                { }
                DataTable table = dataSet.Tables["entry"];
                if (table == null) return;
                //  if (table.Rows.Count < 1001) start_index = -1; else start_index += 1000;
                using (UnitOfWork objectSpace = new UnitOfWork(item.Session.DataLayer))
                {

                    foreach (DataRow row in table.Rows)
                    {
                        try
                        {
                            item.SetMemberValue("EolId",Convert.ToInt32(row[2]));
                        }
                        catch (Exception ex)
                        {

                        }
                        finally
                        {

                        }
                        break; //todo just forst result
                    }
                    objectSpace.CommitChanges();

                    // objectSpace.CommitChanges();
                }
            }

        }
    }
}
