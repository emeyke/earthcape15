using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.ComponentModel;
using EarthCape.Module;

namespace EarthCape.Module.EOL
{
    [NonPersistent]
    public class EolSearchParameters : XPCustomObject
    {

        public EolSearchParameters(Session session) : base(session) { }
        private bool _DownloadEolIdIfMissing;
        public bool DownloadEolIdIfMissing
        {
            get
            {
                return _DownloadEolIdIfMissing;
            }
            set
            {
                SetPropertyValue("DownloadEolIdIfMissing", ref _DownloadEolIdIfMissing, value);
            }
        }
        private bool _DownloadHierarchy;
        public bool DownloadHierarchy
        {
            get
            {
                return _DownloadHierarchy;
            }
            set
            {
                SetPropertyValue("DownloadHierarchy", ref _DownloadHierarchy, value);
            }
        }
      /*  private TaxonomicClassification _DownloadHierarchiesTo;
        public TaxonomicClassification DownloadHierarchiesTo
        {
            get
            {
                return _DownloadHierarchiesTo;
            }
            set
            {
                SetPropertyValue("DownloadHierarchiesTo", ref _DownloadHierarchiesTo, value);
            }
        }*/
    }

}
