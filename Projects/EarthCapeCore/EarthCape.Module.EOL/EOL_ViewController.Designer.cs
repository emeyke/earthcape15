namespace EarthCape.Module.EOL
{
    partial class EOL_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_EOL = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_EOL
            // 
            this.popupWindowShowAction_EOL.AcceptButtonCaption = null;
            this.popupWindowShowAction_EOL.CancelButtonCaption = null;
            this.popupWindowShowAction_EOL.Caption = "EOL";
            this.popupWindowShowAction_EOL.Category = "Tools";
            this.popupWindowShowAction_EOL.ConfirmationMessage = null;
            this.popupWindowShowAction_EOL.Id = "popupWindowShowAction_EOL";
            this.popupWindowShowAction_EOL.ImageName = "eol";
            this.popupWindowShowAction_EOL.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.popupWindowShowAction_EOL.Shortcut = null;
            this.popupWindowShowAction_EOL.Tag = null;
            this.popupWindowShowAction_EOL.TargetObjectsCriteria = null;
            this.popupWindowShowAction_EOL.TargetObjectType = typeof(EarthCape.Module.Core.TaxonomicName);
            this.popupWindowShowAction_EOL.TargetViewId = null;
            this.popupWindowShowAction_EOL.ToolTip = null;
            this.popupWindowShowAction_EOL.TypeOfView = null;
            this.popupWindowShowAction_EOL.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_EOL_Execute);
            this.popupWindowShowAction_EOL.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_EOL_CustomizePopupWindowParams);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_EOL;
    }
}
