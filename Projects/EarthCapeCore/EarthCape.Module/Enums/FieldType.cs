﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthCape.Module.Enums
{
    public enum FieldType
    {
        String,
        Number,
        Boolean,
        DateTime,
        TaxonomicName,
        Locality
    }
}
