﻿using System;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using EarthCape.Module.Core;
using System.Collections.ObjectModel;
using System.IO;
using System.Drawing;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Persistent.Base.General;

using DevExpress.Spreadsheet;
using DevExpress.XtraSpreadsheet;
using DevExpress.Persistent.BaseImpl;
using Xpand.Xpo;
using Xpand.Xpo.MetaData;
using Xpand.Persistent.Base.General;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;
using System.Linq;
using Xpand.Persistent.Base;
using DevExpress.ExpressApp.Model.Core;
using Xpand.Persistent.Base.RuntimeMembers.Model;
using DevExpress.ExpressApp.Model;
using DevExpress.Entity.Model;
using Xpand.Persistent.Base.RuntimeMembers;
using EarthCape.Module.GBIF;

namespace EarthCape.Module.Importer
{
 /*   public static class MDOExt
    {
        public static void ModifyModel(this ModelDifferenceObject modelDifferenceObject, Action<IModelApplication> action)
        {
            var xafApplication = ApplicationHelper.Instance.Application;
            var existingMembers = xafApplication.Model.BOModel.SelectMany(modelClass => modelClass.OwnMembers).OfType<IModelMemberEx>().ToArray();
            var modelApplicationBase = ((ModelApplicationBase)xafApplication.Model);
            var lastLayer = modelApplicationBase.LastLayer;
            ModelApplicationHelper.RemoveLayer(modelApplicationBase);
            var afterSetupLayer = modelApplicationBase.CreatorInstance.CreateModelApplication();
            afterSetupLayer.Id = "After Setup";
            ModelApplicationHelper.AddLayer(modelApplicationBase, afterSetupLayer);
            var mdoModel = modelDifferenceObject.GetModel(modelApplicationBase);
            ModelApplicationHelper.RemoveLayer(modelApplicationBase);
            var modelApplication = (IModelApplication)mdoModel;
            action(modelApplication);
            var modelMemberExs = modelApplication.BOModel.SelectMany(modelClass => modelClass.OwnMembers).OfType<IModelMemberEx>();
            var newMembers = modelMemberExs.Except(existingMembers).ToArray();
            if (newMembers.Any())
            {
                var objectSpaceProviders = xafApplication.ObjectSpaceProviders;
                var nonThreadSafeProviders = objectSpaceProviders.OfType<XPObjectSpaceProvider>().Any(provider => !provider.ThreadSafe);
                var xpandObjectSpaceProviders = objectSpaceProviders.OfType<XpandObjectSpaceProvider>().Any();
                if (!nonThreadSafeProviders && !xpandObjectSpaceProviders)
                    throw new ProviderNotSupportedException($"Use a non ThreadSafe {nameof(XPObjectSpaceProvider)} or the {nameof(XpandObjectSpaceProvider)}");
            }
            foreach (var modelMemberEx in newMembers)
            {
                if (string.IsNullOrEmpty(modelMemberEx.Id()))
                    modelMemberEx.SetValue("Id", "Test");
                modelMemberEx.SetValue("IsCustom", "True");
                modelMemberEx.CreatedAtDesignTime = false;
            }

            RuntimeMemberBuilder.CreateRuntimeMembers(modelApplication);
            ModelApplicationHelper.AddLayer(modelApplicationBase, lastLayer);
        }

    }
    */
    public class ImportHelper
    {

        public static void LoadDocument(out Worksheet sheet, out CellRange range, out Collection<string> filefields, out Collection<string> newfields, FileData file, Type oftype)
        {
            //SpreadsheetControl doc = new SpreadsheetControl();
            Workbook workbook = new Workbook();
          //  FileData file = importfile.File.FileStored;
            //IWorkbook workbook = doc.Document;
            try
            {
                MemoryStream stream = new MemoryStream();
                {
                    file.SaveToStream(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    if (Path.GetExtension(file.FileName) == ".xlsx")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.OpenXml);
                    if (Path.GetExtension(file.FileName) == ".xls")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Xls);
                    if (Path.GetExtension(file.FileName) == ".csv")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Csv);
                    if (Path.GetExtension(file.FileName) == ".txt")
                        workbook.LoadDocument(stream, DevExpress.Spreadsheet.DocumentFormat.Text);
                }
            }
            finally
            {
            }
            sheet = workbook.Worksheets[0];
            range = sheet.GetUsedRange();
            RowCollection rows = sheet.Rows;
            filefields = new Collection<string>();
            newfields = new Collection<string>();

            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(oftype);

            for (int i = 0; i <= range.ColumnCount - 1; i++)
            {
                Cell cell = sheet.Cells[0, i];
                filefields.Add(cell.Value.ToString());
                bool exists = false;
                foreach (XPMemberInfo member in classInfo.Members)
                {
                    if (member.Name == cell.Value.ToString())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                    newfields.Add(cell.Value.ToString());
            }
       }


        public static void DoImport(XafApplication application, string viewid, IObjectSpace os, DoWorkEventArgs e, Worksheet sheet, CellRange range, Collection<string> filefields, Type oftype,bool importGBIFTaxonomy)
        {
            int col = range.ColumnCount;
            bool hasExtraFields = false;
            if (filefields.IndexOf("Oid") == -1)
            {
                filefields.Add("Oid");
                sheet.Cells[0, col].SetValue("Oid");
                col = col + 1;
            }
            if (filefields.IndexOf("ImportedName") == -1)
            {
                filefields.Add("ImportedName");
                sheet.Cells[0, col].SetValue("ImportedName");
            }
            if (typeof(TaxonomicName).IsAssignableFrom(oftype))
            {
                hasExtraFields = ImportTaxonomicFields(os, e, sheet, range, filefields, oftype);
                hasExtraFields = false;
            }
            // if (typeof(Locality).IsAssignableFrom(dataObject.GetObjectType()))
            if (typeof(Locality).IsAssignableFrom(oftype))
            {
                hasExtraFields = ImportLocalityFields(os, e, sheet, range, filefields, oftype);
                hasExtraFields = true;
            }
            if (typeof(Unit).IsAssignableFrom(oftype))
            {
                hasExtraFields = true;
            }
            object keyValue = "";
            String keyField = "";
            //os.CommitChanges();
            for (int i = 1; i <= range.RowCount - 1; i++)
            {
                //break;
                FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "UnitID", "UserName", "Name", "Oid", "Code" });
                XPCustomObject obj = ImportRecord(os, keyValue, keyField, null, null, oftype, importGBIFTaxonomy);
                if (obj != null)
                {
                    ImportSimplePropertiesGeneric(os, obj, sheet, range, filefields, i);
                    ImportReferencePropertiesGeneric(os, obj, sheet, range, filefields, i, oftype, importGBIFTaxonomy);
                    ImportPropertiesSpecialized(os, obj, sheet, range, filefields, i);
                    //if (hasExtraFields)
                     //   ImportCustomData(application,viewid, os, obj, sheet, range, filefields, i, oftype);
                    obj.Save();
                    os.CommitChanges();
                }
            }
        }


        public static void FindKey(Worksheet sheet, Collection<string> filefields, ref object keyValue, ref String keyField, int i, string[] keys)
        {
            foreach (string key in keys)
                if (filefields.IndexOf(key) > -1)
                    if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(key)].Value.ToString()))
                    {
                        keyValue = sheet.Cells[i, filefields.IndexOf(key)].Value.ToObject().ToString();
                        keyField = key;
                        return;
                    }

            if (filefields.IndexOf("Oid") > -1)
                if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Oid")].Value.ToString()))
                {
                    keyValue = sheet.Cells[i, filefields.IndexOf("Oid")].Value.ToObject();
                    keyField = "Oid";
                    return;
                }

        }

        public static void ImportSimplePropertiesGeneric(IObjectSpace os, Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if (cell.Value != null)
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            if (member.ReferenceType == null)
                            {
                                try
                                {
                                    if (!cell.Value.IsEmpty)
                                    {
                                        if (typeof(String).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                if ((member.MappingFieldSize > -1) && (cell.Value.ToString().Length > member.MappingFieldSize))
                                                    throw new Exception("Maximum size: " + member.MappingFieldSize);
                                                else
                                                    member.SetValue(obj, cell.Value.ToObject().ToString());
                                        if (typeof(Boolean).IsAssignableFrom(memberType))
                                            member.SetValue(obj, cell.Value.BooleanValue);
                                        if (typeof(DateTime).IsAssignableFrom(memberType))
                                            if (cell.Value.DateTimeValue > DateTime.MinValue)
                                                member.SetValue(obj, cell.Value.DateTimeValue);
                                        if (typeof(Double).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, cell.Value.NumericValue);
                                        if ((typeof(int).IsAssignableFrom(memberType)) ||
                                            (typeof(System.Int16).IsAssignableFrom(memberType)) ||
                                            (typeof(System.Int32).IsAssignableFrom(memberType)))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, Convert.ToInt32(cell.Value.NumericValue));
                                        if (typeof(System.Int64).IsAssignableFrom(memberType))
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                                member.SetValue(obj, Convert.ToInt64(cell.Value.NumericValue));
                                        if (memberType.IsEnum)
                                        {
                                            if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                            {
                                                object enumValue = Enum.Parse(memberType, cell.Value.ToString());
                                                if (Enum.IsDefined(memberType, enumValue))
                                                    member.SetValue(obj, enumValue);
                                            }
                                        }
                                    }
                                    //else 
                                    //  member.SetValue(obj, null);                                 
                                }
                                catch (Exception ex)
                                {
                                    cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                                    sheet.Comments.Add(cell, ex.Message);
                                    cell.Fill.BackgroundColor = Color.Red;
                                }
                                finally
                                { }
                            }

                        }
                    }
            }
        }
        public static void ImportPropertiesSpecialized(IObjectSpace os, Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i)
        {
            //  MessageBox.Show("1");
            Cell cell = null;
            try
            {
                if (obj is UnitAttachment)
                {
                    cell = ImportAttachment(os, (UnitAttachment)obj, sheet, filefields, i, cell);
                }
            }
            catch (Exception ex)
            {
                cell = sheet.Cells[i, 0];
                sheet.Comments.Add(cell, ex.Message);
                cell.Fill.BackgroundColor = Color.Red;
            }
            finally
            { }   /*       if (obj is FileItemLinked)
            {
                cell = ImportFileItemLinked((FileItemLinked)obj, sheet, filefields, i, cell);
            }
        /*    if (obj is FileItemAzureBlob)
            {
                cell = ImportFileAzureBlob((FileItemAzureBlob)obj, sheet, filefields, i, cell);
            }*/
        }

        public static Cell ImportAttachment(IObjectSpace os, UnitAttachment file, Worksheet sheet, Collection<string> filefields, int i, Cell cell)
        {
            // MessageBox.Show("2");
            string filePath = "";
            string filename = "";
            string unitID = "";
            string localityName = "";
            //AttachmentStorage store = null;
            cell = sheet.Cells[i, filefields.IndexOf("FilePath")];
            if (cell.Value != null)
                filePath = cell.Value.TextValue;
            cell = sheet.Cells[i, filefields.IndexOf("FileName")];
            if (cell.Value != null)
                filename = cell.Value.TextValue;
            cell = sheet.Cells[i, filefields.IndexOf("Unit")];
            if (cell.Value != null)
                unitID = cell.Value.TextValue;
           /* cell = sheet.Cells[i, filefields.IndexOf("Storage")];
            if (cell.Value != null)
            {
                store = os.FindObject<AttachmentStorage>(new BinaryOperator("Name", cell.Value.TextValue));
            }*/
           /* if (store != null)
                using (FileStream fs = new FileStream(Path.Combine(filePath, filename), FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    if (GeneralHelper.IsRecognisedImageFile(filename))
                        file.Thumbnail = AttachmentHelper.CreateThumbnail(fs, 100, 100);// ResizeImage(Image.FromFile(filename), 100);
                    file.OriginalFileName = filename;
                    file.OriginalFilePath = filePath;
                   // file.Storage = store;
                    file.Save();
                    os.CommitChanges();
                    string Key = string.Format("{0:N}{1}", file.Oid, Path.GetExtension(file.OriginalFileName)).ToUpper();
                    file.Key = Key;
                    file.Save();
                    os.CommitChanges();
                    store.Upload(file, fs, Key);
                }*/
            file.Save();


            Unit unit = null;
            if (!String.IsNullOrEmpty(unitID))
                unit = os.FindObject<Unit>(new BinaryOperator("UnitID", unitID));
            Locality loc = null;
            if (!String.IsNullOrEmpty(localityName))
                loc = os.FindObject<Locality>(new BinaryOperator("Name", localityName));
            // MessageBox.Show(unitID + "/" + localityName);

            /*  if (loc != null)
              {
               //   MessageBox.Show("3");
                  LocalityFile f = os.CreateObject<LocalityFile>();
                  f.Locality = loc;
                  f.File = obj;
                  f.Save();
              }*/
            if (unit != null)
            {
                //  MessageBox.Show("4");
                UnitAttachment f = os.CreateObject<UnitAttachment>();
                f.Unit = unit;
                f.Save();
            }
            return cell;
        }


        /*       private static Cell ImportFileAzureBlob(FileItemAzureBlob obj, Worksheet sheet, Collection<string> filefields, int i, Cell cell)
               {
                   // MessageBox.Show("2");
                   string filePath = "";
                   string fileName = "";
                   string unitID = "";
                   string localityName = "";
                   cell = sheet.Cells[i, filefields.IndexOf("FilePath")];
                   if (cell.Value != null)
                       filePath = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("FileName")];
                   if (cell.Value != null)
                       fileName = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("LinkedUnitID")];
                   if (cell.Value != null)
                       unitID = cell.Value.TextValue;
                   cell = sheet.Cells[i, filefields.IndexOf("LinkedLocalityName")];
                   if (cell.Value != null)
                       localityName = cell.Value.TextValue;
                   string path = Path.Combine(filePath, fileName);
                   if (File.Exists(path))
                   {

                       using (var fileStream = File.OpenRead(path))
                       {
                           var data = os.CreateObject<FileDataAzureBlob>();

                           data.LoadFromStream(Path.GetFileName(path), fileStream);
                           data.FullName = path;
                           obj.File = data;
                       }
                       obj.Save();
                       Unit unit = null;
                       if (!String.IsNullOrEmpty(unitID))
                           unit = os.FindObject<Unit>(new BinaryOperator("UnitID", unitID));
                       Locality loc = null;
                       if (!String.IsNullOrEmpty(localityName))
                           loc = os.FindObject<Locality>(new BinaryOperator("Name", localityName));
                       // MessageBox.Show(unitID + "/" + localityName);

                       if (loc != null)
                       {
                           //   MessageBox.Show("3");
                           LocalityFile f = os.CreateObject<LocalityFile>();
                           f.Locality = loc;
                           f.File = obj;
                           f.Save();
                       }
                       if (unit != null)
                       {
                           //  MessageBox.Show("4");
                           UnitFile f = os.CreateObject<UnitFile>();
                           f.Unit = unit;
                           f.File = obj;
                           f.Save();
                       }
                   }

                   return cell;
               }
               */


     /*   public static void ImportCustomData(XafApplication application, string viewid, IObjectSpace os, Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i, Type oftype)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);

            foreach (string column in filefields)
            {
                if ((column != "Order1") && (column != "Genus1") && (column != "Species1"))
                {
                    bool stop = false;
                    foreach (XPMemberInfo member in classInfo.Members)
                    {
                        if (member.Name == column)
                        {
                            stop = true;
                            break;
                        }
                    }
                      if (!stop)
                     {
                        Column col = sheet.Columns[filefields.IndexOf(column)];
                        col.
                         cell = sheet.Cells[i, filefields.IndexOf(column)];
                        if ((cell.Value != null) && (!string.IsNullOrEmpty(cell.Value.ToString())))
                            try
                            {
                                switch (cell.Value.Type)
                                {
                                    case CellValueType.Text:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(string));
                                        break;
                                    case CellValueType.Boolean:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(bool));
                                        break;
                                    case CellValueType.DateTime:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(DateTime));
                                        break;
                                    case CellValueType.Numeric:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(Double));
                                        break;
                                    case CellValueType.Unknown:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(string));
                                        break;
                                    case CellValueType.None:
                                        CreateField(application, viewid, os, column, column, oftype, typeof(string));
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                cell = sheet.Cells[i, filefields.IndexOf(column)];
                                sheet.Comments.Add(cell, ex.Message);
                                cell.Fill.BackgroundColor = Color.Red;
                            }
                            finally
                            { }

                     }
                             
                }
            }
        }
        */
        public static void CreateNewFields(XafApplication application, string viewid, IObjectSpace os ,Type oftype, Worksheet sheet, Collection<string> filefields)
        {
       
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(oftype);

            foreach (string column in filefields)
            {
                if ((column != "Order1") && (column != "Genus1") && (column != "Species1"))
                {
                    bool stop = false;
                    foreach (XPMemberInfo member in classInfo.Members)
                    {
                        if (member.Name == column)
                        {
                            stop = true;
                            break;
                        }
                    }
                    if (!stop)
                    {
                           try
                            {
                                CreateField(application, viewid, os, column, column, oftype, typeof(string));
                            }
                            catch (Exception ex)
                            {
                                cell = sheet.Cells[0, filefields.IndexOf(column)];
                                sheet.Comments.Add(cell, ex.Message);
                                cell.Fill.BackgroundColor = Color.Red;
                            }
                            finally
                            { }

                    }

                }
            }
        }

        public static bool CreateField(XafApplication application,string viewid, IObjectSpace objectSpace, string column, string column_caption, Type oftype, Type coltype)
        {

            if (XafTypesInfo.Instance.FindTypeInfo(oftype).FindMember(column) != null)
            {
                return false;
            }
            var modelDifferenceObject = objectSpace.FindObject<ModelDifferenceObject>(new BinaryOperator("Name", application.Title));
            if (modelDifferenceObject == null) return false;
            modelDifferenceObject.ModifyModel(modelDifferenceObject, modelApplication => CreateNewMember(application, oftype, column, column_caption, coltype, modelApplication, modelDifferenceObject));
            modelDifferenceObject.Save();

            objectSpace.CommitChanges();
            return true;

        }

        public static IModelMemberPersistent CreateNewMember(XafApplication application, Type type, string column, string column_caption, Type coltype,IModelApplication modelApplication, ModelDifferenceObject modelDifferenceObject)
        {
            var modelListView =(IModelListView)modelApplication.Views[application.FindListViewId(type)];
            //create member
            var memberPersistent = modelListView.ModelClass.OwnMembers.AddNode<IModelMemberPersistent>();
            memberPersistent.Type = coltype;
            memberPersistent.Name = column;
            memberPersistent.Caption = column_caption;
            //create column
            var modelColumn = modelListView.Columns.AddNode<IModelColumn>();
            modelColumn.PropertyName = column;
            modelColumn.Caption = column_caption;
            modelColumn.Index = modelListView.Columns.Count;
            modelDifferenceObject.XmlContent = modelApplication.Xml();
            return memberPersistent;
        }

        public static void ImportReferencePropertiesGeneric(IObjectSpace os, Object obj, Worksheet sheet, CellRange range, Collection<string> filefields, int i, Type oftype,bool importGBIFTaxonomy)
        {
            Cell cell = null;
            XPDictionary xpDictionary = XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary;
            XPClassInfo classInfo = xpDictionary.GetClassInfo(obj);
            foreach (XPMemberInfo member in classInfo.Members)
            {
                if (member.IsPersistent)
                    if (filefields.IndexOf(member.Name) > -1)
                    {
                        cell = sheet.Cells[i, filefields.IndexOf(member.Name)];
                        if ((cell.Value != null) && (cell.Value.ToObject() != null) && (!string.IsNullOrEmpty(cell.Value.ToObject().ToString())))
                        {
                            Type memberType = member.MemberType;
                            if (Nullable.GetUnderlyingType(member.MemberType) != null)
                                memberType = Nullable.GetUnderlyingType(member.MemberType);
                            if (member.ReferenceType != null)
                            {
                                try
                                {
                                    XPCustomObject imported = ImportRecord(os, cell.Value.ToObject().ToString(), "", member, obj, oftype, importGBIFTaxonomy);
                                    member.SetValue(obj, imported);
                                }
                                catch (Exception ex)
                                {
                                    sheet.Comments.Add(cell, ex.Message);
                                    cell.Fill.BackgroundColor = Color.Red;
                                }
                                finally
                                { }
                            }
                        }
                    }
            }
        }


        public static XPCustomObject ImportRecord(IObjectSpace os, object keyValue, string keyField, XPMemberInfo referencingMember, object ReferencingObject, Type oftype, bool importGBIFTaxonomy)
        {
            XPCustomObject newObject = null;
            Type type;
            //   string keyField="";
            if (referencingMember != null)
                type = referencingMember.MemberType;
            else
                type = oftype;
            CriteriaOperator cr = null;
            if (!String.IsNullOrEmpty((string)keyValue))
            {
                if (keyField == "Oid")
                    cr = new BinaryOperator("Oid", keyValue);
                else
                    if (!String.IsNullOrEmpty(keyField))
                    cr = new BinaryOperator(keyField, keyValue);
                else
                    cr = GetKeyCriteria(type, keyValue, out keyField);
                newObject = (XPCustomObject)os.FindObject(type, cr);
            }
            if ((ReferencingObject != null) && (referencingMember != null) && (newObject != null))
                referencingMember.SetValue(ReferencingObject, newObject);
            else
                if (newObject == null)
            {
                if ((typeof(TaxonomicName).IsAssignableFrom(type)) && (!typeof(HigherTaxon).IsAssignableFrom(type)) && (!typeof(SubspecificTaxon).IsAssignableFrom(type)))
                    newObject = os.CreateObject<Species>();
                else
                    if ((typeof(TaxonomicName).IsAssignableFrom(type)))
                    newObject = (XPCustomObject)os.CreateObject(SecuritySystem.UserType);
                else
                    newObject = (XPCustomObject)os.CreateObject(type);
                if ((!string.IsNullOrEmpty(keyField)) && (keyValue != null))
                {
                    if (typeof(PersonCore).IsAssignableFrom(type))
                    {
                        if (((string)keyValue).Contains(" "))
                        {
                            string fname = ((string)keyValue).Split(new string[] { " " }, 2, StringSplitOptions.None)[0].Trim();
                            string lname = ((string)keyValue).Split(new string[] { " " }, 2, StringSplitOptions.None)[1].Trim();
                            ((PersonCore)newObject).FirstName = fname;
                            ((PersonCore)newObject).LastName = lname;
                        }
                        else
                            ((PersonCore)newObject).FirstName = ((string)keyValue);
                    }
                    else
                        newObject.SetMemberValue(keyField, keyValue);
                }
                newObject.Save();
                 os.CommitChanges();
                if ((typeof(TaxonomicName).IsAssignableFrom(newObject.GetType())) && importGBIFTaxonomy)
                {
                    PublishHelper.GBIFTaxonomy((TaxonomicName)newObject, os);
                    PublishHelper.UpdateNcbi((TaxonomicName)newObject, os);
                }
            }
            return newObject;
        }

        public static CriteriaOperator GetKeyCriteria(Type type, object keyValue, out string keyField)
        {
            CriteriaOperator cr = null;
            keyField = "";

            if (typeof(TaxonomicName).IsAssignableFrom(type))
            {
                cr = CriteriaOperator.Or(new BinaryOperator("ShortName", keyValue), new BinaryOperator("FullName", keyValue));
                keyField = "Name";
                return cr;
            }
                   /*if (typeof(PersonCore).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("FullName", keyValue);
                keyField = "FullName";
                return cr;
            }*/

            if (typeof(Unit).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("UnitID", keyValue);
                keyField = "UnitID";
                return cr;
            }
            if ((typeof(ICode).IsAssignableFrom(type)) && (typeof(IName).IsAssignableFrom(type)))
            {
                cr = CriteriaOperator.Or(new BinaryOperator("Name", keyValue), new BinaryOperator("Code", keyValue));
                keyField = "Name";
                return cr;
            }
            if (typeof(ICode).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("Code", keyValue);
                keyField = "Code";
                return cr;
            }
            if (typeof(IName).IsAssignableFrom(type))
            {
                cr = new BinaryOperator("Name", keyValue);
                keyField = "Name";
                return cr;
            }


            return null;
        }







        public static bool ImportTaxonomicFields(IObjectSpace os, DoWorkEventArgs e, Worksheet sheet, CellRange range, Collection<string> filefields, Type oftype)
        {
            List<string> supportedCategories = new List<string>(new string[]
    {
        "Kingdom",
        "Phylum",
        "Class",
        "Order",
        "Family",
        "Subfamily",
        "Genus",
        "Species",
        "Subspecies",
        "Variety",
        "Form"
    });
            // string parentColumn = "";
            // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
            // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
            foreach (string category in supportedCategories)
            {
                if (filefields.IndexOf(category) > -1)
                {
                    GetTaxonomicColumn(os, e, sheet, category, oftype, range, filefields, supportedCategories);
                    os.CommitChanges();
                }
            }
            if (filefields.IndexOf("Order") > -1)
                filefields[filefields.IndexOf("Order")] = "Order1";
            if (filefields.IndexOf("Genus") > -1)
                filefields[filefields.IndexOf("Genus")] = "Genus1";
            if (filefields.IndexOf("Species") > -1)
                filefields[filefields.IndexOf("Species")] = "Species1";


            //checking for extra fields apart from hierarcgical ones
            return CheckExtraFields(filefields, supportedCategories);

        }

        private static bool ImportLocalityFields(IObjectSpace os, DoWorkEventArgs e, Worksheet sheet1, CellRange range, Collection<string> filefields, Type oftype)
        {
            List<string> supportedCategories = new List<string>(new string[]
     {
        "Continent",
        "Country",
        "Admin1",
        "Admin2",
        "Admin3",
        "Admin4",
        "Name",
    });
            Worksheet sheet = sheet1.Workbook.Worksheets.Add();
            sheet.CopyFrom(sheet1);
            sheet.Name = "Hierarchy import";
            int col = range.ColumnCount;
            if (filefields.IndexOf("Oid") == -1)
            {
                filefields.Add("Oid");
                sheet.Cells[0, col].SetValue("Oid");
                col = col + 1;
            }


            // string parentColumn = "";
            // sheet.Cells[0, range.ColumnCount+1].SetValue("ImportedName");
            // sheet.Cells[0, range.ColumnCount].SetValue("Oid");
            foreach (string category in supportedCategories)
            {
                if (filefields.IndexOf(category) > -1)
                {
                    GetLocalityColumn(os, e, sheet, sheet1, category, oftype, range, filefields, supportedCategories);
                    //  os.CommitChanges();
                }
            }
            os.CommitChanges();

            //checking for extra fields apart from hierarcgical ones
            return CheckExtraFields(filefields, supportedCategories);
        }

        private static bool CheckExtraFields(Collection<string> filefields, List<string> supportedCategories)
        {
            foreach (string filefield in filefields)
            {
                if ((filefield != "Code") && (filefield != "ShortName") && (filefield != "Oid") && (filefield != "ImportedName") && (filefield != "Order1") && (filefield != "Genus1") && (filefield != "Species1"))
                    if (supportedCategories.IndexOf(filefield) == -1)
                        return true;
            }
            return false;
        }

        private static ITreeNode parent = null;
        private static void GetTaxonomicColumn(IObjectSpace os, DoWorkEventArgs e, Worksheet sheet, string category, Type oftype, CellRange range, Collection<string> filefields, List<string> supportedCategories)
        {
            bool keyFieldIsCode = false;

            if (filefields.IndexOf(category) > -1)
            {
                string current = "";
                Guid parentOid = Guid.Empty;
                Guid currentOid = Guid.Empty;
                TaxonomicName taxon = null;
                for (int i = 1; i < range.RowCount; i++)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(category)].Value.ToString()))
                        {
                            string parentColumn = GetParentColumn(sheet, category, i, supportedCategories, filefields);
                            string childColumn = GetChildColumn(sheet, category, i, supportedCategories, filefields);
                            if ((sheet.Cells[i, filefields.IndexOf(category)].Value.ToString() != current) || ((!string.IsNullOrEmpty(parentColumn)) && (sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString() != parentOid.ToString())))
                            {
                                string shortName = "";
                                if ((filefields.IndexOf("ShortName") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString())))
                                    shortName = sheet.Cells[i, filefields.IndexOf("ShortName")].Value.ToString();
                                parentOid = Guid.Empty;
                                current = sheet.Cells[i, filefields.IndexOf(category)].Value.ToString();
                                string keyField = "";
                                object keyValue = null;
                                if (!string.IsNullOrEmpty(parentColumn))
                                    parentOid = Guid.Parse(sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString());
                                if (!string.IsNullOrEmpty(childColumn))
                                {
                                    keyField = "Name";
                                    keyValue = current;
                                    taxon = GetTaxon(os, keyField, keyValue, category, "", current, parentOid, oftype, false);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(shortName))
                                    {
                                        keyField = "ShortName";
                                        keyValue = shortName;
                                        keyFieldIsCode = true;
                                    }
                                    else
                                    {
                                        keyField = "Name";
                                        keyValue = current;
                                        keyFieldIsCode = false;
                                    }
                                    //  FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "Oid", "Name", "ShortName" });
                                    taxon = GetTaxon(os, keyField, keyValue, category, shortName, current, parentOid, oftype, keyFieldIsCode);
                                }
                                if (taxon != null)
                                {
                                    parent = taxon;
                                    currentOid = taxon.Oid;
                                }
                            }
                            sheet.Cells[i, filefields.IndexOf(category)].SetValue(taxon.Oid.ToString());
                            if (string.IsNullOrEmpty(childColumn))
                            {
                                if ((String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString())) || IsLower(supportedCategories, category, sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString()))
                                {
                                    sheet.Cells[i, filefields.IndexOf("ImportedName")].SetValue(category);
                                    sheet.Cells[i, filefields.IndexOf("Oid")].SetValue(taxon.Oid.ToString());
                                }
                            }


                        }
                        else
                        {
                        }
                    }
                    catch (Exception ex)
                    {
                        Cell cell = sheet.Cells[i, filefields.IndexOf(category)];
                        sheet.Comments.Add(cell, SecuritySystem.CurrentUserName, ex.Message);
                        cell.Fill.BackgroundColor = Color.Red;
                    }
                }
            }
        }

        private static Locality parentLoc = null;
        private static void GetLocalityColumn(IObjectSpace os, DoWorkEventArgs e, Worksheet sheet, Worksheet sheetOriginal, string category, Type oftype, CellRange range, Collection<string> filefields, List<string> supportedCategories)
        {
            if (filefields.IndexOf(category) > -1)
            {
                string current = "";
                Guid parentOid = Guid.Empty;
                Guid currentOid = Guid.Empty;
                Locality loc = null;
                for (int i = 1; i < range.RowCount; i++)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf(category)].Value.ToString()))
                        {
                            string parentColumn = GetParentColumn(sheet, category, i, supportedCategories, filefields);
                            string childColumn = GetChildColumn(sheet, category, i, supportedCategories, filefields);
                            if ((sheet.Cells[i, filefields.IndexOf(category)].Value.ToString() != current) || ((!string.IsNullOrEmpty(parentColumn)) && (sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString() != parentOid.ToString())))
                            {
                                string Code = "";
                                parentOid = Guid.Empty;
                                current = sheet.Cells[i, filefields.IndexOf(category)].Value.ToString();
                                if (String.IsNullOrEmpty(childColumn))
                                    if ((filefields.IndexOf("Code") > -1) && (!String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString())))
                                        Code = sheet.Cells[i, filefields.IndexOf("Code")].Value.ToString();
                                //string keyField = "";
                                //object keyValue = null;
                                if (!string.IsNullOrEmpty(parentColumn))
                                    parentOid = Guid.Parse(sheet.Cells[i, filefields.IndexOf(parentColumn)].Value.ToString());
                                //FindKey(sheet, filefields, ref keyValue, ref keyField, i, new string[] { "Oid", "Name", "Code" });
                                loc = GetLocality(os, "Name", current, category, Code, current, parentOid, oftype);
                                if (loc != null)
                                {
                                    parentLoc = loc;
                                    currentOid = loc.Oid;
                                }
                            }
                            sheet.Cells[i, filefields.IndexOf(category)].SetValue(loc.Oid.ToString());
                            if ((String.IsNullOrEmpty(sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString())) || IsLower(supportedCategories, category, sheet.Cells[i, filefields.IndexOf("ImportedName")].Value.ToString()))
                            {
                                sheet.Cells[i, filefields.IndexOf("ImportedName")].SetValue(category);
                                sheet.Cells[i, filefields.IndexOf("Oid")].SetValue(loc.Oid.ToString());
                                sheetOriginal.Cells[i, filefields.IndexOf("Oid")].SetValue(loc.Oid.ToString());
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        Cell cell = sheet.Cells[i, filefields.IndexOf(category)];
                        string auth = SecuritySystem.CurrentUserName;
                        if (auth == "") auth = "no user";
                        sheet.Comments.Add(cell, auth, ex.Message);
                        cell.Fill.BackgroundColor = Color.Red;

                    }
                }
            }
        }
        private static bool IsLower(List<string> supportedCategories, string category, string p)
        {
            return supportedCategories.IndexOf(category) > supportedCategories.IndexOf(p);
        }

        private static string GetParentColumn(Worksheet sheet, string p, int row, List<string> supportedCategories, Collection<string> filefields)
        {
            int colIndex = -1;
            int i = supportedCategories.IndexOf(p) - 1;
            while ((colIndex == -1) && (i > -1))
            {
                if (filefields.IndexOf(supportedCategories[i]) > -1)
                    if ((!string.IsNullOrEmpty(sheet.Cells[row, filefields.IndexOf(supportedCategories[i])].Value.ToString())))
                        colIndex = filefields.IndexOf(supportedCategories[i]);
                i = i - 1;
            }
            if (colIndex == -1)
                return "";
            return filefields[colIndex];
        }
        private static string GetChildColumn(Worksheet sheet, string p, int row, List<string> supportedCategories, Collection<string> filefields)
        {
            int colIndex = -1;
            int i = supportedCategories.IndexOf(p) + 1;
            while ((colIndex == -1) && (i < supportedCategories.Count))
            {
                if (filefields.IndexOf(supportedCategories[i]) > -1)
                    if ((!string.IsNullOrEmpty(sheet.Cells[row, filefields.IndexOf(supportedCategories[i])].Value.ToString())))
                        colIndex = filefields.IndexOf(supportedCategories[i]);
                i = i + 1;
            }
            if (colIndex == -1)
                return "";
            return filefields[colIndex];
        }



        private static TaxonomicName GetTaxon(IObjectSpace os, string keyField, object keyValue, string categoryName, string shortName, string name, Guid parentOid, Type oftype, bool keyFieldIsCode)
        {
            TaxonomicName taxon = null;
            TaxonomicName parentTaxon = null;
            CriteriaOperator cr = null;
            CriteriaOperator crparent = null;
            Guid projectOid = Guid.Empty;
            if (!parentOid.Equals(Guid.Empty))
            {
                crparent = new BinaryOperator("Parent", parentOid);
                parentTaxon = os.FindObject<TaxonomicName>(new BinaryOperator("Oid", parentOid));
            }
            if (keyFieldIsCode)
                cr = new BinaryOperator(keyField, keyValue);
            else
                cr = CriteriaOperator.Or(
                   CriteriaOperator.And(crparent, new BinaryOperator(keyField, keyValue)),
                   CriteriaOperator.And(new NullOperator("Parent"), new BinaryOperator(keyField, keyValue)) //note NullOperator removes "authorName" error when importing
                   );
            taxon = os.FindObject<TaxonomicName>(cr);
            if ((taxon == null) && (keyField == "ShortName") && (!string.IsNullOrEmpty(name)))
            {
                cr = CriteriaOperator.And(crparent, new BinaryOperator("ShortName", ""), new BinaryOperator("Name", name));
                taxon = os.FindObject<TaxonomicName>(cr);
                if (taxon != null)
                {
                    taxon.ShortName = shortName;
                    taxon.Save();
                    os.CommitChanges();
                }
            }
            if (taxon != null)
            {
                taxon.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,os,null, categoryName);
                if (!string.IsNullOrEmpty(name))
                    taxon.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    taxon.ShortName = shortName;
                if (parentTaxon != null)
                    taxon.Parent = parentTaxon;
                if ((taxon is Species) && (parentTaxon != null) && (parentTaxon is HigherTaxon))
                    ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                if ((taxon is SubspecificTaxon) && (parentTaxon != null) && (parentTaxon is Species))
                    ((SubspecificTaxon)taxon).Species = (Species)parentTaxon;
                taxon.Save();
                os.CommitChanges();
            }
            if (taxon == null)
            {
                if (categoryName == "Genus")
                    taxon = os.CreateObject<Genus>();
                else
                    if (categoryName == "Species")
                {
                    taxon = os.CreateObject<Species>();
                    if (parentTaxon is HigherTaxon)
                        ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                    if (parentTaxon is Genus)
                        ((Species)taxon).Genus = (Genus)parentTaxon;
                }
                else
                        if (categoryName == "Family")
                {
                    taxon = os.CreateObject<Family>();
                }
                else
                            if ((categoryName == "Subspecies") || (categoryName == "Variety") || (categoryName == "Form"))
                {
                    taxon = os.CreateObject<SubspecificTaxon>();
                }
                else
                {
                    taxon = os.CreateObject<HigherTaxon>();
                }
                taxon.SetMemberValue(keyField, keyValue);
                taxon.TaxonomicCategory = GeneralHelper.GetTaxonomicCategory(null,os,null, categoryName);
                if (!string.IsNullOrEmpty(name))
                    taxon.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    taxon.ShortName = shortName;
                if (parentTaxon != null)
                    taxon.Parent = parentTaxon;
                if ((taxon is Species) && (parentTaxon != null) && (parentTaxon is HigherTaxon))
                    ((Species)taxon).Category = (HigherTaxon)parentTaxon;
                if ((taxon is SubspecificTaxon) && (parentTaxon != null) && (parentTaxon is Species))
                    ((SubspecificTaxon)taxon).Species = (Species)parentTaxon;
                taxon.Save();
                os.CommitChanges();
            }
            return taxon;
        }

        private static Locality GetLocality(IObjectSpace os, string keyField, object keyValue, string categoryName, string shortName, string name, Guid parentOid, Type oftype)
        {
            Locality loc = null;
            Locality parentTaxon = null;
            CriteriaOperator cr = null;
            Guid projectOid = Guid.Empty;
            if (!parentOid.Equals(Guid.Empty))
            {
                //cr =CriteriaOperator.Or(new BinaryOperator("Parent", parentOid),new BinaryOperator("Category", parentOid));
                cr = new BinaryOperator("Category", parentOid);
                parentTaxon = os.FindObject<Locality>(CriteriaOperator.And(new BinaryOperator("Oid", parentOid)));
                cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue));
                loc = os.FindObject<Locality>(cr);
                if (loc == null) //covering for existing loc but with no category
                {
                    cr = CriteriaOperator.And(
                        CriteriaOperator.Parse("Category is null"),
                        !CriteriaOperator.Or(
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm).FullName)/*,
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm1).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm2).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm3).FullName),
                            CriteriaOperator.Parse("IsInstanceOfType(This, ?)", typeof(LocalityAdm4).FullName)*/
                        ),
                        new BinaryOperator(keyField, keyValue));
                    loc = os.FindObject<Locality>(cr);
                }
                if (loc == null)
                {
                    cr = new BinaryOperator("Parent", parentOid);
                    cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue));
                    loc = os.FindObject<LocalityAdm>(cr);
                }
                //this was causing bug reassigning root items
                /* if (loc == null) //covering for existing loc but with no category
                 {
                     cr = CriteriaOperator.Parse("Parent is null");
                     cr = CriteriaOperator.And(cr, new BinaryOperator(keyField, keyValue), GetGeneralImportCriteria(oftype));
                     loc = os.FindObject<LocalityAdm>(cr);
                 }*/
                if ((loc != null) && (parentTaxon != null))
                {
                    if (typeof(LocalityAdm).IsAssignableFrom(loc.GetType()))
                    {
                        if ((((LocalityAdm)loc).Parent == null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                        {
                            ((LocalityAdm)loc).Parent = (LocalityAdm)parentTaxon;
                            loc.Save();
                            os.CommitChanges();
                        }
                    }
                    if (!typeof(LocalityAdm).IsAssignableFrom(loc.GetType()))
                    {
                        if (((loc).Category == null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                        {
                            loc.Category = (LocalityAdm)parentTaxon;
                            loc.Save();
                            os.CommitChanges();
                        }
                    }
                }
            }
            else
            {
                cr = new BinaryOperator(keyField, keyValue);
                loc = os.FindObject<Locality>(cr);
            }
            if (loc == null)
            {
                if (
                      (categoryName == "Continent")
                    || (categoryName == "Country")
                    || (categoryName == "Admin1")
                    || (categoryName == "Admin2")
                    || (categoryName == "Admin3")
                    || (categoryName == "Admin4"))
                    loc = os.CreateObject<LocalityAdm>();
                else
                    loc = os.CreateObject<Locality>();
                loc.SetMemberValue(keyField, keyValue);
                if (!string.IsNullOrEmpty(name))
                    loc.Name = name;
                if (!string.IsNullOrEmpty(shortName))
                    loc.Code = shortName;
                if ((loc is Locality) && (parentTaxon != null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())) && (!typeof(LocalityAdm).IsAssignableFrom(loc.GetType())))
                    ((Locality)loc).Category = (LocalityAdm)parentTaxon;
                if ((loc is LocalityAdm) && (parentTaxon != null) && (typeof(LocalityAdm).IsAssignableFrom(parentTaxon.GetType())))
                    ((LocalityAdm)loc).Parent = (LocalityAdm)parentTaxon;
                loc.Save();
                os.CommitChanges();
            }
            return loc;
        }


        private static Locality CreateAdm(IObjectSpace os, Worksheet sheet, Collection<string> filefields, int i, CriteriaOperator cr, LocalityAdm parent, string fieldName)
        {
            Locality Admin = null;
            if ((filefields.IndexOf(fieldName) > -1) && (!sheet.Cells[i, filefields.IndexOf(fieldName)].Value.Equals(null)))
            {
                string AdminVal = sheet.Cells[i, filefields.IndexOf(fieldName)].Value.ToString();
                Admin = os.FindObject<Locality>(CriteriaOperator.And(new BinaryOperator("Name", AdminVal), cr));
                //  if ((Admin != null) && (fieldName == "Country") && (Admin.GetType() != typeof(Country))) throw new Exception("Cannot create Country as object already exists with the same name but different type");
                // if ((Admin != null) && (fieldName == "Continent") && (Admin.GetType() != typeof(Continent))) throw new Exception("Cannot create Continent as object already exists with the same name but different type");
                if ((Admin != null) && (parent != null))
                {
                    if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                        if (((LocalityAdm)Admin).Parent.Oid != parent.Oid)
                            Admin = null;
                }
                if (Admin == null)
                {
                    /* if (fieldName == "Continent")
                         Admin = os.CreateObject<Continent>();
                     else
                         if (fieldName == "Country")
                             Admin = os.CreateObject<Country>();
                         else
                             Admin = os.CreateObject<LocalityAdm>();*/
                    Admin.Name = AdminVal;
                    if (parent != null)
                        if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                            ((LocalityAdm)Admin).Parent = parent;
                    Admin.Save();
                }
                if (typeof(LocalityAdm).IsAssignableFrom(Admin.GetType()))
                    parent = (LocalityAdm)Admin;
            }
            return Admin;
        }
    }
}