﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using EarthCape.Module.Core;
using EarthCape.Module.Importer;
using EarthCape.Module.Enums;

namespace EarthCape.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FieldManagement_ViewController : ViewController
    {
        public FieldManagement_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if ((popupWindowShowAction_AddField.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_AddField.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new DevExpress.ExpressApp.Security.PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, DevExpress.ExpressApp.Security.SecurityOperations.Write)));
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_AddField_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            AddFieldOptions options = os.CreateObject<AddFieldOptions>();
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;
        }

        Type gettype(FieldType type)
        {
            switch (type)
            {
                case FieldType.String:
                    return typeof(System.String);
                case FieldType.Boolean:
                    return typeof(System.Boolean);
                case FieldType.DateTime:
                    return typeof(System.DateTime);
                case FieldType.Number:
                    return typeof(System.Int32);
                case FieldType.TaxonomicName:
                    return typeof(TaxonomicName);
                case FieldType.Locality:
                    return typeof(Locality);
            }
            return null;
        }
        private void popupWindowShowAction_AddField_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            View view = Application.CreateListView(os, View.ObjectTypeInfo.Type, true);
            Frame.SetView(null);
            AddFieldOptions options = ((AddFieldOptions)(e.PopupWindowViewCurrentObject));
            Type type = gettype(options.Type);
            try
            {
                bool res=ImportHelper.CreateField(Application, view.Id, os, options.Name, options.Caption, view.ObjectTypeInfo.Type, type);
                Frame.SetView(view);
                if (res)
                {
                    MessageOptions options1 = new MessageOptions();
                    options1.Duration = 5000;
                    options1.Message = string.Format("Field: {0} created.", options.Name);
                    options1.Type = InformationType.Success;
                    options1.Web.Position = InformationPosition.Top;
                    options1.Win.Caption = "Field created";
                    options1.Win.Type = WinMessageType.Toast;
                    Application.ShowViewStrategy.ShowMessage(options1);
                }
                else
                {
                    MessageOptions options1 = new MessageOptions();
                    options1.Duration = 5000;
                    options1.Message = string.Format("Field {0} already exists.", options.Name);
                    options1.Type = InformationType.Info;
                    options1.Web.Position = InformationPosition.Top;
                    options1.Win.Caption = "Field exists";
                    options1.Win.Type = WinMessageType.Toast;
                    Application.ShowViewStrategy.ShowMessage(options1);
                }
            }
            catch (Exception ex)
            {
                MessageOptions options1 = new MessageOptions();
                options1.Duration = 5000;
                options1.Message = string.Format("Something went wrong while creating {0} field. Error message:{1}", options.Name, ex.Message);
                options1.Type = InformationType.Error;
                options1.Web.Position = InformationPosition.Top;
                options1.Win.Caption = "Field creation error";
                options1.Win.Type = WinMessageType.Toast;
                Application.ShowViewStrategy.ShowMessage(options1);
            }
        }
    }
}
