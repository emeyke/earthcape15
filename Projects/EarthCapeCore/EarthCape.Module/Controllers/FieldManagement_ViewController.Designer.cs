﻿namespace EarthCape.Module.Controllers
{
    partial class FieldManagement_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_AddField = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_AddField
            // 
            this.popupWindowShowAction_AddField.AcceptButtonCaption = null;
            this.popupWindowShowAction_AddField.CancelButtonCaption = null;
            this.popupWindowShowAction_AddField.Category = "Tools";
            this.popupWindowShowAction_AddField.Caption = "Create field";
            this.popupWindowShowAction_AddField.ConfirmationMessage = null;
            this.popupWindowShowAction_AddField.Id = "popupWindowShowAction_AddField";
            this.popupWindowShowAction_AddField.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_AddField.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_AddField.ToolTip = null;
            this.popupWindowShowAction_AddField.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_AddField.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_AddField_CustomizePopupWindowParams);
            this.popupWindowShowAction_AddField.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_AddField_Execute);
            // 
            // FieldManagement_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_AddField);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_AddField;
    }
}
