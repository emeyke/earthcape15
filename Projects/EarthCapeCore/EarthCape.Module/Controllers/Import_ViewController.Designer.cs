﻿namespace EarthCape.Module.Core.Controllers
{
    partial class Import_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupWindowShowAction_Import = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupWindowShowAction_Import
            // 
            this.popupWindowShowAction_Import.AcceptButtonCaption = null;
            this.popupWindowShowAction_Import.CancelButtonCaption = null;
            this.popupWindowShowAction_Import.Caption = "Import";
            this.popupWindowShowAction_Import.Category = "Tools";
            this.popupWindowShowAction_Import.ConfirmationMessage = null;
            this.popupWindowShowAction_Import.Id = "popupWindowShowAction_Import";
            this.popupWindowShowAction_Import.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.popupWindowShowAction_Import.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupWindowShowAction_Import.ToolTip = "Imoport spreadsheet data for a currently opened data view";
            this.popupWindowShowAction_Import.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupWindowShowAction_Import.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupWindowShowAction_Import_CustomizePopupWindowParams);
            this.popupWindowShowAction_Import.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupWindowShowAction_Import_Execute);
            // 
            // Import_ViewController
            // 
            this.Actions.Add(this.popupWindowShowAction_Import);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupWindowShowAction_Import;
    }
}
