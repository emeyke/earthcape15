﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Spreadsheet;
using System.Collections.ObjectModel;
using EarthCape.Module.Importer;
using DevExpress.ExpressApp.Security;
using DevExpress.Persistent.AuditTrail;

namespace EarthCape.Module.Core.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Import_ViewController : ViewController
    {
        public Import_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            ViewControlsCreated += Import_ViewController_ViewControlsCreated;
        }

        private void Import_ViewController_ViewControlsCreated(object sender, EventArgs e)
        {
            if ((popupWindowShowAction_Import.Active) && (View.ObjectTypeInfo != null))
            {
                popupWindowShowAction_Import.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, View.ObjectTypeInfo.Type, SecurityOperations.Write)));
            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupWindowShowAction_Import_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            /*
             *https://documentation.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.ShowViewStrategyBase.ShowMessage.method(24cbQw)
             * 
             *   MessageOptions options = new MessageOptions();
              options.Duration = 2000;
              options.Message = "Press";
              options.Type = InformationType.co;
              options.Web.ToastConfiguration.
              options.Web.Position = InformationPosition.Right;
              options.Win.Caption = "Success";
              options.Win.Type = WinMessageType.Flyout;
              Application.ShowViewStrategy.ShowMessage(options);*/

            IObjectSpace os = Application.CreateObjectSpace();
            ImportFileObject options = os.CreateObject<ImportFileObject>();
            options.ImportGBIFTaxonomy = false;
            DetailView dv = Application.CreateDetailView(os, options);
            dv.ViewEditMode = ViewEditMode.Edit;
            e.View = dv;

        }
        void Instance_SaveAuditTrailData(object sender, SaveAuditTrailDataEventArgs e)
        {
            e.Handled = true;
        }

        private void popupWindowShowAction_Import_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ImportFileObject file = ((ImportFileObject)(e.PopupWindowViewCurrentObject));
          //  file.Save();
           // e.PopupWindowView.ObjectSpace.CommitChanges();
            Worksheet sheet = null;
            CellRange range = null;
            Collection<string> filefields;
            Collection<string> newfields;
            IObjectSpace os = Application.CreateObjectSpace();
            ImportHelper.LoadDocument(out sheet, out range, out filefields, out newfields, file.File, View.ObjectTypeInfo.Type);

            if (newfields.Count > 0)
            {
                MessageOptions options1 = new MessageOptions();
                options1.Duration = 5000;
                options1.Message = string.Format("Unrecognized field name(s): {0}! Please, use Create field command or use correct field names.", String.Join(", ", newfields.ToArray()));
                options1.Type = InformationType.Error;
                options1.Web.Position = InformationPosition.Top;
                options1.Win.Caption = "Import aborted";
                options1.Win.Type = WinMessageType.Toast;
                Application.ShowViewStrategy.ShowMessage(options1);
                return;
            }

            View view = Application.CreateListView(os, View.ObjectTypeInfo.Type, true);
            string id = ((ListView)View).Id;
            Type oftype = View.ObjectTypeInfo.Type;
            /*Frame.SetView(null);
     
            ImportHelper.CreateNewFields(Application, id, os, oftype,sheet, filefields);

            Frame.SetView(view);*/
            try
            {
                AuditTrailService.Instance.SaveAuditTrailData += Instance_SaveAuditTrailData;

                ImportHelper.DoImport(Application,id,os,null, sheet, range, filefields, oftype,file.ImportGBIFTaxonomy);
            }
            finally
            {
                AuditTrailService.Instance.SaveAuditTrailData -= Instance_SaveAuditTrailData;
            }   //View.LoadModel(true);
                //   RefreshController cont =Frame.GetController<RefreshController>();
                // if (cont != null) cont.RefreshAction.DoExecute();

            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("Import finished!");
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Bottom;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Toast;
            Application.ShowViewStrategy.ShowMessage(options);

        }
    }
}
