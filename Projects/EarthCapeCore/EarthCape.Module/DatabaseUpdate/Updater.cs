﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using Xpand.ExpressApp.ModelDifference.Security;
using Xpand.ExpressApp.Security.Core;
using Xpand.Persistent.BaseImpl.Security;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using EarthCape.Module.Core;
using Xpand.ExpressApp.IO.Core;
using System.IO;
using DevExpress.Persistent.BaseImpl;
using System.Xml.Linq;
using DevExpress.ExpressApp.Dashboards;
using DevExpress.ExpressApp.Security;
using DevExpress.XtraReports.UI;
using DevExpress.Persistent.Base.ReportsV2;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using Xpand.ExpressApp.Dashboard.BusinessObjects;
using Xpand.ExpressApp.Security.Registration;
using EarthCape.Module.Logistics;

namespace EarthCape.Module.DatabaseUpdate
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppUpdatingModuleUpdatertopic
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion)
        {
        }
        public override void UpdateDatabaseBeforeUpdateSchema()
        {
            base.UpdateDatabaseBeforeUpdateSchema();

            // DeleteTables((XPObjectSpace)ObjectSpace);
            //ObjectSpace.CommitChanges();
        }
        private void UpdateDashboardDataSource()
        {
            IList<DashboardData> allDashboards = ObjectSpace.GetObjects<DashboardData>();
            foreach (DashboardData dashboardData in allDashboards)
            {
                XDocument document = dashboardData.LoadDocument();
                var allDataSources = document.Descendants("DataSource");
                foreach (XElement element in allDataSources)
                {
                    XAttribute typeAttribute = element.Attributes("Type").FirstOrDefault();
                    string typeName = typeAttribute.Value.Substring(0, typeAttribute.Value.IndexOf(','));
                    Type resultType = ReflectionHelper.FindType(typeName);
                    typeAttribute.Value = resultType.AssemblyQualifiedName;
                }
                dashboardData.SaveDocument(document);
            }
            IList<DashboardDefinition> allXDashboards = ObjectSpace.GetObjects<DashboardDefinition>();
            foreach (DashboardData dashboardData in allDashboards)
            {
                XDocument document = dashboardData.LoadDocument();
                var allDataSources = document.Descendants("DataSource");
                foreach (XElement element in allDataSources)
                {
                    XAttribute typeAttribute = element.Attributes("Type").FirstOrDefault();
                    string typeName = typeAttribute.Value.Substring(0, typeAttribute.Value.IndexOf(','));
                    Type resultType = ReflectionHelper.FindType(typeName);
                    typeAttribute.Value = resultType.AssemblyQualifiedName;
                }
                dashboardData.SaveDocument(document);
            }

            ObjectSpace.CommitChanges();
        }

        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
         //  UpdateDashboardDataSource();

            XpandPermissionPolicyRole adminRole = ObjectSpace.FindObject<XpandPermissionPolicyRole>(new BinaryOperator("Name", "Admin"));
            if (adminRole == null)
                adminRole = (XpandPermissionPolicyRole)ObjectSpace.GetAdminRole("Admin");

            XpandPermissionPolicyUser adminUser = ObjectSpace.FindObject<XpandPermissionPolicyUser>(new BinaryOperator("UserName", "Admin"));
            if (adminUser == null)
            {
                adminUser = (XpandPermissionPolicyUser)adminRole.GetUser("Admin");
                adminUser.ChangePasswordOnFirstLogon = true;
#if EASYTEST
                    adminUser.ChangePasswordOnFirstLogon = false;
#endif
                adminUser.Save();
            }


            /*    XpandPermissionPolicyRole anonRole = ObjectSpace.FindObject<XpandPermissionPolicyRole>(new BinaryOperator("Name", "Anonymous"));
                if (anonRole == null)
                {
                    anonRole = (XpandPermissionPolicyRole)ObjectSpace.GetAnonymousPermissionPolicyRole("Anonymous");
                    anonRole.CanEditModel = false;
                }
                XpandPermissionPolicyUser anonUser = ObjectSpace.FindObject<XpandPermissionPolicyUser>(new BinaryOperator("UserName", "Anonymous"));
                if (anonUser == null)
                    anonUser = (XpandPermissionPolicyUser)anonRole.GetAnonymousPermissionPolicyUser("Anonymous");*/
            var defaultRole = (XpandPermissionPolicyRole)ObjectSpace.GetDefaultRole();
            var modelRole = (XpandPermissionPolicyRole)ObjectSpace.GetDefaultModelRole("ModelRole");
            var anonymousRole = ObjectSpace.GetAnonymousPermissionPolicyRole("Anonymous");
            XpandPermissionPolicyUser anonusr = (XpandPermissionPolicyUser)anonymousRole.GetAnonymousPermissionPolicyUser("Anonymous");
            anonusr.Roles.Add(modelRole);
            anonymousRole.CanEditModel = false;
               // anonymousRole.AddTypePermission<Search>(SecurityOperations.FullObjectAccess, SecurityPermissionState.Allow);
            anonymousRole.AddObjectPermission(SecuritySystem.UserType, SecurityOperations.ReadOnlyAccess,"[Oid] = CurrentUserId()", SecurityPermissionState.Allow); //  anonymousRole.AddMemberPermission(SecuritySystem.UserType, SecurityOperations.ReadWriteAccess, "ChangePasswordOnFirstLogon; StoredPassword", null, SecurityPermissionState.Allow);
            anonusr.Roles.Add(anonymousRole);
                anonusr.Roles.Add(defaultRole);
            anonymousRole.AddTypePermission<ContentPage>(SecurityOperations.Read, SecurityPermissionState.Allow);
            anonymousRole.AddTypePermission<ContentPage>(SecurityOperations.Navigate, SecurityPermissionState.Allow);
            anonymousRole.AddObjectPermission(typeof(XpandPermissionPolicyUser), SecurityOperations.ReadOnlyAccess, "[Oid] = CurrentUserId()",SecurityPermissionState.Allow);
            anonymousRole.Save();
            anonusr.Save();
            //     defaultRole.AddTypePermission<InformationRequest>(SecurityOperations.Read, SecurityPermissionState.Allow);
            anonymousRole.AddObjectPermission(typeof(InformationRequest), SecurityOperations.ReadWriteAccess, "[CreatedByUser] = CurrentUserName()", SecurityPermissionState.Allow);
            defaultRole.AddTypePermission<AuditDataItemPersistent>(SecurityOperations.Create, SecurityPermissionState.Allow);
            defaultRole.AddTypePermission<AuditDataItemPersistent>(SecurityOperations.Write, SecurityPermissionState.Allow);
            defaultRole.AddTypePermission<InformationRequest>(SecurityOperations.Navigate, SecurityPermissionState.Allow);
            defaultRole.AddTypePermission<InformationRequest>(SecurityOperations.Create, SecurityPermissionState.Allow);
            defaultRole.AddTypePermission<InformationRequest>(SecurityOperations.Read, SecurityPermissionState.Deny);
            defaultRole.Save();

            ContentPage page = ObjectSpace.FindObject<ContentPage>(CriteriaOperator.Parse("IsNullOrEmpty(Content)"));
            int count = (int)ObjectSpace.Evaluate(typeof(ContentPage), CriteriaOperator.Parse("Count()"), CriteriaOperator.Parse("!IsNullOrEmpty(Content)"));

            if ((page != null) || (count == 0))
            {
                page = ObjectSpace.CreateObject<ContentPage>();
                page.Title = "Welcome";
                page.Content = @"<h1>Hi! Welcome to EarthCape database!</h1>
<p> Please consult with EarthCape online documentation for next steps: <a href=""https://earthcape.com/docs/"" target=_new>https://earthcape.com/docs/</a><br>
</p>";
                page.Save();
            }

            var importEngine = new ImportEngine();
            Stream stream = GetType().Assembly.GetManifestResourceStream("EarthCape.Module.EmbeddedData.EmailTemplates.xml");
            importEngine.ImportObjects(stream, info => ObjectSpace);

          //  importEngine = new ImportEngine();
           // stream = GetType().Assembly.GetManifestResourceStream("EarthCape.Module.EmbeddedData.Reports.xml");
            //importEngine.ImportObjects(stream, info => ObjectSpace);

            importEngine = new ImportEngine();
            stream = GetType().Assembly.GetManifestResourceStream("EarthCape.Module.EmbeddedData.Dashboards.xml");
            importEngine.ImportObjects(stream, info => ObjectSpace);

            /*  stream = GetType().Assembly.GetManifestResourceStream("EarthCape.Module.EmbeddedData.xml");
              importEngine.ImportObjects(stream, info => ObjectSpace);*/


            ObjectSpace.CommitChanges();

            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
       
       

        }
    }
}
