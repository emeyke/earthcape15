﻿using Xpand.ExpressApp.Security.Core;
using Xpand.Persistent.BaseImpl.Security;

namespace EarthCape.Module {
	partial class EarthCapeBaseModule
    {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            // 
            // EarthCapeBaseModule
            // 
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.SystemModule.SystemModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.Validation.ValidationModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.AuditTrail.AuditTrailModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.Chart.ChartModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.ReportsV2.ReportsModuleV2));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.Notifications.NotificationsModule));
            this.AdditionalExportedTypes.Add(typeof(DevExpress.Persistent.BaseImpl.FileData));
            this.AdditionalExportedTypes.Add(typeof(DevExpress.Persistent.BaseImpl.FileAttachmentBase));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.Dashboard.DashboardModule));
            this.RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.CloneObject.CloneObjectModule));
         
            this.AdditionalExportedTypes.Add(typeof(DevExpress.Persistent.BaseImpl.RichTextMailMergeData));
            this.AdditionalExportedTypes.Add(typeof(Xpand.ExpressApp.Security.Core.XpandUser));
            this.AdditionalExportedTypes.Add(typeof(Xpand.Persistent.BaseImpl.SequenceObject));
            this.AdditionalExportedTypes.Add(typeof(Xpand.Persistent.BaseImpl.SequenceReleasedObject));
            this.AdditionalExportedTypes.Add(typeof(Xpand.Persistent.BaseImpl.Security.XpandPermissionPolicyUser));
            this.AdditionalExportedTypes.Add(typeof(Xpand.Persistent.BaseImpl.Security.XpandPermissionPolicyRole));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.ModelArtifactState.ModelArtifactStateModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.ModelDifference.ModelDifferenceModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.IO.IOModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.ExpressApp.ExcelImporter.ExcelImporterModule));
            this.RequiredModuleTypes.Add(typeof(LlamachantFramework.Module.LlamachantFrameworkModule));
            this.RequiredModuleTypes.Add(typeof(Xpand.XAF.Modules.ModelMapper.ModelMapperModule));

            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.Core.EarthCapeModule));
            this.RequiredModuleTypes.Add(typeof(EarthCape.Module.GBIF.EarthCapeGBIFModule));
        }

        #endregion
    }
}
