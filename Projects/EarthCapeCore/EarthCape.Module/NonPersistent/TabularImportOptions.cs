using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;

namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class TabularImportOptions : XPCustomObject
    {
        public TabularImportOptions(Session session) : base(session) { }
        private AttachmentFileData _File;
        public AttachmentFileData File
        {
            get
            {
                return _File;
            }
            set
            {
                SetPropertyValue("File", ref _File, value);
            }
        }
        private Project _Project;
        public Project Project
        {
            get
            {
                return _Project;
            }
            set
            {
                SetPropertyValue("Project", ref _Project, value);
            }
        }
    }

}
