using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;
using EarthCape.Module.Enums;

namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class AddFieldOptions : XPCustomObject
    {
        public AddFieldOptions(Session session) : base(session) { }
        string name;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
                if (Caption == "") Caption = name;
            }
        }
        string caption;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Caption
        {
            get
            {
                return caption;
            }
            set
            {
                SetPropertyValue("Caption", ref caption, value);
            }
        }
        FieldType type;
        public FieldType Type
        {
            get
            {
                return type;
            }
            set
            {
                SetPropertyValue("Type", ref type, value);
            }
        }
    }

}
