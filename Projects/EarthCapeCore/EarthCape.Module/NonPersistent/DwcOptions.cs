using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EarthCape.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;
using EarthCape.Module.Enums;

namespace EarthCape.Module.Core 
{
    [NonPersistent]
    public class DwcOptions : XPCustomObject
    {
        public DwcOptions(Session session) : base(session) { }
     
    }

}
