﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp;
using System.Collections;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Model;
using System.Web.UI;
using System.IO;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.ExpressApp.Web;
using EarthCape.Interfaces;

namespace EarthCapePro.Module.Web.Editors
{

 
 
    [ListEditor(typeof(IThumbnail))]
    public class GalleryListEditor : ListEditor
    {
        private ASPxImageGallery control;
    
        protected override object CreateControlsCore()
        {
            control = new ASPxImageGallery();
            control.ID = "GalleryListEditor_control";
            //           control.OnClick += new EventHandler<GalleryListEditorClickEventArgs>(control_OnClick);
            return control;
        }
        protected override void AssignDataSourceToControl(Object dataSource)
        {
            if (control != null)
            {
                control.DataSource = ListHelper.GetList(dataSource);
            }
        }
        public override void Refresh()
        {
            if (control != null)
            {
            }
        }
        public override void BreakLinksToControls()
        {
            control = null;
            base.BreakLinksToControls();
        }
        public GalleryListEditor(IModelListView info) : base(info) { }

        public override SelectionType SelectionType
        {
            get { return SelectionType.TemporarySelection; }
        }
        public override IList GetSelectedObjects()
        {
            List<object> selectedObjects = new List<object>();
            if (FocusedObject != null)
            {
                selectedObjects.Add(FocusedObject);
            }
            return selectedObjects;
        }
        public override DevExpress.ExpressApp.Templates.IContextMenuTemplate ContextMenuTemplate
        {
            get { return null; }
        }
       
    }
}
