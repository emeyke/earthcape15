using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// https://www.devexpress.com/support/center/Question/Details/T323893#comment-ecd03dd0-5a28-4594-8a46-a9ae137a0880
namespace EarthCapePro.Module.Web.Functions
{
	public static class HandyFileFunctions
	{
		public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
		{
			var ratioX = (double)maxWidth / image.Width;
			var ratioY = (double)maxHeight / image.Height;
			var ratio = Math.Min(ratioX, ratioY);

			var newWidth = (int)(image.Width * ratio);
			var newHeight = (int)(image.Height * ratio);

			var newImage = new Bitmap(newWidth, newHeight);

			using (var graphics = Graphics.FromImage(newImage))
				graphics.DrawImage(image, 0, 0, newWidth, newHeight);

			return newImage;
		}
		public static System.Drawing.Image ByteArrayToImage(byte[] byteArrayIn)
		{
			try
			{
				var ms = new MemoryStream(byteArrayIn);
				var returnImage = System.Drawing.Image.FromStream(ms);
				return returnImage;
			}
			catch (Exception)
			{
				return null;

			}

		}
		public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
		{
			var ms = new MemoryStream();
			//imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
			imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
			return ms.ToArray();
		}

		public static byte[] GetSmallImageBytes(byte[] bytes)
		{
			var image = ByteArrayToImage(bytes);
			if (image == null)
			{
				return null;
			}
			var smallImage = ScaleImage(image, 300, 300);
			return ImageToByteArray(smallImage);
		}
	}
}