﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp;
using System.Collections;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Model;
using System.Web.UI;
using System.IO;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.ExpressApp.Web;
using EarthCape.Interfaces;

namespace EarthCapePro.Module.Web.Editors
{

    public class ThumbnailClickEventArgs : EventArgs
    {
        public IThumbnail ItemClicked;
    }
    
    [ListEditor(typeof(IThumbnail))]
    public class ASPxImageGalleryListEditor : ListEditor
    {
        public ASPxImageGalleryListEditor(IModelListView info) : base(info) { }
        private ASPxImageGallery control;
        protected override object CreateControlsCore()
        {
            control = new ASPxImageGallery();
            control.ID = "ASPxImageGalleryListEditor_control";
             return control;
        }
        protected override void AssignDataSourceToControl(Object dataSource)
        {
            if (control != null)
            {
                control.DataSource = ListHelper.GetList(dataSource);
                control.ThumbnailUrlField
            }
        }
        public override void Refresh()
        {
          //  if (control != null) control.
        }
        public override void BreakLinksToControls()
        {
            control = null;
            base.BreakLinksToControls();
        }
        private void control_OnClick(object sender, ThumbnailClickEventArgs e)
        {
            this.FocusedObject = e.ItemClicked;
            OnSelectionChanged();
            OnProcessSelectedItem();
        }
        private object focusedObject;
        public override object FocusedObject
        {
            get
            {
                return focusedObject;
            }
            set
            {
                focusedObject = value;
            }
        }
        public override SelectionType SelectionType
        {
            get { return SelectionType.TemporarySelection; }
        }
        public override IList GetSelectedObjects()
        {
            List<object> selectedObjects = new List<object>();
            if (FocusedObject != null)
            {
                selectedObjects.Add(FocusedObject);
            }
            return selectedObjects;
        }
        public override DevExpress.ExpressApp.Templates.IContextMenuTemplate ContextMenuTemplate
        {
            get { return null; }
        }
    }
}
