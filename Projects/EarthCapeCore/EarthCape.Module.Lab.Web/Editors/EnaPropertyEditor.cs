﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace EarthCapePro.Module.Web.Editors
{
    [PropertyEditor(typeof(String), "EnaPropertyEditor", false)]
    public class EnaPropertyEditor : ASPxPropertyEditor
    {
        private bool _cancelClickEventPropagation;

        public EnaPropertyEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
            _cancelClickEventPropagation = true;
        }

        public override bool CancelClickEventPropagation
        {
            get { return _cancelClickEventPropagation; }
            set { _cancelClickEventPropagation = value; }
        }

        protected override WebControl CreateEditModeControlCore()
        {
            if (AllowEdit)
            {
                var textBox = RenderHelper.CreateASPxTextBox();
                textBox.MaxLength = MaxLength;
                textBox.TextChanged += EditValueChangedHandler;
                return textBox;
            }
            return CreateHyperLink();
        }


        protected override void ApplyReadOnly()
        {
            if (Editor is ASPxTextBox)
                base.ApplyReadOnly();
        }

        protected override void ReadEditModeValueCore()
        {
            base.ReadEditModeValueCore();
            SetupHyperLink(PropertyValue, Editor);
        }
        protected override WebControl CreateViewModeControlCore()
        {
            return CreateHyperLink();
        }

        protected override void ReadViewModeValueCore()
        {
            base.ReadViewModeValueCore();
            SetupHyperLink(PropertyValue, InplaceViewModeEditor);
        }

        static string GetResolvedUrl(object value)
        {
            string url = Convert.ToString(value);
            if (!string.IsNullOrEmpty(url))
            {
                url = string.Format("http://www.ebi.ac.uk/ena/data/view/{0}", url);
                    return url;
            }
            return string.Empty;
        }

    
        ASPxHyperLink CreateHyperLink()
        {
            ASPxHyperLink hyperlink = RenderHelper.CreateASPxHyperLink();
            return hyperlink;
        }

        void SetupHyperLink(object value, object editor)
        {

            var hyperlink = editor as ASPxHyperLink;
            if (hyperlink != null)
            {
                string url = Convert.ToString(value);
                hyperlink.Text = url;
                hyperlink.NavigateUrl = GetResolvedUrl(url);
                hyperlink.Target = "blank";
            }
        }
    }
}
