﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Win.SystemModule;
using DevExpress.ExpressApp.Xpo;
using System.Linq;
using Xpand.ExpressApp.ModelDifference.Core;
using Xpand.ExpressApp.ModelDifference.DataStore.BaseObjects;
using Xpand.Persistent.Base.MiddleTier;

namespace EarthCapeApplicationServer {
    public class EarthCapeApplicationServerServerApplication : XpandServerApplication, IModelDifferenceServerModels {
        public IQueryable<ModelDifferenceObject> Where(IQueryable<ModelDifferenceObject> modelDifferenceObjects)
        {
            return modelDifferenceObjects;
        }

        public EarthCapeApplicationServerServerApplication(ISecurityStrategyBase securityStrategyBase) : base(securityStrategyBase) {
            ApplicationName = "EarthCape";
            
            Modules.Add(new SystemWindowsFormsModule());
            Modules.Add(new EarthCape.Module.Core.EarthCapeModule());
            Modules.Add(new EarthCape.Module.EarthCapeBaseModule());
             Modules.Add(new EarthCape.Module.Field.Win.EarthCapeFieldWinModule());
             Modules.Add(new EarthCape.Module.Importer.Win.EarthCapeImporterWinModule());
         //    Modules.Add(new EarthCape.Module.GBIF.EarthCapeGBIFModule());
             Modules.Add(new EarthCape.Module.GIS.EarthCapeGISModule());
             Modules.Add(new EarthCape.Module.GIS.Win.EarthCapeGISWinModule());
             Modules.Add(new EarthCape.Module.Lab.Win.EarthCapeLabModuleWin());
        //     Modules.Add(new EarthCape.Module.Logistics.EarthCapeLogisticsModule());

            /*Modules.Add(new DevExpress.ExpressApp.Validation.Win.ValidationWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.Dashboards.DashboardsModule());
             Modules.Add(new DevExpress.ExpressApp.AuditTrail.AuditTrailModule());
             Modules.Add(new DevExpress.ExpressApp.Dashboards.Win.DashboardsWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.HtmlPropertyEditor.Win.HtmlPropertyEditorWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.Chart.ChartModule());
             Modules.Add(new DevExpress.ExpressApp.Chart.Win.ChartWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.Office.Win.OfficeWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.PivotGrid.Win.PivotGridWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.PivotGrid.PivotGridModule());
             Modules.Add(new DevExpress.ExpressApp.Notifications.NotificationsModule());
             Modules.Add(new DevExpress.ExpressApp.Notifications.Win.NotificationsWindowsFormsModule());
             Modules.Add(new DevExpress.ExpressApp.CloneObject.CloneObjectModule());
             Modules.Add(new DevExpress.ExpressApp.Scheduler.Win.SchedulerWindowsFormsModule());

             Modules.Add(new Xpand.ExpressApp.SystemModule.XpandSystemModule());
             Modules.Add(new Xpand.ExpressApp.Win.SystemModule.XpandSystemWindowsFormsModule());
             Modules.Add(new Xpand.ExpressApp.Security.XpandSecurityModule());
             Modules.Add(new Xpand.ExpressApp.Security.Win.XpandSecurityWinModule());
             Modules.Add(new Xpand.ExpressApp.ModelDifference.ModelDifferenceModule());
             Modules.Add(new Xpand.ExpressApp.Email.EmailModule());
             Modules.Add(new Xpand.ExpressApp.ViewVariants.XpandViewVariantsModule());
             Modules.Add(new Xpand.ExpressApp.ExcelImporter.ExcelImporterModule());
             Modules.Add(new Xpand.ExpressApp.ExcelImporter.Win.ExcelImporterWinModule());
             Modules.Add(new Xpand.ExpressApp.Dashboard.DashboardModule());
             Modules.Add(new Xpand.ExpressApp.XtraDashboard.Win.DashboardWindowsFormsModule());


             Modules.Add(new LlamachantFramework.Module.LlamachantFrameworkModule());
             Modules.Add(new LlamachantFramework.Module.Win.LlamachantFrameworkWindowsFormsModule());
             */
        }

        protected override void OnDatabaseVersionMismatch(DatabaseVersionMismatchEventArgs args) {
            args.Updater.Update();
            args.Handled = true;
        }

        protected override void CreateDefaultObjectSpaceProvider(CreateCustomObjectSpaceProviderEventArgs args) {
            args.ObjectSpaceProvider = new XPObjectSpaceProvider(args.ConnectionString, args.Connection);
        }
    }
}