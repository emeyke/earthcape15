﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;

namespace EarthCape.Module.Docs.Win.BusinessObjects
{
    [DefaultClassOptions]
    //[ImageName("BO_Contact")]
    [DefaultProperty("Caption")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Doc : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        public Doc(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        bool isDefault;
        private String _NodeId;

        public String NodeId
        {
            get
            {
                return _NodeId;
            }
            set
            {
                SetPropertyValue("NodeId", ref _NodeId, value);
            }
        }
        
        public bool IsDefault
        {
            get => isDefault;
            set => SetPropertyValue(nameof(IsDefault), ref isDefault, value);
        }
        private String _Caption;
        [Size(255)]
        public String Caption
        {
            get
            {
                return _Caption;
            }
            set
            {
                SetPropertyValue("Caption", ref _Caption, value);
            }
        }
        private int _Order;
        public int Order
        {
            get
            {
                return _Order;
            }
            set
            {
                SetPropertyValue("Order", ref _Order, value);
            }
        }
        private String _DbName;
        public String DbName
        {
            get
            {
                return _DbName;
            }
            set
            {
                SetPropertyValue("DbName", ref _DbName, value);
            }
        }
        private Image _Screenshot;
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Screenshot
        {
            get
            {
                return _Screenshot;
            }
            set
            {
                SetPropertyValue("Screenshot", ref _Screenshot, value);
            }
        }

        private String _Chapter;
        public String Chapter
        {
            get
            {
                return _Chapter;
            }
            set
            {
                SetPropertyValue("Chapter", ref _Chapter, value);
            }
        }
        private Doc _Parent;
        [Association("ParentChild")]
        public Doc Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                SetPropertyValue("Parent", ref _Parent, value);
            }
        }
        [Association("ParentChild")]
        public XPCollection<Doc> Children
        {
            get { return GetCollection<Doc>("Children"); }
        }
        private String _View;
        [Size(255)]
        public String View
        {
            get
            {
                return _View;
            }
            set
            {
                SetPropertyValue("View", ref _View, value);
            }
        }
        private String _ToolTip;
        [Size(1000)]
        public String ToolTip
        {
            get
            {
                return _ToolTip;
            }
            set
            {
                SetPropertyValue("ToolTip", ref _ToolTip, value);
            }
        }
        private DocType _Type;
        public DocType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                SetPropertyValue("Type", ref _Type, value);
            }
        }


        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}