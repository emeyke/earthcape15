﻿namespace EarthCape.Module.Docs.Win.BusinessObjects
{
    public enum DocType
    {
        ListView,
        DetailView,
        Column,
        ViewItem,
        Chapter
    }
}