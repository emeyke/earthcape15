﻿namespace EarthCape.Module.Docs.Win.Controllers
{
    partial class Documentation_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleAction_DocGeneration = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // simpleAction_DocGeneration
            // 
            this.simpleAction_DocGeneration.Caption = "Generate documentation";
            this.simpleAction_DocGeneration.Category = "Tools";
            this.simpleAction_DocGeneration.ConfirmationMessage = null;
            this.simpleAction_DocGeneration.Id = "simpleAction_DocGeneration";
            this.simpleAction_DocGeneration.TargetObjectType = typeof(EarthCape.Module.Docs.Win.BusinessObjects.Doc);
            this.simpleAction_DocGeneration.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.simpleAction_DocGeneration.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.simpleAction_DocGeneration.ToolTip = null;
            this.simpleAction_DocGeneration.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.simpleAction_DocGeneration.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.simpleAction_DocGeneration_Execute);
            // 
            // Documentation_ViewController
            // 
            this.Actions.Add(this.simpleAction_DocGeneration);
            this.TargetObjectType = typeof(EarthCape.Module.Docs.Win.BusinessObjects.Doc);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction simpleAction_DocGeneration;
    }
}
