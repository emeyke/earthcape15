﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;
using EarthCape.Module.Docs.Win.BusinessObjects;
using EarthCape.Module.Core;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.ExpressApp.Security;
using DevExpress.XtraBars.Ribbon;
using System.Drawing.Imaging;
using DevExpress.XtraGrid;
using Xpand.ExpressApp.TreeListEditors.Win.ListEditors;
using System.IO;

namespace EarthCape.Module.Docs.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Documentation_ViewController : ViewController
    {
        int i = 0;
        public Documentation_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        
            if ((simpleAction_DocGeneration.Active) && (View.ObjectTypeInfo != null))
            {
                //     simpleAction_DocGeneration.Active.SetItemValue("SecurityAllowance", SecuritySystem.IsGranted(new PermissionRequest(ObjectSpace, typeof(Doc), SecurityOperations.Write)));
            }
        }



        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        TextWriter toctw = null;
        TextWriter indextw = null;

        private void simpleAction_DocGeneration_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Value_WindowController.Instance().Importing = true;
            try
            {

              i = 0;
            i++;
                currentIndex = 0;
                if (myList!=null)
                myList.Clear();
            string toc = @"C:\Repos\earthcape-docs\application\toc.yml";
            string index = @"C:\Repos\earthcape-docs\application\index.md";
            this.toctw = new StreamWriter(toc, false);
            this.indextw = new StreamWriter(index, false);
            indextw.WriteLine("#### Application");
            indextw.WriteLine("");


            IModelNavigationItem node = null;
            var xafApplication = Application;
            var model = (xafApplication.Model);
            IModelList<IModelView> modelViews = model.Application.Views;
            IObjectSpace os = Application.CreateObjectSpace();
            IModelApplicationNavigationItems navigationItems = model as IModelApplicationNavigationItems;
            myList = new List<IModelNavigationItem>();
            foreach (IModelNode nd_ts in navigationItems.NavigationItems.AllItems)
            {
                if (nd_ts != null)
                {
                    node = (IModelNavigationItem)(nd_ts);
                    CreateToc(node);
                    if ((node.View != null) && ((typeof(IModelListView).IsAssignableFrom(node.View.GetType()))))
                    // if (((IModelListView)node.View).EditorType.Name != "XpandTreeListEditor")
                    {
                        if (node.Caption.IndexOf("model") < 0)
                        {
                            myList.Add(node);
                            CreateMdDoc(node);
                        }
                    }
                }
            }
            ProcessCurrentNode();

          
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                this.toctw.Flush();
                this.indextw.Flush();
                toctw.Close();
                indextw.Close();
                Value_WindowController.Instance().Importing = false;
            }
        }



        List<IModelNavigationItem> myList;
        int currentIndex;
        bool listCreated;
        string path = "Output";
        private void ProcessCurrentNode()
        {
            if (currentIndex < myList.Count)
            {
                if (!listCreated)
                {
                    var node = myList[currentIndex];
                    var listview = Application.CreateListView(((IModelListView)((IModelView)node.View)).ModelClass.TypeInfo.Type, true);
                    if ((listview.Editor.GetType()!= typeof(XpandTreeListEditor)) && (listview.Editor.GetType() != typeof(XpandTreeListEditor)))
                    {
                        listview.ControlsCreated += Listview_ControlsCreated;
                        Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                        Frame.SetView(listview);
                        Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                        //    CreateDoc(node, tw);
                        listCreated = true;
                    }
                    else
                    {
                        currentIndex++;
                        ProcessCurrentNode();
                    }
                }
                else
                {
                    listCreated = false;
                    var node = myList[currentIndex++];
                    var type = ((IModelListView)((IModelView)node.View)).ModelClass.TypeInfo.Type;
                    if (!type.IsAbstract)
                    {
                        IObjectSpace os1 = Application.CreateObjectSpace();
                        //  Object obj = os1.FindObject(type, new BinaryOperator("Oid", Guid.NewGuid(), BinaryOperatorType.NotEqual));
                        // if (obj == null)
                        Object obj = os1.CreateObject(type);

                        DetailView dv = Application.CreateDetailView(os1, obj);
                        if (dv != null)
                        {
                            dv.ControlsCreated += Listview_ControlsCreated;
                            try
                            {
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                                Frame.SetView(dv);
                                Frame.GetController<ModificationsController>().ModificationsHandlingMode = ModificationsHandlingMode.AutoRollback;
                            }

                            catch (Exception)
                            {

                            }
                        }
                    }
                    else ProcessCurrentNode();
               //     if (!os1.IsDisposed)
                 /*   {
              //          os1.Delete(obj);
                        os1.RemoveFromModifiedObjects(obj);
                        os1.Rollback();
                    }*/
                }
            }
        }
    
    private void CreateDoc(IModelNavigationItem node)
        {
            if (node.View is IModelListView)
            {
                i++;
                IObjectSpace os = ObjectSpace;
                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                if (doc == null)
                    doc = os.CreateObject<Doc>();
                doc.NodeId = "view_" + node.Id;
                doc.Caption = node.Caption;
                doc.Order = i;
                doc.ToolTip = node.ToolTip;
                doc.Type = DocType.ListView;
                doc.View = node.View.Id;
                doc.Save();
                os.CommitChanges();

                IModelDetailView myDv = null;
                if (node.View is IModelListView)
                    myDv = ((IModelListView)(node.View)).DetailView;
                if (node.View is IModelDetailView)
                    myDv = ((IModelDetailView)(node.View));
                i++;
                if (myDv != null)
                {
                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                    if (doc1 == null)
                        doc1 = os.CreateObject<Doc>();
                    doc1.NodeId = "view_" + myDv.Id;
                    doc1.Caption = myDv.Caption;

                    doc1.Order = i;
                    doc1.Type = DocType.DetailView;
                    doc1.View = myDv.Id;
                    doc1.Parent = doc;
                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                    doc1.Save();
                    os.CommitChanges();
                    SaveViewItems(myDv.Layout[0], doc1, os);
                }
                SaveColumns(((IModelListView)(node.View)), doc, os);
             }
        }

        private void CreateDoc(IModelNavigationItem node, TextWriter tw)
        {
            if (node.View is IModelListView)
            {
                tw.WriteLine(string.Format("- name: {0}", node.Caption));
                tw.WriteLine(string.Format("  href: {0}.md", node.Id));
            
/*
            IObjectSpace os = ObjectSpace;
                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                if (doc == null)
                    doc = os.CreateObject<Doc>();
                doc.NodeId = "view_" + node.Id;
                doc.Caption = node.Caption;
                doc.Order = i;
                doc.ToolTip = node.ToolTip;
                doc.Type = DocType.ListView;
                doc.View = node.View.Id;
                doc.Save();
                os.CommitChanges();

                IModelDetailView myDv = null;
                if (node.View is IModelListView)
                    myDv = ((IModelListView)(node.View)).DetailView;
                if (node.View is IModelDetailView)
                    myDv = ((IModelDetailView)(node.View));
                i++;
                if (myDv != null)
                {
                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                    if (doc1 == null)
                        doc1 = os.CreateObject<Doc>();
                    doc1.NodeId = "view_" + myDv.Id;
                    doc1.Caption = myDv.Caption;

                    doc1.Order = i;
                    doc1.Type = DocType.DetailView;
                    doc1.View = myDv.Id;
                    doc1.Parent = doc;
                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                    doc1.Save();
                    os.CommitChanges();
                    SaveViewItems(myDv.Layout[0], doc1, os);
                }
                SaveColumns(((IModelListView)(node.View)), doc, os);
                */
            }
        }
        private void CreateMdDoc(IModelNavigationItem node)
        {
            if (node.View is IModelListView)
            {
                IModelClass modelClass = ((IModelListView)(node.View)).ModelClass;
                TextWriter listViewiewDoc = new StreamWriter(String.Format(@"C:\Repos\earthcape-docs\application\{0}.md", modelClass.Name.Split('.').Last()), false);
                if (node.View is IModelListView)
                {
                    listViewiewDoc.WriteLine(String.Format("#### {0} (List View)", ((IModelListView)(node.View)).Caption));
                    listViewiewDoc.WriteLine("");
                    listViewiewDoc.WriteLine(((IModelListView)(node.View)).ModelClass.GetNode("Documentation").GetValue<String>("Description"));
                    listViewiewDoc.WriteLine("");
                    listViewiewDoc.WriteLine(String.Format("[![](images/{0}.png)](images/{0}.png)", ((IModelListView)(node.View)).Id.Replace("@", "")));

                    listViewiewDoc.WriteLine("");

                    listViewiewDoc.WriteLine(String.Format("#### {0} (Detail View)", ((IModelListView)(node.View)).DetailView.Caption));
                    listViewiewDoc.WriteLine("");
                    listViewiewDoc.WriteLine(String.Format("[![](images/{0}.png)](images/{0}.png)", ((IModelListView)(node.View)).DetailView.Id.Replace("@", "")));
                    listViewiewDoc.WriteLine("");

                    SaveColumns((IModelListView)(node.View), listViewiewDoc);

                    listViewiewDoc.Flush();
                    listViewiewDoc.Close();


              /*      TextWriter detailViewDoc = new StreamWriter(String.Format(@"C:\Repos\earthcape-docs\application\{0}.md", ((IModelListView)(node.View)).DetailView.Id.Replace("@", "")), false);
                    detailViewDoc.WriteLine(String.Format("#### {0}", ((IModelListView)(node.View)).DetailView.Caption));
                    detailViewDoc.WriteLine("");
                    detailViewDoc.WriteLine(String.Format("[![](images/{0}.png)](images/{0}.png)",  ((IModelListView)(node.View)).DetailView.Id.Replace("@", "")));
             
                    detailViewDoc.Flush();
                    detailViewDoc.Close();*/
                }
                /*
                            IObjectSpace os = ObjectSpace;
                                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                                if (doc == null)
                                    doc = os.CreateObject<Doc>();
                                doc.NodeId = "view_" + node.Id;
                                doc.Caption = node.Caption;
                                doc.Order = i;
                                doc.ToolTip = node.ToolTip;
                                doc.Type = DocType.ListView;
                                doc.View = node.View.Id;
                                doc.Save();
                                os.CommitChanges();

                                IModelDetailView myDv = null;
                                if (node.View is IModelListView)
                                    myDv = ((IModelListView)(node.View)).DetailView;
                                if (node.View is IModelDetailView)
                                    myDv = ((IModelDetailView)(node.View));
                                i++;
                                if (myDv != null)
                                {
                                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                                    if (doc1 == null)
                                        doc1 = os.CreateObject<Doc>();
                                    doc1.NodeId = "view_" + myDv.Id;
                                    doc1.Caption = myDv.Caption;

                                    doc1.Order = i;
                                    doc1.Type = DocType.DetailView;
                                    doc1.View = myDv.Id;
                                    doc1.Parent = doc;
                                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                                    doc1.Save();
                                    os.CommitChanges();
                                    SaveViewItems(myDv.Layout[0], doc1, os);
                                }
                                SaveColumns(((IModelListView)(node.View)), doc, os);
                                */
            }
        }

        private void CreateToc(IModelNavigationItem node)
        {
            if (node.View is IModelListView)
            {
                IModelClass modelClass = ((IModelListView)(node.View)).ModelClass;
                toctw.WriteLine(string.Format("- name: {0}", node.Caption));
                toctw.WriteLine(string.Format("  href: {0}.md", modelClass.Name.Split('.').Last()));
                indextw.WriteLine(string.Format("- [{0}]({1}.md)",node.Caption, modelClass.Name.Split('.').Last()));

                /*
                            IObjectSpace os = ObjectSpace;
                                Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + node.Id));
                                if (doc == null)
                                    doc = os.CreateObject<Doc>();
                                doc.NodeId = "view_" + node.Id;
                                doc.Caption = node.Caption;
                                doc.Order = i;
                                doc.ToolTip = node.ToolTip;
                                doc.Type = DocType.ListView;
                                doc.View = node.View.Id;
                                doc.Save();
                                os.CommitChanges();

                                IModelDetailView myDv = null;
                                if (node.View is IModelListView)
                                    myDv = ((IModelListView)(node.View)).DetailView;
                                if (node.View is IModelDetailView)
                                    myDv = ((IModelDetailView)(node.View));
                                i++;
                                if (myDv != null)
                                {
                                    Doc doc1 = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_" + myDv.Id));
                                    if (doc1 == null)
                                        doc1 = os.CreateObject<Doc>();
                                    doc1.NodeId = "view_" + myDv.Id;
                                    doc1.Caption = myDv.Caption;

                                    doc1.Order = i;
                                    doc1.Type = DocType.DetailView;
                                    doc1.View = myDv.Id;
                                    doc1.Parent = doc;
                                    doc1.DbName = ((IModelDetailView)(myDv)).ModelClass.Name.Substring(((IModelDetailView)(myDv)).ModelClass.Name.LastIndexOf('.') + 1);
                                    doc1.Save();
                                    os.CommitChanges();
                                    SaveViewItems(myDv.Layout[0], doc1, os);
                                }
                                SaveColumns(((IModelListView)(node.View)), doc, os);
                                */
            }
        }

        private static string getassembly(string s)
        {
             int idx = s.LastIndexOf('.');
            if (idx != -1)
                return s.Substring(0, idx);
            return "";
        }
     

        private void Listview_ControlsCreated(object sender, EventArgs e)
        {
            DevExpress.ExpressApp.View view = (sender as DevExpress.ExpressApp.View);
            Control viewControl = (Control)view.Control;
            viewControl.Tag = view.Id;
            if (View is DevExpress.ExpressApp.ListView)
            {
                viewControl.Width = 1550;
                viewControl.Height = 100;
            }
            if (View is DevExpress.ExpressApp.DetailView)
            {
                viewControl.Width = 1550;
                viewControl.Height = 1000;
            }
            //viewControl.Refresh();
            //viewControl.Focus();
            viewControl.Paint += viewControl_Paint;
        }

     
string imagespath= @"C:\Repos\earthcape-docs\application\images\";
        void viewControl_Paint(object sender, PaintEventArgs e)
            {
                try
                {
                    ((Control)sender).Paint -= viewControl_Paint;
                    Snip1((Control)sender, ((Control)sender).Tag.ToString());
                    ProcessCurrentNode();
                }
                catch (Exception)
                {
                }
            }

        private void SaveColumns(IModelListView myIModelListView, TextWriter listviewtw)
        {
            foreach (IModelColumn col in myIModelListView.Columns)
            {
                if ((col != null) && (col.Index > -1))
                {
                    listviewtw.WriteLine(String.Format("- {0}\t{1}\t[{2}]({2}.md)\t{3}",col.ModelMember.Caption, col.ModelMember.Name,col.ModelMember.Type.Name.Split('.').Last(), col.ModelMember.ToolTip));
                }
            }
        }

        private void SaveColumns(IModelListView myIModelListView, Doc parent, IObjectSpace os)
        {
            foreach (IModelColumn col in myIModelListView.Columns)
            {
                if ((col != null) && (col.Index > -1))
                {
                    i++;
                    Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", parent.NodeId + "_" + col.Id));
                    if (doc == null)
                        doc = os.CreateObject<Doc>();
                    doc.NodeId = parent.NodeId + "_" + col.Id;
                    doc.Caption = col.Caption;
                    doc.Order = i;
                    if (col.ModelMember.ModelClass.DefaultProperty == col.PropertyName)
                        doc.IsDefault = true;
                    doc.DbName = col.ModelMember.Name;
                    doc.ToolTip = col.ModelMember.ToolTip;
                    doc.Type = DocType.Column;
                    doc.Parent = parent;
                    doc.Save();
                    os.CommitChanges();
                }
            }
        }
        private void SaveViewItems(IModelViewLayoutElement el, Doc parent, IObjectSpace os)
        {
           if (el is IModelLayoutViewItem)
            {
                IModelLayoutViewItem item = (IModelLayoutViewItem)el;
                if (item.ViewItem is IModelPropertyEditor)
                {
                    i++;
                    Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", parent.NodeId + "_" + item.ViewItem.Id));
                    if (doc == null)
                        doc = os.CreateObject<Doc>();
                    doc.NodeId = parent.NodeId + "_" + item.ViewItem.Id;
                    doc.Caption = item.ViewItem.Caption;
                    doc.Order = i;
                    doc.DbName = ((IModelPropertyEditor)(item.ViewItem)).ModelMember.Name;
                    doc.ToolTip = ((IModelPropertyEditor)(item.ViewItem)).ModelMember.ToolTip;
                    doc.Type = DocType.ViewItem;
                    doc.Parent = parent;
                    doc.Save();
                    os.CommitChanges();
                }
            }
            for (int j = 0; j < el.NodeCount; j++)
            {
                SaveViewItems((IModelViewLayoutElement)el.GetNode(j),parent,os);
            }
        }
        private void Snip1(Control myControl, string filename)
        {

             if (!Directory.Exists(imagespath))
               Directory.CreateDirectory(imagespath);
      
            //Graphics g = myControl.CreateGraphics();
            Bitmap bmp = new Bitmap(myControl.Width, myControl.Height);
            myControl.DrawToBitmap(bmp, new Rectangle(0, 0, myControl.Width, myControl.Height));
       /*     IObjectSpace os = Application.CreateObjectSpace();
            Doc doc = os.FindObject<Doc>(new BinaryOperator("NodeId", "view_"+myControl.Tag));
            if (doc != null)
            {
                doc.Screenshot = bmp;
                doc.Save();
                os.CommitChanges();
            }*/
            bmp.Save(imagespath + filename + ".png", ImageFormat.Png);
            bmp.Dispose();
        }
  
    }
}
