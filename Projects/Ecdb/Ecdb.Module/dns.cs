﻿//using Microsoft.SqlServer.Management.Common;
//using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using maestropanel.plesklib;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace Ecdb.Module
{
    public class dns
    {
        public static void MakeDnsRecord(string username)

        {
          
            var server = new ServerManager();
            var site = server.Sites.FirstOrDefault(a => a.Name.Contains("ecdb"));
              if (site != null)
            {
                var client = new PleskClient("88.99.212.75", "Administrator", "8WbLkVUluhqt");
                var result = client.CreateAlias("ecdb.io", username + ".ecdb.io", false, false, false, false);
                server.CommitChanges();
            }
     
        }
        public static bool MakeDnsRecordManual(string username)

        {

            var server = new ServerManager();
            var site = server.Sites.FirstOrDefault(a => a.Name.Contains("ecdb"));
            if (site != null)
            {
               /* String thumbprint = "‎a0 a8 b3 42 9f 91 10 9b ac 92 cc 8d 20 52 3e b8 74 25 09 9f";
                thumbprint = Regex.Replace(thumbprint.ToUpper(), @"[^0-9A-F]+", string.Empty);
                string serial = "00 90 b2 b2 29 74 2f ad 13 e5 d6 18 f9 25 8f fd 52";
                serial = Regex.Replace(serial.ToUpper(), @"[^0-9A-F]+", string.Empty);


                X509Store store = new X509Store(StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                var certificate = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, true);
                store.Close();*/

                Binding binding = site.Bindings.Add($"*:443:{username}.ecdb.io", null, "WebHosting");
             //   binding = site.Bindings.Add($"*:443:ipv4.{username}.ecdb.io", certificate[0].GetCertHash(), "WebHosting");
                binding = site.Bindings.Add($"*:443:ipv4.{username}.ecdb.io",null, "WebHosting");
                binding = site.Bindings.Add($"*:443:www.{username}.ecdb.io", null, "WebHosting");
                binding = site.Bindings.Add($"*:80:{username}.ecdb.io", "http");
                binding = site.Bindings.Add($"*:80:ipv4.{username}.ecdb.io", "http");
                binding = site.Bindings.Add($"*:80:www.{username}.ecdb.io", "http");

                try
                {
                    server.CommitChanges();

                }
                catch (Exception)
                {

                    throw;
                }
                finally {}
                return true;
            }
            else { return false; }
        }

        public static void DeleteDnsRecord(string username)

        {
            var server = new ServerManager();
            var site = server.Sites.FirstOrDefault(a => a.Name.Contains("ecdb"));
            if (site != null)
            {
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:443:{username}.ecdb.io")));
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:443:ipv4.{username}.ecdb.io")));
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:443:www.{username}.ecdb.io")));
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:80:{username}.ecdb.io")));
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:80:ipv4.{username}.ecdb.io")));
                site.Bindings.Remove(site.Bindings.FirstOrDefault(a => a.BindingInformation.Contains($"*:80:www.{username}.ecdb.io")));
                server.CommitChanges();
            }

            //  var client = new PleskClient("88.99.212.75", "Administrator", "8WbLkVUluhqt");

            //  var result = client.CreateAlias("ecdb.io", username+".ecdb.io",true,false,false,false);
        }
        public static void CreateDatabase(string dbname)
        {
            using (SqlConnection connection = new SqlConnection("Data Source=WIN-RH2DEQT3EVP;Initial Catalog=ECDBAdmin1;User Id=admin;Password = Apollo%2018; "))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "CREATE DATABASE " + dbname;
                command.ExecuteNonQuery();

                ServerConnection conn = new ServerConnection(connection);
                Server srv = new Server(conn);
                Database db = srv.Databases[dbname];
                User u = new User(db, @"WIN-RH2DEQT3EVP\IWPD_1(earthcape)");
                u.Login = @"WIN-RH2DEQT3EVP\IWPD_1(earthcape)";
                u.Create();
                u.AddToRole("db_owner");
                u.AddToRole("db_datareader");
                u.AddToRole("db_datawriter");
                srv.Refresh();
            }
        }
        public static void DropDatabase(string dbname)
        {
            using (SqlConnection connection = new SqlConnection("Data Source=88.99.212.75,1433;Initial Catalog=ECDBadmin1;User Id=admin;Password=Apollo%2018; "))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "DROP DATABASE " + dbname;
                command.ExecuteNonQuery();
            }
        }
    }
}
