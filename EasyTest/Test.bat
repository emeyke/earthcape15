CD C:\Program Files (x86)\DevExpress 18.2\Components\Tools\eXpressAppFramework\EasyTest
del C:\Repos\EarthCape15\EasyTest\results.txt

TestExecutor.v18.2.exe C:\Repos\EarthCape15\Projects\EarthCapeTM\EarthCapeTM.Module\FunctionalTests\full_colony_maintenance.ets
copy C:\Repos\EarthCape15\Projects\EarthCapeTM\EarthCapeTM.Module\FunctionalTests\TestsLog.xml C:\Repos\EarthCape15\EasyTest\TM_full_colony_maintenance_Log.xml

TestExecutor.v18.2.exe C:\Repos\EarthCape15\Projects\EarthCapeTM\EarthCapeTM.Module\FunctionalTests\autotestWeb.ets
copy C:\Repos\EarthCape15\Projects\EarthCapeTM\EarthCapeTM.Module\FunctionalTests\TestsLog.xml C:\Repos\EarthCape15\EasyTest\TM_autotestWeb_Log.xml


copy C:\Repos\EarthCape15\EasyTest\*.xml C:\Repos\EarthCape15\EasyTest\results.txt
del C:\Repos\EarthCape15\EasyTest\*.xml

start C:\Repos\EarthCape15\EasyTest\results.txt

PAUSE
CD C:\Repos\EarthCape15\EasyTest