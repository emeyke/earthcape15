param(
    [string]$AzDevOpsToken,
    [string]$AzOrganization="EarthCape",
    [string]$AzProject="EarthCape",
    [ValidateSet("Build.Lab","easytest","Build.Release")]
    [string]$DefinitionName
)

$ErrorActionPreference = "stop"
if (!(Get-Module XpandPwsh -ListAvailable)) {
    Install-Module XpandPwsh -Scope CurrentUser -Force -Repository PSGallery
}

Add-AzBuild -Definition $DefinitionName


