
param(
    [string]$DXFeed=$env:DxFeed,
    [bool]$WarnAsError=$true,
    [string]$AzDevOpsToken=$env:AzDevOpsToken,
    [string]$root="$PSScriptRoot\..",   
    [string]$OutputPath="$root\bin",   
    [string]$TargetFramework="4.7.2",
    [string]$logsPath="$root\bin\logs",
    [string]$AzOrganization="EarthCape",
    [string]$AzProject="EarthCape",
    [string]$LabDefinition="Build.Lab",
    [string]$ReleaseDefinition="Build.Release",
    [string]$XpandPwshVersion="1.201.25.7"
)
$env:DX_NUGET_PUB=$DXFeed
if ($env:Build_DefinitionName){
    Install-Module XpandPwsh -Scope CurrentUser -Force -Repository PSGallery -AllowClobber -requiredVersion $XpandPwshVersion
    "XpandPwsh=$((Get-Module XpandPwsh -ListAvailable).Version)"
}
$ErrorActionPreference = "stop"
$VerbosePreference="continue"


if ((Get-Command "Switch-AzToEarthCape" -ErrorAction SilentlyContinue)){
    Switch-AzToEarthCape
}
else{
    $env:AzDevOpsToken=$AzDevOpsToken
    $env:AzOrganization=$AzOrganization
    $env:AzProject=$AzProject
}

Invoke-Script{
    $EarthCapeVersion  = & "$PSScriptRoot\CalculateVersion.ps1" $LabDefinition $ReleaseDefinition "Release"
    Get-ChildItem $root *.csproj -Recurse|ForEach-Object{
        Update-AssemblyInfoVersion -path $_.DirectoryName -version $EarthCapeVersion
    }
    $sln="$root\Projects\EarthcapeCore\EarthCape.sln"
    Start-Build -Path $sln -WarnAsError:$warnAsError -BinaryLogPath "$logsPath\build.binlog" -Configuration "Release"
}