param(
    [parameter()][string[]]$TestFilter=@("*AutotestWeb*.ets","*secondfilter*.ets"),
    [parameter()][int]$RetryOnFailure=2,
    [parameter()][string]$DXFeed=$env:DxFeed,
    [parameter()][string]$XpandPwshVersion="1.201.25.7",
    [parameter()][string]$easyTestPath="$PSScriptRoot\..\Projects\EarthCapeCore\EarthCape.Module\FunctionalTests\",
    [parameter()][string]$ArtifactDirectory="$PSScriptRoot\..\bin"
)

if ($env:Build_DefinitionName){
    Install-Module XpandPwsh -Scope CurrentUser -Force -Repository PSGallery -AllowClobber -requiredVersion $XpandPwshVersion
    "XpandPwsh=$((Get-Module XpandPwsh -ListAvailable).Version)"
    Get-ChildItem "$ArtifactDirectory\build"  | Move-Item -Destination $ArtifactDirectory -Verbose
}
$VerbosePreference="continue"
$labVersion=Get-AssemblyVersion "$ArtifactDirectory\EarthCape.Win.exe"
Set-VsoVariable build.updatebuildnumber $labVersion
Push-Location "$PSScriptRoot\..\Projects\EarthCapeCore\EarthCape.Web" 
if (!(Test-Path ".\bin")){
    New-Item -Name bin -ItemType Junction -Target "$PSScriptRoot\..\bin"
}

Pop-Location
Start-EasyTest -AssembliesDirectory $ArtifactDirectory -EasyTestDirectory $easyTestPath -DXFeed $DXFeed -TestFilter $TestFilter -RetryOnFailure $RetryOnFailure


