param(
    [parameter(Mandatory)]
    $LabDefinition,
    [parameter(Mandatory)]
    $ReleaseDefinition,
    [parameter(Mandatory)]
    $Release
)


$definition=$LabDefinition
if ($Release -eq "Release"){
    $definition=$ReleaseDefinition
}

Write-HostFormatted "Calculating $Release Version for $definition" -Section -Stream Verbose
function IsValidBuild {
    [CmdletBinding()]
    param (
        [parameter(ValueFromPipeline,Mandatory)]
        $BuildNumber
    )
    
    process {
        if ((Test-version $BuildNumber) -and (([version]$BuildNumber).Build -gt -1)){
            $BuildNumber
        }
    }
    
}
$lastBuild=(Get-AzBuilds -Definition $definition -Top 100 -Result succeeded -Status completed).buildNumber|IsValidBuild|Select-Object -First 1

Get-Variable lastBuild |Out-Variable
$dt=[datetime]::Now
$major="$($dt.Year.ToString().Substring(2))"
if (!$lastBuild){
    if ($Release -eq "Release"){
        $labBuild=[version]((Get-AzBuilds -Definition $LabDefinition -Top 100 -Result succeeded -Status completed).buildNumber|IsValidBuild|Select-Object -First 1)
        if (!$labBuild){
            throw "You cannot release without a valid"
        }
        $minor=$labBuild.Minor+1
        $newBuildVersion="$major.$minor.0"
    }
    else{
        $releasebuild=(Get-AzBuilds -Definition $ReleaseDefinition -Top 100 -Result succeeded -Status completed).buildNumber|IsValidBuild|Select-Object -First 1
        $minor=0
        $major=0
        if ($releasebuild){
            $minor=([version]$releasebuild).Minor
            $major=([version]$releasebuild).Major
        }
        $newBuildVersion+="$major.$minor.0"
    }    
}
else{
    $newBuildVersion=[version]$lastBuild
    if ($Release -eq "Release"){
        $newBuildVersion=Update-Version $lastBuild -minor
    }
    else{
        $releasebuild=(Get-AzBuilds -Definition $ReleaseDefinition -Top 100 -Result succeeded -Status completed).buildNumber|IsValidBuild|Select-Object -First 1
        $minor=([version]$lastBuild).Minor
        $build=([version]$lastBuild).Build
        if ($releasebuild){
            $minor=([version]$releasebuild).Minor
            if (([version]$releasebuild).Minor -gt ([version]$lastBuild).Minor){
                $build=0
            }
        }
        $newBuildVersion=Update-Version "$major.$minor.$build" -Build
    }
}
Get-Variable newBuildVersion|Out-Variable
Set-VsoVariable build.updatebuildnumber $newBuildVersion    
$newBuildVersion